package it.tn.unitn.disi.ppsmet.Entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;

import java.util.Objects;

@Entity(tableName = "tableAllergeni",
        foreignKeys = {@ForeignKey(entity = Ingredienti.class,
                parentColumns = "id",//Colanna della classe a qui si rifierisce
                childColumns  = "id", //ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE)})
public class Allergeni extends Ingredienti{
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @ColumnInfo(name = "descrizione")
    private String descrizione;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    @Ignore
    public Allergeni() {}
//--------------------------------------------------------------------------------------------------
    public Allergeni(Long id,
                     @NonNull String nome,
                     @NonNull String descrizione) {
        super(id, nome);
        this.descrizione = descrizione;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    @NonNull
    public String getDescrizione() {
        return descrizione;
    }
//--------------------------------------
    public void setDescrizione(@NonNull String descrizione) {
        this.descrizione = descrizione;
    }
//==================================================================================================
//==================================================================================================
//                                      EQUALS E HASH_CODE
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Allergeni)) return false;
        if (!super.equals(o)) return false;
        Allergeni allergeni = (Allergeni) o;
        return getDescrizione().equals(allergeni.getDescrizione());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getDescrizione());
    }
//==================================================================================================
//##################################################################################################
}

