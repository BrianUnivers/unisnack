package it.tn.unitn.disi.ppsmet.IncludeView_Setup.SelettoreABarra.OnChangeListener;

import android.view.View;

public interface OnChangeListener<T> {
/*################################################################################################*/
/*                                  CREAZIONE DEL LISTENER                                        */
/*################################################################################################*/
    public void onChangeListener(View view,T itemSelected);
/*################################################################################################*/
}
