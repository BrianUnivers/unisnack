package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListSpinnerWithAdd.AdapterWithSpinner;


import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListSpinnerWithAdd.AdapterWithSpinner.Interface.ListaSpinnerRVAdapter;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.MySpinnerAdapter.SpinnerAdapter_Allergeni;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.ViewInputSpinner_Text;
import it.tn.unitn.disi.ppsmet.R;

public class ListaSpinnerAllergeniRVAdapter extends ListaSpinnerRVAdapter<ListaSpinnerAllergeniRVAdapter.MyViewHoder, Allergeni>{
/*################################################################################################*/
/*                              CLASSE INTERNA                                                    */
/*################################################################################################*/
    /**
     * Questa classe è quella che permette alla RecyclingView Adapter di definire un layout custom
     * del singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public ViewGroup mainLayout;
        //Componeti
        public Button btnCancellaElemento;
        public ViewInputSpinner_Text<Allergeni> spinner;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            mainLayout      = itemView.findViewById(R.id.cly_GruppoSpinnerButton);
            spinner         = new ViewInputSpinner_Text(mainLayout,R.id.include_Spinner,R.string.lbl_IntolleranzeEAllergie, Allergeni.class);
            btnCancellaElemento = itemView.findViewById(R.id.btn_Cancella);
        }
/*================================================================================================*/
    }
/*################################################################################################*/

/*################################################################################################*/
/*                              CLASSE PRINCIPALE ListaSpinnerAllergeniRVAdapter                  */
/*################################################################################################*/
    private Context context;
    /**
     * Questa variabile contiene i valori di default dei primi spinner .
     */
    private List<Allergeni> listaValoriDefaultSpinner = new LinkedList<>();
    private HashMap<Allergeni,Boolean> defaultValueImpostato = new HashMap<>();
    /*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView di spinner e un set di elementi
     * che saranno i valori di default dei primi n spinner.
     */
    public ListaSpinnerAllergeniRVAdapter(Context context, List<Allergeni> listaAllergeni, Set<Allergeni> listaSpinnerPredefinititi) {
        this.adapter=new SpinnerAdapter_Allergeni(context,listaAllergeni);//For Spinner After Default Value
        this.context = context;
        if(listaSpinnerPredefinititi!=null){
            Iterator<Allergeni> iterator = listaSpinnerPredefinititi.iterator();
            while (iterator.hasNext()){
                Allergeni allergeniDefault  = iterator.next();
                defaultValueImpostato.put(allergeniDefault,false);
                //Aggiungi spinner con valori preimpostati
                listaValoriDefaultSpinner.add(allergeniDefault);
                listaIdSelezionatori.add(numProssimoElemetoAggiunto);
                numProssimoElemetoAggiunto++;
            }
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                              CERAZIONE DEL SINGOLO ELEMETOTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public ListaSpinnerAllergeniRVAdapter.MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View bloccoElemeto = inflater.inflate(R.layout.row_text_spinner_with_button, parent,false);
        return new ListaSpinnerAllergeniRVAdapter.MyViewHoder(bloccoElemeto);
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              INSERIRE L'ELEMETO NEL LAYOUT                                     */
/*================================================================================================*/
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista costruita
     * all'interno del crostruttore  di ListaSpinnerAllergeniRVAdapter con le agginte fatte in seguito.
     * Inoltre aggiunge la possibilità di premere sul singolo pulsante così da cancellare l'elemento
     * selezionato dalla lista che si stà mostrando.
     **/
    @Override
    public void onBindViewHolder(@NonNull ListaSpinnerAllergeniRVAdapter.MyViewHoder holder, int position) {
        Integer idSelezionatori = listaIdSelezionatori.get(position);
        //Inserisci i dati per spinner
        holder.spinner.setAdapter(adapter);
        if((position<listaValoriDefaultSpinner.size()) ){
            Allergeni defaultValue = listaValoriDefaultSpinner.get(position);
            if(!defaultValueImpostato.get(defaultValue)){
                defaultValueImpostato.put(defaultValue,true);
                holder.spinner.setSelection(defaultValue);
            }
        }

        holder.spinner.setOnClicListener(onChangeListener);

        selezionatori.put(idSelezionatori,holder.spinner);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  IMPOSTA COME BOTTONE IL CAMPIONATO
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        holder.btnCancellaElemento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eliminaElemento(idSelezionatori);
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
/*================================================================================================*/
}