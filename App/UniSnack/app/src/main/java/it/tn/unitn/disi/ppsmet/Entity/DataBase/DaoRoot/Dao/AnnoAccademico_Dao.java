package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.Date;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.M_fornisce_P;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

@Dao
public abstract class AnnoAccademico_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(AnnoAccademico annoAccademico);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Query("DELETE FROM tableAnnoAccademico")
    public abstract void deleteAll();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE By ID                                                    */
/*================================================================================================*/
    @Query("DELETE FROM tableAnnoAccademico WHERE id == :id")
    public abstract void delete(Long id);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = AnnoAccademico.class,
            onConflict = OnConflictStrategy.ABORT)
    public abstract void update(AnnoAccademico annoAccademico);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    @Query("SELECT * FROM tableAnnoAccademico")
    public abstract List<AnnoAccademico> getAll();
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableAnnoAccademico")
    public abstract LiveData<List<AnnoAccademico>> getAll_Synchronized();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    @Query("SELECT * FROM tableAnnoAccademico WHERE id == :id")
    public abstract AnnoAccademico getByPrimaryKey(Long id);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableAnnoAccademico WHERE id == :id")
    public abstract LiveData<AnnoAccademico> getByPrimaryKey_Synchronized(Long id);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET VALID SPECIFIC DAY                                          */
/*================================================================================================*/
    @Query("SELECT * FROM tableAnnoAccademico WHERE :date BETWEEN dataInizio AND dataFine" )
    public abstract AnnoAccademico getActiveAtSpecificDay(Date date);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableAnnoAccademico WHERE :date BETWEEN dataInizio AND dataFine")
    public abstract LiveData<AnnoAccademico> getActiveAtSpecificDay_Synchronized(Date date);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET VALID SPECIFIC DAY                                          */
/*================================================================================================*/
    public AnnoAccademico getActiveAtToday(){
        return getActiveAtSpecificDay(new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<AnnoAccademico> getActiveAtToday_Synchronized(){
        return getActiveAtSpecificDay_Synchronized(new Date());
    }
/*================================================================================================*/
/*################################################################################################*/
}
