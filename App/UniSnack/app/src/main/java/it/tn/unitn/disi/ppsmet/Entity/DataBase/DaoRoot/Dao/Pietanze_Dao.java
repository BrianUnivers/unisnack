package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import android.database.sqlite.SQLiteConstraintException;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.M_fornisce_P;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.P_contiene_A;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.PastoDelMese;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;

/*################################################################################################*/
/*################################################################################################*/
@Dao
public abstract class Pietanze_Dao extends PietanzeInsertExist_Dao {
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Query("DELETE FROM tablePietanze")
    public abstract void deleteAll();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Query("DELETE FROM tablePietanze WHERE id == :id ")
    public abstract void delete(Long id);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = Pietanze.class,
            onConflict = OnConflictStrategy.ABORT)
    public abstract void update(Pietanze pietanze);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    @Query("SELECT * FROM tablePietanze")
    public abstract List<Pietanze> getAll();
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tablePietanze")
    public abstract LiveData<List<Pietanze>> getAll_Synchronized();
/*------------------------------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa ")
    public abstract List<PietanzeDellaMense> getPietanzeOfMenseAll();
/*------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine," +
                    " M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa ")
    public abstract LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseAll_Synchronized();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    @Query("SELECT * FROM tablePietanze WHERE id == :id")
    public abstract Pietanze getByPrimaryKey(Long id);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tablePietanze WHERE id == :id")
    public abstract LiveData<Pietanze> getByPrimaryKey_Synchronized(Long id);
/*------------------------------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE  P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
                    "P.id == :id")
    public abstract PietanzeDellaMense getPietanzeOfMenseByPrimaryKey(Long id);
/*------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
                    "P.id == :id")
    public abstract LiveData<PietanzeDellaMense> getPietanzeOfMenseByPrimaryKey_Synchronized(Long id);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID TIPO PIETANZA                                         */
/*================================================================================================*/
    @Query("SELECT * " +
            "FROM tablePietanze AS P " +
            "WHERE P.id_tipoPietanza == :idTipoPietanza")
    public abstract List<Pietanze> getByIdOfTipoPietanza(Long idTipoPietanza);
/*------------------------------------------------------------------------*/
    @Query("SELECT * " +
            "FROM tablePietanze AS P " +
            "WHERE P.id_tipoPietanza == :idTipoPietanza")
    public abstract LiveData<List<Pietanze>> getByIdOfTipoPietanza_Synchronized(Long idTipoPietanza);
/*------------------------------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE  P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
                    "P.id_tipoPietanza == :idTipoPietanza ")
    public abstract List<PietanzeDellaMense> getPietanzeOfMenseByIdOfTipoPietanza(Long idTipoPietanza);
/*------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE  P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
                    "P.id_tipoPietanza == :idTipoPietanza ")
    public abstract LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfTipoPietanza_Synchronized(Long idTipoPietanza);
/*todo================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID MENSA AND TIPO MENU'                                  */
/*================================================================================================*/
    @Query( "SELECT  F.*, " +
            "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
            "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
            "P.id_tipoPietanza AS P_id_tipoPietanza, " +
            "TM.id AS TM_id, TM.nome AS TM_nome, " +
            "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
            "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
            "M.id == :idMensa AND  TM.id == :idTipoMenu ")
    public abstract List<PietanzeDellaMense> getPietanzeOfMenseByIdOfMensa_TipoMenu(Long idMensa,Long idTipoMenu);
/*------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
            "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
            "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
            "P.id_tipoPietanza AS P_id_tipoPietanza, " +
            "TM.id AS TM_id, TM.nome AS TM_nome, " +
            "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
            "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
            "M.id == :idMensa AND  TM.id == :idTipoMenu ")
    public abstract LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_Synchronized(Long idMensa,Long idTipoMenu);
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU'                                 */
/*                          SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR                         */
/*================================================================================================*/
    @Query("SELECT P.* " +
            "FROM tablePietanze AS P , tableM_fornisce_P AS M " +
            "WHERE   P.id == M.id_pietanza AND M.id_mensa == :idMensa AND M.id_tipoMenu == :idTipoMenu AND " +
                    "M.anno == :anno AND M.mese == :mese AND M.giornoSettimana == :giornoSettimana")
    public abstract List<Pietanze> getByIdOfMensa_TipoMenu_SpecificMoment(Long idMensa,
                                                                          Long idTipoMenu,
                                                                          Integer anno,
                                                                          Integer mese,
                                                                          Integer giornoSettimana);
/*------------------------------------------------------------------------*/
    @Query("SELECT P.* " +
            "FROM tablePietanze AS P , tableM_fornisce_P AS M " +
            "WHERE   P.id == M.id_pietanza AND M.id_mensa == :idMensa AND M.id_tipoMenu == :idTipoMenu AND " +
            "M.anno == :anno AND M.mese == :mese AND M.giornoSettimana == :giornoSettimana")
    public abstract LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_SpecificMoment_Synchronized(Long idMensa,
                                                                                                 Long idTipoMenu,
                                                                                                 Integer anno,
                                                                                                 Integer mese,
                                                                                                 Integer giornoSettimana);
/*------------------------------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
                    "M.id == :idMensa AND  TM.id == :idTipoMenu AND " +
                    "F.anno == :anno AND F.mese == :mese AND F.giornoSettimana == :giornoSettimana")
    public abstract List<PietanzeDellaMense> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment(Long idMensa,
                                                                                                   Long idTipoMenu,
                                                                                                   Integer anno,
                                                                                                   Integer mese,
                                                                                                   Integer giornoSettimana);
/*------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
                    "M.id == :idMensa AND  TM.id == :idTipoMenu AND " +
                    "F.anno == :anno AND F.mese == :mese AND F.giornoSettimana == :giornoSettimana")
    public abstract LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment_Synchronized(Long idMensa,
                                                                                                                          Long idTipoMenu,
                                                                                                                          Integer anno,
                                                                                                                          Integer mese,
                                                                                                                          Integer giornoSettimana);
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU' SPECIFIC DAY                    */
/*================================================================================================*/
    public List<Pietanze> getByIdOfMensa_TipoMenu_SpecificDay(@NonNull Long idMensa,
                                                              @NonNull Long idTipoMenu,
                                                              @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getByIdOfMensa_TipoMenu_SpecificMoment(idMensa,idTipoMenu,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_SpecificDay_Synchronized(   @NonNull Long idMensa,
                                                                                        @NonNull Long idTipoMenu,
                                                                                        @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getByIdOfMensa_TipoMenu_SpecificMoment_Synchronized(idMensa,idTipoMenu,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------------------------------*/
    public List<PietanzeDellaMense> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificDay( @NonNull Long idMensa,
                                                                                           @NonNull Long idTipoMenu,
                                                                                           @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment(idMensa,idTipoMenu,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificDay_Synchronized(@NonNull Long idMensa,
                                                                                                    @NonNull Long idTipoMenu,
                                                                                                    @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment_Synchronized(idMensa,idTipoMenu,anno,mese,giornoSettimana);
    }
/*todo================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU' AND TIPO PIETANZA               */
/*                          SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR                         */
/*================================================================================================*/
    @Query("SELECT P.* " +
            "FROM tablePietanze AS P , tableM_fornisce_P AS M " +
            "WHERE   P.id == M.id_pietanza AND M.id_mensa == :idMensa AND M.id_tipoMenu == :idTipoMenu AND " +
                    "P.id_tipoPietanza == :idTipoPietanza AND " +
                    "M.anno == :anno AND M.mese == :mese AND M.giornoSettimana == :giornoSettimana")
    public abstract List<Pietanze> getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment(Long idMensa,
                                                                                       Long idTipoMenu,
                                                                                       Long idTipoPietanza,
                                                                                       Integer anno,
                                                                                       Integer mese,
                                                                                       Integer giornoSettimana);
/*------------------------------------------------------------------------*/
    @Query("SELECT P.* " +
            "FROM tablePietanze AS P , tableM_fornisce_P AS M " +
            "WHERE   P.id == M.id_pietanza AND M.id_mensa == :idMensa AND M.id_tipoMenu == :idTipoMenu AND " +
            "P.id_tipoPietanza == :idTipoPietanza AND " +
            "M.anno == :anno AND M.mese == :mese AND M.giornoSettimana == :giornoSettimana")
    public abstract LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_Synchronized( Long idMensa,
                                                                                                               Long idTipoMenu,
                                                                                                               Long idTipoPietanza,
                                                                                                               Integer anno,
                                                                                                               Integer mese,
                                                                                                               Integer giornoSettimana);
/*------------------------------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
            "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
            "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
            "P.id_tipoPietanza AS P_id_tipoPietanza, " +
            "TM.id AS TM_id, TM.nome AS TM_nome, " +
            "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
            "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
            "M.id == :idMensa AND  TM.id == :idTipoMenu AND P.id_tipoPietanza == :idTipoPietanza AND " +
            "F.anno == :anno AND F.mese == :mese AND F.giornoSettimana == :giornoSettimana")
    public abstract List<PietanzeDellaMense> getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment(
                   Long idMensa,
                   Long idTipoMenu,
                   Long idTipoPietanza,
                   Integer anno,
                   Integer mese,
                   Integer giornoSettimana);
/*------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
            "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
            "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
            "P.id_tipoPietanza AS P_id_tipoPietanza, " +
            "TM.id AS TM_id, TM.nome AS TM_nome, " +
            "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
            "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
            "M.id == :idMensa AND  TM.id == :idTipoMenu AND  P.id_tipoPietanza == :idTipoPietanza AND " +
            "F.anno == :anno AND F.mese == :mese AND F.giornoSettimana == :giornoSettimana")
    public abstract LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_Synchronized(
                  Long idMensa,
                  Long idTipoMenu,
                  Long idTipoPietanza,
                  Integer anno,
                  Integer mese,
                  Integer giornoSettimana);
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU' AND TIPO PIETANZA SPECIFIC DAY  */
/*================================================================================================*/
    public List<Pietanze> getByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay(@NonNull Long idMensa,
                                                              @NonNull Long idTipoMenu,
                                                              @NonNull Long idTipoPietanza,
                                                              @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment(idMensa,idTipoMenu,idTipoPietanza,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay_Synchronized(  @NonNull Long idMensa,
                                                                                                    @NonNull Long idTipoMenu,
                                                                                                    @NonNull Long idTipoPietanza,
                                                                                                    @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_Synchronized(   idMensa,idTipoMenu,idTipoPietanza,
                                                                                        anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------------------------------*/
    public List<PietanzeDellaMense> getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay( @NonNull Long idMensa,
                                                                                                     @NonNull Long idTipoMenu,
                                                                                                     @NonNull Long idTipoPietanza,
                                                                                                     @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment( idMensa,idTipoMenu,idTipoPietanza,
                                                                                        anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay_Synchronized( @NonNull Long idMensa,
                                                                                                                            @NonNull Long idTipoMenu,
                                                                                                                            @NonNull Long idTipoPietanza,
                                                                                                                            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_Synchronized(idMensa,idTipoMenu,idTipoPietanza,
                                                                                                    anno,mese,giornoSettimana);
    }
/*================================================================================================*/
/*TODO================================================================================================*/
/*                          METODO GET SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR              */
/*================================================================================================*/
    @Query( "SELECT  F.*, " +
            "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
            "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
            "P.id_tipoPietanza AS P_id_tipoPietanza, " +
            "TM.id AS TM_id, TM.nome AS TM_nome, " +
            "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
            "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
            "F.anno == :anno AND F.mese == :mese AND F.giornoSettimana == :giornoSettimana")
    public abstract List<PietanzeDellaMense> getPietanzeOfMense_SpecificMoment(Integer anno,
                                                                               Integer mese,
                                                                               Integer giornoSettimana);
/*------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
            "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
            "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
            "P.id_tipoPietanza AS P_id_tipoPietanza, " +
            "TM.id AS TM_id, TM.nome AS TM_nome, " +
            "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
            "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
            "F.anno == :anno AND F.mese == :mese AND F.giornoSettimana == :giornoSettimana")
    public abstract LiveData<List<PietanzeDellaMense>> getPietanzeOfMense_SpecificMoment_Synchronized(Integer anno,
                                                                                                      Integer mese,
                                                                                                      Integer giornoSettimana);
/*------------------------------------------------------------------------------------------------*/
/*                          METODO GET SPECIFIC DAY                                               */
/*------------------------------------------------------------------------------------------------*/
    public List<PietanzeDellaMense> getPietanzeOfMense_SpecificDay( @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMense_SpecificMoment(anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMense_SpecificDay_Synchronized( @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMense_SpecificMoment_Synchronized(anno,mese,giornoSettimana);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR  */
/*================================================================================================*/
    @Query( "SELECT  F.*, " +
            "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
            "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
            "P.id_tipoPietanza AS P_id_tipoPietanza, " +
            "TM.id AS TM_id, TM.nome AS TM_nome, " +
            "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
            "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
            "M.id == :idMensa AND " +
            "F.anno == :anno AND F.mese == :mese AND F.giornoSettimana == :giornoSettimana")
    public abstract List<PietanzeDellaMense> getPietanzeOfMenseByIdOfMensa_SpecificMoment( Long idMensa,
                                                                                           Integer anno,
                                                                                           Integer mese,
                                                                                           Integer giornoSettimana);
/*------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
            "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
            "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
            "P.id_tipoPietanza AS P_id_tipoPietanza, " +
            "TM.id AS TM_id, TM.nome AS TM_nome, " +
            "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
            "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
            "M.id == :idMensa AND " +
            "F.anno == :anno AND F.mese == :mese AND F.giornoSettimana == :giornoSettimana")
    public abstract LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_SpecificMoment_Synchronized( Long idMensa,
                                                                                                                  Integer anno,
                                                                                                                  Integer mese,
                                                                                                                  Integer giornoSettimana);
/*------------------------------------------------------------------------------------------------*/
/*                          METODO GET by ID MENSA SPECIFIC DAY                                   */
/*------------------------------------------------------------------------------------------------*/
    public List<PietanzeDellaMense> getPietanzeOfMenseByIdOfMensa_SpecificDay( @NonNull Long idMensa,
                                                                               @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_SpecificMoment(idMensa,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_SpecificDay_Synchronized( @NonNull Long idMensa,
                                                                                                      @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_SpecificMoment_Synchronized(idMensa,anno,mese,giornoSettimana);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by TIPO MENU' SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR*/
/*================================================================================================*/
    @Query( "SELECT  F.*, " +
            "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
            "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
            "P.id_tipoPietanza AS P_id_tipoPietanza, " +
            "TM.id AS TM_id, TM.nome AS TM_nome, " +
            "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
            "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
            "TM.id == :idTipoMenu AND " +
            "F.anno == :anno AND F.mese == :mese AND F.giornoSettimana == :giornoSettimana")
    public abstract List<PietanzeDellaMense> getPietanzeOfMenseByIdOfTipoMenu_SpecificMoment(  Long idTipoMenu,
                                                                                               Integer anno,
                                                                                               Integer mese,
                                                                                               Integer giornoSettimana);
/*------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, " +
            "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
            "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
            "P.id_tipoPietanza AS P_id_tipoPietanza, " +
            "TM.id AS TM_id, TM.nome AS TM_nome, " +
            "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine, " +
            "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
            "TM.id == :idTipoMenu AND " +
            "F.anno == :anno AND F.mese == :mese AND F.giornoSettimana == :giornoSettimana")
    public abstract LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfTipoMenu_SpecificMoment_Synchronized(  Long idTipoMenu,
                                                                                                                      Integer anno,
                                                                                                                      Integer mese,
                                                                                                                      Integer giornoSettimana);
/*------------------------------------------------------------------------------------------------*/
/*                          METODO GET by TIPO MENU' SPECIFIC DAY                                 */
/*------------------------------------------------------------------------------------------------*/
    public List<PietanzeDellaMense> getPietanzeOfMenseByIdOfTipoMenu_SpecificDay(@NonNull Long idTipoMenu,
                                                                                 @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfTipoMenu_SpecificMoment(idTipoMenu,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfTipoMenu_SpecificDay_Synchronized(@NonNull Long idTipoMenu,
                                                                                                        @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfTipoMenu_SpecificMoment_Synchronized(idTipoMenu,anno,mese,giornoSettimana);
    }
/*================================================================================================*/
/*todo================================================================================================*/
/*                         METODO GET by ID PASTO DEL MESE                                        */
/*================================================================================================*/
    public List<Pietanze> getByIdOfPastoDelMese(PastoDelMese pastoDelMese){
        return this.getByIdOfPastoDelMese(  pastoDelMese.getId_utente(),
                                            pastoDelMese.getMese(),
                                            pastoDelMese.getAnno());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfPastoDelMese_Synchronized(PastoDelMese pastoDelMese){
        return this.getByIdOfPastoDelMese_Synchronized(pastoDelMese.getId_utente(),
                                                        pastoDelMese.getMese(),
                                                        pastoDelMese.getAnno());
    }
/*------------------------------------------------------------------------------------------------*/
    @Query( "SELECT  P.* " +
            "FROM tablePietanze AS P, tablePdM_propone_P AS Prop " +
            "WHERE   Prop.id_proponente == :idUtente AND Prop.id_mese == :mese AND Prop.id_anno == :anno AND " +
                    "P.id == Prop.id_pietanza ")
    protected abstract List<Pietanze> getByIdOfPastoDelMese(Long idUtente,Short mese,Short anno);
/*------------------------------------------------------------------------*/
    @Query( "SELECT  P.* " +
            "FROM tablePietanze AS P, tablePdM_propone_P AS Prop " +
            "WHERE   Prop.id_proponente == :idUtente AND Prop.id_mese == :mese AND Prop.id_anno == :anno AND " +
                    "P.id == Prop.id_pietanza ")
    protected abstract LiveData<List<Pietanze>> getByIdOfPastoDelMese_Synchronized(Long idUtente,Short mese,Short anno);
/*================================================================================================*/
/*################################################################################################*/
}
/*################################################################################################*/
/*################################################################################################*/
/*################################################################################################*/
@Dao
abstract class PietanzeInsertExist_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(Pietanze pietanze);
/*------------------------------------------------------------------------*/
    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long insertPietazaAlMenuDellaMensa(Pietanze pietanze,
                                              Mense mense,
                                              TipoMenu tipoMenu,
                                              Integer anno,
                                              Integer mese,
                                              Integer giornoSettimana) {
        if (mese <= 0 || mese > 12) {
            throw new SQLiteConstraintException("Mese inserito errato deve essere compreso tra 1 e 12 mentre e' " + mese + ".");
        }
        if (giornoSettimana <= 0 || giornoSettimana > 7) {
            throw new SQLiteConstraintException("Il giorno della settimana inserito errato deve essere compreso tra 1 e 7 mentre e' " + giornoSettimana + ".");
        }
        Long id = exist(pietanze);
        if (id == null) {
            id = this.insert(pietanze);
        }
        M_fornisce_P relazione = new M_fornisce_P(mense.getId(), id, mese, anno, giornoSettimana,tipoMenu.getId());
        this.insert(relazione);
        return id;
    }
/*------------------------------------------------------------------------*/
    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public Long insertAssociazioneIngrediente(Pietanze pietanze, Ingredienti ingredienti) {
        List<Ingredienti> listaIngredienti = new LinkedList<Ingredienti>();
        listaIngredienti.add(ingredienti);
        List<Long> ris = this.insertAssociazioneIngrediente(pietanze, listaIngredienti);
        if (ris.isEmpty()) {
            throw new SQLiteConstraintException("Non è stato possible creare l'associazione.");
        }
        return ris.get(0);
    }
/*------------------------------------------------------------------------*/
    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public List<Long> insertAssociazioneIngrediente(Pietanze pietanze, List<Ingredienti> ingredienti) {
        Iterator<Ingredienti> iterator = ingredienti.iterator();
        List<Long> ris = new ArrayList<>();
        while (iterator.hasNext()) {
            P_contiene_A contiene = new P_contiene_A(pietanze.getId(), iterator.next().getId());
            Long id = this.inset(contiene);
            ris.add(id);
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    protected abstract long insert(M_fornisce_P m_fornisce_p);

/*------------------------------------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    protected abstract long inset(P_contiene_A contiene);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         EXIST                                                                  */
/*================================================================================================*/
    @Transaction
    public long exist(Pietanze pietanze) {
        return this.exist(pietanze.getNome(),
                pietanze.getCalorie(),
                pietanze.getCarboidrati(),
                pietanze.getGrassi(),
                pietanze.getProteine(),
                pietanze.getImmagine(),
                pietanze.getUrlImmagine(),
                pietanze.getId_tipoPietanza());
    }

/*------------------------------------------------------------------------------------------------*/
    @Query("SELECT P.id " +
            "FROM tablePietanze AS P " +
            "WHERE   P.calorie == :calorie AND " +
            "P.carboidrati == :carboidrati AND " +
            "P.grassi == :grassi AND " +
            "P.id_tipoPietanza == :id_tipoPietanza AND " +
            "P.immagine == :immagine AND " +
            "P.urlImmagine == :urlImmagine AND " +
            "P.nome == :nome AND " +
            "P.proteine == :proteine")
    protected abstract long exist(String nome,
                                  Double calorie,
                                  Double carboidrati,
                                  Double grassi,
                                  Double proteine,
                                  byte[] immagine,
                                  String urlImmagine,
                                  Long id_tipoPietanza);
/*================================================================================================*/
/*################################################################################################*/
}