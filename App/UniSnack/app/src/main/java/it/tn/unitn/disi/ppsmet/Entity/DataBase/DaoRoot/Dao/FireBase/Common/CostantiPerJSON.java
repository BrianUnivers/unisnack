package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common;

public interface CostantiPerJSON {
/*################################################################################################*/
/*================================================================================================*/
/*                         ETICHETTE DELLE LISTE OGGETTI JSON                                     */
/*================================================================================================*/
    public static final String L_ALLERGENI                                          = "Allergeni";
    public static final String L_ANNO_ACCADEMICO                                    = "AnnoAccademico";

    public static final String L_CORSI                                              = "Corsi";
    public static final String L_CORSI_DEL_UNIVERSITA                               = "Corsi_del_Universita";

    public static final String L_INGREDIENTI                                        = "Ingredienti";
    public static final String L_INGREDIENTI_DEL_PIETANZA                           = "Ingredienti_del_Pietanza";
    public static final String L_INTOLLERANTE                                       = "Intollerante";
    public static final String L_INTOLLERANZE_DEL_UTENTE                            = "Intolleranze_del_Utente";
    public static final String L_ISCRITTI                                           = "Iscritti";


    //public static final String L_KEY_PASTO_DEL_MESE                           = "Key_PastoDelMese";
    //public static final String L_KEY_PIETANZE                                 = "Key_Pietanze";
    //public static final String L_KEY_ORDINAZIONI                              = "Key_Ordinazioni";
    //public static final String L_KEY_UTENTI                                   = "Key_Utenti";

    public static final String L_MENSE                                              = "Mense";
    public static final String L_MENSE_DEL_UNIVERSITA                               = "Mense_del_Universita";

    public static final String L_ORDINAZIONI                                        = "Ordinazioni";
    public static final String L_ORDINAZIONI_DEL_UTENTE                             = "Ordinazioni_del_Utente";

    public static final String L_PARTECIPA_UNIVERSITA                               = "Partecipa_Universita";
    public static final String L_PASTO_DEL_MESE                                     = "PastoDelMese";
    public static final String L_PASTO_DEL_MESE_PROPOSTO_DAL_UTENTE                 = "PastoDelMese_proposto_Utente";
    public static final String L_PASTO_DEL_MESE_VOTATI_DAL_UTENTE                   = "PastoDelMese_votati_Utente";
    public static final String L_PERIODO_PER_TIPO_MENU_DEL_MENSE                    = "Periodo_per_TipoMenu_del_Mense";
    public static final String L_PIETANZE                                           = "Pietanze";
    public static final String L_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE       = "Pietanze_del_Periodo_per_TipoMenu_del_Mense";
    public static final String L_PROPOSTE                                           = "Proposte";

    public static final String L_SOLO_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE  = "SoloPietanze_del_Periodo_per_TipoMenu_del_Mense";

    public static final String L_TESSERE                                            = "Tessere";
    public static final String L_TIPO_MENU                                          = "TipoMenu";
    public static final String L_TIPO_PASTO                                         = "TipoPasto";
    public static final String L_TIPO_PIETANZA                                      = "TipoPietanza";

    public static final String L_UNIVERSITA                                         = "Universita";
    public static final String L_UNIVERSITA_E_TESSERE_DEL_UTENTE                    = "UniversitaETessere_del_Utente";
    public static final String L_UTENTI                                             = "Utenti";
    public static final String L_UTENTI_DEL_CORSO_DEL_UNIVERSITA                    = "Utenti_del_Corso_del_Universita";

    public static final String L_VOTI                                               = "Voti";
    public static final String L_VOTI_PASTO_DEL_MESE                                = "Voti_PastoDelMese";
/*================================================================================================*/
/*================================================================================================*/
/*                         ETICHETTE DEI PARAMETRI JSON                                           */
/*================================================================================================*/
    public static final String P_ANNO                   = "anno";
    public static final String P_ANNO__MESE             = "anno__mese";

    public static final String P_CALORIE                = "calorie";
    public static final String P_CARBOIDRATI            = "carboidrati";
    public static final String P_COGNOME                = "cognome";

    public static final String P_DATA                   = "data";
    public static final String P_DATA_DEL_VALUTAZIONE   = "dataDelValutazione";
    public static final String P_DATA_INIZIO            = "dataInizio";
    public static final String P_DATA_FINE              = "dataFine";
    public static final String P_DESCRIZIONE            = "descrizione";

    public static final String P_EMAIL                  = "email";
    public static final String P_EMAIL__HASHED_PSW      = "email__hashedPSW";

    public static final String P_GIORNO                 = "giorno";
    public static final String P_GIORNO_SETTIMANA       = "giornoSettimana";
    public static final String P_GRASSI                 = "grassi";

    public static final String P_HASHED_PSW             = "hashedPSW";
    public static final String P_KEY                    = "id";

    public static final String P_ID_ANNO_ACCADEMICO     = "idAnnoAccademico";
    public static final String P_ID_TIPO_PASTO          = "idTipoPasto";
    public static final String P_ID_TIPO_PIETANZA       = "idTipoPietanza";

    public static final String P_LATITUDINE             = "latitudine";
    public static final String P_LONGITUDINE            = "longitudine";

    public static final String P_MATRICOLA              = "matricola";
    public static final String P_MESE                   = "mese";

    public static final String P_NATO                   = "nato";
    public static final String P_NOME                   = "nome";
    public static final String P_NOME_TIPO              = "nomeTipo";

    public static final String P_PROTEINE               = "proteine";

    public static final String P_SALT                   = "salt";
    public static final String P_SIGLA                  = "sigla";
    public static final String P_STATO_TESSERA          = "statoTessera";

    public static final String P_URL_CODICE_A_BARRE     = "urlCodiceABarre";
    public static final String P_URL_IMMAGINE           = "urlImmagine";
    public static final String P_URL_QRCODE             = "urlQRcode";

    public static final String P_VOTO                   = "voto";
    public static final String P_VOTO_COTTURA           = "votoCottura";
    public static final String P_VOTO_SAPORE            = "votoSapore";
    public static final String P_VOTO_TEMPERATURA       = "votoTemperatura";
/*================================================================================================*/
/*================================================================================================*/
/*                         SEPARATORI DEI VLORI                                                   */
/*================================================================================================*/
    public static String S_SEPARARORE = "/";
    public static String S_SEPARARORE_PER_CHIAVI = "-";
    public static String S_SEPARARORE_PER_PARAMETRI = "__";
/*================================================================================================*/
/*################################################################################################*/
}
