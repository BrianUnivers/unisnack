package it.tn.unitn.disi.ppsmet.ActivityManager.RegistrazioneActivity;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import com.google.firebase.database.annotations.NotNull;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.AnnoAccademico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Corsi_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.IdentificativiSessioni_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Tessere_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Universita_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Utenti_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DatiPerSessione.IdentificativiSessioni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class RegistrazioneViewModel extends AndroidViewModel {
    private Long  TIMEOUT_FB = 10000L;
    private Timer TIMER = new Timer();
/*################################################################################################*/
    private IdentificativiSessioni_Repository rIdentificativiSessioni;
    private Universita_Repository     rUniversita;
    private Tessere_Repository        rTessere;
    private Utenti_Repository         rUtenti;
    private Corsi_Repository          rCorsi;
    private AnnoAccademico_Repository rAA;
/*================================================================================================*/
/*                                  COSTRUTTORE VIEW MODEL                                        */
/*================================================================================================*/
    /**
     * Questo metodo costruttore del viewmode inizializza le repository che si interfacciano con entrambi
     * i database sia interno che estreno a seconda delle operazioni che vengono richeste. Che verranno
     * utilizate dall'activity e i suoi fragment.
     * @param application
     */
    public RegistrazioneViewModel(@NonNull Application application) {
        super(application);
        rIdentificativiSessioni = new IdentificativiSessioni_Repository(application);
        rUniversita = new Universita_Repository(application);
        rUtenti     = new Utenti_Repository(application);
        rCorsi      = new Corsi_Repository(application);
        rAA         = new AnnoAccademico_Repository(application);
        rTessere    = new Tessere_Repository(application);
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  AZZIONI LEGATE ALLA GESITIONE DEI DATABASE                    */
/*################################################################################################*/
/*================================================================================================*/
/*                                  GET ALL UNIVERISTA'                                           */
/*================================================================================================*/
    /**
     * Questo metodo restituisce la lista completa di tutte le università presenti nel database
     * @return
     */
    public LiveData<List<Universita>> getAllUniversita(){
        return rUniversita.getAll();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  GET ALL CORSI BY UNIVERSITA                                   */
/*================================================================================================*/
    /**
     * Questo metodo restituisce la lista completa dei corsi presenti in una università specificata
     * dal parametro passato.
     * @param universita
     * @return
     */
    public LiveData<List<Corsi>> getAllCorsiByUniversita(Universita universita){
        return rCorsi.getByIdOfUniversita(universita);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  GET ALL CORSI BY UNIVERSITA                                   */
/*================================================================================================*/
    /**
     * Questo metodo restituisce l'anno accademico attuale.
     * @return
     */
    public LiveData<AnnoAccademico> getActiveAtToday(){
        return rAA.getActiveAtToday();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  ESISTE UTENTE CON EMAIL                                       */
/*================================================================================================*/
    /**
     * Questo metodo verifica l'esistenza di un eventuale utente avente la stessa email che ho
     * passato come pararametro. Questa verifica può essere fattoa solo con il dispositivo
     * online alirimenti sacatterà il timer che invierà un messaggio null.
     * Se è presente la risposta verà inserira in un oggetto livedata contenente true se presene e false
     * altrimeni.
     * @param email
     * @return
     */
    public LiveData<Boolean> esiteEmail (@NotNull String email){
        MediatorLiveData<Boolean> booleanoRisposta = new MediatorLiveData();
        //RICHIESTA VERA E PROPRIA
        booleanoRisposta.addSource(rUtenti.esiteEmail(email) ,
                new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean esite) {
                        booleanoRisposta.removeObserver(this);
                        booleanoRisposta.setValue(esite);

                    }
                });
        //TIMEOUT DELLA RICHIESTA D'AUTENTICAZIONE
        TIMER.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if(booleanoRisposta.getValue()==null){
                            booleanoRisposta.postValue(null);
                        }
                    }
                }, TIMEOUT_FB);
        return booleanoRisposta;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  ESISTE UTENTE CON LA STESSA MATRICOLA                         */
/*                                  NELLA STTESSA UNIVERSITA E CORSO                              */
/*================================================================================================*/
    /**
     * Questo metodo verifica l'esistenza di un eventuale utente avente la stessa matricola e che frequenti
     * lo stesso corso e universita. Questa verifica può essere fattoa solo con il dispositivo
     * online alirimenti sacatterà il timer che invierà un messaggio null.
     * Se è presente la risposta verà inserira in un oggetto livedata contenente true se presene e false
     * altrimeni.
     * @param idUniversita
     * @param idCrso
     * @param matricola
     * @return
     */
    public LiveData<Boolean> esiteMatricola (@NotNull Long idUniversita,@NotNull Long idCrso ,@NotNull Integer matricola){
        MediatorLiveData<Boolean> booleanoRisposta = new MediatorLiveData();
        //RICHIESTA VERA E PROPRIA
        booleanoRisposta.addSource(rUtenti.esiteMatricola(idUniversita,idCrso,matricola),
                new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean esite) {
                        booleanoRisposta.removeObserver(this);
                        booleanoRisposta.setValue(esite);
                    }
                });
        //TIMEOUT DELLA RICHIESTA D'AUTENTICAZIONE
        TIMER.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if(booleanoRisposta.getValue()==null){
                            booleanoRisposta.postValue(null);
                        }
                    }
                }, TIMEOUT_FB);
        return booleanoRisposta;
    }
/*================================================================================================*/
/*                                  ISCRIZIONI UTENTI                                             */
/*================================================================================================*/
    /**
     * Questo metodo effetua l'inserimento del utente passato come parametro nel database
     * (esterno,interno) e restituisce  l'id del utente appena inserito.
     * @param utenti
     * @return
     */
    public LiveData<Long> iscrizioneUtente(Utenti utenti){
        return rUtenti.iscrizioneUtente(utenti);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  AGGIUNI TESSERE DI PARTECIPAZIONE                             */
/*================================================================================================*/
    /**
     * Questo metodo efetua l'isermeto delle tesse delle alte universita a cui l'utente non è iscritto
     * ma a cui partecipa, di cui può avere delle tessere attive o disattive.
     * @param utenti
     * @param listaTessere
     */
    public void inserisciTessereDiPartecipazione(Utenti utenti, List<Tessere> listaTessere){
        rTessere.insert(utenti,listaTessere);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  ATTIVA NUOVA SESSIONE                                         */
/*================================================================================================*/
    /**
     * Questo metodo permette di aggiungere una sessione così da ricocdarne l'assocazione tra
     * all'identificativo di una sessione  e l'utente.
     * @param utenti
     * @param idSessione
     */
    public void aggiungiNuovaSessioneAttiva(Utenti utenti,String idSessione){
        rIdentificativiSessioni.insertSessioneByUtenteAndIdSessione(utenti,idSessione);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  GET UTENTE BY PRIMARY KEY                                     */
/*================================================================================================*/
    /**
     * Questo metodo permette di richiedere al database l'utete a cui appartiene l'id passato come
     * parametro.
     * @param id
     * @return
     */
    public LiveData<Utenti> getUtenteByPrimaryKey(Long id){
        return rUtenti.getByPrimaryKey(id);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  METODI PER LA GESTIONE DEI DATI SLEGATI DA PERIFERICHE        */
/*################################################################################################*/
/*================================================================================================*/
/*                                  VERIFICA VALIDITA STRIGA                                      */
/*================================================================================================*/
    /**
     * Verifica che il testo passato non sia vuoto o inesitete, se non ci troviamo in uno di questi
     * casi la variabile è ritenuta valida.
     * @param testo
     * @return
     */
    public boolean verificaValiditaTesto(String testo){
        return ((testo!=null) && (!testo.equals("")));
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  VERIFICA VALIDITA NUMERO POSITIVO DIVERSO DA ZERO             */
/*================================================================================================*/
    /**
     * Questo metodo verifica che il valore passato si strettamente maggiore di zero e diverso da null.
     * @param num
     * @return
     */
    public boolean verificaValiditaNumeroInteroDiversoDaZero(Integer num){
        return ((num!=null) && (num>0));
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  VERIFICA VALIDITA STRIGA                                      */
/*================================================================================================*/
    private int maggioreEta = 18;
    private int etaMassima  = 100;
    /**
     * Questo metodo verifica che l'utente abbia inserito una data valaida, cioè che sia maggiorenne
     * e abbia un limite superiore dell'età pari al valore di "etaMassima".
     * @param dataDiNascita
     * @return
     */
    public boolean verificaValiditaDataDiNascitaMaggiorenne(Date dataDiNascita){
        Calendar maggiorenneOggi = Calendar.getInstance();
        maggiorenneOggi.set(Calendar.YEAR,(maggiorenneOggi.get(Calendar.YEAR)-maggioreEta));
        maggiorenneOggi.set(Calendar.DAY_OF_MONTH,1);
        Calendar etaMassimaDaOggi = Calendar.getInstance();
        etaMassimaDaOggi.set(Calendar.YEAR,(etaMassimaDaOggi.get(Calendar.YEAR)-etaMassima));
        etaMassimaDaOggi.set(Calendar.DAY_OF_MONTH,1);
        return ((dataDiNascita!=null) && (! (   (dataDiNascita.after(maggiorenneOggi.getTime())) ||
                                                (dataDiNascita.before(etaMassimaDaOggi.getTime())) )));
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  VERIFICA VALIDITA EMAIL                                       */
/*================================================================================================*/
    private static final String regex = "^(.+)@(.+)\\.(.+)$";
    /**
     * Questo metodo verifica se una stringa può essere definibile come email.
     */
    public boolean verificaValiditaEmail(String email){
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return ((verificaValiditaTesto(email)) && (matcher.matches()));
    }
/*================================================================================================*/
/*################################################################################################*/
}
