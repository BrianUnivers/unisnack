package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Universita_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Universita_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

public class Universita_ThreadAggiornamento  extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA UNIVERSITA LOCALE                */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "Universita" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_Universita";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista delle università del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private List<Universita> listaUniversita;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * universita.
         * @param listaUniversita Questo parametro contiene la lista di tutti gli elementi
         *                        tra qui quelli nuovi e quelli già presenti ma da modificare.
         */
        public ThreadAggiornamento(List<Universita> listaUniversita) {
            super();
            this.listaUniversita =listaUniversita;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                              */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella universita.");
            aggionaTabellaUniversita(listaUniversita);
            Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella universita.");
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param listaUniversita
         */
        private synchronized void aggionaTabellaUniversita(List<Universita> listaUniversita){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella universita.");
            //Dati presenti nel database locale.
            List<Universita> listaUniversitaLocale = universitaDao.getAll();
            if((listaUniversitaLocale==null)||(listaUniversitaLocale.isEmpty())){
                inserisciListaElementi(listaUniversita);
            }else if((listaUniversita==null)||(listaUniversita.isEmpty())) {
                eliminaListaElementi(listaUniversitaLocale);
            }else{
                HashMap<Long,Universita> hashMapUniversitaLocali = convertiListaInHashMap(listaUniversitaLocale);

                List<Universita> listaUniversitaNuove = new LinkedList<>();
                List<Universita> listaUniversitaAggiornate = new LinkedList<>();
                //Serve solo per poter conoscere quali elementi caccelare tra quelli già presenti localmente.
                List<Universita> listaVechieUniversitaDaMantenere = new LinkedList<>();

                Iterator<Universita> iterator = listaUniversita.iterator();
                while (iterator.hasNext()){
                    Universita universita =iterator.next();
                    if(universita!=null){
                        Universita universitaVecchia = hashMapUniversitaLocali.get(universita.getId());
                        if(universitaVecchia==null){
                            listaUniversitaNuove.add(universita);
                        }else if(!universitaVecchia.equals(universita)){
                            listaUniversitaAggiornate.add(universita);
                            listaVechieUniversitaDaMantenere.add(universitaVecchia);
                        }else{
                            listaVechieUniversitaDaMantenere.add(universitaVecchia);
                        }
                    }
                }
                //MANTENGO SOLO GLI ELEMNETI CHE DEVO CANCELLARE
                listaUniversitaLocale.removeAll(listaVechieUniversitaDaMantenere);
                inserisciListaElementi(listaUniversitaNuove);
                aggiornaListaElementi(listaUniversitaAggiornate);
                eliminaListaElementi(listaUniversitaLocale);
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         GENERAZIONE HASH MAP FROM LIST                                         */
/*================================================================================================*/
        private synchronized HashMap<Long,Universita> convertiListaInHashMap(List<Universita> lista){
            HashMap<Long,Universita> ris=new HashMap<>();
            if(lista!=null){
                Iterator<Universita> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Universita elemento = iterator.next();
                    if((elemento!=null) && (elemento.getId()!=null)){
                        ris.put(elemento.getId(),elemento);
                    }
                }
            }
            return ris;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO INSERIMENTO NUOVI ELEMENTI                                      */
/*================================================================================================*/
        private synchronized void inserisciListaElementi(List<Universita> lista){
            if(lista!=null){
                Iterator<Universita> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Universita elemento=iterator.next();
                    if(elemento!=null){
                        universitaDao.insert(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO AGGIORNAMENTO ELEMENTI PRESENTI                                 */
/*================================================================================================*/
        private synchronized void aggiornaListaElementi(List<Universita> lista){
            if(lista!=null){
                Iterator<Universita> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Universita elemento=iterator.next();
                    if(elemento!=null){
                        universitaDao.update(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaListaElementi(List<Universita> lista){
            if(lista!=null){
                Iterator<Universita> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Universita elemento=iterator.next();
                    if(elemento!=null){
                        universitaDao.delete(elemento.getId());
                    }
                }
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_Universita_Dao fbUniversitaDao = new FB_Universita_Dao();
    //QUERY FIREBASE
    private static LiveData<List<Universita>> fbAllUniversita=null;
    //ROOM DATABASE
    public static Universita_Dao universitaDao=null;
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaUniversita(){
        if(fbAllUniversita==null){
            fbAllUniversita = fbUniversitaDao.getAll_Synchronized();
        }
        if(universitaDao==null){
            universitaDao = db.UniversitaDao();
        }
        if(!fbAllUniversita.hasObservers()){//SE NON HA già osservatori per rilevare aggiornamenti
            fbAllUniversita.observeForever(new Observer<List<Universita>>() {
                @Override
                public void onChanged(List<Universita> lista) {
                    if((lista!=null)){
                        executor.execute(new ThreadAggiornamento(lista));
                    }
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Universita_ThreadAggiornamento(Application application) {
        super(application);
        gestioneAggiornametoTabellaUniversita();
    }
/*================================================================================================*/
/*################################################################################################*/
}
