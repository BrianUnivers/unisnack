package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.util.Iterator;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;

public class Deserializer_Ingredienti implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto Ingredienti.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String LOG_TAG = "Ingredienti_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "Ingredienti" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "Ingredienti".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public static Ingredienti estraiDaDataSnapshot(DataSnapshot dataSnapshot){
        Ingredienti ingredienti = null;
        if(dataSnapshot.exists()){
            try{
                //Campi not null
                Long   id          = Long.parseLong( dataSnapshot.getKey());
                String nome        = dataSnapshot.child(P_NOME).getValue(String.class);
                String descrizione = dataSnapshot.child(P_DESCRIZIONE).getValue(String.class);
                //Verifica campi not null
                if( (id!=null) && (nome!=null) && (descrizione==null)){
                    ingredienti = new Ingredienti(id,nome);
                }else if((id!=null) && (nome!=null) && (descrizione!=null)) {
                    ingredienti = new Allergeni(id,nome,descrizione);
                }
            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
                ingredienti = null;
            }
        }
        return ingredienti;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         GET AN ACTRIBUTE OBJECT FOR AN OTHER                                   */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET AN ACTRIBUTE                                                       */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce, se esiste, un singolo ogetto "Ingredienti" da un attributo di un altro
 * oggetto attraverso una sottoparte dell'oggetto "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "Ingredienti".
 * @param nomeAttributo Questo parametro contine il nome del attributo in cui cercare un siglo oggetto.
 * @return  Il riutato è il primo oggetto, valido se ce ne fossero più d'uno ritorna il primo trovato
 *          mentre se non ce ne sono ritorna null.
 */
    public static Ingredienti getAttributeOf(DataSnapshot dataSnapshot,String nomeAttributo){
        Ingredienti ingredienti = null;
        DataSnapshot ingredientiDataSnapshot = dataSnapshot.child(nomeAttributo);
        if(ingredientiDataSnapshot.exists()){
            Iterator<DataSnapshot> iterator =ingredientiDataSnapshot.getChildren().iterator();
            while ( (iterator.hasNext()) && (ingredienti==null) ){//Restituisce il primo oggetto valido
                ingredienti = estraiDaDataSnapshot(iterator.next());
            }
        }
        return ingredienti;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, Ingredienti>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Object extends GenericDeserializer_Object<Ingredienti> {
        @Override
        public Ingredienti estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Ingredienti.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<Ingredienti>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List<Ingredienti> {
        @Override
        public Ingredienti estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Ingredienti.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
