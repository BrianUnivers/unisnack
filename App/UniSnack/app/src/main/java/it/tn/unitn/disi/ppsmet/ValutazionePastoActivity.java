package it.tn.unitn.disi.ppsmet;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.ValutazionePastoActivity.ValutazionePastoViewModel;
import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.AppCompatActivityWithMyPrimaryMenu;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.GestioneMyIntent.MyIntent;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerVotoPrietanze.VotoPietanzeRVAdapter;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewValutazione.OnChangeValueListener.OnChangeValueListener;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewValutazione.TestoConValutazioneView;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.TitoloSopraContenutoView;

public class ValutazionePastoActivity extends AppCompatActivityWithMyPrimaryMenu {
/*################################################################################################*/
/*                                  IMPOSTA PARAMETRI DEI INTET PER MENU'LATERALE                 */
/*################################################################################################*/
    /**
     * Questo parametro viene chiamato nel momento in cui si preme un pulsante tra quelli del
     * menù laterale principale così da poter inserire i vari valori da passare tramite intent,
     * all'activity che segue. Conclusa questa procedura verrà attivato, l'intente per effetuare
     * il cabiamento d'activity, se chiamato dal menu laterare principale.
     * @param intent Questo parameto contiene l'intent pre impostata con l'activity d'arrivo.
     */
    @Override
    protected void setParamiter(MyIntent intent) {
        intent.putExtraIdUtente(MyIntent.I_UTENTE_ATTIVO,viewModel.getIdUtenteAttivo());
    }
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private ValutazionePastoActivity activity = this;
/*------------------------------------------------------------------------*/
    private ValutazionePastoViewModel viewModel;
/*------------------------------------------------------------------------*/
    private ViewGroup pagina;
    private NestedScrollView paginaOrdinazione;
/*------------------------------------------------------------------------*/
    private TitoloSopraContenutoView titoloMensa;
    private ImageView                imagineMensa;
    private RecyclerView             listaVotiPietanze;
/*------------------------------------------------------------------------*/
    private TestoConValutazioneView valutazioneTemperatura;
    private TestoConValutazioneView valutazioneCottura;
    private TestoConValutazioneView valutazioneSapore;
/*------------------------------------------------------------------------*/
    private LiveData<Ordinazioni>                                ldUltimaOrdinazioni;
    private LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> ldListaPietanza;
/*------------------------------------------------------------------------*/
    private Ordinazioni ultimaOrdinazioni = null;
    private List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze = null;
/*------------------------------------------------------------------------*/
    private Long idUtenteAttivo;
/*================================================================================================*/
/*                                  CREAZIONE DELL'ACTIVITY                                       */
/*================================================================================================*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valutazione_pasto);
/*------------------------------------------------------------------------*/
        //IMPOSTA MENU LETERALE
        super.initToolBar(R.menu.bar_menu_valutazione);
        super.impostaPaginaComeHome();
/*------------------------------------------------------------------------*/
        pagina            = findViewById(R.id.cly_PagineVotazionePasto);
        paginaOrdinazione = findViewById(R.id.nsv_ScrollPagina);
/*------------------------------------------------------------------------*/
        imagineMensa = findViewById(R.id.img_Mensa);
        int orientation = getResources().getConfiguration().orientation;
        if((orientation== Configuration.ORIENTATION_LANDSCAPE)){
            imagineMensa.getLayoutParams().height = (int) getResources().getDimension(R.dimen.dim_imagini_voto_piccole);
        }
/*------------------------------------------------------------------------*/
        titoloMensa = TitoloSopraContenutoView.newInstance(pagina,R.id.include_NomeMensa,R.string.lbl_Mensa);
        valutazioneTemperatura = TestoConValutazioneView.newInstance(paginaOrdinazione,R.id.include_VotoTemperatura,R.string.lbl_Temperatura);
        valutazioneCottura     = TestoConValutazioneView.newInstance(paginaOrdinazione,R.id.include_VotoCottura,R.string.lbl_Cottura);
        valutazioneSapore      = TestoConValutazioneView.newInstance(paginaOrdinazione,R.id.include_VotoSapore,R.string.lbl_Sapore);
/*------------------------------------------------------------------------*/
        listaVotiPietanze = findViewById(R.id.rcl_VotiSingolePietanze);
/*------------------------------------------------------------------------*/
        //GESTIONE VIEWMODEL E UTENTE ATTIVO
        viewModel =  new ViewModelProvider(this).get(ValutazionePastoViewModel.class);
        viewModel.setParameterFromIntet(getIntent());
/*------------------------------------------------------------------------*/
        idUtenteAttivo = viewModel.getIdUtenteAttivo();
        if(idUtenteAttivo==null){
            super.doLogout();
        }else {
/*------------------------------------------------------------------------*/
            ldUltimaOrdinazioni = viewModel.getLastOrdinazioni(idUtenteAttivo);
/*------------------------------------------------------------------------*/
            ldUltimaOrdinazioni.observe(activity, new Observer<Ordinazioni>() {
                @Override
                public void onChanged(Ordinazioni ordinazioni) {
                    if(ordinazioni!=null){
                        impostaDatiOrdinazione(ordinazioni);
                    }else{
                        nascondiDatiOrdinazioni();
                    }
                }
            });
/*------------------------------------------------------------------------*/
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  NASCONDI DATI ORDINAZIONE                                     */
/*================================================================================================*/
    /**
     * Questo metodo permette di nascodere la parte della pagina legata alle singole ordinazioni in
     * modo tale da mstrare un contenuto neutro.
     */
    private void nascondiDatiOrdinazioni(){
        titoloMensa.setText(R.string.lbl_Mensa);
        paginaOrdinazione.setVisibility(View.GONE);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  IMPOSTA DATI DELLE ORDINAZIONI                                */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA LA PAGINA CON L'ULTIMA ORDINAZIONE                    */
/*================================================================================================*/
    /**
     * Questo metodo permette di richiedere la lista delle pietanze ordinate associate al parmetro
     * "ordinazioni" passato, se esiste, altrimenti renderà la pagina senza alcuna ordinazione.
     * @param ordinazioni
     */
    private void impostaDatiOrdinazione(Ordinazioni ordinazioni){
        if((ordinazioni!=null) && (ordinazioni.getGiorno()!=null) && (ordinazioni.getId()!=null)){
            ldListaPietanza = viewModel.getPietanzeOrdinateByIdOfOrdinazioni(ordinazioni.getId());
            ldListaPietanza.observe(activity, new Observer<List<OrdinazionePietanzeDellaMenseConVoto>>() {
                        @Override
                        public void onChanged(List<OrdinazionePietanzeDellaMenseConVoto> list) {
                            if((list!=null) && (!list.isEmpty())){
                                mostraDatiOrdinazioniEPietanze(ordinazioni,list);
                            }else{
                                nascondiDatiOrdinazioni();
                            }
                        }
                    });
        }else{
            nascondiDatiOrdinazioni();
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA DATI ORDINAZIONE E PIETANZE NELLA PAGINA              */
/*================================================================================================*/
    /**
     * Questo metodo permette di mostrare la votazione del utente del ordinazione e delle sue pietanze
     * in modo tale che l'utente possa vedere la sua votazione precedente o, se nei limiti di tempo,
     * di sostituirla con un altra.
     * @param nuovaOrdinazioni
     * @param listaPietanze
     */
    private void mostraDatiOrdinazioniEPietanze(Ordinazioni nuovaOrdinazioni,List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze){
        this.ultimaOrdinazioni = nuovaOrdinazioni;
        this.listaPietanze     = new LinkedList<>();
        this.listaPietanze.addAll(listaPietanze);
        //Imposta mensa
        if((listaPietanze!=null) && (!listaPietanze.isEmpty())){
            Mense mense = listaPietanze.get(0).getMense();
            titoloMensa.setText(mense.getNome());
        }
        //Imposta limite per modificare
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MILLISECOND,999);
        calendar.set(Calendar.SECOND,59);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.add(Calendar.DATE,-2);
        Date ieri = calendar.getTime();
        boolean modificabile=false;
        if((ultimaOrdinazioni.getGiorno()!=null)&&(ieri.before(ultimaOrdinazioni.getGiorno()))){
            modificabile = true;
        }
/*------------------------------------------------------------------------*/
        Short votoTemperatura = ultimaOrdinazioni.getVotoTemperatura();
        votoTemperatura = ((votoTemperatura == null) ? 2 : votoTemperatura);
        valutazioneTemperatura.setModificablie(modificabile);
        valutazioneTemperatura.setVoto(votoTemperatura);
        valutazioneTemperatura.setOnChangeValueListener(new OnChangeValueListener() {
            @Override
            public void onChangeValueListener(View view, short value) {
                ultimaOrdinazioni.setVotoTemperatura(value);
            }
        });
/*------------------------------------------------------------------------*/
        Short votoCottura = ultimaOrdinazioni.getVotoCottura();
        votoCottura = ((votoCottura == null) ? 2 : votoCottura);
        valutazioneCottura.setModificablie(modificabile);
        valutazioneCottura.setVoto(votoCottura);
        valutazioneCottura.setOnChangeValueListener(new OnChangeValueListener() {
            @Override
            public void onChangeValueListener(View view, short value) {
                ultimaOrdinazioni.setVotoCottura(value);
            }
        });
/*------------------------------------------------------------------------*/
        Short votoSapore = ultimaOrdinazioni.getVotoSapore();
        votoSapore = ((votoSapore == null) ? 2 : votoSapore);
        valutazioneSapore.setModificablie(modificabile);
        valutazioneSapore.setVoto(votoSapore);
        valutazioneSapore.setOnChangeValueListener(new OnChangeValueListener() {
            @Override
            public void onChangeValueListener(View view, short value) {
                ultimaOrdinazioni.setVotoSapore(value);
            }
        });
/*------------------------------------------------------------------------*/
        listaVotiPietanze.setLayoutManager(new LinearLayoutManager(activity));
        VotoPietanzeRVAdapter adapter = new VotoPietanzeRVAdapter(activity,this.listaPietanze,modificabile);
        listaVotiPietanze.setAdapter(adapter);
/*------------------------------------------------------------------------*/
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE DEL MENU' SULLA TOOLBAR                              */
/*################################################################################################*/
/*================================================================================================*/
/*                                  GESTIONE DEI EVENTI DI CLICK SULLE VOCI DEL MENU'             */
/*================================================================================================*/
    /**
     * Metodo per distingere i tasti della toolbar e sapere cosa si deve fare.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.btn_bar_Coferma:
                salvaVotazioneAttuale();
        }
        return super.onOptionsItemSelected(item);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  SALVA VOTAZIONE                                               */
/*################################################################################################*/
/*================================================================================================*/
/*                                  SALVA VOTAZIONE ATTUALE                                       */
/*================================================================================================*/
    /**
     * Questo metodo permette di rendere definitive le votazioni attualmente visualizzate.
     */
    private void salvaVotazioneAttuale(){
        if((ultimaOrdinazioni!=null) && (listaPietanze!=null)){
            viewModel.updateOrdinazioneEPietanze(ultimaOrdinazioni,listaPietanze);
            Toast.makeText(activity, R.string.msg_VotazioneSalvata, Toast.LENGTH_LONG).show();
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}