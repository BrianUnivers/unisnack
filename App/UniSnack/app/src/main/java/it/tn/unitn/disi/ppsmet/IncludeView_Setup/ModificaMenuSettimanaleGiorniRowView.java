package it.tn.unitn.disi.ppsmet.IncludeView_Setup;

import android.graphics.Typeface;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface.OnFragmentSelezioneGiornoETipoPastoEventListener;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerModificaMenuSettimanale.RecyclerMMSGiorni.ModificaMenuSettimanaleTipoPastoRVAdapter;
import it.tn.unitn.disi.ppsmet.R;

public class ModificaMenuSettimanaleGiorniRowView {
/*################################################################################################*/
    private ViewGroup    contenitore;
    private ViewGroup    contenitoreCalendario;
    private TextView     numeroGiorno;
    private TextView     abbreviazioneMese;
    private TextView     giornoDellaSettimana;
    private RecyclerView listaPulsantiTipiMenu;
/*------------------------------------------------------------------------------------------------*/
    private Date dataOrdinazione = null;
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    private ModificaMenuSettimanaleGiorniRowView(ViewGroup contenitore,
                                                ViewGroup contenitoreCalendario,
                                                TextView  numeroGiorno,
                                                TextView  abbreviazioneMese,
                                                TextView  giornoDellaSettimana,
                                                RecyclerView listaPulsantiTipiMenu) {
        this.contenitore          = contenitore;
        this.contenitoreCalendario= contenitoreCalendario;
        this.numeroGiorno         = numeroGiorno;
        this.abbreviazioneMese    = abbreviazioneMese;
        this.giornoDellaSettimana = giornoDellaSettimana;
        this.listaPulsantiTipiMenu= listaPulsantiTipiMenu;
    }
/*------------------------------------------------------------------------------------------------*/
    public static ModificaMenuSettimanaleGiorniRowView newInstance(ViewGroup rootLayout, int idLayoutIncluded) {
        ModificaMenuSettimanaleGiorniRowView ris = null;
        if((rootLayout!=null)){
            ViewGroup barraContenitore = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(barraContenitore!=null){
                ViewGroup contenitoreCalendario    = barraContenitore.findViewById(R.id.lly_Calendario);
                TextView  numeroGiorno             = barraContenitore.findViewById(R.id.txt_NumeroGiorno);
                TextView  abbreviazioneMese        = barraContenitore.findViewById(R.id.txt_AbbreviazioneMese);
                TextView  giornoDellaSettimana     = barraContenitore.findViewById(R.id.txt_GiornoSettimana);
                RecyclerView listaPulsantiTipiMenu = barraContenitore.findViewById(R.id.rcl_ListaPulsantiTipiMenu);
                if((numeroGiorno!=null) && (abbreviazioneMese!=null) &&
                        (giornoDellaSettimana!=null) && (contenitoreCalendario!=null) &&
                        (listaPulsantiTipiMenu!=null)){
                    ris = new ModificaMenuSettimanaleGiorniRowView( barraContenitore,
                                                                    contenitoreCalendario,
                                                                    numeroGiorno,
                                                                    abbreviazioneMese,
                                                                    giornoDellaSettimana,
                                                                    listaPulsantiTipiMenu);
                }
            }
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  IMPOSTAZIONI DI LAYOU                                         */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA E CAMBIA DATA                                         */
/*================================================================================================*/
    public void setData(Date data){
        this.dataOrdinazione = data;
        if(data!=null){
            String format2 = "dd-MMM-yy";
            SimpleDateFormat sdf = new SimpleDateFormat(format2, Locale.ITALIAN);
            String dataStinga = sdf.format(data);
            String[] infoDati = dataStinga.split("-");
            if((infoDati!=null) && (infoDati.length==3)){
                this.numeroGiorno.setText(infoDati[0]);
                this.abbreviazioneMese.setText(infoDati[1]);
                this.giornoDellaSettimana.setText(new SimpleDateFormat("EEEE", Locale.ITALY).format(data.getTime()));
            }
        }else{
            this.numeroGiorno.setText("");
            this.abbreviazioneMese.setText("");
            this.giornoDellaSettimana.setText("");
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA TIPO GIORNO ATTIVO O DISATTIVO                        */
/*================================================================================================*/
    public void setStatoAttivo(boolean b){
        if(!b){
            //Disattivo
            this.giornoDellaSettimana.setTextColor(contenitore.getResources().getColor(R.color.colorNormalText,null));
            this.numeroGiorno.setTypeface(null, Typeface.NORMAL);
            this.abbreviazioneMese.setTypeface(null, Typeface.NORMAL);
            this.giornoDellaSettimana.setTypeface(null, Typeface.NORMAL);
            this.contenitoreCalendario.setBackgroundTintList(contenitore.getResources().getColorStateList(R.color.colorSecondaryLight,null));
        }else{
            //Attivo
            this.giornoDellaSettimana.setTextColor(contenitore.getResources().getColor(R.color.colorPrimary,null));
            this.numeroGiorno.setTypeface(null, Typeface.BOLD);
            this.abbreviazioneMese.setTypeface(null, Typeface.BOLD);
            this.giornoDellaSettimana.setTypeface(null, Typeface.BOLD);
            this.contenitoreCalendario.setBackgroundTintList(contenitore.getResources().getColorStateList(R.color.colorPrimaryLight,null));
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  IMPOSTAZIONI DI LAYOU                                         */
/*################################################################################################*/
    public void setListTipoPasto(List<TipoPasto> listTipoPAsto,
                                 HashMap<Long, Ordinazioni> ordinazioniNelGiorno,
                                 OnFragmentSelezioneGiornoETipoPastoEventListener listener){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(listaPulsantiTipiMenu.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        this.listaPulsantiTipiMenu.setLayoutManager(linearLayoutManager);
        this.listaPulsantiTipiMenu.setAdapter(
                new ModificaMenuSettimanaleTipoPastoRVAdapter(  listaPulsantiTipiMenu.getContext(),
                                                                listTipoPAsto,
                                                                this.dataOrdinazione,
                                                                ordinazioniNelGiorno,
                                                                listener));
    }
/*################################################################################################*/
}
