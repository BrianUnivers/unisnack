package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListSpinnerWithAdd.AdapterWithSpinner.Interface;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.ViewInputSpinner_Text;

public abstract class ListaSpinnerRVAdapter<M extends RecyclerView.ViewHolder,T> extends RecyclerView.Adapter<M>  {
/*################################################################################################*/
    protected List<Integer>    listaIdSelezionatori = new ArrayList<>();
    protected HashMap<Integer,ViewInputSpinner_Text<T>> selezionatori = new HashMap<>();
    protected Integer         numProssimoElemetoAggiunto = 1;
    protected ArrayAdapter<T> adapter;
/*------------------------------------------------------------------------------------------------*/
    private List<Button> listaButtonAggiungi = new LinkedList<>();
/*------------------------------------------------------------------------------------------------*/
    protected View.OnClickListener onChangeListener = null;
    protected View viewConteiner=null;
/*================================================================================================*/
/*                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE                  */
/*================================================================================================*/
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaIdSelezionatori.size();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                              INSERISCI UN NUOVO ELEMENTO DA MOSTRARE                           */
/*================================================================================================*/
    /**
     * Questo metodo aggiunge un nuovo spinner se e solo se il numero di elementi selezionabili è
     * superiore al numero di spinnere attualmente presenti. In qualuque caso viene chiamato il metodo
     * "aggiornaSatoBottoni" così da rendere vi sibile il pulsante aggiungi(Se non lo era già), o
     * lo fa somparire se si è raggiunto il limite di spinner.
     * In oltre se viene aggiunto il nuovo elemento alla lista ed è stato impostato un "OnChangeLisserner"
     * questo metodo lo attiva.
     */
    public void inserisciNuovoElemento(){
        if(this.getItemCount() < this.adapter.getCount()){
            int id = numProssimoElemetoAggiunto;
            numProssimoElemetoAggiunto++;
            listaIdSelezionatori.add(id);
            this.notifyItemInserted(listaIdSelezionatori.indexOf(id));
            //Attiva Eventuali lissenere sul cambiamento
            activeOnChangeLisserner();
        }
        aggiornaSatoBottoni();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                              ELIMINA ELEMENTO CHE SI STA' MOSTRANDO                            */
/*================================================================================================*/
    /**
     * Questo metodo toglie uno spinner spicifico dalla lista. Questo metodo viene chiamato quando si
     * preme il pulsante cancella affianco allo spinner. Oppure per togliere eventuali spinner di
     * troppo rispetto al numero di dati.
     * In ogni caso queso metodo chiama la procedura  "aggiornaSatoBottoni" così da rendere vi sibile
     * il pulsante aggiungi(Se non lo era già), o lo fa somparire se si è raggiunto il limite di spinner.
     * In oltre se viene aggiunto il nuovo elemento alla lista ed è stato impostato un "OnChangeLisserner"
     * questo metodo lo attiva.
     * @param idElemento
     */
    public void eliminaElemento(Integer idElemento){
        int posizione =  listaIdSelezionatori.indexOf(idElemento);
        listaIdSelezionatori.remove((Object)idElemento);
        selezionatori.remove(idElemento);
        this.notifyItemRemoved(posizione);
        aggiornaSatoBottoni();
        //Attiva Eventuali lissenere sul cambiamento
        activeOnChangeLisserner();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                              NOTIFY DATA SET CHANGED OF SPINNER                                */
/*================================================================================================*/
    /**
     * Questo metodo serve per notificare al adaprer,utilizzato da tutti gli spinnere che è stata cambiata
     * la lista dei valori possibili(l'aggiornamento è generale, implica la ricrazione dei item).
     * Se in oltre, con questo cambiamento rimangono meno dati per gli spinner,rispetto al loro
     * numero, vengono tolti(in modo albitrario) tutti gli ultimi spinner fino a raggiungere il
     * numero di spinner uguale a quello dei elementi.
     * In ogni caso queso metodo chiama la procedura  "aggiornaSatoBottoni" così da rendere vi sibile
     * il pulsante aggiungi(Se non lo era già), o lo fa somparire se si è raggiunto il limite di spinner.
     */
    public void notifyDataSetChangedSpinner(){
        this.adapter.notifyDataSetChanged();
        int dif = this.getItemCount()- this.adapter.getCount();
        //Se il numero di seletori è maggiore dei elementi selezionabli si deve togliere dei elementi.
        if(dif>0){
            for(int i=0; i<dif;i++){
                //Togli elemneti da hashmap
                int posizione = listaIdSelezionatori.size()-1;
                selezionatori.remove(listaIdSelezionatori.get(posizione));
                listaIdSelezionatori.remove(posizione);
                this.notifyItemRemoved(posizione);
            }
        }
        aggiornaSatoBottoni();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                              AGGIORNA STATO BOTTONI                                            */
/*================================================================================================*/
    /**
     * Questo è il metododo generico che pota ad aggiornare, in modo querente con i dati e spinner della
     * streuttura, la presenza o l'assenza, dei pulsanti che si sono aggiunti, per l'insrimento
     * di nuovi spinner.
     */
    protected void aggiornaSatoBottoni(){
        if(this.getItemCount() < this.adapter.getCount()){
            attivaBottoni();
        }else{
            disattivaBottoni();
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                              ATTIVA BOTTONI                                                    */
/*================================================================================================*/
    /**
     * Questo mettodo rende visibili tutti i pusanti legati alla possibile aggiunta di un nuovo spinner.
     */
    private void attivaBottoni(){
        Iterator<Button> iterator = listaButtonAggiungi.iterator();
        while (iterator.hasNext()){
            Button button=iterator.next();
            button.setVisibility(View.VISIBLE);
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                              DISSATTIVA BOTTONI                                                */
/*================================================================================================*/
    /**
     * Questo mettodo rende invisibili tutti i pusanti legati alla possibile aggiunta di un nuovo spinner.
     */
    private void disattivaBottoni(){
        Iterator<Button> iterator = listaButtonAggiungi.iterator();
        while (iterator.hasNext()){
            Button button=iterator.next();
            button.setVisibility(View.GONE);
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                              AGGIUNGI BOTTONE DA ATTIVARE E DISATTIVARE                        */
/*================================================================================================*/
    /**
     * Questo aggiunge un pulsante che deve essere visibile solo quando è possibile aggiungere un nuovo
     * spinner.
     * In oltre metodo chiama la procedura  "aggiornaSatoBottoni" così da rendere vi sibile
     *  il pulsante aggiungi(Se non lo era già), o lo fa somparire se si è raggiunto il limite di spinner.
     */
    public void aggiungiBottoneDaAttivareOsiattivare(Button button){
        if(button!=null){
            listaButtonAggiungi.add(button);
            aggiornaSatoBottoni();
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                              IMPOSTA TUTTI GLI SPINNER CON L'AVVISO                            */
/*================================================================================================*/
    /**
     * Questo metodo permette di impostrare un avviso a tutti gli spinnere presenti nell'adapter.
     */
    public void impostaAvviso(){
        Iterator<Integer> iterator=listaIdSelezionatori.iterator();
        while (iterator.hasNext()){
            Integer id = iterator.next();
            ViewInputSpinner_Text elemento = selezionatori.get(id);
            if(elemento!=null){
                elemento.impostaAvviso();
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                              IMPOSTA TUTTI GLI SPINNER CON L'AVVISO                            */
/*================================================================================================*/
    /**
     * Questo metodo permette di restituire con un sigola richiesta tutti gli elementi selezioni
     * in qul momento. Si noti che se ci sono dei elementi duplicati, questo metodo non li toglie.
     * @return
     */
    public  List<T> getElemetiSelezionati(){
        List<T> listaElementi = new LinkedList<>();
        Iterator<Integer> iterator=listaIdSelezionatori.iterator();
        while (iterator.hasNext()){
            Integer id = iterator.next();
            ViewInputSpinner_Text<T> elemento = selezionatori.get(id);
            listaElementi.add(elemento.getItemSelected());
        }
        return listaElementi;
    }
/*================================================================================================*/
/*################################################################################################*/

/*################################################################################################*/
/*================================================================================================*/
/*                                  ADD LISSERENR ON CHANGE                                       */
/*================================================================================================*/
    /**
     * Questo metodo definisce un lisenere che viene attivato se viene aggiunto o tolto uno uno
     * spinnere dalla lista ed se gli spinnere lo implementano anche la selezione di un valore di uno
     * qualuque delgi spinner.
     * @param listener
     * @param view
     */
    public void addChangeListener(View.OnClickListener listener,View view){
        this.viewConteiner = view;
        this.onChangeListener = listener;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  START LISSENNER                                               */
/*================================================================================================*/
    /**
     * Questo metodo è quello che attiva il OnChangeLisserner.
     */
    private void activeOnChangeLisserner(){
        if(onChangeListener!=null){
            onChangeListener.onClick(viewConteiner);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
