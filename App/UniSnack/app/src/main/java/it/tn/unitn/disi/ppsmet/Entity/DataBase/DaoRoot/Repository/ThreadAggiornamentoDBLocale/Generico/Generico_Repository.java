package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;

public class Generico_Repository {
/*################################################################################################*/
/*================================================================================================*/
/*                         GESTIONE TREAD CONTEMPORANEI PER L'AGGIONAMETO DELLE TABELLE DEL       */
/*                         DATABASE LOCALE (room)                                                 */
/*================================================================================================*/
    private final static int NUM_MAX_THREAD  = 10;
    protected final static ExecutorService executor  = Executors.newFixedThreadPool(NUM_MAX_THREAD);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    protected static MyRoomDatabase db=null;
/*------------------------------------------------------------------------------------------------*/
    public Generico_Repository(Application application) {
        if(db==null){
            db = MyRoomDatabase.getDatabase(application);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                         MEDODO PER RICHEDERE UN IMMAGINE DA UN URL A BITMAP                    */
/*################################################################################################*/
/*================================================================================================*/
/*                         GET IMAGE FROM URL                                                     */
/*================================================================================================*/


/*================================================================================================*/
/*################################################################################################*/
}
