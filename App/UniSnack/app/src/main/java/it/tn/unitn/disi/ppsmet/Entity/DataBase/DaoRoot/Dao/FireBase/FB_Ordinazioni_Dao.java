package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import androidx.lifecycle.LiveData;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.O_relativa_F;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;

public abstract class FB_Ordinazioni_Dao extends ConnesioneAFirebase<Ordinazioni> {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
  /*  @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    public long insert(Ordinazioni ordinazioni, List<PietanzeDellaMense> listaPietanzeDellaMense ){
        List<OrdinazionePietanzeDellaMenseConVoto> listaPietanzeDellaMenseConVoto =new LinkedList<>();
        Iterator<PietanzeDellaMense> iterator = listaPietanzeDellaMense.iterator();
        while(iterator.hasNext()){
            listaPietanzeDellaMenseConVoto.add(new OrdinazionePietanzeDellaMenseConVoto(iterator.next(),null));
        }
        return this.insertConVoto(ordinazioni,listaPietanzeDellaMenseConVoto);
    }
/*------------------------------------------------------------------------*/
  /*  @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    public long insertConVoto(Ordinazioni ordinazioni, List<OrdinazionePietanzeDellaMenseConVoto> listaPietanzeDellaMenseConVoto ){
        long id = this.insert(ordinazioni);
        Iterator<OrdinazionePietanzeDellaMenseConVoto> iterator = listaPietanzeDellaMenseConVoto.iterator();
        while(iterator.hasNext()){
            OrdinazionePietanzeDellaMenseConVoto elemet =  iterator.next();
            O_relativa_F relazione =
                    new O_relativa_F( id,
                            elemet.getMense().getId(),
                            elemet.getPietanze().getId(),
                            elemet.getMese(),
                            elemet.getAnno(),
                            elemet.getGiornoSettimana(),
                            elemet.getTipoMenu().getId(),
                            elemet.getVotoPietanza() );
            this.insert(relazione);
        }
        return id;
    }
/*------------------------------------------------------------------------------------------------*/
 /*   @Insert(onConflict = OnConflictStrategy.IGNORE)
    protected abstract long insert(Ordinazioni ordinazioni);
/*------------------------------------------------------------------------*/
/*    @Insert(onConflict = OnConflictStrategy.IGNORE)
    protected abstract long insert(O_relativa_F corsi);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
  /*  @Transaction
    public void deleteAll(){
        this.deleteAllRelativa();
        this.deleteAllOrdinazioni();
    }
/*------------------------------------------------------------------------------------------------*/
  /* @Query("DELETE FROM tableOrdinazioni")
    protected abstract void deleteAllOrdinazioni();
/*------------------------------------------------------------------------*/
  /*  @Query("DELETE FROM tableO_relativa_F")
    protected abstract void deleteAllRelativa();
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
  /*  @Update(entity = Ordinazioni.class,
            onConflict = OnConflictStrategy.ABORT)
    public abstract void update(Ordinazioni ordinazioni);
/*------------------------------------------------------------------------*/
 /*   @Transaction
    @Update(onConflict = OnConflictStrategy.ABORT)
    public void update(Ordinazioni ordinazioni, List<OrdinazionePietanzeDellaMenseConVoto> listaPietanzeDellaMenseConVoto ){
        this.update(ordinazioni);
        Iterator<OrdinazionePietanzeDellaMenseConVoto> iterator = listaPietanzeDellaMenseConVoto.iterator();
        while(iterator.hasNext()){
            OrdinazionePietanzeDellaMenseConVoto elemet = iterator.next();
            O_relativa_F relazione =
                    new O_relativa_F(
                            ordinazioni.getId(),
                            elemet.getMense().getId(),
                            elemet.getPietanze().getId(),
                            elemet.getMese(),
                            elemet.getAnno(),
                            elemet.getGiornoSettimana(),
                            elemet.getTipoMenu().getId(),
                            elemet.getVotoPietanza() );
            this.update(relazione);
        }
    }
/*------------------------------------------------------------------------------------------------*/
/*    @Update(entity = O_relativa_F.class,
            onConflict = OnConflictStrategy.ABORT)
    protected abstract void update(O_relativa_F relativa);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
/*Todo  @Query("SELECT O.* FROM tableOrdinazioni AS O")
    public abstract List<Ordinazioni> getAll();
/*------------------------------------------------------------------------*/
/*Todo  @Query("SELECT O.* FROM tableOrdinazioni AS O")
    public abstract LiveData<List<Ordinazioni>> getAll_Synchronized();
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
/*Todo    @Query("SELECT O.* FROM tableOrdinazioni AS O WHERE id == :id")
    public abstract Ordinazioni getByPrimaryKey(Long id);
/*------------------------------------------------------------------------*/
/*Todo   @Query("SELECT O.* FROM tableOrdinazioni AS O WHERE id == :id")
    public abstract LiveData<Ordinazioni> getByPrimaryKey_Synchronized(Long id);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
/*Todo  @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE O.id_utente == :idUntente")
    public abstract List<Ordinazioni> getByIdOfUtenti(Long idUntente);
/*------------------------------------------------------------------------*/
  /*Todo  @Query( "SELECT O.* " +
        "FROM tableOrdinazioni AS O " +
        "WHERE O.id_utente == :idUntente")
    public abstract LiveData<List<Ordinazioni>> getByIdOfUtenti_Synchronized(Long idUntente);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE LAST EAT IN THIS DATA                          */
/*================================================================================================*/
 /*Todo   @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE   O.id_utente == :idUntente AND " +
                    "O.giorno <= :date AND " +
                    "O.giorno =  (" +
                            "SELECT MAX(O1.giorno) " +
                            "FROM tableOrdinazioni AS O1 " +
                            "WHERE   O1.id_utente == :idUntente AND  O1.giorno <= :date )")
    public abstract List<Ordinazioni> getLastEatBeforeDataByIdOfUtenti(Long idUntente, Date date);
/*------------------------------------------------------------------------*/
 /*Todo   @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE   O.id_utente == :idUntente AND " +
                    "O.giorno <= :date AND " +
                    "O.giorno =  (" +
                        "SELECT MAX(O1.giorno) " +
                        "FROM tableOrdinazioni AS O1 " +
                        "WHERE   O1.id_utente == :idUntente AND  O1.giorno <= :date )")
    public abstract LiveData<List<Ordinazioni>> getLastEatBeforeDataByIdOfUtenti_Synchronized(Long idUntente, Date date);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE LAST EAT IN THIS DATA                          */
/*================================================================================================*/
 /*Todo   @Transaction
    public List<Ordinazioni> getLastEatBeforeTodayByIdOfUtenti(Long idUntente){
        return this.getLastEatBeforeDataByIdOfUtenti(idUntente,new Date());
    }
/*------------------------------------------------------------------------*/
  /*Todo  public LiveData<List<Ordinazioni>> getLastEatBeforeTodayByIdOfUtenti_Synchronized(Long idUntente){
        return this.getLastEatBeforeDataByIdOfUtenti_Synchronized(idUntente,new Date());
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND TIPO PASTO                                 */
/*================================================================================================*/
  /*Todo  @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE O.id_utente == :idUntente AND O.id_tipoPasto == :tipoPasto")
    public abstract List<Ordinazioni> getByIdOfUtenti_TipoPasto(Long idUntente, Long tipoPasto);
/*------------------------------------------------------------------------*/
 /*Todo   @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE O.id_utente == :idUntente AND O.id_tipoPasto == :tipoPasto")
    public abstract LiveData<List<Ordinazioni>> getByIdOfUtenti_TipoPasto_Synchronized(Long idUntente, Long tipoPasto);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND TIPO PASTO  LAST EAT IN THIS DATA          */
/*================================================================================================*/
/*Todo  @Transaction
    public List<Ordinazioni> getLastEatBeforeTodayByIdOfUtenti_TipoPasto(Long idUntente, Long tipoPasto){
        return this.getLastEatBeforeDataByIdOfUtenti_TipoPasto(idUntente,tipoPasto,new Date());
    }
/*------------------------------------------------------------------------*/
  /*Todo  public LiveData<List<Ordinazioni>> getLastEatBeforeTodayByIdOfUtenti_TipoPasto_Synchronized(Long idUntente, Long tipoPasto){
        return this.getLastEatBeforeDataByIdOfUtenti_TipoPasto_Synchronized(idUntente,tipoPasto,new Date());
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND TIPO PASTO  LAST EAT IN THIS DATA          */
/*================================================================================================*/
  /*Todo  @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE   O.id_utente == :idUntente AND " +
                    "O.giorno <= :date AND   " +
                    "O.id_tipoPasto == :tipoPasto AND " +
                    "O.giorno =  (" +
                            "SELECT MAX(O1.giorno) " +
                            "FROM tableOrdinazioni AS O1 " +
                            "WHERE   O1.id_utente == :idUntente  AND " +
                                    "O.id_tipoPasto == :tipoPasto AND  " +
                                    "O1.giorno <= :date )")
    public abstract List<Ordinazioni> getLastEatBeforeDataByIdOfUtenti_TipoPasto(Long idUntente, Long tipoPasto, Date date);
/*------------------------------------------------------------------------*/
  /*Todo  @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE   O.id_utente == :idUntente AND " +
                    "O.giorno <= :date AND   " +
                    "O.id_tipoPasto == :tipoPasto AND " +
                    "O.giorno =  (" +
                            "SELECT MAX(O1.giorno) " +
                            "FROM tableOrdinazioni AS O1 " +
                            "WHERE   O1.id_utente == :idUntente  AND " +
                                    "O.id_tipoPasto == :tipoPasto AND  " +
                                    "O1.giorno <= :date )")
    public abstract LiveData<List<Ordinazioni>> getLastEatBeforeDataByIdOfUtenti_TipoPasto_Synchronized(Long idUntente, Long tipoPasto, Date date);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND TIPO PASTO  LAST EAT IN THIS DATA          */
/*================================================================================================*/
  /*Todo  @Query( "SELECT  F.*, R.votoPietanza , " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine," +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F, tableO_relativa_F AS R " +
            "WHERE P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_tipoMenu AND " +
                  "R.id_f_anno == F.anno AND R.id_f_mese == F.mese AND R.id_f_mensa == F.id_mensa AND R.id_f_pietanza == F.id_pietanza AND R.id_f_tipoMenu == F.id_tipoMenu AND " +
                  "R.id_ordinazione == :idOrdinazione ")
    public abstract List<OrdinazionePietanzeDellaMenseConVoto> getPietanzeOrdinateByIdOfOrdinazioni(Long idOrdinazione);
/*------------------------------------------------------------------------*/
  /*Todo  @Query( "SELECT  F.*, R.votoPietanza , " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine," +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F, tableO_relativa_F AS R " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_tipoMenu AND " +
                    "R.id_f_anno == F.anno AND R.id_f_mese == F.mese AND R.id_f_mensa == F.id_mensa AND R.id_f_pietanza == F.id_pietanza AND R.id_f_tipoMenu == F.id_tipoMenu AND " +
                    "R.id_ordinazione == :idOrdinazione ")
    public abstract LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> getPietanzeOrdinateByIdOfOrdinazioni_Synchronized(Long idOrdinazione);
/*================================================================================================*/
/*################################################################################################*/
}
