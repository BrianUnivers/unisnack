package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Corsi_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Corsi_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Ordinazioni_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Universita_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.Uni_ha_C;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

public class Ordinazioni_ThreadAggiornamento extends Generico_Repository {
    //DIPENDENZE
    protected static TipoPasto_ThreadAggiornamento             dipendezaTipoPasto            = null;
    protected static Utenti_ThreadAggiornamento                dipendezaUtenti               = null;
    protected static PietanzeForniteMensa_ThreadAggiornamento  dipendezaPietanzeForniteMensa = null;
    //ROOM DATABASE
    protected static Ordinazioni_Dao ordinazioniDao = null;
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaOrdinazioni(){
        if(ordinazioniDao==null){
            ordinazioniDao = db.OrdinazioniDao();
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA ORDINAZIONE(e le sue derivazioni        */
/*                         seguendo un ordine topologico per garantire gli aggiornamenti)         */
/*                          ATTRAVERO IL DATABASE LOCALE (room)                                   */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Ordinazioni_ThreadAggiornamento(Application application) {
        super(application);
        if(dipendezaTipoPasto==null){
            dipendezaTipoPasto = new TipoPasto_ThreadAggiornamento(application);
        }
        if(dipendezaUtenti==null){
            dipendezaUtenti = new Utenti_ThreadAggiornamento(application);
        }
        if(dipendezaPietanzeForniteMensa==null){
            dipendezaPietanzeForniteMensa = new PietanzeForniteMensa_ThreadAggiornamento(application);
        }
        gestioneAggiornametoTabellaOrdinazioni();
    }
/*================================================================================================*/
/*################################################################################################*/
}
