package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common;


import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.net.InetAddress;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_MaxNumberKey;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public abstract class ConnesioneAFirebase<T> implements CostantiPerJSON {
/*################################################################################################*/
    private static FirebaseDatabase DATABASE = null;
/*================================================================================================*/
/*                         GET REFEREBCE DATABASE FIREBASE                                        */
/*================================================================================================*/
    public static FirebaseDatabase getReferenceFireBase(){
        if(DATABASE==null){
            DATABASE = FirebaseDatabase.getInstance();
        }
        return DATABASE;
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                         GET LAST ID OF TABLE                                                   */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET LAST ID OF ORDINAZIONI                                             */
/*------------------------------------------------------------------------------------------------*/
    public static LiveData<Long>getMaxIdOrdinazioni() {
        DatabaseReference ref =  getReferenceFireBase().getReference().child(L_ORDINAZIONI);
        FirebaseQueryLiveData_MaxNumberKey ris = new FirebaseQueryLiveData_MaxNumberKey(ref);
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET LAST ID OF PASTO DELMESE                                           */
/*------------------------------------------------------------------------------------------------*/
/*    public static LiveData<Long> getMaxIdPastoDelMese() {
        DatabaseReference ref =  getReferenceFireBase().getReference().child(L_PASTO_DEL_MESE);
        FirebaseQueryLiveData_MaxNumberKey ris = new FirebaseQueryLiveData_MaxNumberKey(ref);
        return ris;
    }*/
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET LAST ID OF PIETANZE                                                */
/*------------------------------------------------------------------------------------------------*/
    public static LiveData<Long> getMaxIdPietanze() {
        DatabaseReference ref =  getReferenceFireBase().getReference().child(L_PIETANZE);
        FirebaseQueryLiveData_MaxNumberKey ris = new FirebaseQueryLiveData_MaxNumberKey(ref);
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET LAST ID OF UTENTI                                                  */
/*------------------------------------------------------------------------------------------------*/
    public static LiveData<Long> getMaxIdUtenti() {
        DatabaseReference ref =  getReferenceFireBase().getReference().child(L_UTENTI);
        FirebaseQueryLiveData_MaxNumberKey ris = new FirebaseQueryLiveData_MaxNumberKey(ref);
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                         MERGE LIVE DATA                                                        */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         MERGE TWO LIVE DATA                                                    */
/*------------------------------------------------------------------------------------------------*/
    public LiveData<List<T>> mergeLiveData(LiveData<List<T>> liveData1,LiveData<List<T>>liveData2){
        List<LiveData<List<T>>> liveDataList = new LinkedList<>();
        liveDataList.add(liveData1);
        liveDataList.add(liveData2);
        return mergeListOfLiveData(liveDataList);
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         MERGE LIST LIVE DATA                                                   */
/*------------------------------------------------------------------------------------------------*/
    public LiveData<List<T>> mergeListOfLiveData(List<LiveData<List<T>>> liveDataList){
        MediatorLiveData<List<T>> ris=new MediatorLiveData<>();
        Iterator<LiveData<List<T>>> iterator = liveDataList.iterator();

        while (iterator.hasNext()){
            LiveData<List<T>> element= iterator.next();
            ris.addSource(element, new Observer<List<T>>() {
                private List<T> lastValueInsert = new LinkedList<>();
                @Override
                public void onChanged(List<T> ts) {
                    List<T> oldValue = ris.getValue();
                    if((oldValue!=null)&& (!oldValue.isEmpty()) ){
                        if( (!lastValueInsert.isEmpty()) ){
                            oldValue.removeAll(lastValueInsert);
                        }
                    }
                    lastValueInsert.clear();
                    lastValueInsert.addAll(ts);
                    if((oldValue!=null) && (!oldValue.isEmpty()) ){
                        ts.addAll(oldValue);
                    }
                    ris.postValue(ts);
                }
            });
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                         BUILDER KEY FROM DATE FOR FIREBASE DATABASE                            */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         BUILDER KEY FROM UTENTI                                                */
/*------------------------------------------------------------------------------------------------*/
    protected static String builderKeyFromUtenti(@NonNull Utenti utenti){
        StringBuilder key =new StringBuilder();
        key.append(utenti.getId_Universita())
                .append(S_SEPARARORE_PER_CHIAVI)
                .append(utenti.getId_Corso())
                .append(S_SEPARARORE_PER_CHIAVI)
                .append(utenti.getId());
        return key.toString();
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         BUILDER KEY FROM FORNISCE                                              */
/*------------------------------------------------------------------------------------------------*/
    protected static String builderKeyFromFornisce(@NonNull Long idMensa,@NonNull Long idTipoMenu,
                                                   @NonNull Integer anno,@NonNull Integer mese,@NonNull Integer giornoSettimana){
        StringBuilder key =new StringBuilder();
        key.append(idMensa)
                .append(S_SEPARARORE_PER_CHIAVI)
                .append(idTipoMenu)
                .append(S_SEPARARORE_PER_CHIAVI)
                .append(anno).append(S_SEPARARORE_PER_PARAMETRI)
                .append(mese).append(S_SEPARARORE_PER_PARAMETRI)
                .append(giornoSettimana);
        return key.toString();
    }
/*-------------------------*/
    protected static String builderKeyFromFornisce(@NonNull Mense mense, @NonNull TipoMenu tipoMenu,
                                                   @NonNull Integer anno, @NonNull Integer mese, @NonNull Integer giornoSettimana){
        return builderKeyFromFornisce(mense.getId(),tipoMenu.getId(),anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         BUILDER KEY FROM ANNO MESE                                             */
/*------------------------------------------------------------------------------------------------*/
    protected static String builderKeyFromUtentiAnnoMese(@NonNull Utenti utenti,@NonNull Short anno,@NonNull Short mese){
        StringBuilder key =new StringBuilder();
        key.append(builderKeyFromUtenti(utenti))
                .append(S_SEPARARORE_PER_CHIAVI)
                .append(builderKeyFromAnnoMese(anno,mese));
        return key.toString();
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         BUILDER KEY FROM UTENTI ANNO MESE                                      */
/*------------------------------------------------------------------------------------------------*/
    protected static String builderKeyFromAnnoMese(@NonNull Short anno,@NonNull Short mese){
        StringBuilder key =new StringBuilder();
        key.append(anno)
                .append(mese).append(S_SEPARARORE_PER_PARAMETRI)
                .append(builderKeyFromAnnoMese(anno,mese));
        return key.toString();
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                         BUILDER AUTHENTICATOR KEY                                              */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         BUILDER AUTHENTICATOR KEY                                               */
/*------------------------------------------------------------------------------------------------*/
    protected static String builderAuthenticatorKey(@NonNull String email,@NonNull String hashedPSW){
        StringBuilder key =new StringBuilder();
        key.append(email)
                .append(S_SEPARARORE_PER_PARAMETRI)
                .append(hashedPSW);
        return key.toString();
    }
/*================================================================================================*/
/*################################################################################################*/
}
