package it.tn.unitn.disi.ppsmet;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import it.tn.unitn.disi.ppsmet.ActivityManager.ProfiloUtenteActivity.ProfiloUtenteViewModel;
import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.AppCompatActivityWithMyPrimaryMenu;
import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;
import it.tn.unitn.disi.ppsmet.GestioneAssetPerFoto.GestioneFotoDaAsset;
import it.tn.unitn.disi.ppsmet.GestioneMyIntent.MyIntent;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.AlertDialogApp.MessageAlertDialog;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.AlertDialogApp.SetFingerprintAlertDialog;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.BarraConDuePulsanti;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListDatiTessere.AdapterDatiSpinner.DatiTessereRVAdapter;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListSpinnerWithAdd.AdapterWithSpinner.ListaSpinnerAllergeniRVAdapter;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListSpinnerWithAdd.ViewRecyclerListSpinnerWithAdd;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.SeparatorePaginaConTesto;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ShapedImageView;

public class ProfiloUtenteActivity extends AppCompatActivityWithMyPrimaryMenu {
/*################################################################################################*/
/*                                  IMPOSTA PARAMETRI DEI INTET PER MENU'LATERALE                 */
/*################################################################################################*/
    /**
     * Questo parametro viene chiamato nel momento in cui si preme un pulsante tra quelli del
     * menù laterale principale così da poter inserire i vari valori da passare tramite intent,
     * all'activity che segue. Conclusa questa procedura verrà attivato, l'intente per effetuare
     * il cabiamento d'activity, se chiamato dal menu laterare principale.
     * @param intent Questo parameto contiene l'intent pre impostata con l'activity d'arrivo.
     */
    @Override
    protected void setParamiter(MyIntent intent) {
        intent.putExtraIdUtente(MyIntent.I_UTENTE_ATTIVO,viewModel.getIdUtenteAttivo());
    }
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private ProfiloUtenteActivity activity = this;
/*------------------------------------------------------------------------*/
    private ProfiloUtenteViewModel viewModel;
    private GestioneFotoDaAsset    ACCEDI_FOTO = null;
/*------------------------------------------------------------------------*/
    private ViewGroup pagina;
    private ViewGroup divisoreTessere;
    private ViewGroup divisoreFingerprint;
    private ViewGroup divisoreAllegeni;
    private ViewGroup conteinerListaAllergeniView;
/*------------------------------------------------------------------------*/
    private ShapedImageView fotoUtente;
    private TextView        nomeCognomeUtennte;
    private RecyclerView    listaTessere;
    private CheckBox        ckbFingerprint;
/*------------------------------------------------------------------------*/
    private ViewRecyclerListSpinnerWithAdd listaSpinnerAllerdeni;
    private BarraConDuePulsanti            barraPulsantiAllergeni;
/*------------------------------------------------------------------------*/
    private LiveData<Utenti>          ldUtenteAttivo        = null;
    private LiveData<List<Tessere>>   ldListaTessere        = null;
    private LiveData<List<Allergeni>> ldListaAllAllergeni   = null;
    private LiveData<List<Allergeni>> ldListaAllergeniUtente= null;
/*------------------------------------------------------------------------*/
    private DatiTessereRVAdapter           adapterTessere = null;
    private ListaSpinnerAllergeniRVAdapter adapter_listaSpinnerAllergeni=null;
/*------------------------------------------------------------------------*/
    private Long          idUtenteAttivo;
    private Utenti        utenteAttivo          = null;
    private List<Tessere> listaTessereValide    = new LinkedList<>();
    private List<Allergeni> listaAllAllergeni   = new LinkedList<>();
    private Set<Allergeni>  listaAllergeniUtente= new HashSet<>();
/*================================================================================================*/
/*                                  CREAZIONE DELL'ACTIVITY                                       */
/*================================================================================================*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilo_utente);
/*------------------------------------------------------------------------*/
        //IMPOSTA MENU LETERALE
        super.initToolBar(R.menu.bar_menu_vuoto);
        super.impostaPaginaComeHome();
/*------------------------------------------------------------------------*/
        pagina              = findViewById(R.id.cly_PaginaProfiloUtente);
        divisoreTessere     = findViewById(R.id.trw_DivisoreTessere);
        divisoreFingerprint = findViewById(R.id.trw_DivisoreFingerprint);
        divisoreAllegeni    = findViewById(R.id.trw_DivisoreAllergeni);
        conteinerListaAllergeniView  = findViewById(R.id.trw_RigaListaAllergeni);
/*------------------------------------------------------------------------*/
        fotoUtente = findViewById(R.id.img_Utente);
        fotoUtente.setCircularCenter();
        nomeCognomeUtennte = findViewById(R.id.txt_NomeCognome);
        listaTessere       = findViewById(R.id.rcl_ListaTessera);
        ckbFingerprint     = findViewById(R.id.ckb_Fingerprint);
/*------------------------------------------------------------------------*/
        impostaLayoutStatico();
/*------------------------------------------------------------------------*/
        ACCEDI_FOTO = new GestioneFotoDaAsset(activity);
        //GESTIONE VIEWMODEL E UTENTE ATTIVO
       viewModel =  new ViewModelProvider(this).get(ProfiloUtenteViewModel.class);
       viewModel.setParameterFromIntet(getIntent());
/*------------------------------------------------------------------------*/
       idUtenteAttivo = viewModel.getIdUtenteAttivo();
       if(idUtenteAttivo==null){
           super.doLogout();
       }else{
/*------------------------------------------------------------------------*/
/*                                  AZZIONI CON L'UTENTE  ATTIVO          */
/*------------------------------------------------------------------------*/
           ldUtenteAttivo         = viewModel.cambiaUtenteAttivo(idUtenteAttivo);
           ldListaTessere         = viewModel.getListaTessereAACorrente(idUtenteAttivo);
           ldListaAllAllergeni    = viewModel.getAllAllergeni();
           ldListaAllergeniUtente = viewModel.getAllergeniByIdOfUtente(idUtenteAttivo);
/*------------------------------------------------------------------------*/
           adapterTessere = new DatiTessereRVAdapter(activity,listaTessereValide);
           listaTessere.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false));
           listaTessere.setAdapter(adapterTessere);
/*------------------------------------------------------------------------------------------------*/
/*                   |--------------DEL SEGUENTE PATRE DI CODICE L'ORDINE E'IMPOTRANTE            */
/*                   V              PER IL FUNZIONAMENTO DELLA LISTA DEI ALLERGENI                */
/*------------------------------------------------------------------------------------------------*/
           //Deve essere prima della chiamata al metodo "observeLiveDataAllergeni()"
           adapter_listaSpinnerAllergeni = new ListaSpinnerAllergeniRVAdapter(activity,listaAllAllergeni,null);
/*------------------------------------------------------------------------*/
           observeLiveDataUtenti(ldUtenteAttivo);
           observeLiveDataListaTessere(ldListaTessere);
           observeLiveDataAllergeni();//Deve essere eseguito prima di definire "listaSpinnerAllerdeni"
/*------------------------------------------------------------------------*/
           //Imposta RecyclerView dei allergeni
           listaSpinnerAllerdeni = new ViewRecyclerListSpinnerWithAdd(activity,
                    conteinerListaAllergeniView,
                   R.id.include_ListaSpinnerAllergeni,
                   R.string.lbl_ButtonAggiungiAllergene,null);
/*------------------------------------------------------------------------*/
           //Deve sessere l'ultima istrizione eseguita per la gestione dei allergeni
           impostaLissernerSugliEventiDiGestioneDeiAllergeni();
/*------------------------------------------------------------------------------------------------*/
           ckbFingerprint.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   ckbFingerprint.setChecked(!ckbFingerprint.isChecked());
                   SetFingerprintAlertDialog alertDialog= new SetFingerprintAlertDialog(
                           activity, R.layout.alert_dialog_configure_fingerprint,ckbFingerprint);
                   alertDialog.create();
                   alertDialog.show();
               }
           });
       }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA LAYOUT STATICO                                        */
/*================================================================================================*/
    /**
     * Questo metodo permette di definire le varie parti statiche del layout della pagina.
     */
    private void impostaLayoutStatico(){
        barraPulsantiAllergeni = new BarraConDuePulsanti(  pagina,
                R.id.include_SalvareAllergeni,
                R.string.lbl_ButtonSalva,
                R.string.lbl_ButtonAnnula);
/*------------------------------------------------------------------------*/
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude( divisoreTessere,
                R.id.include_DivisoreTessere,
                R.string.lbl_dvd_DatiTessere);
/*------------------------------------------------------------------------*/
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude( divisoreFingerprint,
                R.id.include_DivisoreFingereprint,
                R.string.lbl_dvd_GestioneMensa);
/*------------------------------------------------------------------------*/
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude( divisoreAllegeni,
                R.id.include_DivisoreAllergeni,
                R.string.lbl_dvd_ImpostaAllergie);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                 GESTIONE AZIONI LIVEDATA                                       */
/*################################################################################################*/
/*================================================================================================*/
/*                                  OBSERVER DATI UTENTE                                          */
/*================================================================================================*/
    /**
     * Questo metodo permette di ascoltare, impoatare ed eventualente cabiare i dati dell'utente che
     * sono mostrati nel layout.
     */
    private void observeLiveDataUtenti(LiveData<Utenti> ldUtenteAttivo){
        ldUtenteAttivo.observe(activity,
                new Observer<Utenti>() {
                    @Override
                    public void onChanged(Utenti utenti) {
                        if(utenti!=null){
                            utenteAttivo = utenti;
                            impostaDatiUtenteAttivo(utenti);
                        }
                    }
                });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  OBSERVER LISTA TESSERE                                        */
/*================================================================================================*/
    /**
     * Questo metodo permette di ascoltare, impoatare ed eventualente cabiare i dati delle tesserre
     * che sono valide in questo anno accademico,sia che sono attive che disattive, e mantiene aggornato
     * tale lista notificado anchè l'adaprer così protrà ricaricare e mostrare sempre dati aggiornati.
     */
    private void observeLiveDataListaTessere(LiveData<List<Tessere>> ldListaTessere){
        ldListaTessere.observe(activity,
                new Observer<List<Tessere>>() {
                    @Override
                    public void onChanged(List<Tessere> listaTessere) {
                        if(listaTessere!=null){
                            List<Tessere> tempo =new LinkedList<>();
                            Iterator<Tessere> iterator = listaTessere.iterator();
                            while (iterator.hasNext()) {
                                Tessere tessere = iterator.next();
                                if(tessere!=null){
                                    tempo.add(tessere);
                                }
                            }
                            listaTessereValide.clear();
                            listaTessereValide.addAll(tempo);
                            if(adapterTessere!=null){
                                adapterTessere.notifySortDataSetChanged();
                            }
                        }
                    }
                });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  OBSERVER ALLERGENI SIA UTENTE CHE TOTALE                      */
/*================================================================================================*/
    /**
     * Questo metodo permette di ascoltare, impoatare ed eventualente cabiare i dati delle allerie
     * di un utente e mantiene aggiunato la lista di tutti gli allergeni sempre aggionrata.
     */
    private void observeLiveDataAllergeni(){
        //Observere deli allerdeni già selezionati e salvati dal utente
        ldListaAllergeniUtente.observe(activity, new Observer<List<Allergeni>>() {
            @Override
            public void onChanged(List<Allergeni> allergenis) {
                listaAllergeniUtente.clear();
                listaAllergeniUtente.addAll(allergenis);
                adapter_listaSpinnerAllergeni = new ListaSpinnerAllergeniRVAdapter(activity,listaAllAllergeni,listaAllergeniUtente);
                listaSpinnerAllerdeni.setNewAdapter(activity,adapter_listaSpinnerAllergeni);
            }
        });
        //Observere oggetti selezionabili
        ldListaAllAllergeni.observe(activity, new Observer<List<Allergeni>>() {
            @Override
            public void onChanged(List<Allergeni> allergenis) {
                listaAllAllergeni.clear();
                listaAllAllergeni.addAll(allergenis);
                if(adapter_listaSpinnerAllergeni!=null){
                    adapter_listaSpinnerAllergeni.impostaAvviso();
                    adapter_listaSpinnerAllergeni.notifyDataSetChangedSpinner();
                }
            }
        });
    }
/*################################################################################################*/
/*################################################################################################*/
/*                                 IMPOSTAZIONI DEI DATI DEI LIVEDATA                             */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA DATI UTENTE ATTIVO                                    */
/*================================================================================================*/
    /**
     * Questo metodo riceve come parametro i dati utenti da inserire nella paggia.
     * @param utenti Parametro che contiene i dati utente.
     */
    private void impostaDatiUtenteAttivo(Utenti utenti){
        if(utenti!=null){
            nomeCognomeUtennte.setText(utenti.getNome()+" "+utenti.getCognome());
            if(utenti.getUrlImmagine()!=null){
                if(fotoUtente.setImageBitmapIfNotNUll(ACCEDI_FOTO.getFoto(utenti.getUrlImmagine(),R.drawable.img_userdefault))){
                    fotoUtente.setCircularCenter();
                }
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                 GESTIONE PULSANTI SALVA E ANNULLA PER ALLERGENI                */
/*################################################################################################*/
/*================================================================================================*/
/*                                  GESTIONE EVENTI LEGATI AI DUE TASTI PER ALLERGENI             */
/*================================================================================================*/
    /**
     * Questo metodo permette di impostare un lisserner sulla lista degli spinnere in modo tale che
     * sia possibile sapere quando viene aggiunto,tolto uno spinner ed in oltre se uno di essi ha
     * camniato item selezionato. Se dovesse accadere una di queste azzioni tale lisserner non fa
     * altro che rendere visibile la "barraPulsantiAllergeni".
     *
     * In oltre contiene i due lissener dei due pulsanti "salva" e "annulla" contenuti nella
     * "barraPulsantiAllergeni" in modo tale:
     *      - Se premuto "salva" rendere permanete le modifiche ai allergeni e chiudere la barra dei
     *          due tasti.
     *      - Se prenuto "annulla" ricarica la lista di spinner portandola con i parametri originali
     *          e chiude la barra dei due tasti.
     */
    private void impostaLissernerSugliEventiDiGestioneDeiAllergeni(){
        //Imposta salva nuovi valori allergeni
        barraPulsantiAllergeni.setOnPrimaryButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Allergeni> listaElementiSelezionati = adapter_listaSpinnerAllergeni.getElemetiSelezionati();
                viewModel.insertAllergia(utenteAttivo,listaElementiSelezionati,listaAllergeniUtente);
                barraPulsantiAllergeni.setVisibility(View.GONE);
            }
        });
        //Annulla cambiamenti valori
        barraPulsantiAllergeni.setOnSecondaryButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter_listaSpinnerAllergeni = new ListaSpinnerAllergeniRVAdapter(activity,listaAllAllergeni,listaAllergeniUtente);
                listaSpinnerAllerdeni.setNewAdapter(activity,adapter_listaSpinnerAllergeni);
                barraPulsantiAllergeni.setVisibility(View.GONE);
            }
        });
        //Rendi visibile i tasti "annulla" e "salva" qunado vienie modificato in qualche maniera
        //i valore dei allergeni
        listaSpinnerAllerdeni.addChangeListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                barraPulsantiAllergeni.setVisibility(View.VISIBLE);
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
}