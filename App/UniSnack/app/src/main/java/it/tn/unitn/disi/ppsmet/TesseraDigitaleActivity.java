package it.tn.unitn.disi.ppsmet;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.TesseraDigitaleActivity.Fragment.TessereBarCodeFragment;
import it.tn.unitn.disi.ppsmet.ActivityManager.TesseraDigitaleActivity.Fragment.TessereNonAttivaFragment;
import it.tn.unitn.disi.ppsmet.ActivityManager.TesseraDigitaleActivity.Fragment.TessereQRCodeFragment;
import it.tn.unitn.disi.ppsmet.ActivityManager.TesseraDigitaleActivity.TesseraDigitaleViewModel;
import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.AppCompatActivityWithMyPrimaryMenu;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.GestioneMyIntent.MyIntent;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.SelettoreABarra.OnChangeListener.OnChangeListener;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.SelettoreABarra.SelettoreABarra;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.MySpinnerAdapter.SpinnerAdapter_Tessere;
import it.tn.unitn.disi.ppsmet.MyAnimation.MyAnimationView;

public class TesseraDigitaleActivity extends AppCompatActivityWithMyPrimaryMenu {
/*################################################################################################*/
/*                                  IMPOSTA PARAMETRI DEI INTET PER MENU'LATERALE                 */
/*################################################################################################*/
    /**
     * Questo parametro viene chiamato nel momento in cui si preme un pulsante tra quelli del
     * menù laterale principale così da poter inserire i vari valori da passare tramite intent,
     * all'activity che segue. Conclusa questa procedura verrà attivato, l'intente per effetuare
     * il cabiamento d'activity, se chiamato dal menu laterare principale.
     * @param intent Questo parameto contiene l'intent pre impostata con l'activity d'arrivo.
     */
    @Override
    protected void setParamiter(MyIntent intent) {
        intent.putExtraIdUtente(MyIntent.I_UTENTE_ATTIVO,viewModel.getIdUtenteAttivo());
    }
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private TesseraDigitaleActivity activity = this;
/*------------------------------------------------------------------------*/
    private TesseraDigitaleViewModel viewModel;
/*------------------------------------------------------------------------*/
    private ViewGroup   pagina;
    private Fragment    fragment;
/*------------------------------------------------------------------------*/
    private SelettoreABarra<Tessere> selettoreABarra;
    private MyAnimationView          lockAnimatio;
    private BottomNavigationView     bottomTabarView;
/*------------------------------------------------------------------------*/
    private LiveData<List<Tessere>> ldListaTessere;
/*------------------------------------------------------------------------*/
    private SpinnerAdapter_Tessere adapter;
/*------------------------------------------------------------------------*/
    private Long          idUtenteAttivo;
    private boolean       isTesseraBloccata      = true;
    private List<Tessere> listaTessereAttiveOggi = new LinkedList<>();
/*================================================================================================*/
/*                                  CREAZIONE DELL'ACTIVITY                                       */
/*================================================================================================*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tessera_digitale);
/*------------------------------------------------------------------------*/
        pagina   = findViewById(R.id.cly_Pagina);
/*------------------------------------------------------------------------*/
        lockAnimatio = findViewById(R.id.img_anim_lock);
/*------------------------------------------------------------------------*/
        //IMPOSTA MENU LETERALE
        super.initToolBar(R.menu.bar_menu_vuoto);
        super.impostaPaginaComeHome();
/*------------------------------------------------------------------------*/
        //GESTIONE VIEWMODEL E UTENTE ATTIVO
        viewModel =  new ViewModelProvider(this).get(TesseraDigitaleViewModel.class);
        viewModel.setParameterFromIntet(getIntent());
/*------------------------------------------------------------------------*/
        idUtenteAttivo = viewModel.getIdUtenteAttivo();
        if(idUtenteAttivo==null){
            super.doLogout();
        }else{
/*------------------------------------------------------------------------*/
            ldListaTessere = viewModel.getActiveByIdOfUtenteAtToday(idUtenteAttivo);
/*------------------------------------------------------------------------*/
            selettoreABarra = SelettoreABarra.newInstance(pagina, R.id.include_SelettoreTessere);
            adapter = new SpinnerAdapter_Tessere(activity,listaTessereAttiveOggi,true);
            selettoreABarra.setAdapter(adapter);
/*------------------------------------------------------------------------*/
            selettoreABarra.setOnChangeListener(new OnChangeListener<Tessere>() {
                @Override
                public void onChangeListener(View view, Tessere itemSelected) {
                    viewModel.setTesseraSelezzionata(itemSelected);
                }
            });
/*------------------------------------------------------------------------*/
            ldListaTessere.observe(activity, new Observer<List<Tessere>>() {
                    @Override
                    public void onChanged(List<Tessere> listaTessere) {
                        if(listaTessere!=null){
                            listaTessereAttiveOggi.clear();
                            listaTessereAttiveOggi.addAll(listaTessere);
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
/*------------------------------------------------------------------------*/
            impostaFingerprintLock();
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
/*                                  BOTTONI DELLA TABAR IN BASSO                                  */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
            bottomTabarView = findViewById(R.id.tbb_TabTipoTessera);
            bottomTabarView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    return cambiaTesseraDaVisualizzare(menuItem,isTesseraBloccata);
                }
            });
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  IMPOSTA FINGERPRINT LOCK                                      */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA FINGERPRINT LOCK                                      */
/*================================================================================================*/
    /**
     * In questa versione del metodo il lock delle tessere si sblocca premento sull'immagine del
     * lucchetto e non c'è una reale lettuara del'impronta digitale.
     */
    private void impostaFingerprintLock(){
        isTesseraBloccata = true;
/*------------------------------------------------------------------------*/
        lockAnimatio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lockAnimatio.setNewAnimazioneRipetuta(R.drawable.avd_anim_lock_error,1);
            }
        });
/*------------------------------------------------------------------------*/
        lockAnimatio.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                lockAnimatio.setNewAnimazioneRipetuta(R.drawable.avd_anim_unlock,MyAnimationView.ULTIMA_ANIMAZIONE);
                lockAnimatio.setOnLongClickListener(null);
                lockAnimatio.setOnClickListener(null);
                isTesseraBloccata = false;
                bottomTabarView.setSelectedItemId(bottomTabarView.getSelectedItemId());
                return true;
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE SELEZIONE FRAGMENT                                   */
/*################################################################################################*/
/*================================================================================================*/
/*                                  SELEZIONE FRAGMENT                                            */
/*================================================================================================*/
    /**
     * Questo metodo si occupa di selezionare il fraggment corrette da visualizare. Se il valere di
     * "isTesseraBloccata" passato come parametro al metodo è true allora verra sempre visualizzato
     * il fraggment che indica che la tessera deve essere sbloccata.
     * Altrimenti, in base al valore "menuItem" selezionato viene visualizzato nel fraggment o la
     * tessera nella versione QRCode o nella vesione Fragment.
     * N.B.
     * La tessera selezionata che viene mostrata viene caricata sempre,nel viewModel e poi estratta
     * dal eventuale fragment, che la rende visibile.
     * @param menuItem
     * @param isTesseraBloccata
     * @return
     */
    private boolean cambiaTesseraDaVisualizzare(@NonNull MenuItem menuItem,boolean isTesseraBloccata){
        if(!isTesseraBloccata){
            switch (menuItem.getItemId()){
                case R.id.btn_tab_CodiceABarre:
                    fragment = new TessereBarCodeFragment();
                    break;
                case R.id.btn_tab_QRCode:
                    fragment= new TessereQRCodeFragment();
                    break;
                default:
                    fragment= null;
                    break;
            }
        }else{
            fragment= new TessereNonAttivaFragment();
        }
        boolean ris = false;
        if(fragment!=null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fly_Tessera, fragment);
            ft.commit();
            ris = true;
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE ANIMAZIONE E RIATTIVAZIONE FRAGMENT                  */
/*################################################################################################*/
/*================================================================================================*/
/*                                  ON RESUME                                                     */
/*================================================================================================*/
    /**
     * In questo override del metodo "onResume" si trova la logica legata alla attivazione delle
     * animazioni infinite presenti nella pagina. Questo poichè saranno immediatamente fermate appena
     * verrà chiamato il metodo "onPause".
     * In oltre in questo metodo viene selezionato sempre la tessera a codice a barra così da ricaricare
     * il fragment attivo. Questo serrve, (se sono presenti fingerprint) per riattivare la richesta di
     * sblocco della tessera.
     */
    @Override
    protected void onResume() {
        super.onResume();
        //Richiedi di visualizzare il codice a barra
        bottomTabarView.setSelectedItemId(bottomTabarView.getSelectedItemId());//Selezione la tessere
        //Attiva animazione infinita
        lockAnimatio.setNewAnimazioneRipetuta(   R.drawable.avd_anim_lock_scann,
                                                 MyAnimationView.RIPETIZIONE_INFINITA);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  ON PAUSE                                                      */
/*================================================================================================*/
    /**
     * In questo override del metodo "onPause" si trova la logica legata alla attivazione delle
     * animazioni presenti nella pagina così da blocare qualunque animazione.
     * In oltre rimposta il fingerprint lock così che alla prossima riattivazione richieda nuovamente
     * lo sblocco.
     */
    @Override
    protected void onPause() {
        super.onPause();
        //Fai richiedere di sbloccare la tessera ogni volta che si riattiva la pagina.
        impostaFingerprintLock();
        //Disattiva animazione infinita
        lockAnimatio.setNewAnimazioneRipetuta(   R.drawable.avd_anim_lock_scann,
                                                 MyAnimationView.RIPETIZIONE_DI_DEFAUULT);
    }
/*================================================================================================*/
/*################################################################################################*/
}