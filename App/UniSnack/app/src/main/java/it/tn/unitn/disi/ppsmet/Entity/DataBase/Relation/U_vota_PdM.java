package it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

import it.tn.unitn.disi.ppsmet.Entity.PastoDelMese;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;


@Entity(tableName = "tableU_vota_PdM",
        primaryKeys = {"id_proponete", "id_mese", "id_anno",
                       "id_utente"},
        foreignKeys = {
            @ForeignKey(entity = PastoDelMese.class,
                parentColumns = {"id_utente",
                                 "mese",
                                 "anno"},   //Colanna della classe a qui si rifierisce
                childColumns  = {"id_proponete",
                                 "id_mese",
                                 "id_anno"},//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE),
            @ForeignKey(entity = Utenti.class,
                parentColumns = "id",       //Colanna della classe a qui si rifierisce
                childColumns  = "id_utente",//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE)},
        indices = {
                @Index(value = {"id_mese", "id_anno", "id_utente"}, unique = true),
                @Index("id_utente")})
public class U_vota_PdM {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @ColumnInfo(name = "id_proponete")
    private Long    id_proponete;
    @NonNull
    @ColumnInfo(name = "id_mese")
    private Short id_mese;
    @NonNull
    @ColumnInfo(name = "id_anno")
    private Short id_anno;
    @NonNull
    @ColumnInfo(name = "id_utente")
    private Long    id_utente;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    public U_vota_PdM(@NonNull Long id_proponete,
                      @NonNull Short id_mese,
                      @NonNull Short id_anno,
                      @NonNull Long id_utente) {
        this.id_proponete = id_proponete;
        this.id_mese = id_mese;
        this.id_anno = id_anno;
        this.id_utente = id_utente;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    @NonNull
    public Long getId_proponete() {
        return id_proponete;
    }
//--------------------------------------
    public void setId_proponete(@NonNull Long id_proponete) {
        this.id_proponete = id_proponete;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Short getId_mese() {
        return id_mese;
    }
//--------------------------------------
    public void setId_mese(@NonNull Short id_mese) {
        this.id_mese = id_mese;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Short getId_anno() {
        return id_anno;
    }
//--------------------------------------
    public void setId_anno(@NonNull Short id_anno) {
        this.id_anno = id_anno;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_utente() {
        return id_utente;
    }
//--------------------------------------
    public void setId_utente(@NonNull Long id_utente) {
        this.id_utente = id_utente;
    }
//==================================================================================================
//##################################################################################################
}
