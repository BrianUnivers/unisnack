package it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.InterfaceViewModel.MenuProfiloUtenteViewModel;
import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Allergeni_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Mense_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.PietanzeForniteMensa_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Tessere_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.TipoPietanza_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Utenti_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class MenuDelleMenseViewModel extends MenuProfiloUtenteViewModel {
/*################################################################################################*/
    private PietanzeForniteMensa_Repository rPietanze;
    private TipoPietanza_Repository rTipoPietanza;
    private Mense_Repository rMense;
/*================================================================================================*/
/*                                  COSTRUTTORE VIEW MODEL                                        */
/*================================================================================================*/
    /**
     * Questo metodo costruttore del viewmode inizializza le repository che si interfacciano con entrambi
     * i database sia interno che estreno a seconda delle operazioni che vengono richeste. Che verranno
     * utilizate dall'activity e i suoi fragment.
     * @param application
     */
    public MenuDelleMenseViewModel(@NonNull Application application) {
        super(application);
        rMense        = new Mense_Repository(application);
        rPietanze     = new PietanzeForniteMensa_Repository(application);
        rTipoPietanza = new TipoPietanza_Repository(application);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  AZZIONI LEGATE ALLA GESITIONE DEI DATABASE                    */
/*################################################################################################*/
/*================================================================================================*/
/*                                  MENSE                                                         */
/*================================================================================================*/
    /**
     * Questo metodo richiede al database (interno), le mense che l'utente con l'id passato come
     * parametro ha accesso con una tessera attiva in questo moento.
     * Cioè il risultato contiene tutte le mense che fanno parte di una università a cui l'utente
     * è iscritto o piò partecipare, di cui però ha una tessera attiva per il corrente anno accademico
     * (o attualmente attiva).
     * @param idUtente
     * @return
     */
    public LiveData<List<Mense>> getMenseAccessibleByIdOfUtenteAtToday(Long idUtente){
        return rMense.getAccessibleByIdOfUtenteAtToday(idUtente);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  TIPO PIETANZA                                                 */
/*================================================================================================*/
    /**
     * Questo metodo reichiede al database (interno) di restituire  tutte le tipologie di pietanze
     * presenti.
     * @return
     */
    public LiveData<List<TipoPietanza>> getAllTipoPietanza(){
        return rTipoPietanza.getAll();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  LISTA PIETANZE IN UN GIORNO SPECIFICO DI UN TIPO DI MENU      */
/*================================================================================================*/
    /**
     * Questo metodo permette di restiuire tutte le pietanze, facenti parte del tipo di menu indicato
     * dall'id passtato come aparamento (idTipoMenu), presenti nel menu della mensa avente l'id passato
     * come parametro(idMensa), nel giorno specificato dal parametro "date",
     * @param idMensa
     * @param idTipoMenu
     * @param date
     * @return
     */
    public LiveData<List<Pietanze>> getPietanzeByIdOfMensa_TipoMenu_AtSpecificDate(Long idMensa,Long idTipoMenu, Date date){
        if((idMensa!=null) && (idTipoMenu!=null) && (date!=null)){
            return rPietanze.getByIdOfMensa_TipoMenu_SpecificDay(idMensa,idTipoMenu,date);
        }else{
            return null;
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  AZZIONI PER PASSARE LA MENSA SELEZIONATA                      */
/*################################################################################################*/
    /**
     * Questo variabile contiene il valore di un oggetto LiveData in cui si potrà inserire una mensa
     * e la fornirà ai richiedenti tramite un observer su questo oggetto.
     * In conclusione questo oggetto una valta chiamato il metodo "setMensaSelezzionata()" conterrà la
     * mensa seleziona.
     */
    protected MediatorLiveData<Mense> mensaSelezionata = new MediatorLiveData<>();
/*================================================================================================*/
/*                                  IMPOSTA MENSA SELEZIONATA                                     */
/*================================================================================================*/
    /**
     * Questo metodo permette d'impostare e sovrascrivere/cambiare la mensa selezionata se e soltanto
     * se l'oggetto mensa passato come paramento è diverso da null.
     * @param mense
     */
    public void setMensaSelezzionata(Mense mense){
        if(mense!=null){
            mensaSelezionata.setValue(mense);
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  RICHIEDI MENSA SELEZIONATA                                    */
/*================================================================================================*/
    /**
     * Questo metodo permette di ricevere un oggetto LiveData che conterrà l'ultima mensa selezionato
     * permettendo tramite un suo observer di rilevare dei cambiamei nei dati. Così facendo si da la
     * possibilità al richiedente di questo oggetto di rimanere aggiornato sul valore contenuto.
     * @return
     */
    public LiveData<Mense> getMensaSelezzionata(){
        return mensaSelezionata;
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  AZZIONI PER PASSARE IL TIPO MENU SELEZIONATA                  */
/*################################################################################################*/
    /**
     * Questo variabile contiene il valore di un oggetto LiveData in cui si potrà inserire una mensa
     * e la fornirà ai richiedenti tramite un observer su questo oggetto.
     * In conclusione questo oggetto una valta chiamato il metodo "setIdTipoPastoSelezzionato()" conterrà la
     * mensa seleziona.
     */
    protected MediatorLiveData<Long> idTipoPastoSelezionato = new MediatorLiveData<>();
/*================================================================================================*/
/*                                  IMPOSTA ID TIPO PASTO SELEZIONATA                             */
/*================================================================================================*/
    /**
     * Questo metodo permette d'impostare e sovrascrivere/cambiare l'id del tipo menu selezionata
     * se e soltanto se l'oggetto passato come paramento è diverso da null.
     * @param idTipMenu
     */
    public void setIdTipoPastoSelezzionato(Long idTipMenu){
        if((idTipMenu!=null)&&(idTipMenu>0)){
            idTipoPastoSelezionato.setValue(idTipMenu);
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  RICHIEDI ID TIPO PASTO SELEZIONATA                            */
/*================================================================================================*/
    /**
     * Questo metodo permette di ricevere un oggetto LiveData che conterrà l'ultima id del tipo manu
     * selezionato permettendo tramite un suo observer di rilevare dei cambiamei nei dati.
     * Così facendo si da la possibilità al richiedente di questo oggetto di rimanere aggiornato sul
     * valore contenuto.
     * @return
     */
    public LiveData<Long> getIdTipoPastoSelezzionato(){
        return idTipoPastoSelezionato;
    }
/*================================================================================================*/
/*################################################################################################*/
}
