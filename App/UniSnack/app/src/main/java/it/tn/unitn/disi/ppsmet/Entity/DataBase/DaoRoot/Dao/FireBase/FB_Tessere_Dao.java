package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_IscrizioniUtente;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Tessere;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.IscrizioniUtente;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;


public class FB_Tessere_Dao extends ConnesioneAFirebase<Tessere> {
    private FB_Universita_Dao FB_Universita_Dao = new FB_Universita_Dao();
    private FB_Corsi_Dao      FB_Corsi_Dao      = new FB_Corsi_Dao();
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERISCI DATI UTENTI                                                  */
/*================================================================================================*/
    public static HashMap<String,Object> inserisciDatiUtenti(HashMap<String,Object> istruzioni, String root, Tessere tessere){
        //Singoli Campi
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_STATO_TESSERA).toString(),
                tessere.getStatoTessera());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_URL_CODICE_A_BARRE).toString(),
                tessere.getUrlCodiceABarre());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_URL_QRCODE).toString(),
                tessere.getUrlQRcode());


        FB_AnnoAccademico_Dao.inserisciDatiAnnoAccademico(istruzioni,
                new StringBuffer(root).append(S_SEPARARORE).append(L_ANNO_ACCADEMICO).toString(),
                tessere.getAnnoAccademico());
        return istruzioni;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    public void insert(@NotNull Utenti utenti,@NotNull List<Tessere> listaTessere){

        DatabaseReference ref = getReferenceFireBase()
                                        .getReference()
                                        .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
                                        .child(builderKeyFromUtenti(utenti))
                                        .child(L_PARTECIPA_UNIVERSITA);
        Iterator<Tessere> iterator = listaTessere.iterator();
        while(iterator.hasNext()) {
            Tessere tessere = iterator.next();
            //Dati universita
            HashMap<String, Object> istrizioniInsermetoUniversita = new HashMap<>();
            FB_Universita_Dao.inserisciDatiUniversita(istrizioniInsermetoUniversita, "", tessere.universita);
            //Dati tessere
            HashMap<String, Object> istrizioniInsermetoTessere = new HashMap<>();
            FB_Universita_Dao.inserisciDatiUniversita(istrizioniInsermetoUniversita, "", tessere.universita);
            //Inserimeto dati
            ref.child(tessere.universita.getId().toString()).updateChildren(istrizioniInsermetoUniversita);
            ref.child(tessere.universita.getId().toString()).child(L_TESSERE).push().updateChildren(inserisciDatiUtenti(istrizioniInsermetoTessere,"",tessere));
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
    public LiveData<List<Tessere>> getByIdOfUtente_OneTime(@NonNull Utenti utenti){
        return getGeneric_OneTime(utenti,null,null);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getByIdOfUtente(@NonNull Utenti utenti){
        return getGeneric(utenti,null,null);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getByIdOfUtente_Synchronized(@NonNull Utenti utenti){
        return getGeneric_Synchronized(utenti,null,null);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE  INTO SPECIFIC DAY                             */
/*================================================================================================*/
    public LiveData<List<Tessere>> getByIdOfUtente_OneTime(@NonNull Utenti utenti, Date date){
        return getGeneric_OneTime(utenti,date,null);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getByIdOfUtente(@NonNull Utenti utenti, Date date){
        return getGeneric(utenti,date,null);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getByIdOfUtente_Synchronized(@NonNull Utenti utenti, Date date){
        return getGeneric_Synchronized(utenti,date,null);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE TODAY                                          */
/*================================================================================================*/
    public LiveData<List<Tessere>> getByIdOfUtenteAtToday_OneTime(@NonNull Utenti utenti){
        return this.getByIdOfUtente_OneTime(utenti,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getByIdOfUtenteAtToday(@NonNull Utenti utenti){
        return this.getByIdOfUtente(utenti,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getByIdOfUtenteAtToday_Synchronized(@NonNull Utenti utenti){
        return this.getByIdOfUtente_Synchronized(utenti,new Date());
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET TESSERE ATTIVE by ID UTENTE                                 */
/*================================================================================================*/
    public LiveData<List<Tessere>> getActiveByIdOfUtente_OneTime(@NonNull Utenti utenti){
        return getGeneric_OneTime(utenti,null,true);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getActiveByIdOfUtente(@NonNull Utenti utenti){
        return getGeneric(utenti,null,true);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getActiveByIdOfUtente_Synchronized(@NonNull Utenti utenti){
        return getGeneric_Synchronized(utenti,null,true);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET TESSERE ATTIVE by ID UTENTE  INTO SPECIFIC DAY              */
/*================================================================================================*/
    public LiveData<List<Tessere>> getActiveByIdOfUtente_OneTime(@NonNull Utenti utenti, Date date){
        return getGeneric_OneTime(utenti,date,true);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getActiveByIdOfUtente(@NonNull Utenti utenti, Date date){
        return getGeneric(utenti,date,true);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getActiveByIdOfUtente_Synchronized(@NonNull Utenti utenti, Date date){
        return getGeneric_Synchronized(utenti,date,true);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET TESSERE ATTIVE by ID UTENTE TODAY                           */
/*================================================================================================*/
    public LiveData<List<Tessere>> getActiveByIdOfUtenteAtToday_OneTime(@NonNull Utenti utenti){
        return this.getActiveByIdOfUtente_OneTime(utenti,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getActiveByIdOfUtenteAtToday(@NonNull Utenti utenti){
        return this.getActiveByIdOfUtente(utenti,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getActiveByIdOfUtenteAtToday_Synchronized(@NonNull Utenti utenti){
        return this.getActiveByIdOfUtente_Synchronized(utenti,new Date());
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET TESSERE GENERIC                                             */
/*================================================================================================*/
    private LiveData<List<Tessere>> getGeneric_OneTime(@NonNull Utenti utenti,Date data,Boolean statoTessera){
//..........................................
        //Patecipa ma non iscritto
        DatabaseReference refPrimaParte = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PARTECIPA_UNIVERSITA);
        //_OneTime
        FirebaseQueryLiveData_OneTime pripmoParziale = new FirebaseQueryLiveData_OneTime(refPrimaParte);
        LiveData<List<Tessere>> risPripmoParziale =
                Transformations.map(pripmoParziale, new Deserializer_Tessere.Deserializer_List(data,statoTessera ));
//..........................................
        //Partecipa PERCHE' E' iscritto
        //_OneTime
        LiveData<Universita> universita = FB_Universita_Dao.getByPrimaryKey_OneTime(utenti.getId_Universita());

        MediatorLiveData<List<Tessere>> risSecondoParziale = new MediatorLiveData<>();
        risSecondoParziale.addSource(universita, new Observer<Universita>() {
            @Override
            public void onChanged(Universita universita) {
                //_OneTime
                LiveData<Corsi>  corsi  = FB_Corsi_Dao.getByIdOfUtente_OneTime(utenti);
                risSecondoParziale.addSource(corsi, new Observer<Corsi>() {
                    @Override
                    public void onChanged(Corsi corsi) {
                        //Query vera e propria
                        DatabaseReference refSecondaParte = getReferenceFireBase().getReference()
                                .child(L_UTENTI_DEL_CORSO_DEL_UNIVERSITA)
                                .child(utenti.getId_Universita().toString())
                                .child(L_CORSI)
                                .child(utenti.getId_Corso().toString())
                                .child(L_ISCRITTI)
                                .child(builderKeyFromUtenti(utenti));
                        //_OneTime
                        FirebaseQueryLiveData_OneTime secondaParziale = new FirebaseQueryLiveData_OneTime(refSecondaParte);
                        LiveData<Tessere> risSecondaParziale =
                                Transformations.map(secondaParziale, new Deserializer_Tessere.Deserializer_Object(universita,corsi,data,statoTessera));
                        risSecondoParziale.addSource(risSecondaParziale, new Observer<Tessere>() {
                            @Override
                            public void onChanged(Tessere tessere) {
                                if(tessere!=null){
                                    List<Tessere> list = new LinkedList<>();
                                    list.add(tessere);
                                    risSecondoParziale.setValue(list);
                                }
                            }
                        });
                    }
                });
            }
        });
//..........................................
        return this.mergeLiveData(risPripmoParziale,risSecondoParziale);
    }
/*------------------------------------------------------------------------*/
    private LiveData<List<Tessere>> getGeneric(@NonNull Utenti utenti,Date data,Boolean statoTessera){
    //..........................................
        //Patecipa ma non iscritto
        DatabaseReference refPrimaParte = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PARTECIPA_UNIVERSITA);
        //_
        FirebaseQueryLiveData pripmoParziale = new FirebaseQueryLiveData(refPrimaParte);
        LiveData<List<Tessere>> risPripmoParziale =
                Transformations.map(pripmoParziale, new Deserializer_Tessere.Deserializer_List(data,statoTessera ));
    //..........................................
        //Partecipa PERCHE' E' iscritto
        //_
        LiveData<Universita> universita = FB_Universita_Dao.getByPrimaryKey(utenti.getId_Universita());

        MediatorLiveData<List<Tessere>> risSecondoParziale = new MediatorLiveData<>();
        risSecondoParziale.addSource(universita, new Observer<Universita>() {
            @Override
            public void onChanged(Universita universita) {
                //_
                LiveData<Corsi>  corsi  = FB_Corsi_Dao.getByIdOfUtente(utenti);
                risSecondoParziale.addSource(corsi, new Observer<Corsi>() {
                    @Override
                    public void onChanged(Corsi corsi) {
                        //Query vera e propria
                        DatabaseReference refSecondaParte = getReferenceFireBase().getReference()
                                .child(L_UTENTI_DEL_CORSO_DEL_UNIVERSITA)
                                .child(utenti.getId_Universita().toString())
                                .child(L_CORSI)
                                .child(utenti.getId_Corso().toString())
                                .child(L_ISCRITTI)
                                .child(builderKeyFromUtenti(utenti));
                        //_
                        FirebaseQueryLiveData secondaParziale = new FirebaseQueryLiveData(refSecondaParte);
                        LiveData<Tessere> risSecondaParziale =
                                Transformations.map(secondaParziale, new Deserializer_Tessere.Deserializer_Object(universita,corsi,data,statoTessera));
                        risSecondoParziale.addSource(risSecondaParziale, new Observer<Tessere>() {
                            @Override
                            public void onChanged(Tessere tessere) {
                                if(tessere!=null){
                                    List<Tessere> list = new LinkedList<>();
                                    list.add(tessere);
                                    risSecondoParziale.setValue(list);
                                }
                            }
                        });
                    }
                });
            }
        });
    //..........................................
        return this.mergeLiveData(risPripmoParziale,risSecondoParziale);
    }
/*------------------------------------------------------------------------*/
private LiveData<List<Tessere>> getGeneric_Synchronized(@NonNull Utenti utenti,Date data,Boolean statoTessera){
//..........................................
    //Patecipa ma non iscritto
    DatabaseReference refPrimaParte = super.getReferenceFireBase().getReference()
            .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
            .child(builderKeyFromUtenti(utenti))
            .child(L_PARTECIPA_UNIVERSITA);
    //_Synchronized
    FirebaseQueryLiveData_Synchronized pripmoParziale = new FirebaseQueryLiveData_Synchronized(refPrimaParte);
    LiveData<List<Tessere>> risPripmoParziale =
            Transformations.map(pripmoParziale, new Deserializer_Tessere.Deserializer_List(data,statoTessera ));
//..........................................
    //Partecipa PERCHE' E' iscritto
    //_Synchronized
    LiveData<Universita> universita = FB_Universita_Dao.getByPrimaryKey_Synchronized(utenti.getId_Universita());

    MediatorLiveData<List<Tessere>> risSecondoParziale = new MediatorLiveData<>();
    risSecondoParziale.addSource(universita, new Observer<Universita>() {
        @Override
        public void onChanged(Universita universita) {
            //_Synchronized
            LiveData<Corsi>  corsi  = FB_Corsi_Dao.getByIdOfUtente_Synchronized(utenti);
            risSecondoParziale.addSource(corsi, new Observer<Corsi>() {
                @Override
                public void onChanged(Corsi corsi) {
                    //Query vera e propria
                    DatabaseReference refSecondaParte = getReferenceFireBase().getReference()
                            .child(L_UTENTI_DEL_CORSO_DEL_UNIVERSITA)
                            .child(utenti.getId_Universita().toString())
                            .child(L_CORSI)
                            .child(utenti.getId_Corso().toString())
                            .child(L_ISCRITTI)
                            .child(builderKeyFromUtenti(utenti));
                    //_Synchronized
                    FirebaseQueryLiveData_Synchronized secondaParziale = new FirebaseQueryLiveData_Synchronized(refSecondaParte);
                    LiveData<Tessere> risSecondaParziale =
                            Transformations.map(secondaParziale, new Deserializer_Tessere.Deserializer_Object(universita,corsi,data,statoTessera));
                    risSecondoParziale.addSource(risSecondaParziale, new Observer<Tessere>() {
                        @Override
                        public void onChanged(Tessere tessere) {
                            if(tessere!=null){
                                List<Tessere> list = new LinkedList<>();
                                list.add(tessere);
                                risSecondoParziale.setValue(list);
                            }
                        }
                    });
                }
            });
        }
    });
//..........................................
    return this.mergeLiveData(risPripmoParziale,risSecondoParziale);
}
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
    public LiveData<List<Tessere>> getSecondaryByIdOfUtente_OneTime(@NonNull Utenti utenti){
        return getSecondaryGeneric_OneTime(utenti,null,null);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getSecondaryByIdOfUtente(@NonNull Utenti utenti){
        return getSecondaryGeneric(utenti,null,null);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getSecondaryByIdOfUtente_Synchronized(@NonNull Utenti utenti){
        return getSecondaryGeneric_Synchronized(utenti,null,null);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET TESSERE SECONDARY GENERIC                                   */
/*================================================================================================*/
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getSecondaryGeneric_OneTime(@NonNull Utenti utenti,Date data,Boolean statoTessera){
        DatabaseReference refPrimaParte = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PARTECIPA_UNIVERSITA);
        //_OneTime
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(refPrimaParte);
        return Transformations.map(ris, new Deserializer_Tessere.Deserializer_List(data,statoTessera ));
    }
/*------------------------------------------------------------------------*/
public LiveData<List<Tessere>> getSecondaryGeneric(@NonNull Utenti utenti,Date data,Boolean statoTessera){
    DatabaseReference refPrimaParte = super.getReferenceFireBase().getReference()
            .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
            .child(builderKeyFromUtenti(utenti))
            .child(L_PARTECIPA_UNIVERSITA);
    //_
    FirebaseQueryLiveData ris = new FirebaseQueryLiveData(refPrimaParte);
    return Transformations.map(ris, new Deserializer_Tessere.Deserializer_List(data,statoTessera ));
}
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getSecondaryGeneric_Synchronized(@NonNull Utenti utenti,Date data,Boolean statoTessera){
        DatabaseReference refPrimaParte = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PARTECIPA_UNIVERSITA);
        //_Synchronized
        FirebaseQueryLiveData_Synchronized pripmoParziale = new FirebaseQueryLiveData_Synchronized(refPrimaParte);
        return Transformations.map(pripmoParziale, new Deserializer_Tessere.Deserializer_List(data,statoTessera ));
    }
/*================================================================================================*/
/*################################################################################################*/
}
