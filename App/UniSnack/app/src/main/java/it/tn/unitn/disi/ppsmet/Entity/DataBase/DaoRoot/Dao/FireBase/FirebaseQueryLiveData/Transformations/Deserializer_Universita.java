package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.util.Date;
import java.util.Iterator;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;

public class Deserializer_Universita implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto Universita.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String LOG_TAG = "Universita_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "Universita" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "Universita".
 * @param data Questo parametro può contenere una data che può essere definita o null. Se definita
 *             vuol dire che si stà chiedendo se l'iscrizione all'universita  che si trova nel parameto
 *             "dataSnapshot" ha validita in quella data. Se si l'oggeto università verrà inserito se
 *             no la funzione ritornerà null. Se invece è null, ritornerà sempre, se presente, universittà
 *             richiesta contenuta nei dati "dataSnapshot".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public static Universita estraiDaDataSnapshot(DataSnapshot dataSnapshot,Date data){
        Universita universita = null;
        if(dataSnapshot.exists()){
            try{
                if(verificaPartecipazioneAllaData(dataSnapshot,data)){
                    //Campi not null
                    Long id      = Long.parseLong( dataSnapshot.getKey());
                    String nome  = dataSnapshot.child(P_NOME).getValue(String.class);
                    String sigla = dataSnapshot.child(P_SIGLA).getValue(String.class);
                    //Verifica campi not null
                    if((id!=null) && (nome!=null) && (sigla!=null)){
                        universita = new Universita(id,nome,sigla);
                    }
                }
            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
                universita = null;
            }
        }
        return universita;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         VERIFICA CHE CI SIA TRA LE TESSERE UNA CHE HA UN ANNO ACCADEMICO       */
/*                         CONTENENTE LA DATA PASSATA                                             */
/*================================================================================================*/
    private static boolean verificaPartecipazioneAllaData(DataSnapshot dataSnapshot,Date date){
        boolean ris = false;
        if(date==null){
            ris = true;
        }else{
            if(verificaDataInternaAlAnnoAccademicoDiAmenoUnaTessera(dataSnapshot,date) ){
                ris = true;
            }
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
    private static boolean verificaDataInternaAlAnnoAccademicoDiAmenoUnaTessera(DataSnapshot dataSnapshot,Date date){
        boolean trovatoTesseraValida = false;
        boolean RicercaTerminata = false;
        DataSnapshot tessereDataSnapshot = dataSnapshot.child(L_TESSERE);
        if(tessereDataSnapshot.exists()){
            Iterator<DataSnapshot> iterator = tessereDataSnapshot.getChildren().iterator();
            do{
                if(iterator.hasNext()){
                    DataSnapshot tessera= iterator.next();
                    AnnoAccademico aaTessera= Deserializer_AnnoAccademico.getAttributeOf(tessera,L_ANNO_ACCADEMICO);
                    if((aaTessera!=null) && (aaTessera.dataNelAnnoAccademico(date))){
                        trovatoTesseraValida = true;
                        RicercaTerminata = true;
                    }
                }else{
                    RicercaTerminata=true;
                }
            }while (!RicercaTerminata);
        }
        return trovatoTesseraValida;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, Universita>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Object extends GenericDeserializer_Object<Universita> {
        @Override
        public Universita estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Universita.estraiDaDataSnapshot(dataSnapshot,null);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<Universita>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List<Universita> {
    private Date date =null;
/*================================================================================================*/
/*                         COSTRUTTORE                                                            */
/*================================================================================================*/
    public Deserializer_List() {
        super();
    }
/*------------------------------------------------------------------------*/
    public Deserializer_List( Date date) {
        super();
        this.date= date;
    }
/*================================================================================================*/
        @Override
        public Universita estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Universita.estraiDaDataSnapshot(dataSnapshot,date);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
