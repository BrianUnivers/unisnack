package it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner;

import android.content.res.ColorStateList;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.LinkedList;

import it.tn.unitn.disi.ppsmet.R;

public class ViewInputSpinner_Text<T> {
/*################################################################################################*/
    private   TextView label = null;
    protected Spinner  input = null;
/*------------------------------------------------------------------------*/
    private   Class<T> t;
/*------------------------------------------------------------------------*/
    protected boolean  campoObbligatorio = false;
/*------------------------------------------------------------------------*/
    private View.OnClickListener onClicListener=null;
    private AdapterView.OnItemSelectedListener onItemSelectedListener=null;
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP VIEW FIELD                                */
/*================================================================================================*/
    public ViewInputSpinner_Text(ViewGroup rootLayout, int idLayoutIncluded, int idLable,Class<T> t) {
        this.campoObbligatorio =false;
        this.t = t;
        impostaLayoutComune(rootLayout,idLayoutIncluded,idLable,null);
    }
/*------------------------------------------------------------------------*/
    public ViewInputSpinner_Text(ViewGroup rootLayout, int idLayoutIncluded, int idLable, boolean obbligatorio,Class<T> t) {
        this.campoObbligatorio =obbligatorio;
        this.t = t;
        impostaLayoutComune(rootLayout,idLayoutIncluded,idLable,null);
    }
/*------------------------------------------------------------------------*/
    public ViewInputSpinner_Text(ViewGroup rootLayout, int idLayoutIncluded, int idLable,SpinnerAdapter adapter,Class<T> t) {
        this.campoObbligatorio =false;
        this.t = t;
        impostaLayoutComune(rootLayout,idLayoutIncluded,idLable,adapter);
    }
/*------------------------------------------------------------------------*/
    public ViewInputSpinner_Text(ViewGroup rootLayout, int idLayoutIncluded, int idLable,SpinnerAdapter adapter, boolean obbligatorio,Class<T> t) {
        this.campoObbligatorio =obbligatorio;
        this.t = t;
        impostaLayoutComune(rootLayout,idLayoutIncluded,idLable,adapter);
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Qusto è il metodo vero e proprio che contiene la procedura per definire e costuire uno spinner.
     * Infatti chamato da tutti i vari costruttori, e permette di inserire una lable allo spinnere
     * e inserirci un adapter che lo gestisce.
     * @param rootLayout
     * @param idLayoutIncluded
     * @param idLable
     * @param adapter
     */
    private void impostaLayoutComune(ViewGroup rootLayout, int idLayoutIncluded, int idLable,SpinnerAdapter adapter) {
        if(rootLayout!=null){
            ViewGroup gruppoField = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(gruppoField!=null){
                //LABLE
                label = (TextView) gruppoField.findViewById(R.id.txt_LabelSpinner);
                label.setText(idLable);
                if(campoObbligatorio){
                    label.setText(label.getText()+" *");
                }
                //INPUT
                input = (Spinner) gruppoField.findViewById(R.id.spn_Input_Spinner);
                if(adapter!=null){
                    input.setAdapter(adapter);
                }
                input.setOnItemSelectedListener(new MyOnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        super.onItemSelected(adapterView, view, i, l);
                    }
                });
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP VIEW FIELD                                */
/*================================================================================================*/
    /**
     * Questo metodo permette di insereire in un secondo momento un'adapter allo spinner, se presente,
     * sostituendo il precedente.
     * Ritorna true solo se è stato possibile impostare l'adapter altrimenti è false.
     * @param adapter
     * @return
     */
    public boolean setAdapter(ArrayAdapter<T> adapter){
        boolean ris = false;
        if(input!=null){
            ris = true;
            input.setAdapter(adapter);
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  SET ON ITEM SELECTED LISTERNER                                */
/*================================================================================================*/
    /**
     * Questo metodo permette di impostare un "ItemSelectedListene" allo spinner.
     * Ritorna true solo se è stato possibile impostare il Listene altrimenti è false.
     * @param listener
     * @return
     */
    public boolean setOnItemSelectedListener(AdapterView.OnItemSelectedListener listener){
        boolean ris = false;
        if(input!=null){
            this.onItemSelectedListener = listener;
            ris = true;
            input.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if(onItemSelectedListener!=null) {
                        onItemSelectedListener.onItemSelected(adapterView,view,i,l);
                        //Questo server per definire un altro lissrener.
                        if(onClicListener!=null) {
                            onClicListener.onClick(input);
                        }
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    if(onItemSelectedListener!=null) {
                        onItemSelectedListener.onNothingSelected(adapterView);
                    }
                }
            });
        }
        return ris;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  SET ON CLICK LISSENER                                         */
/*================================================================================================*/
    /**
     * Questo metodo permette di defineire un altro lissenere simile a quello "ItemSelectedListene"
     * che identifica se c'è un cambiamneto di valore nello spinner selezionato.
     * @param onClicListener
     */
    public void setOnClicListener(View.OnClickListener onClicListener){
        this.onClicListener = onClicListener;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  SET ON ITEM SELECTED LISTERNER                                */
/*================================================================================================*/
    /**
     * Questo metodo permette di estrarre i valore selezionato dallo spinner.
     * @return
     */
    public T getItemSelected(){
        T ris = null;
        if(input!=null){
            Object o =input.getSelectedItem();
            if((o!=null) && (t.isAssignableFrom(o.getClass())) ){
                ris = (T) o;
            }
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  SEGALE DI STARE ATTENTO                                       */
/*================================================================================================*/
    /**
     * Questo metodo permette di impostare il layout dello spinnere in modo tale da seggnalare che
     * qualche avvenimento(in generalre il cambiamento dei valori dello spinner), cambiando il colore.
     */
    public void impostaAvviso(){
        if(input!=null){
            input.setBackgroundTintList(ColorStateList.valueOf(input.getResources().getColor(R.color.colorWarning,null)));
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  SEGALE DI STARE ATTENTO                                       */
/*================================================================================================*/
    /**
     * Questo metodo permette di impostare il layout dello spinnere in modo tale da annulare aventiali
     * camiamenti apportati con precedenti chiamate al metodo "impostaAvviso".
     */
    public void annullaAvviso(){
        if(input!=null){
            input.setBackgroundTintList(ColorStateList.valueOf(input.getResources().getColor(R.color.colorPrimary,null)));
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  SEGALE DI STARE ATTENTO                                       */
/*################################################################################################*/
    /**
     * Qusta classe contiene è legata ad un oggetto e permette di definire un ascoltatre di "ItemSelectedListener", in
     * cui c'è l'annullamento di eventuali avvisi precendenti, ad ogni selezione di elemento.
     * In oltre  questo listener è già presente non appena si crea l'ggetto è fa in modo di attivare
     * un eventiale "onClicListener" solamento dopo avre caricato e quindi IMPOSTAO IL PRIMO
     * ITEM SELEZIONATO. Poichè se non ci fosse questo controllo appena inserisco i dati verrebbe
     * chiamato il metodo definito da "onClicListener".
     */
    public abstract class MyOnItemSelectedListener implements AdapterView.OnItemSelectedListener{
        private boolean primaImpostazione = true;
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            annullaAvviso();
            if(!primaImpostazione){
                //Questo server per definire un altro lissrener.
                if(onClicListener!=null) {
                    onClicListener.onClick(input);
                }
            }else{
                primaImpostazione=false;
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> adapterView) { }
    }
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA VALORE CON QUELLO UGUALE A QUELLO PASSATO             */
/*================================================================================================*/
    /**
     * Questo metodo permette di cambiare l'elemento selezionato con quello passato come parameto,
     * ma questo solo se, tale oggetto è già presente come valore selezionabile tra qulli dello spinner.
     * @param valore
     */
    public void setSelection(T valore) {
        SpinnerAdapter adapter = input.getAdapter();
        if ((!adapter.isEmpty()) && (!getItemSelected().equals(valore))) {
            int maxId = adapter.getCount() - 1;
            int i = -1;
            boolean ricercaTerminata = false;
            boolean valoreTrovato = false;
            do {
                i++;
                if (adapter.getItem(i).equals(valore)) {
                    ricercaTerminata = true;
                    valoreTrovato = true;
                } else {
                    if (i == maxId) {
                        valoreTrovato = false;
                        ricercaTerminata = true;
                    }
                }
            } while (!ricercaTerminata);
            if (valoreTrovato) {
                input.setSelection(i);
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
