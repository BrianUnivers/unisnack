package it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;

@Entity(tableName = "tableM_fornisce_P",
        primaryKeys = {"id_mensa",
                       "id_pietanza",
                       "mese", "anno","giornoSettimana",
                       "id_tipoMenu"},
        foreignKeys = {
            @ForeignKey(entity = Mense.class,
                parentColumns = "id",      //Colanna della classe a qui si rifierisce
                childColumns  = "id_mensa",//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE),
            @ForeignKey(entity = Pietanze.class,
                parentColumns = "id",         //Colanna della classe a qui si rifierisce
                childColumns  = "id_pietanza",//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE),
            @ForeignKey(entity = TipoMenu.class,
                parentColumns = "id",         //Colanna della classe a qui si rifierisce
                childColumns  = "id_tipoMenu",//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE)},
        indices = {
                @Index(value = {"id_pietanza"}),
                @Index(value = {"id_tipoMenu"})})
public class M_fornisce_P {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @ColumnInfo(name = "id_mensa")
    private Long    id_mensa;
    @NonNull
    @ColumnInfo(name = "id_pietanza")
    private Long    id_pietanza;
    @NonNull
    @ColumnInfo(name = "mese")
    private Integer mese = Calendar.getInstance().get(Calendar.MONTH)+1 ;//tra 1 e 12 compresi
    @NonNull
    @ColumnInfo(name = "anno")
    private Integer anno = Calendar.getInstance().get(Calendar.YEAR);
    @NonNull
    @ColumnInfo(name = "giornoSettimana")
    private Integer giornoSettimana = DayOfWeek.SUNDAY.getValue();
    @NonNull
    @ColumnInfo(name = "id_tipoMenu")
    private Long    id_tipoMenu;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    public M_fornisce_P(@NonNull Long id_mensa,
                        @NonNull Long id_pietanza,
                        @NonNull Integer mese,
                        @NonNull Integer anno,
                        @NonNull Integer giornoSettimana,
                        @NonNull Long id_tipoMenu) {
        this.id_mensa = id_mensa;
        this.id_pietanza = id_pietanza;
        this.setAnno(anno);
        this.setMese(mese);
        this.giornoSettimana =giornoSettimana;
        this.id_tipoMenu = id_tipoMenu;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    @NonNull
    public Long getId_mensa() {
        return id_mensa;
    }
//--------------------------------------
    public void setId_mensa(@NonNull Long id_mensa) {
        this.id_mensa = id_mensa;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_pietanza() {
        return id_pietanza;
    }
//--------------------------------------
    public void setId_pietanza(@NonNull Long id_pietanza) {
        this.id_pietanza = id_pietanza;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Integer getMese() {
        return mese;
    }
//--------------------------------------
    public void setMese(@NonNull Integer mese) {
        if((mese!=null) && (mese>=1) && (mese<=12)){
            this.mese = mese;
        }
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Integer getAnno() {
        return anno;
    }
//--------------------------------------
    public void setAnno(@NonNull Integer anno) {
        this.anno = anno;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Integer getGiornoSettimana() {
        return giornoSettimana;
    }
//--------------------------------------
    public void setGiornoSettimana(@NonNull DayOfWeek giornoSettimana) {
        this.giornoSettimana = giornoSettimana.getValue();
    }
//--------------------------------------
    public void setGiornoSettimana(@NonNull Integer giornoSettimana) {
        if((giornoSettimana!=null) && (giornoSettimana>=1) && (giornoSettimana<=7)){
            this.giornoSettimana = giornoSettimana;
        }
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_tipoMenu() {
        return id_tipoMenu;
    }
//--------------------------------------
    public void setId_tipoMenu(@NonNull Long id_tipoMenu) {
        this.id_tipoMenu = id_tipoMenu;
    }
//--------------------------------------------------------------------------------------------------
    public String  getId(){
        return new StringBuffer().append(getAnno())
                .append("-").append(getMese())
                .append("-").append(getGiornoSettimana())
                .append("-").append(getId_mensa())
                .append("-").append(getId_pietanza())
                .append("-").append(getId_tipoMenu()).toString();
    }
//==================================================================================================
//##################################################################################################
}
