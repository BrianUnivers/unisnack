package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.room.Transaction;

import java.util.Date;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Allergeni_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Tessere_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Utenti_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Tessere_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.UtentiPartecipaUniversita_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class Tessere_Repository extends UtentiPartecipaUniversita_ThreadAggiornamento {
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA U_PARTECIPA_UNI (e le sue derivazioni seguendo  */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
    private FB_Tessere_Dao fbTessereDao;
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Tessere_Repository(Application application) {
        super(application);
        fbTessereDao = new FB_Tessere_Dao();
        //AllUtenti get any time the same result
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         ISCRIZIONE NUOVO UTENTE                                                */
/*================================================================================================*/
    public void insert(Utenti utenti,List<Tessere> listaTessere){
        fbTessereDao.insert(utenti,listaTessere);
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
    /*public LiveData<List<Tessere>> getByIdOfUtente(Utenti utenti){
        return tessereDao.getByIdOfUtente_Synchronized(utenti.getId());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE  INTO SPECIFIC DAY                             */
/*================================================================================================*/
 /*   public  LiveData<List<Tessere>> getByIdOfUtente(Utenti utenti, Date date){
        return tessereDao.getByIdOfUtente_Synchronized(utenti.getId(),date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE TODAY                                          */
/*================================================================================================*/
    public LiveData<List<Tessere>> getByIdOfUtenteAtToday(Utenti utenti){
        return tessereDao.getByIdOfUtenteAtToday_Synchronized(utenti.getId());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getByIdOfUtenteAtToday(Long idUtenti){
        return tessereDao.getByIdOfUtenteAtToday_Synchronized(idUtenti);
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET TESSERE ATTIVE by ID UTENTE                                 */
/*================================================================================================*/
    public LiveData<List<Tessere>> getActiveByIdOfUtente(Utenti utenti){
        return tessereDao.getActiveByIdOfUtente_Synchronized(utenti.getId());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getActiveByIdOfUtente(Long idUtenti){
        return tessereDao.getActiveByIdOfUtente_Synchronized(idUtenti);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE  INTO SPECIFIC DAY                             */
/*================================================================================================*/
    /*  public LiveData<List<Tessere>> getActiveByIdOfUtente(Utenti utenti, Date date){
        return tessereDao.getActiveByIdOfUtente_Synchronized(utenti.getId(),date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE TODAY                                          */
/*================================================================================================*/
    /*  public LiveData<List<Tessere>> getActiveByIdOfUtenteAtToday(Utenti utenti){
        return tessereDao.getActiveByIdOfUtenteAtToday_Synchronized(utenti.getId());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET TESSERE ATTIVE by ID UTENTE TODAY                           */
/*================================================================================================*/
    public LiveData<List<Tessere>> getActiveByIdOfUtenteAtToday(Utenti utenti){
        return tessereDao.getActiveByIdOfUtenteAtToday_Synchronized(utenti.getId());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getActiveByIdOfUtenteAtToday(Long idUtenti){
        return tessereDao.getActiveByIdOfUtenteAtToday_Synchronized(idUtenti);
    }
/*================================================================================================*/
/*################################################################################################*/
}
