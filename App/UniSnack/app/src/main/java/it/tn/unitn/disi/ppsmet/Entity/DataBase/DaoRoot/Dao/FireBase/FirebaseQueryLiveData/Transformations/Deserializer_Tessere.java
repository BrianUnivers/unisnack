package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List_In_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

public class Deserializer_Tessere implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto Tessere.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String LOG_TAG = "Tessere_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "Tessere" dai dati passati da "DataSnapshot"
 * ed dai dati passati "Universita" e "Corso". Questo perchè tali informazioni devono essere
 * reperite in un primo momento poichè i dati persenti nel risultato della query non li contengono.
 * <br>
 * <PRE>
 *"Utenti_del_Corso_del_Universita" : [
 *  "$id_universita":{
 *      "Corsi" : [
 *          "$id_corsi" :{
 *              "Iscritti" : [
 *                  "$id_universita-id_corsi-id_utente" : {
 *                      "nome"      : "Nome",
 *                      "cognome"   : "Cognome",
 *                      "nato"      : "01/10/2000",
 *                      "email"     : "address@email.com",
 *                      "hashed-psw"    : "YGYGYJVHGYG",
 *                      "salt"          : "HJHGKGKUSDL",
 *                      "email_hashed-psw" : "address@email.com_YGYGYJVHGYG",
 *                      "urlImmagine"   : "url",
 *                      "matricola"     : "123456",
 *                      "statoTessera"  : "true",
 *                      "urlQRcode"     : "Url",
 *                      "urlCodiceABarre"   : "Url",
 *                      "AnnoAccademico" : {
 *                          "$id_AnnoAccademico" : {
 *                              "dataInizio" : "12/09/2021",
 *                              "dataFine"   : "11/09/2022"
 *                              }
 *                          }
 *                      }
 *                  ]
 *              }
 *          ]
 *      }
 *  ]
 * </PRE>
 * @param universita   Questo parametro contiene l'universita a cui si è iscritto l'utente.
 * @param corso        Questo parametro contiene il corso a cui si è iscritto l'utente.
 * @param data         Questo parametro contiene una data che se devera da null, deve essere tra quella
 *                     d'inizio e di fine del anno accademico a qui l'utente è scritto, altrimenti tale
 *                     risultato sara da non considerere. In quest'ultimo caso il risultato è null;
 * @param statoTessera Questo parametro se diverso da null contiene lo stato che deve avere la tessera
 *                     per essere accettata. Se null, tutte le tessere saranno considerate valide.
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "Tessere".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo oppure la data,se diversa
 *          da null, passata come parametro non è contenuta all'interno dell'intervallo dell'anno
 *          accademico, il risultato sarà null.
 */
    public static Tessere estraiDaDataSnapshot(DataSnapshot dataSnapshot, Universita universita, Corsi corso, Date data,Boolean statoTessera){
        Tessere tessere = null;
        try{
            //Verifica campi not null
            if((dataSnapshot.exists()) && (universita!=null)){
                AnnoAccademico annoAccademico = Deserializer_AnnoAccademico.getAttributeOf(dataSnapshot,L_ANNO_ACCADEMICO);
                //Verifica campi not null
                if((annoAccademico!=null)){
                    if( (data==null ) || (annoAccademico.dataNelAnnoAccademico(data))){
                        Boolean statoTesseraAttuale = dataSnapshot.child(P_STATO_TESSERA).getValue(Boolean.class);
                        if(statoTesseraAttuale!=null){
                            if ((statoTessera==null) || (statoTessera==statoTesseraAttuale)){
                                //Caso particolare
                                Integer matricola = null;
                                if(dataSnapshot.child(P_MATRICOLA).exists()){
                                    matricola = Integer.parseInt( dataSnapshot.child(P_MATRICOLA).getValue(String.class) );
                                }
                                //Comune
                                String urlCodiceABarre = dataSnapshot.child(P_URL_CODICE_A_BARRE).getValue(String.class);
                                String urlQRcode = dataSnapshot.child(P_URL_QRCODE).getValue(String.class);
                                tessere = new Tessere(universita,annoAccademico,corso,matricola,statoTesseraAttuale,urlQRcode,urlCodiceABarre);
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
            tessere = null;
        }
        return tessere;
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "Tessere" dai dati passati da "DataSnapshot".
 * <br>
 *  <PRE>
 *"UniversitaETessere_del_Utente" : [
 *  "$id_universita-id_corsi-id_utente" : {
 *      "Partecipa_Universita" : [
 *          "$id_universita" : {
 *              "nome"  : "Università di Trento",
 *              "sigla" : "Unitn",
 *              "Tessere" : [
 *                  "$id_tessera" : {
 *                      "statoTessera"      : "true",
 *                      "urlQRcode"         : "Url",
 *                      "urlCodiceABarre"   : "Url",
 *                      "AnnoAccademico" : {
 *                          "£id_AnnoAccademico" : {
 *                              "dataInizio" : "12/09/2021",
 *                              "dataFine"   : "11/09/2022"
 *                              }
 *                          }
 *                      }
 *                  ]
 *              }
 *          ]
 *      }
 *  ]
 * </PRE>
 * @param data         Questo parametro contiene una data che se devera da null, deve essere tra quella
 *                     d'inizio e di fine del anno accademico a qui l'utente è scritto, altrimenti tale
 *                     risultato sara da non considerere. In quest'ultimo caso il risultato è null;
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "Tessere".
 * @param statoTessera Questo parametro se diverso da null contiene lo stato che deve avere la tessera
 *                     per essere accettata. Se null, tutte le tessere saranno considerate valide.
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo oppure la data,se diversa
 *          da null, passata come parametro non è contenuta all'interno dell'intervallo dell'anno
 *          accademico, il risultato sarà null.
 */
    public static List<Tessere> estraiDaDataSnapshot(DataSnapshot dataSnapshot, Date data,Boolean statoTessera){
        List<Tessere> ris = new LinkedList<>();
        try{
            if(dataSnapshot.exists()){
                Universita universita = Deserializer_Universita.estraiDaDataSnapshot(dataSnapshot,data);
                if(universita!=null){
                    Iterator<DataSnapshot> iterator = dataSnapshot.child(L_TESSERE).getChildren().iterator();
                    while (iterator.hasNext()){
                        Tessere tessera = estraiDaDataSnapshot(iterator.next(),universita,null,data,statoTessera);
                        if(tessera!=null){
                            ris.add(tessera);
                        }
                    }
                }
            }
        }catch (Exception e){
            Log.d(LOG_TAG, "1) Immposibile costruire un oggetto da un possibile risultato.",e);
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, Tessere>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Object extends GenericDeserializer_Object<Tessere> {
    private Corsi      corso;
    private Universita universita;
    private Date       date =null;
    private Boolean    statoTessera =null;
/*================================================================================================*/
/*                         COSTRUTTORE                                                            */
/*================================================================================================*/
    public Deserializer_Object(Universita universita, Corsi corso) {
        super();
        this.corso=corso;
        this.universita= universita;
    }
/*------------------------------------------------------------------------*/
    public Deserializer_Object(Universita universita, Corsi corso, Date date) {
        super();
        this.corso=corso;
        this.universita= universita;
        this.date= date;
    }
/*------------------------------------------------------------------------*/
    public Deserializer_Object(Universita universita,Corsi corso,  Boolean statoTessera) {
        this.corso = corso;
        this.universita = universita;
        this.statoTessera = statoTessera;
    }
/*------------------------------------------------------------------------*/
    public Deserializer_Object(Universita universita, Corsi corso,  Date date, Boolean statoTessera) {
        this.corso = corso;
        this.universita = universita;
        this.date = date;
        this.statoTessera = statoTessera;
    }
/*================================================================================================*/
        @Override
        public Tessere estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Tessere.estraiDaDataSnapshot(dataSnapshot,this.universita,this.corso,this.date,this.statoTessera);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<Tessere>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List_In_List<Tessere> {
    private Date       date =null;
    private Boolean    statoTessera =null;
/*================================================================================================*/
/*                         COSTRUTTORE                                                            */
/*================================================================================================*/
    public Deserializer_List() {
        super();
    }
/*------------------------------------------------------------------------*/
    public Deserializer_List( Date date) {
        super();
        this.date= date;
    }
/*------------------------------------------------------------------------*/
    public Deserializer_List(Boolean statoTessera) {
        this.statoTessera = statoTessera;
    }
/*------------------------------------------------------------------------*/
    public Deserializer_List(Date date, Boolean statoTessera) {
        this.date = date;
        this.statoTessera = statoTessera;
    }
/*================================================================================================*/
        @Override
        public List<Tessere> estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Tessere.estraiDaDataSnapshot(dataSnapshot,this.date,this.statoTessera);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
