package it.tn.unitn.disi.ppsmet.ActivityManager.ProfiloUtenteActivity;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.InterfaceViewModel.MenuProfiloUtenteViewModel;
import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Allergeni_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.IdentificativiSessioni_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Pietanze_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Tessere_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Utenti_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class ProfiloUtenteViewModel extends MenuProfiloUtenteViewModel {
/*################################################################################################*/
    private Utenti_Repository    rUtenti;
    private Tessere_Repository   rTessere;
    private Allergeni_Repository rAllergeni;
/*================================================================================================*/
/*                                  COSTRUTTORE VIEW MODEL                                        */
/*================================================================================================*/
    /**
     * Questo metodo costruttore del viewmode inizializza le repository che si interfacciano con entrambi
     * i database sia interno che estreno a seconda delle operazioni che vengono richeste. Che verranno
     * utilizate dall'activity e i suoi fragment.
     * @param application
     */
    public ProfiloUtenteViewModel(@NonNull Application application) {
        super(application);
        rUtenti    = new Utenti_Repository(application);
        rTessere   = new Tessere_Repository(application);
        rAllergeni = new Allergeni_Repository(application);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  AZZIONI LEGATE ALLA GESITIONE DEI DATABASE                    */
/*################################################################################################*/
/*================================================================================================*/
/*                                  GET LISTA TESSERE VALIDE DEL UTENTE CON ID                    */
/*================================================================================================*/
    /**
     * Questo metodo permette di richiedere al database la lista di tessere,sia attive che disattive
     * presenti nel anno accademico correte di un sigolo utente specificato tramite l'id passato come
     * parametro.
     * @param idUtente
     * @return
     */
    public LiveData<List<Tessere>> getListaTessereAACorrente(Long idUtente){
        return rTessere.getByIdOfUtenteAtToday(idUtente);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  GET LISTA ALLEGENI                                            */
/*================================================================================================*/
    /**
     * Questo metodo permette di richiedere al database la lista completa dei allergeni presenti.
     * @return
     */
    public LiveData<List<Allergeni>> getAllAllergeni(){
        return rAllergeni.getAll();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  GET LISTA ALLEGENI                                            */
/*================================================================================================*/
    /**
     * Questo metodo permette di ricevere la lista dei allergei a cui è intollerante o allergico
     * l'utente selezionato tramite l'id passato come parameto.
     * @param idUtente
     * @return
     */
    public LiveData<List<Allergeni>> getAllergeniByIdOfUtente(Long idUtente){
        return rAllergeni.getByIdOfUtente(idUtente);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  SOSTITUISCI GLI ALLERGENI UTENTI CON QUESTA LISTA             */
/*================================================================================================*/
    /**
     * Questo metodo riceve la lista di allergeni inseriti dal utente e l'insieme dei vecchi aallergeni
     * a cui l'utente è allergico. Questo perchè verranno inseriti nel database solo gli elemeni non
     * presenti e rimossi quelli non più presenti nella lista passata.
     * Inoltre questo metodo si occupa di renre la lista pasata senza ripetizioni.
     * @param utenti
     * @param listaAllergeni
     * @param vecchiaListaAllergeni
     */
    public void insertAllergia(Utenti utenti, List<Allergeni> listaAllergeni,Set<Allergeni> vecchiaListaAllergeni){
        if((utenti!=null) && (utenti.getId()!=null) && (listaAllergeni!=null) &&(vecchiaListaAllergeni!=null)){
            Set<Allergeni> setAllergeni = new HashSet<>();
            setAllergeni.addAll(listaAllergeni);//Eliminazione dei duplicati
            List<Allergeni> listaAllergeniSenzaDuplicati = new LinkedList<>();
            listaAllergeniSenzaDuplicati.addAll(setAllergeni);
            //Selsziona elementi da camcellare
            List<Allergeni> elementiDaCancellare = new LinkedList<>();
            elementiDaCancellare.addAll(vecchiaListaAllergeni);
            elementiDaCancellare.removeAll(listaAllergeniSenzaDuplicati);
            //Selezioni elementi da inserire
            List<Allergeni> elementiDaInserire = new LinkedList<>();
            elementiDaInserire.addAll(listaAllergeniSenzaDuplicati);
            elementiDaInserire.removeAll(vecchiaListaAllergeni);
            //Fa azzioni con il database
            rAllergeni.deleteListAllergeniByIdOfUtente(utenti,elementiDaCancellare);
            rAllergeni.insertAllergia(utenti,listaAllergeniSenzaDuplicati);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
