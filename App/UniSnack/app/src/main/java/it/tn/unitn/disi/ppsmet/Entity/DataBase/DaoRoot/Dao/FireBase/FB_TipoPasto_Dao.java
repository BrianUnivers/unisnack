package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.google.firebase.database.DatabaseReference;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_TipoMenu;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_TipoPasto;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;


public class FB_TipoPasto_Dao extends ConnesioneAFirebase<TipoPasto> {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<TipoPasto>> getAll_OneTime(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_TIPO_PASTO);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_TipoPasto.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<TipoPasto>> getAll(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_TIPO_PASTO);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_TipoPasto.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<TipoPasto>> getAll_Synchronized(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_TIPO_PASTO);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_TipoPasto.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    public LiveData<TipoPasto> getByPrimaryKey_OneTime(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_TIPO_PASTO)
                .child(id.toString());
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_TipoPasto.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<TipoPasto> getByPrimaryKey(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_TIPO_PASTO)
                .child(id.toString());
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_TipoPasto.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<TipoPasto> getByPrimaryKey_Synchronized(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_TIPO_PASTO)
                .child(id.toString());
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_TipoPasto.Deserializer_Object());
    }
/*================================================================================================*/
/*################################################################################################*/
}

