package it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Ignore;

import java.util.Objects;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class IscrizioniUtente {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @Embedded(prefix = "Uni_")
    public Universita universita;
    @NonNull
    @Embedded(prefix = "AA_")
    public AnnoAccademico annoAccademico;
    @Embedded(prefix = "C_")
    public Corsi corsi = null;
    @ColumnInfo(name = "matricola")
    public Integer matricola = null;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    public IscrizioniUtente(@NonNull Universita universita,
                            @NonNull AnnoAccademico annoAccademico) {
        this.universita = universita;
        this.annoAccademico = annoAccademico;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public IscrizioniUtente(@NonNull Universita universita,
                            @NonNull AnnoAccademico annoAccademico,
                            Corsi corsi,
                            Integer matricola) {
        this.universita = universita;
        this.annoAccademico = annoAccademico;
        this.corsi = corsi;
        this.matricola = matricola;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    @NonNull
    public Universita getUniversita() {
        return universita;
    }
//--------------------------------------
    public void setUniversita(@NonNull Universita universita) {
        this.universita = universita;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public AnnoAccademico getAnnoAccademico() {
        return annoAccademico;
    }
//--------------------------------------
    public void setAnnoAccademico(@NonNull AnnoAccademico annoAccademico) {
        this.annoAccademico = annoAccademico;
    }
//--------------------------------------------------------------------------------------------------
    public Corsi getCorsi() {
        return corsi;
    }
//--------------------------------------
    public void setCorsi(Corsi corsi) {
        this.corsi = corsi;
    }
//--------------------------------------------------------------------------------------------------
    public Integer getMatricola() {
        return matricola;
    }
//--------------------------------------
    public void setMatricola(Integer matricola) {
        this.matricola = matricola;
    }
//==================================================================================================
//##################################################################################################
//==================================================================================================
//                                      EQUALS AND HASHCODE
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IscrizioniUtente)) return false;
        IscrizioniUtente that = (IscrizioniUtente) o;
        return getUniversita().equals(that.getUniversita()) &&
                getAnnoAccademico().equals(that.getAnnoAccademico()) &&
                Objects.equals(getCorsi(), that.getCorsi()) &&
                Objects.equals(getMatricola(), that.getMatricola());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(getUniversita(), getAnnoAccademico(), getCorsi(), getMatricola());
    }

//==================================================================================================
//##################################################################################################
}
