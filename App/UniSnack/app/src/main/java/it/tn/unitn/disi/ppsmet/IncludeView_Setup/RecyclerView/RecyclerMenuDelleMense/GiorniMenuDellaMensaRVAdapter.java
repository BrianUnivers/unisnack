package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerMenuDelleMense;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.Image;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.Fragment.Interface.OnFragmentMenuDelleMenseNelGiornoEventListener;
import it.tn.unitn.disi.ppsmet.GestioneAssetPerFoto.GestioneFotoDaAsset;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.GiornoSelezionatoRowView;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ShapedImageView;
import it.tn.unitn.disi.ppsmet.R;

public class GiorniMenuDellaMensaRVAdapter extends RecyclerView.Adapter<GiorniMenuDellaMensaRVAdapter.MyViewHoder> {
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE INTERNA PADRE                                              */
/*################################################################################################*/
    /**
     * Questa classe è quella padre di quelle che vengono usate realmente che permettono alla RecyclingView
     * Adapter di definire un layout custom del singolo elemeto,in due formati distini.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public ViewGroup v_mainLayout;
        public ImageView v_frecciaLaterale;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout      = itemView.findViewById(R.id.cly_MenuDelGiono);
            v_frecciaLaterale = itemView.findViewById(R.id.img_Freccia);
        }
/*================================================================================================*/
    }
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE INTERNA FIGLIO GENERICO                                    */
/*################################################################################################*/
    /**
     * Questa classe è quella che verrà usata realmente, che rappresentano i gioni semplici in modo
     * tale che il RecyclingView  Adapter di definire un layout custom del singolo elemeto.
     */
    protected class MyViewHoderGionoGenerico extends MyViewHoder  {
        public TextView numeroDelGiorno;
        public TextView abbreviazioneMese;
        public TextView gionroSettimana;
/*================================================================================================*/
//                              COSTRUTTORI
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoderGionoGenerico(@NonNull View itemView) {
            super(itemView);
            numeroDelGiorno   = itemView.findViewById(R.id.txt_NumeroGiorno);
            abbreviazioneMese = itemView.findViewById(R.id.txt_AbbreviazioneMese);
            gionroSettimana   = itemView.findViewById(R.id.txt_GiornoDellaSettimana);
        }
/*================================================================================================*/
    }
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE INTERNA FIGLIO OGGI                                        */
/*################################################################################################*/
    /**
     * Questa classe è quella che verrà usata realmente, che rappresentano il giorno attuale in modo
     * tale che il RecyclingView  Adapter di definire un layout custom del singolo elemeto.
     */
    protected class MyViewHoderOggi extends MyViewHoder  {
        public ImageView imgSinistra;
        public ImageView imgDestra;
        public ShapedImageView imgCentrale;
        public TextView gionroAttuale;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoderOggi(@NonNull View itemView) {
            super(itemView);
            imgSinistra     = itemView.findViewById(R.id.img_Sinistra);
            imgDestra       = itemView.findViewById(R.id.img_Destra);
            imgCentrale     = itemView.findViewById(R.id.img_Centrale);
            gionroAttuale   = itemView.findViewById(R.id.txt_GiornoAttuale);
        }
/*================================================================================================*/
    }
/*################################################################################################*/
/*################################################################################################*/



/*################################################################################################*/
/*                              CLASSE PRINCIPALE GiorniMenuDellaMensaRVAdapter                   */
/*################################################################################################*/
    private static final int GIONO_SEMPLICE = 0;
    private static final int GIONO_OGGI     = 2;
/*------------------------------------------------------------------------------------------------*/
    private GestioneFotoDaAsset ACCEDI_FOTO = null;
/*------------------------------------------------------------------------------------------------*/
    private Context comtext=null;
    private List<Date> listaDate = null;
    private List<String> listaUrlImmage=null;
    private OnFragmentMenuDelleMenseNelGiornoEventListener listener = null;
    /*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public GiorniMenuDellaMensaRVAdapter(Context comtext,
                                         List<Date> listaDate,
                                         List<String> listaUrlImmage,
                                         OnFragmentMenuDelleMenseNelGiornoEventListener listener) {
        this.comtext = comtext;
        this.listaDate = listaDate;
        this.listaUrlImmage = listaUrlImmage;
        this.ACCEDI_FOTO = new GestioneFotoDaAsset(comtext);
        this.listener= listener;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              SELEZIONE DEL TIPO DI OGGETTO DA RAPPRESENTARE                    */
/*================================================================================================*/
    /**
     * Questo modtodo permette di individuare se il giono che si vule visualizzare deve essre rappresentato
     * in un modo o in un altro.
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        Date data = listaDate.get(position);
        int ris = GIONO_SEMPLICE;
        if(isSameDay(data,new Date())){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(data);
            if((calendar.get(Calendar.DAY_OF_WEEK)!=Calendar.SATURDAY) &&
                    (calendar.get(Calendar.DAY_OF_WEEK)!=Calendar.SUNDAY)){
                ris = GIONO_OGGI;
            }
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo permette di sapere se due date sono nello stesso giorno.
     * @param date1
     * @param date2
     * @return
     */
    public static boolean isSameDay(Date date1, Date date2) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        return fmt.format(date1).equals(fmt.format(date2));
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              CERAZIONE DEL SINGOLO ELEMETOTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali. Questo lo fa tenendo conto del tipo di "viewType", infatti
     * in esso è contenuta l'infoemazione per distigure le varie rappresentazioni.
     */
    @NonNull
    @Override
    public MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(comtext);
        MyViewHoder myViewHoder;
        switch (viewType) {
            case GIONO_OGGI:{
                View bloccoElemeto = inflater.inflate(R.layout.row_oggi_menu_della_mensa, parent,false);
                myViewHoder = new MyViewHoderOggi(bloccoElemeto);
                break;
            }
            case GIONO_SEMPLICE:
            default:{
                View bloccoElemeto = inflater.inflate(R.layout.row_giorni_menu_della_mensa, parent,false);
                myViewHoder = new MyViewHoderGionoGenerico(bloccoElemeto);
                break;
            }
        }

        return myViewHoder;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              INSERIRE L'ELEMETO NEL LAYOUT                                     */
/*================================================================================================*/
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore GiorniMenuDellaMensaRVAdapter.
     * Inoltre aggiunge la possibilità di premere sul singolo oggeto così da aprire un altro frammeento.
     */
    @Override
    public void onBindViewHolder(@NonNull MyViewHoder holder, int position) {
        final Date date =listaDate.get(position);
        //Seleziona il tipo di layout
        if(holder instanceof MyViewHoderGionoGenerico){
            inserisciGionoGenericoLayout((MyViewHoderGionoGenerico)holder, date);
        }else if(holder instanceof MyViewHoderOggi){
            inserisciOggiLayout((MyViewHoderOggi)holder, date);
        }

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  IMPOSTA COME BOTTONE APERTURA FRAGMENT MENU
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if((calendar.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY) ||
                (calendar.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY)){
            //Elemento NON cliccabile
            holder.v_mainLayout.setOnClickListener(null);
        }else{
            //Elemento cliccabile
            holder.v_mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null){
                        listener.apriMenuDelGiorno(date);
                    }
                }
            });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }
    }
/*================================================================================================*/
/*                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE                  */
/*================================================================================================*/
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaDate.size();
    }
/*================================================================================================*/


/*================================================================================================*/
/*                              INSERISCI I DATI NEL LAYOUT                                       */
/*================================================================================================*/
    /**
     * Questo metodo si occupa di inserire i dati del giono semplice nel layout del singolo elemeto.
     */
    private void inserisciGionoGenericoLayout(@NonNull MyViewHoderGionoGenerico holder, Date date){
        GiornoSelezionatoRowView row =
                new GiornoSelezionatoRowView(holder.v_mainLayout,
                                             holder.numeroDelGiorno,
                                             holder.abbreviazioneMese,
                                             holder.gionroSettimana,
                                             holder.v_frecciaLaterale,date);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if((calendar.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY) ||
                (calendar.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY)){
            row.setStatoAttivo(false);
        }else{
            row.setStatoAttivo(true);
        }
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo si occupa di inserire i dati del giorno che è quello attuale  nel layout del
     * singolo elemeto.
     */
    private void inserisciOggiLayout(@NonNull MyViewHoderOggi holder, Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String dataString = new StringBuffer()
                .append(new SimpleDateFormat("EEEE", Locale.ITALY).format(date.getTime())).append("\n")
                .append(calendar.get(Calendar.DAY_OF_MONTH)).append(" ")
                .append(new SimpleDateFormat("MMMM", Locale.ITALY).format(date.getTime())).toString();
        holder.gionroAttuale.setText(dataString);


        //Gestione Immaginei
        Bitmap nessunaPietanza = BitmapFactory.decodeResource(comtext.getResources(), R.drawable.img_nessun_piatto);
        //Imposto l'immagine sinistra
        if(listaUrlImmage.size()>=1){
            holder.imgSinistra.setImageBitmap(ACCEDI_FOTO.getFoto(listaUrlImmage.get(0),R.drawable.img_nessun_piatto));
        }
        //Imposto l'immagine centrale
        if(listaUrlImmage.size()>=2){
            Bitmap immaginePietanza = ACCEDI_FOTO.getFoto(listaUrlImmage.get(1),R.drawable.img_nessun_piatto);
            if(immaginePietanza!=null){
                holder.imgCentrale.setImageOnShapeMask(immaginePietanza);
            }else{
                holder.imgCentrale.setImageOnShapeMask(nessunaPietanza);
            }
        }else{
            holder.imgCentrale.setImageOnShapeMask(nessunaPietanza);
        }
        //Imposto l'immagine destra
        if(listaUrlImmage.size()>=3) {
            holder.imgDestra.setImageBitmap(ACCEDI_FOTO.getFoto(listaUrlImmage.get(2),R.drawable.img_nessun_piatto));
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}