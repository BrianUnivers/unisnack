package it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.MenuDelleMenseViewModel;
import it.tn.unitn.disi.ppsmet.ActivityManager.ProfiloUtenteActivity.ProfiloUtenteViewModel;
import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.InterfaceViewModel.MenuProfiloUtenteViewModel;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;
import it.tn.unitn.disi.ppsmet.GestioneAssetPerFoto.GestioneFotoDaAsset;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ShapedImageView;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.TitoloSopraContenutoView;
import it.tn.unitn.disi.ppsmet.R;


public class MenuMenuDelleMenseFragment extends Fragment {
/*################################################################################################*/
/*                                  INIZIALIZZAZIONE VALORI                                       */
/*################################################################################################*/
    private MenuDelleMenseViewModel viewModel    = null;
    private LiveData<Mense> mensaSelezionata = null;
/*================================================================================================*/
/*                                  COSTRUTTORE                                                   */
/*================================================================================================*/
    /**
     * Questo è il metodo costruttore del fragment.
     */
    public MenuMenuDelleMenseFragment()  {
        super();
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private ViewGroup                rootView;
/*------------------------------------------------------------------------*/
    private TitoloSopraContenutoView titolo;
/*------------------------------------------------------------------------*/
    private Button apriGuidaMappa;
    private Button apriPosizioneMappa;
    private FloatingActionButton apriMenuMappa;
/*------------------------------------------------------------------------*/
    private String guidaVerso    = null;
    private String apriPosizione = null;
/*------------------------------------------------------------------------*/
    private boolean visionePulsanti=false;
    //Bottoni animati
    private Animation animOpen;
    private Animation animClose;
/*================================================================================================*/
/*                                  CREAZIONE DELL FRAGMENT                                       */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'inizializazione del layout del fragment ed inoltre contiene le loro
     * possibili interazioni con l'utente.
     * In aggiunta viene richesto un modelView che è quello usato dall'activity in modo tale da accedere
     * alla sua stessa istanza, in modo tale da utilizarlo come metodo per passare parameti da una parte
     * all'altra.
     *
     * In questo caso viene usato per ricevere come informazione della mensa  selezionata.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_menu_menu_delle_mense, container, false);
/*------------------------------------------------------------------------*/
        try {
            viewModel = new ViewModelProvider(getActivity()).get(MenuDelleMenseViewModel.class);
        }catch (Exception e){
            viewModel=null;
        }
/*------------------------------------------------------------------------*/
        //Animazioni
        animOpen = AnimationUtils.loadAnimation(getActivity(),R.anim.fab_open);
        animClose = AnimationUtils.loadAnimation(getActivity(),R.anim.fab_close);
/*------------------------------------------------------------------------*/
        titolo = TitoloSopraContenutoView.newInstance(  rootView,
                                                        R.id.include_titolo_sopra_contenuto,
                                                        R.string.ttl_MenuDelleMense);
        //Pulsanti
        apriMenuMappa      = rootView.findViewById(R.id.btn_ApriMenuMappa);
        apriPosizioneMappa = rootView.findViewById(R.id.btn_PosizioneMappa);
        apriGuidaMappa     = rootView.findViewById(R.id.btn_GuidaMappa);
/*------------------------------------------------------------------------*/
        if(viewModel!=null){
            richiediDatiEInserisci();
            impostaAzzionePulsanteMenuMappa();
            impostaAzzionePulsanteAperturaMappaInUnPunto();
            impostaAzzionePulsanteAperturaNavigatoreVersoUnPunto();
        }
/*------------------------------------------------------------------------*/
        return rootView;
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  ADATTA AL UTENTE                                              */
/*################################################################################################*/
/*================================================================================================*/
/*                                  RICHIEDI UTENTE E ADATTA I DATO                               */
/*================================================================================================*/
    /**
     * Qusto metodo riceve e cambia eventualmente i dati del fragmento in base all'aggiornamento
     * del dato mensa selezionata che qualcuno(l'activity che gestisce il menu delle mense) ha selezionato
     * e caricato nella viewmodel.
     * Infatti con questo metodo si definisce un observer per riceve un aggiornamento dei dati della
     * mesa così che venga poi adattata la pagina.
     */
    private void richiediDatiEInserisci(){
        mensaSelezionata = viewModel.getMensaSelezzionata();
        mensaSelezionata.observe(getViewLifecycleOwner(), new Observer<Mense>() {
            @Override
            public void onChanged(Mense mense) {
                    impostaDatiMensa(mense);

            }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA I DATI UTENTI NEL LAYOUT                              */
/*================================================================================================*/
    /**
     * Questo metodo riceve un oggetto mensa per poi verificare che non sia null per poi eventualmente
     * modificare l'etichette dei pulsanti che aprono la mappae ed le sringnhe che contengono i
     * parametri per gli intent in modo tale da specificare le azioni da far
     * @param mense
     */
    private void impostaDatiMensa(Mense mense){
        if(mense!=null){
            apriPosizioneMappa.setText("Vai a "+mense.getSigla());
            apriGuidaMappa.setText("Guida "+mense.getSigla());
            if((mense.getLatitudine()!=null) && (mense.getLongitudine()!=null)){
                //Dato per aprire il navigatore verso la mensa
                guidaVerso = new StringBuffer("google.navigation:q=")
                        .append(mense.getLatitudine()).append(",")
                        .append(mense.getLongitudine()).toString();
                //Dato per aprire la mappa nel luogo della mensa
                apriPosizione = new StringBuffer("geo:")
                        .append(mense.getLatitudine()).append(",")
                        .append(mense.getLongitudine()).append("?z=30").toString();
            }else{
                guidaVerso    = null;
                apriPosizione = null;
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE PUSANTI DELLA MAPPA                                  */
/*################################################################################################*/
/*================================================================================================*/
/*                                  GESTIONE VISIONE PULSANTI MAPPA                               */
/*================================================================================================*/
    /**
     * Questo metodo si occupa di far apparire e scomparire i pulsanti relativi alla mappa associati
     * alle mensa selezionata. In questo modo sia premendo sul taso princiaple e sia su uno dei due
     * che effetuano le vere e proprie operazioni, il menu verra aperto o chiuso all'occorenza.
     */
    private void buttonCambiaVisionePulsanti(){
        visionePulsanti=!visionePulsanti;
        if(!visionePulsanti){//é vero se FALSO !!
            apriMenuMappa.setImageResource(R.drawable.ic_baseline_map_24);//Icona
            apriGuidaMappa.startAnimation(animClose);
            apriPosizioneMappa.startAnimation(animClose);
            apriGuidaMappa.setClickable(false);
            apriPosizioneMappa.setClickable(false);
        }else{
            // Voglio aprire il menu di scelta
            apriMenuMappa.setImageResource(R.drawable.ic_baseline_cancel_24);//Icona
            apriGuidaMappa.startAnimation(animOpen);
            apriPosizioneMappa.startAnimation(animOpen);
            apriGuidaMappa.setClickable(true);
            apriPosizioneMappa.setClickable(true);
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  BOTTONE APERURA E CHIUSURA MENU MAPPA                         */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'impostazione del listener per il pulsante che si ocupa solo dell'apertura
     * e chiusaura del menu per la mappa.
     */
    private void impostaAzzionePulsanteMenuMappa(){
        apriMenuMappa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonCambiaVisionePulsanti();
            }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  BOTTONE APRI MAPPA IN UN PUNTO                                */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'impostazione del listenr per il pulsante che si occupa di chiamare
     * una applicazione esterna per aprire, in un punto specifico la mappa. Così da mostrare la
     * posizione della mensa.
     */
    private void impostaAzzionePulsanteAperturaMappaInUnPunto(){
        apriPosizioneMappa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(guidaVerso!=null){
                    Uri gmmIntentUri = Uri.parse(apriPosizione);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW );
                    mapIntent.setData(gmmIntentUri);
                    startActivity(mapIntent);
                }else{
                    Toast.makeText(rootView.getContext(), R.string.msg_ImpossibileAprireMappa, Toast.LENGTH_LONG).show();
                }
                buttonCambiaVisionePulsanti();
            }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  BOTTONE APRI NAVIGATORE VERSO UN PUNTO                        */
/*================================================================================================*/
    /**
     * Questo  metodo contiene l'impostazione del listenr per il pulsante che si occupa di chiamare
     * una applicazione esterna, per aprire un navigatore preimpostato verso un punto specifico.
     * Così da aprire un navigatore verso la mensa selezionata.
     */
    private void impostaAzzionePulsanteAperturaNavigatoreVersoUnPunto(){
        apriGuidaMappa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(guidaVerso!=null){
                    Uri gmmIntentUri = Uri.parse(guidaVerso);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW );
                    mapIntent.setData(gmmIntentUri);
                    startActivity(mapIntent);
                }else{
                    Toast.makeText(rootView.getContext(), R.string.msg_ImpossibileAprireMappa, Toast.LENGTH_LONG).show();
                }
                buttonCambiaVisionePulsanti();
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
}