package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;

import com.google.firebase.database.DatabaseReference;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;


public class FB_Allergeni_Dao extends ConnesioneAFirebase<Allergeni>  {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/

    public Future<Long> insertAllergia(@NonNull Utenti utenti, @NonNull Allergeni allergeni ){
       List<Allergeni> list = new LinkedList<>();
       list.add(allergeni);
       return insertAllergia(utenti,list);
    }
/*------------------------------------------------------------------------*/
    public Future<Long> insertAllergia(@NonNull Utenti utenti,@NonNull List<Allergeni> listaAllergeni){
        ExecutorService executor  = Executors.newSingleThreadExecutor();
        return executor.submit(() -> {
            Long ris= 0L;
            Iterator<Allergeni> iterator = listaAllergeni.iterator();
            while (iterator.hasNext()){
                Allergeni allergeni = iterator.next();
                if(super.getReferenceFireBase().getReference()
                        .child(L_INTOLLERANZE_DEL_UTENTE)
                        .child(builderKeyFromUtenti(utenti))
                        .child(L_INTOLLERANTE)
                        .child(allergeni.getId().toString()).setValue(allergeni).isSuccessful()){
                    ris++;
                }
            }
            return ris;
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE by ID UTENTE                                             */
/*================================================================================================*/
    public void deleteAllAllergeni(@NonNull Utenti utenti){
        super.getReferenceFireBase().getReference()
                .child(L_INTOLLERANZE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_INTOLLERANTE).removeValue();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE by ID UTENTE AND ALLERGENI                               */
/*================================================================================================*/
    public  void deleteSpecificAllergeniByIdOfUtente(@NonNull Utenti utenti,@NonNull Allergeni allergeni){
        super.getReferenceFireBase().getReference()
                .child(L_INTOLLERANZE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_INTOLLERANTE)
                .child(allergeni.getId().toString()).removeValue();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<Allergeni>> getAll_OneTime(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_ALLERGENI);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Allergeni.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Allergeni>> getAll(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_ALLERGENI);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Allergeni.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Allergeni>> getAll_Synchronized(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_ALLERGENI);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Allergeni.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    public LiveData<Allergeni> getByPrimaryKey_OneTime(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_ALLERGENI)
                .child(id.toString());
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Allergeni.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Allergeni> getByPrimaryKey(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_ALLERGENI)
                .child(id.toString());
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Allergeni.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
   public LiveData<Allergeni> getByPrimaryKey_Synchronized(@NonNull Long id){
       DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_ALLERGENI)
                                                                          .child(id.toString());
       FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
       return Transformations.map(ris, new Deserializer_Allergeni.Deserializer_Object());
   }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by by ID UTENTE                                             */
/*================================================================================================*/
    public LiveData<List<Allergeni>> getByIdOfUtente_OneTime(@NonNull Utenti utenti){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_INTOLLERANZE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_INTOLLERANTE);
        FirebaseQueryLiveData_OneTime ris= new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Allergeni.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Allergeni>> getByIdOfUtente(@NonNull Utenti utenti){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_INTOLLERANZE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_INTOLLERANTE);
        FirebaseQueryLiveData ris= new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Allergeni.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Allergeni>> getByIdOfUtente_Synchronized(@NonNull Utenti utenti){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_INTOLLERANZE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_INTOLLERANTE);
        FirebaseQueryLiveData_Synchronized ris= new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Allergeni.Deserializer_List());
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         FILTER AND MAINTAIN ONLY EXIST DATA                                    */
/*================================================================================================*/
/**
 * Questo metodo confrona i dati della lista passata con quelli della lista in un determinato momento
 * nel database e restituisce la lista stessa con i soli dati peresenti sul database in quel moento.
 * Eventuali aggiunte succesive alla lista dei valori accetati dal database non influiranno
 * sull'operazione fatta in precedenza.
 *
 * @param listaAllergeni Lista di allergeni di cui si vuole verificare la validità.
 * @return Patre della lita dei allergeni passata come parametro, avente solo i dati  persenti e quindi
 *         ritenuti validi dal database.
 */
    public LiveData<List<Allergeni>>filterOnlyExists(@NonNull List<Allergeni> listaAllergeni){
        MediatorLiveData<List<Allergeni>> ris = new MediatorLiveData<>();

        LiveData<List<Allergeni>> tuttiAllergeni = getAll_OneTime();//Effetua una solo valta
        ris.addSource(tuttiAllergeni, new Observer<List<Allergeni>>() {
            @Override
            public void onChanged(List<Allergeni> insert) {
                listaAllergeni.retainAll(insert);
                ris.setValue(listaAllergeni);
                //ris.removeSource(tuttiAllergeni);//Effetua una sola volta l'aggionamento dei dati
            }
        });
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
}
