package it.tn.unitn.disi.ppsmet.GestioneMyIntent.Interface;

/**
 * Costanti usater per passare i dati da una Activity all'altra tramite gli Intent.
 */
public interface CostantiInserireEscrarreDatiDaiInternt {
//##################################################################################################
    public static String I_UTENTE_ATTIVO                    = "I_UtenteAttivo";
//==================================================================================================
//                              PER I DATI DEL UTENTE
//==================================================================================================
    public static String I_UTENTE_ID                    = "I_Utente_Id";
//==================================================================================================
//##################################################################################################
}
