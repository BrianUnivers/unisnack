package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.google.firebase.database.DatabaseReference;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;


public class FB_Ingredienti_Dao extends ConnesioneAFirebase<Ingredienti> {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<Ingredienti>> getAll_OneTime(){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_INGREDIENTI);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Ingredienti.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Ingredienti>> getAll(){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_INGREDIENTI);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Ingredienti.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Ingredienti>> getAll_Synchronized(){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_INGREDIENTI);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Ingredienti.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    public LiveData<Ingredienti> getByPrimaryKey_OneTime(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_INGREDIENTI)
                .child(id.toString());
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Ingredienti.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Ingredienti> getByPrimaryKey(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_INGREDIENTI)
                .child(id.toString());
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Ingredienti.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Ingredienti> getByPrimaryKey_Synchronized(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_INGREDIENTI)
                .child(id.toString());
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Ingredienti.Deserializer_Object());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID PIETANZA                                              */
/*================================================================================================*/
    public LiveData<List<Ingredienti>> getByIdOfPietanza_OneTime(@NonNull Long idPietanza){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_PIETANZE)
                .child(idPietanza.toString())
                .child(L_INGREDIENTI);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Ingredienti.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Ingredienti>> getByIdOfPietanza(@NonNull Long idPietanza){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_PIETANZE)
                .child(idPietanza.toString())
                .child(L_INGREDIENTI);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Ingredienti.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Ingredienti>> getByIdOfPietanza_Synchronized(@NonNull Long idPietanza){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_PIETANZE)
                .child(idPietanza.toString())
                .child(L_INGREDIENTI);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Ingredienti.Deserializer_List());
    }
/*================================================================================================*/
/*################################################################################################*/
}
