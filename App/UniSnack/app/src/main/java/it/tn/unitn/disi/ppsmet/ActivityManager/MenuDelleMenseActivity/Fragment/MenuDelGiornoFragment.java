package it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.Fragment.Interface.OnFragmentMenuDelleMenseNelGiornoEventListener;
import it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.MenuDelleMenseViewModel;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.GiornoSelezionatoRowView;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerMenuDelGiorno.MenuDelGiornoRVAdapter;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerMenuDelleMense.GiorniMenuDellaMensaRVAdapter;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.TitoloSopraContenutoView;
import it.tn.unitn.disi.ppsmet.R;

public class MenuDelGiornoFragment extends Fragment {
    /**
     * Questo parametro coterrà se sarà possibile l'accesso a determinati metodi dell'acticity contenete
     * il seguete fragment.
     */
    private OnFragmentMenuDelleMenseNelGiornoEventListener listener = null;
/*################################################################################################*/
/*                                  INIZIALIZZAZIONE VALORI                                       */
/*################################################################################################*/
    private static final String ARG_PARAM_DATA_MENU = "Long_Data";
/*================================================================================================*/
/*                                  COSTRUTTORE                                                   */
/*================================================================================================*/
    /**
     * Questo è il metodo costruttore del fragment. Non contene nessun parametro poichè il metodo di
     * inserimento di quest'ultimi verrà fatto tramite i "Bundle".
     */
    public MenuDelGiornoFragment() {
        super();
    }
/*------------------------------------------------------------------------------------------------*/
/*                                  COSTRUTTORE VERI                                              */
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo è il vero e proprio costruttore del fragment che oltre a costruire l'oggetto fragment,
     * permette anche di inserire i parametri nel "Bundle" ad esso assegnato. Così che in seguito
     * li possa eventualmente estarre.
     * @param date
     * @return
     */
    public static MenuDelGiornoFragment newInstance(Date date) {
        MenuDelGiornoFragment fragment = new MenuDelGiornoFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_PARAM_DATA_MENU, date.getTime());
        fragment.setArguments(args);
        return fragment;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
    private Date dataDelMenu = new Date();
/*================================================================================================*/
/*                                  INIZIALIZZATORE DI PARAMETRI                                  */
/*================================================================================================*/
    /**
     * Questo metodo viene chieamato nella fase di costruzione del frugment ed è quello che si occupa
     * di estrarre i dati inseriti nel "Bundle" dal costruttore e li imposta come volori del fragment.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Long dataLong = getArguments().getLong(ARG_PARAM_DATA_MENU);
            if(dataLong!=null){
                dataDelMenu.setTime(dataLong);
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/

/*################################################################################################*/
/*                                  COMUNICAZIONE CON L'ACTIVITY                                  */
/*################################################################################################*/
/*================================================================================================*/
/*                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY              */
/*================================================================================================*/
    /**
     * Questo metodo permete di estrarre un eventuale implementeazione da parte dell'activity(contesto
     * padre) così da poter accedere ad dei metodi esterni al fragmen.
     * @param context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (OnFragmentMenuDelleMenseNelGiornoEventListener) context;
        } catch (ClassCastException castException) {
            listener = null;
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private Fragment fragment = this;
/*------------------------------------------------------------------------*/
    private MenuDelleMenseViewModel viewModel    = null;
/*------------------------------------------------------------------------*/
    private ViewGroup rootView;
    private RecyclerView listaDateView;
    private GiornoSelezionatoRowView giornoSelezionato;
    private RecyclerView listaPietanze;
/*------------------------------------------------------------------------*/
    private LiveData<Mense> ldMensa      = null;
    private LiveData<Long>  ldIdTipoMenu = null;
    private LiveData<List<TipoPietanza>> ldListaTipoPietanza = null;
/*------------------------------------------------------------------------*/
    private Long  idTipoMenu = null;
    private Mense mensa      = null;
    private HashMap<Long,TipoPietanza> hmTipoPietanza = new HashMap<>();
/*================================================================================================*/
/*                                  CREAZIONE DELL'FRAGMENT                                       */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'inizializazione del layout del fragment ed inoltre contiene le loro
     * possibili interazioni con l'utente.
     * In aggiunta viene richesto un modelView che è quello usato dall'activity in modo tale da accedere
     * alla sua stessa istanza, in modo tale da utilizarlo come metodo per passare parameti da una parte
     * all'altra.
     *
     * In questo caso viene usato per ricevere come informazione il tipo di menu che si vule
     * visualizzare e di quale mensa.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @SuppressLint("FragmentLiveDataObserve")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_menu_del_giorno, container, false);
/*------------------------------------------------------------------------*/
        listaDateView = rootView.findViewById(R.id.rcl_MenuDelGiorno);
        listaDateView.setLayoutManager(new LinearLayoutManager(getContext()));
/*------------------------------------------------------------------------*/
        giornoSelezionato = GiornoSelezionatoRowView.newInstance(rootView,R.id.include_GiornoSelezionato,dataDelMenu);
        if(giornoSelezionato!=null){
            giornoSelezionato.setFrecciaVersoSinistra();
            giornoSelezionato.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.chiudiMenuDelGiorno();
                }
            });
        }
/*------------------------------------------------------------------------*/
        try {
            viewModel = new ViewModelProvider(getActivity()).get(MenuDelleMenseViewModel.class);
        }catch (Exception e){
            viewModel=null;
        }
/*------------------------------------------------------------------------*/
        if(viewModel!=null){
            ldMensa             = viewModel.getMensaSelezzionata();
            ldIdTipoMenu        = viewModel.getIdTipoPastoSelezzionato();
            ldListaTipoPietanza = viewModel.getAllTipoPietanza();
        }
    /*------------------------------------------------------------------------*/
        if((ldMensa!=null) && (ldIdTipoMenu!=null) && (ldListaTipoPietanza!=null)){
            //Agiornameto Mensa selezionata
            ldMensa.observe(fragment, new Observer<Mense>() {
                @Override
                public void onChanged(Mense nuovaMense) {
                    if(nuovaMense!=null){
                        mensa = nuovaMense;
                        impostaPaginaConNuoviDati(mensa,idTipoMenu,dataDelMenu);
                    }
                }
            });
            //Aggiornamento Tipo Menu selezionato
            ldIdTipoMenu.observe(fragment, new Observer<Long>() {
                @Override
                public void onChanged(Long nuovoIdTipoMensa) {
                    if(nuovoIdTipoMensa!=null){
                        idTipoMenu = nuovoIdTipoMensa;
                        impostaPaginaConNuoviDati(mensa,idTipoMenu,dataDelMenu);
                    }
                }
            });
            //Aggiornamento Tipo Menu selezionato
            ldListaTipoPietanza.observe(fragment, new Observer<List<TipoPietanza>>() {
                @Override
                public void onChanged(List<TipoPietanza> nuovoListaTipoPietanza) {
                    if(nuovoListaTipoPietanza!=null){
                        hmTipoPietanza.clear();
                        Iterator<TipoPietanza> iterator = nuovoListaTipoPietanza.iterator();
                        while (iterator.hasNext()){
                            TipoPietanza tipoPietanza = iterator.next();
                            if(tipoPietanza!=null){
                                hmTipoPietanza.put(tipoPietanza.getId(),tipoPietanza);
                            }
                        }
                        impostaPaginaConNuoviDati(mensa,idTipoMenu,dataDelMenu);
                    }
                }
            });
        }
/*------------------------------------------------------------------------*/
        return rootView;
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  INSERIMENTO E AGGIORNAMENTO RECYCLERVIEW                      */
/*================================================================================================*/
    /**
     * Questo metodo verifica che ci siano impostati i tre valori che possono variare in ogni momento
     * e con quelli richiedo al database i dati delle pietanze che formano il menu richiesto.
     * @param mensa
     * @param idTipoMenu
     * @param date
     */
    private void impostaPaginaConNuoviDati(Mense mensa,Long idTipoMenu,Date date){
        if((mensa!=null) && (idTipoMenu!=null) && (!hmTipoPietanza.isEmpty())){
            LiveData<List<Pietanze>> ldListaPietanze = viewModel.getPietanzeByIdOfMensa_TipoMenu_AtSpecificDate(mensa.getId(),idTipoMenu,date);
            if(ldListaPietanze!=null){
                ldListaPietanze.observe(fragment, new Observer<List<Pietanze>>() {
                    @Override
                    public void onChanged(List<Pietanze> liataPietanze) {
                        if(liataPietanze!=null){
                            List<Pair<Boolean,String>> listaElementi = generaMenu(liataPietanze,hmTipoPietanza);
                            MenuDelGiornoRVAdapter adapter = new MenuDelGiornoRVAdapter(getContext(),listaElementi);
                            listaDateView.setAdapter(adapter);
                        }
                    }
                });
            }
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  GENERA LISTA TIPO PIETANZE E PIETAZE                          */
/*================================================================================================*/
    /**
     * Questo metodo si occupa di rendere la lista dei pietanze in una lista di coppie <booleano,stringa>
     * aventi come primo parametro true solo se la stringa a cui è associato contiene il nome di una
     * pietanza. Oppiure false se la tringa associata contiene il tipo di pietanze che seguono.
     *
     * Il risultato di questo metodo è l'imput richiesto dal adapter che si occupa di mostrare nel
     * ricyclerView la lista dei menu.
     * @param listaPietanza
     * @param hmTipoPietanza
     * @return
     */
    List<Pair<Boolean,String>> generaMenu(List<Pietanze> listaPietanza,HashMap<Long,TipoPietanza> hmTipoPietanza){
        //Raggruppa pietanze per tipo cosi da avere liste di pietanze associate a un sigolo tipo
        HashMap<Long,List<Pietanze>> hashMap= new HashMap<>();
        Iterator<Pietanze> iterator = listaPietanza.iterator();
        while (iterator.hasNext()){
            Pietanze pietanze = iterator.next();
            if(pietanze!=null){
                List<Pietanze> list = new LinkedList<>();
                if(hashMap.get(pietanze.getId_tipoPietanza())!=null){
                    list.addAll(hashMap.get(pietanze.getId_tipoPietanza()));
                }
                list.add(pietanze);
                hashMap.put(pietanze.getId_tipoPietanza(),list);
            }
        }
        //Questa parte inserisce in sequeanza le pietanze in modo tale che ogni gruppo
        //identificato pecedentemente sia preceduto dalla variabile tipo che identificherà le pietanze
        //che seguono.
        List<Pair<Boolean,String>> ris = new LinkedList<>();
        Iterator<Long> iteratorIdTipoPietanze = hashMap.keySet().iterator();
        while (iteratorIdTipoPietanze.hasNext()){
            Long idTipoPietanze = iteratorIdTipoPietanze.next();
            TipoPietanza tipoPietanza = hmTipoPietanza.get(idTipoPietanze);
            if(tipoPietanza!=null){
                Pair<Boolean, String> elTipoPietanza = new Pair(false,tipoPietanza.getNome());
                ris.add(elTipoPietanza);
                //Inserisci pietanze
                Iterator<Pietanze> iteratorPietanza = hashMap.get(idTipoPietanze).iterator();
                while (iteratorPietanza.hasNext()){
                    Pietanze pietanze = iteratorPietanza.next();
                    Pair<Boolean, String> elPietanza = new Pair(true,pietanze.getNome());
                    ris.add(elPietanza);
                }
            }
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
}