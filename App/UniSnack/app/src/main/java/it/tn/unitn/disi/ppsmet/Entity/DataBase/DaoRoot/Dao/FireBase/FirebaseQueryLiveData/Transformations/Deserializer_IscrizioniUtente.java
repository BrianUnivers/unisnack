package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import androidx.lifecycle.LiveData;

import com.google.firebase.database.DataSnapshot;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_AnnoAccademico_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List_In_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.IscrizioniUtente;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

public class Deserializer_IscrizioniUtente implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto IscrizioniUtente.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String LOG_TAG = "IscrizioniUtente_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "IscrizioniUtente" dai dati passati da "DataSnapshot"
 * ed dai dati passati "Universita" e "Corso". Questo perchè tali informazioni devono essere
 * reperite in un primo momento poichè i dati persenti nel risultato della query non li contengono.
 * <br>
 * <PRE>
 *"Utenti_del_Corso_del_Universita" : [
 *  "$id_universita":{
 *      "Corsi" : [
 *          "$id_corsi" :{
 *              "Iscritti" : [
 *                  "$id_universita-id_corsi-id_utente" : {
 *                      "nome"      : "Nome",
 *                      "cognome"   : "Cognome",
 *                      "nato"      : "01/10/2000",
 *                      "email"     : "address@email.com",
 *                      "hashed-psw"    : "YGYGYJVHGYG",
 *                      "salt"          : "HJHGKGKUSDL",
 *                      "email_hashed-psw" : "address@email.com_YGYGYJVHGYG",
 *                      "urlImmagine"   : "url",
 *                      "matricola"     : "123456",
 *                      "statoTessera"  : "true",
 *                      "urlQRcode"     : "Url",
 *                      "urlCodiceABarre"   : "Url",
 *                      "AnnoAccademico" : {
 *                          "$id_AnnoAccademico" : {
 *                              "dataInizio" : "12/09/2021",
 *                              "dataFine"   : "11/09/2022"
 *                              }
 *                          }
 *                      }
 *                  ]
 *              }
 *          ]
 *      }
 *  ]
 * </PRE>
 * @param universita   Questo parametro contiene l'universita a cui si è iscritto l'utente.
 * @param corso        Questo parametro contiene il corso a cui si è iscritto l'utente.
 * @param data         Questo parametro contiene una data che se devera da null, deve essere tra quella
 *                     d'inizio e di fine del anno accademico a qui l'utente è scritto, altrimenti tale
 *                     risultato sara da non considerere. In quest'ultimo caso il risultato è null;
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "IscrizioniUtente".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo oppure la data,se diversa
 *          da null, passata come parametro non è contenuta all'interno dell'intervallo dell'anno
 *          accademico, il risultato sarà null.
 */
    public static IscrizioniUtente estraiDaDataSnapshot(DataSnapshot dataSnapshot,Universita universita, Corsi corso, Date data){
        IscrizioniUtente iscrizioniUtente = null;
        try{
            if(dataSnapshot.exists()){
                AnnoAccademico annoAccademico = Deserializer_AnnoAccademico.getAttributeOf(dataSnapshot,L_ANNO_ACCADEMICO);
                if((annoAccademico!=null)){
                    if( (data==null ) || (annoAccademico.dataNelAnnoAccademico(data))){
                        Integer matricola =  Integer.parseInt( dataSnapshot.child(P_MATRICOLA).getValue(String.class) );
                        iscrizioniUtente = new IscrizioniUtente(universita,annoAccademico,corso,matricola);
                    }
                }
            }
        }catch (Exception e){
            Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
            iscrizioniUtente = null;
        }
        return iscrizioniUtente;
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "IscrizioniUtente" dai dati passati da "DataSnapshot".
 * <br>
 *  <PRE>
 *"UniversitaETessere_del_Utente" : [
 *  "$id_universita-id_corsi-id_utente" : {
 *      "Partecipa_Universita" : [
 *          "$id_universita" : {
 *              "nome"  : "Università di Trento",
 *              "sigla" : "Unitn",
 *              "Tessere" : [
 *                  "$id_tessera" : {
 *                      "statoTessera"      : "true",
 *                      "urlQRcode"         : "Url",
 *                      "urlCodiceABarre"   : "Url",
 *                      "AnnoAccademico" : {
 *                          "£id_AnnoAccademico" : {
 *                              "dataInizio" : "12/09/2021",
 *                              "dataFine"   : "11/09/2022"
 *                              }
 *                          }
 *                      }
 *                  ]
 *              }
 *          ]
 *      }
 *  ]
 * </PRE>
 * @param data         Questo parametro contiene una data che se devera da null, deve essere tra quella
 *                     d'inizio e di fine del anno accademico a qui l'utente è scritto, altrimenti tale
 *                     risultato sara da non considerere. In quest'ultimo caso il risultato è null;
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "IscrizioniUtente".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo oppure la data,se diversa
 *          da null, passata come parametro non è contenuta all'interno dell'intervallo dell'anno
 *          accademico, il risultato sarà null.
 */
    public static List<IscrizioniUtente> estraiDaDataSnapshot(DataSnapshot dataSnapshot, Date data){
        List<IscrizioniUtente> ris = new  LinkedList<>();
        try{
            if(dataSnapshot.exists()){
                Universita universita = Deserializer_Universita.estraiDaDataSnapshot(dataSnapshot,data);
                if(universita!=null){

                    Iterator<DataSnapshot> iterator = dataSnapshot.child(L_TESSERE).getChildren().iterator();
                    DataSnapshot dataSnapshotTessera;
                    while (iterator.hasNext()){
                        try {
                            dataSnapshotTessera = iterator.next();
                            AnnoAccademico annoAccademicoPerTessera =
                                    Deserializer_AnnoAccademico.getAttributeOf(dataSnapshotTessera,L_ANNO_ACCADEMICO);
                            if(annoAccademicoPerTessera!=null){
                                if( (data==null ) || (annoAccademicoPerTessera.dataNelAnnoAccademico(data))){
                                    ris.add( new IscrizioniUtente(universita,annoAccademicoPerTessera,null,null) );
                                }
                            }
                        }catch (Exception e){
                            Log.d(LOG_TAG, "2) Immposibile costruire un oggetto da un possibile risultato.",e);
                        }
                    }
                }
            }
        }catch (Exception e){
            Log.d(LOG_TAG, "1) Immposibile costruire un oggetto da un possibile risultato.",e);
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, IscrizioniUtente>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Object extends GenericDeserializer_Object<IscrizioniUtente> {
        private Corsi      corso =null;
        private Universita universita;
        private Date       date =null;
/*================================================================================================*/
/*                         COSTRUTTORE                                                            */
/*================================================================================================*/
        public Deserializer_Object(Universita universita, Corsi corso) {
            super();
            this.corso=corso;
            this.universita= universita;
        }
/*------------------------------------------------------------------------*/
        public Deserializer_Object(Universita universita, Corsi corso, Date date) {
            super();
            this.corso=corso;
            this.universita= universita;
            this.date= date;
        }
/*================================================================================================*/
        @Override
        public IscrizioniUtente estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_IscrizioniUtente.estraiDaDataSnapshot(dataSnapshot, this.universita, this.corso, this.date);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<IscrizioniUtente>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List_In_List<IscrizioniUtente> {
        private Date       date =null;
/*================================================================================================*/
/*                         COSTRUTTORE                                                            */
/*================================================================================================*/
        public Deserializer_List() {
            super();
        }
/*------------------------------------------------------------------------*/
        public Deserializer_List( Date date) {
            super();
            this.date= date;
        }
/*================================================================================================*/
        @Override
        public List<IscrizioniUtente> estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_IscrizioniUtente.estraiDaDataSnapshot(dataSnapshot,date);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
