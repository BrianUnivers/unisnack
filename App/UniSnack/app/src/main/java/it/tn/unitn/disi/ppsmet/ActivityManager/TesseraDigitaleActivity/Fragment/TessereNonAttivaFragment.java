package it.tn.unitn.disi.ppsmet.ActivityManager.TesseraDigitaleActivity.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.tn.unitn.disi.ppsmet.R;

public class TessereNonAttivaFragment extends Fragment {
/*################################################################################################*/
/*                                  INIZIALIZZAZIONE VALORI                                       */
/*################################################################################################*/
/*================================================================================================*/
/*                                  COSTRUTTORE                                                   */
/*================================================================================================*/
    /**
     * Questo è il metodo costruttore del fragment.
     */
    public TessereNonAttivaFragment() {
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
/*================================================================================================*/
/*                                  CREAZIONE DELL FRAGMENT                                       */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'inizializazione del layout del fragment.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tessere_non_attiva, container, false);
    }
/*================================================================================================*/
/*################################################################################################*/
}