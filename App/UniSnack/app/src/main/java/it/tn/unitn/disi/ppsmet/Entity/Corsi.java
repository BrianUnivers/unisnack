package it.tn.unitn.disi.ppsmet.Entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity(tableName = "tableCorsi",
        indices = { @Index(value = {"sigla"}, unique = true)})
public class Corsi {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @PrimaryKey
    private Long   id;
    @NonNull
    @ColumnInfo(name = "nome")
    private String nome;
    @NonNull
    @ColumnInfo(name = "sigla")
    private String sigla;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    @Ignore
    public Corsi() {}
//--------------------------------------------------------------------------------------------------
    public Corsi(Long id,
                 @NonNull String nome,
                 @NonNull String sigla) {
        this.id = id;
        this.nome = nome;
        this.sigla = sigla;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    public Long getId() {
        return id;
    }
//--------------------------------------
    public void setId(Long id) {
        this.id = id;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getNome() {
        return nome;
    }
//--------------------------------------
    public void setNome(@NonNull String nome) {
        this.nome = nome;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getSigla() {
        return sigla;
    }
//--------------------------------------
    public void setSigla(@NonNull String sigla) {
        this.sigla = sigla;
    }
//==================================================================================================
//==================================================================================================
//                                      EQUALS E HASH_CODE
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Corsi)) return false;
        Corsi corsi = (Corsi) o;
        return Objects.equals(getId(), corsi.getId()) &&
                getNome().equals(corsi.getNome()) &&
                getSigla().equals(corsi.getSigla());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNome(), getSigla());
    }
//==================================================================================================
//##################################################################################################
}
