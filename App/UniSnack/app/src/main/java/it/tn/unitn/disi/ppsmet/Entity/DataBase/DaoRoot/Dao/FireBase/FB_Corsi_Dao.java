package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.google.firebase.database.DatabaseReference;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.Uni_ha_C;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;


public class FB_Corsi_Dao extends ConnesioneAFirebase<Corsi> {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public  LiveData<List<Corsi>> getAll_OneTime(){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_CORSI);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Corsi.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<Corsi>> getAll(){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_CORSI);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Corsi.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Corsi>> getAll_Synchronized(){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_CORSI);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Corsi.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UNIVERSITA'                                           */
/*================================================================================================*/
    public  LiveData<List<Corsi>> getByIdOfUniverita_OneTime(@NonNull Long idUniversita){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_CORSI_DEL_UNIVERSITA)
                .child(idUniversita.toString())
                .child(L_CORSI);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Corsi.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<Corsi>> getByIdOfUniverita(@NonNull Long idUniversita){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_CORSI_DEL_UNIVERSITA)
                .child(idUniversita.toString())
                .child(L_CORSI);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Corsi.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Corsi>> getByIdOfUniverita_Synchronized(@NonNull Long idUniversita){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_CORSI_DEL_UNIVERSITA)
                .child(idUniversita.toString())
                .child(L_CORSI);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Corsi.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID and ID UNIVERSITA'                                    */
/*================================================================================================*/
    public LiveData<Corsi> getByPrimaryKeyAndUniversita_OneTime(@NonNull Long id,@NonNull Long idUniversita){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_CORSI_DEL_UNIVERSITA)
                .child(idUniversita.toString())
                .child(L_CORSI)
                .child(id.toString());
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Corsi.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Corsi> getByPrimaryKeyAndUniversita(@NonNull Long id,@NonNull Long idUniversita){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_CORSI_DEL_UNIVERSITA)
                .child(idUniversita.toString())
                .child(L_CORSI)
                .child(id.toString());
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Corsi.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Corsi> getByPrimaryKeyAndUniversita_Synchronized(@NonNull Long id,@NonNull Long idUniversita){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_CORSI_DEL_UNIVERSITA)
                .child(idUniversita.toString())
                .child(L_CORSI)
                .child(id.toString());
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Corsi.Deserializer_Object());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
    public LiveData<Corsi> getByIdOfUtente_OneTime(@NonNull Utenti utenti){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_CORSI_DEL_UNIVERSITA)
                .child(utenti.getId_Universita().toString())
                .child(L_CORSI)
                .child(utenti.getId_Corso().toString());
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Corsi.Deserializer_Object());
    }
    /*------------------------------------------------------------------------*/
    public LiveData<Corsi> getByIdOfUtente(@NonNull Utenti utenti){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_CORSI_DEL_UNIVERSITA)
                .child(utenti.getId_Universita().toString())
                .child(L_CORSI)
                .child(utenti.getId_Corso().toString());
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Corsi.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Corsi> getByIdOfUtente_Synchronized(@NonNull Utenti utenti){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_CORSI_DEL_UNIVERSITA)
                .child(utenti.getId_Universita().toString())
                .child(L_CORSI)
                .child(utenti.getId_Corso().toString());
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Corsi.Deserializer_Object());
    }
/*================================================================================================*/
/*################################################################################################*/
}
