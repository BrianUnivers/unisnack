package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Allergeni_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Ingredienti_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_TipoMenu_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Ingredienti_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.TipoMenu_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;

public class IngredientiEAllergeni_ThreadAggiornamento extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA ANNO ACCADEMICO LOCALE           */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "IngredientiEAllergeni" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_IngredientiEAllergeni";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista dei IngredientiEAllergeni del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private List<Ingredienti> listaIngredienti;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * Ingredienti.
         * @param listaIngredienti Questo parametro contiene la lista di tutti gli elementi
         *                        tra qui quelli nuovi e quelli già presenti ma da modificare.
         */
        public ThreadAggiornamento(List<Ingredienti> listaIngredienti) {
            super();
            this.listaIngredienti =listaIngredienti;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                                  */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella Ingredienti.");
            aggionaTabellaIngredienti(listaIngredienti);
            Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella Ingredienti.");
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param listaIngredienti
         */
        private synchronized void aggionaTabellaIngredienti(List<Ingredienti> listaIngredienti){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella Ingredienti.");
            //Dati presenti nel database locale.
            List<Ingredienti> listaIngredientiLocale = ingredientiDao.getAll();
            List<Allergeni>   listaAllergeniLocale   =  allergeniDao.getAll();
            listaIngredientiLocale.removeAll(listaAllergeniLocale);
            listaIngredientiLocale.addAll(listaAllergeniLocale);
            //Allergeni e Ingrdienti
            if((listaIngredientiLocale==null)||(listaIngredientiLocale.isEmpty())){
                inserisciListaElementi(listaIngredienti);
            }else if((listaIngredienti==null)||(listaIngredienti.isEmpty())) {
                eliminaListaElementi(listaIngredientiLocale);
            }else{
                HashMap<Long,Ingredienti> hashMapIngredientiLocali = convertiListaInHashMap(listaIngredientiLocale);

                List<Ingredienti> listaIngredientiNuove = new LinkedList<>();
                List<Ingredienti> listaIngredientiAggiornate = new LinkedList<>();
                //Serve solo per poter conoscere quali elementi caccelare tra quelli già presenti localmente.
                List<Ingredienti> listaVechieIngredientiDaMantenere = new LinkedList<>();

                Iterator<Ingredienti> iterator = listaIngredienti.iterator();
                while (iterator.hasNext()){
                    Ingredienti ingredienti =iterator.next();
                    if(ingredienti!=null){
                        Ingredienti ingredientiVecchia = hashMapIngredientiLocali.get(ingredienti.getId());
                        if(ingredientiVecchia==null){
                            listaIngredientiNuove.add(ingredienti);
                        }else if(!ingredientiVecchia.equals(ingredienti)){
                            listaIngredientiAggiornate.add(ingredienti);
                            listaVechieIngredientiDaMantenere.add(ingredientiVecchia);
                        }else{
                            listaVechieIngredientiDaMantenere.add(ingredientiVecchia);
                        }
                    }
                }
                //MANTENGO SOLO GLI ELEMNETI CHE DEVO CANCELLARE
                listaIngredientiLocale.removeAll(listaVechieIngredientiDaMantenere);
                inserisciListaElementi(listaIngredientiNuove);
                aggiornaListaElementi(listaIngredientiAggiornate);
                eliminaListaElementi(listaIngredientiLocale);
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         GENERAZIONE HASH MAP FROM LIST                                         */
/*================================================================================================*/
        private synchronized HashMap<Long,Ingredienti> convertiListaInHashMap(List<Ingredienti> lista){
            HashMap<Long,Ingredienti> ris=new HashMap<>();
            if(lista!=null){
                Iterator<Ingredienti> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Ingredienti elemento = iterator.next();
                    if((elemento!=null) && (elemento.getId()!=null)){
                        ris.put(elemento.getId(),elemento);
                    }
                }
            }
            return ris;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO INSERIMENTO NUOVI ELEMENTI                                      */
/*================================================================================================*/
        private synchronized void inserisciListaElementi(List<Ingredienti> lista){
            if(lista!=null){
                Iterator<Ingredienti> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Ingredienti elemento=iterator.next();
                    if(elemento!=null){
                        ingredientiDao.insert(elemento);
                        if(elemento instanceof Allergeni){
                            allergeniDao.insert((Allergeni) elemento);
                        }
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO AGGIORNAMENTO ELEMENTI PRESENTI                                 */
/*================================================================================================*/
        private synchronized void aggiornaListaElementi(List<Ingredienti> lista){
            if(lista!=null){
                Iterator<Ingredienti> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Ingredienti elemento=iterator.next();
                    if(elemento!=null){
                        ingredientiDao.update(elemento);
                        if(elemento instanceof Allergeni){
                            allergeniDao.update((Allergeni) elemento);
                        }
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaListaElementi(List<Ingredienti> lista){
            if(lista!=null){
                Iterator<Ingredienti> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Ingredienti elemento=iterator.next();
                    if(elemento!=null){
                        ingredientiDao.delete(elemento.getId());
                        /* if(elemento instanceof Allergeni){
                            allergeniDao.delete((Allergeni) elemento.getId());
                        }*/
                    }
                }
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_Ingredienti_Dao fbIngredientiDao = new FB_Ingredienti_Dao();
    //QUERY FIREBASE
    private static LiveData<List<Ingredienti>> fbAllIngredienti=null;
    //ROOM DATABASE
    public static Ingredienti_Dao ingredientiDao = null;
    public static Allergeni_Dao   allergeniDao   = null;
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaIngredienti(){
        if(fbAllIngredienti==null){
            fbAllIngredienti= fbIngredientiDao.getAll_Synchronized();
        }
        if(ingredientiDao==null){
            ingredientiDao = db.IngredientiDao();
        }
        if(allergeniDao==null){
            allergeniDao = db.AllergeniDao();
        }
        if(!fbAllIngredienti.hasObservers()){//SE NON HA già osservatori per rilevare aggiornamenti
            fbAllIngredienti.observeForever(new Observer<List<Ingredienti>>() {
                @Override
                public void onChanged(List<Ingredienti> lista) {
                    if((lista!=null)){
                        executor.execute(new ThreadAggiornamento(lista));
                    }
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public IngredientiEAllergeni_ThreadAggiornamento(Application application) {
        super(application);
        gestioneAggiornametoTabellaIngredienti();
    }
/*================================================================================================*/
/*################################################################################################*/
}
