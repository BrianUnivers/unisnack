package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData;

import android.util.Log;

import androidx.lifecycle.LiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class FirebaseQueryLiveData_OneTime extends LiveData<DataSnapshot> {
/**
 * Questa classe ci permette di gestire e mantenere i dati, <b>al momento della richiesta,</b> senza
 * la possibilita di ricalcolare se non esplicitamnete tali riultati. Questo in modo tale da evitare
 * che delle modifiche sui dati o la riattivazione di un Activity faccia ricalcolare i dati potando
 * al esecuzione di codice non voluto.
 */
/*################################################################################################*/
    private static final String LOG_TAG = "FirebaseQueryLiveData_OneTime";

    private final Query query;
    private final MyValueEventListener listener = new MyValueEventListener();
/*================================================================================================*/
/*                         CONSTRUCTOR OF QUERY AT FIREBASE RUNTIME DATABASE                      */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET LAST ID OF ORDINAZIONI                                             */
/*------------------------------------------------------------------------------------------------*/
    public FirebaseQueryLiveData_OneTime(Query query) {
        this.query = query;
        query.addListenerForSingleValueEvent(listener);
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET LAST ID OF ORDINAZIONI                                             */
/*------------------------------------------------------------------------------------------------*/
    public FirebaseQueryLiveData_OneTime(DatabaseReference ref) {
        this.query = ref;
        query.addListenerForSingleValueEvent(listener);
    }

/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         ACTIVATION AND INATIVATION UPDATE OF RESULTS OF QUERY                  */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         ACTIVATION UPDATE OF RESULTS OF QUERY (Activity is show on screen)     */
/*                         It is call when activity is onSTARTED or onRESUMED                     */
/*------------------------------------------------------------------------------------------------*/
    @Override
    protected void onActive() {
        Log.d(LOG_TAG, "onActive do nothing");
        //query.addListenerForSingleValueEvent(listener);
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         INATIVATION UPDATE OF RESULTS OF QUERY (Activity is inactive or destroied)*/
/*                         It is call when activity is onDESTROY or onPAUSE                       */
/*------------------------------------------------------------------------------------------------*/
    @Override
    protected void onInactive() {
        Log.d(LOG_TAG, "onInactive do nothing");
        query.removeEventListener(listener);
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
    private class MyValueEventListener implements ValueEventListener {
/*================================================================================================*/
/*                         ACTION FOR UPDATE THE RESULTS OF QUERY                                 */
/*================================================================================================*/
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            setValue(dataSnapshot);//Insermento nella lista LiveData dei risultati
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         ERROR IT CAN'T CONNECTION TO FIREBASE RUNTIME DATABASE                 */
/*================================================================================================*/
        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e(LOG_TAG, "Can't listen to query " + query, databaseError.toException());
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}