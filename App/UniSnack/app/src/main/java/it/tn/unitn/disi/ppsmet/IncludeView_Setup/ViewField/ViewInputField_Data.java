package it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewField;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import it.tn.unitn.disi.ppsmet.R;

public class ViewInputField_Data extends ViewInputField_Text {
/*################################################################################################*/
    private Calendar dataInserita= Calendar.getInstance();//Defoult oggi
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP VIEW FIELD                                */
/*================================================================================================*/
    public ViewInputField_Data(ViewGroup rootLayout, int idLayoutIncluded, int idLable, int idTestValue) {
        super(rootLayout,idLayoutIncluded,idLable,idTestValue,false);
        impostaLayoutComune();
    }
/*------------------------------------------------------------------------*/
    public ViewInputField_Data(ViewGroup rootLayout, int idLayoutIncluded, int idLable, int idTestValue, boolean obbligatorio) {
        super(rootLayout,idLayoutIncluded,idLable,idTestValue,obbligatorio);
        impostaLayoutComune();
    }
/*------------------------------------------------------------------------------------------------*/
    private void impostaLayoutComune() {
        if(input!=null){
            input.setFocusable(false);
            input.setFocusableInTouchMode(false);
            input.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    azzionePerInserimetoDati();
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  AZZIONI DURANTE L'INSERIMETO DEI DATI                         */
/*================================================================================================*/
    public Date getData(){
        return dataInserita.getTime();
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  AZZIONI PER L'INSERIMETO DEI DATI                             */
/*================================================================================================*/
    private DatePickerDialog.OnDateSetListener dataListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            dataInserita.set(Calendar.YEAR,year);
            dataInserita.set(Calendar.MONTH,month);
            dataInserita.set(Calendar.DAY_OF_MONTH,dayOfMonth);
            input.setText(""+dayOfMonth+" / "+ (1+month)+" / "+year);
            annullaErrori();
            input.setError(null);
        }

    };
/*------------------------------------------------------------------------------------------------*/
    protected void azzionePerInserimetoDati() {
        int anno = dataInserita.get(Calendar.YEAR);
        int mese = dataInserita.get(Calendar.MONTH);
        int giorno = dataInserita.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(
                input.getContext(),
                R.style.MyDatePickerStyle,
                dataListener,
                anno,mese,giorno);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.show();
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if(campoObbligatorio && ( (input!=null) && (input.getText().toString().trim().length() == 0)) ){
                    impostaErroreCampoObbligatorio();
                }
            }
        });
    }

/*================================================================================================*/
/*################################################################################################*/
}
