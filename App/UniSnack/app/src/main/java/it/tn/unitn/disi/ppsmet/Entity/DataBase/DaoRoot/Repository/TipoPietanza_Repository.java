package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.TipoPietanza_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.TipoPasto_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.TipoPietanza_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;

public class TipoPietanza_Repository  extends TipoPietanza_ThreadAggiornamento {
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA TIPO PIETANZA (e le sue derivazioni     */
/*                         seguendo un ordine topologico per garantire gli aggiornamenti)         */
/*                         ATTRAVERO IL DATABASE LOCALE (room)                                    */
/*################################################################################################*/
    private LiveData<List<TipoPietanza>> mAllTipoPietanza;
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public TipoPietanza_Repository(Application application) {
        super(application);
        //AllTipoPietanza get any time the same result
        mAllTipoPietanza = tipoPietanzaDao.getAll_Synchronized();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERT                                                                 */
/*================================================================================================*/
    /*public Future<Long> insert(TipoPietanza tipoPietanza){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                    @Override
                    public Long call() throws Exception {
                        return tipoPietanzaDao.insert(tipoPietanza);
                    }
                });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    /*public void deleteAll(){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            tipoPietanzaDao.deleteAll();
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    /*public void update(TipoPietanza tipoPietanza){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            tipoPietanzaDao.update(tipoPietanza);
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<TipoPietanza>> getAll(){
        return mAllTipoPietanza;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    /*public LiveData<TipoPietanza> getByPrimaryKey(Long id){
        return tipoPietanzaDao.getByPrimaryKey_Synchronized(id);
    }
/*================================================================================================*/
/*################################################################################################*/
}
