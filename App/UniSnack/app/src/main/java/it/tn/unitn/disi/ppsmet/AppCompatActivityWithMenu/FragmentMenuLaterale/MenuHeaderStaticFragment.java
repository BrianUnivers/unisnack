package it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import it.tn.unitn.disi.ppsmet.IncludeView_Setup.TitoloSopraContenutoView;
import it.tn.unitn.disi.ppsmet.R;


public class MenuHeaderStaticFragment extends Fragment {
/*################################################################################################*/
/*                                  INIZIALIZZAZIONE VALORI                                       */
/*################################################################################################*/
    private static final String ARG_PARAM_ID_TITOLO           = "idTitolo";
    private static final String ARG_PARAM_ID_IMAGE_BACKGROUND = "idImageBackground";
    /**
     * La costante "ARG_PARAM_CHECK_ID_IMAGE" contiene un booleano che stà ad indicare se c'è il valore
     * associato a "ARG_PARAM_ID_IMAGE".
     */
    private static final String ARG_PARAM_CHECK_ID_IMAGE      = "checkIdImage";
    private static final String ARG_PARAM_ID_IMAGE            = "idImage";
/*================================================================================================*/
/*                                  COSTRUTTORE                                                   */
/*================================================================================================*/
    /**
     * Questo è il metodo costruttore del fragment. Non contene nessun parametro poichè il metodo di
     * inserimento di quest'ultimi verrà fatto tramite i "Bundle".
     */
    //OBBLIGATORIO PER I FRAGMENT
    public MenuHeaderStaticFragment() {
        super();
        this.idTestoTitolo     = R.string.ttl_TitoloProva;
        this.idImageBackground = R.drawable.img_menu_config_profilo_utente;
    }
/*------------------------------------------------------------------------------------------------*/
/*                                  COSTRUTTORE VERI                                              */
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo è il vero e proprio costruttore del fragment che oltre a costruire l'oggetto fragment,
     * permette anche di inserire i parametri nel "Bundle" ad esso assegnato. Così che in seguito
     * li possa eventualmente estarre.
     * @param idTestoTitolo
     * @param idImageBackground
     * @param idImage
     * @return
     */
    public static MenuHeaderStaticFragment newInstance(int idTestoTitolo,int idImageBackground, int idImage){
        MenuHeaderStaticFragment fragment = new MenuHeaderStaticFragment();
        Bundle args = new Bundle();
        args.putInt(     ARG_PARAM_ID_TITOLO,            idTestoTitolo);
        args.putInt(     ARG_PARAM_ID_IMAGE_BACKGROUND,  idImageBackground);
        args.putBoolean( ARG_PARAM_CHECK_ID_IMAGE,       true);
        args.putInt(     ARG_PARAM_ID_IMAGE,             idImage);
        fragment.setArguments(args);
        return fragment;
    }
/*--------------------------------------------------------*/
    /**
     * Questo è il vero e proprio costruttore del fragment che oltre a costruire l'oggetto fragment,
     * permette anche di inserire i parametri nel "Bundle" ad esso assegnato. Così che in seguito
     * li possa eventualmente estarre.
     * @param idTestoTitolo
     * @param idImageBackground
     * @return
     */
    public static MenuHeaderStaticFragment newInstance(int idTestoTitolo,int idImageBackground){
        MenuHeaderStaticFragment fragment = new MenuHeaderStaticFragment();
        Bundle args = new Bundle();
        args.putInt(     ARG_PARAM_ID_TITOLO,            idTestoTitolo);
        args.putInt(     ARG_PARAM_ID_IMAGE_BACKGROUND,  idImageBackground);
        args.putBoolean( ARG_PARAM_CHECK_ID_IMAGE,       false);
        fragment.setArguments(args);
        return fragment;
    }
/*================================================================================================*/
    private Integer idTestoTitolo;
    private Integer idImageBackground;
    private Integer idImage = null;
/*================================================================================================*/
/*                                  INIZIALIZZATORE DI PARAMETRI                                  */
/*================================================================================================*/
    /**
     * Questo metodo viene chieamato nella fase di costruzione del frugment ed è quello che si occupa
     * di estrarre i dati inseriti nel "Bundle" dal costruttore e li imposta come volori del fragment.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            idTestoTitolo     = getArguments().getInt(ARG_PARAM_ID_TITOLO);
            idImageBackground = getArguments().getInt(ARG_PARAM_ID_IMAGE_BACKGROUND);
            if(getArguments().getBoolean(             ARG_PARAM_CHECK_ID_IMAGE,false)){
                idImage = getArguments().getInt(      ARG_PARAM_ID_IMAGE);
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private ViewGroup rootView;
    private ImageView imageBackground;
    private ImageView image;
    private TitoloSopraContenutoView titolo;
/*================================================================================================*/
/*                                  CREAZIONE DELL'FRAGMENT                                       */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'inizializazione del layout del fragment .
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_menu_header_static, container, false);
/*------------------------------------------------------------------------*/
        imageBackground = (ImageView) rootView.findViewById(R.id.img_Background);
        imageBackground.setImageResource(idImageBackground);
/*------------------------------------------------------------------------*/
        if(idImage!=null){
            image = (ImageView) rootView.findViewById(R.id.img_Immagine);
            image.setImageResource(idImage);
            image.setVisibility(View.VISIBLE);
        }
/*------------------------------------------------------------------------*/
        titolo = TitoloSopraContenutoView.newInstance(  rootView,
                                                        R.id.include_titolo_sopra_contenuto,
                                                        this.idTestoTitolo);
/*------------------------------------------------------------------------*/
        return rootView;
    }
/*================================================================================================*/
/*################################################################################################*/
}