package it.tn.unitn.disi.ppsmet.GestioneRicordaSessione;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.Pair;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.concurrent.ExecutionException;

public class GestioneSessione  {
    /**
     * La costante "TYPE_OF_HASH" contiene il tipo di algoritmo usato per calcolare l'hash.
     */
    private static String TYPE_OF_HASH        = "SHA-256";
    /**
     * La costante "SESSIONE_UTENTE" contene il nome della sessione da cercare che contiene i dati di
     * sessione da memorizzare.
     */
    private static final String SESSIONE_UTENTE = "SESSIONE_UTENTE_UNISNACK";
    /**
     * La costante "ID_SESSIONE" contene l'identificativo del utente che si vuole mantenere attivo.
     */
    private static final String ID_SESSIONE  = "ID" ;
    /**
     * La costante "MAX_DATA_VALIDA" contiene il giono limite, senza accessi intemedi, prima che la
     * sessione scada;
     */
    private static final String MAX_DATA_VALIDA  = "MAX_DATA" ;
    /**
     * La costante "MAX_PERIOD" contiene il numero di millisecondi che deve passare dal ultimo accesso
     * prima che scada la sessione.
     */
    private static final Integer MAX_PERIOD = 1000*60*60*24*7;//millisecondi
//##################################################################################################
//                              GEESTIONE SESSIONE
//##################################################################################################
    private static SharedPreferences sharedPreferences=null;
//==================================================================================================
//                              CREAZIONE SESSIONE UTENTE REGISTRATO
//==================================================================================================
    /**
     * Questo metodo crea una sessione se il valore di "remember" è true altimenti cancella tutti i
     * dati di sessione già presenti.
     * @param context
     * @param remember
     * @return
     */
    public static String creaSessione(Context context,Boolean remember){
        sharedPreferences = context.getSharedPreferences(SESSIONE_UTENTE,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String nuovoIdSessione = null;
        if (remember){
            nuovoIdSessione = genraIdSessione();
            editor.putString(ID_SESSIONE,  nuovoIdSessione);
            editor.putLong(MAX_DATA_VALIDA,((new Date().getTime())+MAX_PERIOD));
        }else{
            cancellaSessioneAperte(editor);
        }
        editor.apply();
        return nuovoIdSessione;
    }
//==================================================================================================
//==================================================================================================
//                              CANCELLA SESSIONE
//==================================================================================================
    /**
     * Questo metodo cancella tutti i parametri di sessione già presenti.
     * @param context
     */
    public static void cancellaSessioneAperte(Context context){
        sharedPreferences = context.getSharedPreferences(SESSIONE_UTENTE,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        cancellaSessioneAperte(editor);
        editor.apply();
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Questo è il metodo esecutivo della sessione specificata nel parametro passato.
     * @param editor
     */
    private static void cancellaSessioneAperte(SharedPreferences.Editor editor){
        editor.clear();
    }
//==================================================================================================
//==================================================================================================
//                              GENERA ID SESSIONE
//==================================================================================================
    /**
     * Qusta funzione genrara un id di sessione sepre diverso a partire dai millesecondi attuali.
     *
     * @return
     *      Il risultato di questo metodo è una stringa contenete una rappresentazione in esadecimale
     *      della sequenza di byte che viene generato. Se non fosse possibile calcolare un id il
     *      risultata retiuirà null.
     */
    public static String genraIdSessione(){
        Date today = new Date();
        Long seedNum = today.getTime();
        String seedString = seedNum.toString();
        return genraHashFromString(seedString);
    }
//==================================================================================================
//==================================================================================================
//                              GENERA ID FROM STRING
//==================================================================================================
    /**
     * Questa funzione dato una stringa effetua l'operazione di hash e ne restituisce una corrispondente
     * al array binario del hash
     *
     * @param text
     *      Il parametro passsato sarà il dato sorgente sul quale verrà applicato la funzione di hash.
     * @return
     *      Il rusultato di questo metodo è una stringa che contiene la rapresentazione in esadecimale
     *      della sequenza di byte denerata dal hash del parametro iniziale.
     *      Se non fosse possibile calcolare l'hash, il risultato di tale funzione è null.
     */
    public static String genraHashFromString(String text){
        MessageDigest digest = null;
        String ris = null;
        try {
            digest = MessageDigest.getInstance(TYPE_OF_HASH);
            byte[] encodedhash = digest.digest(text.getBytes(StandardCharsets.UTF_8));
            ris = bytesToHex(encodedhash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return ris;
    }
//==================================================================================================

//==================================================================================================
//                              CONVERTE UN ARRAY BYTE A STRING
//==================================================================================================
    /**
     * La costante "HEX_ARRAY" contiene in un array di caratteri i valori esadecipali,in modo tale
     * da avere una rappresentazione di un byte.
     */
    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    /**
     * Questa funzione serve per convertire un array binario in una stringa di caratteri non spciali
     * @param bytes
     *      Il parametro che passi è un array di byte.
     * @return
     *      Il risutltato è una stringa che è la rappresentazione in caratte non speciali del array
     *      che si è passato come parametro.
     */
    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                              ESISSTE SESSIONE
//##################################################################################################
//==================================================================================================
//                              ESISTE SESSIONE ATTIVA
//==================================================================================================
    /**
     * Questo metodo permette dei verificare se esiste una sessione aperta e valida cioè che la data
     * in qui si fa questa oerazione non sia succesiva alla data di scadenza della sessione.
     * @param context
     * @return
     */
    public static boolean esisteSessioneApera(Context context){
        sharedPreferences = context.getSharedPreferences(SESSIONE_UTENTE,Context.MODE_PRIVATE);
        String  sessione = sharedPreferences.getString(ID_SESSIONE,null);
        Long time = sharedPreferences.getLong(MAX_DATA_VALIDA,0);
        Date maxData = new Date();
        maxData.setTime(time);
        return  ((maxData.after(new Date())) && (sessione!=null));
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                              GET INFORMATION SESION
//##################################################################################################
//==================================================================================================
//                              GET ID SESSIONE
//==================================================================================================
    /**
     * Questo metodo permette di estrarre l'idetificativo della sessione ma solo se esiste ed è ancora
     * valida. Altrimenti il risultato sarà null.
     * @param context
     * @return
     */
    public static  String getIdSessione(Context context){
        sharedPreferences = context.getSharedPreferences(SESSIONE_UTENTE,Context.MODE_PRIVATE);
        String id = null;
        if(esisteSessioneApera(context)){
            id = sharedPreferences.getString(ID_SESSIONE,null);
        }
        return id;
    }
//==================================================================================================
//##################################################################################################
}
