package it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewValutazione;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewValutazione.OnChangeValueListener.OnChangeValueListener;
import it.tn.unitn.disi.ppsmet.R;

public class TestoConValutazioneView {
/*################################################################################################*/
    private Integer   LIMITE_MIN = 0;
    private Integer   LIMITE_MAX = 10;
/*------------------------------------------------------------------------*/
    private Integer   LIMITE_PRIMA_STELLA      = 1;
    private Integer   LIMITE_SECONDA_STELLA    = 3;
    private Integer   LIMITE_TERZA_STELLA      = 5;
    private Integer   LIMITE_QUARTA_STELLA     = 7;
    private Integer   LIMITE_QUINTA_STELLA     = 9;
/*------------------------------------------------------------------------*/
    private ViewGroup contenitore      = null;
    private TextView  txtTitolo        = null;
    private ImageView imgPrimaStella   = null;
    private ImageView imgSecondaStella = null;
    private ImageView imgTerzaStella   = null;
    private ImageView imgQuartaStella  = null;
    private ImageView imgQuintaStella  = null;
/*------------------------------------------------------------------------*/
    private short     numeroStelle     = 0;
    private boolean   isSelectable     = true;
/*------------------------------------------------------------------------*/
    private OnChangeValueListener listener = null;
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    /**
     * Questo metodo permette di costruire e definire il layout impostato
     */
    public TestoConValutazioneView(ViewGroup contenitore,
                                   TextView txtTitolo,
                                   ImageView imgPrimaStella,
                                   ImageView imgSecondaStella,
                                   ImageView imgTerzaStella,
                                   ImageView imgQuartaStella,
                                   ImageView imgQuintaStella) {
        this.contenitore = contenitore;
        this.txtTitolo = txtTitolo;
        this.imgPrimaStella   = imgPrimaStella;
        this.imgSecondaStella = imgSecondaStella;
        this.imgTerzaStella   = imgTerzaStella;
        this.imgQuartaStella  = imgQuartaStella;
        this.imgQuintaStella  = imgQuintaStella;

        this.imgPrimaStella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isSelectable){
                    setVoto((short) 2);
                    attivaLisener();
                }
            }
        });
        this.imgSecondaStella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isSelectable){
                    setVoto((short) 4);
                    attivaLisener();

                }
            }
        });
        this.imgTerzaStella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isSelectable){
                    setVoto((short) 6);
                    attivaLisener();
                }
            }
        });
        this.imgQuartaStella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isSelectable){
                    setVoto((short) 8);
                    attivaLisener();
                }
            }
        });
        this.imgQuintaStella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isSelectable){
                    setVoto((short) 10);
                    attivaLisener();
                }
            }
        });
    }
/*------------------------------------------------------------------------*/
    /**
     * Questo è il costuttore di quando l'oggetto viene impostato in una pagina o in una sua parte.
     * @param rootLayout
     * @param idLayoutIncluded
     * @return
     */
    public static TestoConValutazioneView newInstance(ViewGroup rootLayout, int idLayoutIncluded) {
        TestoConValutazioneView ris = null;
        if(rootLayout!=null){
            ViewGroup contenitore = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(contenitore!=null){
                TextView  txtTitolo        = (TextView) contenitore.findViewById(R.id.txt_Titolo);
                ImageView imgPrimaStella   = (ImageView) contenitore.findViewById(R.id.img_PrimaStella);
                ImageView imgSecondaStella = (ImageView) contenitore.findViewById(R.id.img_SecondaStella);
                ImageView imgTerzaStella   = (ImageView) contenitore.findViewById(R.id.img_TerzaStella);
                ImageView imgQuartaStella  = (ImageView) contenitore.findViewById(R.id.img_QuartaStella);
                ImageView imgQuintaStella  = (ImageView) contenitore.findViewById(R.id.img_QuintaStella);
                if((txtTitolo!=null) && (imgPrimaStella!=null) && (imgSecondaStella!=null) &&
                        (imgTerzaStella!=null) && (imgQuartaStella!=null) && (imgQuintaStella!=null)){
                    ris = new TestoConValutazioneView(contenitore,txtTitolo,imgPrimaStella,imgSecondaStella,
                                                        imgTerzaStella,imgQuartaStella,imgQuintaStella);
                }
            }
        }
        return ris;
    }
/*------------------------------------------------------------------------*/
    /**
     * Questo è il costuttore di quando l'oggetto viene impostato in una pagina o in una sua parte.
     * @param rootLayout
     * @param idLayoutIncluded
     * @return
     */
    public static TestoConValutazioneView newInstance(ViewGroup rootLayout, int idLayoutIncluded, int idTextLable) {
        TestoConValutazioneView ris = newInstance(rootLayout,idLayoutIncluded);
        if(ris!=null){
            ris.setLabel(idTextLable);
        }
        return ris;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    /**
     * Questo metodo permette di impostare e cambiare la lable del gurppo di layout, partendo da
     * identificatere della risosssa della stringa.
     * @param idTextTitolo
     */
    public void setLabel(int idTextTitolo){
        this.txtTitolo.setText(idTextTitolo);
    }
/*--------------------------------------------*/
    /**
     * Questo metodo permette di impostare e cambiare la lable del gurppo di layout, partendo dalla
     * stringa.
     * @param textTitolo
     */
    public void setLabel(String textTitolo){
        this.txtTitolo.setText(textTitolo);
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo permette di impostare il numero di stelle selezionate se il valore è superiore
     * a 10 0 inferiore a 0 compresi.
     * @param numeroStelle
     */
    public boolean setVoto(short numeroStelle){
        boolean ris = false;
        if((numeroStelle>=LIMITE_MIN) && (numeroStelle<=LIMITE_MAX)){
            this.numeroStelle = numeroStelle;
            //Prima stella
            if(numeroStelle==LIMITE_PRIMA_STELLA){
                this.imgPrimaStella.setImageResource(R.drawable.ic_baseline_star_half_24_primary);
            }else if(numeroStelle >LIMITE_PRIMA_STELLA){
                this.imgPrimaStella.setImageResource(R.drawable.ic_baseline_star_24_primary);
            }else{
                this.imgPrimaStella.setImageResource(R.drawable.ic_baseline_star_border_24);
            }
            //Seconda stella
            if(numeroStelle==LIMITE_SECONDA_STELLA){
                this.imgSecondaStella.setImageResource(R.drawable.ic_baseline_star_half_24_primary);
            }else if(numeroStelle >LIMITE_SECONDA_STELLA){
                this.imgSecondaStella.setImageResource(R.drawable.ic_baseline_star_24_primary);
            }else{
                this.imgSecondaStella.setImageResource(R.drawable.ic_baseline_star_border_24);
            }
            //Terza stella
            if(numeroStelle==LIMITE_TERZA_STELLA){
                this.imgTerzaStella.setImageResource(R.drawable.ic_baseline_star_half_24_primary);
            }else if(numeroStelle >LIMITE_TERZA_STELLA){
                this.imgTerzaStella.setImageResource(R.drawable.ic_baseline_star_24_primary);
            }else{
                this.imgTerzaStella.setImageResource(R.drawable.ic_baseline_star_border_24);
            }
            //Quarta stella
            if(numeroStelle==LIMITE_QUARTA_STELLA){
                this.imgQuartaStella.setImageResource(R.drawable.ic_baseline_star_half_24_primary);
            }else if(numeroStelle >LIMITE_QUARTA_STELLA){
                this.imgQuartaStella.setImageResource(R.drawable.ic_baseline_star_24_primary);
            }else{
                this.imgQuartaStella.setImageResource(R.drawable.ic_baseline_star_border_24);
            }
            //Quinta stella
            if(numeroStelle==LIMITE_QUINTA_STELLA){
                this.imgQuintaStella.setImageResource(R.drawable.ic_baseline_star_half_24_primary);
            }else if(numeroStelle >LIMITE_QUINTA_STELLA){
                this.imgQuintaStella.setImageResource(R.drawable.ic_baseline_star_24_primary);
            }else{
                this.imgQuintaStella.setImageResource(R.drawable.ic_baseline_star_border_24);
            }
        }
        return ris;
    }
/*--------------------------------------------*/
    /**
     * Questo metodo permette di estrarre il voto selezionato in questo momento.
     * @return
     */
    public short getVoto(){
        return this.numeroStelle;
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo permette di decidere se i valori rappresentati dalle stelle sono modificabili o
     * meno.
     * @param isModificabile
     */
    public void  setModificablie(boolean isModificabile){
        this.isSelectable = isModificabile;
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  LISTENER                                                      */
/*################################################################################################*/
/*================================================================================================*/
/*                                  ATTIVA LISTENER                                               */
/*================================================================================================*/
    private void attivaLisener(){
        if(listener!=null){
            listener.onChangeValueListener(contenitore,getVoto());
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  SET LISTENER                                                  */
/*================================================================================================*/
    public void setOnChangeValueListener(OnChangeValueListener listener){
        this.listener = listener;
    }
/*================================================================================================*/
/*################################################################################################*/
}
