package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.*;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DatiPerSessione.IdentificativiSessioni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.*;
import it.tn.unitn.disi.ppsmet.Entity.*;

@Database(  version = 1,
            exportSchema = false,
            entities = {
                    TipoMenu.class,         // 1 LIVELLO DI DIPENDEZA
                    TipoPasto.class,
                    TipoPietanza.class,
                    AnnoAccademico.class,
                    Ingredienti.class,
                    Corsi.class,
                    Universita.class,
                    Pietanze.class,         // 2 LIVELLO DI DIPENDEZA
                    Mense.class,
                    Allergeni.class,
                    Uni_ha_C.class,
                    Utenti.class,           // 3 LIVELLO DI DIPENDEZA
                    M_fornisce_P.class,
                    P_contiene_A.class,
                    Ordinazioni.class,      // 4 LIVELLO DI DIPENDEZA
                    PastoDelMese.class,
                    U_partecipa_Uni.class,
                    U_intollerante_A.class,
                    PdM_propone_P.class,    // 5 LIVELLO DI DIPENDEZA
                    O_relativa_F.class,
                    U_vota_PdM.class,
                    IdentificativiSessioni.class//EXTERNAL DATABASE ONLY LOCAL
                    })
@TypeConverters({MyConvertersData.class})
public abstract class MyRoomDatabase extends RoomDatabase {

    public abstract TipoMenu_Dao        TipoMenuDao();          // 1 LIVELLO DI DIPENDEZA
    public abstract TipoPasto_Dao       TipoPastoDao();
    public abstract TipoPietanza_Dao    TipoPietanzaDao();
    public abstract AnnoAccademico_Dao  AnnoAccademicoDao();
    public abstract Ingredienti_Dao     IngredientiDao();
    public abstract Corsi_Dao           CorsiDao();
    public abstract Universita_Dao      UniversitaDao();
    public abstract Pietanze_Dao        PietanzeDao();          // 2 LIVELLO DI DIPENDEZA
    public abstract Mense_Dao           MenseDao();
    public abstract Allergeni_Dao       AllergeniDao();
    public abstract Utenti_Dao          UtentiDao();           // 3 LIVELLO DI DIPENDEZA
    public abstract Ordinazioni_Dao     OrdinazioniDao();      // 4 LIVELLO DI DIPENDEZA
    public abstract PastoDelMese_Dao    PastoDelMeseDao();
                                                               // ESTENSIONI
    public abstract IscrizioniUtente_Dao IscrizioniUtenteDao();
    public abstract Tessere_Dao          TessereDao();
    public abstract IdentificativiSessioni_Dao IdentificativiSessioni_Dao();
    public abstract P_Contiene_A_Dao      PContieneADao();
    public abstract M_Fornisce_P_Dao      MForniscePDao();

    private static volatile MyRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static MyRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MyRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MyRoomDatabase.class, "UniSnack_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
/*
            // If you want to keep data through app restarts,
            // comment out the following block
            databaseWriteExecutor.execute(() -> {
                // Populate the database in the background.
                // If you want to start with more words, just add them.
                 TipoPasto_Dao dao = INSTANCE.TipoPastoDao();
               // dao.deleteAll();

                TipoPasto pranzo = new TipoPasto(1L,"Pranzo");
                TipoPasto cena   = new TipoPasto(2L,"Cena");
                dao.insert(pranzo);
                dao.insert(cena);
            });*/
        }
    };
}

