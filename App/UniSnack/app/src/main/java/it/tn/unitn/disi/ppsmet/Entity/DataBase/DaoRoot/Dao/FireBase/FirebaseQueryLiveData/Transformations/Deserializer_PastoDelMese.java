package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;
import it.tn.unitn.disi.ppsmet.Entity.PastoDelMese;

public class Deserializer_PastoDelMese implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto PastoDelMese.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String DATA_FORMAT = "dd/MM/yyyy";
    private static String LOG_TAG = "PastoDelMese_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "PastoDelMese" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "PastoDelMese".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public static PastoDelMese estraiDaDataSnapshot(DataSnapshot dataSnapshot){
        PastoDelMese PastoDelMese = null;
        if(dataSnapshot.exists()){
            try{
                String idUniversita_idCorso_idUtente_idPastoDelMese=dataSnapshot.getKey();
                String[] ids=idUniversita_idCorso_idUtente_idPastoDelMese.split(S_SEPARARORE_PER_CHIAVI);

                if(ids.length==4){
                    //Campi not null
                    Long idUtente      = Long.parseLong(ids[3]);
                    Short anno         = dataSnapshot.child(P_ANNO).getValue(Short.class);
                    Short mese         = dataSnapshot.child(P_MESE).getValue(Short.class);
                    Date  dataPorposta = new SimpleDateFormat(DATA_FORMAT)
                                                .parse( dataSnapshot.child(P_DATA).getValue(String.class));
                    //Verifica campi not null
                    if((idUtente!=null) && (anno!=null) && (mese!=null) && (dataPorposta!=null)){
                        PastoDelMese = new PastoDelMese(idUtente,mese,anno,dataPorposta);
                    }
                }

            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
                PastoDelMese = null;
            }
        }
        return PastoDelMese;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         GET AN ACTRIBUTE OBJECT FOR AN OTHER                                   */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET AN ACTRIBUTE                                                       */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce, se esiste, un singolo ogetto "PastoDelMese" da un attributo di un altro
 * oggetto attraverso una sottoparte dell'oggetto "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "PastoDelMese".
 * @return  Il riutato è il primo oggetto, valido se ce ne fossero più d'uno ritorna il primo trovato
 *          mentre se non ce ne sono ritorna null.
 */
    public static PastoDelMese getAttributeOf(DataSnapshot dataSnapshot,String nomeAttributo){
        PastoDelMese PastoDelMese = null;
        DataSnapshot PastoDelMeseDataSnapshot = dataSnapshot.child(nomeAttributo);
        if(PastoDelMeseDataSnapshot.exists()){
            Iterator<DataSnapshot> iterator =PastoDelMeseDataSnapshot.getChildren().iterator();
            while ( (iterator.hasNext()) && (PastoDelMese==null) ){//Restituisce il primo oggetto valido
                PastoDelMese = estraiDaDataSnapshot(iterator.next());
            }
        }
        return PastoDelMese;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, PastoDelMese>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Object extends GenericDeserializer_Object<PastoDelMese> {
        @Override
        public PastoDelMese estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_PastoDelMese.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<PastoDelMese>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List<PastoDelMese> {
        @Override
        public PastoDelMese estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_PastoDelMese.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
