package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_PastoDelMese;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.PdM_propone_P;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.U_vota_PdM;
import it.tn.unitn.disi.ppsmet.Entity.PastoDelMese;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class FB_PastoDelMese_Dao extends ConnesioneAFirebase<PastoDelMese>{ //extends FB_PietanzeInsertExist_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
/*todo @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    public long insert(PastoDelMese pastoDelMese, List<Pietanze> listaPietanza){
        Long ris = this.insert(pastoDelMese);
        Iterator<Pietanze> iterator=listaPietanza.iterator();
        while(iterator.hasNext()){
            Pietanze pietanze=iterator.next();
            Long idPietanze =pietanze.getId();
            if(idPietanze==null){
                idPietanze =super.exist(pietanze);
                if(idPietanze==null){
                    idPietanze =this.insert(pietanze);
                }
            }
            PdM_propone_P propone =new PdM_propone_P(pastoDelMese.getId_utente(),pastoDelMese.getMese(),pastoDelMese.getAnno(),idPietanze);
            ris++;
        }
        this.insertVoto(pastoDelMese,pastoDelMese.getId_utente());
        ris++;
        return ris;
    }
/*------------------------------------------------------------------------*/
/*todo   @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    public long insertVoto(PastoDelMese pastoDelMese, Long idUtenti ){
        U_vota_PdM voto =new U_vota_PdM(pastoDelMese.getId_utente(),pastoDelMese.getMese(),pastoDelMese.getAnno(),idUtenti);
        if(this.exist(voto)!=0){
            this.delete(voto);
        }
        return this.insert(voto);
    }
/*------------------------------------------------------------------------------------------------*/
/*todo   @Insert(onConflict = OnConflictStrategy.ABORT)
    protected abstract long insert(PdM_propone_P propone);
/*------------------------------------------------------------------------*/
/*todo @Insert(onConflict = OnConflictStrategy.ABORT)
    protected abstract long insert(PastoDelMese pastoDelMese);
/*------------------------------------------------------------------------*/
/*todo @Insert(onConflict = OnConflictStrategy.ABORT)
    protected abstract long insert(U_vota_PdM vota);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<PastoDelMese>> getAll_OneTime(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_PASTO_DEL_MESE);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_PastoDelMese.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PastoDelMese>> getAll(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_PASTO_DEL_MESE);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_PastoDelMese.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PastoDelMese>> getAll_Synchronized(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_PASTO_DEL_MESE);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_PastoDelMese.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    public LiveData<PastoDelMese> getByPrimaryKey_OneTime(@NonNull Utenti utenti,@NonNull  Short mese,@NonNull Short anno){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE)
                .child(builderKeyFromUtentiAnnoMese(utenti,anno,mese));
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_PastoDelMese.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<PastoDelMese> getByPrimaryKey(@NonNull Utenti utenti,@NonNull  Short mese,@NonNull Short anno){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE)
                .child(builderKeyFromUtentiAnnoMese(utenti,anno,mese));
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_PastoDelMese.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<PastoDelMese> getByPrimaryKey_Synchronized(@NonNull Utenti utenti,@NonNull  Short mese,@NonNull Short anno){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE)
                .child(builderKeyFromUtentiAnnoMese(utenti,anno,mese));
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_PastoDelMese.Deserializer_Object());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTI                                                */
/*================================================================================================*/
    public LiveData<List<PastoDelMese>> getByIdUtente_OneTime(@NonNull Utenti utenti){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE_PROPOSTO_DAL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PROPOSTE);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_PastoDelMese.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PastoDelMese>> getByIdUtente(@NonNull Utenti utenti){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE_PROPOSTO_DAL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PROPOSTE);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_PastoDelMese.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PastoDelMese>> getByIdUtente_Synchronized(@NonNull Utenti utenti){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE_PROPOSTO_DAL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PROPOSTE);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_PastoDelMese.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ANNO AND MESE                                            */
/*================================================================================================*/
    public LiveData<List<PastoDelMese>> getByAnno_Mese_OneTime(Short mese,Short anno){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE)
                .orderByChild(P_ANNO__MESE)
                .equalTo(builderKeyFromAnnoMese(anno,mese));
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_PastoDelMese.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PastoDelMese>> getByAnno_Mese(Short mese,Short anno){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE)
                .orderByChild(P_ANNO__MESE)
                .equalTo(builderKeyFromAnnoMese(anno,mese));
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_PastoDelMese.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PastoDelMese>> getByAnno_Mese_Synchronized(Short mese,Short anno){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE)
                .orderByChild(P_ANNO__MESE)
                .equalTo(builderKeyFromAnnoMese(anno,mese));
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_PastoDelMese.Deserializer_List());
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET VOTI of PASTO DEL MESE                                      */
/*================================================================================================*/
/*ToDo public LiveData<Long> getVotiSingoloPastoDelMese_OneTime(PastoDelMese pastoDelMese){
        return this.getVotiSingoloPastoDelMese_OneTime( pastoDelMese.getId_utente(),
                                                        pastoDelMese.getMese(),
                                                        pastoDelMese.getAnno());
    }
/*------------------------------------------------------------------------*/
/*ToDo public LiveData<Long> getVotiSingoloPastoDelMese(PastoDelMese pastoDelMese){
        return this.getVotiSingoloPastoDelMese( pastoDelMese.getId_utente(),
                                                pastoDelMese.getMese(),
                                                pastoDelMese.getAnno());
    }
/*------------------------------------------------------------------------*/
/*ToDo  public LiveData<Long> getVotiSingoloPastoDelMese_Synchronized(PastoDelMese pastoDelMese){
        return this.getVotiSingoloPastoDelMese_Synchronized(pastoDelMese.getId_utente(),
                                                            pastoDelMese.getMese(),
                                                            pastoDelMese.getAnno());
    }
/*------------------------------------------------------------------------------------------------*/
/*ToDo @Query( "SELECT COUNT(DISTINCT V.id_utente) " +
            "FROM tablePastoDelMese AS PdM, tableU_vota_PdM AS V " +
            "WHERE   V.id_proponete == PdM.id_utente AND V.id_mese == PdM.mese AND V.id_anno == PdM.anno AND " +
            "PdM.id_utente == :idUtente AND " +
            "PdM.mese == :mese AND " +
            "PdM.anno == :anno " )
    protected LiveData<Long> getVotiSingoloPastoDelMese_OneTime(Long idUtente,
                                                                Short mese,
                                                                Short anno);
/*------------------------------------------------------------------------*/
  /*ToDo  @Query( "SELECT COUNT(DISTINCT V.id_utente) " +
            "FROM tablePastoDelMese AS PdM, tableU_vota_PdM AS V " +
            "WHERE   V.id_proponete == PdM.id_utente AND V.id_mese == PdM.mese AND V.id_anno == PdM.anno AND " +
                    "PdM.id_utente == :idUtente AND " +
                    "PdM.mese == :mese AND " +
                    "PdM.anno == :anno " )
    protected LiveData<Long> getVotiSingoloPastoDelMese(Long idUtente,
                                                      Short mese,
                                                      Short anno);
/*------------------------------------------------------------------------*/
/*ToDo @Query( "SELECT COUNT(DISTINCT V.id_utente) " +
            "FROM tablePastoDelMese AS PdM, tableU_vota_PdM AS V " +
            "WHERE   V.id_proponete == PdM.id_utente AND V.id_mese == PdM.mese AND V.id_anno == PdM.anno AND " +
                    "PdM.id_utente == :idUtente AND " +
                    "PdM.mese == :mese AND " +
                    "PdM.anno == :anno " )
    protected LiveData<Long>  getVotiSingoloPastoDelMese_Synchronized(Long idUtente,
                                                                               Short mese,
                                                                               Short anno);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET VOTI of PASTO DEL MESE RISPETTO AL TOTALE                   */
/*================================================================================================*/
   /*ToDo public LiveData<Double> getVotiSingoloPastoDelMeseRispettoAlTotale_OneTime(PastoDelMese pastoDelMese){
        return this.getVotiSingoloPastoDelMeseRispettoAlTotale_OneTime( pastoDelMese.getId_utente(),
                                                                        pastoDelMese.getMese(),
                                                                        pastoDelMese.getAnno());
    }
/*------------------------------------------------------------------------*/
 /*ToDo   public LiveData<Double> getVotiSingoloPastoDelMeseRispettoAlTotale(PastoDelMese pastoDelMese){
        return this.getVotiSingoloPastoDelMeseRispettoAlTotale( pastoDelMese.getId_utente(),
                                                                pastoDelMese.getMese(),
                                                                pastoDelMese.getAnno());
    }
/*------------------------------------------------------------------------*/
  /*ToDo  public LiveData<Double> getVotiSingoloPastoDelMeseRispettoAlTotale_Synchronized(PastoDelMese pastoDelMese){
        return this.getVotiSingoloPastoDelMeseRispettoAlTotale_Synchronized(pastoDelMese.getId_utente(),
                                                                            pastoDelMese.getMese(),
                                                                            pastoDelMese.getAnno());
    }
/*------------------------------------------------------------------------------------------------*/
   /*ToDo protected LiveData<Double> getVotiSingoloPastoDelMeseRispettoAlTotale_OneTime(Long idUtente,
                                                                                  Short mese,
                                                                                  Short anno);
/*------------------------------------------------------------------------*/
   /*ToDo @Query( "Select ParzV.voti/TotV.voti " +
            "From ( SELECT COUNT(DISTINCT V.id_utente) AS voti " +
                    "FROM tablePastoDelMese AS PdM, tableU_vota_PdM AS V " +
                    "WHERE   V.id_proponete == PdM.id_utente AND V.id_mese == PdM.mese AND V.id_anno == PdM.anno AND " +
                    "PdM.id_utente == :idUtente AND " +
                    "PdM.mese == :mese AND " +
                    "PdM.anno == :anno " +
                ") AS ParzV ," +
                "(  SELECT COUNT(DISTINCT V.id_utente) AS voti " +
                    "FROM tablePastoDelMese AS PdM, tableU_vota_PdM AS V " +
                    "WHERE   V.id_proponete == PdM.id_utente AND V.id_mese == PdM.mese AND V.id_anno == PdM.anno AND " +
                        "PdM.mese == :mese AND " +
                        "PdM.anno == :anno ) AS TotV ")
    protected LiveData<Double> getVotiSingoloPastoDelMeseRispettoAlTotale(Long idUtente,
                                                       Short mese,
                                                       Short anno);
/*------------------------------------------------------------------------*/
   /*ToDo @Query( "Select ParzV.voti/TotV.voti " +
            "From ( SELECT COUNT(DISTINCT V.id_utente) AS voti " +
                    "FROM tablePastoDelMese AS PdM, tableU_vota_PdM AS V " +
                    "WHERE   V.id_proponete == PdM.id_utente AND V.id_mese == PdM.mese AND V.id_anno == PdM.anno AND " +
                            "PdM.id_utente == :idUtente AND " +
                            "PdM.mese == :mese AND " +
                            "PdM.anno == :anno " +
                    ") AS ParzV ," +
                    "(  SELECT COUNT(DISTINCT V.id_utente) AS voti " +
                        "FROM tablePastoDelMese AS PdM, tableU_vota_PdM AS V " +
                        "WHERE   V.id_proponete == PdM.id_utente AND V.id_mese == PdM.mese AND V.id_anno == PdM.anno AND " +
                                "PdM.mese == :mese AND " +
                                "PdM.anno == :anno ) AS TotV ")
    protected LiveData<Double>  getVotiSingoloPastoDelMeseRispettoAlTotale_Synchronized(Long idUtente,
                                                                                   Short mese,
                                                                                   Short anno);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         EXIST                                                                  */
/*================================================================================================*/
 /*   @Transaction
    public long exist(PastoDelMese pastoDelMese){
        return this.exist(  pastoDelMese.getId_utente(),
                            pastoDelMese.getMese(),
                            pastoDelMese.getAnno(),
                            pastoDelMese.getDataProposta());
    }
/*------------------------------------------------------------------------*/
  /*  @Transaction
    public long exist(U_vota_PdM vota){
        return this.exist(  vota.getId_proponete(),
                            vota.getId_mese(),
                            vota.getId_anno(),
                            vota.getId_utente());
    }
/*------------------------------------------------------------------------------------------------*/
 /*  @Query( "SELECT COUNT(  *) AS tot  " +
           "FROM (  SELECT DISTINCT PdM.id_utente AS col1 , PdM.mese  AS col2, PdM.anno AS col3 " +
                    "FROM tablePastoDelMese AS PdM " +
                    "WHERE   PdM.id_utente == :idUtente AND " +
                            "PdM.mese == :mese AND " +
                            "PdM.anno == :anno AND " +
                            "PdM.dataProposta == :dataProposta)" )
    protected abstract long exist(Long idUtente,
                                  Short mese,
                                  Short anno,
                                  Date dataProposta);
/*------------------------------------------------------------------------*/
   /* @Query( "SELECT COUNT(*) AS tot  " +
            "FROM (  SELECT DISTINCT V.id_utente AS col1 , V.id_mese AS col2 , V.id_anno AS col3 "+
                    "FROM tableU_vota_PdM AS V " +
                    "WHERE   V.id_proponete == :idProponete AND " +
                            "V.id_mese == :idMese AND " +
                            "V.id_anno == :idAnno AND " +
                            "V.id_utente == :idUtente ) " )
    protected abstract long exist(Long idProponete,
                                  Short idMese,
                                  Short idAnno,
                                  Long idUtente);
/*================================================================================================*/
/*################################################################################################*/
}