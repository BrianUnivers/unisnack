package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface;

import androidx.arch.core.util.Function;

import com.google.firebase.database.DataSnapshot;

import java.util.LinkedList;
import java.util.List;

/**
 * Questa classe pernde il risultato di una query al firebase runtime database e lo trasforma in oggetti
 * incapsulati in uno di tipo "LiveData".
 * ATTENZIONE! Anche null è da considerarsi come valore valido che viene incapsulato singolarmente ed o
 * tra i dati validi.
 */
public abstract class GenericDeserializer_List_In_List<T> implements Function<DataSnapshot, List<T>> {
/*################################################################################################*/
/*================================================================================================*/
/*                         GET DataLive OF LIST OBJECTS                                           */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET DataLive OF LIST OBJECTS FROM DataSnapshot                         */
/*------------------------------------------------------------------------------------------------*/
    @Override
    public List<T> apply(DataSnapshot dataSnapshot) {
        List<T> ris = new LinkedList<>();
        for(DataSnapshot ds : dataSnapshot.getChildren()){
            List<T>  oggetto = estraiDaDataSnapshot(ds);
            if((oggetto != null) && (!oggetto.isEmpty())){
                ris.addAll(oggetto);
            }
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/

/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "AnnoAccademico" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "AnnoAccademico".
 * @return  Il riutato è la lista dei oggeti trovati e correttemente creati nello stesso sottoinsieme
 *          di dati che forma la parte,passata del risultto della query. Però è possibile che il risultato
 *          sia una lista vuota.
 */
    public abstract List<T> estraiDaDataSnapshot(DataSnapshot dataSnapshot);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
}

