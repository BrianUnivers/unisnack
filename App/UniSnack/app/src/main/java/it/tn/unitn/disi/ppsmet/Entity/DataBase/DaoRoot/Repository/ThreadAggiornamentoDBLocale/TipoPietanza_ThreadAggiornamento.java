package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_TipoPietanza_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.TipoPietanza_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;

public class TipoPietanza_ThreadAggiornamento    extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA ANNO ACCADEMICO LOCALE           */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "TipoPietanza" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_TipoPietanza";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista dei TipoPietanza del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private List<TipoPietanza> listaTipoPietanza;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * TipoPietanza.
         * @param listaTipoPietanza Questo parametro contiene la lista di tutti gli elementi
         *                        tra qui quelli nuovi e quelli già presenti ma da modificare.
         */
        public ThreadAggiornamento(List<TipoPietanza> listaTipoPietanza) {
            super();
            this.listaTipoPietanza =listaTipoPietanza;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                                  */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella TipoPietanza.");
            aggionaTabellaTipoPietanza(listaTipoPietanza);
            Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella TipoPietanza.");
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param listaTipoPietanza
         */
        private synchronized void aggionaTabellaTipoPietanza(List<TipoPietanza> listaTipoPietanza){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella TipoPietanza.");
            //Dati presenti nel database locale.
            List<TipoPietanza> listaTipoPietanzaLocale = tipoPietanzaDao.getAll();
            if((listaTipoPietanzaLocale==null)||(listaTipoPietanzaLocale.isEmpty())){
                inserisciListaElementi(listaTipoPietanza);
            }else if((listaTipoPietanza==null)||(listaTipoPietanza.isEmpty())) {
                eliminaListaElementi(listaTipoPietanzaLocale);
            }else{
                HashMap<Long,TipoPietanza> hashMapTipoPietanzaLocali = convertiListaInHashMap(listaTipoPietanzaLocale);

                List<TipoPietanza> listaTipoPietanzaNuove = new LinkedList<>();
                List<TipoPietanza> listaTipoPietanzaAggiornate = new LinkedList<>();
                //Serve solo per poter conoscere quali elementi caccelare tra quelli già presenti localmente.
                List<TipoPietanza> listaVechieTipoPietanzaDaMantenere = new LinkedList<>();

                Iterator<TipoPietanza> iterator = listaTipoPietanza.iterator();
                while (iterator.hasNext()){
                    TipoPietanza tipoPietanza =iterator.next();
                    if(tipoPietanza!=null){
                        TipoPietanza tipoPietanzaVecchia = hashMapTipoPietanzaLocali.get(tipoPietanza.getId());
                        if(tipoPietanzaVecchia==null){
                            listaTipoPietanzaNuove.add(tipoPietanza);
                        }else if(!tipoPietanzaVecchia.equals(tipoPietanza)){
                            listaTipoPietanzaAggiornate.add(tipoPietanza);
                            listaVechieTipoPietanzaDaMantenere.add(tipoPietanzaVecchia);
                        }else{
                            listaVechieTipoPietanzaDaMantenere.add(tipoPietanzaVecchia);
                        }
                    }
                }
                //MANTENGO SOLO GLI ELEMNETI CHE DEVO CANCELLARE
                listaTipoPietanzaLocale.removeAll(listaVechieTipoPietanzaDaMantenere);
                inserisciListaElementi(listaTipoPietanzaNuove);
                aggiornaListaElementi(listaTipoPietanzaAggiornate);
                eliminaListaElementi(listaTipoPietanzaLocale);
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         GENERAZIONE HASH MAP FROM LIST                                         */
/*================================================================================================*/
        private synchronized HashMap<Long,TipoPietanza> convertiListaInHashMap(List<TipoPietanza> lista){
            HashMap<Long,TipoPietanza> ris=new HashMap<>();
            if(lista!=null){
                Iterator<TipoPietanza> iterator = lista.iterator();
                while (iterator.hasNext()){
                    TipoPietanza elemento = iterator.next();
                    if((elemento!=null) && (elemento.getId()!=null)){
                        ris.put(elemento.getId(),elemento);
                    }
                }
            }
            return ris;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO INSERIMENTO NUOVI ELEMENTI                                      */
/*================================================================================================*/
        private synchronized void inserisciListaElementi(List<TipoPietanza> lista){
            if(lista!=null){
                Iterator<TipoPietanza> iterator = lista.iterator();
                while (iterator.hasNext()){
                    TipoPietanza elemento=iterator.next();
                    if(elemento!=null){
                        tipoPietanzaDao.insert(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO AGGIORNAMENTO ELEMENTI PRESENTI                                 */
/*================================================================================================*/
        private synchronized void aggiornaListaElementi(List<TipoPietanza> lista){
            if(lista!=null){
                Iterator<TipoPietanza> iterator = lista.iterator();
                while (iterator.hasNext()){
                    TipoPietanza elemento=iterator.next();
                    if(elemento!=null){
                        tipoPietanzaDao.update(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaListaElementi(List<TipoPietanza> lista){
            if(lista!=null){
                Iterator<TipoPietanza> iterator = lista.iterator();
                while (iterator.hasNext()){
                    TipoPietanza elemento=iterator.next();
                    if(elemento!=null){
                        tipoPietanzaDao.delete(elemento.getId());
                    }
                }
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_TipoPietanza_Dao fbTipoPietanzaDao = new FB_TipoPietanza_Dao();
    //QUERY FIREBASE
    private static LiveData<List<TipoPietanza>> fbAllTipoPietanza=null;
    //ROOM DATABASE
    protected static TipoPietanza_Dao tipoPietanzaDao = null;
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaTipoPietanza(){
        if(fbAllTipoPietanza==null){
            fbAllTipoPietanza= fbTipoPietanzaDao.getAll_Synchronized();
        }
        if(tipoPietanzaDao==null){
            tipoPietanzaDao = db.TipoPietanzaDao();
        }
        if(!fbAllTipoPietanza.hasObservers()){//SE NON HA già osservatori per rilevare aggiornamenti
            fbAllTipoPietanza.observeForever(new Observer<List<TipoPietanza>>() {
                @Override
                public void onChanged(List<TipoPietanza> lista) {
                    if((lista!=null)){
                        executor.execute(new ThreadAggiornamento(lista));
                    }
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public TipoPietanza_ThreadAggiornamento(Application application) {
        super(application);
        gestioneAggiornametoTabellaTipoPietanza();
    }
/*================================================================================================*/
/*################################################################################################*/
}
