package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.Date;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.U_partecipa_Uni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.IscrizioniUtente;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

@Dao
public abstract class Tessere_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(U_partecipa_Uni partecipa);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Query("DELETE FROM tableU_partecipa_Uni")
    public abstract void deleteAll();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE By ID                                                    */
/*================================================================================================*/
    @Query("DELETE FROM tableU_partecipa_Uni " +
            "WHERE id_utente == :idUtente AND id_AA == :idAA AND id_universita == :idUniversita")
    public abstract void delete(Long idUtente,Long idAA,Long idUniversita);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = U_partecipa_Uni.class,
            onConflict = OnConflictStrategy.ABORT)
    public abstract void update(U_partecipa_Uni partecipa);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
                "U.matricola, " +
                "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND U.id == :idUtenti " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "null AS C_id, null AS C_nome, null AS C_sigla, " +
                    "null, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U "+
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND U.id_utente == :idUtenti " )
    public abstract List<Tessere> getByIdOfUtente(Long idUtenti);
/*------------------------------------------------------------------------*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
                    "U.matricola, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre, U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND U.id == :idUtenti " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "null AS C_id, null AS C_nome, null AS C_sigla, " +
                    "null, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre, U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND U.id_utente == :idUtenti " )
    public abstract LiveData<List<Tessere>> getByIdOfUtente_Synchronized(Long idUtenti);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE  INTO SPECIFIC DAY                             */
/*================================================================================================*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
                    "U.matricola, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND" +
                    " U.id == :idUtenti  AND :date BETWEEN AA.dataInizio AND AA.dataFine " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "null AS C_id, null AS C_nome, null AS C_sigla, " +
                    "null, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND "+
                    " U.id_utente == :idUtenti  AND :date BETWEEN AA.dataInizio AND AA.dataFine " )
    public abstract List<Tessere> getByIdOfUtente(Long idUtenti, Date date);
/*------------------------------------------------------------------------*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
                    "U.matricola, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND" +
                    " U.id == :idUtenti  AND :date BETWEEN AA.dataInizio AND AA.dataFine " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "null AS C_id, null AS C_nome, null AS C_sigla, " +
                    "null, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND "+
                    " U.id_utente == :idUtenti  AND :date BETWEEN AA.dataInizio AND AA.dataFine " )
    public abstract LiveData<List<Tessere>> getByIdOfUtente_Synchronized(Long idUtenti, Date date);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE TODAY                                          */
/*================================================================================================*/
@Transaction
    public List<Tessere> getByIdOfUtenteAtToday(Long idUtenti){
        return this.getByIdOfUtente(idUtenti,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getByIdOfUtenteAtToday_Synchronized(Long idUtenti){
        return this.getByIdOfUtente_Synchronized(idUtenti,new Date());
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET TESSERE ATTIVE by ID UTENTE                                 */
/*================================================================================================*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
                    "U.matricola, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND U.id == :idUtenti AND U.statoTessera == 1 " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "null AS C_id, null AS C_nome, null AS C_sigla, " +
                    "null, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U "+
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND U.id_utente == :idUtenti AND U.statoTessera == 1 " )
    public abstract List<Tessere> getActiveByIdOfUtente(Long idUtenti);
/*------------------------------------------------------------------------*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
                    "U.matricola, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND U.id == :idUtenti AND U.statoTessera == 1 " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "null AS C_id, null AS C_nome, null AS C_sigla, " +
                    "null, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND U.id_utente == :idUtenti AND U.statoTessera == 1 " )
    public abstract LiveData<List<Tessere>> getActiveByIdOfUtente_Synchronized(Long idUtenti);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET TESSERE ATTIVE by ID UTENTE  INTO SPECIFIC DAY              */
/*================================================================================================*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
                    "U.matricola, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND" +
                    " U.id == :idUtenti AND U.statoTessera == 1 AND :date BETWEEN AA.dataInizio AND AA.dataFine " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "null AS C_id, null AS C_nome, null AS C_sigla, " +
                    "null, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND "+
                    " U.id_utente == :idUtenti AND U.statoTessera == 1  AND :date BETWEEN AA.dataInizio AND AA.dataFine " )
    public abstract List<Tessere> getActiveByIdOfUtente(Long idUtenti, Date date);
/*------------------------------------------------------------------------*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
                    "U.matricola, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND" +
                    " U.id == :idUtenti AND U.statoTessera == 1  AND :date BETWEEN AA.dataInizio AND AA.dataFine " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "null AS C_id, null AS C_nome, null AS C_sigla, " +
                    "null, " +
                    "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
                    "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND "+
                    " U.id_utente == :idUtenti AND U.statoTessera == 1  AND :date BETWEEN AA.dataInizio AND AA.dataFine " )
    public abstract LiveData<List<Tessere>> getActiveByIdOfUtente_Synchronized(Long idUtenti, Date date);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET TESSERE ATTIVE by ID UTENTE TODAY                           */
/*================================================================================================*/
    @Transaction
    public List<Tessere> getActiveByIdOfUtenteAtToday(Long idUtenti){
        return this.getActiveByIdOfUtente(idUtenti,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Tessere>> getActiveByIdOfUtenteAtToday_Synchronized(Long idUtenti){
        return this.getActiveByIdOfUtente_Synchronized(idUtenti,new Date());
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "null AS C_id, null AS C_nome, null AS C_sigla, " +
            "null, " +
            "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
            "U.codiceABarre AS codiceABarre,  U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U "+
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND U.id_utente == :idUtenti " )
    public abstract List<Tessere> getSecondaryByIdOfUtente(Long idUtenti);
/*------------------------------------------------------------------------*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "null AS C_id, null AS C_nome, null AS C_sigla, " +
            "null, " +
            "U.statoTessera AS statoTessera, U.qrCode AS QRcode, U.urlQRCode AS urlQRCode, " +
            "U.codiceABarre AS codiceABarre, U.urlCodiceABarre AS urlCodiceABarre " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND U.id_utente == :idUtenti " )
    public abstract LiveData<List<Tessere>> getSecondaryByIdOfUtente_Synchronized(Long idUtenti);
/*================================================================================================*/
/*################################################################################################*/
}
