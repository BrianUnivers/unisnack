package it.tn.unitn.disi.ppsmet.ActivityManager.ValutazionePastoActivity;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.InterfaceViewModel.MenuProfiloUtenteViewModel;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Ordinazioni_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;

public class ValutazionePastoViewModel extends MenuProfiloUtenteViewModel {
/*################################################################################################*/
    private Ordinazioni_Repository rOrdinazioni;
/*================================================================================================*/
/*                                  COSTRUTTORE VIEW MODEL                                        */
/*================================================================================================*/
    /**
     * Questo metodo costruttore del viewmode inizializza le repository che si interfacciano con entrambi
     * i database sia interno che estreno a seconda delle operazioni che vengono richeste. Che verranno
     * utilizate dall'activity e i suoi fragment.
     * @param application
     */
    public ValutazionePastoViewModel(@NonNull Application application) {
        super(application);
        rOrdinazioni  = new Ordinazioni_Repository(application);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  AZZIONI LEGATE ALLA GESITIONE DEI DATABASE                    */
/*################################################################################################*/
    private final static Long ID_PRANZO = 1l;
    private final static Long ID_CENA   = 2l;
/*================================================================================================*/
/*                                  GET LAST ORDINAZIONE                                          */
/*================================================================================================*/
    /**
     * Questo metodo permette di individuare l'ultima ordinazione fatta e svolta. Se è svolta lo stesso
     * giorno controlla se è passato un ceroto orario prima di poterla visualizzare, così da essere certi
     * che l'utente abbia avuto la possibilità di compiere l'azione.
     * @param idUtente
     * @return
     */
    public LiveData<Ordinazioni> getLastOrdinazioni(Long idUtente){
        List<Long> idTipoPasto = new LinkedList<>();
        Date date = new Date();
        //Set Post Pranzo
        Calendar calPostParanzo = Calendar.getInstance();
        calPostParanzo.setTime(date);
        calPostParanzo.set(Calendar.MILLISECOND,0);
        calPostParanzo.set(Calendar.SECOND,0);
        calPostParanzo.set(Calendar.MINUTE,0);
        calPostParanzo.set(Calendar.HOUR_OF_DAY,15);
        //Set Post Cena
        Calendar calPostCena = Calendar.getInstance();
        calPostCena.setTime(calPostParanzo.getTime());
        calPostCena.set(Calendar.HOUR_OF_DAY,22);
        //Paranzo fatto
        if(date.after(calPostParanzo.getTime())){
            idTipoPasto.add(ID_PRANZO);
        }
        //Cena Fatta
        if(date.after(calPostCena.getTime())){
            idTipoPasto.add(ID_CENA);
            Log.e("ROOD","OJ");
        }
        Log.e("ROOD","sss");
        return rOrdinazioni.getLastEatBeforeTodayByIdOfUtenti_ListIdTipoPasto(idUtente,idTipoPasto);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  GET ORDINAZIONE PIETANZE DELLA MENSE CON VOTO BY ID ORDINAZIONE*/
/*================================================================================================*/
    public LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> getPietanzeOrdinateByIdOfOrdinazioni(Long idOrdinazioni){
        return rOrdinazioni.getPietanzeOrdinateByIdOfOrdinazioni(idOrdinazioni);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  AGGIORNAMENTO ORDINAZIONE E  PIETANZE DELLA MENSE CON VOTO  */
/*================================================================================================*/
    public void updateOrdinazioneEPietanze(Ordinazioni ordinazioni,List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze){
        rOrdinazioni.update(ordinazioni,listaPietanze);
    }
/*================================================================================================*/
/*################################################################################################*/
}
