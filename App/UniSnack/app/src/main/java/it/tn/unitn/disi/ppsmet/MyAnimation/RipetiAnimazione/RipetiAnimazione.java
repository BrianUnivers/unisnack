package it.tn.unitn.disi.ppsmet.MyAnimation.RipetiAnimazione;

import android.graphics.drawable.Animatable2;
import android.graphics.drawable.Drawable;

import androidx.vectordrawable.graphics.drawable.Animatable2Compat;
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat;

import it.tn.unitn.disi.ppsmet.MyAnimation.MyAnimationView;

public class RipetiAnimazione {
//##################################################################################################
//                                      GET RIPETIZIONE ANIMAZIONE
//##################################################################################################
    private Integer numRipetizioni;
    private MyAnimatable2CompatAnimationCallback ripetizioneCompat=null;
    private MyAnimatable2AnimationCallback       ripetizione=null;
//==================================================================================================
//                                      COSTRUTTORE
//==================================================================================================
    public RipetiAnimazione(Integer numRipetizioni) {
        this.numRipetizioni=numRipetizioni;
    }
//==================================================================================================

//==================================================================================================
//                                      GETTER
//==================================================================================================
    public Integer getNumRipetizioni() {
        return numRipetizioni;
    }
//==================================================================================================

//==================================================================================================
//                                      GET ANIMAZIONI RIPETUTE
//==================================================================================================
    public Animatable2Compat.AnimationCallback getRipetizioneCompat(MyAnimationView animazione){
        if(ripetizioneCompat==null){
            ripetizioneCompat=new MyAnimatable2CompatAnimationCallback(animazione,this.numRipetizioni);
        }
        return ripetizioneCompat;
    }
//--------------------------------------------------------------------------------------------------
    public Animatable2.AnimationCallback getRipetizione(MyAnimationView animazione){
        if(ripetizione==null){
            ripetizione= new MyAnimatable2AnimationCallback(animazione,this.numRipetizioni);
        }
        return ripetizione;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                      CLASSE INTERNA PER LE RIPETIZIONI
//                                      Animatable2Compat.AnimationCallback
//##################################################################################################
    private class MyAnimatable2CompatAnimationCallback extends Animatable2Compat.AnimationCallback{
//==================================================================================================
        private Integer numRipetizioni;
        private Integer ripetute=0;
        private MyAnimationView animazione;
//==================================================================================================
//==================================================================================================
//                                      COSTRUTTORE
//==================================================================================================
        public MyAnimatable2CompatAnimationCallback(MyAnimationView animazione, Integer numRipetizioni) {
            this.numRipetizioni = numRipetizioni;
            this.animazione = animazione;
        }
//==================================================================================================
//==================================================================================================
//                                      AZZIONE AL TERMINE DELLA ANIMAZIONE
//==================================================================================================
        @Override
        public void onAnimationEnd(Drawable drawable) {
            ripetute++;
            ripetizioni(animazione,numRipetizioni,ripetute);
        }
//==================================================================================================
    }
//##################################################################################################


//##################################################################################################
//                                      CLASSE INTERNA PER LE RIPETIZIONI
//                                      Animatable2Compat.AnimationCallback
//##################################################################################################
    private class MyAnimatable2AnimationCallback extends Animatable2.AnimationCallback{
//==================================================================================================
        private Integer numRipetizioni;
        private Integer ripetute=0;
        private MyAnimationView animazione;
//==================================================================================================
//==================================================================================================
//                                      COSTRUTTORE
//==================================================================================================
        public MyAnimatable2AnimationCallback(MyAnimationView animazione, Integer numRipetizioni) {
            this.numRipetizioni = numRipetizioni;
            this.animazione = animazione;
        }
//==================================================================================================
//==================================================================================================
//                                      AZZIONE AL TERMINE DELLA ANIMAZIONE
//==================================================================================================
        @Override
        public void onAnimationEnd(Drawable drawable) {
            ripetute++;
            ripetizioni(animazione,numRipetizioni,ripetute);
        }
//==================================================================================================
    }
//##################################################################################################

//##################################################################################################
//                                      LOGICA COMUNE
//##################################################################################################
    private void ripetizioni(MyAnimationView animazione,Integer numRipetizioni,Integer ripetute) {
        if((animazione!=null) && (numRipetizioni!=null)){
            if (numRipetizioni==MyAnimationView.RIPETIZIONE_INFINITA){
                //RIPETIZIONE INFINITA
                animazione.start();
            }else if((numRipetizioni > 0) && (numRipetizioni>ripetute)){
                //RIPETIZIONE FINITA
                animazione.start();
            }else{
                animazione.fineRipetizione();
            }
        }
    }
//##################################################################################################
}
