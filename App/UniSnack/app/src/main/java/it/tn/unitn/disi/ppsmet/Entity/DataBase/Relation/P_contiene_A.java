package it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;

@Entity(tableName = "tableP_contiene_A",
        primaryKeys = {"id_pietanza",
                       "id_ingredienti"},
        foreignKeys = {
            @ForeignKey(entity = Pietanze.class,
                parentColumns = "id",             //Colanna della classe a qui si rifierisce
                childColumns  = "id_pietanza",    //ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE),
            @ForeignKey(entity = Ingredienti.class,
                    parentColumns = "id",             //Colanna della classe a qui si rifierisce
                    childColumns  = "id_ingredienti", //ForeignKey
                    onUpdate = ForeignKey.CASCADE,
                    onDelete = ForeignKey.CASCADE)},
        indices =  @Index(value = {"id_ingredienti"}))
public class P_contiene_A {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @ColumnInfo(name = "id_pietanza")
    private Long id_pietanza;
    @NonNull
    @ColumnInfo(name = "id_ingredienti")
    private Long id_ingredienti;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    public P_contiene_A(@NonNull Long id_pietanza,
                        @NonNull Long id_ingredienti) {
        this.id_pietanza = id_pietanza;
        this.id_ingredienti = id_ingredienti;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    @NonNull
    public Long getId_pietanza() {
        return id_pietanza;
    }
//--------------------------------------
    public void setId_pietanza(@NonNull Long id_pietanza) {
        this.id_pietanza = id_pietanza;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_ingredienti() {
        return id_ingredienti;
    }
//--------------------------------------
    public void setId_ingredienti(@NonNull Long id_ingredienti) {
        this.id_ingredienti = id_ingredienti;
    }
//==================================================================================================
//##################################################################################################
}
