package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.util.Iterator;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;

public class Deserializer_TipoMenu implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto TipoMenu.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String LOG_TAG = "TipoMenu_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "TipoMenu" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "TipoMenu".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public static TipoMenu estraiDaDataSnapshot(DataSnapshot dataSnapshot){
        TipoMenu tipoMenu = null;
        if(dataSnapshot.exists()){
            try{
                //Campi not null
                Long id     = Long.parseLong( dataSnapshot.getKey());
                String nome = dataSnapshot.child(P_NOME_TIPO).getValue(String.class);
                //Verifica campi not null
                if((id!=null) && (nome!=null)){
                    tipoMenu = new TipoMenu(id,nome);
                }
            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
                tipoMenu = null;
            }
        }
        return tipoMenu;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         GET AN ACTRIBUTE OBJECT FOR AN OTHER                                   */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET AN ACTRIBUTE                                                       */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce, se esiste, un singolo ogetto "TipoMenu" da un attributo di un altro
 * oggetto attraverso una sottoparte dell'oggetto "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "TipoMenu".
 * @param nomeAttributo Questo parametro contine il nome del attributo in cui cercare un siglo oggetto.
 * @return  Il riutato è il primo oggetto, valido se ce ne fossero più d'uno ritorna il primo trovato
 *          mentre se non ce ne sono ritorna null.
 */
    public static TipoMenu getAttributeOf(DataSnapshot dataSnapshot, String nomeAttributo){
        TipoMenu tipoMenu = null;
        DataSnapshot tipoMenuDataSnapshot = dataSnapshot.child(nomeAttributo);
        if(tipoMenuDataSnapshot.exists()){
            Iterator<DataSnapshot> iterator =tipoMenuDataSnapshot.getChildren().iterator();
            while ( (iterator.hasNext()) && (tipoMenu==null) ){//Restituisce il primo oggetto valido
                tipoMenu = estraiDaDataSnapshot(iterator.next());
            }
        }
        return tipoMenu;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, TipoMenu>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Object extends GenericDeserializer_Object<TipoMenu> {
        @Override
        public TipoMenu estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_TipoMenu.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<TipoMenu>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List<TipoMenu> {
        @Override
        public TipoMenu estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_TipoMenu.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
