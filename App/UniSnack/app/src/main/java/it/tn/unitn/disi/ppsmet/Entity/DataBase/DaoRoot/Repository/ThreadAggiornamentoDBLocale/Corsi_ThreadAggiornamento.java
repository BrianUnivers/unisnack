package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Corsi_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Corsi_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_TipoMenu_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.TipoMenu_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;

public class Corsi_ThreadAggiornamento extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA ANNO ACCADEMICO LOCALE           */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "Corsi" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_Corsi";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista dei Corsi del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private List<Corsi> listaCorsi;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * Corsi.
         * @param listaCorsi Questo parametro contiene la lista di tutti gli elementi
         *                        tra qui quelli nuovi e quelli già presenti ma da modificare.
         */
        public ThreadAggiornamento(List<Corsi> listaCorsi) {
            super();
            this.listaCorsi =listaCorsi;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                                  */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella Corsi.");
            aggionaTabellaCorsi(listaCorsi);
            Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella Corsi.");
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param listaCorsi
         */
        private synchronized void aggionaTabellaCorsi(List<Corsi> listaCorsi){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella Corsi.");
            //Dati presenti nel database locale.
            List<Corsi> listaCorsiLocale = corsiDao.getAll();
            if((listaCorsiLocale==null)||(listaCorsiLocale.isEmpty())){
                inserisciListaElementi(listaCorsi);
            }else if((listaCorsi==null)||(listaCorsi.isEmpty())) {
                eliminaListaElementi(listaCorsiLocale);
            }else{
                HashMap<Long,Corsi> hashMapCorsiLocali = convertiListaInHashMap(listaCorsiLocale);

                List<Corsi> listaCorsiNuove = new LinkedList<>();
                List<Corsi> listaCorsiAggiornate = new LinkedList<>();
                //Serve solo per poter conoscere quali elementi caccelare tra quelli già presenti localmente.
                List<Corsi> listaVechieCorsiDaMantenere = new LinkedList<>();

                Iterator<Corsi> iterator = listaCorsi.iterator();
                while (iterator.hasNext()){
                    Corsi corsi =iterator.next();
                    if(corsi!=null){
                        Corsi corsiVecchia = hashMapCorsiLocali.get(corsi.getId());
                        if(corsiVecchia==null){
                            listaCorsiNuove.add(corsi);
                        }else if(!corsiVecchia.equals(corsi)){
                            listaCorsiAggiornate.add(corsi);
                            listaVechieCorsiDaMantenere.add(corsiVecchia);
                        }else{
                            listaVechieCorsiDaMantenere.add(corsiVecchia);
                        }
                    }
                }
                //MANTENGO SOLO GLI ELEMNETI CHE DEVO CANCELLARE
                listaCorsiLocale.removeAll(listaVechieCorsiDaMantenere);
                inserisciListaElementi(listaCorsiNuove);
                aggiornaListaElementi(listaCorsiAggiornate);
                eliminaListaElementi(listaCorsiLocale);
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         GENERAZIONE HASH MAP FROM LIST                                         */
/*================================================================================================*/
        private synchronized HashMap<Long,Corsi> convertiListaInHashMap(List<Corsi> lista){
            HashMap<Long,Corsi> ris=new HashMap<>();
            if(lista!=null){
                Iterator<Corsi> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Corsi elemento = iterator.next();
                    if((elemento!=null) && (elemento.getId()!=null)){
                        ris.put(elemento.getId(),elemento);
                    }
                }
            }
            return ris;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO INSERIMENTO NUOVI ELEMENTI                                      */
/*================================================================================================*/
        private synchronized void inserisciListaElementi(List<Corsi> lista){
            if(lista!=null){
                Iterator<Corsi> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Corsi elemento=iterator.next();
                    if(elemento!=null){
                        corsiDao.insert(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO AGGIORNAMENTO ELEMENTI PRESENTI                                 */
/*================================================================================================*/
        private synchronized void aggiornaListaElementi(List<Corsi> lista){
            if(lista!=null){
                Iterator<Corsi> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Corsi elemento=iterator.next();
                    if(elemento!=null){
                        corsiDao.update(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaListaElementi(List<Corsi> lista){
            if(lista!=null){
                Iterator<Corsi> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Corsi elemento=iterator.next();
                    if(elemento!=null){
                        corsiDao.delete(elemento.getId());
                    }
                }
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_Corsi_Dao fbCorsiDao = new FB_Corsi_Dao();
    //QUERY FIREBASE
    private static LiveData<List<Corsi>> fbAllCorsi=null;
    //ROOM DATABASE
    protected static Corsi_Dao corsiDao = null;
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaCorsi(){
        if(fbAllCorsi==null){
            fbAllCorsi= fbCorsiDao.getAll_Synchronized();
        }
        if(corsiDao==null){
            corsiDao = db.CorsiDao();
        }
        if(!fbAllCorsi.hasObservers()){//SE NON HA già osservatori per rilevare aggiornamenti
            fbAllCorsi.observeForever(new Observer<List<Corsi>>() {
                @Override
                public void onChanged(List<Corsi> lista) {
                    if((lista!=null)){
                        executor.execute(new ThreadAggiornamento(lista));
                    }
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Corsi_ThreadAggiornamento(Application application) {
        super(application);
        gestioneAggiornametoTabellaCorsi();
    }
/*================================================================================================*/
/*################################################################################################*/
}
