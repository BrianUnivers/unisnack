package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;

public class Deserializer_AnnoAccademico implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto AnnoAccademico.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String DATA_FORMAT = "dd/MM/yyyy";
    private static String LOG_TAG = "AnnoAccademico_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "AnnoAccademico" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "AnnoAccademico".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public static AnnoAccademico estraiDaDataSnapshot(DataSnapshot dataSnapshot){
        AnnoAccademico annoAccademico = null;
        if(dataSnapshot.exists()){
            try {
                //Campi not null
                Long id = null;
                if (dataSnapshot.child(P_KEY).exists()){
                    id = dataSnapshot.child(P_KEY).getValue(Long.class);
                }else{
                    id = Long.parseLong( dataSnapshot.getKey());
                }
                Date dataInizio =  new SimpleDateFormat(DATA_FORMAT)
                                        .parse(dataSnapshot.child(P_DATA_INIZIO).getValue(String.class));
                Date dataFine   = new SimpleDateFormat(DATA_FORMAT)
                                        .parse(dataSnapshot.child(P_DATA_FINE).getValue(String.class));
                //Verifica campi not null
                if((id!=null) && (dataInizio!=null) && (dataFine!=null)){
                    annoAccademico = new AnnoAccademico(id,dataInizio,dataFine);
                }
            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
                annoAccademico = null;
            }
        }
        return annoAccademico;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         GET AN ACTRIBUTE OBJECT FOR AN OTHER                                   */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET AN ACTRIBUTE                                                       */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce, se esiste, un singolo ogetto "AnnoAccademico" da un attributo di un altro
 * oggetto attraverso una sottoparte dell'oggetto "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "AnnoAccademico".
 * @return  Il riutato è il primo oggetto, valido se ce ne fossero più d'uno ritorna il primo trovato
 *          mentre se non ce ne sono ritorna null.
 */
    public static AnnoAccademico getAttributeOf(DataSnapshot dataSnapshot,String nomeAttributo){
        AnnoAccademico annoAccademico = null;
        if(dataSnapshot.child(nomeAttributo).exists()){
            DataSnapshot dataSnapshotAA = dataSnapshot.child(nomeAttributo);
            annoAccademico = estraiDaDataSnapshot(dataSnapshotAA);
        }
        return annoAccademico;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, AnnoAccademico>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Object extends GenericDeserializer_Object<AnnoAccademico> {
        @Override
        public AnnoAccademico estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_AnnoAccademico.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<AnnoAccademico>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List<AnnoAccademico> {
        @Override
        public AnnoAccademico estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_AnnoAccademico.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
