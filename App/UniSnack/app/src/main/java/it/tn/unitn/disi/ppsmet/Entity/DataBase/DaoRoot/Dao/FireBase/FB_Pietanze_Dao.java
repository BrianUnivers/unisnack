package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import android.database.sqlite.SQLiteConstraintException;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_MFornisceP;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Mense;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_PastoDelMese;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.M_fornisce_P;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.P_contiene_A;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.PastoDelMese;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

/*################################################################################################*/
/*################################################################################################*/
public class FB_Pietanze_Dao extends FB_PietanzeInsertExist_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<Pietanze>> getAll_OneTime(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_PIETANZE);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getAll(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_PIETANZE);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getAll_Synchronized(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_PIETANZE);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    public LiveData<Pietanze> getByPrimaryKey_OneTime(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_PIETANZE)
                .child(id.toString());
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Pietanze> getByPrimaryKey(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_PIETANZE)
                .child(id.toString());
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Pietanze> getByPrimaryKey_Synchronized(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_PIETANZE)
                .child(id.toString());
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_Object());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID TIPO PIETANZA                                         */
/*================================================================================================*/
    public LiveData<List<Pietanze>> getByIdOfTipoPietanza_OneTime(@NonNull Long idTipoPietanza){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PIETANZE)
                .orderByChild(L_TIPO_PIETANZA+"/"+P_KEY)
                .equalTo(idTipoPietanza);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfTipoPietanza(@NonNull Long idTipoPietanza){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PIETANZE)
                .orderByChild(L_TIPO_PIETANZA+"/"+P_KEY)
                .equalTo(idTipoPietanza);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfTipoPietanza_Synchronized(@NonNull Long idTipoPietanza){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PIETANZE)
                .orderByChild(L_TIPO_PIETANZA+"/"+P_KEY)
                .equalTo(idTipoPietanza);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List());
    }
/*================================================================================================*/

/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU'                                 */
/*                          SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR                         */
/*================================================================================================*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_SpecificMoment_OneTime(
                                                                    @NonNull Long idMensa,
                                                                    @NonNull Long idTipoMenu,
                                                                    @NonNull Integer anno,
                                                                    @NonNull Integer mese,
                                                                    @NonNull Integer giornoSettimana){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_SOLO_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(idMensa.toString())
                .child(L_TIPO_MENU)
                .child(idTipoMenu.toString())
                .child(builderKeyFromFornisce(idMensa,idTipoMenu,anno,mese,giornoSettimana))
                .child(L_PIETANZE);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_SpecificMoment(
                                                                    @NonNull Long idMensa,
                                                                    @NonNull Long idTipoMenu,
                                                                    @NonNull Integer anno,
                                                                    @NonNull Integer mese,
                                                                    @NonNull Integer giornoSettimana){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_SOLO_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(idMensa.toString())
                .child(L_TIPO_MENU)
                .child(idTipoMenu.toString())
                .child(builderKeyFromFornisce(idMensa,idTipoMenu,anno,mese,giornoSettimana))
                .child(L_PIETANZE);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_SpecificMoment_Synchronized(
                                                                    @NonNull Long idMensa,
                                                                    @NonNull Long idTipoMenu,
                                                                    @NonNull Integer anno,
                                                                    @NonNull Integer mese,
                                                                    @NonNull Integer giornoSettimana){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_SOLO_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(idMensa.toString())
                .child(L_TIPO_MENU)
                .child(idTipoMenu.toString())
                .child(builderKeyFromFornisce(idMensa,idTipoMenu,anno,mese,giornoSettimana))
                .child(L_PIETANZE);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU' SPECIFIC DAY                    */
/*================================================================================================*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_SpecificDay_OneTime(
                                                                            @NonNull Long idMensa,
                                                                            @NonNull Long idTipoMenu,
                                                                            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getByIdOfMensa_TipoMenu_SpecificMoment_OneTime(idMensa,idTipoMenu,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_SpecificDay(    @NonNull Long idMensa,
                                                                            @NonNull Long idTipoMenu,
                                                                            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getByIdOfMensa_TipoMenu_SpecificMoment(idMensa,idTipoMenu,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_SpecificDay_Synchronized(   @NonNull Long idMensa,
                                                                                        @NonNull Long idTipoMenu,
                                                                                        @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getByIdOfMensa_TipoMenu_SpecificMoment_Synchronized(idMensa,idTipoMenu,anno,mese,giornoSettimana);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU' AND TIPO PIETANZA               */
/*                          SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR                         */
/*================================================================================================*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_OneTime(
                                                                    @NonNull Long idMensa,
                                                                    @NonNull Long idTipoMenu,
                                                                    @NonNull Long idTipoPietanza,
                                                                    @NonNull Integer anno,
                                                                    @NonNull Integer mese,
                                                                    @NonNull Integer giornoSettimana){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_SOLO_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(idMensa.toString())
                .child(L_TIPO_MENU)
                .child(idTipoMenu.toString())
                .child(builderKeyFromFornisce(idMensa,idTipoMenu,anno,mese,giornoSettimana))
                .child(L_PIETANZE)
                .orderByChild(L_TIPO_PIETANZA+"/"+P_KEY)
                .equalTo(idTipoPietanza);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment(
                                                                    @NonNull Long idMensa,
                                                                    @NonNull Long idTipoMenu,
                                                                    @NonNull Long idTipoPietanza,
                                                                    @NonNull Integer anno,
                                                                    @NonNull Integer mese,
                                                                    @NonNull Integer giornoSettimana){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_SOLO_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(idMensa.toString())
                .child(L_TIPO_MENU)
                .child(idTipoMenu.toString())
                .child(builderKeyFromFornisce(idMensa,idTipoMenu,anno,mese,giornoSettimana))
                .child(L_PIETANZE)
                .orderByChild(L_TIPO_PIETANZA+"/"+P_KEY)
                .equalTo(idTipoPietanza);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_Synchronized(
                                                                    @NonNull Long idMensa,
                                                                    @NonNull Long idTipoMenu,
                                                                    @NonNull Long idTipoPietanza,
                                                                    @NonNull Integer anno,
                                                                    @NonNull Integer mese,
                                                                    @NonNull Integer giornoSettimana){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_SOLO_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(idMensa.toString())
                .child(L_TIPO_MENU)
                .child(idTipoMenu.toString())
                .child(builderKeyFromFornisce(idMensa,idTipoMenu,anno,mese,giornoSettimana))
                .child(L_PIETANZE)
                .orderByChild(L_TIPO_PIETANZA+"/"+P_KEY)
                .equalTo(idTipoPietanza);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU' AND TIPO PIETANZA SPECIFIC DAY  */
/*================================================================================================*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay_OneTime(
                                                                        @NonNull Long idMensa,
                                                                        @NonNull Long idTipoMenu,
                                                                        @NonNull Long idTipoPietanza,
                                                                        @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_OneTime(idMensa,idTipoMenu,idTipoPietanza,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay(
                                                                        @NonNull Long idMensa,
                                                                        @NonNull Long idTipoMenu,
                                                                        @NonNull Long idTipoPietanza,
                                                                        @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment(idMensa,idTipoMenu,idTipoPietanza,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay_Synchronized(
                                                                        @NonNull Long idMensa,
                                                                        @NonNull Long idTipoMenu,
                                                                        @NonNull Long idTipoPietanza,
                                                                        @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_Synchronized(   idMensa,idTipoMenu,idTipoPietanza,
                anno,mese,giornoSettimana);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID PASTO DEL MESE                                        */
/*================================================================================================*/
    public LiveData<List<Pietanze>> getByIdOfPastoDelMese_OneTime(PastoDelMese pastoDelMese){
        MediatorLiveData<List<Pietanze>> ris = new MediatorLiveData<>();
        //_OneTime
        ris.addSource(FB_Utenti_Dao.getByPrimaryKey_OneTime(pastoDelMese.getId_utente()),
                new Observer<Utenti>() {
                    @Override
                    public void onChanged(Utenti utenti) {
                        //_OneTime
                        ris.addSource(getByIdOfPastoDelMese_OneTime(utenti, pastoDelMese.getMese(), pastoDelMese.getAnno()),
                                new Observer<List<Pietanze>>() {
                                    @Override
                                    public void onChanged(List<Pietanze> pietanzes) {
                                        ris.setValue(pietanzes);
                                    }
                                });
                    }
                });
        return ris;
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfPastoDelMese(PastoDelMese pastoDelMese){
        MediatorLiveData<List<Pietanze>> ris = new MediatorLiveData<>();
        //_
        ris.addSource(FB_Utenti_Dao.getByPrimaryKey(pastoDelMese.getId_utente()),
                new Observer<Utenti>() {
                    @Override
                    public void onChanged(Utenti utenti) {
                        //_
                        ris.addSource(getByIdOfPastoDelMese(utenti, pastoDelMese.getMese(), pastoDelMese.getAnno()),
                                new Observer<List<Pietanze>>() {
                                    @Override
                                    public void onChanged(List<Pietanze> pietanzes) {
                                        ris.setValue(pietanzes);
                                    }
                                });
                    }
                });
        return ris;
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfPastoDelMese_Synchronized(PastoDelMese pastoDelMese){
        MediatorLiveData<List<Pietanze>> ris = new MediatorLiveData<>();
        //_Synchronized
        ris.addSource(FB_Utenti_Dao.getByPrimaryKey_Synchronized(pastoDelMese.getId_utente()),
                new Observer<Utenti>() {
                    @Override
                    public void onChanged(Utenti utenti) {
                        //_Synchronized
                        ris.addSource(getByIdOfPastoDelMese_Synchronized(utenti, pastoDelMese.getMese(), pastoDelMese.getAnno()),
                                new Observer<List<Pietanze>>() {
                                    @Override
                                    public void onChanged(List<Pietanze> pietanzes) {
                                        ris.setValue(pietanzes);
                                    }
                                });
                    }
                });
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfPastoDelMese_OneTime(@NonNull Utenti utenteProponete,
                                                                     @NonNull Short mese,
                                                                     @NonNull Short anno){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE_PROPOSTO_DAL_UTENTE)
                .child(builderKeyFromUtenti(utenteProponete))
                .child(L_PROPOSTE)
                .orderByChild(P_ANNO__MESE)
                .equalTo(builderKeyFromAnnoMese(anno,mese));
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List_In_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfPastoDelMese( @NonNull Utenti utenteProponete,
                                                              @NonNull Short mese,
                                                              @NonNull Short anno){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE_PROPOSTO_DAL_UTENTE)
                .child(builderKeyFromUtenti(utenteProponete))
                .child(L_PROPOSTE)
                .orderByChild(P_ANNO__MESE)
                .equalTo(builderKeyFromAnnoMese(anno,mese));
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List_In_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfPastoDelMese_Synchronized(@NonNull Utenti utenteProponete,
                                                                       @NonNull Short mese,
                                                                       @NonNull Short anno){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE_PROPOSTO_DAL_UTENTE)
                .child(builderKeyFromUtenti(utenteProponete))
                .child(L_PROPOSTE)
                .orderByChild(P_ANNO__MESE)
                .equalTo(builderKeyFromAnnoMese(anno,mese));
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List_In_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ANNO E MESE                                              */
/*================================================================================================*/
public LiveData<List<Pietanze>> getByIdOfPastoDelMese_OneTime(@NonNull Short mese,
                                                              @NonNull Short anno){
    Query ref = super.getReferenceFireBase().getReference()
            .child(L_PASTO_DEL_MESE)
            .orderByChild(P_ANNO__MESE)
            .equalTo(builderKeyFromAnnoMese(anno,mese));
    FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
    return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List_In_List());
}
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfPastoDelMese( @NonNull Short mese,
                                                           @NonNull Short anno){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE)
                .orderByChild(P_ANNO__MESE)
                .equalTo(builderKeyFromAnnoMese(anno,mese));
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List_In_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Pietanze>> getByIdOfPastoDelMese_Synchronized(@NonNull Short mese,
                                                                       @NonNull Short anno){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PASTO_DEL_MESE)
                .orderByChild(P_ANNO__MESE)
                .equalTo(builderKeyFromAnnoMese(anno,mese));
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Pietanze.Deserializer_List_In_List());
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                          METODO GET MFornisceP by ID MENSA AND TIPO MENU'                      */
/*================================================================================================*/
    public LiveData<List<M_fornisce_P>> getMForniscePByIdOfMensa_TipoMenu_OneTime(
            @NonNull Long idMensa,
            @NonNull Long idTipoMenu){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(idMensa.toString())
                .child(L_TIPO_MENU)
                .child(idTipoMenu.toString());
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_MFornisceP.Deserializer_List(idMensa,idTipoMenu));
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<M_fornisce_P>> getMForniscePByIdOfMensa_TipoMenu(
            @NonNull Long idMensa,
            @NonNull Long idTipoMenu){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(idMensa.toString())
                .child(L_TIPO_MENU)
                .child(idTipoMenu.toString());
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_MFornisceP.Deserializer_List(idMensa,idTipoMenu));
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<M_fornisce_P>> getMForniscePByIdOfMensa_TipoMenu_Synchronized(
            @NonNull Long idMensa,
            @NonNull Long idTipoMenu){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(idMensa.toString())
                .child(L_TIPO_MENU)
                .child(idTipoMenu.toString());
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_MFornisceP.Deserializer_List(idMensa,idTipoMenu));
    }
/*================================================================================================*/
/*################################################################################################*/
}

/*################################################################################################*/
/*################################################################################################*/
/*################################################################################################*/
 class FB_PietanzeInsertExist_Dao  extends ConnesioneAFirebase<Pietanze> {
     protected FB_Utenti_Dao FB_Utenti_Dao =  new FB_Utenti_Dao();
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
 /*TODO    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(Pietanze pietanze);
/*------------------------------------------------------------------------*/
/*TODO     @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    public long insertPietazaAlMenuDellaMensa(Pietanze pietanze,
                                              Mense mense,
                                              TipoMenu tipoMenu,
                                              Integer anno,
                                              Integer mese) {
        if (mese <= 0 || mese > 12) {
            throw new SQLiteConstraintException("Mese inserito errato deve essere compreso tra 1 e 12 mentre e' " + mese + ".");
        }
        Long id = exist(pietanze);
        if (id == null) {
            id = this.insert(pietanze);
        }
        M_fornisce_P relazione = new M_fornisce_P(mense.getId(), id, mese, anno, tipoMenu.getId());
        this.insert(relazione);
        return id;
    }
/*------------------------------------------------------------------------*/
/*TODO     @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    public Long insertAssociazioneIngrediente(Pietanze pietanze, Ingredienti ingredienti) {
        List<Ingredienti> listaIngredienti = new LinkedList<Ingredienti>();
        listaIngredienti.add(ingredienti);
        List<Long> ris = this.insertAssociazioneIngrediente(pietanze, listaIngredienti);
        if (ris.isEmpty()) {
            throw new SQLiteConstraintException("Non è stato possible creare l'associazione.");
        }
        return ris.get(0);
    }
/*------------------------------------------------------------------------*/
/*TODO   @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    public List<Long> insertAssociazioneIngrediente(Pietanze pietanze, List<Ingredienti> ingredienti) {
        Iterator<Ingredienti> iterator = ingredienti.iterator();
        List<Long> ris = new ArrayList<>();
        while (iterator.hasNext()) {
            P_contiene_A contiene = new P_contiene_A(pietanze.getId(), iterator.next().getId());
            Long id = this.inset(contiene);
            ris.add(id);
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
 /*TODO    @Insert(onConflict = OnConflictStrategy.ABORT)
    protected abstract long insert(M_fornisce_P m_fornisce_p);

/*------------------------------------------------------------------------*/
 /*TODO    @Insert(onConflict = OnConflictStrategy.IGNORE)
    protected abstract long inset(P_contiene_A contiene);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         EXIST                                                                  */
/*================================================================================================*/
 /*TODO    @Transaction
    public long exist(Pietanze pietanze) {
        return this.exist(pietanze.getNome(),
                pietanze.getCalorie(),
                pietanze.getCarboidrati(),
                pietanze.getGrassi(),
                pietanze.getProteine(),
                pietanze.getImmagine(),
                pietanze.getId_tipoPietanza());
    }

/*------------------------------------------------------------------------------------------------*/
 /*TODO    @Query("SELECT P.id " +
            "FROM tablePietanze AS P " +
            "WHERE   P.calorie == :calorie AND " +
            "P.carboidrati == :carboidrati AND " +
            "P.grassi == :grassi AND " +
            "P.id_tipoPietanza == :id_tipoPietanza AND " +
            "P.immagine == :immagine AND " +
            "P.nome == :nome AND " +
            "P.proteine == :proteine")
    protected abstract long exist(String nome,
                                  Double calorie,
                                  Double carboidrati,
                                  Double grassi,
                                  Double proteine,
                                  byte[] immagine,
                                  Long id_tipoPietanza);
/*================================================================================================*/
/*################################################################################################*/
}