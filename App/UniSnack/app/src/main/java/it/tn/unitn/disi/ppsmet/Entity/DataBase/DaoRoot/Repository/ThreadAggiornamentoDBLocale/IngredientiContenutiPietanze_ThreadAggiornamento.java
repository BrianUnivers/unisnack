package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Ingredienti_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Pietanze_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Ingredienti_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.P_Contiene_A_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Pietanze_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.P_contiene_A;
import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

public class IngredientiContenutiPietanze_ThreadAggiornamento extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA ANNO ACCADEMICO LOCALE           */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "IngredientiContenutiPietanze" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_IngredientiContenutiPietanze";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista dei Pietanze del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private List<Ingredienti> listaIngredienti;
        private Long              idPietanze;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * P_contiene_A.
         * @param listaIngredienti Questo parametro contiene la lista di tutti gli elementi
         *                        tra qui quelli nuovi e quelli già presenti ma da modificare.
         */
        public ThreadAggiornamento(List<Ingredienti> listaIngredienti,Long idPietanze) {
            super();
            this.listaIngredienti =listaIngredienti;
            this.idPietanze = idPietanze;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                                  */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella Ingredienti.");
            aggionaTabellaIngredienti(listaIngredienti,idPietanze);
            Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella Ingredienti.");
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param listaIngredienti
         */
        private synchronized void aggionaTabellaIngredienti(List<Ingredienti> listaIngredienti,Long idPietanze){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella Ingredienti.");
            //Dati presenti nel database locale.
            List<Ingredienti> listaIngredientiLocale = ingredientiDao.getByIdOfPietanza(idPietanze);
            if((listaIngredientiLocale==null)||(listaIngredientiLocale.isEmpty())){
                inserisciListaElementi(listaIngredienti,idPietanze);
            }else if((listaIngredienti==null)||(listaIngredienti.isEmpty())) {
                eliminaListaElementi(listaIngredientiLocale,idPietanze);
            }else{
                HashMap<Long,Ingredienti> hashMapIngredientiLocali = convertiListaInHashMap(listaIngredientiLocale);

                List<Ingredienti> listaIngredientiNuove = new LinkedList<>();
                List<Ingredienti> listaIngredientiAggiornate = new LinkedList<>();
                //Serve solo per poter conoscere quali elementi caccelare tra quelli già presenti localmente.
                List<Ingredienti> listaVechieIngredientiDaMantenere = new LinkedList<>();

                Iterator<Ingredienti> iterator = listaIngredienti.iterator();
                while (iterator.hasNext()){
                    Ingredienti ingredienti =iterator.next();
                    if(ingredienti!=null){
                        Ingredienti ingredientiVecchia = hashMapIngredientiLocali.get(ingredienti.getId());
                        if(ingredientiVecchia==null){
                            listaIngredientiNuove.add(ingredienti);
                        }else if(!ingredientiVecchia.equals(ingredienti)){
                            listaIngredientiAggiornate.add(ingredienti);
                            listaVechieIngredientiDaMantenere.add(ingredientiVecchia);
                        }else{
                            listaVechieIngredientiDaMantenere.add(ingredientiVecchia);
                        }
                    }
                }
                //MANTENGO SOLO GLI ELEMNETI CHE DEVO CANCELLARE
                listaIngredientiLocale.removeAll(listaVechieIngredientiDaMantenere);
                inserisciListaElementi(listaIngredientiNuove,idPietanze);
                aggiornaListaElementi(listaIngredientiAggiornate,idPietanze);
                eliminaListaElementi(listaIngredientiLocale,idPietanze);
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         GENERAZIONE HASH MAP FROM LIST                                         */
/*================================================================================================*/
        private synchronized HashMap<Long,Ingredienti> convertiListaInHashMap(List<Ingredienti> lista){
            HashMap<Long,Ingredienti> ris=new HashMap<>();
            if(lista!=null){
                Iterator<Ingredienti> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Ingredienti elemento = iterator.next();
                    if((elemento!=null) && (elemento.getId()!=null)){
                        ris.put(elemento.getId(),elemento);
                    }
                }
            }
            return ris;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO INSERIMENTO NUOVI ELEMENTI                                      */
/*================================================================================================*/
        private synchronized void inserisciListaElementi(List<Ingredienti> lista,Long idPietanze){
            if(lista!=null){
                Iterator<Ingredienti> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Ingredienti elemento=iterator.next();
                    if(elemento!=null){
                        PContieneADao.insert(new P_contiene_A(idPietanze,elemento.getId()));
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO AGGIORNAMENTO ELEMENTI PRESENTI                                 */
/*================================================================================================*/
        private synchronized void aggiornaListaElementi(List<Ingredienti> lista,Long idPietanze){
            if(lista!=null){
                Iterator<Ingredienti> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Ingredienti elemento=iterator.next();
                    if(elemento!=null){
                        PContieneADao.update(new P_contiene_A(idPietanze,elemento.getId()));
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaListaElementi(List<Ingredienti> lista,Long idPietanze){
            if(lista!=null){
                Iterator<Ingredienti> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Ingredienti elemento=iterator.next();
                    if(elemento!=null){
                        PContieneADao.delete(idPietanze,elemento.getId());
                    }
                }
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_Ingredienti_Dao fbIngredientiDao = new FB_Ingredienti_Dao();
    //QUERY FIREBASE
    private static LiveData<List<Pietanze>> AllPietanze=null;
    //ROOM DATABASE
    protected static P_Contiene_A_Dao PContieneADao= null;
    public static Ingredienti_Dao     ingredientiDao = null;
    public static Pietanze_Dao        pietanzeDao    = null;
/*------------------------------------------------------------------------------------------------*/
    //DIPENDENZE
    protected static Pietanze_ThreadAggiornamento dipendezaPietanze                           = null;
    protected static IngredientiEAllergeni_ThreadAggiornamento dipendezaIngredientiEAllergeni = null;
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaIngredienti(){
        if(ingredientiDao==null){
            ingredientiDao = db.IngredientiDao();
        }
        if(PContieneADao==null){
            PContieneADao = db.PContieneADao();
        }
        if(pietanzeDao==null){
            pietanzeDao = db.PietanzeDao();
        }
        if(AllPietanze==null){
            AllPietanze= pietanzeDao.getAll_Synchronized();
        }
        if(!AllPietanze.hasObservers()){//SE NON HA già osservatori per rilevare aggiornamenti
            AllPietanze.observeForever(new Observer<List<Pietanze>>() {
                private  List<Pietanze>  lastListPietanze = new LinkedList<>();
                private  List<Pietanze>  temp = new LinkedList<>();
                @Override
                public void onChanged(List<Pietanze> listaPietanze) {
                    if((listaPietanze!=null)){
                        temp.clear();
                        temp.addAll(listaPietanze);
                        if(!lastListPietanze.isEmpty()){
                            listaPietanze.removeAll(lastListPietanze);
                        }
                        //Solo quelli nuovi
                        Iterator<Pietanze> iterator = listaPietanze.iterator();
                        while (iterator.hasNext()){
                            Pietanze pietanze =iterator.next();
                            if((pietanze!=null) && (pietanze.getId()!=null)){
                                //Aggiungi un altro ossservatore
                                fbIngredientiDao.getByIdOfPietanza_Synchronized(pietanze.getId())
                                        .observeForever(new MyIngredientiContenutiPietanzeObserver(pietanze.getId()));
                            }
                        }
                        lastListPietanze.clear();
                        lastListPietanze.addAll(temp);
                    }
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
private static class MyIngredientiContenutiPietanzeObserver implements Observer<List<Ingredienti>>{
    private Long idPietanze;
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public MyIngredientiContenutiPietanzeObserver(Long idPietanze) {
        this.idPietanze =idPietanze;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO CHIAMATO NEL MOMENTO DEL CAMBIAMNETO                            */
/*================================================================================================*/
    @Override
    public void onChanged(List<Ingredienti> lista) {
        if((lista!=null)){
            executor.execute(new ThreadAggiornamento(lista,this.idPietanze));
        }
    }
/*================================================================================================*/
}
/*################################################################################################*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public IngredientiContenutiPietanze_ThreadAggiornamento(Application application) {
        super(application);
        if(dipendezaIngredientiEAllergeni==null){
            dipendezaIngredientiEAllergeni = new IngredientiEAllergeni_ThreadAggiornamento(application);
        }
        if(dipendezaPietanze==null){
            dipendezaPietanze = new Pietanze_ThreadAggiornamento(application);
        }

        gestioneAggiornametoTabellaIngredienti();
    }
/*================================================================================================*/
/*################################################################################################*/
}
