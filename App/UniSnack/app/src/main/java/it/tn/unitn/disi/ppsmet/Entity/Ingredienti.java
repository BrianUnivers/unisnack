package it.tn.unitn.disi.ppsmet.Entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity(tableName = "tableIngredienti")
public class Ingredienti {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @PrimaryKey
    private Long   id;
    @NonNull
    @ColumnInfo(name = "nome")
    private String nome;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    @Ignore
    public Ingredienti() {}
//--------------------------------------------------------------------------------------------------
    public Ingredienti(Long id,
                       @NonNull String nome) {
        this.id = id;
        this.nome = nome;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    public Long getId() {
        return id;
    }
//--------------------------------------
    public void setId(Long id) {
        this.id = id;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getNome() {
        return nome;
    }
//--------------------------------------
    public void setNome(@NonNull String nome) {
        this.nome = nome;
    }
//==================================================================================================
//==================================================================================================
//                                      EQUALS E HASH_CODE
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ingredienti)) return false;
        Ingredienti that = (Ingredienti) o;
        return Objects.equals(getId(), that.getId()) &&
                getNome().equals(that.getNome());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNome());
    }
//==================================================================================================
//##################################################################################################
}
