package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Ingredienti_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.IngredientiContenutiPietanze_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.Mense;

public class IngredientiContenutiPietanze_Repository extends IngredientiContenutiPietanze_ThreadAggiornamento {
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA MENSE (e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public IngredientiContenutiPietanze_Repository(Application application) {
        super(application);
    }
/*================================================================================================*/
    //Ingredienti_Dao
/*################################################################################################*/
}
