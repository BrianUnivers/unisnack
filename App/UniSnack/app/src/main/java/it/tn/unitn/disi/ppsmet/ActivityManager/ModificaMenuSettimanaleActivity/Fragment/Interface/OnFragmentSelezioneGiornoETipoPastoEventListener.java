package it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface;

import java.util.Date;

import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;

public interface OnFragmentSelezioneGiornoETipoPastoEventListener {
/*================================================================================================*/
/*                                  GESTIONE DELLE MODIFICHE                                      */
/*================================================================================================*/
    /**
     * Questo metodo permete al fragmet contenete la lista di giorni e dei tipi di pasto di aprire
     * il frammeto contenete i dati da modificare richiesti.
     * @param data
     * @param tipoPasto
     */
    public void apriModificaManuDelGiornoTipoPasto(Date data, TipoPasto tipoPasto);
/*================================================================================================*/
/*================================================================================================*/
/*                                  SALVA LE MODIFICHE DELLE ORDONAZIONI                          */
/*================================================================================================*/
    /**
     * Questo metodo permette di salvare le modifiche applicate al menu settimanale che si è appena
     * personalizzato.
     */
    public void salvaModificheMenuSettimanale();
/*================================================================================================*/
/*================================================================================================*/
/*                                   ANNULLA MODIFICHE                                            */
/*================================================================================================*/
    /**
     * Questo metodo permette di annulare le modifice appotate al menu settimanale che si è appanna
     * modificato.
     */
    public void annullaModificheMenuSettimanale();
/*================================================================================================*/
}
