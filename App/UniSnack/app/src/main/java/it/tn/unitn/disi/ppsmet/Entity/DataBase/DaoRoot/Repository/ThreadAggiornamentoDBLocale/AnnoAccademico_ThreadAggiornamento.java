package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.AnnoAccademico_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_AnnoAccademico_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.AnnoAccademico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;

public class AnnoAccademico_ThreadAggiornamento  extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA ANNO ACCADEMICO LOCALE           */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "AnnoAccademico" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_AnnoAccademico";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista dei AnnoAccademici del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private List<AnnoAccademico> listaAnnoAccademico;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * AnnoAccademico.
         * @param listaAnnoAccademico Questo parametro contiene la lista di tutti gli elementi
         *                        tra qui quelli nuovi e quelli già presenti ma da modificare.
         */
        public ThreadAggiornamento(List<AnnoAccademico> listaAnnoAccademico) {
            super();
            this.listaAnnoAccademico =listaAnnoAccademico;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                                  */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella AnnoAccademico.");
            aggionaTabellaAnnoAccademico(listaAnnoAccademico);
            Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella AnnoAccademico.");
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param listaAA
         */
        private synchronized void aggionaTabellaAnnoAccademico(List<AnnoAccademico> listaAA){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella AnnoAccademico.");
            //Dati presenti nel database locale.
            List<AnnoAccademico> listaAALocale = annoAccademicoDao.getAll();
            if((listaAALocale==null)||(listaAALocale.isEmpty())){
                inserisciListaElementi(listaAA);
            }else if((listaAA==null)||(listaAA.isEmpty())) {
                eliminaListaElementi(listaAALocale);
            }else{
                HashMap<Long,AnnoAccademico> hashMapAALocali = convertiListaInHashMap(listaAALocale);

                List<AnnoAccademico> listaAANuove = new LinkedList<>();
                List<AnnoAccademico> listaAAAggiornate = new LinkedList<>();
                //Serve solo per poter conoscere quali elementi caccelare tra quelli già presenti localmente.
                List<AnnoAccademico> listaVechieAADaMantenere = new LinkedList<>();

                Iterator<AnnoAccademico> iterator = listaAA.iterator();
                while (iterator.hasNext()){
                    AnnoAccademico annoAccademico =iterator.next();
                    if(annoAccademico!=null){
                        AnnoAccademico annoAccademicoVecchia = hashMapAALocali.get(annoAccademico.getId());
                        if(annoAccademicoVecchia==null){
                            listaAANuove.add(annoAccademico);
                        }else if(!annoAccademicoVecchia.equals(annoAccademico)){
                            listaAAAggiornate.add(annoAccademico);
                            listaVechieAADaMantenere.add(annoAccademicoVecchia);
                        }else{
                            listaVechieAADaMantenere.add(annoAccademicoVecchia);
                        }
                    }
                }
                //MANTENGO SOLO GLI ELEMNETI CHE DEVO CANCELLARE
                listaAALocale.removeAll(listaVechieAADaMantenere);
                inserisciListaElementi(listaAANuove);
                aggiornaListaElementi(listaAAAggiornate);
                eliminaListaElementi(listaAALocale);
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         GENERAZIONE HASH MAP FROM LIST                                         */
/*================================================================================================*/
        private synchronized HashMap<Long,AnnoAccademico> convertiListaInHashMap(List<AnnoAccademico> lista){
            HashMap<Long,AnnoAccademico> ris=new HashMap<>();
            if(lista!=null){
                Iterator<AnnoAccademico> iterator = lista.iterator();
                while (iterator.hasNext()){
                    AnnoAccademico elemento = iterator.next();
                    if((elemento!=null) && (elemento.getId()!=null)){
                        ris.put(elemento.getId(),elemento);
                    }
                }
            }
            return ris;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO INSERIMENTO NUOVI ELEMENTI                                      */
/*================================================================================================*/
        private synchronized void inserisciListaElementi(List<AnnoAccademico> lista){
            if(lista!=null){
                Iterator<AnnoAccademico> iterator = lista.iterator();
                while (iterator.hasNext()){
                    AnnoAccademico elemento=iterator.next();
                    if(elemento!=null){
                        annoAccademicoDao.insert(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO AGGIORNAMENTO ELEMENTI PRESENTI                                 */
/*================================================================================================*/
        private synchronized void aggiornaListaElementi(List<AnnoAccademico> lista){
            if(lista!=null){
                Iterator<AnnoAccademico> iterator = lista.iterator();
                while (iterator.hasNext()){
                    AnnoAccademico elemento=iterator.next();
                    if(elemento!=null){
                        annoAccademicoDao.update(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaListaElementi(List<AnnoAccademico> lista){
            if(lista!=null){
                Iterator<AnnoAccademico> iterator = lista.iterator();
                while (iterator.hasNext()){
                    AnnoAccademico elemento =iterator.next();
                    if(elemento!=null){
                        annoAccademicoDao.delete(elemento.getId());
                    }
                }
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_AnnoAccademico_Dao fbAnnoAccademicoDao = new FB_AnnoAccademico_Dao();
    //QUERY FIREBASE
    private static LiveData<List<AnnoAccademico>> fbAllAnnoAccademico=null;
    //ROOM DATABASE
    protected static AnnoAccademico_Dao annoAccademicoDao=null;
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaAnnoAccademico(){
        if(fbAllAnnoAccademico==null){
            fbAllAnnoAccademico= fbAnnoAccademicoDao.getAll_Synchronized();
        }
        if(annoAccademicoDao==null){
            annoAccademicoDao = db.AnnoAccademicoDao();
        }
        if(!fbAllAnnoAccademico.hasObservers()){//SE NON HA già osservatori per rilevare aggiornamenti
            fbAllAnnoAccademico.observeForever(new Observer<List<AnnoAccademico>>() {
                @Override
                public void onChanged(List<AnnoAccademico> lista) {
                    if((lista!=null)){
                        executor.execute(new ThreadAggiornamento(lista));
                    }
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public AnnoAccademico_ThreadAggiornamento(Application application) {
        super(application);
        gestioneAggiornametoTabellaAnnoAccademico();
    }
/*================================================================================================*/
/*################################################################################################*/
}
