package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Corsi_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Corsi_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Universita_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.Uni_ha_C;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

public class CorsiDelUniversita_ThreadAggiornamento extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA ANNO ACCADEMICO LOCALE           */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "CorsoDelUniversita" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_CorsoDelUniversita";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista dei CorsoDelUniversita del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private List<Corsi> listaCorsi;
        private Long        idUniversita;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * Corsi.
         * @param listaCorsi Questo parametro contiene la lista di tutti gli elementi
         *                        tra qui quelli nuovi e quelli già presenti ma da modificare.
         */
        public ThreadAggiornamento(List<Corsi> listaCorsi,Long idUniversita) {
            super();
            this.listaCorsi =listaCorsi;
            this.idUniversita=idUniversita;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                                  */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella Corsi.");
            if(idUniversita!=null){
                aggionaTabellaCorsi(listaCorsi,idUniversita);
                Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella Corsi.");
            }else{
                Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella Corsi. ERRORE UNIVERSITA");
            }
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param listaCorsi
         * @param idUniversita
         */
        private synchronized void aggionaTabellaCorsi(List<Corsi> listaCorsi,Long idUniversita){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella Corsi.");
            //Controllo se posso già inserire i dati
            Universita universita = universitaDao.getByPrimaryKey(idUniversita);
            if(universita!=null){
                //Dati presenti nel database locale.
                List<Corsi> listaCorsiLocale = corsiDao.getByIdOfUniverita(idUniversita);
                if((listaCorsiLocale==null)||(listaCorsiLocale.isEmpty())){
                    inserisciListaElementi(listaCorsi, universita);
                }else if((listaCorsi==null)||(listaCorsi.isEmpty())) {
                    eliminaListaElementi(listaCorsiLocale, universita);
                }else{
                    HashMap<Long,Corsi> hashMapCorsiLocali = convertiListaInHashMap(listaCorsiLocale);

                    List<Corsi> listaCorsiNuove = new LinkedList<>();
                    //Serve solo per poter conoscere quali elementi caccelare tra quelli già presenti localmente.
                    List<Corsi> listaVechieCorsiDaMantenere = new LinkedList<>();

                    Iterator<Corsi> iterator = listaCorsi.iterator();
                    while (iterator.hasNext()){
                        Corsi corsi =iterator.next();
                        if(corsi!=null){
                            Corsi corsiVecchia = hashMapCorsiLocali.get(corsi.getId());
                            if(corsiVecchia==null){
                                listaCorsiNuove.add(corsi);
                            }else if(corsiVecchia.getId()==corsi.getId()){
                                listaVechieCorsiDaMantenere.add(corsiVecchia);
                            }
                        }
                    }
                    //MANTENGO SOLO GLI ELEMNETI CHE DEVO CANCELLARE
                    listaCorsiLocale.removeAll(listaVechieCorsiDaMantenere);
                    inserisciListaElementi(listaCorsiNuove, universita);
                    eliminaListaElementi(listaCorsiLocale, universita);
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         GENERAZIONE HASH MAP FROM LIST                                         */
/*================================================================================================*/
        private synchronized HashMap<Long,Corsi> convertiListaInHashMap(List<Corsi> lista){
            HashMap<Long,Corsi> ris=new HashMap<>();
            if(lista!=null){
                Iterator<Corsi> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Corsi elemento = iterator.next();
                    if((elemento!=null) && (elemento.getId()!=null)){
                        ris.put(elemento.getId(),elemento);
                    }
                }
            }
            return ris;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO INSERIMENTO NUOVI ELEMENTI                                      */
/*================================================================================================*/
        private synchronized void inserisciListaElementi(List<Corsi> lista,Universita universita){
            if(lista!=null){
                Iterator<Corsi> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Corsi elemento=iterator.next();
                    if(elemento!=null){
                        corsiDao.insert(new Uni_ha_C(universita.getId(),elemento.getId()));
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaListaElementi(List<Corsi> lista,Universita universita){
            if(lista!=null){
                Iterator<Corsi> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Corsi elemento=iterator.next();
                    if(elemento!=null){
                        corsiDao.delete(universita.getId(),elemento.getId());
                    }
                }
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_Corsi_Dao fbCorsiDao = new FB_Corsi_Dao();
    //ROOM DATABASE
    public static Corsi_Dao corsiDao = null;
/*------------------------------------------------------------------------------------------------*/
    //DIPENDENZE
    protected static Universita_ThreadAggiornamento dipendezaUniversita = null;
    protected static Corsi_ThreadAggiornamento      dipendezaCorso      = null;
    //ROOM DATABASE
    protected static Universita_Dao universitaDao = null;
    //QUERY ROOM DATABASE
    protected static  LiveData<List<Universita>> allUniversita = new MediatorLiveData<>();
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaCorsi(){
        if(universitaDao==null){
            //universitaDao è un attributo statico anche se è presente solo se è stoto creato un oggetto
            universitaDao= dipendezaUniversita.universitaDao;
            allUniversita = universitaDao.getAll_Synchronized();
        }
        if(corsiDao==null){
            corsiDao = db.CorsiDao();
        }
        if((!allUniversita.hasObservers())){//SE NON HA già osservatori per rilevare aggiornamenti
            allUniversita.observeForever(new Observer<List<Universita>>() {
                private  List<Universita>  lastListUniversita = new LinkedList<>();
                private  List<Universita>  temp = new LinkedList<>();
                @Override
                public void onChanged(List<Universita> listaUniversita) {
                    temp.clear();
                    temp.addAll(listaUniversita);
                    if(!lastListUniversita.isEmpty()){
                        listaUniversita.removeAll(lastListUniversita);
                    }
                    //Solo quelli nuovi
                    Iterator<Universita> iterator = listaUniversita.iterator();
                    while (iterator.hasNext()){
                        Universita universita =iterator.next();
                        if((universita!=null) && (universita.getId()!=null)){
                            //Aggiungi un altro ossservatore
                            fbCorsiDao.getByIdOfUniverita_Synchronized(universita.getId()).observeForever(new MyCorsiObserver(universita.getId()));
                        }
                    }
                    lastListUniversita.clear();
                    lastListUniversita.addAll(temp);
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
    private static class MyCorsiObserver implements Observer<List<Corsi>>{
        private Long idUniversita;
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        public MyCorsiObserver(Long idUniversita) {
            this.idUniversita =idUniversita;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO CHIAMATO NEL MOMENTO DEL CAMBIAMNETO                            */
/*================================================================================================*/
        @Override
        public void onChanged(List<Corsi> lista) {
            if((lista!=null)){
                executor.execute(new ThreadAggiornamento(lista,this.idUniversita));
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public CorsiDelUniversita_ThreadAggiornamento(Application application) {
        super(application);
        if(dipendezaUniversita==null){
            dipendezaUniversita = new Universita_ThreadAggiornamento(application);
        }
        if(dipendezaCorso==null){
            dipendezaCorso = new Corsi_ThreadAggiornamento(application);
        }
        gestioneAggiornametoTabellaCorsi();
    }
/*================================================================================================*/
/*################################################################################################*/
}
