package it.tn.unitn.disi.ppsmet.ActivityManager;

import android.app.Application;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import it.tn.unitn.disi.ppsmet.GestioneMyIntent.MyIntent;

public class CommonViewModelUserLogged extends AndroidViewModel {
/*################################################################################################*/
/*================================================================================================*/
/*                                  COSTRUTTORE VIEW MODEL                                        */
/*================================================================================================*/
    /**
     * Questo metodo costruttore del viewmode inizializza le repository che si interfacciano con entrambi
     * i database sia interno che estreno a seconda delle operazioni che vengono richeste. Che verranno
     * utilizate dall'activity e i suoi fragment.
     * @param application
     */
    public CommonViewModelUserLogged(@NonNull Application application) {
        super(application);
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  GESIONE DEI INTENT CHIAMANTI L'ACTIVITY GESTITA               */
/*################################################################################################*/
    private Long idUtenteAttivo = null;
/*================================================================================================*/
/*                                  GESIONE DEI INTENT CHIAMANTI L'ACTIVITY GESTITA               */
/*================================================================================================*/
    /**
     * Questo metodo permette al viewModel Di ricevere un intent ed se contiene il parametro contenente
     * l'id di un utente specificato come quello attivo permette di estrarlo e salvarlo nel ViewModel.
     * Se succesivamente viene  ricaricata la pagina dopo una rotazione dello schermo
     * e quindi il nuovo intent non avrebbe tale paramento, il suo precedente valore è comunque persente.
     * ATTEZIONE Se però non viene mai inizalizzato tale parametoro risulterà null.
     * @param intent
     */
    public void setParameterFromIntet(Intent intent){
        Long nuovoId = MyIntent.getIdUtenteExtra(intent,MyIntent.I_UTENTE_ATTIVO);
        if(nuovoId!=null){
            this.idUtenteAttivo=nuovoId;
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  ESTRAZIONE DATI DA INTENT                                     */
/*################################################################################################*/
/*================================================================================================*/
/*                                  GET ID UTENTE ATTIVO                                          */
/*================================================================================================*/
    /**
     * Questo motodo permette all'Activity che lo richedere di ricevere id dell'ultimo utente attivo,
     * che è stato passato da un intent in precedena.
     * ATTENZIONE Se però in nessun intent precedente non era contenuto nessun id, il risultato di
     * talte metodo sarà null;
     */
    public Long getIdUtenteAttivo(){
        return idUtenteAttivo;
    }
/*================================================================================================*/
/*################################################################################################*/
}
