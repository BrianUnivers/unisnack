package it.tn.unitn.disi.ppsmet.Entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@Entity(tableName = "tableAnnoAccademico")
public class AnnoAccademico {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @PrimaryKey
    private Long id;
    @NonNull
    @ColumnInfo(name = "dataInizio")
    private Date dataInizio;
    @NonNull
    @ColumnInfo(name = "dataFine")
    private Date dataFine;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    @Ignore
    public AnnoAccademico() {}
//--------------------------------------------------------------------------------------------------
    public AnnoAccademico(Long id,
                          @NonNull Date dataInizio,
                          @NonNull Date dataFine) {
        this.id = id;
        this.dataInizio = dataInizio;
        this.dataFine = dataFine;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    public Long getId() {
        return id;
    }
//--------------------------------------
    public void setId(Long id) {
        this.id = id;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Date getDataInizio() {
        return dataInizio;
    }
//--------------------------------------
    public void setDataInizio(@NonNull Date dataInizio) {
        this.dataInizio = dataInizio;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Date getDataFine() {
        return dataFine;
    }
//--------------------------------------
    public void setDataFine(@NonNull Date dataFine) {
        this.dataFine = dataFine;
    }
//==================================================================================================
//==================================================================================================
//                                      EQUALS E HASH_CODE
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AnnoAccademico)) return false;
        AnnoAccademico that = (AnnoAccademico) o;
        return Objects.equals(getId(), that.getId()) &&
                getDataInizio().equals(that.getDataInizio()) &&
                getDataFine().equals(that.getDataFine());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDataInizio(), getDataFine());
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                                      METODI AGGIUNTIVI
//##################################################################################################
//==================================================================================================
//                                      DATA INTERNO AL ANNO ACCADEMICO
//==================================================================================================
    public boolean dataNelAnnoAccademico(Date data){
        return ((data!=null)  &&
                ((data.after(this.getDataInizio()))&&(data.before(this.getDataFine()))));
    }
//==================================================================================================
//##################################################################################################
//##################################################################################################
//                                      REPPRESENTAZIONI
//##################################################################################################
//==================================================================================================
//                                      GET PERIODD
//==================================================================================================
    public String getStringPeriodo(){
        String ris="Non definibile";
        if((this.getDataFine()!=null) && (this.getDataInizio()!=null)){
            Calendar dataInizio = Calendar.getInstance();
            Calendar dataFine = Calendar.getInstance();
            dataInizio.setTime(this.getDataInizio());
            dataFine.setTime(this.getDataFine());

            ris= new StringBuffer()
                            .append(dataInizio.get(Calendar.YEAR))
                            .append(" - ")
                            .append(dataFine.get(Calendar.YEAR)).toString();
        }
        return ris;
    }
//==================================================================================================
//##################################################################################################
}
