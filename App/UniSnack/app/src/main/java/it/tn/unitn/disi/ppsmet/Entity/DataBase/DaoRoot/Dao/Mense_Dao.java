package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.Date;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

@Dao
public interface Mense_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long insert(Mense mense);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Query("DELETE FROM tableMense")
    public void deleteAll();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE By ID                                                    */
/*================================================================================================*/
    @Query("DELETE FROM tableMense WHERE id == :id")
    public abstract void delete(Long id);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = Mense.class,
            onConflict = OnConflictStrategy.ABORT)
    public void update(Mense mense);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    @Query("SELECT * FROM tableMense")
    public List<Mense> getAll();
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableMense")
    public LiveData<List<Mense>> getAll_Synchronized();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    @Query("SELECT * FROM tableMense WHERE id == :id")
    public Mense getByPrimaryKey(Long id);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableMense WHERE id == :id")
    public LiveData<Mense> getByPrimaryKey_Synchronized(Long id);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID OF UNIVERSITA'                                        */
/*================================================================================================*/
    @Query("SELECT * FROM tableMense WHERE id_Universita==:idUniversita")
    public List<Mense> getByIdOfUniverita(Long idUniversita);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableMense WHERE id_Universita==:idUniversita")
    public LiveData<List<Mense>> getByIdOfUniverita_Synchronized(Long idUniversita);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID OF UENTE                                              */
/*================================================================================================*/
    @Query(" SELECT M.* " +
            "FROM tableMense AS M ,tableUniversita AS Uni, tableUtenti AS U, tableAnnoAccademico AS AA " +
            "WHERE   M.id_universita == Uni.id AND Uni.id == U.id_universita AND " +
                    "U.statoTessera == 1 AND U.id == :idUtente AND U.id_AA == AA.id AND" +
                    " :date BETWEEN AA.dataInizio AND AA.dataFine " +
            "UNION " +
            "SELECT M.* " +
            "FROM tableMense AS M, tableUniversita AS Uni, tableU_partecipa_Uni AS U, tableAnnoAccademico AS AA " +
            "WHERE   M.id_universita == Uni.id AND Uni.id == U.id_universita AND  " +
                    "U.statoTessera == 1 AND U.id_utente == :idUtente AND U.id_AA == AA.id AND " +
                    " :date BETWEEN AA.dataInizio AND AA.dataFine ")
    public List<Mense> getAccessibleByIdOfUtenteAtSpecificDate(Long idUtente, Date date);
/*------------------------------------------------------------------------*/
    @Query(" SELECT M.* " +
            "FROM tableMense AS M ,tableUniversita AS Uni, tableUtenti AS U, tableAnnoAccademico AS AA " +
            "WHERE   M.id_universita == Uni.id AND Uni.id == U.id_universita AND " +
                    "U.statoTessera == 1 AND U.id == :idUtente AND U.id_AA == AA.id AND" +
                    " :date BETWEEN AA.dataInizio AND AA.dataFine " +
            "UNION " +
            "SELECT M.* " +
            "FROM tableMense AS M, tableUniversita AS Uni, tableU_partecipa_Uni AS U, tableAnnoAccademico AS AA " +
            "WHERE   M.id_universita == Uni.id AND Uni.id == U.id_universita AND  " +
                    "U.statoTessera == 1 AND U.id_utente == :idUtente AND U.id_AA == AA.id AND " +
                    " :date BETWEEN AA.dataInizio AND AA.dataFine ")
    public LiveData<List<Mense>> getAccessibleByIdOfUtenteAtSpecificDate_Synchronized(Long idUtente,Date date);
/*================================================================================================*/
}
