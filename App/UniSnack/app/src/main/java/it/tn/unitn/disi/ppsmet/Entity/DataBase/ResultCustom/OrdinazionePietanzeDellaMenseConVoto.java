package it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Ignore;

import com.google.firebase.database.annotations.NotNull;

import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;

public class OrdinazionePietanzeDellaMenseConVoto extends PietanzeDellaMense {

//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @ColumnInfo(name = "id_ordinazione")
    public Long idOrdinazione;
    @ColumnInfo(name = "votoPietanza")
    public Short votoPietanza;
//##################################################################################################
//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    public OrdinazionePietanzeDellaMenseConVoto(   @NonNull Long idOrdinazione,
                                                   @NonNull Integer mese,
                                                   @NonNull Integer anno,
                                                   @NonNull Integer giornoSettimana,
                                                   @NonNull TipoMenu tipoMenu,
                                                   @NonNull Mense mense,
                                                   @NonNull Pietanze pietanze,
                                                   Short votoPietanza) {
        super(mese, anno, giornoSettimana ,tipoMenu, mense, pietanze);
        this.idOrdinazione =idOrdinazione;
        this.votoPietanza = votoPietanza;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public OrdinazionePietanzeDellaMenseConVoto(@NonNull Long idOrdinazione,
                                                @NonNull Integer mese,
                                                @NonNull Integer anno,
                                                @NonNull Integer giornoSettimana,
                                                @NonNull TipoMenu tipoMenu,
                                                @NonNull Mense mense,
                                                @NonNull Pietanze pietanze) {
        super(mese, anno, giornoSettimana ,tipoMenu, mense, pietanze);
        this.idOrdinazione =idOrdinazione;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public OrdinazionePietanzeDellaMenseConVoto(@NonNull Long idOrdinazione,
                                                @NotNull PietanzeDellaMense pietanzeDellaMense,
                                                Short votoPietanza) {
        super(  pietanzeDellaMense.getMese(),
                pietanzeDellaMense.getAnno(),
                pietanzeDellaMense.getGiornoSettimana(),
                pietanzeDellaMense.getTipoMenu(),
                pietanzeDellaMense.getMense(),
                pietanzeDellaMense.getPietanze());
        this.votoPietanza = votoPietanza;
        this.idOrdinazione =idOrdinazione;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public OrdinazionePietanzeDellaMenseConVoto(@NonNull Long idOrdinazione,
                                                @NotNull PietanzeDellaMense pietanzeDellaMense) {
        super(  pietanzeDellaMense.getMese(),
                pietanzeDellaMense.getAnno(),
                pietanzeDellaMense.getGiornoSettimana(),
                pietanzeDellaMense.getTipoMenu(),
                pietanzeDellaMense.getMense(),
                pietanzeDellaMense.getPietanze());
        this.idOrdinazione =idOrdinazione;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public OrdinazionePietanzeDellaMenseConVoto(@NotNull PietanzeDellaMense pietanzeDellaMense,
                                                Short votoPietanza) {
        super(  pietanzeDellaMense.getMese(),
                pietanzeDellaMense.getAnno(),
                pietanzeDellaMense.getGiornoSettimana(),
                pietanzeDellaMense.getTipoMenu(),
                pietanzeDellaMense.getMense(),
                pietanzeDellaMense.getPietanze());
        this.votoPietanza = votoPietanza;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public OrdinazionePietanzeDellaMenseConVoto(@NotNull  PietanzeDellaMense pietanzeDellaMense) {
        super(  pietanzeDellaMense.getMese(),
                pietanzeDellaMense.getAnno(),
                pietanzeDellaMense.getGiornoSettimana(),
                pietanzeDellaMense.getTipoMenu(),
                pietanzeDellaMense.getMense(),
                pietanzeDellaMense.getPietanze());
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    public Short getVotoPietanza() {
        return votoPietanza;
    }
//-------------------------------------
    public void setVotoPietanza(Short votoPietanza) {
        this.votoPietanza = votoPietanza;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getIdOrdinazione() {
        return idOrdinazione;
    }
//-------------------------------------
    public void setIdOrdinazione(@NonNull Long idOrdinazione) {
        this.idOrdinazione = idOrdinazione;
    }
//==================================================================================================
//##################################################################################################
}
