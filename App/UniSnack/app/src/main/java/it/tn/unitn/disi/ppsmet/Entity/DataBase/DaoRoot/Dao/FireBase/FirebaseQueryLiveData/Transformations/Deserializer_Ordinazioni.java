package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.util.Iterator;

import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;

public class Deserializer_Ordinazioni implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto Ordinazioni.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String LOG_TAG = "Ordinazioni_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "Ordinazioni" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "Ordinazioni".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public static Ordinazioni estraiDaDataSnapshot(DataSnapshot dataSnapshot){
        Ordinazioni Ordinazioni = null;
        if(dataSnapshot.exists()){
            try{
                Ordinazioni = new Ordinazioni();
                Ordinazioni.setId(Long.parseLong( dataSnapshot.getKey()));
              //  Ordinazioni.setNome(              dataSnapshot.child(P_NOME).getValue(String.class));
              //  Ordinazioni.setSigla(             dataSnapshot.child(P_SIGLA).getValue(String.class));

/*todo
                * this.id = id;
        this.giorno = giorno;
        this.id_utente = id_utente;
        this.id_tipoPasto = id_tipoPasto;
        this.dataValutazione = dataValutazione;
        this.votoCottura = votoCottura;
        this.votoTemperatura = votoTemperatura;
        this.votoSapore = votoSapore;
                * */
            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
                Ordinazioni = null;
            }
        }
        return Ordinazioni;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         GET AN ACTRIBUTE OBJECT FOR AN OTHER                                   */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET AN ACTRIBUTE                                                       */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce, se esiste, un singolo ogetto "Ordinazioni" da un attributo di un altro
 * oggetto attraverso una sottoparte dell'oggetto "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "Ordinazioni".
 * @return  Il riutato è il primo oggetto, valido se ce ne fossero più d'uno ritorna il primo trovato
 *          mentre se non ce ne sono ritorna null.
 */
    public static Ordinazioni getAttributeOf(DataSnapshot dataSnapshot,String nomeAttributo){
        Ordinazioni Ordinazioni = null;
        DataSnapshot OrdinazioniDataSnapshot = dataSnapshot.child(nomeAttributo);
        if(OrdinazioniDataSnapshot.exists()){
            Iterator<DataSnapshot> iterator =OrdinazioniDataSnapshot.getChildren().iterator();
            while ( (iterator.hasNext()) && (Ordinazioni==null) ){//Restituisce il primo oggetto valido
                Ordinazioni = estraiDaDataSnapshot(iterator.next());
            }
        }
        return Ordinazioni;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, Ordinazioni>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Object extends GenericDeserializer_Object<Ordinazioni> {
        @Override
        public Ordinazioni estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Ordinazioni.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<Ordinazioni>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List<Ordinazioni> {
        @Override
        public Ordinazioni estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Ordinazioni.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
