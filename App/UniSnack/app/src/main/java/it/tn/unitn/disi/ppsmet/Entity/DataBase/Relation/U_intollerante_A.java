package it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;


@Entity(tableName = "tableU_intollerante_A",
        primaryKeys = {"id_utente",
                       "id_allergeni"},
        foreignKeys = {
                @ForeignKey(entity = Utenti.class,
                        parentColumns = "id",       //Colanna della classe a qui si rifierisce
                        childColumns  = "id_utente",//ForeignKey
                        onUpdate = ForeignKey.CASCADE,
                        onDelete = ForeignKey.CASCADE),
                @ForeignKey(entity = Allergeni.class,
                        parentColumns = "id",          //Colanna della classe a qui si rifierisce
                        childColumns  = "id_allergeni",//ForeignKey
                        onUpdate = ForeignKey.CASCADE,
                        onDelete = ForeignKey.CASCADE)},
        indices = {
                @Index(value = {"id_utente"}),
                @Index(value = {"id_allergeni"})})
public class U_intollerante_A {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @ColumnInfo(name = "id_utente")
    private Long id_utente;
    @NonNull
    @ColumnInfo(name = "id_allergeni")
    private Long id_allergeni;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    public U_intollerante_A(@NonNull Long id_utente,
                            @NonNull Long id_allergeni) {
        this.id_utente = id_utente;
        this.id_allergeni = id_allergeni;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    @NonNull
    public Long getId_utente() {
        return id_utente;
    }
//--------------------------------------
    public void setId_utente(@NonNull Long id_utente) {
        this.id_utente = id_utente;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_allergeni() {
        return id_allergeni;
    }
//--------------------------------------
    public void setId_allergeni(@NonNull Long id_allergeni) {
        this.id_allergeni = id_allergeni;
    }
//==================================================================================================
//##################################################################################################
}
