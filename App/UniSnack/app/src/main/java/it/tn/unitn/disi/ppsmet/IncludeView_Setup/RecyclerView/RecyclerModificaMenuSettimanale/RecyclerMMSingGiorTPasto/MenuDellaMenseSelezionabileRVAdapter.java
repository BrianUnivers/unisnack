package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerModificaMenuSettimanale.RecyclerMMSingGiorTPasto;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.TitoloSopraContenutoView;
import it.tn.unitn.disi.ppsmet.R;

public class MenuDellaMenseSelezionabileRVAdapter extends RecyclerView.Adapter<MenuDellaMenseSelezionabileRVAdapter.MyViewHoder> {
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE INTERNA PADRE                                              */
/*################################################################################################*/
    /**
     * Questa classe è quella quella che viene usata per permettere alla RecyclingView Adapter di definire
     * il layout singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public ViewGroup                v_mainLayout;
        public TitoloSopraContenutoView v_TitoloTipoPietanza;
        public RecyclerView             v_ListaPietanze;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout         = itemView.findViewById(R.id.cly_RowElemento);
            v_TitoloTipoPietanza = TitoloSopraContenutoView.newInstance((ViewGroup) itemView,R.id.include_TitoloTipoPietanze);
            v_ListaPietanze      = itemView.findViewById(R.id.rcl_ListaPietanze);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(comtext);
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            v_ListaPietanze.setLayoutManager(linearLayoutManager);
        }
/*================================================================================================*/
    }
/*################################################################################################*/

/*################################################################################################*/
/*                              CLASSE PRINCIPALE ModificaMenuSettimanaleTipoPastoRVAdapter       */
/*################################################################################################*/
    private HashMap<Long,PietanzaDellaMenseSelezionabileRVAdapter>      hmAdapter;
    private HashMap<Long,TipoPietanza> hmTipoPietanza = null;
    /**
     * Questo parametro contiene l'associazione tra id del tipo pietanze
     */
    private HashMap<Long,List<PietanzeDellaMense>> hmPietanzeSeparatePerTipo = new HashMap<>();
/*------------------------------------------------------------------------------------------------*/
    private Context comtext                = null;
    private List<Long> idListaTipoPietanza = null;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public MenuDellaMenseSelezionabileRVAdapter(Context comtext,
                                                List<PietanzeDellaMense> listaPietanze,
                                                List<TipoPietanza> listaTipoPietanza) {
        this.comtext = comtext;
        this.hmTipoPietanza = generaHashMapTipoPietanze(listaTipoPietanza);
        this.hmPietanzeSeparatePerTipo = generaListePietanzeSeparatePerTipo(listaPietanze);
        //Genera HashMap Adapter
        this.hmAdapter           = generaHashMapAdapter(this.hmPietanzeSeparatePerTipo);
        this.idListaTipoPietanza = new LinkedList<>();
        idListaTipoPietanza.addAll(hmPietanzeSeparatePerTipo.keySet());
    }
/*================================================================================================*/


/*================================================================================================*/
/*                              CERAZIONE DEL SINGOLO ELEMETOTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(comtext);
        View bloccoElemeto = inflater.inflate(R.layout.row_seleziona_da_lista_pietanza, parent,false);
        MyViewHoder myViewHoder = new MyViewHoder(bloccoElemeto);
        return myViewHoder;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              INSERIRE L'ELEMETO NEL LAYOUT                                     */
/*================================================================================================*/
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore ModificaMenuSettimanaleTipoPastoRVAdapter.
     */
    @Override
    public void onBindViewHolder(@NonNull MyViewHoder holder, int position) {
        final Long               idTipoPietanza = idListaTipoPietanza.get(position);
        TipoPietanza             tipoPietanza   = hmTipoPietanza.get(idTipoPietanza);
        List<PietanzeDellaMense> lista          = hmPietanzeSeparatePerTipo.get(idTipoPietanza);
        PietanzaDellaMenseSelezionabileRVAdapter adapter = hmAdapter.get(idTipoPietanza);
        inserisciPietanzeLayout(holder, tipoPietanza,lista,adapter);
    }
/*================================================================================================*/
/*                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE                  */
/*================================================================================================*/
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return idListaTipoPietanza.size();
    }
/*================================================================================================*/


/*================================================================================================*/
/*                              INSERISCI I DATI NEL LAYOUT                                       */
/*================================================================================================*/
    /**
     * Questo metodo si occupa di inserire i dati della pietanza che è quello attuale  nel layout del
     * singolo elemeto.
     */
    private void inserisciPietanzeLayout(@NonNull MyViewHoder holder,
                                         TipoPietanza tipoPietanza,
                                         List<PietanzeDellaMense> listaPietanza,
                                         PietanzaDellaMenseSelezionabileRVAdapter adapter){
        if(tipoPietanza!=null){
            holder.v_TitoloTipoPietanza.setText(tipoPietanza.getNome());
        }else{
            holder.v_TitoloTipoPietanza.setText("");
        }
        holder.v_ListaPietanze.setAdapter(adapter);
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                              GET PIETANZE SELEZIONATE                                          */
/*================================================================================================*/
    /**
     * Questo metodo permette di restituire la lista di pietanze selezionate in questo momento.
     * @return
     */
    public List<PietanzeDellaMense> getListaPietanzeSelezionate(){
        List<PietanzeDellaMense> ris = new LinkedList<>();
        List<PietanzaDellaMenseSelezionabileRVAdapter> list = new LinkedList();
        list.addAll(hmAdapter.values());
        Iterator<PietanzaDellaMenseSelezionabileRVAdapter> iterator = list.iterator();
        while (iterator.hasNext()){
            PietanzaDellaMenseSelezionabileRVAdapter adapter = iterator.next();
            if(adapter!=null){
                ris.addAll(adapter.getListaPietanzeSelezionate());
            }
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                              GENERA DATI PER ADAPTER                                           */
/*================================================================================================*/
    /**
     * Separa la lista pietanze in liste associate all'id del tipo pietanze cosi da separarle, ed così
     * da sapere quali elementi tipo pietanza devono essere usati.
     * @param listaPietanze
     * @return
     */
    private HashMap<Long,List<PietanzeDellaMense>> generaListePietanzeSeparatePerTipo(List<PietanzeDellaMense> listaPietanze){
        HashMap<Long,List<PietanzeDellaMense>> ris = new HashMap<>();
        if(listaPietanze!=null){
            Iterator<PietanzeDellaMense> iterator = listaPietanze.iterator();
            while (iterator.hasNext()){
                PietanzeDellaMense pietanza = iterator.next();
                if((pietanza!=null) && (pietanza.getPietanze()!=null) && (pietanza.getPietanze().getId_tipoPietanza()!=null)){
                    Long key= pietanza.getPietanze().getId_tipoPietanza();
                    List<PietanzeDellaMense> list = new LinkedList<>();
                    List<PietanzeDellaMense> oldList = ris.get(key);
                    if(oldList!=null){
                        list.addAll(oldList);
                    }
                    list.add(pietanza);
                    ris.put(key,list);
                }
            }
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo permette di avere tutti i tipi di pietanze associate ia loro id così da facilitarne
     * l'individuazione.
     * @param listaTipoPietanza
     * @return
     */
    private HashMap<Long,TipoPietanza> generaHashMapTipoPietanze(List<TipoPietanza> listaTipoPietanza){
        HashMap<Long,TipoPietanza> ris = new HashMap<>();
        if(listaTipoPietanza!=null){
            Iterator<TipoPietanza> iterator = listaTipoPietanza.iterator();
            while (iterator.hasNext()){
                TipoPietanza tipoPietanza = iterator.next();
                if((tipoPietanza!=null) && (tipoPietanza.getId()!=null) ){
                    Long key= tipoPietanza.getId();
                    ris.put(key,tipoPietanza);
                }
            }
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo perepara tutti gli adfapter che verrano utilizzati nei vari elemeti di questo
     * adapter principale, poichè non dovranno essere creati dei nuovi ognivolta che viene ricreato
     * o rivisualizato lo stesso elemento. Questo perchè contengono i gli elementi selezionato che
     * devono essere raggiungibili in ogni momento e devono esserci tutti gli elementi selezionato.
     * @param hmPietanze
     * @return
     */
    private HashMap<Long,PietanzaDellaMenseSelezionabileRVAdapter> generaHashMapAdapter(HashMap<Long,List<PietanzeDellaMense>> hmPietanze){
        HashMap<Long,PietanzaDellaMenseSelezionabileRVAdapter> ris = new HashMap<>();
        Iterator<Long> iterator = hmPietanze.keySet().iterator();
        while (iterator.hasNext()){
            Long key = iterator.next();
            if(key!=null){
                ris.put(key,new PietanzaDellaMenseSelezionabileRVAdapter(  comtext,hmPietanze.get(key) ));
            }
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
}