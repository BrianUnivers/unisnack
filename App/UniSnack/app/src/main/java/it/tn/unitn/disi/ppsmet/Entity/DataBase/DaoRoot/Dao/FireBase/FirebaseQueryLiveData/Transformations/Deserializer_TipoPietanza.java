package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.util.Iterator;

import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;

public class Deserializer_TipoPietanza implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto TipoPietanza.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String LOG_TAG = "TipoPietanza_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "TipoPietanza" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "TipoPietanza".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public static TipoPietanza estraiDaDataSnapshot(DataSnapshot dataSnapshot){
        TipoPietanza tipoPietanza = null;
        if(dataSnapshot.exists()){
            try{
                //Campi not null
                Long id = null;
                if (dataSnapshot.child(P_KEY).exists()){
                    id = dataSnapshot.child(P_KEY).getValue(Long.class);
                }else{
                    id = Long.parseLong( dataSnapshot.getKey());
                }
                String nome = dataSnapshot.child(P_NOME_TIPO).getValue(String.class);
                //Verifica campi not null
                if((id!=null) && (nome!=null)){
                    tipoPietanza = new TipoPietanza(id,nome);
                }
            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
                tipoPietanza = null;
            }
        }
        return tipoPietanza;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         GET AN ACTRIBUTE OBJECT FOR AN OTHER                                   */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET AN ACTRIBUTE                                                       */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce, se esiste, un singolo ogetto "TipoPietanza" da un attributo di un altro
 * oggetto attraverso una sottoparte dell'oggetto "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "TipoPietanza".
 * @param nomeAttributo Questo parametro contine il nome del attributo in cui cercare un siglo oggetto.
 * @return  Il riutato è il primo oggetto, valido se ce ne fossero più d'uno ritorna il primo trovato
 *          mentre se non ce ne sono ritorna null.
 */
    public static TipoPietanza getAttributeOf(DataSnapshot dataSnapshot, String nomeAttributo){
        TipoPietanza tipoPietanza = null;
        if(dataSnapshot.child(nomeAttributo).exists()){
            DataSnapshot dataSnapshotTipoPietanza = dataSnapshot.child(nomeAttributo);
            tipoPietanza = estraiDaDataSnapshot(dataSnapshotTipoPietanza);
        }
        return tipoPietanza;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, TipoPietanza>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Object extends GenericDeserializer_Object<TipoPietanza> {
        @Override
        public TipoPietanza estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_TipoPietanza.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<TipoPietanza>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List<TipoPietanza> {
        @Override
        public TipoPietanza estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_TipoPietanza.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
