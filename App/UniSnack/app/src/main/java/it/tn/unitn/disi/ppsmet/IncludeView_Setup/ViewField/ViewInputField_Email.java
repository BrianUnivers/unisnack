package it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewField;

import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.tn.unitn.disi.ppsmet.R;

public class ViewInputField_Email extends ViewInputField_Text {
/*################################################################################################*/
    private int messaggioErrore = R.string.err_Email;
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP VIEW FIELD                                */
/*================================================================================================*/
    public ViewInputField_Email(ViewGroup rootLayout, int idLayoutIncluded, int idLable, int idTestValue) {
       super(rootLayout,idLayoutIncluded,idLable,idTestValue);
        impostaLayoutComune();
    }
/*------------------------------------------------------------------------*/
    public ViewInputField_Email(ViewGroup rootLayout, int idLayoutIncluded, int idLable, int idTestValue,boolean obbligatorio) {
        super(rootLayout,idLayoutIncluded,idLable,idTestValue,obbligatorio);
        impostaLayoutComune();
    }
/*------------------------------------------------------------------------------------------------*/
    private void impostaLayoutComune() {
        if(input!=null){
            input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  AZZIONI DURANTE L'INSERIMETO DEI DATI                         */
/*================================================================================================*/
    public String getEmail(){
        String ris = null;
        if(input!=null){
            ris =input.getText().toString();
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  AZZIONI DURANTE L'INSERIMETO DEI DATI                         */
/*================================================================================================*/
    @Override
    protected void azzioneDuranteInserimetoDati(){
        String email = input.getText().toString();
        if((email!=null) && (!email.equals("")) &&!verificaEmailInseritaValida()){
            impostaErrore(messaggioErrore);
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  VERIFICA VALIDITA DI BASE                                     */
/*================================================================================================*/
    private static final String regex = "^(.+)@(.+)\\.(.+)$";
    private boolean verificaEmailInseritaValida(){
        String email = input.getText().toString();
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return (email!=null) && (!email.equals("")) && (matcher.matches());
    }
/*================================================================================================*/
/*################################################################################################*/
}
