package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.room.Query;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Allergeni_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.IngredientiEAllergeni_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class Allergeni_Repository  extends IngredientiEAllergeni_ThreadAggiornamento {
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA INGREDEINTI E ALLERGENI (e le sue       */
/*                         derivazioni seguendo un ordine topologico per garantire gli            */
/*                         aggiornamenti) ATTRAVERO IL DATABASE LOCALE (room)                     */
/*################################################################################################*/
    private LiveData<List<Allergeni>> mAllAllergeni;
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Allergeni_Repository(Application application) {
        super(application);
        //AllAllergeni get any time the same result
        mAllAllergeni = allergeniDao.getAll_Synchronized();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERT                                                                 */
/*================================================================================================*/
    public Future<Long> insert(Allergeni allergeni){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                    @Override
                    public Long call() throws Exception {
                        return allergeniDao.insert(allergeni);
                    }
                });
    }
/*------------------------------------------------------------------------*/
    public Future<Long> insertAllergia(Utenti utenti, Allergeni allergeni ){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                @Override
                public Long call() throws Exception {
                    return allergeniDao.insertAllergia(utenti,allergeni);
                }
            });
    }
/*------------------------------------------------------------------------*/
    public Future<Long> insertAllergia(Utenti utenti,List<Allergeni> listaAllergeni){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                    @Override
                    public Long call() throws Exception {
                        return allergeniDao.insertAllergia(utenti,listaAllergeni);
                    }
                });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
 /*   public void deleteAll(){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            allergeniDao.deleteAll();
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE by ID UTENTE AND ALLERGENI                               */
/*================================================================================================*/
    public void deleteAllAllergeniByIdOfUtente(Utenti utenti){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            allergeniDao.deleteAllAllergeni(utenti.getId());
        });
    }
/*------------------------------------------------------------------------*/
    public void deleteSpecificAllergeniByIdOfUtente(Utenti utenti,Allergeni allergeni){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            allergeniDao.deleteSpecificAllergeniByIdOfUtente(utenti.getId(),allergeni.getId());
        });
    }
/*------------------------------------------------------------------------*/
    public void deleteListAllergeniByIdOfUtente(Utenti utenti,List<Allergeni> listaAllergeni){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            List<Long> listaIdAllergeni  = new LinkedList<>();
            Iterator<Allergeni> iterator = listaAllergeni.iterator();
            while(iterator.hasNext()){
                Allergeni allergeni=iterator.next();
                if((allergeni!=null)&&(allergeni.getId()!=null)){
                    listaIdAllergeni.add(allergeni.getId());
                }
            }
            //Elimina elementi
            if(!listaIdAllergeni.isEmpty()){
                allergeniDao.deleteListAllergeniByIdOfUtente(utenti.getId(),listaIdAllergeni);
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
  /*  public void update(Allergeni allergeni){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            allergeniDao.update(allergeni);
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<Allergeni>> getAll(){
        return mAllAllergeni;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
  /*  public LiveData<Allergeni> getByPrimaryKey(Long id){
        return allergeniDao.getByPrimaryKey_Synchronized(id);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by by ID UTENTE                                             */
/*================================================================================================*/
    public  LiveData<List<Allergeni>> getByIdOfUtente(Utenti utenti){
        return allergeniDao.getByIdOfUtente_Synchronized(utenti.getId());
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<Allergeni>> getByIdOfUtente(Long idUtenti){
        return allergeniDao.getByIdOfUtente_Synchronized(idUtenti);
    }
/*================================================================================================*/
/*################################################################################################*/
}