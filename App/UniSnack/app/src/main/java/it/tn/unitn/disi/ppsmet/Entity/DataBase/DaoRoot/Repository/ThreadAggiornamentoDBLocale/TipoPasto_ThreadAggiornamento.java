package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_TipoMenu_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_TipoPasto_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.TipoMenu_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.TipoPasto_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;

public class TipoPasto_ThreadAggiornamento extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA ANNO ACCADEMICO LOCALE           */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "TipoPasto" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_TipoPasto";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista dei TipoPasto del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private List<TipoPasto> listaTipoPasto;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * TipoPasto.
         * @param listaTipoPasto Questo parametro contiene la lista di tutti gli elementi
         *                        tra qui quelli nuovi e quelli già presenti ma da modificare.
         */
        public ThreadAggiornamento(List<TipoPasto> listaTipoPasto) {
            super();
            this.listaTipoPasto =listaTipoPasto;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                                  */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella TipoPasto.");
            aggionaTabellaTipoPasto(listaTipoPasto);
            Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella TipoPasto.");
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param listaTipoPasto
         */
        private synchronized void aggionaTabellaTipoPasto(List<TipoPasto> listaTipoPasto){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella TipoPasto.");
            //Dati presenti nel database locale.
            List<TipoPasto> listaTipoPastoLocale = tipoPastoDao.getAll();
            if((listaTipoPastoLocale==null)||(listaTipoPastoLocale.isEmpty())){
                inserisciListaElementi(listaTipoPasto);
            }else if((listaTipoPasto==null)||(listaTipoPasto.isEmpty())) {
                eliminaListaElementi(listaTipoPastoLocale);
            }else{
                HashMap<Long,TipoPasto> hashMapTipoPastoLocali = convertiListaInHashMap(listaTipoPastoLocale);

                List<TipoPasto> listaTipoPastoNuove = new LinkedList<>();
                List<TipoPasto> listaTipoPastoAggiornate = new LinkedList<>();
                //Serve solo per poter conoscere quali elementi caccelare tra quelli già presenti localmente.
                List<TipoPasto> listaVechieTipoPastoDaMantenere = new LinkedList<>();

                Iterator<TipoPasto> iterator = listaTipoPasto.iterator();
                while (iterator.hasNext()){
                    TipoPasto tipoPasto =iterator.next();
                    if(tipoPasto!=null){
                        TipoPasto tipoPastoVecchia = hashMapTipoPastoLocali.get(tipoPasto.getId());
                        if(tipoPastoVecchia==null){
                            listaTipoPastoNuove.add(tipoPasto);
                        }else if(!tipoPastoVecchia.equals(tipoPasto)){
                            listaTipoPastoAggiornate.add(tipoPasto);
                            listaVechieTipoPastoDaMantenere.add(tipoPastoVecchia);
                        }else{
                            listaVechieTipoPastoDaMantenere.add(tipoPastoVecchia);
                        }
                    }
                }
                //MANTENGO SOLO GLI ELEMNETI CHE DEVO CANCELLARE
                listaTipoPastoLocale.removeAll(listaVechieTipoPastoDaMantenere);
                inserisciListaElementi(listaTipoPastoNuove);
                aggiornaListaElementi(listaTipoPastoAggiornate);
                eliminaListaElementi(listaTipoPastoLocale);
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         GENERAZIONE HASH MAP FROM LIST                                         */
/*================================================================================================*/
        private synchronized HashMap<Long,TipoPasto> convertiListaInHashMap(List<TipoPasto> lista){
            HashMap<Long,TipoPasto> ris=new HashMap<>();
            if(lista!=null){
                Iterator<TipoPasto> iterator = lista.iterator();
                while (iterator.hasNext()){
                    TipoPasto elemento = iterator.next();
                    if((elemento!=null) && (elemento.getId()!=null)){
                        ris.put(elemento.getId(),elemento);
                    }
                }
            }
            return ris;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO INSERIMENTO NUOVI ELEMENTI                                      */
/*================================================================================================*/
        private synchronized void inserisciListaElementi(List<TipoPasto> lista){
            if(lista!=null){
                Iterator<TipoPasto> iterator = lista.iterator();
                while (iterator.hasNext()){
                    TipoPasto elemento=iterator.next();
                    if(elemento!=null){
                        tipoPastoDao.insert(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO AGGIORNAMENTO ELEMENTI PRESENTI                                 */
/*================================================================================================*/
        private synchronized void aggiornaListaElementi(List<TipoPasto> lista){
            if(lista!=null){
                Iterator<TipoPasto> iterator = lista.iterator();
                while (iterator.hasNext()){
                    TipoPasto elemento=iterator.next();
                    if(elemento!=null){
                        tipoPastoDao.update(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaListaElementi(List<TipoPasto> lista){
            if(lista!=null){
                Iterator<TipoPasto> iterator = lista.iterator();
                while (iterator.hasNext()){
                    TipoPasto elemento=iterator.next();
                    if(elemento!=null){
                        tipoPastoDao.delete(elemento.getId());
                    }
                }
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_TipoPasto_Dao fbTipoPastoDao = new FB_TipoPasto_Dao();
    //QUERY FIREBASE
    private static LiveData<List<TipoPasto>> fbAllTipoPasto=null;
    //ROOM DATABASE
    protected static TipoPasto_Dao tipoPastoDao = null;
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaTipoPasto(){
        if(fbAllTipoPasto==null){
            fbAllTipoPasto= fbTipoPastoDao.getAll_Synchronized();
        }
        if(tipoPastoDao==null){
            tipoPastoDao = db.TipoPastoDao();
        }
        if(!fbAllTipoPasto.hasObservers()){//SE NON HA già osservatori per rilevare aggiornamenti
            fbAllTipoPasto.observeForever(new Observer<List<TipoPasto>>() {
                @Override
                public void onChanged(List<TipoPasto> lista) {
                    if((lista!=null)){
                        executor.execute(new ThreadAggiornamento(lista));
                    }
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public TipoPasto_ThreadAggiornamento(Application application) {
        super(application);
        gestioneAggiornametoTabellaTipoPasto();
    }
/*================================================================================================*/
/*################################################################################################*/
}
