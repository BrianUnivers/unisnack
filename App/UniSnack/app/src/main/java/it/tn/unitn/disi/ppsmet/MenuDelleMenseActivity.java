package it.tn.unitn.disi.ppsmet;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.Fragment.Interface.OnFragmentMenuDelleMenseNelGiornoEventListener;
import it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.Fragment.MenuDelGiornoFragment;
import it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.Fragment.MenuDelleMenseFragment;
import it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.MenuDelleMenseViewModel;
import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.AppCompatActivityWithMyPrimaryMenu;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.GestioneMyIntent.MyIntent;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.SelettoreABarra.OnChangeListener.OnChangeListener;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.SelettoreABarra.SelettoreABarra;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.MySpinnerAdapter.SpinnerAdapter_Mense;

public class MenuDelleMenseActivity extends AppCompatActivityWithMyPrimaryMenu
                                    implements OnFragmentMenuDelleMenseNelGiornoEventListener {
    private final static Long ID_PASTO_LESTO    = 2l;
    private final static Long ID_PASTO_COMPLETO = 1l;
    /*################################################################################################*/
/*                                  IMPOSTA PARAMETRI DEI INTET PER MENU'LATERALE                 */
/*################################################################################################*/
    /**
     * Questo parametro viene chiamato nel momento in cui si preme un pulsante tra quelli del
     * menù laterale principale così da poter inserire i vari valori da passare tramite intent,
     * all'activity che segue. Conclusa questa procedura verrà attivato, l'intente per effetuare
     * il cabiamento d'activity, se chiamato dal menu laterare principale.
     * @param intent Questo parameto contiene l'intent pre impostata con l'activity d'arrivo.
     */
    @Override
    protected void setParamiter(MyIntent intent) {
        intent.putExtraIdUtente(MyIntent.I_UTENTE_ATTIVO,viewModel.getIdUtenteAttivo());
    }
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private MenuDelleMenseActivity activity = this;
/*------------------------------------------------------------------------*/
    private MenuDelleMenseViewModel viewModel;
/*------------------------------------------------------------------------*/
    private ViewGroup pagina;
    private Fragment fragmentListaGiorni;
/*------------------------------------------------------------------------*/
    private BottomNavigationView     bottomTabarView;
/*------------------------------------------------------------------------*/
    private SelettoreABarra<Mense> selettoreABarra;
/*------------------------------------------------------------------------*/
    private LiveData<List<Mense>>  ldListaMense;
/*------------------------------------------------------------------------*/
    private SpinnerAdapter_Mense adapter;
/*------------------------------------------------------------------------*/
    private Long          idUtenteAttivo;
    private List<Mense>   listaMensePerUtente = new LinkedList<>();
/*================================================================================================*/
/*                                  CREAZIONE DELL'ACTIVITY                                       */
/*================================================================================================*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_delle_mense);
/*------------------------------------------------------------------------*/
        //IMPOSTA MENU LETERALE
        super.initToolBar(R.menu.bar_menu_vuoto);
        super.impostaPaginaComeHome();
/*------------------------------------------------------------------------*/
        pagina = findViewById(R.id.cly_PaginaMenuDelleMense);
/*------------------------------------------------------------------------*/
        //GESTIONE VIEWMODEL E UTENTE ATTIVO
        viewModel =  new ViewModelProvider(this).get(MenuDelleMenseViewModel.class);
        viewModel.setParameterFromIntet(getIntent());
/*------------------------------------------------------------------------*/
        idUtenteAttivo = viewModel.getIdUtenteAttivo();
        if(idUtenteAttivo==null){
            super.doLogout();
        }else {
/*------------------------------------------------------------------------*/
            ldListaMense = viewModel.getMenseAccessibleByIdOfUtenteAtToday(idUtenteAttivo);
/*------------------------------------------------------------------------*/
            selettoreABarra = SelettoreABarra.newInstance(pagina, R.id.include_SelettoreMense);
            adapter = new SpinnerAdapter_Mense(activity, listaMensePerUtente, true);
            selettoreABarra.setAdapter(adapter);
/*------------------------------------------------------------------------*/
            selettoreABarra.setOnChangeListener(new OnChangeListener<Mense>() {
                @Override
                public void onChangeListener(View view, Mense itemSelected) {
                    viewModel.setMensaSelezzionata(itemSelected);
                }
            });
/*------------------------------------------------------------------------*/
            ldListaMense.observe(activity, new Observer<List<Mense>>() {
                @Override
                public void onChanged(List<Mense> listaMense) {
                    if (listaMense != null) {
                        listaMensePerUtente.clear();
                        listaMensePerUtente.addAll(listaMense);
                        adapter.notifyDataSetChanged();
                    }
                }
            });
        }
/*------------------------------------------------------------------------*/
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        fragmentListaGiorni = new MenuDelleMenseFragment();
        ft.replace(R.id.fly_MenuMensa, fragmentListaGiorni);
        ft.commit();
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
/*                                  BOTTONI DELLA TABAR IN BASSO                                  */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        bottomTabarView = findViewById(R.id.tbb_TabTipoMenu);
        bottomTabarView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return cambiaTipoMenuDaVisualizzare(menuItem);
            }
        });
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        bottomTabarView.setSelectedItemId(bottomTabarView.getSelectedItemId());
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE SELEZIONE ID TIPO MENU                               */
/*################################################################################################*/
/*================================================================================================*/
/*                                  SELEZIONE DEL TIPO MENU                                       */
/*================================================================================================*/
    /**
     * Questo metodo si occupa di selezionare del tipo di menu che si fa rifermento nei fragmanet.
     * In modo tale qualunque sia il fragment visualizato cambia il suo contenuto adatandosi ai nuovi
     * dati.
     * @param menuItem
     * @return
     */
    private boolean cambiaTipoMenuDaVisualizzare(@NonNull MenuItem menuItem){
        boolean ris = false;
        switch (menuItem.getItemId()){
            case R.id.btn_tab_PastoCompleto:
                ris = true;
                viewModel.setIdTipoPastoSelezzionato(ID_PASTO_COMPLETO);
                break;
            case R.id.btn_tab_PastoLesto:
                ris = true;
                viewModel.setIdTipoPastoSelezzionato(ID_PASTO_LESTO);
                break;
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE SELEZIONE FRAGMENT                                   */
/*################################################################################################*/
/*================================================================================================*/
/*                                  GESTIONE MENU DEL GIORNO                                      */
/*================================================================================================*/
    /**
     * Questo medodo permette di ricaricare il fragment contenete la lita dei giorni.
     * Questo metodo viene chiamato permedo sul tato indietro del fragment che mostra i singoli menu.
     */
    @Override
    public void chiudiMenuDelGiorno() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_right);
        if(fragmentListaGiorni==null){
            fragmentListaGiorni = new MenuDelleMenseFragment();
        }
        ft.replace(R.id.fly_MenuMensa, this.fragmentListaGiorni);
        ft.commit();
        //Reimposta come pagina principale
        impostaPaginaComeHome();
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo medodo permette sostoituire il fragment con quello che mostra il menu di un giorno specifico.
     * Questo metodo viene chiamato permedo sul singolo giorno del fragment con la lista dei giorni.
     */
    @Override
    public void apriMenuDelGiorno(Date date) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = MenuDelGiornoFragment.newInstance(date);
        ft.setCustomAnimations(R.anim.slide_in_right,R.anim.slide_out_left);
        ft.replace(R.id.fly_MenuMensa, fragment);
        ft.commit();
        //Imposta tasto indietro per passare all'altro frame
        this.getOnBackPressedDispatcher().addCallback(this,
                                new OnBackPressedCallback(true) {
                                    @Override
                                    public void handleOnBackPressed() {
                                        chiudiMenuDelGiorno();
                                    }
                                });
    }
/*================================================================================================*/
/*################################################################################################*/
}