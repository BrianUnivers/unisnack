package it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.Fragment.Interface.OnFragmentMenuDelleMenseNelGiornoEventListener;
import it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.MenuDelleMenseViewModel;
import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.MenuHeaderStaticFragment;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerMenuDelleMense.GiorniMenuDellaMensaRVAdapter;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.TitoloSopraContenutoView;
import it.tn.unitn.disi.ppsmet.R;

public class MenuDelleMenseFragment extends Fragment {
    /**
     * Questo parametro coterrà se sarà possibile l'accesso a determinati metodi dell'acticity contenete
     * il seguete fragment.
     */
    private OnFragmentMenuDelleMenseNelGiornoEventListener listener = null;
/*################################################################################################*/
/*                                  INIZIALIZZAZIONE VALORI                                       */
/*################################################################################################*/
/*================================================================================================*/
/*                                  COSTRUTTORE                                                   */
/*================================================================================================*/
    /**
     * Questo è il metodo costruttore del fragment.
     */
    public MenuDelleMenseFragment() {
        super();
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
//                                  COMUNICAZIONE CON L'ACTIVITY
/*################################################################################################*/
/*================================================================================================*/
/*                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY              */
/*================================================================================================*/
    /**
     * Questo metodo permete di estrarre un eventuale implementeazione da parte dell'activity(contesto
     * padre) così da poter accedere ad dei metodi esterni al fragmen.
     * @param context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (OnFragmentMenuDelleMenseNelGiornoEventListener) context;
        } catch (ClassCastException castException) {
            listener = null;
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private Fragment fragment = this;
/*------------------------------------------------------------------------*/
    private MenuDelleMenseViewModel viewModel    = null;
/*------------------------------------------------------------------------*/
    private ViewGroup rootView;
    private RecyclerView listaDateView;
/*------------------------------------------------------------------------*/
    private LiveData<Long> ldIdTipoMenu = null;
    private LiveData<Mense> ldMensa     = null;
/*------------------------------------------------------------------------*/
    private Long  idTipoMenu = null;
    private Mense mensa      = null;
    private List<Date> listaDate = new LinkedList<>();
/*================================================================================================*/
/*                                  CREAZIONE DELL'FRAGMENT                                       */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'inizializazione del layout del fragment ed inoltre contiene le loro
     * possibili interazioni con l'utente.
     * In aggiunta viene richesto un modelView che è quello usato dall'activity in modo tale da accedere
     * alla sua stessa istanza, in modo tale da utilizarlo come metodo per passare parameti da una parte
     * all'altra.
     *
     * In questo caso viene usato per ricevere come informazione il tipo di menu che si vule
     * visualizzare e di quale mensa.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @SuppressLint("FragmentLiveDataObserve")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = (ViewGroup)  inflater.inflate(R.layout.fragment_menu_delle_mense, container, false);
/*------------------------------------------------------------------------*/
        listaDateView = rootView.findViewById(R.id.rcl_ListaGiorniMensa);
        listaDateView.setLayoutManager(new LinearLayoutManager(getContext()));
        inizializzaListaGiorni();
/*------------------------------------------------------------------------*/
        try {
            viewModel = new ViewModelProvider(getActivity()).get(MenuDelleMenseViewModel.class);
        }catch (Exception e){
            viewModel=null;
        }
/*------------------------------------------------------------------------*/
        if(viewModel!=null){
            ldMensa = viewModel.getMensaSelezzionata();
            ldIdTipoMenu = viewModel.getIdTipoPastoSelezzionato();
        }
/*------------------------------------------------------------------------*/
        if((ldMensa!=null) && (ldIdTipoMenu!=null)){
            //Agiornameto Mensa selezionata
            ldMensa.observe(fragment, new Observer<Mense>() {
                @Override
                public void onChanged(Mense nuovaMense) {
                    if(nuovaMense!=null){
                        impostaPaginaConNuoviDati(nuovaMense,idTipoMenu);
                    }
                }
            });
            //Aggiornamento Tipo Menu selezionato
            ldIdTipoMenu.observe(fragment, new Observer<Long>() {
                @Override
                public void onChanged(Long nuovoIdTipoMensa) {
                    if(nuovoIdTipoMensa!=null){
                        impostaPaginaConNuoviDati(mensa,nuovoIdTipoMensa);
                    }
                }
            });
        }
/*------------------------------------------------------------------------*/
        return rootView;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  CREAZIONE LISTA GIORNI DA VISUALIZZARE                        */
/*================================================================================================*/
    /**
     * Questo metodo genera una lista di giorni che dovranno essere rappresentati nella reciclerview
     * in modo da selezionare quello che si vule vedere.
     */
    private void inizializzaListaGiorni(){
        listaDate.clear();
        Date data = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(data);
        for (int i=0; i<10; i++ ){
            listaDate.add(data);//Aggoungo anche oggi
            c.add(Calendar.DATE, 1);
            data=c.getTime();
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  INSERIMENTO E AGGIORNAMENTO RECYCLERVIEW                      */
/*================================================================================================*/
    /**
     * Questo metodo verifica che ci siano impostati i due valori che possono variare in ogni momento
     * e con quelli richiedo al database i dati delle pietanze del giorno attuale così da poter
     * selezionare tre pietanze a caso tra di esse da rappresentare come sfondo del elemento contenente
     * la data di oggi.
     * @param mensa
     * @param idTipoMenu
     */
    private synchronized void impostaPaginaConNuoviDati(Mense mensa,Long idTipoMenu){
        this.mensa     = mensa;
        this.idTipoMenu=idTipoMenu;
        if((mensa!=null) && (idTipoMenu!=null)){
            LiveData<List<Pietanze>> ldListaPietanze = viewModel.getPietanzeByIdOfMensa_TipoMenu_AtSpecificDate(mensa.getId(),idTipoMenu,new Date());
            if(ldListaPietanze!=null){
                ldListaPietanze.observe(fragment, new Observer<List<Pietanze>>() {
                    @Override
                    public void onChanged(List<Pietanze> liataPietanze) {
                        if(liataPietanze!=null){
                            List<String> listaUrlImage = selezionaImmagini(liataPietanze);
                            GiorniMenuDellaMensaRVAdapter adapter = new GiorniMenuDellaMensaRVAdapter(getContext(),listaDate,listaUrlImage,listener);
                            listaDateView.setAdapter(adapter);
                        }
                    }
                });
            }
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                 SELEZIONE DELLE IMMAGINI DA UTILIZZARE COME SFONDO DEL EELEMETO*/
/*================================================================================================*/
    /**
     * Questo metodo permette di mescolare le pietanze ricevute nella lista passata e prendere i primi
     * tre per generare una nuova lista con i URL di tali immagini.
     * Questo perchè sarà parte del input del adapter utilizzato per la selzeione dei giorni.
     * @param liataPietanze
     * @return
     */
    private List<String> selezionaImmagini(List<Pietanze> liataPietanze){
        List<String> ris = new LinkedList<>();
        Collections.shuffle(liataPietanze);
        Iterator<Pietanze> iterator = liataPietanze.iterator();
        int i=0;
        while (iterator.hasNext() && i<3){
            Pietanze pietanze=iterator.next();
            if((pietanze!=null) && (pietanze.getUrlImmagine()!=null) && (!pietanze.getUrlImmagine().equals("")) ) {
                i++;
                ris.add(pietanze.getUrlImmagine());
            }
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
}