package it.tn.unitn.disi.ppsmet.Entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity(tableName = "tableUniversita",
        indices = { @Index(value = {"sigla"}, unique = true)})
public class Universita {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @PrimaryKey
    private Long    id;
    @NonNull
    @ColumnInfo(name = "nome")
    private String  nome;
    @NonNull
    @ColumnInfo(name = "sigla")
    private String  sigla;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    @Ignore
    public Universita() {}
//--------------------------------------------------------------------------------------------------
    public Universita(Long id,
                      @NonNull String nome,
                      @NonNull String sigla) {
        this.id = id;
        this.nome = nome;
        this.sigla = sigla;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    public Long getId() {
        return id;
    }
//--------------------------------------
    public void setId(Long id) {
        this.id = id;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getNome() {
        return nome;
    }
//--------------------------------------
    public void setNome(@NonNull String nome) {
        this.nome = nome;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getSigla() {
        return sigla;
    }
//--------------------------------------
    public void setSigla(@NonNull String sigla) {
        this.sigla = sigla;
    }
//==================================================================================================
//==================================================================================================
//                                      EQUALS E HASH_CODE
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Universita)) return false;
        Universita that = (Universita) o;
        return Objects.equals(getId(), that.getId()) &&
                getNome().equals(that.getNome()) &&
                getSigla().equals(that.getSigla());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNome(), getSigla());
    }
//==================================================================================================
//##################################################################################################
}
