package it.tn.unitn.disi.ppsmet.ActivityManager.StatisticheUtenteActivity;

import android.app.Application;

import androidx.annotation.NonNull;

import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.InterfaceViewModel.MenuProfiloUtenteViewModel;

public class StatisticheUtenteViewModel extends MenuProfiloUtenteViewModel {
/*################################################################################################*/
/*================================================================================================*/
/*                                  COSTRUTTORE VIEW MODEL                                        */
/*================================================================================================*/
    /**
     * Questo metodo costruttore del viewmode inizializza le repository che si interfacciano con entrambi
     * i database sia interno che estreno a seconda delle operazioni che vengono richeste. Che verranno
     * utilizate dall'activity e i suoi fragment.
     * @param application
     */
    public StatisticheUtenteViewModel(@NonNull Application application) {
        super(application);
    }
/*================================================================================================*/
/*################################################################################################*/
}
