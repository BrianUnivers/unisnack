package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import com.google.firebase.database.annotations.NotNull;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Utenti_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Utenti_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class Utenti_Repository  extends Utenti_ThreadAggiornamento {
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA MENSE (e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
    private FB_Utenti_Dao fbUtentiDao;
    private LiveData<List<Utenti>> mAllUtenti;
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Utenti_Repository(Application application) {
        super(application);
        fbUtentiDao = new FB_Utenti_Dao();
        //AllUtenti get any time the same result
        mAllUtenti = utentiDao.getAll_Synchronized();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERT                                                                 */
/*================================================================================================*/
    private Future<Long> insert(Utenti utenti){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
            @Override
            public Long call() throws Exception {
                Long id;
                try {
                    id = utentiDao.insert(utenti);
                }catch (Exception e){
                    Log.d("INSERT_UNTENTI","Utente già inserito.\n\t"+e.getMessage(),e);
                    id = -1l;
                }
                return id;
            }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         ISCRIZIONE NUOVO UTENTE                                                */
/*================================================================================================*/
    public LiveData<Long> iscrizioneUtente(Utenti utenti){
        MediatorLiveData<Long> idUtenteLiveData = new MediatorLiveData<>();
        LiveData<Long> untenteInseritoFrireBase =fbUtentiDao.insert(utenti);
        idUtenteLiveData.addSource(untenteInseritoFrireBase, new Observer<Long>() {
            @Override
            public void onChanged(Long idUtente) {
                if((idUtente!=null) && (idUtente>0)){
                    utenti.setId(idUtente);
                    insert(utenti);
                }
                idUtenteLiveData.postValue(idUtente);
                idUtenteLiveData.removeSource(untenteInseritoFrireBase);
            }
        });
       return idUtenteLiveData;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    /*public void deleteAll(){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            utentiDao.deleteAll();
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    /*public void update(Utenti utenti){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            utentiDao.update(utenti);
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    /*public LiveData<List<Utenti>> getAll(){
        return mAllUtenti;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    public LiveData<Utenti> getByPrimaryKey(Long id){
        return utentiDao.getByPrimaryKey_Synchronized(id);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by AUTENTICATION                                            */
/*================================================================================================*/
    public LiveData<Utenti> getByAuthentication(String email,String password){
        MediatorLiveData<Utenti> utenteAutenticato = new MediatorLiveData<>();
        utenteAutenticato.addSource(fbUtentiDao.getByAuthentication_OneTime(email, password),
                new Observer<Utenti>() {
                    @Override
                    public void onChanged(Utenti utenti) {
                        if((utenti!=null) && (utenti.getId()!=null)){
                            insert(utenti);
                            utenteAutenticato.addSource(utentiDao.getByPrimaryKey_Synchronized(utenti.getId()),
                                    new Observer<Utenti>() {
                                        @Override
                                        public void onChanged(Utenti utenti) {
                                            utenteAutenticato.setValue(utenti);
                                        }
                                    });
                        }else{
                            //In caso di errore
                            utenteAutenticato.setValue(null);
                        }
                    }
                });
        return utenteAutenticato;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by SESSIONE                                                 */
/*================================================================================================*/
    public Future<Utenti>  getByIdSessione(String idSessione){
        return MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Utenti>(){
                    @Override
                    public Utenti call() throws Exception {
                        return utentiDao.getByIdSessione(idSessione);
                    }
                });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  ESISTE UTENTE CON EMAIL                                       */
/*================================================================================================*/
    public LiveData<Boolean> esiteEmail (@NotNull String email){
        return fbUtentiDao.esiteEmail_OneTime(email);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  ESISTE UTENTE CON EMAIL                                       */
/*================================================================================================*/
    public LiveData<Boolean> esiteMatricola (@NotNull Long idUniversita,@NotNull Long idCrso  ,@NotNull Integer matricola){
        return fbUtentiDao.esiteMatricola_OneTime(idUniversita,idCrso,matricola);
    }
/*================================================================================================*/
/*################################################################################################*/
}
