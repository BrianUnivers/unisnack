package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerMenuDelGiorno;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.Fragment.Interface.OnFragmentMenuDelleMenseNelGiornoEventListener;
import it.tn.unitn.disi.ppsmet.GestioneAssetPerFoto.GestioneFotoDaAsset;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.GiornoSelezionatoRowView;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ShapedImageView;
import it.tn.unitn.disi.ppsmet.R;

public class MenuDelGiornoRVAdapter extends RecyclerView.Adapter<MenuDelGiornoRVAdapter.MyViewHoder> {
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE INTERNA PADRE                                              */
/*################################################################################################*/
    /**
     * Questa classe è quella padre di quelle che vengono usate realmente che permettono alla RecyclingView
     * Adapter di definire un layout custom del singolo elemeto,in due formati distini.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public ViewGroup v_mainLayout;
        public TextView  v_nomeElemento;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout      = itemView.findViewById(R.id.cly_RowElemento);
            v_nomeElemento    = itemView.findViewById(R.id.txt_Elemento);
        }
/*================================================================================================*/
    }
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE INTERNA FIGLIO TIPO PIETANZA                               */
/*################################################################################################*/
    /**
     * Questa classe è quella che verrà usata realmente, che rappresentano i tipi di pietanza in modo
     * tale che il RecyclingView  Adapter di definire un layout custom del singolo elemeto.
     */
    protected class MyViewHoderTipoPietanza extends MyViewHoder  {
/*================================================================================================*/
//                              COSTRUTTORI
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoderTipoPietanza(@NonNull View itemView) {
            super(itemView);
        }
/*================================================================================================*/
    }
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE INTERNA FIGLIO OGGI                                        */
/*################################################################################################*/
    /**
     * Questa classe è quella che verrà usata realmente, che rappresentano la singola pietanza in modo
     * tale che il RecyclingView  Adapter di definire un layout custom del singolo elemeto.
     */
    protected class MyViewHoderPietanza extends MyViewHoder  {
        public ImageView v_imgInfo;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoderPietanza(@NonNull View itemView) {
            super(itemView);
            v_imgInfo = itemView.findViewById(R.id.img_Info);
        }
/*================================================================================================*/
    }
/*################################################################################################*/
/*################################################################################################*/



/*################################################################################################*/
/*                              CLASSE PRINCIPALE MenuDelGiornoRVAdapter                          */
/*################################################################################################*/
    private static final int TIPO_PIETANZA = 0;
    private static final int PIETANZA     = 2;
/*------------------------------------------------------------------------------------------------*/
    private Context comtext=null;
    private List<Pair<Boolean,String>> listaPietanzeETipi = null;
    /*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public MenuDelGiornoRVAdapter(Context comtext,
                                  List<Pair<Boolean,String>> listaPietanzeETipi) {
        this.comtext = comtext;
        this.listaPietanzeETipi = listaPietanzeETipi;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              SELEZIONE DEL TIPO DI OGGETTO DA RAPPRESENTARE                    */
/*================================================================================================*/
    /**
     * Questo modtodo permette di individuare se l'elemento che si vule visualizzare è una pientanza
     * (se assonciata alla stigna c'è true) o se è un tipo di pietanza (se assonciata alla stigna c'è false).
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        Pair<Boolean,String> elemento = listaPietanzeETipi.get(position);
        int ris = TIPO_PIETANZA;
        if(elemento.first){
            ris = PIETANZA;
        }
        return ris;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              CERAZIONE DEL SINGOLO ELEMETOTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali. Questo lo fa tenendo conto del tipo di "viewType", infatti
     * in esso è contenuta l'infoemazione per distigure le varie rappresentazioni.
     */
    @NonNull
    @Override
    public MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(comtext);
        MyViewHoder myViewHoder;
        switch (viewType) {
            case TIPO_PIETANZA:{
                View bloccoElemeto = inflater.inflate(R.layout.row_menu_del_giorno_tipo_pietanza, parent,false);
                myViewHoder = new MyViewHoderTipoPietanza(bloccoElemeto);
                break;
            }
            case PIETANZA:
            default:{
                View bloccoElemeto = inflater.inflate(R.layout.row_menu_del_giorno_pietanza, parent,false);
                myViewHoder = new MyViewHoderPietanza(bloccoElemeto);
                break;
            }
        }

        return myViewHoder;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              INSERIRE L'ELEMETO NEL LAYOUT                                     */
/*================================================================================================*/
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore MenuDelGiornoRVAdapter.
     */
    @Override
    public void onBindViewHolder(@NonNull MyViewHoder holder, int position) {
        final  Pair<Boolean,String> elemento  =listaPietanzeETipi.get(position);
        //Seleziona il tipo di layout
        if(holder instanceof MyViewHoderTipoPietanza){
            inserisciTipoPietanzaLayout((MyViewHoderTipoPietanza)holder, elemento.second);
        }else if(holder instanceof MyViewHoderPietanza){
            inserisciPietanzaLayout((MyViewHoderPietanza)holder, elemento.second);
        }
    }
/*================================================================================================*/
/*                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE                  */
/*================================================================================================*/
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaPietanzeETipi.size();
    }
/*================================================================================================*/


/*================================================================================================*/
/*                              INSERISCI I DATI NEL LAYOUT                                       */
/*================================================================================================*/
    /**
     * Questo metodo si occupa di inserire i dati del tipo pietanza nel layout del singolo elemeto.
     */
    private void inserisciTipoPietanzaLayout(@NonNull MyViewHoderTipoPietanza holder, String nome){
        holder.v_nomeElemento.setText(nome);
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo si occupa di inserire i dati della pietanza che è quello attuale  nel layout del
     * singolo elemeto.
     */
    private void inserisciPietanzaLayout(@NonNull MyViewHoderPietanza holder, String nome){
        holder.v_nomeElemento.setText(nome);

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  IMPOSTA COME BOTTONE APERTURA DEL LINK IN UN BROWER
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        holder.v_imgInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String paramettro = new String(nome);
                paramettro.replaceAll("( +)","%20").trim();
                String urlStringa = new StringBuffer("https://www.google.com/search?q=").append(paramettro).toString();
                Uri ricerca = Uri.parse(urlStringa);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, ricerca);
                comtext.startActivity(browserIntent);
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
/*================================================================================================*/
/*################################################################################################*/
}