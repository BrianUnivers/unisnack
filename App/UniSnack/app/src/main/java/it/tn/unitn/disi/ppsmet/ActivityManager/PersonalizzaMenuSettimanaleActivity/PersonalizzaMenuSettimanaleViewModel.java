package it.tn.unitn.disi.ppsmet.ActivityManager.PersonalizzaMenuSettimanaleActivity;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.InterfaceViewModel.MenuProfiloUtenteViewModel;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Ordinazioni_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.TipoPasto_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.TipoPietanza_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;

public class PersonalizzaMenuSettimanaleViewModel extends MenuProfiloUtenteViewModel {
/*################################################################################################*/
    private TipoPietanza_Repository rTipoPietanza;
    private Ordinazioni_Repository  rOrdinazioni;
    private TipoPasto_Repository    rTipoPasto;
/*================================================================================================*/
/*                                  COSTRUTTORE VIEW MODEL                                        */
/*================================================================================================*/
    /**
     * Questo metodo costruttore del viewmode inizializza le repository che si interfacciano con entrambi
     * i database sia interno che estreno a seconda delle operazioni che vengono richeste. Che verranno
     * utilizate dall'activity e i suoi fragment.
     * @param application
     */
    public PersonalizzaMenuSettimanaleViewModel(@NonNull Application application) {
        super(application);
        rTipoPietanza = new TipoPietanza_Repository(application);
        rOrdinazioni  = new Ordinazioni_Repository(application);
        rTipoPasto   = new TipoPasto_Repository(application);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  AZZIONI LEGATE ALLA GESITIONE DEI DATABASE                    */
/*################################################################################################*/
/*================================================================================================*/
/*                                  ORDINAZIONI                                                   */
/*================================================================================================*/
    /**
     * Questo metodo permette di richiedere a database (locale) tutte le ordinazioni a partire da quelle
     * di oggi, dell'utente indicato con l'id passato come parametro.
     * @param idUtente
     * @return
     */
    public LiveData<List<Ordinazioni>> getAllOrdinazioniByIdUtente_InTheWeek(Long idUtente){
        return rOrdinazioni.getByIdOfUtenti_InTheFutureOfSpecificDate(idUtente,selezionePrimoGiornoDellaSettimana());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  ORDINAZIONE PIETANZE DELLA MENSE CON VOTO                     */
/*================================================================================================*/
    /**
     * Questo metodo permette di richiedere al database tutte le pietanze ordinate in una mensa che fanno
     * parte di un ordine fatto dall'utente idicato con l'identificatore passato come parametro, tali
     * che siano nel futuro compreso quelli di oggi.
     * @param idUtente
     * @return
     */
    public LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> getAlOrdinazionePietanzeDellaMenseConVotoByIdUtente_InTheWeek(Long idUtente){
        return rOrdinazioni.getAllOrdinazionePietanzeDellaMenseConVotoByIdUtente_InTheFuture(idUtente,selezionePrimoGiornoDellaSettimana());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  TIPO PIETANZA                                                 */
/*================================================================================================*/
    /**
     * Questo metodo reichiede al database (interno) di restituire  tutte le tipologie di pietanze
     * presenti.
     * @return
     */
    public LiveData<List<TipoPietanza>> getAllTipoPietanza(){
        return rTipoPietanza.getAll();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  TIPO PASTO                                                    */
/*================================================================================================*/
    /**
     * Questo metodo permette di ricevere la lista di tutti i tipi di pasto (pranzo,cena) presenti
     * nel database sempre aggiornati.
     * @return
     */
    public LiveData<List<TipoPasto>> getAllTipoPasto(){
        return rTipoPasto.getAll();
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GENERA LISTA DATE DELLA SETTIMANA                             */
/*################################################################################################*/
private static final Integer LUNGHEZZA_SETTIMANA_LAVORATIVA = 5;
/*================================================================================================*/
/*                                  SELEZIONE PRIMO GIORNO DELLA SETTIMANA                        */
/*================================================================================================*/
    /**
     * Questo metodo deve infividuare il primo giorno della settimana corrente inteso come il primo
     * lunedì che ha almeno un giorno lavorativo che sia nel futuro, cioè che deve ancora finire
     * in modo tale da permette di avere almeno un giorno che sia possibile modificare il menu personalizato.
     * @return
     */
    private Date selezionePrimoGiornoDellaSettimana(){
        Date oggi = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(oggi);
        int offset = 0;
        switch(cal.get(Calendar.DAY_OF_WEEK)){
            case Calendar.MONDAY:
                offset = 0;
                break;
            case Calendar.TUESDAY:
                offset = -1;
                break;
            case Calendar.WEDNESDAY:
                offset = -2;
                break;
            case Calendar.THURSDAY:
                offset = -3;
                break;
            case Calendar.FRIDAY:
                offset = -4;
                break;
            case Calendar.SATURDAY:
                offset =  2;
                break;
            case Calendar.SUNDAY:
                offset =  1;
                break;
        }
        cal.add(Calendar.DATE, offset);
        return cal.getTime();
    }
/*================================================================================================*/
/*################################################################################################*/
}
