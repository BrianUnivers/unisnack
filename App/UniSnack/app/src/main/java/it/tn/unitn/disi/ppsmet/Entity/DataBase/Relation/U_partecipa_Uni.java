package it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

@Entity(tableName = "tableU_partecipa_Uni",
        primaryKeys = {"id_utente","id_universita","id_AA"},
        foreignKeys = {
            @ForeignKey(entity = Utenti.class,
                parentColumns = "id",       //Colanna della classe a qui si rifierisce
                childColumns  = "id_utente",//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE),
            @ForeignKey(entity = Universita.class,
                parentColumns = "id",           //Colanna della classe a qui si rifierisce
                childColumns  = "id_universita",//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE),
            @ForeignKey(entity = AnnoAccademico.class,
                parentColumns = "id",   //Colanna della classe a qui si rifierisce
                childColumns  = "id_AA",//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE)},
        indices = {
                @Index(value = {"id_utente"}),
                @Index(value = {"id_universita"}),
                @Index(value = {"id_AA"})})
public class U_partecipa_Uni {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @ColumnInfo(name = "id_utente")
    private Long    id_utente;
    @NonNull
    @ColumnInfo(name = "id_universita")
    private Long    id_universita;
    @NonNull
    @ColumnInfo(name = "id_AA")
    private Long    id_AA;
    @NonNull
    @ColumnInfo(name = "statoTessera")
    private Boolean statoTessera;
    @ColumnInfo(name = "QRcode")
    private byte[]  QRcode = null;
    @ColumnInfo(name = "urlQRCode")
    private String urlQRCode =null;
    @ColumnInfo(name = "codiceABarre")
    private byte[]  codiceABarre = null;
    @ColumnInfo(name = "urlCodiceABarre")
    private String urlCodiceABarre =null;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    @Ignore
    public U_partecipa_Uni(@NonNull Long id_utente,
                           @NonNull Long id_universita,
                           @NonNull Long id_AA,
                           @NonNull Boolean statoTessera) {
        this.id_utente = id_utente;
        this.id_universita = id_universita;
        this.id_AA = id_AA;
        this.statoTessera = statoTessera;
    }
//--------------------------------------------------------------------------------------------------
    public U_partecipa_Uni(@NonNull Long id_utente,
                           @NonNull Long id_universita,
                           @NonNull Long id_AA,
                           @NonNull Boolean statoTessera,
                           byte[] QRcode,
                           String urlQRCode,
                           byte[] codiceABarre,
                           String urlCodiceABarre) {
        this.id_utente = id_utente;
        this.id_universita = id_universita;
        this.id_AA = id_AA;
        this.statoTessera = statoTessera;
        this.QRcode = QRcode;
        this.urlQRCode = urlQRCode;
        this.codiceABarre = codiceABarre;
        this.urlCodiceABarre = urlCodiceABarre;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    @NonNull
    public Long getId_utente() {
        return id_utente;
    }
//--------------------------------------
    public void setId_utente(@NonNull Long id_utente) {
        this.id_utente = id_utente;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_universita() {
        return id_universita;
    }
//--------------------------------------
    public void setId_universita(@NonNull Long id_universita) {
        this.id_universita = id_universita;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_AA() {
        return id_AA;
    }
//--------------------------------------
    public void setId_AA(@NonNull Long id_AA) {
        this.id_AA = id_AA;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Boolean getStatoTessera() {
        return statoTessera;
    }
//--------------------------------------
    public void setStatoTessera(@NonNull Boolean statoTessera) {
        this.statoTessera = statoTessera;
    }
//--------------------------------------------------------------------------------------------------
    public byte[] getQRcode() {
        return QRcode;
    }
//--------------------------------------
    public Bitmap getBitmapQRcode() {
        //Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
        //Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);
        return BitmapFactory.decodeByteArray(QRcode, 0, QRcode.length);
    }
//--------------------------------------
    public void setQRcode(byte[] QRcode) {
        this.QRcode = QRcode;
    }
//--------------------------------------------------------------------------------------------------
    public String getUrlQRCode() {
        return urlQRCode;
    }
//--------------------------------------
    public void setUrlQRCode(String urlQRCode) {
        this.urlQRCode = urlQRCode;
    }
//--------------------------------------------------------------------------------------------------
    public byte[] getCodiceABarre() {
        return codiceABarre;
    }
//--------------------------------------
    public Bitmap getBitmapCodiceABarre() {
        //Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
        //Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);
        return BitmapFactory.decodeByteArray(codiceABarre, 0, codiceABarre.length);
    }
//--------------------------------------
    public void setCodiceABarre(byte[] codiceABarre) {
        this.codiceABarre = codiceABarre;
    }
//--------------------------------------------------------------------------------------------------
    public String getUrlCodiceABarre() {
        return urlCodiceABarre;
    }
//--------------------------------------
    public void setUrlCodiceABarre(String urlCodiceABarre) {
        this.urlCodiceABarre = urlCodiceABarre;
    }
//==================================================================================================
//##################################################################################################
}
