package it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.MySpinnerAdapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.R;

public class SpinnerAdapter_Mense extends ArrayAdapter<Mense> {
/*################################################################################################*/
    private Boolean isTitolo = false;
/*================================================================================================*/
/*                                  COSTRUTTRI                                                    */
/*================================================================================================*/
    public SpinnerAdapter_Mense(Context context, List<Mense> lista){
        super(context,0,lista);
        this.isTitolo = false;
    }
/*------------------------------------------------------------------------------------------------*/
    public SpinnerAdapter_Mense(Context context, List<Mense> lista, Boolean isTitolo){
        super(context,0,lista);
        this.isTitolo= isTitolo;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA VISUALIZZAZIONE DEL ELEMENTO SELEZIONATO              */
/*================================================================================================*/
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent,isTitolo);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA VISUALIZZAZIONE DEI ELEMENTI ELEMENTO                 */
/*================================================================================================*/
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent,false);
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  IMPOSTA IL LAYOUT                                             */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA IL LAYOUT PER LE TIGHE                                */
/*================================================================================================*/
    /**
     * Questo metodo restituisce la view da usare per la singola voce sia selezionata sia per la lista
     * dello spinner.
     */
    private View initView(int position, View convertView,  ViewGroup parent,boolean isTitolo){
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.row_spinner,
                    parent,false);
        }
        TextView viewTesto = convertView.findViewById(R.id.txt_Testo);

        Mense elemento = getItem(position);
        if(elemento!=null) {
            viewTesto.setText(elemento.getNome());
            if(isTitolo) {
                viewTesto.setTypeface(null, Typeface.BOLD);
                viewTesto.setTextColor(viewTesto.getResources().getColor(R.color.colorPrimary, null));
                viewTesto.setGravity(Gravity.CENTER_HORIZONTAL);
            }
        }
        return convertView;
    }
/*================================================================================================*/
}
