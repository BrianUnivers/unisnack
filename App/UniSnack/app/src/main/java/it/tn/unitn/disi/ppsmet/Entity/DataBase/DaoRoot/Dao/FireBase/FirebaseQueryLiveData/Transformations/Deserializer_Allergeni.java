package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.util.Iterator;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;

public class Deserializer_Allergeni implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto Allergeni.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String LOG_TAG = "Allergeni_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "Allergeni" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "Allergeni".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public static Allergeni estraiDaDataSnapshot(DataSnapshot dataSnapshot){
        Allergeni allergeni = null;
        if(dataSnapshot.exists()){
            try{
                //Campi not null
                Long   id          = Long.parseLong( dataSnapshot.getKey());
                String nome        = dataSnapshot.child(P_NOME).getValue(String.class);
                String descrizione = dataSnapshot.child(P_DESCRIZIONE).getValue(String.class);
                //Verifica campi not null
                if((id!=null) && (nome!=null) && (descrizione!=null)){
                    allergeni = new Allergeni(id,nome,descrizione);
                }
            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
                allergeni = null;
            }
        }
        return allergeni;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         GET AN ACTRIBUTE OBJECT FOR AN OTHER                                   */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET AN ACTRIBUTE                                                       */
/*------------------------------------------------------------------------------------------------*/
    /**
     * Qusrto metodo restituisce, se esiste, un singolo ogetto "Allergeni" da un attributo di un altro
     * oggetto attraverso una sottoparte dell'oggetto "DataSnapshot".
     * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
     *                     informazioni utili per creare un oggetto di tipo "Allergeni".
     * @return  Il riutato è il primo oggetto, valido se ce ne fossero più d'uno ritorna il primo trovato
     *          mentre se non ce ne sono ritorna null.
     */
    public static Allergeni getAttributeOf(DataSnapshot dataSnapshot,String nomeAttributo){
        Allergeni allergeni = null;
        DataSnapshot allergeniDataSnapshot = dataSnapshot.child(nomeAttributo);
        if(allergeniDataSnapshot.exists()){
            Iterator<DataSnapshot> iterator =allergeniDataSnapshot.getChildren().iterator();
           while ( (iterator.hasNext()) && (allergeni==null) ){//Restituisce il primo oggetto valido
                allergeni = estraiDaDataSnapshot(iterator.next());
           }
        }
        return allergeni;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, Allergeni>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Object extends GenericDeserializer_Object<Allergeni> {
        @Override
        public Allergeni estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Allergeni.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<Allergeni>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List<Allergeni> {
        @Override
        public Allergeni estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Allergeni.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
