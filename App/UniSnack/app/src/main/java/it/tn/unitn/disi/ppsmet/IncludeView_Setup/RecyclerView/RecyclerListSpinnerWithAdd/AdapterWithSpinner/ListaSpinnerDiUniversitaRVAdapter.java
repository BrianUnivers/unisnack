package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListSpinnerWithAdd.AdapterWithSpinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListSpinnerWithAdd.AdapterWithSpinner.Interface.ListaSpinnerRVAdapter;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.MySpinnerAdapter.SpinnerAdapter_Universita;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.ViewInputSpinner_Text;
import it.tn.unitn.disi.ppsmet.R;

public class ListaSpinnerDiUniversitaRVAdapter  extends ListaSpinnerRVAdapter<ListaSpinnerDiUniversitaRVAdapter.MyViewHoder,Universita> {
/*################################################################################################*/
/*                              CLASSE INTERNA                                                    */
/*################################################################################################*/
    /**
     * Questa classe è quella che permette alla RecyclingView Adapter di definire un layout custom
     * del singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public ViewGroup   mainLayout;
        //Componeti
        public Button      btnCancellaElemento;
        public ViewInputSpinner_Text<Universita> spinner;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            mainLayout      = itemView.findViewById(R.id.cly_GruppoSpinnerButton);
            spinner         = new ViewInputSpinner_Text(mainLayout,R.id.include_Spinner,R.string.lbl_Universita, Universita.class);
            btnCancellaElemento = itemView.findViewById(R.id.btn_Cancella);
        }
/*================================================================================================*/
    }
/*################################################################################################*/

/*################################################################################################*/
/*                              CLASSE PRINCIPALE ListaSpinnerDiUniversitaRVAdapter               */
/*################################################################################################*/
    private Context context;
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public ListaSpinnerDiUniversitaRVAdapter(Context context, List<Universita> listaUniversita) {
        this.adapter=new SpinnerAdapter_Universita(context,listaUniversita);
        this.context = context;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                              CERAZIONE DEL SINGOLO ELEMETOTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View bloccoElemeto = inflater.inflate(R.layout.row_text_spinner_with_button, parent,false);
        return new MyViewHoder(bloccoElemeto);
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              INSERIRE L'ELEMETO NEL LAYOUT                                     */
/*================================================================================================*/
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista costruita
     * all'interno del crostruttore  di ListaSpinnerDiUniversitaRVAdapter con le agginte fatte in seguito.
     * Inoltre aggiunge la possibilità di premere sul singolo pulsante così da cancellare l'elemento
     * selezionato dalla lista che si stà mostrando.
     **/
    @Override
    public void onBindViewHolder(@NonNull MyViewHoder holder, int position) {
        Integer idSelezionatori = listaIdSelezionatori.get(position);
        //Inserisci i dati per spinner
        holder.spinner.setAdapter(adapter);
        selezionatori.put(idSelezionatori,holder.spinner);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  IMPOSTA COME BOTTONE IL CAMPIONATO
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        holder.btnCancellaElemento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eliminaElemento(idSelezionatori);
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
/*================================================================================================*/
}
