package it.tn.unitn.disi.ppsmet.Entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity(tableName = "tableMense",
        foreignKeys = {@ForeignKey(entity = Universita.class,
                parentColumns = "id",            //Colanna della classe a qui si rifierisce
                childColumns  = "id_universita", //ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE)},
        indices = @Index("id_universita"))
public class Mense {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @PrimaryKey
    private Long   id;
    @NonNull
    @ColumnInfo(name = "nome")
    private String nome;
    @NonNull
    @ColumnInfo(name = "sigla")
    private String sigla;
    @ColumnInfo(name = "longitudine")
    private Double longitudine = null;
    @ColumnInfo(name = "latitudine")
    private Double latitudine = null;
    @NonNull
    @ColumnInfo(name = "id_universita")
    private Long   id_Universita;
    @Ignore
    private Universita universita = null;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    @Ignore
    public Mense() { }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Mense(Long id,
                 @NonNull String nome,
                 @NonNull String sigla,
                 @NonNull Long id_Universita) {
        this.id = id;
        this.nome = nome;
        this.sigla = sigla;
        this.id_Universita = id_Universita;
    }
//--------------------------------------------------------------------------------------------------
    public Mense(Long id,
                 @NonNull String nome,
                 @NonNull String sigla,
                 Double longitudine,
                 Double latitudine,
                 @NonNull Long id_Universita) {
        this.id = id;
        this.nome = nome;
        this.sigla = sigla;
        this.longitudine = longitudine;
        this.latitudine = latitudine;
        this.id_Universita = id_Universita;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Mense(Long id,
                 String nome,
                 String sigla,
                 Universita universita) {
        this.id = id;
        this.nome = nome;
        this.sigla = sigla;
        setUniversita(universita);
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Mense(Long id,
                 String nome,
                 String sigla,
                 Double longitudine,
                 Double latitudine,
                 Universita universita) {
        this.id = id;
        this.nome = nome;
        this.sigla = sigla;
        this.longitudine = longitudine;
        this.latitudine = latitudine;
        setUniversita(universita);
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    public Long getId() {
        return id;
    }
//--------------------------------------
    public void setId(Long id) {
        this.id = id;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getNome() {
        return nome;
    }
//--------------------------------------
    public void setNome(@NonNull String nome) {
        this.nome = nome;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getSigla() {
        return sigla;
    }
//--------------------------------------
    public void setSigla(@NonNull String sigla) {
        this.sigla = sigla;
    }
//--------------------------------------------------------------------------------------------------
    public Double getLongitudine() {
        return longitudine;
    }
//--------------------------------------
    public void setLongitudine(Double longitudine) {
        this.longitudine = longitudine;
    }
//--------------------------------------------------------------------------------------------------
    public Double getLatitudine() {
        return latitudine;
    }
//--------------------------------------
    public void setLatitudine(Double latitudine) {
        this.latitudine = latitudine;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_Universita() {
        return id_Universita;
    }
//--------------------------------------
    public void setId_Universita(@NonNull Long id_Universita) {
        this.id_Universita = id_Universita;
        if((universita!=null) && (id_Universita!=null)  && (id_Universita!=universita.getId())){
            universita=null;
        }else if(id_Universita==null){
            universita=null;//TODO: Oppure mantenrere lo stesso id  di tipo
        }
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Universita getUniversita() {
        return universita;
    }
//--------------------------------------
    @Ignore
    public boolean setUniversita(Universita universita) {
        boolean ris = true;
        if((id_Universita!=null) && (id_Universita==universita.getId()) ){
            this.universita = universita;
        }else if((id_Universita==null) && (universita!=null)){
            this.id_Universita = universita.getId();
            this.universita = universita;
        }else{
            ris = false;
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                                      EQUALS E HASH_CODE
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mense)) return false;
        Mense mense = (Mense) o;
        return Objects.equals(getId(), mense.getId()) &&
                getNome().equals(mense.getNome()) &&
                getSigla().equals(mense.getSigla()) &&
                getId_Universita().equals(mense.getId_Universita());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNome(), getSigla(), getId_Universita());
    }
//==================================================================================================
//##################################################################################################
}
