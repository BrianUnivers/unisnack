package it.tn.unitn.disi.ppsmet.IncludeView_Setup;

import android.view.ViewGroup;
import android.widget.TextView;

import it.tn.unitn.disi.ppsmet.R;

public class DataWithLabelView {
/*################################################################################################*/
    private TextView txtLabel = null;
    private TextView txtValue = null;
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    /**
     * Questo metodo permette di costruire e definire il layout impostato
     */
    private DataWithLabelView(TextView txtLabel,TextView txtValue) {
        this.txtLabel = txtLabel;
        this.txtValue = txtValue;
    }
/*------------------------------------------------------------------------*/
    /**
     * Questo è il costuttore di quando l'oggetto viene impostato in una pagina o in una sua parte.
     * @param rootLayout
     * @param idLayoutIncluded
     * @return
     */
    public static DataWithLabelView newInstance(ViewGroup rootLayout, int idLayoutIncluded) {
        DataWithLabelView ris = null;
        if(rootLayout!=null){
            ViewGroup contenitore = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(contenitore!=null){
                TextView txtLabel = (TextView) contenitore.findViewById(R.id.txt_Label);
                TextView txtValue = (TextView) contenitore.findViewById(R.id.txt_Valore);
                if((txtLabel!=null) && (txtValue!=null) ){
                    ris = new DataWithLabelView(txtLabel,txtValue);
                }
            }
        }
        return ris;
    }
/*------------------------------------------------------------------------*/
    /**
     * Questo è il costuttore di quando l'oggetto viene impostato in una pagina o in una sua parte.
     * @param rootLayout
     * @param idLayoutIncluded
     * @return
     */
    public static DataWithLabelView newInstance(ViewGroup rootLayout, int idLayoutIncluded, int idTextLable) {
        DataWithLabelView ris = newInstance(rootLayout,idLayoutIncluded);
        if(ris!=null){
            ris.setLabel(idTextLable);
        }
        return ris;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    /**
     * Questo metodo permette di impostare e cambiare la lable del gurppo di layout, partendo da
     * identificatere della risosssa della stringa.
     * @param idTextTitolo
     */
    public void setLabel(int idTextTitolo){
        txtLabel.setText(idTextTitolo);
    }
/*--------------------------------------------*/
    /**
     * Questo metodo permette di impostare e cambiare la lable del gurppo di layout, partendo dalla
     * stringa.
     * @param textTitolo
     */
    public void setLabel(String textTitolo){
        txtLabel.setText(textTitolo);
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo permette di impostare e cambiare il valore del gurppo di layout, partendo da
     * identificatere della risosssa della stringa.
     * @param idTextValue
     */
    public void setValue(int idTextValue){
        txtValue.setText(idTextValue);
    }
/*--------------------------------------------*/
    /**
     * Questo metodo permette di impostare e cambiare il valore del gurppo di layout, partendo dalla
     * stringa.
     * @param textValue
     */
    public void setValue(String textValue){
        txtValue.setText(textValue);
    }
/*================================================================================================*/
/*################################################################################################*/
}
