package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import android.util.Log;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.O_relativa_F;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

@Dao
public abstract class Ordinazioni_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    public long insert(Ordinazioni ordinazioni, List<PietanzeDellaMense> listaPietanzeDellaMense ){
        List<OrdinazionePietanzeDellaMenseConVoto> listaPietanzeDellaMenseConVoto =new LinkedList<>();
        Iterator<PietanzeDellaMense> iterator = listaPietanzeDellaMense.iterator();
        while(iterator.hasNext()){
            listaPietanzeDellaMenseConVoto.add(new OrdinazionePietanzeDellaMenseConVoto(iterator.next(),null));
        }
        return this.insertConVoto(ordinazioni,listaPietanzeDellaMenseConVoto);
    }
/*------------------------------------------------------------------------*/
    @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    public long insertConVoto(Ordinazioni ordinazioni, List<OrdinazionePietanzeDellaMenseConVoto> listaPietanzeDellaMenseConVoto ){
        long id = -1;
        try {
            Log.i("CIAO"," sss     ggggg " +ordinazioni );
            ordinazioni.setId(null);
            id = this.insert(ordinazioni);
        }catch (Exception e){
            Log.i("CIAO"," sss     ERRORRE " +e.getMessage() );
        }
        Log.i("CIAO"," sssss     Inserisco tutto  "+ id );
        Iterator<OrdinazionePietanzeDellaMenseConVoto> iterator = listaPietanzeDellaMenseConVoto.iterator();
        while(iterator.hasNext()){
            OrdinazionePietanzeDellaMenseConVoto elemet =  iterator.next();
            O_relativa_F relazione =
                    new O_relativa_F( id,
                            elemet.getMense().getId(),
                            elemet.getPietanze().getId(),
                            elemet.getMese(),
                            elemet.getAnno(),
                            elemet.getGiornoSettimana(),
                            elemet.getTipoMenu().getId(),
                            elemet.getVotoPietanza() );
            this.insert(relazione);
        }
        return id;
    }
/*------------------------------------------------------------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.ABORT)
    protected abstract long insert(Ordinazioni ordinazioni);
/*------------------------------------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.ABORT)
    protected abstract long insert(O_relativa_F corsi);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Transaction
    public void deleteAll(){
        this.deleteAllRelativa();
        this.deleteAllOrdinazioni();
    }
/*------------------------------------------------------------------------------------------------*/
    @Query("DELETE FROM tableOrdinazioni")
    protected abstract void deleteAllOrdinazioni();
/*------------------------------------------------------------------------*/
    @Query("DELETE FROM tableO_relativa_F")
    protected abstract void deleteAllRelativa();
    /*------------------------------------------------------------------------*/
    @Query( "DELETE FROM tableO_relativa_F " +
            "WHERE id_ordinazione == :idOrdinazioni")
    protected abstract void deleteRelativaByIdOrdinazioni(Long idOrdinazioni);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE by ID                                                    */
/*================================================================================================*/
    @Query( "DELETE FROM tableOrdinazioni " +
            "WHERE id == :idOrdinazioni ")
    public abstract void delete(Long idOrdinazioni);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE by ID UTENTE ID TIPO PASTO AT SPECIFIC DAY               */
/*================================================================================================*/
    @Query( "DELETE FROM tableOrdinazioni " +
            "WHERE id_utente == :idUtente AND id_tipoPasto == :idTipoPasto AND giorno == :date ")
    public abstract void deleteByIdUtente_IdTipoPasto_AtSpecificDay(Long idUtente,Long idTipoPasto,Date date);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = Ordinazioni.class,
            onConflict = OnConflictStrategy.ABORT)
    public abstract void update(Ordinazioni ordinazioni);
/*------------------------------------------------------------------------*/
    @Transaction
    @Update(onConflict = OnConflictStrategy.ABORT)
    public void update(Ordinazioni ordinazioni, List<OrdinazionePietanzeDellaMenseConVoto> listaPietanzeDellaMenseConVoto ){
        this.update(ordinazioni);
        Iterator<OrdinazionePietanzeDellaMenseConVoto> iterator = listaPietanzeDellaMenseConVoto.iterator();
        while(iterator.hasNext()){
            OrdinazionePietanzeDellaMenseConVoto elemet = iterator.next();
            O_relativa_F relazione =
                    new O_relativa_F(
                            ordinazioni.getId(),
                            elemet.getMense().getId(),
                            elemet.getPietanze().getId(),
                            elemet.getMese(),
                            elemet.getAnno(),
                            elemet.getGiornoSettimana(),
                            elemet.getTipoMenu().getId(),
                            elemet.getVotoPietanza() );
            this.update(relazione);
        }
    }
/*------------------------------------------------------------------------------------------------*/
    @Update(entity = O_relativa_F.class,
            onConflict = OnConflictStrategy.ABORT)
    protected abstract void update(O_relativa_F relativa);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO OVERWRITE ALL Ordinazione                                       */
/*                                          AND OrdinazionePietanzeDellaMenseConVoto              */
/*================================================================================================*/
    @Transaction
    @Update(onConflict = OnConflictStrategy.ABORT)
    public void overwriteAll(Ordinazioni ordinazioni, List<OrdinazionePietanzeDellaMenseConVoto> listaPietanzeDellaMenseConVoto ){
        this.deleteByIdUtente_IdTipoPasto_AtSpecificDay(ordinazioni.getId_utente(),ordinazioni.getId_tipoPasto(),ordinazioni.getGiorno());
        Long id = this.insert(ordinazioni);
        //Pietanze
        Iterator<OrdinazionePietanzeDellaMenseConVoto> iterator = listaPietanzeDellaMenseConVoto.iterator();
        while(iterator.hasNext()){
            OrdinazionePietanzeDellaMenseConVoto elemet = iterator.next();
            O_relativa_F relazione =
                    new O_relativa_F(
                            id,
                            elemet.getMense().getId(),
                            elemet.getPietanze().getId(),
                            elemet.getMese(),
                            elemet.getAnno(),
                            elemet.getGiornoSettimana(),
                            elemet.getTipoMenu().getId(),
                            elemet.getVotoPietanza() );
            this.insert(relazione);
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO OVERWRITE OrdinazionePietanzeDellaMenseConVoto                  */
/*================================================================================================*/
    @Transaction
    @Update(onConflict = OnConflictStrategy.ABORT)
    public void overwriteListaOrdinazionePietanzeDellaMenseConVoto(Long idOrdinazioni, List<OrdinazionePietanzeDellaMenseConVoto> listaPietanzeDellaMenseConVoto ){
        this.deleteRelativaByIdOrdinazioni(idOrdinazioni);//Cancella tutte le pietanze precedenti

        Iterator<OrdinazionePietanzeDellaMenseConVoto> iterator = listaPietanzeDellaMenseConVoto.iterator();
        while(iterator.hasNext()){
            OrdinazionePietanzeDellaMenseConVoto elemet = iterator.next();
            O_relativa_F relazione =
                    new O_relativa_F(
                            idOrdinazioni,
                            elemet.getMense().getId(),
                            elemet.getPietanze().getId(),
                            elemet.getMese(),
                            elemet.getAnno(),
                            elemet.getGiornoSettimana(),
                            elemet.getTipoMenu().getId(),
                            elemet.getVotoPietanza() );
            this.insert(relazione);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    @Query("SELECT O.* FROM tableOrdinazioni AS O")
    public abstract List<Ordinazioni> getAll();
/*------------------------------------------------------------------------*/
    @Query("SELECT O.* FROM tableOrdinazioni AS O")
    public abstract LiveData<List<Ordinazioni>> getAll_Synchronized();
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    @Query("SELECT O.* FROM tableOrdinazioni AS O WHERE id == :id")
    public abstract Ordinazioni getByPrimaryKey(Long id);
/*------------------------------------------------------------------------*/
    @Query("SELECT O.* FROM tableOrdinazioni AS O WHERE id == :id")
    public abstract LiveData<Ordinazioni> getByPrimaryKey_Synchronized(Long id);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
    @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE O.id_utente == :idUntente")
    public abstract List<Ordinazioni> getByIdOfUtenti(Long idUntente);
/*------------------------------------------------------------------------*/
@Query( "SELECT O.* " +
        "FROM tableOrdinazioni AS O " +
        "WHERE O.id_utente == :idUntente")
    public abstract LiveData<List<Ordinazioni>> getByIdOfUtenti_Synchronized(Long idUntente);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE IN THE FUTURE                                  */
/*================================================================================================*/
    @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE O.id_utente == :idUntente AND O.giorno >= :date " +
            "ORDER BY O.giorno ASC, O.id_tipoPasto ASC")
    public abstract List<Ordinazioni> getByIdOfUtenti_InTheFutureOfSpecificDate(Long idUntente,Date date);
/*------------------------------------------------------------------------*/
    @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE O.id_utente == :idUntente AND O.giorno >= :date " +
            "ORDER BY O.giorno ASC, O.id_tipoPasto ASC ")
    public abstract LiveData<List<Ordinazioni>> getByIdOfUtenti_InTheFutureOfSpecificDate_Synchronized(Long idUntente,Date date);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET ORDINAZIONE PIETANZE DELLA MENSE CON VOTO                   */
/*                         by ID UTENTE IN THE FUTURE                                             */
/*================================================================================================*/
    @Query( "SELECT  F.*, R.votoPietanza , R.id_ordinazione , " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine," +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F, tableO_relativa_F AS R, tableOrdinazioni as O " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
                    "R.id_f_anno == F.anno AND R.id_f_mese == F.mese AND R.id_f_giornoSettimana == F.giornoSettimana AND " +
                    "R.id_f_mensa == F.id_mensa AND R.id_f_pietanza == F.id_pietanza AND R.id_f_tipoMenu == F.id_tipoMenu AND R.id_ordinazione == O.id AND " +
                    "O.id_utente == :idUntente AND O.giorno >= :date "+
            "ORDER BY O.giorno ASC, O.id_tipoPasto ASC ")
    public abstract List<OrdinazionePietanzeDellaMenseConVoto> getAllOrdinazionePietanzeDellaMenseConVotoByIdUtente_InTheFuture(Long idUntente,Date date);
/*------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, R.votoPietanza ,  R.id_ordinazione , " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine," +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F, tableO_relativa_F AS R, tableOrdinazioni as O " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
                    "R.id_f_anno == F.anno AND R.id_f_mese == F.mese AND R.id_f_giornoSettimana == F.giornoSettimana AND " +
                    "R.id_f_mensa == F.id_mensa AND R.id_f_pietanza == F.id_pietanza AND R.id_f_tipoMenu == F.id_tipoMenu AND R.id_ordinazione == O.id AND " +
                    "O.id_utente == :idUntente AND O.giorno >= :date "+
            "ORDER BY O.giorno ASC, O.id_tipoPasto ASC ")
    public abstract LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> getAllOrdinazionePietanzeDellaMenseConVotoByIdUtente_InTheFuture_Synchronized(Long idUntente,Date date);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE LAST EAT IN THIS DATA                          */
/*================================================================================================*/
    @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE   O.id_utente == :idUntente AND " +
                    "O.giorno <= :date AND " +
                    "O.giorno =  (" +
                            "SELECT MAX(O1.giorno) " +
                            "FROM tableOrdinazioni AS O1 " +
                            "WHERE   O1.id_utente == :idUntente AND  O1.giorno <= :date )")
    public abstract List<Ordinazioni> getLastEatBeforeDataByIdOfUtenti(Long idUntente, Date date);
/*------------------------------------------------------------------------*/
    @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE   O.id_utente == :idUntente AND " +
                    "O.giorno <= :date AND " +
                    "O.giorno =  (" +
                        "SELECT MAX(O1.giorno) " +
                        "FROM tableOrdinazioni AS O1 " +
                        "WHERE   O1.id_utente == :idUntente AND  O1.giorno <= :date )")
    public abstract LiveData<List<Ordinazioni>> getLastEatBeforeDataByIdOfUtenti_Synchronized(Long idUntente, Date date);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE LAST EAT IN THIS DATA                          */
/*================================================================================================*/
    @Transaction
    public List<Ordinazioni> getLastEatBeforeTodayByIdOfUtenti(Long idUntente){
        return this.getLastEatBeforeDataByIdOfUtenti(idUntente,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Ordinazioni>> getLastEatBeforeTodayByIdOfUtenti_Synchronized(Long idUntente){
        return this.getLastEatBeforeDataByIdOfUtenti_Synchronized(idUntente,new Date());
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND TIPO PASTO                                 */
/*================================================================================================*/
    @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE O.id_utente == :idUntente AND O.id_tipoPasto == :idTipoPasto")
    public abstract List<Ordinazioni> getByIdOfUtenti_TipoPasto(Long idUntente, Long idTipoPasto);
/*------------------------------------------------------------------------*/
    @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE O.id_utente == :idUntente AND O.id_tipoPasto == :idTipoPasto")
    public abstract LiveData<List<Ordinazioni>> getByIdOfUtenti_TipoPasto_Synchronized(Long idUntente, Long idTipoPasto);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND ID TIPO PASTO  LAST EAT IN THIS DATA       */
/*================================================================================================*/
    @Transaction
    public Ordinazioni getLastEatBeforeTodayByIdOfUtenti_IdTipoPasto(Long idUntente, Long idtipoPasti){
        List<Long> listaIdtipoPasti = new LinkedList<>();
        listaIdtipoPasti.add(idtipoPasti);
        return this.getLastEatBeforeDataByIdOfUtenti_ListIdTipoPasto(idUntente,listaIdtipoPasti,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Ordinazioni> getLastEatBeforeTodayByIdOfUtenti_IdTipoPasto_Synchronized(Long idUntente, Long idtipoPasti){
        List<Long> listaIdtipoPasti = new LinkedList<>();
        listaIdtipoPasti.add(idtipoPasti);
        return this.getLastEatBeforeDataByIdOfUtenti_ListIdTipoPasto_Synchronized(idUntente,listaIdtipoPasti,new Date());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND LIST ID TIPO PASTO  LAST EAT IN THIS DATA  */
/*================================================================================================*/
    @Transaction
    public Ordinazioni getLastEatBeforeTodayByIdOfUtenti_ListIdTipoPasto(Long idUntente, List<Long> idtipoPasti){
        return this.getLastEatBeforeDataByIdOfUtenti_ListIdTipoPasto(idUntente,idtipoPasti,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Ordinazioni> getLastEatBeforeTodayByIdOfUtenti_ListIdTipoPasto_Synchronized(Long idUntente, List<Long> idtipoPasti){
        return this.getLastEatBeforeDataByIdOfUtenti_ListIdTipoPasto_Synchronized(idUntente,idtipoPasti,new Date());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND LIST ID TIPO PASTO  LAST EAT IN THIS DATA  */
/*================================================================================================*/
    @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE   O.id_utente == :idUntente AND " +
                    " ( (O.giorno < :date) OR (O.giorno == :date AND O.id_tipoPasto IN( :idtipoPasti) ) ) " +
                    " AND " +
                    " O.giorno =  ( " +
                            "SELECT MAX(O1.giorno) " +
                            "FROM tableOrdinazioni AS O1 " +
                            "WHERE   O1.id_utente == :idUntente  AND " +
                                    "( (O1.giorno < :date) OR (O1.giorno == :date AND O1.id_tipoPasto IN( :idtipoPasti) ) ) )" +
            "ORDER BY O.giorno DESC, O.id_tipoPasto DESC " +
            "LIMIT 1 ")
    public abstract Ordinazioni getLastEatBeforeDataByIdOfUtenti_ListIdTipoPasto(Long idUntente, List<Long> idtipoPasti, Date date);
/*------------------------------------------------------------------------*/
    @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE   O.id_utente == :idUntente AND " +
                    " ( (O.giorno < :date) OR (O.giorno == :date AND O.id_tipoPasto IN( :idtipoPasti) ) ) " +
                    " AND " +
                    " O.giorno =  ( " +
                            "SELECT MAX(O1.giorno) " +
                            "FROM tableOrdinazioni AS O1 " +
                            "WHERE   O1.id_utente == :idUntente  AND " +
                                    "( (O1.giorno < :date) OR (O1.giorno == :date AND O1.id_tipoPasto IN( :idtipoPasti) ) ) )" +
            "ORDER BY O.giorno DESC, O.id_tipoPasto DESC " +
            "LIMIT 1 ")
    public abstract LiveData<Ordinazioni> getLastEatBeforeDataByIdOfUtenti_ListIdTipoPasto_Synchronized(Long idUntente, List<Long> idtipoPasti, Date date);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND TIPO PASTO  LAST EAT IN THIS DATA          */
/*================================================================================================*/
    @Query( "SELECT  F.*, R.votoPietanza , R.id_ordinazione , " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine," +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F, tableO_relativa_F AS R " +
            "WHERE P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
                  "R.id_f_anno == F.anno AND R.id_f_mese == F.mese AND R.id_f_giornoSettimana == F.giornoSettimana AND R.id_f_mensa == F.id_mensa AND R.id_f_pietanza == F.id_pietanza AND R.id_f_tipoMenu == F.id_tipoMenu AND " +
                  "R.id_ordinazione == :idOrdinazione ")
    public abstract List<OrdinazionePietanzeDellaMenseConVoto> getPietanzeOrdinateByIdOfOrdinazioni(Long idOrdinazione);
/*------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, R.votoPietanza , R.id_ordinazione ,  " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine," +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F, tableO_relativa_F AS R " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
                    "R.id_f_anno == F.anno AND R.id_f_mese == F.mese AND R.id_f_giornoSettimana == F.giornoSettimana AND R.id_f_mensa == F.id_mensa AND R.id_f_pietanza == F.id_pietanza AND R.id_f_tipoMenu == F.id_tipoMenu AND " +
                    "R.id_ordinazione == :idOrdinazione ")
    public abstract LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> getPietanzeOrdinateByIdOfOrdinazioni_Synchronized(Long idOrdinazione);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND WITH DATE IN THIS LIST                     */
/*================================================================================================*/
    @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE   O.id_utente == :idUntente AND " +
                    "O.giorno IN (:listaData) ")
    public abstract List<Ordinazioni> getAllOrdinazioniByIdUtente_InListDate(Long idUntente, List<Date> listaData);
/*------------------------------------------------------------------------*/
    @Query( "SELECT O.* " +
            "FROM tableOrdinazioni AS O " +
            "WHERE   O.id_utente == :idUntente AND " +
                    "O.giorno IN (:listaData) ")
    public abstract LiveData<List<Ordinazioni>> getAllOrdinazioniByIdUtente_InListDate_Synchronized(Long idUntente, List<Date> listaData);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET PIETANZE ORDINATE by ID UTENTE AND TIPO PASTO               */
/*                         AT SPECIFIC DATA                                                       */
/*================================================================================================*/
    @Query( "SELECT  F.*, R.votoPietanza , R.id_ordinazione ,  " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine," +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F, tableO_relativa_F AS R, tableOrdinazioni AS O " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
                    "R.id_f_anno == F.anno AND R.id_f_mese == F.mese AND R.id_f_giornoSettimana == F.giornoSettimana AND R.id_f_mensa == F.id_mensa AND R.id_f_pietanza == F.id_pietanza AND R.id_f_tipoMenu == F.id_tipoMenu AND R.id_ordinazione == O.id AND " +
                    "O.id_utente == :idUntente AND O.giorno == :date AND O.id_tipoPasto == :idTipoPasto")
    public abstract LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> getPietanzeOrdinateByIdUtente_IdTipoPasto_AtSpecificDate(Long idUntente, Long idTipoPasto, Date date);
/*------------------------------------------------------------------------*/
    @Query( "SELECT  F.*, R.votoPietanza ,  R.id_ordinazione , " +
                    "P.id AS P_id, P.nome AS P_nome, P.calorie AS P_calorie , P.carboidrati AS P_carboidrati, " +
                    "P.grassi AS P_grassi, P.proteine AS P_proteine, P.immagine AS P_immagine, P.urlImmagine AS P_urlImmagine, " +
                    "P.id_tipoPietanza AS P_id_tipoPietanza, " +
                    "TM.id AS TM_id, TM.nome AS TM_nome, " +
                    "M.id AS M_id, M.nome AS M_nome, M.sigla AS M_sigla, M.longitudine AS M_longitudine, M.latitudine AS M_latitudine," +
                    "M.id_universita AS M_id_universita " +
            "FROM tablePietanze AS P,tableTipoMenu AS TM, tableMense AS M,tableM_fornisce_P AS F, tableO_relativa_F AS R, tableOrdinazioni AS O " +
            "WHERE   P.id == F.id_pietanza AND TM.id == F.id_tipoMenu AND M.id == F.id_mensa AND " +
                    "R.id_f_anno == F.anno AND R.id_f_mese == F.mese AND R.id_f_giornoSettimana == F.giornoSettimana AND R.id_f_mensa == F.id_mensa AND R.id_f_pietanza == F.id_pietanza AND R.id_f_tipoMenu == F.id_tipoMenu AND R.id_ordinazione == O.id AND " +
                    "O.id_utente == :idUntente AND O.giorno == :date AND O.id_tipoPasto == :idTipoPasto")
    public abstract LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> getPietanzeOrdinateByIdUtente_IdTipoPasto_AtSpecificDate_Synchronized(Long idUntente, Long idTipoPasto, Date date);
/*================================================================================================*/
/*################################################################################################*/
}
