package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_TipoMenu_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.TipoMenu_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;

public class TipoMenu_ThreadAggiornamento extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA ANNO ACCADEMICO LOCALE           */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "TipoMenu" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_TipoMenu";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista dei TipoMenu del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private List<TipoMenu> listaTipoMenu;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * TipoMenu.
         * @param listaTipoMenu Questo parametro contiene la lista di tutti gli elementi
         *                        tra qui quelli nuovi e quelli già presenti ma da modificare.
         */
        public ThreadAggiornamento(List<TipoMenu> listaTipoMenu) {
            super();
            this.listaTipoMenu =listaTipoMenu;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                                  */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella TipoMenu.");
            aggionaTabellaTipoMenu(listaTipoMenu);
            Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella TipoMenu.");
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param listaTipoMenu
         */
        private synchronized void aggionaTabellaTipoMenu(List<TipoMenu> listaTipoMenu){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella TipoMenu.");
            //Dati presenti nel database locale.
            List<TipoMenu> listaTipoMenuLocale = tipoMenuDao.getAll();
            if((listaTipoMenuLocale==null)||(listaTipoMenuLocale.isEmpty())){
                inserisciListaElementi(listaTipoMenu);
            }else if((listaTipoMenu==null)||(listaTipoMenu.isEmpty())) {
                eliminaListaElementi(listaTipoMenuLocale);
            }else{
                HashMap<Long,TipoMenu> hashMapTipoMenuLocali = convertiListaInHashMap(listaTipoMenuLocale);

                List<TipoMenu> listaTipoMenuNuove = new LinkedList<>();
                List<TipoMenu> listaTipoMenuAggiornate = new LinkedList<>();
                //Serve solo per poter conoscere quali elementi caccelare tra quelli già presenti localmente.
                List<TipoMenu> listaVechieTipoMenuDaMantenere = new LinkedList<>();

                Iterator<TipoMenu> iterator = listaTipoMenu.iterator();
                while (iterator.hasNext()){
                    TipoMenu tipoMenu =iterator.next();
                    if(tipoMenu!=null){
                        TipoMenu tipoMenuVecchia = hashMapTipoMenuLocali.get(tipoMenu.getId());
                        if(tipoMenuVecchia==null){
                            listaTipoMenuNuove.add(tipoMenu);
                        }else if(!tipoMenuVecchia.equals(tipoMenu)){
                            listaTipoMenuAggiornate.add(tipoMenu);
                            listaVechieTipoMenuDaMantenere.add(tipoMenuVecchia);
                        }else{
                            listaVechieTipoMenuDaMantenere.add(tipoMenuVecchia);
                        }
                    }
                }
                //MANTENGO SOLO GLI ELEMNETI CHE DEVO CANCELLARE
                listaTipoMenuLocale.removeAll(listaVechieTipoMenuDaMantenere);
                inserisciListaElementi(listaTipoMenuNuove);
                aggiornaListaElementi(listaTipoMenuAggiornate);
                eliminaListaElementi(listaTipoMenuLocale);
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         GENERAZIONE HASH MAP FROM LIST                                         */
/*================================================================================================*/
        private synchronized HashMap<Long,TipoMenu> convertiListaInHashMap(List<TipoMenu> lista){
            HashMap<Long,TipoMenu> ris=new HashMap<>();
            if(lista!=null){
                Iterator<TipoMenu> iterator = lista.iterator();
                while (iterator.hasNext()){
                    TipoMenu elemento = iterator.next();
                    if((elemento!=null) && (elemento.getId()!=null)){
                        ris.put(elemento.getId(),elemento);
                    }
                }
            }
            return ris;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO INSERIMENTO NUOVI ELEMENTI                                      */
/*================================================================================================*/
        private synchronized void inserisciListaElementi(List<TipoMenu> lista){
            if(lista!=null){
                Iterator<TipoMenu> iterator = lista.iterator();
                while (iterator.hasNext()){
                    TipoMenu elemento=iterator.next();
                    if(elemento!=null){
                        tipoMenuDao.insert(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO AGGIORNAMENTO ELEMENTI PRESENTI                                 */
/*================================================================================================*/
        private synchronized void aggiornaListaElementi(List<TipoMenu> lista){
            if(lista!=null){
                Iterator<TipoMenu> iterator = lista.iterator();
                while (iterator.hasNext()){
                    TipoMenu elemento=iterator.next();
                    if(elemento!=null){
                        tipoMenuDao.update(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaListaElementi(List<TipoMenu> lista){
            if(lista!=null){
                Iterator<TipoMenu> iterator = lista.iterator();
                while (iterator.hasNext()){
                    TipoMenu elemento=iterator.next();
                    if(elemento!=null){
                        tipoMenuDao.delete(elemento.getId());
                    }
                }
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_TipoMenu_Dao fbTipoMenuDao = new FB_TipoMenu_Dao();
    //QUERY FIREBASE
    private static LiveData<List<TipoMenu>> fbAllTipoMenu=null;
    //ROOM DATABASE
    protected static TipoMenu_Dao tipoMenuDao = null;
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaTipoMenu(){
        if(fbAllTipoMenu==null){
            fbAllTipoMenu= fbTipoMenuDao.getAll_Synchronized();
        }
        if(tipoMenuDao==null){
            tipoMenuDao = db.TipoMenuDao();
        }
        if(!fbAllTipoMenu.hasObservers()){//SE NON HA già osservatori per rilevare aggiornamenti
            fbAllTipoMenu.observeForever(new Observer<List<TipoMenu>>() {
                @Override
                public void onChanged(List<TipoMenu> lista) {
                    if((lista!=null)){
                        executor.execute(new ThreadAggiornamento(lista));
                    }
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public TipoMenu_ThreadAggiornamento(Application application) {
        super(application);
        gestioneAggiornametoTabellaTipoMenu();
    }
/*================================================================================================*/
/*################################################################################################*/
}
