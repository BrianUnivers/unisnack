package it.tn.unitn.disi.ppsmet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TableRow;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import it.tn.unitn.disi.ppsmet.ActivityManager.RegistrazioneActivity.RegistrazioneViewModel;
import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Authentication.HashPasswordWithSalt;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;
import it.tn.unitn.disi.ppsmet.GestioneMyIntent.MyIntent;
import it.tn.unitn.disi.ppsmet.GestioneRicordaSessione.GestioneSessione;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.AlertDialogApp.MessageAlertDialog;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.BarraConDuePulsanti;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.BarraConLogoTitolo;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListSpinnerWithAdd.AdapterWithSpinner.ListaSpinnerDiUniversitaRVAdapter;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListSpinnerWithAdd.ViewRecyclerListSpinnerWithAdd;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.SeparatorePaginaConTesto;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewField.ViewInputField_Data;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewField.ViewInputField_Email;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewField.ViewInputField_Integer;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewField.ViewInputField_Password;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewField.ViewInputField_Text;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.MySpinnerAdapter.SpinnerAdapter_Corsi;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.MySpinnerAdapter.SpinnerAdapter_Universita;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.ViewInputSpinner_Text;

public class RegistrazioneActivity extends AppCompatActivity {
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private RegistrazioneActivity  activity=this;
    private RegistrazioneViewModel viewModel;
/*------------------------------------------------------------------------*/
    private TableRow divisoreDatiUtentiView;
    private TableRow divisoreServizziUniversitariView;
    private TableRow divisoreTerminiCondizzioniView;
/*------------------------------------------------------------------------*/
    private ViewGroup pagina;
    private ViewGroup formView;
    private ViewGroup seletoriView;
/*------------------------------------------------------------------------*/
    private BarraConLogoTitolo tioloPagina;
/*------------------------------------------------------------------------*/
    private ViewInputField_Text     campoNome;
    private ViewInputField_Text     campoCognome;
    private ViewInputField_Data     campoDataNato;
    private ViewInputField_Integer  campoMatricola;
    private ViewInputField_Email    campoEmail;
    private ViewInputField_Password campoPassword;
    private ViewInputField_Password campoRipetiPassword;
/*------------------------------------------------------------------------*/
    private CheckBox policySicurezzaView;
    private CheckBox ricordamiView;
/*------------------------------------------------------------------------*/
    private BarraConDuePulsanti     barraConDuePulsanti;
/*------------------------------------------------------------------------*/
    private List<Universita> allUniversita = new ArrayList<>();
    private Universita       universitaSelezionata=null;
    private List<Corsi>      allCorsiUniversita = new ArrayList<>();
    private AnnoAccademico   annoAccademicoAttuale = null;
/*------------------------------------------------------------------------*/
    private ViewInputSpinner_Text<Universita> spinnerUniversita;
    private ViewInputSpinner_Text<Corsi>      spinnerCorsi;
    private ViewRecyclerListSpinnerWithAdd    listSpinnerWithAdd;
/*------------------------------------------------------------------------*/
    private SpinnerAdapter_Universita         adapter_universita;
    private SpinnerAdapter_Corsi              adapter_corsi;
    private ListaSpinnerDiUniversitaRVAdapter adapter_listaSpinnerDiUniversita;
/*------------------------------------------------------------------------*/
    private LiveData<List<Universita>> liveDataAllUniversita;
    private LiveData<AnnoAccademico>   liveDataAnnoAccademicoAttuale;
/*================================================================================================*/
/*                                  CREAZIONE DELL'ACTITVITY                                      */
/*================================================================================================*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrazione);
/*------------------------------------------------------------------------*/
        pagina       = findViewById(R.id.cly_PageRegistrazione);
        formView     = findViewById(R.id.lly_FormRegisrazione);
        seletoriView = findViewById(R.id.trw_PartecipaListaUniversita);
/*------------------------------------------------------------------------*/
        divisoreDatiUtentiView           = findViewById(R.id.trw_DivisoreDatiUtenti);
        divisoreServizziUniversitariView = findViewById(R.id.trw_DivisoreServizziUniversitari);
        divisoreTerminiCondizzioniView   = findViewById(R.id.trw_DivisoreTerminiCondizioni);
        policySicurezzaView = findViewById(R.id.ckb_PolicySicurezza);
        ricordamiView       = findViewById(R.id.ckb_Input_Ricordami);
        impostaLayoutStatico();
        impostaAzzioniPulsanti();
/*------------------------------------------------------------------------*/
        viewModel =  new ViewModelProvider(this).get(RegistrazioneViewModel.class);
        liveDataAllUniversita         = viewModel.getAllUniversita();
        liveDataAnnoAccademicoAttuale = viewModel.getActiveAtToday();
/*------------------------------------------------------------------------*/
        liveDataAnnoAccademicoAttuale.observe(activity, new Observer<AnnoAccademico>() {
            @Override
            public void onChanged(AnnoAccademico annoAccademico) {
                annoAccademicoAttuale = annoAccademico;
            }
        });
/*------------------------------------------------------------------------*/
        adapter_universita               = new SpinnerAdapter_Universita(activity,allUniversita);
        adapter_corsi                    = new SpinnerAdapter_Corsi(activity,allCorsiUniversita);
        adapter_listaSpinnerDiUniversita = new ListaSpinnerDiUniversitaRVAdapter(activity,allUniversita);
/*------------------------------------------------------------------------*/
        impostaLayoutSpinner();
        impostaLayoutDellaListaDeiSelettoriUniversitari();
/*------------------------------------------------------------------------*/
        mantieniAggiornatiDatiDeiAdapter();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA LAYOUT STATICO                                        */
/*================================================================================================*/
    /**
     * Questo metodo pertette di impostrate le varie parti statiche di layout.
     */
    private void impostaLayoutStatico(){
        tioloPagina = new BarraConLogoTitolo(pagina,
                R.id.include_TitleBar,
                R.string.ttl_Registrazione);
        campoNome = new ViewInputField_Text(    formView,
                R.id.include_Input_Nome,
                R.string.lbl_Nome,
                R.string.try_Nome,true);
        campoCognome = new ViewInputField_Text( formView,
                R.id.include_Input_Cognome,
                R.string.lbl_Cognome,
                R.string.try_Cognome,true);
        campoDataNato = new ViewInputField_Data(formView,
                R.id.include_Input_Nato,
                R.string.lbl_Nato,
                R.string.try_Data,true);
        campoMatricola = new ViewInputField_Integer(formView,
                R.id.include_Input_Matricola,
                R.string.lbl_Matricola,
                R.string.try_Matricola,true);
        campoEmail = new ViewInputField_Email(  formView,
                R.id.include_Input_Email,
                R.string.lbl_Email,
                R.string.try_Email,true);
        campoPassword = new ViewInputField_Password(formView,
                R.id.include_Input_Password,
                R.string.lbl_Password,
                R.string.try_Password,true);
        campoRipetiPassword = new ViewInputField_Password(formView,
                R.id.include_Input_RipetiPassword,
                R.string.lbl_RipetiPassword,
                R.string.try_Password,true);
/*------------------------------------------------------------------------*/
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude( divisoreDatiUtentiView,
                R.id.include_DatiUtenti,
                R.string.lbl_dvd_DatiUtente);
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude( divisoreServizziUniversitariView,
                R.id.include_ServizziMensa,
                R.string.lbl_dvd_ServizziUniversitari);
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude( divisoreTerminiCondizzioniView,
                R.id.include_TerminiCondizioni,
                R.string.lbl_dvd_TerminiCondizzioni);
/*------------------------------------------------------------------------*/
        barraConDuePulsanti = new BarraConDuePulsanti(  pagina,
                R.id.include_ButtonBar,
                R.string.lbl_ButtonConferma,
                false,
                R.string.lbl_ButtonAnnula);
/*------------------------------------------------------------------------*/
        policySicurezzaView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                barraConDuePulsanti.setOnPrimaryButtonEnabled( b);
                if(policySicurezzaView.isChecked()){
                    MessageAlertDialog alertDialog= new MessageAlertDialog(
                                    activity,
                                    R.layout.alert_dialog_message,
                                    R.string.ttl_TerminiCondizioni,
                                    R.string.txt_TerminiCondizioni);
                    alertDialog.create();
                    alertDialog.show();
                }
            }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA LAYOUT SPINNER                                        */
/*================================================================================================*/
    /**
     * Questo metodo imposta gli spinner assegnadolgi anche gli adapre.
     */
    private void impostaLayoutSpinner(){
        spinnerCorsi = new ViewInputSpinner_Text(   pagina,
                                                    R.id.include_Spinner_Corso,
                                                    R.string.lbl_CorsoLaurea,
                                                    adapter_corsi,
                                                    true,Corsi.class);
        spinnerUniversita = new ViewInputSpinner_Text(  pagina,
                                                        R.id.include_Spinner_Universita,
                                                        R.string.lbl_UniversitaConIscrizioneAttiva,
                                                        adapter_universita,
                                                        true,Universita.class);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA LAYOUT LISTA SELETTORI                                */
/*================================================================================================*/
    /**
     * Questo metodo imposta il layout della lista di spinner che si può allungare aggiungendo nuovi
     * elementi. Ed assegna un adapre da usare per tutti gli spinner.
     */
    private void impostaLayoutDellaListaDeiSelettoriUniversitari(){
        listSpinnerWithAdd = new ViewRecyclerListSpinnerWithAdd( activity,
                                                        seletoriView,
                                                        R.id.include_rcl_add_list_spinner_Universita,
                                                        R.string.lbl_ButtonAggiungiUniversita,
                                                        adapter_listaSpinnerDiUniversita);
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  AGGIORNAMENTI NOTIFICA AI ADAPTER I CAMBIAMENTI DI DATI       */
/*################################################################################################*/
/*================================================================================================*/
/*                                  AGGIORNA SPINNER SINGOLI E LISTA DI SELETORI                  */
/*================================================================================================*/
    /**
     * Questo metodo mantiene aggionato i vari dati tra cui quelli di adapter notifiandoli. Così da
     * matenerli semre validi e coerenti con il database.
     */
    private void mantieniAggiornatiDatiDeiAdapter(){
        liveDataAllUniversita.observe(activity, new Observer<List<Universita>>() {
            @Override
            public void onChanged(List<Universita> universitas) {
                allUniversita.clear();
                allUniversita.addAll(universitas);
                adapter_universita.notifyDataSetChanged();
                adapter_listaSpinnerDiUniversita.notifyDataSetChangedSpinner();
                listSpinnerWithAdd.impostaAvviso();
            }
        });
/*------------------------------------------------------------------------*/
        spinnerUniversita.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                universitaSelezionata = spinnerUniversita.getItemSelected();
                if(universitaSelezionata!=null){
                    spinnerCorsi.impostaAvviso();
                    viewModel.getAllCorsiByUniversita(universitaSelezionata).observe(activity ,
                            new Observer<List<Corsi>>() {
                                @Override
                                public void onChanged(List<Corsi> corsis) {
                                    allCorsiUniversita.clear();
                                    allCorsiUniversita.addAll(corsis);
                                    adapter_corsi.notifyDataSetChanged();
                                }
                            });
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView){}
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  IMPOSTA AZZIONE PULSANTI                                      */
/*################################################################################################*/
/*================================================================================================*/
/*                                  AGGIUNGI LISSENER PULSANTI                                    */
/*================================================================================================*/
    /**
     * Questo medodo definise i due lisserne dei due pulsanti infondo alla pagina, sia quello per
     * registrare l'utente che per annulare la fase di registrazione.
     */
    private void impostaAzzioniPulsanti(){
        barraConDuePulsanti.setOnSecondaryButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        apriActivityLoginUtente();
                    }
                });
/*----------------------------------*/
        barraConDuePulsanti.setOnPrimaryButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrazioneNuovoUtente();
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  REGISTRAZIONE UTENTE                                          */
/*################################################################################################*/
/*================================================================================================*/
/*                                  REGISTRAZIONE UTENTE                                          */
/*================================================================================================*/
    /**
     * Questo metodo estrai i valiri dai vari campi ed effetua i controlli di validita dei dati e se
     * è tutto valido effetua la procedeura di registrazione del utente.
     */
    private void registrazioneNuovoUtente(){
        boolean datiValidi=true;
/*----------------------------------*/
        String nome = this.campoNome.getTesto();
        if(!viewModel.verificaValiditaTesto(nome)){
            this.campoNome.impostaErroreCampoObbligatorio();
            datiValidi=false;
        }
/*----------------------------------*/
        String cogome = this.campoCognome.getTesto();
        if(!viewModel.verificaValiditaTesto(cogome)){
            this.campoCognome.impostaErroreCampoObbligatorio();
            datiValidi=false;
        }
/*----------------------------------*/
        Date dataDiNascita = this.campoDataNato.getData();
        if(!viewModel.verificaValiditaDataDiNascitaMaggiorenne(dataDiNascita)){
            this.campoDataNato.impostaErrore(R.string.err_Data);
            datiValidi=false;
        }
/*----------------------------------*/
        Integer matricola = this.campoMatricola.getNumero();
        if(!viewModel.verificaValiditaNumeroInteroDiversoDaZero(matricola)){
            this.campoMatricola.impostaErrore(R.string.err_Matricola);
            datiValidi=false;
        }
/*----------------------------------*/
        String email = this.campoEmail.getEmail();
        if(!viewModel.verificaValiditaEmail(email)){
            this.campoEmail.impostaErrore(R.string.err_Email);
            datiValidi=false;
        }
/*----------------------------------*/
        String password = this.campoPassword.getPassword();
        if(!viewModel.verificaValiditaTesto(password)){
            this.campoPassword.impostaErrore(R.string.err_Password);
            datiValidi=false;
        }
/*----------------------------------*/
        String ripetiPassword = this.campoRipetiPassword.getPassword();
        if(!viewModel.verificaValiditaTesto(ripetiPassword)){
            this.campoRipetiPassword.impostaErrore(R.string.err_Password);
            datiValidi=false;
        }else if(!ripetiPassword.equals(password)){
            this.campoRipetiPassword.impostaErrore(R.string.err_PasswordRipetuta);
            datiValidi=false;
        }
/*----------------------------------*/
        Universita universitaPrincipale = this.spinnerUniversita.getItemSelected();
        if(universitaPrincipale==null){
            datiValidi=false;
        }
/*----------------------------------*/
        Corsi      corsoPrincipale      = this.spinnerCorsi.getItemSelected();
        if(corsoPrincipale==null){
            datiValidi=false;
        }
/*----------------------------------*/
        Set<Universita> listaUnivertaPartecipa = new HashSet<>();
        listaUnivertaPartecipa.addAll( this.listSpinnerWithAdd.adapter.getElemetiSelezionati());
        listaUnivertaPartecipa.remove(universitaPrincipale);
/*----------------------------------*/
        if(datiValidi){
            String salt = HashPasswordWithSalt.genraSalt();
            String pswHash = HashPasswordWithSalt.genraPasswordHashWithSalt(password,salt);
            Utenti utenti = new Utenti( null, nome,cogome, dataDiNascita,email,pswHash, salt,
                                            universitaPrincipale.getId(),corsoPrincipale.getId(),
                                            matricola,annoAccademicoAttuale,true);
            effetuaRegistrazioneNuovoUtente(utenti,listaUnivertaPartecipa);
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  EFFETUA REGISEAZIONE                                          */
/*================================================================================================*/
    /**
     * Questo metodo riceve i dati di un possibie nuovo utente e la lista delle università di cui
     * non fa parte ma di cui può usufruire del servizio mensa, per poi inizare una seconda procedura
     * di verifiche di unicita del'email e della matricola per corso di univerità. Se supera con successo
     * tali controlli passa alla fase vera e propria di registrazione. Altrimenti segnalerà errore nel
     * campo in cui non è rispettata l'unicita.
     * @param utenti
     * @param listUniversita
     */
    private void effetuaRegistrazioneNuovoUtente(Utenti utenti,Set<Universita> listUniversita){
        LiveData<Boolean> esiteEmail     = viewModel.esiteEmail(utenti.getEmail());
        LiveData<Boolean> esiteMatricola = viewModel.esiteMatricola(utenti.getId_Universita(),utenti.getId_Corso(),utenti.getMatricola());
        esiteEmail.observe(activity, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean!=null){
                    if (aBoolean){
                        campoEmail.impostaErrore(R.string.err_EmailPresente);
                    }else{
                        campoEmail.annullaErrori();
/*------------------------------------------------------------------------------------------------*/
                        esiteMatricola.observe(activity, new Observer<Boolean>() {
                            @Override
                            public void onChanged(Boolean aBoolean) {
                                if (aBoolean!=null){
                                    if(aBoolean){
                                        campoMatricola.impostaErrore(R.string.err_MatricolaPresente);
                                    }else{
                                        campoMatricola.annullaErrori();
                                        registraUtente(utenti,listUniversita);
                                    }
                                }else{
                                    Toast.makeText(activity,R.string.msg_IscrizioneImpossibile,Toast.LENGTH_LONG).show();
                                }
                                esiteMatricola.removeObserver(this);
                            }
                        });
/*------------------------------------------------------------------------------------------------*/
                    }
                }else{
                    Toast.makeText(activity,R.string.msg_IscrizioneImpossibile,Toast.LENGTH_LONG).show();
                }
                esiteEmail.removeObserver(this);
            }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  REGISEAZIONE UTENTE E LA SUA PARTECIPAZIONE                   */
/*================================================================================================*/
    /**
     * Questo metodo permette di effetuare la regisrazione del utente e le sue partecipazioni ad
     * altre univerista (sottoforma di tessere).
     * N.B.
     * Questa funzione e nella funzione di inserimento dell'utente nel database di firebase, ci sono
     * delle righe di codice che simulano l'operazione che dovrebbero fare le segreterie delle varie
     * università cioè quella di completare l'iscrizione con la foto dello studente e con quella
     * delle tessere sia qrcod che codice a barre.
     * @param utenti
     * @param listUniversita
     */
    private void registraUtente(Utenti utenti,Set<Universita> listUniversita){
        List<Tessere> listaTessere = new LinkedList<>();
        Iterator<Universita> iterator = listUniversita.iterator();
        int idTessere= 1;
        while (iterator.hasNext()){
            idTessere++;
            Universita universita = iterator.next();
            Tessere tessere;
            if(idTessere!=3){//Per vedere la differenza
                tessere = new Tessere(universita,annoAccademicoAttuale,true);
            }else{
                tessere = new Tessere(universita,annoAccademicoAttuale,false);
            }
            /*-------------------------------*/
            /*        SOLO PER SIMILAZIONE   */
            /*-------------------------------*/
            tessere.setUrlCodiceABarre("Foto/Tessere/CodiceABarre/"+idTessere+".png");
            tessere.setUrlQRcode("Foto/Tessere/QrCode/"+idTessere+".png");
            /*-------------------------------*/
            /*-------------------------------*/
            listaTessere.add(tessere);
        }
        /*-------------------------------*/
        /*        SOLO PER SIMILAZIONE   */
        /*-------------------------------*/
        utenti.setUrlCodiceABarre("Foto/Tessere/CodiceABarre/1.png");
        utenti.setUrlQRCode(      "Foto/Tessere/QrCode/1.png");
        /*-------------------------------*/
        /*-------------------------------*/
        //N.B. Nel metodo in qui si fa la registazione vengono inserti i riferimenti all'immagioni
        // del profilo utente
        viewModel.iscrizioneUtente(utenti).observe(this, new Observer<Long>() {
            @Override
            public void onChanged(Long id) {
                if(id!=null) {
                    utenti.setId(id);
                    //Inserire le partecipazioni
                    if((listaTessere!=null) && (!listaTessere.isEmpty())){
                        viewModel.inserisciTessereDiPartecipazione(utenti,listaTessere);
                    }
                    gestioneSessioneUtente(utenti);
                }else{
                    Toast.makeText(activity,R.string.msg_VerificareLaConnnesione,Toast.LENGTH_LONG).show();
                }
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE SESSIONE PRIMA DI APRIRE IL PROFILO                  */
/*################################################################################################*/
    /**
     * Questo metodo viene chiamato una volta che l'utente è stato registrato in ogni sua parte. Qui
     * viene fatta una verifica della presenza locale del account ed viene eventualmente agiunto
     * se necessario una sessione che permettera al utente di rimanere attivo.
     * @param utenti
     */
    private void gestioneSessioneUtente(Utenti utenti){
        LiveData<Utenti> utenteRegistrato = viewModel.getUtenteByPrimaryKey(utenti.getId());
        utenteRegistrato.observe(activity, new Observer<Utenti>() {
            @Override
            public void onChanged(Utenti utenti) {
                boolean ricordami = ricordamiView.isChecked();
                if(utenti!=null){
                    Toast.makeText(activity,R.string.msg_RegistrazioneEffetuata,Toast.LENGTH_LONG).show();
                    String idSessione = GestioneSessione.creaSessione(getApplicationContext(),ricordami);
                    if(ricordami){
                        viewModel.aggiungiNuovaSessioneAttiva(utenti,idSessione);
                    }
                    apriActivityProfiloUtente(utenti);
                    utenteRegistrato.removeObserver(this);
                }
            }
        });
    }
/*################################################################################################*/
/*################################################################################################*/
/*                                  CAMBIA ACTIVITY                                               */
/*################################################################################################*/
/*================================================================================================*/
/*                                  APRI ACTYVITY LOGIN                                           */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'inserimento del identificativo del utente nel intent per cambiare pagina
     * verso l'activity di profilo utente.
     */
    private void apriActivityProfiloUtente(Utenti utenti){
        MyIntent i = new MyIntent(RegistrazioneActivity.this, ProfiloUtenteActivity.class);
        i.putExtraIdUtente(MyIntent.I_UTENTE_ATTIVO,utenti);
        startActivity(i);
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo contene il modo per tornare alla pagina di login.
     */
    private void apriActivityLoginUtente(){
        Intent i = new Intent(RegistrazioneActivity.this, LoginActivity.class);
        startActivity(i);
    }
/*================================================================================================*/
/*################################################################################################*/
}