package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerVotoPrietanze;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewValutazione.OnChangeValueListener.OnChangeValueListener;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewValutazione.TestoConValutazioneView;
import it.tn.unitn.disi.ppsmet.R;

public class VotoPietanzeRVAdapter extends RecyclerView.Adapter<VotoPietanzeRVAdapter.MyViewHoder> {
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE INTERNA                                                    */
/*################################################################################################*/
    /**
     * Questa classe è quella quella che viene usata per permettere alla RecyclingView Adapter di definire
     * il layout singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public ViewGroup v_mainLayout;
        //Contenitorei
        public TestoConValutazioneView v_Valutazione;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout = itemView.findViewById(R.id.cly_RowElemento);
            v_Valutazione= TestoConValutazioneView.newInstance(v_mainLayout,R.id.include_valutazione);
        }
/*================================================================================================*/
    }
/*################################################################################################*/


/*################################################################################################*/
/*                              CLASSE PRINCIPALE ModificaMenuSettimanaleGiorniRVAdapter          */
/*################################################################################################*/
    private Context comtext    = null;
    private List<OrdinazionePietanzeDellaMenseConVoto>   listaPietanze = null;
    private boolean modificabile= false;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public VotoPietanzeRVAdapter(Context comtext,
                                 List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze,
                                 boolean modificabile) {
        this.comtext       = comtext;
        this.modificabile  = modificabile;
        this.listaPietanze = listaPietanze;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              CERAZIONE DEL SINGOLO ELEMETOTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(comtext);
        View bloccoElemeto = inflater.inflate(R.layout.row_pietanza_con_valutazione, parent,false);
        MyViewHoder myViewHoder = new MyViewHoder(bloccoElemeto);
        return myViewHoder;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              INSERIRE L'ELEMETO NEL LAYOUT                                     */
/*================================================================================================*/
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore ModificaMenuSettimanaleGiorniRVAdapter.
     */
    @Override
    public void onBindViewHolder(@NonNull MyViewHoder holder, int position) {
        final OrdinazionePietanzeDellaMenseConVoto elemento = listaPietanze.get(position);
        //Imposta pietanaza
        String nomiPietanze = elemento.getPietanze().getNome();
        nomiPietanze =  nomiPietanze.substring(0, 1).toUpperCase() + nomiPietanze.substring(1);
        holder.v_Valutazione.setLabel(nomiPietanze);
        holder.v_Valutazione.setModificablie(this.modificabile);
        //Imposta voto
        Short voto = elemento.getVotoPietanza();
        voto = ((voto == null) ? 2 : voto);
        holder.v_Valutazione.setVoto(voto);
        holder.v_Valutazione.setOnChangeValueListener(new OnChangeValueListener() {
            @Override
            public void onChangeValueListener(View view, short value) {
                elemento.setVotoPietanza(value);
            }
        });
    }
/*================================================================================================*/
/*                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE                  */
/*================================================================================================*/
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaPietanze.size();
    }
/*================================================================================================*/
/*################################################################################################*/
}