package it.tn.unitn.disi.ppsmet.IncludeView_Setup;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import it.tn.unitn.disi.ppsmet.R;

public class BarraConDuePulsanti {
/*################################################################################################*/
    private ViewGroup barraContenitore = null;
    private Button btnPrincipale = null;
    private Button btnSecondario = null;
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    public BarraConDuePulsanti(ViewGroup rootLayout, int idLayoutIncluded,
                               int idTextButtonPrimary, int idTextButtonSecondary) {
        impostaLayoutComune(rootLayout,idLayoutIncluded,idTextButtonPrimary,true,
                                                        idTextButtonSecondary,true);
    }
/*------------------------------------------------------------------------*/
    public BarraConDuePulsanti(ViewGroup rootLayout, int idLayoutIncluded,
                               int idTextButtonPrimary,boolean statoButtonPrimary,
                               int idTextButtonSecondary) {
        impostaLayoutComune(rootLayout,idLayoutIncluded,idTextButtonPrimary,statoButtonPrimary,
                                                        idTextButtonSecondary,true);
    }
/*------------------------------------------------------------------------*/
    public BarraConDuePulsanti(ViewGroup rootLayout, int idLayoutIncluded,
                               int idTextButtonPrimary,boolean statoButtonPrimary,
                               int idTextButtonSecondary,boolean statoButtonSecondary) {
        impostaLayoutComune(rootLayout,idLayoutIncluded,idTextButtonPrimary,statoButtonPrimary,
                                                        idTextButtonSecondary,statoButtonSecondary);
    }
/*------------------------------------------------------------------------------------------------*/
    private void impostaLayoutComune(ViewGroup rootLayout, int idLayoutIncluded,
                                     int idTextButtonPrimary,boolean statoButtonPrimary,
                                     int idTextButtonSecondary,boolean statoButtonSecondary) {
        if(rootLayout!=null){
             barraContenitore = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(barraContenitore!=null){
                btnPrincipale = (Button) barraContenitore.findViewById(R.id.btn_Primario);
                btnSecondario = (Button) barraContenitore.findViewById(R.id.btn_Secondario);
                btnPrincipale.setText(idTextButtonPrimary);
                btnSecondario.setText(idTextButtonSecondary);
                btnPrincipale.setEnabled(statoButtonPrimary);
                btnSecondario.setEnabled(statoButtonSecondary);
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  ACTION BUTTON                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                                  SET ON CLICK LIESSERNER                                       */
/*================================================================================================*/
    public boolean setOnPrimaryButtonClickListener(View.OnClickListener listener){
        boolean ris = false;
        if(btnPrincipale!=null){
            ris = true;
            btnPrincipale.setOnClickListener(listener);
        }
        return ris;
    }
/*------------------------------------------------------------------------*/
    public boolean setOnSecondaryButtonClickListener(View.OnClickListener listener){
        boolean ris = false;
        if(btnSecondario!=null){
            ris = true;
            btnSecondario.setOnClickListener(listener);
        }
        return ris;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  SET ENABLED BUTTON                                            */
/*================================================================================================*/
    public boolean setOnPrimaryButtonEnabled(boolean stato){
        boolean ris = false;
        if(btnPrincipale!=null){
            ris = true;
            btnPrincipale.setEnabled(stato);
        }
        return ris;
    }
/*------------------------------------------------------------------------*/
    public boolean setOnSecondaryButtonEnabled(boolean stato){
        boolean ris = false;
        if(btnSecondario!=null){
            ris = true;
            btnSecondario.setEnabled(stato);
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  VISIBILITY BLOCK                                              */
/*################################################################################################*/
/*================================================================================================*/
/*                                  SET ENABLED BUTTON                                            */
/*================================================================================================*/
    public void setVisibility(int visibility){
        switch(visibility){
            case View.VISIBLE:
            case View.INVISIBLE:
            case View.GONE:{
                if(barraContenitore!=null){
                    barraContenitore.setVisibility(visibility);
                }
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
