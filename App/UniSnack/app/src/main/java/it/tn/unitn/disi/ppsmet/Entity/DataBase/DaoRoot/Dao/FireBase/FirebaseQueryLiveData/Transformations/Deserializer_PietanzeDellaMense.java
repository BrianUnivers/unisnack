package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List_In_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;
import it.tn.unitn.disi.ppsmet.Entity.Universita;


public class Deserializer_PietanzeDellaMense implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto PietanzeDellaMense.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String LOG_TAG = "PietanzeDellaMense_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "PietanzeDellaMense" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "PietanzeDellaMense".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public static List<PietanzeDellaMense> estraiDaDataSnapshot(DataSnapshot dataSnapshot, Mense mense, TipoMenu tipoMenu){
        List<PietanzeDellaMense> listaPietanzeDellaMense = new LinkedList<>();
        //Verifica campi not null
        if((dataSnapshot.exists()) && (mense!=null) && (tipoMenu!=null) ){
            try{


                Integer anno            = dataSnapshot.child(P_ANNO).getValue(Integer.class);
                Integer mese            = dataSnapshot.child(P_MESE).getValue(Integer.class);
                Integer giornoSettimana = dataSnapshot.child(P_GIORNO_SETTIMANA).getValue(Integer.class);
                //Verifica campi not null
                if( (anno!=null) && (mese!=null) && (giornoSettimana!=null) &&
                        ( (mese>=1)&&(mese<=12) ) && ( (giornoSettimana>=1)&&(giornoSettimana<=12) ) ){

                    DataSnapshot pietanzeDataSnapshot = dataSnapshot.child(L_PIETANZE);
                    if(pietanzeDataSnapshot.exists()){
                        Iterator<DataSnapshot> iterator=pietanzeDataSnapshot.getChildren().iterator();
                        while (iterator.hasNext()){
                            DataSnapshot datiPietanza= iterator.next();
                            Pietanze pietanze = Deserializer_Pietanze.estraiDaDataSnapshot(datiPietanza);
                            //Verifica campi not null
                            if(pietanze!=null){
                                listaPietanzeDellaMense.add(new PietanzeDellaMense( mese,anno,giornoSettimana,
                                                                                    tipoMenu,mense,pietanze) );
                            }
                        }
                    }
                }


            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
            }
        }
        return listaPietanzeDellaMense;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<PietanzeDellaMense>>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List_Menu extends GenericDeserializer_Object<List<PietanzeDellaMense>> {
        private Mense    mense;
        private TipoMenu tipoMenu;
/*================================================================================================*/
/*                         COSTRUTTORE                                                            */
/*================================================================================================*/
        public Deserializer_List_Menu( Mense mense,TipoMenu tipoMenu) {
            super();
            this.mense= mense;
            this.tipoMenu= tipoMenu;
        }
/*================================================================================================*/
        @Override
        public List<PietanzeDellaMense> estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_PietanzeDellaMense.estraiDaDataSnapshot(dataSnapshot,this.mense,this.tipoMenu);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<PietanzeDellaMense>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List_In_List<PietanzeDellaMense> {
        private Mense    mense;
        private TipoMenu tipoMenu;
/*================================================================================================*/
/*                         COSTRUTTORE                                                            */
/*================================================================================================*/
        public Deserializer_List( Mense mense,TipoMenu tipoMenu) {
            super();
            this.mense= mense;
            this.tipoMenu= tipoMenu;
        }
/*================================================================================================*/
        @Override
        public List<PietanzeDellaMense> estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_PietanzeDellaMense.estraiDaDataSnapshot(dataSnapshot,this.mense,this.tipoMenu);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
