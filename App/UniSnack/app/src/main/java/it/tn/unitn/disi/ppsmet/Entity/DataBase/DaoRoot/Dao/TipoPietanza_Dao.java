package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;

@Dao
public interface TipoPietanza_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long insert(TipoPietanza tipoPietanza);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Query("DELETE FROM tableTipoPietanza")
    public void deleteAll();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE By ID                                                    */
/*================================================================================================*/
    @Query("DELETE FROM tableTipoPietanza WHERE id == :id")
    public abstract void delete(Long id);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = TipoPietanza.class,
            onConflict = OnConflictStrategy.ABORT)
    public void update(TipoPietanza tipoPietanza);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    @Query("SELECT * FROM tableTipoPietanza")
    public List<TipoPietanza> getAll();
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableTipoPietanza")
    public LiveData<List<TipoPietanza>> getAll_Synchronized();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    @Query("SELECT * FROM tableTipoPietanza WHERE id == :id")
    public TipoPietanza getByPrimaryKey(Long id);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableTipoPietanza WHERE id == :id")
    public LiveData<TipoPietanza> getByPrimaryKey_Synchronized(Long id);
/*================================================================================================*/
/*################################################################################################*/
}
