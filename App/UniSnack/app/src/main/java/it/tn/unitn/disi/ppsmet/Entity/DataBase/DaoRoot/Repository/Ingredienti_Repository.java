package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Allergeni_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Ingredienti_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.IngredientiEAllergeni_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;

public class Ingredienti_Repository extends IngredientiEAllergeni_ThreadAggiornamento {
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA ANNO ACCADEMICO(e le sue derivazioni    */
/*                         seguendo un ordine topologico per garantire gli aggiornamenti)         */
/*                         ATTRAVERO IL DATABASE LOCALE (room)                                    */
/*################################################################################################*/
    private LiveData<List<Ingredienti>> mAllIngredienti;
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Ingredienti_Repository(Application application) {
        super(application);
        //AllIngredienti get any time the same result
        mAllIngredienti = ingredientiDao.getAll_Synchronized();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERT                                                                 */
/*================================================================================================*/
    /*public Future<Long> insert(Ingredienti ingredienti){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
            @Override
            public Long call() throws Exception {
                return ingredientiDao.insert(ingredienti);
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    /*public void deleteAll(){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            ingredientiDao.deleteAll();
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    /*public void update(Ingredienti ingredienti){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            ingredientiDao.update(ingredienti);
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
   /* public LiveData<List<Ingredienti>> getAll(){
        return mAllIngredienti;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
   /* public LiveData<Ingredienti> getByPrimaryKey(Long id){
        return ingredientiDao.getByPrimaryKey_Synchronized(id);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID PIETANZA                                              */
/*================================================================================================*/
   /* public LiveData<List<Ingredienti>> getByIdOfPietanza(Pietanze pietanze){
        return ingredientiDao.getByIdOfPietanza_Synchronized(pietanze.getId());
    }
/*================================================================================================*/
/*################################################################################################*/
}
