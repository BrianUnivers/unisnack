package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Pietanze_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_TipoPasto_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Pietanze_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.TipoPasto_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.TipoPietanza_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

public class Pietanze_ThreadAggiornamento extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA ANNO ACCADEMICO LOCALE           */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "Pietanze" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_Pietanze";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista dei Pietanze del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private List<Pietanze> listaPietanze;
        private Long idTipoPietanza;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * Pietanze.
         * @param listaPietanze Questo parametro contiene la lista di tutti gli elementi
         *                        tra qui quelli nuovi e quelli già presenti ma da modificare.
         */
        public ThreadAggiornamento(List<Pietanze> listaPietanze,Long idTipoPietanza) {
            super();
            this.listaPietanze =listaPietanze;
            this.idTipoPietanza = idTipoPietanza;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                                  */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella Pietanze.");
            if(idTipoPietanza!=null){
                aggionaTabellaPietanze(listaPietanze,idTipoPietanza);
            }
            Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella Pietanze.");
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param listaPietanze
         */
        private synchronized void aggionaTabellaPietanze(List<Pietanze> listaPietanze,Long idTipoPietanza){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella Pietanze.");
            //Dati presenti nel database locale.
            List<Pietanze> listaPietanzeLocale = pietanzeDao.getByIdOfTipoPietanza(idTipoPietanza);
            if((listaPietanzeLocale==null)||(listaPietanzeLocale.isEmpty())){
                inserisciListaElementi(listaPietanze);
            }else if((listaPietanze==null)||(listaPietanze.isEmpty())) {
                eliminaListaElementi(listaPietanzeLocale);
            }else{
                HashMap<Long,Pietanze> hashMapPietanzeLocali = convertiListaInHashMap(listaPietanzeLocale);

                List<Pietanze> listaPietanzeNuove = new LinkedList<>();
                List<Pietanze> listaPietanzeAggiornate = new LinkedList<>();
                //Serve solo per poter conoscere quali elementi caccelare tra quelli già presenti localmente.
                List<Pietanze> listaVechiePietanzeDaMantenere = new LinkedList<>();

                Iterator<Pietanze> iterator = listaPietanze.iterator();
                while (iterator.hasNext()){
                    Pietanze pietanze =iterator.next();
                    if(pietanze!=null){
                        Pietanze pietanzeVecchia = hashMapPietanzeLocali.get(pietanze.getId());
                        if(pietanzeVecchia==null){
                            listaPietanzeNuove.add(pietanze);
                        }else if(!pietanzeVecchia.equals(pietanze)){
                            listaPietanzeAggiornate.add(pietanze);
                            listaVechiePietanzeDaMantenere.add(pietanzeVecchia);
                        }else{
                            listaVechiePietanzeDaMantenere.add(pietanzeVecchia);
                        }
                    }
                }
                //MANTENGO SOLO GLI ELEMNETI CHE DEVO CANCELLARE
                listaPietanzeLocale.removeAll(listaVechiePietanzeDaMantenere);
                inserisciListaElementi(listaPietanzeNuove);
                aggiornaListaElementi(listaPietanzeAggiornate);
                eliminaListaElementi(listaPietanzeLocale);
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         GENERAZIONE HASH MAP FROM LIST                                         */
/*================================================================================================*/
        private synchronized HashMap<Long,Pietanze> convertiListaInHashMap(List<Pietanze> lista){
            HashMap<Long,Pietanze> ris=new HashMap<>();
            if(lista!=null){
                Iterator<Pietanze> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Pietanze elemento = iterator.next();
                    if((elemento!=null) && (elemento.getId()!=null)){
                        ris.put(elemento.getId(),elemento);
                    }
                }
            }
            return ris;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO INSERIMENTO NUOVI ELEMENTI                                      */
/*================================================================================================*/
        private synchronized void inserisciListaElementi(List<Pietanze> lista){
            if(lista!=null){
                Iterator<Pietanze> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Pietanze elemento=iterator.next();
                    if(elemento!=null){
                        pietanzeDao.insert(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO AGGIORNAMENTO ELEMENTI PRESENTI                                 */
/*================================================================================================*/
        private synchronized void aggiornaListaElementi(List<Pietanze> lista){
            if(lista!=null){
                Iterator<Pietanze> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Pietanze elemento=iterator.next();
                    if(elemento!=null){
                        pietanzeDao.update(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaListaElementi(List<Pietanze> lista){
            if(lista!=null){
                Iterator<Pietanze> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Pietanze elemento=iterator.next();
                    if(elemento!=null){
                        pietanzeDao.delete(elemento.getId());
                    }
                }
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_Pietanze_Dao fbPietanzeDao = new FB_Pietanze_Dao();
    //QUERY FIREBASE
    private static LiveData<List<Pietanze>> fbAllPietanzePerTipo=null;
    //ROOM DATABASE
    protected static Pietanze_Dao pietanzeDao = null;
    protected static TipoPietanza_Dao tipoPietanzaeDao = null;
    //QUERY ROOM DATABASE
    private static LiveData<List<TipoPietanza>> allTipoPietanze=null;
/*------------------------------------------------------------------------------------------------*/
    //DIPENDENZE
    protected static TipoPietanza_ThreadAggiornamento dipendezaTipoPietanza  = null;
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaPietanze(){
        if(pietanzeDao==null){
            pietanzeDao = db.PietanzeDao();
        }
        if(tipoPietanzaeDao==null){
            tipoPietanzaeDao = db.TipoPietanzaDao();
            allTipoPietanze = tipoPietanzaeDao.getAll_Synchronized();
        }
        if(!allTipoPietanze.hasObservers()){//SE NON HA già osservatori per rilevare aggiornamenti
            allTipoPietanze.observeForever(new Observer<List<TipoPietanza>>() {
                private  List<TipoPietanza>  lastListTipoPasto = new LinkedList<>();
                private  List<TipoPietanza>  temp = new LinkedList<>();
                @Override
                public void onChanged(List<TipoPietanza> listaTipoPietanze) {
                    temp.clear();
                    temp.addAll(listaTipoPietanze);
                    if(!lastListTipoPasto.isEmpty()){
                        listaTipoPietanze.removeAll(lastListTipoPasto);
                    }
                    //Solo quelli nuovi
                    Iterator<TipoPietanza> iterator = listaTipoPietanze.iterator();
                    while (iterator.hasNext()){
                        TipoPietanza tipoPietanza =iterator.next();
                        if((tipoPietanza!=null) && (tipoPietanza.getId()!=null)){
                            //Aggiungi un altro ossservatore
                            fbPietanzeDao.getByIdOfTipoPietanza_Synchronized(tipoPietanza.getId()).observeForever(
                                    new MyPietanzeObserver(tipoPietanza.getId()));
                        }
                    }
                    lastListTipoPasto.clear();
                    lastListTipoPasto.addAll(temp);
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
private static class MyPietanzeObserver implements Observer<List<Pietanze>>{
    private Long idTipoPietanza;
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public MyPietanzeObserver(Long idTipoPietanza) {
        this.idTipoPietanza =idTipoPietanza;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO CHIAMATO NEL MOMENTO DEL CAMBIAMNETO                            */
/*================================================================================================*/
    @Override
    public void onChanged(List<Pietanze> lista) {
        if((lista!=null)){
            executor.execute(new ThreadAggiornamento(lista,idTipoPietanza));
        }
    }
/*================================================================================================*/
}
/*################################################################################################*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Pietanze_ThreadAggiornamento(Application application) {
        super(application);
        if(dipendezaTipoPietanza==null){
            dipendezaTipoPietanza = new TipoPietanza_ThreadAggiornamento(application);
        }
        gestioneAggiornametoTabellaPietanze();
    }
/*================================================================================================*/
/*################################################################################################*/
}
