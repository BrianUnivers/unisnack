package it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.Fragment.Interface;

import java.util.Date;

public interface OnFragmentMenuDelleMenseNelGiornoEventListener {
/*================================================================================================*/
/*                                  GESTIONE MENU DEL GIORNO                                      */
/*================================================================================================*/
    /**
     * L'implemetazione di questo metodo sarà chiamanta nel momento in cui il fragment contenete
     * il menu di un sigolo giorno si dovrà minnimizzare(chiudere).
     */
    public void chiudiMenuDelGiorno();
/*------------------------------------------------------------------------------------------------*/
    /**
     * L'implemetazione di questo metodo sarà chiamanta per aprire il menu del gionrno che sarà passato.
     */
    public void apriMenuDelGiorno(Date date);
/*================================================================================================*/
}
