package it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewField;

import android.content.Intent;
import android.text.InputType;
import android.view.ViewGroup;

public class ViewInputField_Integer extends ViewInputField_Text {
/*################################################################################################*/
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP VIEW FIELD                                */
/*================================================================================================*/
    public ViewInputField_Integer(ViewGroup rootLayout, int idLayoutIncluded, int idLable, int idTestValue) {
        super(rootLayout,idLayoutIncluded,idLable,idTestValue,false);
        impostaLayoutComune();
    }
/*------------------------------------------------------------------------*/
    public ViewInputField_Integer(ViewGroup rootLayout, int idLayoutIncluded, int idLable, int idTestValue, boolean obbligatorio) {
        super(rootLayout,idLayoutIncluded,idLable,idTestValue,obbligatorio);
        impostaLayoutComune();
    }
/*------------------------------------------------------------------------------------------------*/
    private void impostaLayoutComune() {
        if(input!=null){
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  AZZIONI DURANTE L'INSERIMETO DEI DATI                         */
/*================================================================================================*/
    public Integer getNumero(){
        Integer ris = null;
        if((input!=null) && (input.getText()!=null) && (!input.getText().toString().equals("")) ){
            ris = Integer.parseInt(input.getText().toString());
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
}
