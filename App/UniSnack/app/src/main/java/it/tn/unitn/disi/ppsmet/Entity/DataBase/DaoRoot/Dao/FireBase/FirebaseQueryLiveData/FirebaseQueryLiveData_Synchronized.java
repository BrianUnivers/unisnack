package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData;

import android.util.Log;

import androidx.lifecycle.LiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class FirebaseQueryLiveData_Synchronized extends LiveData<DataSnapshot> {
/**
 * Questa classe ci permette di gestire e mantenere sempre aggiornati i risultati di una query al
 * firebase runtime database ed disativare tali aggirnamenti nel momento stesso che tali dati non sono
 * utilizzaati all'interno della attivtà.
 */
/*################################################################################################*/
    private static final String LOG_TAG = "FirebaseQueryLiveData_Synchronized";

    private final Query query;
    private final MyValueEventListener listener = new MyValueEventListener();
/*================================================================================================*/
/*                         CONSTRUCTOR OF QUERY AT FIREBASE RUNTIME DATABASE                      */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET LAST ID OF ORDINAZIONI                                             */
/*------------------------------------------------------------------------------------------------*/
    public FirebaseQueryLiveData_Synchronized(Query query) {
        this.query = query;
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET LAST ID OF ORDINAZIONI                                             */
/*------------------------------------------------------------------------------------------------*/
    public FirebaseQueryLiveData_Synchronized(DatabaseReference ref) {
        this.query = ref;
    }

/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         ACTIVATION AND INATIVATION UPDATE OF RESULTS OF QUERY                  */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         ACTIVATION UPDATE OF RESULTS OF QUERY (Activity is show on screen)     */
/*                         It is call when activity is onSTARTED or onRESUMED                     */
/*------------------------------------------------------------------------------------------------*/
    @Override
    protected void onActive() {
        Log.d(LOG_TAG, "onActive");
        query.addValueEventListener(listener);
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         INATIVATION UPDATE OF RESULTS OF QUERY (Activity is inactive or destroied)*/
/*                         It is call when activity is onDESTROY or onPAUSE                       */
/*------------------------------------------------------------------------------------------------*/
    @Override
    protected void onInactive() {
        Log.d(LOG_TAG, "onInactive");
        query.removeEventListener(listener);
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
    private class MyValueEventListener implements ValueEventListener {
/*================================================================================================*/
/*                         ACTION FOR UPDATE THE RESULTS OF QUERY                                 */
/*================================================================================================*/
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            setValue(dataSnapshot);//Insermento nella lista LiveData dei risultati
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         ERROR IT CAN'T CONNECTION TO FIREBASE RUNTIME DATABASE                 */
/*================================================================================================*/
        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e(LOG_TAG, "Can't listen to query " + query, databaseError.toException());
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}