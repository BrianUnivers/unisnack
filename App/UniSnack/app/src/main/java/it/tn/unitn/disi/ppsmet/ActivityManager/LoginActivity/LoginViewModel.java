package it.tn.unitn.disi.ppsmet.ActivityManager.LoginActivity;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;


import java.sql.Time;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Pietanze_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Utenti_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.IdentificativiSessioni_Repository;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class LoginViewModel extends AndroidViewModel {
    private Long  TIMEOUT_FB = 10000L;
    private Long  TIMEOUT_SECOND_FB = 100L;
    private Timer TIMER = new Timer();
/*################################################################################################*/
    private Utenti_Repository                 rUtenti;
    private IdentificativiSessioni_Repository rIdentificativiSessioni;
    //Al primo accesso aticipa il prima possibile la sincronizzaznione delle pietanze
    private Pietanze_Repository  rPietanze;
/*================================================================================================*/
/*                                  COSTRUTTORE VIEW MODEL                                        */
/*================================================================================================*/
    /**
     * Questo metodo costruttore del viewmode inizializza le repository che si interfacciano con entrambi
     * i database sia interno che estreno a seconda delle operazioni che vengono richeste. Che verranno
     * utilizate dall'activity e i suoi fragment.
     * @param application
     */
    public LoginViewModel(@NonNull Application application) {
        super(application);
        rUtenti                 = new Utenti_Repository(application);
        rIdentificativiSessioni = new IdentificativiSessioni_Repository(application);
        //Al primo accesso aticipa il prima possibile la sincronizzaznione delle pietanze
        rPietanze  = new Pietanze_Repository(application);
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  AZZIONI LEGATE ALLA GESITIONE DEI DATABASE                    */
/*################################################################################################*/
/*================================================================================================*/
/*                                  VERIFICA CREDENZIALI E AUTENTIFICA UTENTE                     */
/*================================================================================================*/
    /**
     * Questo metodo cerca un utente con credenziali "email" e "password" all'interno del database
     * (Esterno) e se tale utente non dovesse essere presente si ricece una risposta pari a null.
     * N.B.
     * 1) Il database esterno alla prima richiesta da parte di un nuovo dispositivo, riponderà sempre null
     * anche se c'è un risultato valido. Per questo motivo ci sono due TIMER che possono ripondere null
     * alla riciesta il primo ed il più vicino nel tempo alla richiesta riponderà null, per confermare
     * un eventuale assenza del utente,poichè il database invierà il primo null e non seguito da un altra
     * risposta negativa(dove eventualmente al primo accesso ci sarebbe la risposta vera e propria).
     * 2) Se il dispostitivo è offline, senza alcun tipo di accesso al database, non ci potranno essre
     * entrabe le risposte. Per questo motivo ci sono due timer che differiscono di poco tempo l'uno
     * dall'altro così da fornire due risposte null. Facendo in modo di segnalare un problema al utente.
     * @param email
     * @param password
     * @return
     */
    public LiveData<Utenti> autenticazioneUtentete(String email,String password){
        MediatorLiveData<Utenti> utenteRisposta = new MediatorLiveData();
        //RICHIESTA VERA E PROPRIA
        utenteRisposta.addSource(rUtenti.getByAuthentication(email,password) ,
                new Observer<Utenti>() {
                    @Override
                    public void onChanged(Utenti utenti) {
                        utenteRisposta.setValue(utenti);
                    }
                });
        //TIMEOUT DELLA RICHIESTA D'AUTENTICAZIONE può attendere una secoda risposta
        TIMER.schedule(new TimerTask() {
                @Override
                public void run() {
                    if(utenteRisposta.getValue()==null){
                        utenteRisposta.postValue(null);
                    }
                }
            }, TIMEOUT_FB);
        //TIMEOUT DELLA RICHIESTA D'AUTENTICAZIONE se si attiva chiude certamente l'attesa e fa dare
        //l'errore
        TIMER.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if(utenteRisposta.getValue()==null){
                            utenteRisposta.postValue(null);
                        }
                    }
                }, TIMEOUT_FB+TIMEOUT_SECOND_FB);
        return utenteRisposta;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  VERIFICA SESSIONE ATTIVA                                      */
/*================================================================================================*/
    /**
     * Questo metodo permette di restituire un utente in base all'identificativo di una sessione aperta.
     * Questo tipo di acceso è l'unico che può avvenire offline.
     * @param idSessione
     * @return
     */
    public Future<Utenti> restituisceSePossibileUtenteConSessioneAttva(String idSessione){
        return rIdentificativiSessioni.getUtenteByIdSessione(idSessione);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  ATTIVA NUOVA SESSIONE                                         */
/*================================================================================================*/
    /**
     * Questo metodo permette di aggiungere una sessione così da ricocdarne l'assocazione tra
     * all'identificativo di una sessione  e l'utente.
     * @param utenti
     * @param idSessione
     */
    public void aggiungiNuovaSessioneAttiva(Utenti utenti,String idSessione){
        rIdentificativiSessioni.insertSessioneByUtenteAndIdSessione(utenti,idSessione);
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  METODI PER LA GESTIONE DEI DATI SLEGATI DA PERIFERICHE        */
/*################################################################################################*/
/*================================================================================================*/
/*                                  VERIFICA CREDENZIALI E AUTENTIFICA UTENTE                     */
/*================================================================================================*/
    private static final String regex = "^(.+)@(.+)\\.(.+)$";
    /**
     * Questo metodo verifica se una stringa può essere definibile come email.
     */
    public boolean verificaValiditaEmail(String email){
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(email);
        return ((email!=null) && (!email.equals("") && (matcher.matches())));
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  VERIFICA CREDENZIALI E AUTENTIFICA UTENTE                     */
/*================================================================================================*/
    /**
     * Questo metodo verifica che la stringa contenete la password si a valida. Se lo è verrà restituito
     * true altimenti false.
     * @param password
     * @return
     */
    public boolean verificaValiditaPassword(String password){
        return ((password!=null) && (!password.equals("")));
    }
/*================================================================================================*/
/*################################################################################################*/
}
