package it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.google.firebase.database.annotations.NotNull;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface.ComunicazioneFragmentModificaMenuSettimanaleViewModel;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Mense_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Ordinazioni_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.PietanzeForniteMensa_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.TipoMenu_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.TipoPasto_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.TipoPietanza_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;

public class ModificaMenuSettimanaleViewModel extends ComunicazioneFragmentModificaMenuSettimanaleViewModel {
/*################################################################################################*/
    private PietanzeForniteMensa_Repository rPietanze;
    private TipoPietanza_Repository rTipoPietanza;
    private Ordinazioni_Repository rOrdinazioni;
    private TipoPasto_Repository rTipoPasto;
    private TipoMenu_Repository rTipoMenu;
    private Mense_Repository rMense;
/*================================================================================================*/
/*                                  COSTRUTTORE VIEW MODEL                                        */
/*================================================================================================*/
    /**
     * Questo metodo costruttore del viewmode inizializza le repository che si interfacciano con entrambi
     * i database sia interno che estreno a seconda delle operazioni che vengono richeste. Che verranno
     * utilizate dall'activity e i suoi fragment.
     * @param application
     */
    public ModificaMenuSettimanaleViewModel(@NonNull Application application) {
        super(application);
        rPietanze    = new PietanzeForniteMensa_Repository(application);
        rTipoPietanza= new TipoPietanza_Repository(application);
        rOrdinazioni = new Ordinazioni_Repository(application);
        rTipoPasto   = new TipoPasto_Repository(application);
        rTipoMenu    = new TipoMenu_Repository(application);
        rMense       = new Mense_Repository(application);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  AZZIONI LEGATE ALLA GESITIONE DEI DATABASE                    */
/*################################################################################################*/
/*================================================================================================*/
/*                                  ORDINAZIONI                                                   */
/*================================================================================================*/
    /**
     * Questo motodo permette di ricevere la lista di tutte delle orinazioni di un singolo utente,
     * soecificato tramite il parametro idUtente, presenti nel database aventi una delle date presenti
     * nella lista passata come parametro.
     * @param idUtente
     * @param listaData
     * @return
     */
    public LiveData<List<Ordinazioni>> getAllOrdinazioniByIdUtente_InListDate(Long idUtente, List<Date> listaData){
        return rOrdinazioni.getAllOrdinazioniByIdUtente_InListDate(idUtente,listaData);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  TIPO PASTO                                                    */
/*================================================================================================*/
    /**
     * Questo metodo permette di ricevere la lista di tutti i tipi di pasto (pranzo,cena) presenti
     * nel database sempre aggiornati.
     * @return
     */
    public LiveData<List<TipoPasto>> getAllTipoPasto(){
        return rTipoPasto.getAll();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  MENSE                                                         */
/*================================================================================================*/
    /**
     * Questo metodo richiede al database (interno), le mense che l'utente con l'id passato come
     * parametro ha accesso con una tessera attiva in questo moento.
     * Cioè il risultato contiene tutte le mense che fanno parte di una università a cui l'utente
     * è iscritto o piò partecipare, di cui però ha una tessera attiva per il corrente anno accademico
     * (o attualmente attiva).
     * @param idUtente
     * @return
     */
    public LiveData<List<Mense>> getMenseAccessibleByIdOfUtenteAtToday(Long idUtente){
        return rMense.getAccessibleByIdOfUtenteAtToday(idUtente);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  TIPO MENU                                                     */
/*================================================================================================*/
    /**
     * Questo metodo richiede al database (interno), la lista di tutti i possbili tipi di menù.
     * @return
     */
    public LiveData<List<TipoMenu>> getAllTipoMenu(){
        return rTipoMenu.getAll();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  ORDINAZIONE PIETANZE DELLAa MENSE CON VOTO                    */
/*================================================================================================*/
    /**
     * Questo metodo richiede al database (interno), la lista di tutte le pietanze ordinate dal utnte
     * nel giorno specificato e con il tipo di pasto.
     * @return
     */
    public LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> getPietanzeOrdinateByIdUtente_IdTipoPasto_AtSpecificDate(
                                                                                                    @NotNull Long idUntente,
                                                                                                    @NotNull Long idTipoPasto,
                                                                                                    @NotNull Date date){
        return rOrdinazioni.getPietanzeOrdinateByIdUtente_IdTipoPasto_AtSpecificDate(idUntente,idTipoPasto,date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  PIETANZE FORNITE DA MENSA                                     */
/*================================================================================================*/
    /**
     * Questo metodo permette di restiuire la lista delle pietaze che fanno parte del menù dello stesso
     * tipo indicato dal paramero, della mensa che viene passata come parametro nel giorno specificato.
     * @param mense
     * @param tipoMenu
     * @param date
     * @return
     */
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificDay(@NotNull Mense mense,
                                                                                       @NotNull TipoMenu tipoMenu,
                                                                                       @NotNull Date date){
        return rPietanze.getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificDay(mense.getId(),tipoMenu.getId(),date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  TIPO PIETANZA                                                 */
/*================================================================================================*/
    /**
     * Questo metodo reichiede al database (interno) di restituire  tutte le tipologie di pietanze
     * presenti.
     * @return
     */
    public LiveData<List<TipoPietanza>> getAllTipoPietanza(){
        return rTipoPietanza.getAll();
    }
/*================================================================================================*/
/*################################################################################################*/

/*################################################################################################*/
/*                                  AZZIONI PER SALVARE LO STATO ATTUALE                          */
/*################################################################################################*/
/*================================================================================================*/
/*                                  SALVA STATO ATTUALE                                           */
/*================================================================================================*/
    /**
     * Questo metodo permette di individurare le ordinazioni già persenti ed eliminarli quelli non
     * più presenti ed aggiornare o aggiungere le nuove aordinazioni, inseerendo i nuovi dati.
     * @param listaOrdinazioni
     * @param listaVecchieOrdinazioni
     */
    public void rendiDefinitiveLeModifiche(List<Ordinazioni> listaOrdinazioni,List<Ordinazioni> listaVecchieOrdinazioni){

        HashMap<String,Ordinazioni> hmVecchieOrdinazioni = new HashMap<>();
        {//Blocco che genera un metodo di ricerca immediata dei veichi ordini
            Iterator<Ordinazioni> iterator = listaVecchieOrdinazioni.iterator();
            while (iterator.hasNext()){
                Ordinazioni ordinazioni = iterator.next();
                String key = super.generaChieaveDaOrdinazioni(ordinazioni);
                if(key!=null){
                    hmVecchieOrdinazioni.put(key,ordinazioni);
                }
            }
        }
        //Operazioni da fare con il database
        Iterator<Ordinazioni> iterator = listaOrdinazioni.iterator();
        while(iterator.hasNext()){
            Ordinazioni ordinazioni = iterator.next();
            String key = super.generaChieaveDaOrdinazioni(ordinazioni);
            if(key!=null){//Postebbe essere un ordine che non ha subito variazioni
                Ordinazioni ordinazioneVecchia = hmVecchieOrdinazioni.get(key);
                //Qui si rimuove dalla lista delle vecchie ordinazione tutte quelle che sono rappresentate
                // anche nella lista delle nuove così da avere alla fine solo quelle che erano presenti
                // ma che dovranndo essere cancellate.
                if(ordinazioneVecchia!=null) {
                    listaVecchieOrdinazioni.remove(ordinazioneVecchia);
                }
                //Operazinoni di modifica
                if((ordinazioneVecchia!=null) && (ordinazioni.equals(ordinazioneVecchia))){
                    //Ordinazione già presente ma le pietanze potebbero aver subito delle variazioni.
                    List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze = this.getListaPietanze(ordinazioni.getGiorno(),ordinazioni.getId_tipoPasto());
                    if((listaPietanze!=null) && (!listaPietanze.isEmpty())){
                        // Le pietanze sono state modificate e quindi si deve sostituirle
                        rOrdinazioni.overwriteListaOrdinazionePietanzeDellaMenseConVoto(ordinazioneVecchia,listaPietanze);
                        //todo----
                    }else if((listaPietanze!=null) && listaPietanze.isEmpty()){
                        //Non ci sono pietanze e quindi si deve cancellare l'ordinazione vecchia
                        rOrdinazioni.deleteByIdUtente_IdTipoPasto_AtSpecificDay(ordinazioneVecchia);
                        //todo----
                    }else{
                        //Le pietanze sono rimate le stesse già presenti
                    }


                }else{//DA QUI SI DEVE SOSTITUIRE L'ORINAZIONE
                    List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze = this.getListaPietanze(ordinazioni.getGiorno(),ordinazioni.getId_tipoPasto());
                    if((listaPietanze!=null) && (!listaPietanze.isEmpty())){
                        // Le pietanze sono state modificate e quindi si deve sostituirle dopo aver aggiunto l'odinzaione per recuperare l'id
                        if(ordinazioneVecchia==null){
                            rOrdinazioni.insertConVoto(ordinazioni,listaPietanze);
                        }else{
                            rOrdinazioni.overwriteAll(ordinazioni,listaPietanze);
                        }
                        //todo----
                    }else if(listaPietanze.isEmpty()){
                        //Non ci sono pietanze e quindi si deve cancellare l'ordinazione vecchia
                        rOrdinazioni.deleteByIdUtente_IdTipoPasto_AtSpecificDay(ordinazioni);
                        //todo----
                    }else{
                        //Le pietanze sono rimate le stesse già presenti si deve cancellare l'ordinazione vecchia
                    }
                }
            }else{
                //Attenzione
                //Non dovrebbe mai accadere
            }
        }
        //Rimozione dei ordini vecchi non più presenti
        Iterator<Ordinazioni> iteratorOld = listaVecchieOrdinazioni.iterator();
        while (iteratorOld.hasNext()){
            Ordinazioni ordinazioniOld = iteratorOld.next();
            //Cancellare l'ordinazione vecchia
            rOrdinazioni.deleteByIdUtente_IdTipoPasto_AtSpecificDay(ordinazioniOld);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
