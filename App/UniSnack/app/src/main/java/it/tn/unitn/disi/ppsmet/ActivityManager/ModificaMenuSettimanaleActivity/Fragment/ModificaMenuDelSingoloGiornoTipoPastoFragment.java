package it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Toast;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface.OnFragmentModificaMenuDelSingoloGiornoTipoPastoEventListener;
import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface.Parameters.SelezionaGiornoTipoPasto;
import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.ModificaMenuSettimanaleViewModel;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.AlertDialogApp.MessageAlertDialog;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.BarraConDuePulsanti;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.CheckableSubtitleView;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerModificaMenuSettimanale.RecyclerMMSingGiorTPasto.MenuDellaMenseSelezionabileRVAdapter;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.MySpinnerAdapter.SpinnerAdapter_Mense;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.MySpinnerAdapter.SpinnerAdapter_TipoMenu;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewSpinner.ViewInputSpinner_Text;
import it.tn.unitn.disi.ppsmet.R;


public class ModificaMenuDelSingoloGiornoTipoPastoFragment extends Fragment {
/*################################################################################################*/
/*                                  INIZIALIZZAZIONE VALORI                                       */
/*################################################################################################*/
    private static final String ARG_PARAM_ID_UTENTE_ATTIVO          = "idUtenteAttivo";
    /**
     * Questo parametro coterrà se sarà possibile l'accesso a determinati metodi dell'acticity contenete
     * il seguete fragment.
     */
    private OnFragmentModificaMenuDelSingoloGiornoTipoPastoEventListener listener = null;
/*################################################################################################*/
/*                                  INIZIALIZZAZIONE VALORI                                       */
/*################################################################################################*/
/*================================================================================================*/
/*                                  COSTRUTTORE                                                   */
/*================================================================================================*/
    /**
     * Questo è il metodo costruttore del fragment. Non contene nessun parametro poichè il metodo di
     * inserimento di quest'ultimi verrà fatto tramite i "Bundle".
     */
    //OBBLIGATORIO PER I FRAGMENT
    public ModificaMenuDelSingoloGiornoTipoPastoFragment() {
        super();
        idUtenteAttivo=-1l;
    }
/*------------------------------------------------------------------------------------------------*/
/*                                  COSTRUTTORE VERI                                              */
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo è il vero e proprio costruttore del fragment che oltre a costruire l'oggetto fragment,
     * permette anche di inserire i parametri nel "Bundle" ad esso assegnato. Così che in seguito
     * li possa eventualmente estarre.
     * @param idUtenteAttivo
     * @return
     */
    public static ModificaMenuDelSingoloGiornoTipoPastoFragment newInstance(Long idUtenteAttivo){
        ModificaMenuDelSingoloGiornoTipoPastoFragment fragment = new ModificaMenuDelSingoloGiornoTipoPastoFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_PARAM_ID_UTENTE_ATTIVO,idUtenteAttivo);
        fragment.setArguments(args);
        return fragment;
    }
/*================================================================================================*/
    private Long idUtenteAttivo;
/*================================================================================================*/
/*                                  INIZIALIZZATORE DI PARAMETRI                                  */
/*================================================================================================*/
    /**
     * Questo metodo viene chieamato nella fase di costruzione del frugment ed è quello che si occupa
     * di estrarre i dati inseriti nel "Bundle" dal costruttore e li imposta come volori del fragment.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            idUtenteAttivo     = getArguments().getLong(ARG_PARAM_ID_UTENTE_ATTIVO);
        }
    }
/*================================================================================================*/
/*################################################################################################*/

/*################################################################################################*/
/*                                  COMUNICAZIONE CON L'ACTIVITY                                  */
/*################################################################################################*/
/*================================================================================================*/
/*                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY              */
/*================================================================================================*/
    /**
     * Questo metodo permete di estrarre un eventuale implementeazione da parte dell'activity(contesto
     * padre) così da poter accedere ad dei metodi esterni al fragmen.
     * @param context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (OnFragmentModificaMenuDelSingoloGiornoTipoPastoEventListener) context;
        } catch (ClassCastException castException) {
            listener = null;
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private Fragment fragment = this;
/*------------------------------------------------------------------------*/
    private ModificaMenuSettimanaleViewModel viewModel    = null;
/*------------------------------------------------------------------------*/
    private ViewGroup rootView;
/*------------------------------------------------------------------------*/
    private RecyclerView rvListaTipoPietazeEPietanze ;
    private View copriSelezioneMenu;
    private CheckableSubtitleView subtitleCheckable;
    private BarraConDuePulsanti barraConDuePulsanti;
    private ViewInputSpinner_Text<Mense>    spinnerMense;
    private ViewInputSpinner_Text<TipoMenu> spinnerTipoMenu;
/*------------------------------------------------------------------------*/
    private SpinnerAdapter_Mense adapterMense;
    private SpinnerAdapter_TipoMenu adapterTipoMenu;
    private MenuDellaMenseSelezionabileRVAdapter adapterSeletorePietanze;
/*------------------------------------------------------------------------*/
    private LiveData<SelezionaGiornoTipoPasto> ldGiornoETipoSelezionato;
    private LiveData<List<Mense>>              ldListaMenseAttiveUtente;
    private LiveData<List<TipoMenu>>           ldListaTipoMenu;
    private LiveData<List<TipoPietanza>>       ldListaTipoPietanza;
/*------------------------------------------------------------------------*/
    private LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> ldPietanzeSelezionate;
/*------------------------------------------------------------------------*/
    private Date               dataDelOrdinazione  = null;
    private TipoPasto          tipoPasto           = null;
    private Mense              mensaSelezionata    = null;
    private TipoMenu           tipoMenuSelezionato = null;
    private List<TipoPietanza> listaTipoPietanze   = null;
/*------------------------------------------------------------------------*/
    private List<Mense>    listaMenseAttiveUtente = new LinkedList<>();
    private List<TipoMenu> listaTipoMenu          = new LinkedList<>();
/*------------------------------------------------------------------------*/
    private List<OrdinazionePietanzeDellaMenseConVoto> pietanzeSelezionate = new LinkedList<>();
/*================================================================================================*/
/*                                  CREAZIONE DELL'FRAGMENT                                       */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'inizializazione del layout del fragment ed inoltre contiene le loro
     * possibili interazioni con l'utente.
     * In aggiunta viene richesto un modelView che è quello usato dall'activity in modo tale da accedere
     * alla sua stessa istanza, in modo tale da utilizarlo come metodo per passare parameti da una parte
     * all'altra.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = (ViewGroup)  inflater.inflate(R.layout.fragment_modifica_menu_del_singolo_giorno_tipo_pasto, container, false);
/*------------------------------------------------------------------------*/
        rvListaTipoPietazeEPietanze = rootView.findViewById(R.id.rcl_ListaTipoPietanzeEPietanze);
        rvListaTipoPietazeEPietanze.setLayoutManager(new LinearLayoutManager(fragment.getContext()));
        impostaLayoutStatico();
        impostaAzzioniPulsanti();
/*------------------------------------------------------------------------*/
        subtitleCheckable = CheckableSubtitleView.newInstance(rootView, R.id.include_CheckableSubtitle);
        subtitleCheckable.setStatoAttivo(false);
        subtitleCheckable.setCheckable(false);
/*------------------------------------------------------------------------*/
        //Copetura pagina
        copriSelezioneMenu = rootView.findViewById(R.id.cvr_CopriSelezioneMenu);
        copriSelezioneMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(fragment.getContext(), R.string.msg_ImpossibiliAbilitareLaSelezioneMenu, Toast.LENGTH_LONG).show();
            }
        });
/*------------------------------------------------------------------------*/
        try {
            viewModel = new ViewModelProvider(getActivity()).get(ModificaMenuSettimanaleViewModel.class);
        }catch (Exception e){
            viewModel=null;
        }
/*------------------------------------------------------------------------*/
/*                      POSSO ACCEDERE AI DATI DEL VIEW MODEL             */
/*------------------------------------------------------------------------*/
        if(viewModel!=null){
/*------------------------------------------------------------------------*/
            ldGiornoETipoSelezionato = viewModel.getGiornoTipoSelezzionato();
            ldGiornoETipoSelezionato.observe(fragment.getViewLifecycleOwner(),
                    new Observer<SelezionaGiornoTipoPasto>() {
                        @Override
                        public void onChanged(SelezionaGiornoTipoPasto selezionaGiornoTipoPasto) {
                            if((selezionaGiornoTipoPasto!=null) &&
                                    (selezionaGiornoTipoPasto.getData()!=null) &&
                                    (selezionaGiornoTipoPasto.getTipoPasto()!=null) ){
                                impostazioniPaginaAdattataAlGiornoETipoPasto(selezionaGiornoTipoPasto);
                            }
                        }
                    });
        }
/*------------------------------------------------------------------------*/
        return rootView;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA LAYOUT STATICO                                        */
/*================================================================================================*/
    /**
     * Questo metodo pertette di impostrate le varie parti statiche di layout.
     */
    private void impostaLayoutStatico(){
        barraConDuePulsanti = new BarraConDuePulsanti(  rootView,
                                                        R.id.include_BottoniSalvaAnnullaModifiche,
                                                        R.string.lbl_ButtonSalva,
                                                        false,
                                                        R.string.lbl_ButtonAnnula);
    }
/*================================================================================================*/
/*################################################################################################*/

/*################################################################################################*/
/*                                  AZZIONI E IMPOSTAZIONI LAYOUT DOPO LA RECEZIONE DEL GIORNO    */
/*                                  E TIPO PASTO SELEZIONATO                                      */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTAZIONIE LAYOUT GENERALI                                 */
/*================================================================================================*/
    /**
     * Questo metodo permette di adattare tutta la pagina alla data e al tipo pasto selezionato e
     * riscevuto come parametro.
     * @param selezionaGiornoTipoPasto
     */
    private void impostazioniPaginaAdattataAlGiornoETipoPasto(SelezionaGiornoTipoPasto selezionaGiornoTipoPasto){
/*------------------------------------------------------------------------*/
        this.dataDelOrdinazione = selezionaGiornoTipoPasto.getData();
        this.tipoPasto          = selezionaGiornoTipoPasto.getTipoPasto();
/*------------------------------------------------------------------------*/
        impostaSottotitolo(this.dataDelOrdinazione,this.tipoPasto);
/*------------------------------------------------------------------------*/
        barraConDuePulsanti.setOnPrimaryButtonEnabled(true);
/*------------------------------------------------------------------------*/
        impostaSpinnerMensa();
        gesioneAggiornametiSpinnerMensa();
/*------------------------------------------------------------------------*/
        impostaSpinnerTipoMenu();
        gesioneAggiornametiSpinnerTipoMenu();
/*------------------------------------------------------------------------*/
        if(viewModel.esisteOrdinazioneProposta(dataDelOrdinazione,tipoPasto.getId())){
            MessageAlertDialog alertDialog= new MessageAlertDialog(
                    fragment.getContext(),
                    R.layout.alert_dialog_message,
                    R.string.ttl_Attenzione,
                    R.string.txt_MessaggioModificheOrdinazioniGiaConfigurate);
            alertDialog.create();
            alertDialog.show();
        }
        selezionaMensaTipoMenuPerRecycleView();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTAZIONIE SOTTOTITOLO                                     */
/*================================================================================================*/
    /**
     * Questo metodo contiene la logica per adattare il sottotitolo ai dati della data e del tipo pasto
     * ed anche quella che permette di rendere non possibile modificare i dati del menu.
     */
    private void impostaSottotitolo(Date dataDelOrdinazione,TipoPasto tipoPasto){
        subtitleCheckable.setData(dataDelOrdinazione);
        subtitleCheckable.setTipoPasto(tipoPasto);
        subtitleCheckable.setCheckable(true);
        subtitleCheckable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean statoDiSelezione) {
                if(statoDiSelezione){
                    //Selezionato
                    copriSelezioneMenu.setVisibility(View.GONE);
                }else{
                    //Non Selezionato
                    copriSelezioneMenu.setVisibility(View.VISIBLE);
                }
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE SPINNER                                              */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA SPINNER MENSA                                         */
/*================================================================================================*/
    /**
     * Questo metodo permette di impostare il layout dello spinner della mensa ed associarci l'adabter
     * che gestirà i dati.
     */
    private void impostaSpinnerMensa(){
        adapterMense = new SpinnerAdapter_Mense(getContext(),listaMenseAttiveUtente);
        spinnerMense = new ViewInputSpinner_Text(rootView,
                                                R.id.include_SpinnerMensa,
                                                R.string.lbl_Mensa,
                                                adapterMense, Mense.class);
    }
/*------------------------------------------------------------------------*/
    /**
     * Questo metodo permette di mantenere aggiornata la lista dei elementi dello spinner che contiene
     * le mense.
     */
    private void gesioneAggiornametiSpinnerMensa(){
        ldListaMenseAttiveUtente = viewModel.getMenseAccessibleByIdOfUtenteAtToday(idUtenteAttivo);
        ldListaMenseAttiveUtente.observe(fragment,
                new Observer<List<Mense>>() {
                    @Override
                    public void onChanged(List<Mense> listaMense) {
                        if(listaMense!=null){
                            listaMenseAttiveUtente.clear();
                            listaMenseAttiveUtente.addAll(listaMense);
                            adapterMense.notifyDataSetChanged();
                        }
                    }
                });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA SPINNER TIPO MENU                                     */
/*================================================================================================*/
    /**
     * Questo metodo permette di impostare il layout dello spinner del tipo menu ed associarci l'adabter
     * che gestirà i dati.
     */
    private void impostaSpinnerTipoMenu(){
        adapterTipoMenu = new SpinnerAdapter_TipoMenu(getContext(),listaTipoMenu);
        spinnerTipoMenu = new ViewInputSpinner_Text(rootView,
                                                    R.id.include_SpinnerTipoMenu,
                                                    R.string.lbl_TipoMenu,
                                                    adapterTipoMenu, TipoMenu.class);
    }
/*------------------------------------------------------------------------*/
    /**
     * Questo metodo permette di mantenere aggiornata la lista dei elementi dello spinner che contiene
     * la lista dei tipi di menu.
     */
    private void gesioneAggiornametiSpinnerTipoMenu(){
        ldListaTipoMenu = viewModel.getAllTipoMenu();
        ldListaTipoMenu.observe(fragment,
                new Observer<List<TipoMenu>>() {
                    @Override
                    public void onChanged(List<TipoMenu> nuovaListaTipoMenu) {
                        if(nuovaListaTipoMenu!=null){
                            listaTipoMenu.clear();
                            listaTipoMenu.addAll(nuovaListaTipoMenu);
                            adapterTipoMenu.notifyDataSetChanged();
                        }
                    }
                });
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE RECYCLER VIEW                                        */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA DATI DI CONTORNO                                      */
/*================================================================================================*/
    /**
     * Questo metodo permette di aggiungere due listener, uno per ciascuno dei due spinner, di cui il
     * primo permette di selezionare la mensa e che un utente può accedere,e il secondo il tipo di menu
     * associato.
     * Così facendo, in questo metodo verranno matenuti aggiornati i valori selezionati e alla variazione
     * di una di essi verrà chiamato il metodo per la variazione che si occupa del cambiamento dei
     * dati da mostrare nel recyclerview.
     * Inoltre contiene la richiesta al database di tutti i tipi di pietanze persenti nel database.
     */
    private void selezionaMensaTipoMenuPerRecycleView(){
/*------------------------------------------------------------------------*/
        spinnerMense.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Mense nuovaMensa = spinnerMense.getItemSelected();
                //Cambia valiori recyclerView
                impostaRecycleView(nuovaMensa,tipoMenuSelezionato,listaTipoPietanze);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
/*------------------------------------------------------------------------*/
        spinnerTipoMenu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TipoMenu nuovoTipoMenu = spinnerTipoMenu.getItemSelected();
                //Cambia valiori recyclerView
                impostaRecycleView(mensaSelezionata,nuovoTipoMenu,listaTipoPietanze);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
/*------------------------------------------------------------------------*/
        /**
         * Richiedi tutti i tipi di pietanze per poterle riconoscere succesivamente.
         */
        ldListaTipoPietanza = viewModel.getAllTipoPietanza();
        ldListaTipoPietanza.observe(fragment, new Observer<List<TipoPietanza>>() {
            @Override
            public void onChanged(List<TipoPietanza> nuovaListaTipoPietanze) {
                impostaRecycleView(mensaSelezionata,tipoMenuSelezionato,nuovaListaTipoPietanze);
            }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA RECYCLER VIEW                                         */
/*================================================================================================*/
    private synchronized void impostaRecycleView(Mense mense,TipoMenu tipoMenu,List<TipoPietanza> listaTipoPietanze){
        this.mensaSelezionata    = mense;
        this.tipoMenuSelezionato = tipoMenu;
        this.listaTipoPietanze   = listaTipoPietanze;
        if((this.mensaSelezionata!=null) && (this.tipoMenuSelezionato!=null) && (this.listaTipoPietanze!=null)){
            LiveData<List<PietanzeDellaMense>> ldListaPietanze = viewModel.getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificDay(
                                                                                                        this.mensaSelezionata,
                                                                                                        this.tipoMenuSelezionato,
                                                                                                        dataDelOrdinazione);
            if(ldListaPietanze!=null){
                ldListaPietanze.observe(fragment,
                        new Observer<List<PietanzeDellaMense>>() {
                            @Override
                            public void onChanged(List<PietanzeDellaMense> liataPietanze) {
                                if(liataPietanze!=null){
                                    adapterSeletorePietanze = new MenuDellaMenseSelezionabileRVAdapter(getContext(),liataPietanze,listaTipoPietanze);
                                    rvListaTipoPietazeEPietanze.setAdapter(adapterSeletorePietanze);
                                }
                            }
                        });
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/

/*################################################################################################*/
/*                                  GESTIONE PULSANTI                                             */
/*################################################################################################*/
/*================================================================================================*/
/*                                  AGGIUNGI LISSENER PULSANTI                                    */
/*================================================================================================*/
    /**
     * Questo medodo definise i due lisserne dei due pulsanti infondo alla pagina, sia quello per
     * salvare le eventuali modifiche appotate, che quello per annulare l'operazione di modifica.
     */
    private void impostaAzzioniPulsanti(){
        barraConDuePulsanti.setOnSecondaryButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.annullaModificheMenuSettimanale(dataDelOrdinazione,tipoPasto);
            }
        });
/*----------------------------------*/
        barraConDuePulsanti.setOnPrimaryButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<PietanzeDellaMense> listaPietanze= new LinkedList<>();
                if((adapterSeletorePietanze!=null) && (subtitleCheckable.isChecked())){
                    listaPietanze = adapterSeletorePietanze.getListaPietanzeSelezionate();
                }
                listener.salvaModificheMenuSettimanaleDelGiorno(idUtenteAttivo,dataDelOrdinazione,tipoPasto,listaPietanze);
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
}