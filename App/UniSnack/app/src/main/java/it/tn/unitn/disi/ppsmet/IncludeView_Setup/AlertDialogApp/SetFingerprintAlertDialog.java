package it.tn.unitn.disi.ppsmet.IncludeView_Setup.AlertDialogApp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import it.tn.unitn.disi.ppsmet.IncludeView_Setup.BarraConDuePulsanti;
import it.tn.unitn.disi.ppsmet.MyAnimation.MyAnimationView;
import it.tn.unitn.disi.ppsmet.R;

public class SetFingerprintAlertDialog extends AlertDialog {
/*################################################################################################*/
    protected Context context;
/*------------------------------------------------------------------------*/
    protected View view=null;//Layout di tutto il AlertDialog
/*------------------------------------------------------------------------*/
    private BarraConDuePulsanti  barraConDuePulsanti;
    private MyAnimationView      fingerprintAnimato;
    private FloatingActionButton btnAggiungi;
    private FloatingActionButton btnCancellaTutto;
/*================================================================================================*/
/*                                  CREAZIONE DEL ALERTDIALOG                                     */
/*================================================================================================*/
    /**
     * Metodo che crea un alertdialog con la struttura di base e le funzionalità di base per inciare
     * un messaggio
     */
    @SuppressLint("RestrictedApi")
    public SetFingerprintAlertDialog(Context context, int idAlertDialog, CheckBox fromView) {
        super(context);
        this.context = context;
        //Imposta il layout
/*------------------------------------------------------------------------*/
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(idAlertDialog,null);
/*------------------------------------------------------------------------*/
        fingerprintAnimato = view.findViewById(R.id.img_anim_Fingerpirnt);
        btnCancellaTutto   = view.findViewById(R.id.btn_CancellaTutto);
        btnAggiungi        = view.findViewById(R.id.btn_Aggiungi);
        btnCancellaTutto   = view.findViewById(R.id.btn_CancellaTutto);
/*------------------------------------------------------------------------*/
        barraConDuePulsanti = new BarraConDuePulsanti(  (ViewGroup)view,
                R.id.include_BottoniFinestra,
                R.string.lbl_ButtonSalva,
                false,
                R.string.lbl_ButtonAnnula);
/*------------------------------------------------------------------------*/
        if(fromView.isChecked()){
            btnCancellaTutto.setVisibility(View.VISIBLE);
            barraConDuePulsanti.setOnPrimaryButtonEnabled(true);
        }
/*------------------------------------------------------------------------*/
        attivaAggiungiFingerprint();
        impostaAzzionePulsanteAggiungi();
        impostaAzzionePulsanteCancellaTutto();
        impostaAzzioniPulsantiAlertDialog(fromView);
/*------------------------------------------------------------------------------------------------*/
        //Non chiudere si non con il tasto
        this.setCanceledOnTouchOutside(false);
        this.setView(view);
/*------------------------------------------------------------------------*/
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  GESTIONE ALTER DIALOG                                         */
/*################################################################################################*/
/*================================================================================================*/
/*                                  LISTENER                                                      */
/*================================================================================================*/
    /**
     * Questo metodo contiene la logica per salvare e rendere definitive le modifice oppure annullare
     * lasciando invariato lo stato attuale.
     */
    private void impostaAzzioniPulsantiAlertDialog(CheckBox fromView){
        // Listener del pulsante
        barraConDuePulsanti.setOnSecondaryButtonClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        cancel();
                    }
                });
        barraConDuePulsanti.setOnPrimaryButtonClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        if(btnCancellaTutto.getVisibility()==View.VISIBLE){
                            fromView.setChecked(true);
                        }else{
                            fromView.setChecked(false);
                        }
                        cancel();
                    }
                });
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  GESTIONE PULSANTI AGGIUNGI E CANCELLA TUTTO                   */
/*################################################################################################*/
/*================================================================================================*/
/*                                  LISTENER AGGIUNGI                                             */
/*================================================================================================*/
    /**
     * Questo metodo permette di riattivare la procedura di aggiunta di una nuova fingerprint.
     */
    private void impostaAzzionePulsanteAggiungi(){
        btnAggiungi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attivaAggiungiFingerprint();
            }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  LISTENER CANCELLA TUTTO                                       */
/*================================================================================================*/
    /**
     * Questo metodo permette di riattivare la procedura di aggiunta nuovo fingerprint cancellano
     * le operazioni fatte fino ad ora.
     */
    private void impostaAzzionePulsanteCancellaTutto(){
        btnCancellaTutto.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View view) {
                attivaAggiungiFingerprint();
                btnAggiungi.setVisibility(View.INVISIBLE);
                btnCancellaTutto.setVisibility(View.INVISIBLE);
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  GESTIONE DEI PULSANTI E ANIMAZIONI PER FINGERPRINT            */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTAZIONI DEL ANIMAZIONE DI BASE PER L'AGGIUNTA            */
/*================================================================================================*/
    /**
     * Questo metodo contiene le operazioni da fare per attivare una nuova lettura d'una improta digitale
     * da aggiungere.
     */
    private void attivaAggiungiFingerprint(){
        fingerprintAnimato.setNewAnimazioneRipetuta(R.drawable.avd_anim,MyAnimationView.RIPETIZIONE_INFINITA);
        fingerprintAnimato.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View view) {
                fingerprintAnimato.setOnClickListener(null);
                fingerprintAnimato.setNewAnimazioneRipetuta(R.drawable.avd_anim_scann_checked,MyAnimationView.ULTIMA_ANIMAZIONE);
                barraConDuePulsanti.setOnPrimaryButtonEnabled(true);
                btnAggiungi.setVisibility(View.VISIBLE);
                btnCancellaTutto.setVisibility(View.VISIBLE);
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE ANIMAZIONE                                           */
/*################################################################################################*/
/*================================================================================================*/
/*                                  ON CANCEL                                                      */
/*================================================================================================*/
    /**
     * In questo override del metodo "cancel" si trova la logica legata alla disattivazione delle
     * animazioni presenti nell'alertDialog così da blocare qualunque animazione.
     */
    @Override
    public void cancel() {
        super.cancel();
        fingerprintAnimato.setNewAnimazioneRipetuta(R.drawable.avd_anim,MyAnimationView.RIPETIZIONE_DI_DEFAUULT);
    }
/*================================================================================================*/
/*################################################################################################*/

}
