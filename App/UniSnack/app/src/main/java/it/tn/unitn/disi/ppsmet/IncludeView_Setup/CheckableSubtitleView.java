package it.tn.unitn.disi.ppsmet.IncludeView_Setup;

import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.R;

public class CheckableSubtitleView {
/*################################################################################################*/
    private ViewGroup contenitore;
    private TextView  titoloSinistro;
    private TextView  titoloDestro;
    private CheckBox  checkBox;
/*------------------------------------------------------------------------*/
    private Date       date;
    private TipoPasto  tipoPasto;
/*------------------------------------------------------------------------*/
    private boolean isCheckable = true;
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    /**
     * Questo è il costrittore vero e proprio del layout.
     * @param contenitore
     * @param titoloSinistro
     * @param titoloDestro
     * @param checkBox
     * @param date
     * @param tipoPasto
     */
    public CheckableSubtitleView(ViewGroup contenitore,
                                 TextView  titoloSinistro,
                                 TextView  titoloDestro,
                                 CheckBox  checkBox,
                                 Date date,
                                 TipoPasto tipoPasto) {
        this.contenitore     = contenitore;
        this.titoloSinistro  = titoloSinistro;
        this.titoloDestro    = titoloDestro;
        this.checkBox        = checkBox;
        setData(date);
        setTipoPasto(tipoPasto);
        setStatoAttivo(false);
        contenitore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isCheckable){
                    setStatoAttivo(!checkBox.isChecked());
                }
            }
        });
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo è il costuttore di quando l'oggetto viene impostato in una pagina o in una sua parte.
     * @param rootLayout
     * @param idLayoutIncluded
     * @param date
     * @param tipoPasto
     * @return
     */
    public static CheckableSubtitleView newInstance(ViewGroup rootLayout, int idLayoutIncluded, Date date, TipoPasto tipoPasto) {
        CheckableSubtitleView ris = null;
        if((rootLayout!=null) && (date!=null) && (tipoPasto!=null)){
            ViewGroup barraContenitore = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(barraContenitore!=null){
                TextView  titoloSinistro= barraContenitore.findViewById(R.id.txt_TitoloSinistro);
                TextView  titoloDestro= barraContenitore.findViewById(R.id.txt_TitoloDestro);
                CheckBox  checkBox= barraContenitore.findViewById(R.id.ckb_Subtitle);
                if((titoloSinistro!=null) && (titoloDestro!=null) && (checkBox!=null)){
                    ris = new CheckableSubtitleView( barraContenitore,
                                                    titoloSinistro,
                                                    titoloDestro,
                                                    checkBox,date,tipoPasto);
                }
            }
        }
        return ris;
    }
/*------------------------------------------------------------------------*/
    /**
     * Questo è il costuttore di quando l'oggetto viene impostato in una pagina o in una sua parte.
     * @param rootLayout
     * @param idLayoutIncluded
     * @return
     */
    public static CheckableSubtitleView newInstance(ViewGroup rootLayout, int idLayoutIncluded) {
        CheckableSubtitleView ris = null;
        if((rootLayout!=null)){
            ViewGroup barraContenitore = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(barraContenitore!=null){
                TextView  titoloSinistro= barraContenitore.findViewById(R.id.txt_TitoloSinistro);
                TextView  titoloDestro= barraContenitore.findViewById(R.id.txt_TitoloDestro);
                CheckBox  checkBox= barraContenitore.findViewById(R.id.ckb_Subtitle);
                if((titoloSinistro!=null) && (titoloDestro!=null) && (checkBox!=null)){
                    ris = new CheckableSubtitleView( barraContenitore,
                                                    titoloSinistro,
                                                    titoloDestro,
                                                    checkBox,null,null);
                }
            }
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  GET AND SET                                                   */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA E CAMBIA DATA                                         */
/*================================================================================================*/
    /**
     * Questo metodo oltre ad impostare la data all'interno del oggete fa cambiare il layout del oggetto.
     * @param data
     */
    public void setData(Date data){
        this.date = data;
        if(data!=null){
            String giornoDellaSettimana = new SimpleDateFormat("EEEE", Locale.ITALY).format(data.getTime());
            giornoDellaSettimana =  giornoDellaSettimana.substring(0, 1).toUpperCase() + giornoDellaSettimana.substring(1);
            this.titoloSinistro.setText(giornoDellaSettimana);
        }else{
            this.titoloSinistro.setText("");
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA E CAMBIA TIPO PASTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo oltre ad impostare il Tipo Pasto all'interno del oggete fa cambiare il layout del oggetto.
     * @param tipoPasto
     */
    public void setTipoPasto(TipoPasto tipoPasto){
        this.tipoPasto = tipoPasto;
        if(tipoPasto!=null){
            this.titoloDestro.setText(tipoPasto.getNome());
        }else{
            this.titoloDestro.setText("");
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  IMPOSTAZIONI DI LAYOU                                         */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA SE L'ELEMETO E' POSSIBILE ATTIVARLO E DIATTIVARLO     */
/*================================================================================================*/
    /**
     * Questo metodo attiva e diattiva la posibilità di utilizare questo oggetto come checkbox.
     * @param isCheckable
     */
    public void setCheckable(boolean isCheckable){
        this.isCheckable = isCheckable;
        if(!isCheckable){
            //Disattivo
            this.checkBox.setVisibility(View.GONE);
        }else{
            //Attivo
            this.checkBox.setVisibility(View.VISIBLE);
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA TIPO GIORNO ATTIVO O DISATTIVO                        */
/*================================================================================================*/
    /**
     * Questo metodo permette di cambiare lo stato se selezionato o deselezionato.
     * @param isActive
     */
    public void setStatoAttivo(boolean isActive){
        checkBox.setChecked(isActive);
        if(!isActive){
            //Disattivo
            this.titoloSinistro.setTextColor(contenitore.getResources().getColor(R.color.colorNormalText,null));
            this.titoloDestro.setTextColor(contenitore.getResources().getColor(R.color.colorNormalText,null));
            this.titoloSinistro.setTypeface(null, Typeface.NORMAL);
            this.titoloDestro.setTypeface(null, Typeface.NORMAL);
            this.contenitore.setBackgroundResource(R.color.colorSecondaryLight);
            checkBox.setButtonTintList(ContextCompat.getColorStateList(checkBox.getContext(), R.color.colorNormalText));
        }else{
            //Attivo
            this.titoloSinistro.setTextColor(contenitore.getResources().getColor(R.color.colorBackgraundBlock,null));
            this.titoloDestro.setTextColor(contenitore.getResources().getColor(R.color.colorBackgraundBlock,null));
            this.titoloSinistro.setTypeface(null, Typeface.BOLD);
            this.titoloDestro.setTypeface(null, Typeface.BOLD);
            this.contenitore.setBackgroundResource(R.color.colorPrimaryLight);
            checkBox.setButtonTintList(ContextCompat.getColorStateList(checkBox.getContext(), R.color.colorBackgraundBlock));
        }
    }
/*================================================================================================*/
/*################################################################################################*/

/*################################################################################################*/
/*                                  GET                                                           */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IS CHECKED                                                    */
/*================================================================================================*/
    /**
     * Questo metodo restituisce lo stato della checkbox sel titolo così da permettere di sapere se
     * è selezionato o meno.
     * @return
     */
    public boolean isChecked(){
        return checkBox.isChecked();
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  IMPOSTAZIONI LISTENER                                         */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA ON CHECKED CHANGE LISTENER                            */
/*================================================================================================*/
    /**
     * Questo metodo permette di avare un listener sul cambiameto di stato.
     * @param listener
     */
    public void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener listener){
        this.checkBox.setOnCheckedChangeListener(listener);
    }
/*================================================================================================*/
/*################################################################################################*/
}
