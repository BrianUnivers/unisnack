package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface;

import androidx.arch.core.util.Function;

import com.google.firebase.database.DataSnapshot;

import java.util.LinkedList;
import java.util.List;

/**
 * Questa classe pernde il risultato di una query al firebase runtime database e lo trasforma in oggetti
 * incapsulati in uno di tipo "LiveData".
 * ATTENZIONE! Anche null è da considerarsi come valore valido che viene incapsulato singolarmente ed o
 * tra i dati validi.
 */
public abstract class GenericDeserializer_List<T> implements Function<DataSnapshot, List<T>> {
/*################################################################################################*/
/*================================================================================================*/
/*                         GET DataLive OF LIST OBJECTS                                           */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET DataLive OF LIST OBJECTS FROM DataSnapshot                         */
/*------------------------------------------------------------------------------------------------*/
    @Override
    public List<T> apply(DataSnapshot dataSnapshot) {
        List<T> ris = new LinkedList<>();
        for(DataSnapshot ds : dataSnapshot.getChildren()){
            T oggetto = estraiDaDataSnapshot(ds);
            if(oggetto != null){
                ris.add(oggetto);
            }
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/

/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "AnnoAccademico" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "AnnoAccademico".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public abstract T estraiDaDataSnapshot(DataSnapshot dataSnapshot);
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
}

