package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListOrdinPiet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ListDataWithLabel.ListDataWithLabelView;
import it.tn.unitn.disi.ppsmet.R;

public class PersonMenuSetPietETPietRVAdapter extends RecyclerView.Adapter<PersonMenuSetPietETPietRVAdapter.MyViewHoder> {
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE INTERNA                                                    */
/*################################################################################################*/
    /**
     * Questa classe è quella quella che viene usata per permettere alla RecyclingView Adapter di definire
     * il layout singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public ViewGroup             v_mainLayout;
        //Contenitorei
        public ListDataWithLabelView v_ListaPietanzeETipoPietanza;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout        = itemView.findViewById(R.id.cly_RowElemento);
            //Contenitorei
            v_ListaPietanzeETipoPietanza = ListDataWithLabelView.newInstance(v_mainLayout,R.id.include_lista_pietanze);
        }
/*================================================================================================*/
    }
/*################################################################################################*/


/*################################################################################################*/
/*                              CLASSE PRINCIPALE ModificaMenuSettimanaleGiorniRVAdapter          */
/*################################################################################################*/
    private Context comtext = null;
    private List<TipoPietanza>  listaTipoPietanzaUsate = null;
    private List<OrdinazionePietanzeDellaMenseConVoto>  listaPietanze = null;
    /**
     * Questo attributo contiene le liste delle pietanze associate all'id dell'ordine di cui fanno
     * parte.
     */
    private HashMap<Long,List<Pietanze>> hmListaPietanze  = null;
    private HashMap<Long,TipoPietanza> hmTipoPietanza  = null;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public PersonMenuSetPietETPietRVAdapter(Context comtext,
                                            List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze,
                                            HashMap<Long,TipoPietanza> hmTipoPietanza) {
        this.comtext = comtext;
        this.listaPietanze     = listaPietanze;
        this.hmTipoPietanza    = hmTipoPietanza;
        this.hmListaPietanze   = generaStrutturaPietanze(listaPietanze);
        //Generazione lista tipo pietanze
        this.listaTipoPietanzaUsate = new LinkedList<>();
        Iterator<Long> iterator = hmListaPietanze.keySet().iterator();
        while (iterator.hasNext()){
            Long idTipoPietanze = iterator.next();
            TipoPietanza tipoPietanza = hmTipoPietanza.get(idTipoPietanze);
            if(tipoPietanza!=null){
                listaTipoPietanzaUsate.add(tipoPietanza);
            }
        }
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              CERAZIONE DEL SINGOLO ELEMETOTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(comtext);
        View bloccoElemeto = inflater.inflate(R.layout.row_list_pietanze_di_tipo_pietanza, parent,false);
        MyViewHoder myViewHoder = new MyViewHoder(bloccoElemeto);
        return myViewHoder;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              INSERIRE L'ELEMETO NEL LAYOUT                                     */
/*================================================================================================*/
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore ModificaMenuSettimanaleGiorniRVAdapter.
     */
    @Override
    public void onBindViewHolder(@NonNull MyViewHoder holder, int position) {
        final TipoPietanza elemento = listaTipoPietanzaUsate.get(position);
        holder.v_ListaPietanzeETipoPietanza.setLabel(elemento.getNome());

        //Pietanze
        List<Pietanze> listaPietanze = hmListaPietanze.get(elemento.getId());
        //Seleziona il tipo di layout
        inserisciPietanzeNelLayout(holder,listaPietanze);
    }
/*================================================================================================*/
/*                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE                  */
/*================================================================================================*/
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaTipoPietanzaUsate.size();
    }
/*================================================================================================*/


/*================================================================================================*/
/*                              INSERISCI I DATI NEL LAYOUT                                       */
/*================================================================================================*/
    /**
     * Questo metodo si occupa di inserire la lista di pietanze associate ad un singolo tipo di pietanza.
     */
    private void inserisciPietanzeNelLayout(@NonNull MyViewHoder holder, List<Pietanze> listaPietanze){
        List<String> listaNomiPietanze = new LinkedList<>();
        if(listaPietanze!=null){
            Iterator<Pietanze> iterator = listaPietanze.iterator();
            while(iterator.hasNext()){
                Pietanze pietanze = iterator.next();
                if((pietanze!=null) && (pietanze.getNome()!=null)){
                    String nomiPietanze = pietanze.getNome();
                    nomiPietanze =  nomiPietanze.substring(0, 1).toUpperCase() + nomiPietanze.substring(1);
                    listaNomiPietanze.add(nomiPietanze);
                }
            }
        }
        ListaStringRVAdapter adapter = new ListaStringRVAdapter(comtext,listaNomiPietanze);
        holder.v_ListaPietanzeETipoPietanza.setValue(adapter);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                              METODI D'APPOGGIO AL ADAPTER                                      */
/*################################################################################################*/
/*================================================================================================*/
/*                              CEAZIONE DEL HASMAP DEI TIPI PASTO                                */
/*================================================================================================*/
    /**
     * Questo metodo permette di separare le varie pietanze per tipo in modo tale da associare ad ogni
     * gruppo l'id del tipo di pasto di appartenenza.
     * @param listaPietanze
     * @return
     */
    private HashMap<Long,List<Pietanze>>  generaStrutturaPietanze(List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze){
        HashMap<Long,List<Pietanze>>  ris = new HashMap<>();
        if(listaPietanze!=null){
            Iterator<OrdinazionePietanzeDellaMenseConVoto> iterator = listaPietanze.iterator();
            while (iterator.hasNext()){
                OrdinazionePietanzeDellaMenseConVoto pietanzeDellaMense = iterator.next();
                if((pietanzeDellaMense!=null) && (pietanzeDellaMense.getPietanze()!=null) &&
                        (pietanzeDellaMense.getPietanze().getId_tipoPietanza()!=null)){
                    List<Pietanze> lista = new LinkedList<>();
                    Long id = pietanzeDellaMense.getPietanze().getId_tipoPietanza();
                    if(ris.get(id)!=null){
                        lista.addAll(ris.get(id));
                    }
                    lista.add(pietanzeDellaMense.getPietanze());
                    ris.put(id,lista);
                }
            }
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/

}