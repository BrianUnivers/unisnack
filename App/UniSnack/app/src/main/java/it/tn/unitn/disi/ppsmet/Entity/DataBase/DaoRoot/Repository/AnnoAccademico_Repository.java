package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.AnnoAccademico_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_AnnoAccademico_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.AnnoAccademico_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;

public class AnnoAccademico_Repository extends AnnoAccademico_ThreadAggiornamento {
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA ANNO ACCADEMICO(e le sue derivazioni    */
/*                         seguendo un ordine topologico per garantire gli aggiornamenti)         */
/*                         ATTRAVERO IL DATABASE LOCALE (room)                                    */
/*################################################################################################*/
    private LiveData<List<AnnoAccademico>> mAllAnnoAccademico;
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public AnnoAccademico_Repository(Application application) {
        super(application);
        //AllAnnoAccademico get any time the same result
        mAllAnnoAccademico = annoAccademicoDao.getAll_Synchronized();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERT                                                                 */
/*================================================================================================*/
    /*public Future<Long> insert(AnnoAccademico annoAccademico){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                    @Override
                    public Long call() throws Exception {
                        return annoAccademicoDao.insert(annoAccademico);
                    }
                });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    /*public void deleteAll(){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            annoAccademicoDao.deleteAll();
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    /*public void update(AnnoAccademico annoAccademico){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            annoAccademicoDao.update(annoAccademico);
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    /*public LiveData<List<AnnoAccademico>> getAll(){
        return mAllAnnoAccademico;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    /*public LiveData<AnnoAccademico> getByPrimaryKey(Long id){
        return annoAccademicoDao.getByPrimaryKey_Synchronized(id);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET ACTIVE TODAY                                                 */
/*================================================================================================*/
    public LiveData<AnnoAccademico> getActiveAtToday(){
        return annoAccademicoDao.getActiveAtToday_Synchronized();
    }
/*================================================================================================*/
/*################################################################################################*/
}
