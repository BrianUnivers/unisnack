package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.Uni_ha_C;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

@Dao
public abstract class  Corsi_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    //Solleva un eccezione se non riesce ad inserire l'associazione.
    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long insert(Corsi corsi, Universita universita){
        Long id = this.exist(corsi);
        if(id==null){
            id = this.insert(corsi);
        }
        Uni_ha_C relazione = new Uni_ha_C(universita.getId(),id);
        this.insert(relazione);
        return id;
    }
/*------------------------------------------------------------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(Corsi corsi);
/*------------------------------------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(Uni_ha_C uni_ha_c);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Query("DELETE FROM tableCorsi")
    public abstract void deleteAll();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE By ID                                                    */
/*================================================================================================*/
    @Query("DELETE FROM tableCorsi WHERE id == :id")
    public abstract void delete(Long id);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE By ID CORSO AND UNIVERSITA                               */
/*================================================================================================*/
    @Query("DELETE FROM tableUni_ha_C WHERE id_corso == :idCorso AND id_universita == :idUniversta ")
    public abstract void delete(Long idUniversta,Long idCorso);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = Corsi.class,
            onConflict = OnConflictStrategy.ABORT)
    public abstract void update(Corsi corsi);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    @Query("SELECT * from tableCorsi")
    public abstract List<Corsi> getAll();
/*------------------------------------------------------------------------*/
    @Query("SELECT * from tableCorsi")
    public abstract LiveData<List<Corsi>> getAll_Synchronized();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    @Query("SELECT * FROM tableCorsi WHERE id == :id")
    public abstract Corsi getByPrimaryKey(Long id);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableCorsi WHERE id == :id")
    public abstract LiveData<Corsi> getByPrimaryKey_Synchronized(Long id);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UNIVERSITA'                                           */
/*================================================================================================*/
    @Query( "SELECT C.* " +
            "FROM tableCorsi AS C, tableUni_ha_C AS U " +
            "WHERE C.id == U.id_corso AND U.id_universita == :idUniversita")
    public abstract List<Corsi> getByIdOfUniverita(Long idUniversita);
/*------------------------------------------------------------------------*/
    @Query( "SELECT C.* " +
            "FROM tableCorsi AS C, tableUni_ha_C AS U " +
            "WHERE C.id == U.id_corso AND U.id_universita == :idUniversita")
    public abstract LiveData<List<Corsi>> getByIdOfUniverita_Synchronized(Long idUniversita);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
    @Query( "SELECT C.* " +
            "FROM tableCorsi AS C, tableUtenti AS U " +
            "WHERE C.id == U.id_corso AND U.id == :idUtente")
    public abstract List<Corsi> getByIdOfUtente(Long idUtente);
/*------------------------------------------------------------------------*/
    @Query( "SELECT C.* " +
            "FROM tableCorsi AS C, tableUtenti AS U " +
            "WHERE C.id == U.id_corso AND U.id == :idUtente")
    public abstract LiveData<List<Corsi>> getByIdOfUtente_Synchronized(Long idUtente);
/*================================================================================================*/
/*================================================================================================*/
/*                         EXIST                                                                  */
/*================================================================================================*/
    @Transaction
    public long exist(Corsi corsi){
        return this.exist(corsi.getNome(),corsi.getSigla());
    }
/*------------------------------------------------------------------------------------------------*/
    @Query("SELECT C.id FROM tableCorsi AS C WHERE C.nome == :nome AND C.sigla == :sigla")
    protected abstract long exist(String nome,String sigla);
/*================================================================================================*/
/*################################################################################################*/
}
