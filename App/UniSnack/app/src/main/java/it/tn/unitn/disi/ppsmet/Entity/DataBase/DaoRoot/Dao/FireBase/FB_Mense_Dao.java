package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.google.firebase.database.DatabaseReference;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Mense;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.IscrizioniUtente;
import it.tn.unitn.disi.ppsmet.Entity.Mense;


public class FB_Mense_Dao extends ConnesioneAFirebase<Mense> {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                      */
/*================================================================================================*/
    public LiveData<List<Mense>> getAll_OneTime(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_MENSE);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Mense.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Mense>> getAll(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_MENSE);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Mense.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Mense>> getAll_Synchronized(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_MENSE);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Mense.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    public LiveData<Mense> getByPrimaryKey_OneTime(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_MENSE)
                .child(id.toString());
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Mense.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Mense> getByPrimaryKey(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_MENSE)
                .child(id.toString());
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Mense.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Mense> getByPrimaryKey_Synchronized(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_MENSE)
                .child(id.toString());
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Mense.Deserializer_Object());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID OF UNIVERSITA'                                        */
/*================================================================================================*/
    public LiveData<List<Mense>> getByIdOfUniverita_OneTime(@NonNull Long idUniversita){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_MENSE_DEL_UNIVERSITA)
                .child(idUniversita.toString())
                .child(L_MENSE);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Mense.Deserializer_List(idUniversita));
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Mense>> getByIdOfUniverita(@NonNull Long idUniversita){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_MENSE_DEL_UNIVERSITA)
                .child(idUniversita.toString())
                .child(L_MENSE);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Mense.Deserializer_List(idUniversita));
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Mense>> getByIdOfUniverita_Synchronized(@NonNull Long idUniversita){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_MENSE_DEL_UNIVERSITA)
                .child(idUniversita.toString())
                .child(L_MENSE);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Mense.Deserializer_List(idUniversita));
    }
/*================================================================================================*/
/*################################################################################################*/
}
