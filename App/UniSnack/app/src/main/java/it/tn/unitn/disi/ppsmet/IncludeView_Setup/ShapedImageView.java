package it.tn.unitn.disi.ppsmet.IncludeView_Setup;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

public class ShapedImageView extends AppCompatImageView {
    private static final String TAG_ERR = "ERROR_MASK_IMAGE";
/*################################################################################################*/
/*================================================================================================*/
/*                                  COSTRUTTORE                                                   */
/*================================================================================================*/
    public ShapedImageView(Context context) {
        super(context);
    }
/*------------------------------------------------------------------------*/
    public ShapedImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
/*------------------------------------------------------------------------*/
    public ShapedImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA IMMAGINE BITMAP                                       */
/*================================================================================================*/
    public boolean setImageBitmapIfNotNUll(Bitmap bm) {
        boolean ris = false;
        if(bm!=null){
            super.setImageBitmap(bm);
            ris = true;
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA FORMA QUADREATO                                       */
/*================================================================================================*/
    public void setSquare(){
        Bitmap imgBitmap = ((BitmapDrawable)super.getDrawable()).getBitmap();
        super.setImageBitmap(trasformaBitmapQuadrata(imgBitmap));
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA FORMA CIRCOLARE DEL QUADRATO CENTRALE                 */
/*================================================================================================*/
    public void setCircular(){
        Bitmap imgBitmap = ((BitmapDrawable)super.getDrawable()).getBitmap();
        super.setImageDrawable(trasformaBitmapCircolare(imgBitmap));
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA FORMA CIRCOLARE DEL QUADRATO CENTRALE                 */
/*================================================================================================*/
    public void setCircularCenter(){
        Bitmap imgBitmap = ((BitmapDrawable)super.getDrawable()).getBitmap();
        super.setImageDrawable(trasformaBitmapCircolareDalQuafratoCentrale(imgBitmap));
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  TRASFORMA BITMAP NEL QUADRATO PIU'GRANDE POSSIBILE AL CENTRO  */
/*================================================================================================*/
    private Bitmap trasformaBitmapQuadrata(Bitmap bitmap){
        Bitmap risBitmap;
        if (bitmap.getWidth() >= bitmap.getHeight()){
            risBitmap = Bitmap.createBitmap(
                    bitmap,
                    bitmap.getWidth()/2 - bitmap.getHeight()/2,
                    0,
                    bitmap.getHeight(),
                    bitmap.getHeight() );
        }else{
            risBitmap = Bitmap.createBitmap(
                    bitmap,
                    0,
                    bitmap.getHeight()/2 - bitmap.getWidth()/2,
                    bitmap.getWidth(),
                    bitmap.getWidth());
        }
        return risBitmap;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  TRASFORMA BITMAP IN CIRCOLARE                                 */
/*================================================================================================*/
    private RoundedBitmapDrawable trasformaBitmapCircolare(Bitmap bitmap){
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(),bitmap);
        roundedBitmapDrawable.setCircular(true);
        return roundedBitmapDrawable;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  TRASFORMA BITMAP CIRCOLARE DEL QUADRATO CENTRALE              */
/*================================================================================================*/
    private RoundedBitmapDrawable trasformaBitmapCircolareDalQuafratoCentrale(Bitmap bitmap){
        Bitmap quadratoBitmap = trasformaBitmapQuadrata(bitmap);
        return trasformaBitmapCircolare(quadratoBitmap);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  IMASCHERE                                                     */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA IMAGGINE A QUI DARE LA FORMA DI QUELLA PRINCIPALE     */
/*================================================================================================*/
    public void setImageOnShapeMask(int idImage){
        Bitmap image = getBitmap(getResources().getDrawable(idImage,null));
        setImageOnShapeMask(image);
    }
/*------------------------------------------------------------------------*/
    public void setImageOnShapeMask(Bitmap image){
        try {
            //Bitmap image =  BitmapFactory.decodeResource(getResources(),idImage);
            Bitmap mask  = getBitmap(super.getDrawable());

            if(image!=null){
                //Dimensione Maschera Origineli
                int im_Width  = image.getWidth();
                int im_Height = image.getHeight();

                //Dimensione Maschera Rappresentata Nella ImageView
                int sacale_im_Width  = super.getDrawable().getIntrinsicWidth();
                int sacale_im_Height = super.getDrawable().getIntrinsicHeight();


                if((mask!=null)){
                    //Creazione delle machera
                    Bitmap ris        = Bitmap.createBitmap(im_Width,im_Height,Bitmap.Config.ARGB_8888);
                    Bitmap maskBitmap = Bitmap.createScaledBitmap(mask,im_Width,im_Height,true);

                    //Reppresentazione delle immagione da creare
                    Canvas canvas = new Canvas(ris);
                    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));

                    //Inserimento dei dati nel immagine creata
                    canvas.drawBitmap(image,0,0,null);
                    canvas.drawBitmap(maskBitmap,0,0,paint);

                    paint.setXfermode(null);
                    paint.setStyle(Paint.Style.STROKE);

                    //Rimposta la dimaensione della nuova immagione uguale alla paschera precedente.
                    Bitmap resizeRis = ThumbnailUtils.extractThumbnail(ris, sacale_im_Width, sacale_im_Height);;
                    super.setImageBitmap(resizeRis);
                }
            }
        }catch (OutOfMemoryError e){
            Log.d(TAG_ERR,e.getMessage(),e);
        }

    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  ESTRAI BITMAP DA UN OGGETO DRAWLABLE (quindi anche SVG)       */
/*================================================================================================*/
    /**
     * Questo metodo permette di ottenere un oggetto Bitmap da un qualsiasi oggetto Drawable così che
     * anche le imagini provenienti da oggetti SVG.
     */
    private Bitmap getBitmap(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                                            drawable.getIntrinsicHeight(),
                                            Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }
/*================================================================================================*/
/*################################################################################################*/

}
