package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List_In_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;

public class Deserializer_Pietanze implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto Pietanze.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String LOG_TAG = "Pietanze_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "Pietanze" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "Pietanze".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public static Pietanze estraiDaDataSnapshot(DataSnapshot dataSnapshot){
        Pietanze pietanze = null;
        if(dataSnapshot.exists()){
            try{
                //Campi not null
                TipoPietanza tipoPietanza =Deserializer_TipoPietanza.getAttributeOf(dataSnapshot,L_TIPO_PIETANZA);
                if(tipoPietanza!=null){
                    Long id = null;
                    if (dataSnapshot.child(P_KEY).exists()){
                        id = dataSnapshot.child(P_KEY).getValue(Long.class);
                    }else{
                        id = Long.parseLong( dataSnapshot.getKey());
                    }
                    String nome = dataSnapshot.child(P_NOME).getValue(String.class);
                    //Verifica campi not null
                    if((id!=null) && (nome!=null) && (tipoPietanza!=null) && (tipoPietanza.getId()!=null)){
                        pietanze = new Pietanze(id,nome,tipoPietanza);
                        pietanze.setCalorie(           dataSnapshot.child(P_CALORIE).getValue(Double.class));
                        pietanze.setCarboidrati(       dataSnapshot.child(P_CARBOIDRATI).getValue(Double.class));
                        pietanze.setGrassi(            dataSnapshot.child(P_GRASSI).getValue(Double.class));
                        pietanze.setProteine(          dataSnapshot.child(P_PROTEINE).getValue(Double.class));
                        pietanze.setUrlImmagine(       dataSnapshot.child(P_URL_IMMAGINE).getValue(String.class));
                    }
                }
            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
                pietanze = null;
            }
        }
        return pietanze;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         GET LIST OF OBJECT                                                     */
/*================================================================================================*/
    public static List<Pietanze> estraiListaDaDataSnapshot(DataSnapshot dataSnapshot){
        List<Pietanze> ris=new LinkedList<>();
        if(dataSnapshot.exists()){
            try{
                if (dataSnapshot.child(L_PIETANZE).exists()){
                    Iterator<DataSnapshot> iterator = dataSnapshot.child(L_PASTO_DEL_MESE).getChildren().iterator();
                    while (iterator.hasNext()){
                        DataSnapshot pietanzeDataSnapshot= iterator.next();
                        Pietanze pietanze=estraiDaDataSnapshot(pietanzeDataSnapshot);
                        if(pietanze!=null){
                            ris.add(pietanze);
                        }
                    }
                }
            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
            }
        }
        return ris;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         GET AN ACTRIBUTE OBJECT FOR AN OTHER                                   */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET AN ACTRIBUTE                                                       */
/*------------------------------------------------------------------------------------------------*/
    /**
     * Qusrto metodo restituisce, se esiste, un singolo ogetto "Pietanze" da un attributo di un altro
     * oggetto attraverso una sottoparte dell'oggetto "DataSnapshot".
     * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
     *                     informazioni utili per creare un oggetto di tipo "Pietanze".
     * @return  Il riutato è il primo oggetto, valido se ce ne fossero più d'uno ritorna il primo trovato
     *          mentre se non ce ne sono ritorna null.
     */
    public static Pietanze getAttributeOf(DataSnapshot dataSnapshot,String nomeAttributo){
        Pietanze pietanze = null;
        DataSnapshot pietanzeDataSnapshot = dataSnapshot.child(nomeAttributo);
        if(pietanzeDataSnapshot.exists()){
            Iterator<DataSnapshot> iterator =pietanzeDataSnapshot.getChildren().iterator();
           while ( (iterator.hasNext()) && (pietanze==null) ){//Restituisce il primo oggetto valido
                pietanze = estraiDaDataSnapshot(iterator.next());
           }
        }
        return pietanze;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, Pietanze>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Object extends GenericDeserializer_Object<Pietanze> {
        @Override
        public Pietanze estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Pietanze.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<Pietanze>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List<Pietanze> {
        @Override
        public Pietanze estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Pietanze.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<Pietanze>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List_In_List extends GenericDeserializer_List_In_List<Pietanze> {
        @Override
        public List<Pietanze> estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Pietanze.estraiListaDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
