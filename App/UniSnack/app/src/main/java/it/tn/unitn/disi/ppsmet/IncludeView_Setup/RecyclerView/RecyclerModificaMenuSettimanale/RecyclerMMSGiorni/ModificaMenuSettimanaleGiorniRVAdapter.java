package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerModificaMenuSettimanale.RecyclerMMSGiorni;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface.OnFragmentSelezioneGiornoETipoPastoEventListener;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ModificaMenuSettimanaleGiorniRowView;
import it.tn.unitn.disi.ppsmet.R;

public class ModificaMenuSettimanaleGiorniRVAdapter extends RecyclerView.Adapter<ModificaMenuSettimanaleGiorniRVAdapter.MyViewHoder> {
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE INTERNA                                                    */
/*################################################################################################*/
    /**
     * Questa classe è quella quella che viene usata per permettere alla RecyclingView Adapter di definire
     * il layout singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public ViewGroup v_mainLayout;
        public ModificaMenuSettimanaleGiorniRowView v_elemento;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout = itemView.findViewById(R.id.cly_RowElemento);
            v_elemento   = ModificaMenuSettimanaleGiorniRowView.newInstance(v_mainLayout,R.id.include_Giorno);
        }
/*================================================================================================*/
    }
/*################################################################################################*/


/*################################################################################################*/
/*                              CLASSE PRINCIPALE ModificaMenuSettimanaleGiorniRVAdapter          */
/*################################################################################################*/
    private Context comtext=null;
    private List<Date> listaDateOrdinazioni = null;
    private List<TipoPasto> listaTipoPasto = null;
    /**
     * Questo parametro contiene le ordinazioni di un singolo giorno che però sono associate alla
     * chieave uguale al id del TipoPasto di cui fanno parte.
     * Così si identifica rapidamente la presenaz o l'assenza di una ordinazione pregressa.
     */
    private HashMap<String, HashMap<Long, Ordinazioni>> ordinazioniNellaSettimana=new HashMap<>();
/*------------------------------------------------------------------------------------------------*/
    private OnFragmentSelezioneGiornoETipoPastoEventListener listener =null;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public ModificaMenuSettimanaleGiorniRVAdapter(Context comtext,
                                                  List<Date> listaDateOrdinazioni,
                                                  List<TipoPasto> listaTipoPasto,
                                                  List<Ordinazioni> listaOrdinazioni,
                                                  OnFragmentSelezioneGiornoETipoPastoEventListener listener) {
        this.comtext = comtext;
        this.listaDateOrdinazioni = listaDateOrdinazioni;
        this.listaTipoPasto = listaTipoPasto;
        this.ordinazioniNellaSettimana = generaStrutturaOrdinazioni(listaOrdinazioni);
        this.listener = listener;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              CERAZIONE DEL SINGOLO ELEMETOTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(comtext);
        View bloccoElemeto = inflater.inflate(R.layout.row_modifica_menu_settimanale_giorno, parent,false);
        MyViewHoder myViewHoder = new MyViewHoder(bloccoElemeto);
        return myViewHoder;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              INSERIRE L'ELEMETO NEL LAYOUT                                     */
/*================================================================================================*/
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore ModificaMenuSettimanaleGiorniRVAdapter.
     */
    @Override
    public void onBindViewHolder(@NonNull MyViewHoder holder, int position) {
        final  Date elemento  =listaDateOrdinazioni.get(position);
        //Seleziona il tipo di layout
        inserisciOrdinazioniNelGiornoLayout(holder,elemento);
    }
/*================================================================================================*/
/*                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE                  */
/*================================================================================================*/
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaDateOrdinazioni.size();
    }
/*================================================================================================*/


/*================================================================================================*/
/*                              INSERISCI I DATI NEL LAYOUT                                       */
/*================================================================================================*/
    /**
     * Questo metodo si occupa di inserire i dati della pietanza che è quello attuale  nel layout del
     * singolo elemeto.
     */
    private void inserisciOrdinazioniNelGiornoLayout(@NonNull MyViewHoder holder, Date date){
        holder.v_elemento.setData(date);
        HashMap<Long, Ordinazioni> hashMapDeiOrdiniDelGiorno = null;
        if(date!=null){
            String key = generaKeyFromDate(date);
            hashMapDeiOrdiniDelGiorno = this.ordinazioniNellaSettimana.get(key);
        }
        //Si attiva solo se hashMapDeiOrdiniDelGiorno è diverso da null
        holder.v_elemento.setStatoAttivo(hashMapDeiOrdiniDelGiorno!=null);
        holder.v_elemento.setListTipoPasto(listaTipoPasto,hashMapDeiOrdiniDelGiorno,this.listener);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                              METODI D'APPOGGIO AL ADAPTER                                      */
/*################################################################################################*/
/*================================================================================================*/
/*                              CEAZIONE DEL HASMAP DEI ORDIN                                     */
/*================================================================================================*/
    /**
     * Questo metodo trasforma una lista di ordinazioni in modo tale che ogni item dell'adapter
     * possa ricevere le eventuali ordinazioni presenti ne giorno in questione e possa cambiare la
     * visualizzazione di se sresso e dei suo recycleview interno.
     * @param listaOrdinazioni
     * @return
     */
    private HashMap<String, HashMap<Long, Ordinazioni>> generaStrutturaOrdinazioni(List<Ordinazioni> listaOrdinazioni){
        HashMap<String, HashMap<Long, Ordinazioni>> ordinazioniNellaSettimana = new HashMap<>();

        if(listaOrdinazioni!=null){

            Iterator<Ordinazioni> iterator = listaOrdinazioni.iterator();
            while (iterator.hasNext()){
                //Prndo un ordinazione
                Ordinazioni ordinazioni = iterator.next();
                if((ordinazioni!=null) && (ordinazioni.getGiorno()!=null) && (ordinazioni.getId_tipoPasto()!=null)){
                    //Genero la chiave unica per le ordinazioni dello stesso giorno
                    String key = generaKeyFromDate(ordinazioni.getGiorno());
                    if(key!=null){
                        //Genero un hashmap che conterra tutte le ordinazioni dello stesso giorno
                        HashMap<Long, Ordinazioni> ordinazioniDelSingoloGiorno = new HashMap<>();

                        if(ordinazioniNellaSettimana.get(key)!=null){
                            //Aggiungo al Hashmap appena cerato l'hashmap che contiene le coppie
                            //chiave valore, dove al chiave è l'id tipo pasto e il valore l'ordinazione
                            ordinazioniDelSingoloGiorno.putAll(ordinazioniNellaSettimana.get(key));
                        }
                        //Aggiungo una nuova ordinazione hashmap più interno
                        ordinazioniDelSingoloGiorno.put(ordinazioni.getId_tipoPasto(),ordinazioni);
                        //Aggiungo l'hashmap più interno a quello più esterno.
                        ordinazioniNellaSettimana.put(key,ordinazioniDelSingoloGiorno);
                    }

                }
            }

        }

        return ordinazioniNellaSettimana;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                              GENERA KEY PER HASHMAP                                            */
/*================================================================================================*/
    /**
     * Questo metodo permete ad una data di generare un identificatore univoco giornagliero, anche
     * se le ore e i minuti so diversi.
     * @param date
     * @return
     */
    private String generaKeyFromDate(Date date){
        String ris = null;
        if(date!=null){
            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
            ris = fmt.format(date);
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
}