package it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom;

import androidx.annotation.NonNull;
import androidx.room.Embedded;
import androidx.room.Ignore;

import java.time.DayOfWeek;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.M_fornisce_P;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

public class PietanzeDellaMense extends M_fornisce_P {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @Embedded(prefix = "TM_")
    public TipoMenu tipoMenu;
    @NonNull
    @Embedded(prefix = "M_")
    public Mense    mense;
    @NonNull
    @Embedded(prefix = "P_")
    public Pietanze pietanze;
//##################################################################################################
//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================

    public PietanzeDellaMense(@NonNull Integer mese,
                              @NonNull Integer anno,
                              @NonNull Integer giornoSettimana,
                              @NonNull TipoMenu tipoMenu,
                              @NonNull Mense mense,
                              @NonNull Pietanze pietanze) {
        super(mense.getId(), pietanze.getId(), mese, anno,giornoSettimana, tipoMenu.getId());
        this.tipoMenu = tipoMenu;
        this.mense = mense;
        this.pietanze = pietanze;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    @NonNull
    public TipoMenu getTipoMenu() {
        return tipoMenu;
    }
//--------------------------------------
    public void setTipoMenu(@NonNull TipoMenu tipoMenu) {
        this.tipoMenu = tipoMenu;
        super.setId_tipoMenu(tipoMenu.getId());
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Mense getMense() {
        return mense;
    }
//--------------------------------------
    public void setMense(@NonNull Mense mense) {
        this.mense = mense;
        super.setId_mensa(mense.getId());
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Pietanze getPietanze() {
        return pietanze;
    }
//--------------------------------------
    public void setPietanze(@NonNull Pietanze pietanze) {
        this.pietanze = pietanze;
        super.setId_pietanza(pietanze.getId());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public void setId_mensa(@NonNull Long id_mensa) {
        if(id_mensa==this.mense.getId()){
            super.setId_mensa(id_mensa);
        }
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public void setId_pietanza(@NonNull Long id_pietanza) {
        if(id_pietanza==this.pietanze.getId()){
            super.setId_pietanza(id_pietanza);
        }
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public void setId_tipoMenu(@NonNull Long id_tipoMenu) {
        if(id_tipoMenu==this.tipoMenu.getId()){
            super.setId_tipoMenu(id_tipoMenu);
        }
    }
//==================================================================================================
}
