package it.tn.unitn.disi.ppsmet.IncludeView_Setup;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import it.tn.unitn.disi.ppsmet.R;

public class TitoloSopraContenutoView {
/*################################################################################################*/
    private TextView txtTitolo = null;
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    /**
     * Questo metodo permette di costruire e definire il layout impostato
     */
    private TitoloSopraContenutoView(TextView txtTitolo) {
        this.txtTitolo = txtTitolo;
    }
/*------------------------------------------------------------------------*/
    /**
     * Questo è il costuttore di quando l'oggetto viene impostato in una pagina o in una sua parte.
     * @param rootLayout
     * @param idLayoutIncluded
     * @return
     */
    public static TitoloSopraContenutoView newInstance(ViewGroup rootLayout, int idLayoutIncluded) {
        TitoloSopraContenutoView ris = null;
        if(rootLayout!=null){
            ViewGroup contenitore = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(contenitore!=null){
                TextView txtTitolo = (TextView) contenitore.findViewById(R.id.txt_Titolo);
                if(txtTitolo!=null){
                    ris = new TitoloSopraContenutoView(txtTitolo);
                }
            }
        }
        return ris;
    }
/*------------------------------------------------------------------------*/
    /**
     * Questo è il costuttore di quando l'oggetto viene impostato in una pagina o in una sua parte.
     * @param rootLayout
     * @param idLayoutIncluded
     * @return
     */
    public static TitoloSopraContenutoView newInstance(ViewGroup rootLayout, int idLayoutIncluded,int idTextTitolo) {
        TitoloSopraContenutoView ris = newInstance(rootLayout,idLayoutIncluded);
        if(ris!=null){
            ris.setText(idTextTitolo);
        }
        return ris;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    /**
     * Questo metodo permette di impostare e cambiare il titolo del gurppo di layout, partendo da
     * identificatere della risosssa della stringa.
     * @param idTextTitolo
     */
    public void setText(int idTextTitolo){
        txtTitolo.setText(idTextTitolo);
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo permette di impostare e cambiare il titolo del gurppo di layout, partendo dalla
     * stringa.
     * @param textTitolo
     */
    public void setText(String textTitolo){
        txtTitolo.setText(textTitolo);
    }
/*================================================================================================*/
/*################################################################################################*/
}
