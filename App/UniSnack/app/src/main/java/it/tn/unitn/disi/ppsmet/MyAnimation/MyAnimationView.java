package it.tn.unitn.disi.ppsmet.MyAnimation;



import android.content.Context;
import android.graphics.drawable.Animatable2;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.vectordrawable.graphics.drawable.Animatable2Compat;
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat;

import it.tn.unitn.disi.ppsmet.MyAnimation.RipetiAnimazione.RipetiAnimazione;

import java.util.LinkedList;
import java.util.List;


public class MyAnimationView extends AppCompatImageView {
    public static final Integer RIPETIZIONE_INFINITA    = -1;
    public static final Integer ULTIMA_ANIMAZIONE =  -2;
    public static final Integer RIPETIZIONE_DI_DEFAUULT =  0;
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    //Versioni di animazioni
    protected LinkedList<Drawable>         listaAnimazione  = new LinkedList<>();
    //Animazione
    protected LinkedList<RipetiAnimazione> listaRipetizioni = new LinkedList<>();
//##################################################################################################

//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    public MyAnimationView(@NonNull Context context) {
        super(context);
        this.listaAnimazione.add(this.getDrawable());
        this.listaRipetizioni.add(new RipetiAnimazione(RIPETIZIONE_DI_DEFAUULT));
        startRipetizione();
    }
//--------------------------------------------------------------------------------------------------
    public MyAnimationView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.listaAnimazione.add(this.getDrawable());
        this.listaRipetizioni.add(new RipetiAnimazione(RIPETIZIONE_DI_DEFAUULT));
        startRipetizione();
    }
//--------------------------------------------------------------------------------------------------
    public MyAnimationView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.listaAnimazione.add(this.getDrawable());
        this.listaRipetizioni.add(new RipetiAnimazione(RIPETIZIONE_DI_DEFAUULT));
        startRipetizione();
    }
//==================================================================================================

//==================================================================================================
//                                      START RIPETIZIONE
//==================================================================================================
    protected void startRipetizione(){
           if((!this.listaAnimazione.isEmpty()) && (!this.listaRipetizioni.isEmpty())){
               Drawable drawable = this.listaAnimazione.getLast();
               RipetiAnimazione ripetiAnimazione = this.listaRipetizioni.getLast();
               if(drawable instanceof AnimatedVectorDrawable){
                   ((AnimatedVectorDrawable)drawable).registerAnimationCallback(ripetiAnimazione.getRipetizione(this));
               }
               if(drawable instanceof AnimatedVectorDrawableCompat){
                   ((AnimatedVectorDrawableCompat)drawable).registerAnimationCallback(ripetiAnimazione.getRipetizioneCompat(this));
               }
               if(ripetiAnimazione.getNumRipetizioni()!=0){
                   start();
               }
           }
    }
//--------------------------------------------------------------------------------------------------
    public void start(){
        if(!this.listaAnimazione.isEmpty()){
            Drawable drawable =this.listaAnimazione.getLast();
            if(drawable instanceof AnimatedVectorDrawable){
                ((AnimatedVectorDrawable)drawable).start();
            }
            if(drawable instanceof AnimatedVectorDrawableCompat){
                ((AnimatedVectorDrawableCompat)drawable).start();
            }
        }
    }
//==================================================================================================

//==================================================================================================
//                                      STOP RIPETIZIONE
//==================================================================================================
    private synchronized void stop(){
        if(!this.listaAnimazione.isEmpty()){
            Drawable drawable =this.listaAnimazione.getLast();
            if(drawable instanceof AnimatedVectorDrawable){
                ((AnimatedVectorDrawable)drawable).stop();
            }
            if(drawable instanceof AnimatedVectorDrawableCompat){
                ((AnimatedVectorDrawableCompat)drawable).stop();
            }
        }
    }
//==================================================================================================

//==================================================================================================
//                                      FINE RIPETIZIONE
//==================================================================================================
    public synchronized void fineRipetizione(){
        try {
            if((this.listaAnimazione.size()>=2) && (this.listaRipetizioni.size()>=2)){
                stop();
                this.listaAnimazione.removeLast();
                Integer ripetizioni = this.listaRipetizioni.removeLast().getNumRipetizioni();
                if((ripetizioni!=ULTIMA_ANIMAZIONE) && (!this.listaAnimazione.isEmpty()) && (!this.listaRipetizioni.isEmpty()) ){
                    Drawable drawable = this.listaAnimazione.getLast();
                    this.setImageDrawable(drawable);
                    startRipetizione();
                }
            }
        }catch (Exception e){
        }

    }
//==================================================================================================
//==================================================================================================
//                                      SET NEW ANIMZAIONE RIPETUTA
//==================================================================================================
    public void  setNewAnimazione(Integer idRes){
        setNewAnimazione(getResources().getDrawable(idRes,null));
    }
//--------------------------------------------------------------------------------------------------
    public void  setNewAnimazione(Drawable drawable){
        setNewAnimazioneRipetuta(drawable,1);
    }
//--------------------------------------------------------------------------------------------------
    public void  setNewAnimazioneRipetuta(Integer idRes,Integer numRipetizione){
        setNewAnimazioneRipetuta(getResources().getDrawable(idRes,null),numRipetizione);
    }
//--------------------------------------------------------------------------------------------------
    public void  setNewAnimazioneRipetuta(Drawable drawable,Integer numRipetizione){
        if(numRipetizione==RIPETIZIONE_INFINITA){
            this.listaAnimazione.removeAll(this.listaAnimazione);
            this.listaRipetizioni.removeAll(this.listaRipetizioni);
        }
        stop();
        this.listaAnimazione.add(drawable);
        this.listaRipetizioni.add(new RipetiAnimazione(numRipetizione));
        this.setImageDrawable(drawable);
        startRipetizione();
    }
//==================================================================================================
//==================================================================================================
//                                      FERMA ANIMZAZIONI PRIMA DI DISTRUGGERE L'OGGETTO
//==================================================================================================
    @Override
    protected void finalize() throws Throwable {
        this.stop();
        super.finalize();
    }
//==================================================================================================
}
