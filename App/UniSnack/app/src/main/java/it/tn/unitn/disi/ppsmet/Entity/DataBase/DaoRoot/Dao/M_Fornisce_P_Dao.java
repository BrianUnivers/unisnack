package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_MFornisceP;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.M_fornisce_P;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.P_contiene_A;
import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;

@Dao
public interface M_Fornisce_P_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long insert(M_fornisce_P m_fornisce_p);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Query("DELETE FROM tableM_fornisce_P")
    public void deleteAll();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE By ID                                                    */
/*================================================================================================*/
    @Query("DELETE FROM tableM_fornisce_P " +
            "WHERE anno == :anno AND mese == :mese AND giornoSettimana == :giornoSettimana AND " +
            "id_mensa == :idMensa AND id_pietanza == :idPietanza AND id_tipoMenu == :idTipoMenu ")
    public abstract void delete(Integer anno,Integer mese,Integer giornoSettimana,Long idMensa,Long idPietanza,Long idTipoMenu);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = M_fornisce_P.class,
            onConflict = OnConflictStrategy.ABORT)
    public void update(M_fornisce_P m_fornisce_p);
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                          METODO GET MFornisceP by ID MENSA AND TIPO MENU'                      */
/*================================================================================================*/
    @Query("SELECT * FROM tableM_fornisce_P " +
            "WHERE id_mensa == :idMensa AND id_tipoMenu == :idTipoMenu ")
    public List<M_fornisce_P> getMForniscePByIdOfMensa_TipoMenu(@NonNull Long idMensa,
                                                                @NonNull Long idTipoMenu);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableM_fornisce_P " +
            "WHERE id_mensa == :idMensa AND id_tipoMenu == :idTipoMenu ")
    public LiveData<List<M_fornisce_P>> getMForniscePByIdOfMensa_TipoMenu_Synchronized(@NonNull Long idMensa,
                                                                                       @NonNull Long idTipoMenu);
/*================================================================================================*/
/*################################################################################################*/
}
