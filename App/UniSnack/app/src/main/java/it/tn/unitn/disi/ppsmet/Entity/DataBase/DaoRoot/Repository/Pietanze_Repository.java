package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Query;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Allergeni_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Pietanze_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Pietanze_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.PastoDelMese;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;

public class Pietanze_Repository extends Pietanze_ThreadAggiornamento {
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA PIETANZE (e le sue derivazioni          */
/*                         seguendo un ordine topologico per garantire gli aggiornamenti)         */
/*                         ATTRAVERO IL DATABASE LOCALE (room)                                    */
/*################################################################################################*/
    private LiveData<List<Pietanze>> mAllPietanze;
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Pietanze_Repository(Application application) {
        super(application);
        //AllPietanze get any time the same result
        mAllPietanze = pietanzeDao.getAll_Synchronized();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERT                                                                 */
/*================================================================================================*/
    /*public Future<Long> insert(Pietanze pietanze){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                    @Override
                    public Long call() throws Exception {
                        return pietanzeDao.insert(pietanze);
                    }
                });
    }
/*------------------------------------------------------------------------*/
   /* public Future<Long> insertPietazaAlMenuDellaMensa(Pietanze pietanze,
                                                      Mense mense,
                                                      TipoMenu tipoMenu,
                                                      Integer anno,
                                                      Integer mese,
                                                      Integer giornoSettimana) {
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                    @Override
                    public Long call() throws Exception {
                        return pietanzeDao.insertPietazaAlMenuDellaMensa(pietanze,mense,tipoMenu,anno,mese,giornoSettimana);
                    }
                });
    }
/*------------------------------------------------------------------------*/
  /*  public Future<Long>  insertAssociazioneIngrediente(Pietanze pietanze, Ingredienti ingredienti) {
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                    @Override
                    public Long call() throws Exception {
                        return pietanzeDao.insertAssociazioneIngrediente(pietanze,ingredienti);
                    }
                });
    }
/*------------------------------------------------------------------------*/
 /*   public Future<List<Long>> insertAssociazioneIngrediente(Pietanze pietanze, List<Ingredienti> ingredienti) {
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<List<Long>>(){
                    @Override
                    public List<Long> call() throws Exception {
                        return pietanzeDao.insertAssociazioneIngrediente(pietanze,ingredienti);
                    }
                });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
  /*  public void deleteAll(){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            pietanzeDao.deleteAll();
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
   /* public void update(Pietanze pietanze){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            pietanzeDao.update(pietanze);
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
  /*  public LiveData<List<Pietanze>> getAll(){
        return mAllPietanze;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
   /* public LiveData<Pietanze> getByPrimaryKey(@NonNull Long id){
        return pietanzeDao.getByPrimaryKey_Synchronized(id);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID TIPO PIETANZA                                         */
/*================================================================================================*/
   /* public LiveData<List<Pietanze>> getByIdOfTipoPietanza(@NonNull TipoPietanza tipoPietanza){
        return pietanzeDao.getByIdOfTipoPietanza_Synchronized(tipoPietanza.getId());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID MENSA AND TIPO MENU'                                  */
/*================================================================================================*/
   /* public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu(@NonNull Mense mense, @NonNull TipoMenu tipoMenu){
        return pietanzeDao.getPietanzeOfMenseByIdOfMensa_TipoMenu_Synchronized(mense.getId(),tipoMenu.getId());
    }
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU'                                 */
/*                          SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR                         */
/*================================================================================================*/
   /* public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_SpecificMoment(@NonNull Mense mense,
                                                                           @NonNull TipoMenu tipoMenu,
                                                                           Integer anno,
                                                                           Integer mese,
                                                                           Integer giornoSettimana){
        return pietanzeDao.getByIdOfMensa_TipoMenu_SpecificMoment_Synchronized( mense.getId(),
                                                                                tipoMenu.getId(),
                                                                                anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------------------------------*/
 /*   public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment(@NonNull Mense mense,
                                                                                                    @NonNull TipoMenu tipoMenu,
                                                                                                    Integer anno,
                                                                                                    Integer mese,
                                                                                                    Integer giornoSettimana){
        return pietanzeDao.getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment_Synchronized(  mense.getId(),
                                                                                                tipoMenu.getId(),
                                                                                                anno, mese, giornoSettimana);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU' SPECIFIC DAY                    */
/*================================================================================================*/
/*    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_SpecificDay(  @NonNull Mense mense,
                                                                          @NonNull TipoMenu tipoMenu,
                                                                          @NonNull Date date){
        return pietanzeDao.getByIdOfMensa_TipoMenu_SpecificDay_Synchronized(mense.getId(),tipoMenu.getId(),date);
    }
/*------------------------------------------------------------------------------------------------*/
  /*  public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificDay(@NonNull Mense mense,
                                                                                                 @NonNull TipoMenu tipoMenu,
                                                                                                 @NonNull Date date){
        return pietanzeDao.getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificDay_Synchronized(mense.getId(),tipoMenu.getId(),date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU' AND TIPO PIETANZA               */
/*                          SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR                         */
/*================================================================================================*/
  /*  public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment(Long idMensa,
                                                                                       Long idTipoMenu,
                                                                                       Long idTipoPietanza,
                                                                                       Integer anno,
                                                                                       Integer mese,
                                                                                       Integer giornoSettimana){
        return pietanzeDao.getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_Synchronized(idMensa,idTipoMenu,idTipoPietanza,
                                                                                            anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------------------------------*/
 /*   public  LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment(
                                                                                                Long idMensa,
                                                                                                Long idTipoMenu,
                                                                                                Long idTipoPietanza,
                                                                                                Integer anno,
                                                                                                Integer mese,
                                                                                                Integer giornoSettimana){
        return pietanzeDao.getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_Synchronized( idMensa,idTipoMenu,idTipoPietanza,
                                                                                                            anno,mese,giornoSettimana);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU' AND TIPO PIETANZA SPECIFIC DAY  */
/*================================================================================================*/
 /*   public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay(  @NonNull Long idMensa,
                                                                                       @NonNull Long idTipoMenu,
                                                                                       @NonNull Long idTipoPietanza,
                                                                                       @NonNull Date date){
        return pietanzeDao.getByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay_Synchronized(idMensa,idTipoMenu,idTipoPietanza, date);
    }
/*------------------------------------------------------------------------------------------------*/
/*   public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay( @NonNull Long idMensa,
                                                                                                               @NonNull Long idTipoMenu,
                                                                                                               @NonNull Long idTipoPietanza,
                                                                                                               @NonNull Date date){
        return pietanzeDao.getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay_Synchronized(idMensa,idTipoMenu,idTipoPietanza,date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR              */
/*================================================================================================*/
   /* public  LiveData<List<PietanzeDellaMense>> getPietanzeOfMense_SpecificMoment(Integer anno,
                                                                                 Integer mese,
                                                                                 Integer giornoSettimana){
        return pietanzeDao.getPietanzeOfMense_SpecificMoment_Synchronized(anno, mese, giornoSettimana);
    }
/*------------------------------------------------------------------------------------------------*/
/*                          METODO GET SPECIFIC DAY                                               */
/*------------------------------------------------------------------------------------------------*/
 /*   public LiveData<List<PietanzeDellaMense>> getPietanzeOfMense_SpecificDay( @NonNull Date date){
        return pietanzeDao.getPietanzeOfMense_SpecificDay_Synchronized(date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR  */
/*================================================================================================*/
   /* public  LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_SpecificMoment( @NonNull Mense mense,
                                                                                             @NonNull Integer anno,
                                                                                             @NonNull Integer mese,
                                                                                             @NonNull  Integer giornoSettimana){
        return pietanzeDao.getPietanzeOfMenseByIdOfMensa_SpecificMoment_Synchronized(mense.getId(),
                                                                                     anno, mese, giornoSettimana);
    }
/*------------------------------------------------------------------------------------------------*/
/*                          METODO GET by ID MENSA SPECIFIC DAY                                   */
/*------------------------------------------------------------------------------------------------*/
  /*  public LiveData<List<PietanzeDellaMense>>  getPietanzeOfMenseByIdOfMensa_SpecificDay( @NonNull Mense mense,
                                                                               @NonNull Date date){
        return pietanzeDao.getPietanzeOfMenseByIdOfMensa_SpecificDay_Synchronized(mense.getId(),date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by TIPO MENU' SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR*/
/*================================================================================================*/
   /* public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfTipoMenu_SpecificMoment( @NonNull TipoMenu tipoMenu,
                                                                                               @NonNull Integer anno,
                                                                                               @NonNull Integer mese,
                                                                                               @NonNull Integer giornoSettimana){
        return pietanzeDao.getPietanzeOfMenseByIdOfTipoMenu_SpecificMoment_Synchronized(tipoMenu.getId(),
                                                                                        anno, mese, giornoSettimana);
    }
/*------------------------------------------------------------------------------------------------*/
/*                          METODO GET by TIPO MENU' SPECIFIC DAY                                 */
/*------------------------------------------------------------------------------------------------*/
  /*  public LiveData<List<PietanzeDellaMense>>  getPietanzeOfMenseByIdOfTipoMenu_SpecificDay(@NonNull TipoMenu tipoMenu,
                                                                                            @NonNull Date date){
        return pietanzeDao.getPietanzeOfMenseByIdOfTipoMenu_SpecificDay_Synchronized(tipoMenu.getId(),date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID PASTO DEL MESE                                        */
/*================================================================================================*/
  /* public LiveData<List<Pietanze>> getByIdOfPastoDelMese(PastoDelMese pastoDelMese){
        return pietanzeDao.getByIdOfPastoDelMese_Synchronized(pastoDelMese);
    }
/*================================================================================================*/
/*################################################################################################*/
}
