package it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu;


import android.app.NotificationManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;


import com.google.android.material.navigation.NavigationView;

import java.util.List;

import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.MenuHeaderStaticFragment;
import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.MenuMenuDelleMenseFragment;
import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.MenuProfiloUtenteFragment;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;
import it.tn.unitn.disi.ppsmet.GestioneMyIntent.MyIntent;
import it.tn.unitn.disi.ppsmet.GestioneRicordaSessione.GestioneSessione;
import it.tn.unitn.disi.ppsmet.LoginActivity;
import it.tn.unitn.disi.ppsmet.MenuDelleMenseActivity;
import it.tn.unitn.disi.ppsmet.PastoDelMeseActivity;
import it.tn.unitn.disi.ppsmet.PersonalizzaMenuSettimanaleActivity;
import it.tn.unitn.disi.ppsmet.ProfiloUtenteActivity;
import it.tn.unitn.disi.ppsmet.R;
import it.tn.unitn.disi.ppsmet.StatisticheUtenteActivity;
import it.tn.unitn.disi.ppsmet.TesseraDigitaleActivity;
import it.tn.unitn.disi.ppsmet.ValutazionePastoActivity;

/*################################################################################################*/
/*                                  CLASSE PER LE ACTIVITY CON IL MENU' LATERALE PRINCIPLAE       */
/*################################################################################################*/
/**
 * Questa classe è stata crata estendendo la classe AppCompatActivity, per gestire in maniera univoca
 * e autonoma tutte le funzioni che devono essere eseguite per utilizzare il menù laterale principale
 * del app.
 *
 * @author Brian Emanuel
 */
public class AppCompatActivityWithMyPrimaryMenu extends AppCompatActivity {
    /**
     * L'attributo "toolbar" contiene l'oggetto che connterrà la toolbar principale dell'activity.
     */
    private Toolbar toolbar=null;
//Menu Laterale Principale
    /**
     * L'attributo "menuLateralePrincipale" contiene l'oggetto che connterrà il menù laterale principale.
     */
    private DrawerLayout menuLateralePrincipale;
    /**
     * L'attributo "myToggle" contiene l'oggetto che si occupa dell'operazione di toggle del menù laterale
     * principale, cioè si occupa della apertura (chiusura) del menù leterale se chiuso (aperto).
     */
    private ActionBarDrawerToggle myToggle;
    /**
     * L'attributo "menuLayout" contiene l'identificatore della serie di pulsanti da usare nella toolbar.
     */
    private int menuLayout;
/*================================================================================================*/
/*                                  CREAZIONE DELLA TOOLBAR E DEL MENU PRINCIPALE LATERALE        */
/*================================================================================================*/
    /**
     * Metodo chiamato nel onCreate delle activity che devono, definire il layout della toolbar, cioè definire
     * i pulsanti presenti su di esso, ed avere del menu laterale principale.
     * In oltre tale metodo è responsabile di avviare il servizzio di notifiche.
     *
     * @param menuLayout
     *      Questo peramento contiene l'identificativo della lista dei comandi che devono essere
     *      presenti nel menu laterale principale.
     */
    protected void initToolBar(int menuLayout){
        this.menuLayout=menuLayout;
        //Implemetazione della toolbar
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Implemetazione del menu laterale principale
        menuLateralePrincipale = findViewById(R.id.dly_MenuLateralePrincipale);
        myToggle  = new ActionBarDrawerToggle(this,menuLateralePrincipale,R.string.nav_Open,R.string.nav_Close);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  LISTENER DEI BOTTONI
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Listener dell' apertura e chiusura del menu laterale principale
        menuLateralePrincipale.addDrawerListener(myToggle);
        myToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//--------------------------------------------------------------------------------------------------
        // Listener dei pulsanti del menu laterale principale
        NavigationView menuLateralePrincipale = (NavigationView) findViewById(R.id.nav_menu_laterale_principale);
        menuLateralePrincipale.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener(){
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return gestioneSelezionePulsanti(item);
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        setSelectedButton();
    }
/*================================================================================================*/
/*################################################################################################*/

/*################################################################################################*/
//                                  GESTIONE DEI PULSANTI MENU' LATERALE PRINCIPLAE
/*################################################################################################*/
/*================================================================================================*/
//                                  IMPOSTAZIONE PULSANTE SELEZIONATO
/*================================================================================================*/
    /**
     * Questo metodo serve per impostare il tasto selezionato del menu laterale in modo tale che cambi
     * in base alla pagina/activity stessa che ha tale menu.
     * In oltre serve anche per caricare il fragment corrtetto nel header del menu.
     */
    private void setSelectedButton() {
        Fragment frag = null;
        //Individua l'Activity attiva in questo momento
        NavigationView manuLateralePrincipale = findViewById(R.id.nav_menu_laterale_principale);
        if( (this.getClass().equals(MenuDelleMenseActivity.class))){
            manuLateralePrincipale.getMenu().getItem(0).setChecked(true);
            frag = new MenuMenuDelleMenseFragment();
        }
        if( (this.getClass().equals(PastoDelMeseActivity.class)  )){
            manuLateralePrincipale.getMenu().getItem(1).setChecked(true);
            frag = MenuHeaderStaticFragment.newInstance(R.string.ttl_PastoDelMese,
                    R.drawable.img_menu_pasto_del_mese,
                    R.drawable.img_menu_cup);//Imposta il fragment corretto.
        }
        if( (this.getClass().equals(PersonalizzaMenuSettimanaleActivity.class))){
            manuLateralePrincipale.getMenu().getItem(2).setChecked(true);
            frag = MenuHeaderStaticFragment.newInstance(R.string.ttl_PersonalizzaMenuSettimanale,
                    R.drawable.img_menu_personalizza_menu_settimanale);//Imposta il fragment corretto.
        }
        if( (this.getClass().equals(ValutazionePastoActivity.class)  )){
            manuLateralePrincipale.getMenu().getItem(3).setChecked(true);
            frag = MenuHeaderStaticFragment.newInstance(R.string.ttl_ValutazioneUltimoPasto,
                    R.drawable.img_menu_valutazione_ultimo_pasto);//Imposta il fragment corretto.
        }
        if( (this.getClass().equals(ProfiloUtenteActivity.class)  )){
            manuLateralePrincipale.getMenu().getItem(4).setChecked(true);
            frag = new MenuProfiloUtenteFragment();//Imposta il fragment corretto.
        }
        if( (this.getClass().equals(StatisticheUtenteActivity.class)  )){
            manuLateralePrincipale.getMenu().getItem(5).setChecked(true);
            frag = MenuHeaderStaticFragment.newInstance(R.string.ttl_StatisticheUtente,
                    R.drawable.img_menu_statistiche_utenti);//Imposta il fragment corretto.
        }
        if( (this.getClass().equals(TesseraDigitaleActivity.class)  )){
            manuLateralePrincipale.getMenu().getItem(6).setChecked(true);
            frag = MenuHeaderStaticFragment.newInstance(R.string.ttl_TesseraDigitale,
                    R.drawable.img_menu_tessera_digitale);//Imposta il fragment corretto.
        }
        if(!(this  instanceof  Object)){//TODO: INSERIRE LA CLASSE A CUI APPATIENE LA ACTIVITY DI DESTINAZIOONE   ATTEZIONE TOGLIERE LA NEGAZIONE
            manuLateralePrincipale.getMenu().getItem(7).setChecked(true);
        }
        //Imposta il fragmento selezionato
        int orientation = getResources().getConfiguration().orientation;
        if((frag!=null) && (orientation==Configuration.ORIENTATION_PORTRAIT)){
            //Controllo se c'è il fragmento
            FrameLayout frameLayoutMenu = findViewById(R.id.fly_HeaderMenuLaterale);
            if(frameLayoutMenu!=null){
                frameLayoutMenu.setVisibility(View.VISIBLE);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fly_HeaderMenuLaterale, frag);
                ft.commit();
            }
        }else{
            FrameLayout frameLayoutMenu = findViewById(R.id.fly_HeaderMenuLaterale);
            if(frameLayoutMenu!=null){
                frameLayoutMenu.setVisibility(View.GONE);
            }
        }
    }
/*================================================================================================*/

/*================================================================================================*/
/*                                  METODO PER LA GESTIONE DEI PULSANTI PREMUTI NEL MENU LATERALE */
/*                                  PRINCIPALE                                                    */
/*================================================================================================*/
    /**
     * Questo metodo serve per impostare l'azzione che il pulsante del menu laterale deve compire in
     * base alla pagina/activity in cui si trova. Se il tasto va verso un altra pagina chiama un metodo
     * che genra l'intent per passare a quella nuova, altrimeti chiude solametre il menu latrale dato
     * che ci si trova già in posizione senza ricaricarla.
     *
     * @param item
     *      Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *      è stato premuto dall'utente.
     */
    public boolean gestioneSelezionePulsanti(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.btn_nav_MenuDelleMense: {
                    if(!(this  instanceof MenuDelleMenseActivity))
                        //Non sono nella pagina scelta
                        buttonMenuDelleMense(item);
                    else
                        //Sono gia nella pagina scelta
                        menuLateralePrincipale.closeDrawers();
                    break;
                    }

            case R.id.btn_nav_PiattoDelMese:{
                if(!(this  instanceof  PastoDelMeseActivity))
                    //Non sono nella pagina scelta
                    buttonPastoDelMese(item);
                else
                    //Sono gia nella pagina scelta
                    menuLateralePrincipale.closeDrawers();
                break;
                }

            case R.id.btn_nav_PersonalizzaMenuSettimanale: {
                if(!(this  instanceof  PersonalizzaMenuSettimanaleActivity))
                    buttonPersonalizzaMenuSettimanale(item);
                else
                    menuLateralePrincipale.closeDrawers();
                break;
                }

            case R.id.btn_nav_ValutazioneUltimoPasto: {
                if(!(this  instanceof ValutazionePastoActivity))
                    //Non sono nella pagina scelta
                    buttonValutazionePastoActivity(item);
                else
                    //Sono gia nella pagina scelta
                    menuLateralePrincipale.closeDrawers();
                break;
            }

            case R.id.btn_nav_ConfigurazioneProfiloUtente: {
                if(!(this  instanceof ProfiloUtenteActivity))
                    //Non sono nella pagina scelta
                    buttonConfigurazioneProfiloUtente(item);
                else
                    //Sono gia nella pagina scelta
                    menuLateralePrincipale.closeDrawers();
                break;
                }

            case R.id.btn_nav_StatiscicheUtente: {
                if(!(this  instanceof  StatisticheUtenteActivity))
                    //Non sono nella pagina scelta
                    buttonStatiscicheUtente(item);
                else
                    //Sono gia nella pagina scelta
                    menuLateralePrincipale.closeDrawers();
                break;
                }

            case R.id.btn_nav_TesseraDigitale: {
                if(!(this  instanceof  TesseraDigitaleActivity))
                    //Non sono nella pagina scelta
                    buttonTesseraDigitale(item);
                else
                    //Sono gia nella pagina scelta
                    menuLateralePrincipale.closeDrawers();
                break;
            }

            case R.id.btn_nav_Logout: {
                    buttonLogout(item);
                break;
                }
        }
        return false;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                                  METODI CHIAMATI ALLA PRESIONE DEI BOTTONI                     */
/*================================================================================================*/
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante "Menu Delle Mense". Questo metodo non fa
     * altro che passare i dati tramite Intent ed avviare la nuova activiti.
     * @param item
     *      Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *      è stato premuto dall'utente.
     */
    private void buttonMenuDelleMense(MenuItem  item){
        try {
            MyIntent i = new MyIntent(this,MenuDelleMenseActivity.class);
            setParamiter(i);//IMPOTANTE: SERVE PER POI FARE L'OVERRIDE DEL METODO PER INSERIRE I PARAMETRI CHE SERVONO.
            startActivity(i);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante "Pasto Del Mese". Questo metodo non fa
     * altro che passare i dati tramite Intent ed avviare la nuova activiti.
     * @param item
     *      Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *      è stato premuto dall'utente.
     */
    private void buttonPastoDelMese(MenuItem  item){
        try {
            MyIntent i = new MyIntent(this, PastoDelMeseActivity.class);
            setParamiter(i);//IMPOTANTE: SERVE PER POI FARE L'OVERRIDE DEL METODO PER INSERIRE I PARAMETRI CHE SERVONO.
            startActivity(i);
        } catch (Exception e) {
            System.out.println(e);
        }

    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante "Personalizza Menu Settimanale".
     * Questo metodo non fa altro che passare i dati tramite Intent ed avviare la nuova activiti.
     * @param item
     *     Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *     è stato premuto dall'utente.
     */
    private void buttonPersonalizzaMenuSettimanale(MenuItem  item){
        try {
            MyIntent i = new MyIntent(this, PersonalizzaMenuSettimanaleActivity.class);
            setParamiter(i);//IMPOTANTE: SERVE PER POI FARE L'OVERRIDE DEL METODO PER INSERIRE I PARAMETRI CHE SERVONO.
            startActivity(i);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante "Valutazione Pasto". Questo metodo non fa
     * altro che passare i dati tramite Intent ed avviare la nuova activiti.
     * @param item
     *     Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *     è stato premuto dall'utente.
     */
    private void buttonValutazionePastoActivity(MenuItem  item){
        try {
            MyIntent i = new MyIntent(this,ValutazionePastoActivity.class);
            setParamiter(i);//IMPOTANTE: SERVE PER POI FARE L'OVERRIDE DEL METODO PER INSERIRE I PARAMETRI CHE SERVONO.
            startActivity(i);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante "Profilo Utente". Questo metodo non fa
     * altro che passare i dati tramite Intent ed avviare la nuova activiti.
     * @param item
     *     Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *     è stato premuto dall'utente.
     */
    private void buttonConfigurazioneProfiloUtente(MenuItem  item){
        try {
            MyIntent i = new MyIntent(this,ProfiloUtenteActivity.class);
            setParamiter(i);//IMPOTANTE: SERVE PER POI FARE L'OVERRIDE DEL METODO PER INSERIRE I PARAMETRI CHE SERVONO.
            startActivity(i);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante "Statistiche Utente". Questo metodo non fa
     * altro che passare i dati tramite Intent ed avviare la nuova activiti.
     * @param item
     *     Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *     è stato premuto dall'utente.
     */
    private void buttonStatiscicheUtente(MenuItem  item){
        try {
            MyIntent i = new MyIntent(this, StatisticheUtenteActivity.class);
            setParamiter(i);//IMPOTANTE: SERVE PER POI FARE L'OVERRIDE DEL METODO PER INSERIRE I PARAMETRI CHE SERVONO.
            startActivity(i);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    /*------------------------------------------------------------------------------------------------*/
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante "Tessere Digitali". Questo metodo non fa
     * altro che passare i dati tramite Intent ed avviare la nuova activiti.
     * @param item
     *     Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *     è stato premuto dall'utente.
     */
    private void buttonTesseraDigitale(MenuItem  item){
        try {
            MyIntent i = new MyIntent(this,TesseraDigitaleActivity.class);
            setParamiter(i);//IMPOTANTE: SERVE PER POI FARE L'OVERRIDE DEL METODO PER INSERIRE I PARAMETRI CHE SERVONO.
            startActivity(i);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Il metodo chiamato dal Lisener quando si preme il pulsante Logout. Questo metodo non fa
     * altro che cancellare i dati di sessioe e le notififiche dell'utente che stà uscendo.
     * @param item
     *     Il parameto item contiene il riperimeto del pulsate, del menu laterale principale, che
     *     è stato premuto dall'utente.
     */
    private void buttonLogout(MenuItem  item){
        doLogout();
    }
/*================================================================================================*/

/*================================================================================================*/
/*                                  METODO PER PASSARE I PARAMETRI ALLE ATRE PAGINE               */
/*================================================================================================*/
    /*
     * Le classi che estendono questa super classe, protranno fare l'override di questo medodo
     * così da addiunggere al intent chiamato in automatioco i parametri in maniera dinamica.
     * Questo petodo infatti non ha senso essere chiamato da solo, ma viene chamato in maierà automatica
     * al momento che si pattiva l'azzione legata alla presione di un tasto tra quelli del menù laterale
     * principale.
     * @param intent
     *      Tale parametro contiene l'azione che verra fatta al termine di questà operazione, quindi
     *      permette l'inserimento in esso dei dati.
     **/
    protected void setParamiter(MyIntent intent){
    };
/*================================================================================================*/
/*################################################################################################*/

/*################################################################################################*/
//                                  RIDUCE APPLICAZIONE
/*################################################################################################*/
/*================================================================================================*/
/*                                  METODO PER IMPEDIRE DI RAGGIUNGERE ACTIVITY PRECEDENTI        */
/*================================================================================================*/
    /**
     * Questo metodo impedisce di ritornare tramite il tasto "Fisico/Generico" indietro dello smartphone alle
     * activity precedenti così da rendere la activity attuale quella di root per la navigazione.
     * Infatti se si preme tale pulsante l'applicazione verrà spostata tra quelle non attive mostrando
     * la pagina principale del dispositivo.
     */
    protected void impostaPaginaComeHome(){
        this.getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startMain);
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                 GESTIONE APERTURA, CHIUSURA E INSERIMETO BOTTONE PER ACCDERE AL*/
/*                                  MENU' LATERALE PRINCIPLAE                                     */
/*################################################################################################*/
/*================================================================================================*/
/*                                  DEFINIZIONE DEL MENU' (senza il tasto a sinistra)             */
/*================================================================================================*/
    /**
     * Questo metodo serve per non avere il tasto sinistro.
     *
     * @see AppCompatActivity#onCreateOptionsMenu(Menu)
     * @return Restituisce solo true.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(menuLayout,menu);
        return true;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  METODO PER LA APERTURA E CHIUSURA DEL MENU LATERALE PRINCIPALE*/
/*================================================================================================*/
    /**
     * Questo metodo serve per aprire e chiudere il menu latrale
     *
     * @see AppCompatActivity#onOptionsItemSelected(MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(myToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  LOGOUT                                                        */
/*################################################################################################*/
/*================================================================================================*/
/*                                  LOGOUT                                                        */
/*================================================================================================*/
    /**
     * Questo è il metdo che effetua il logout del utente.
     */
    public void doLogout(){
        //Toast.makeText(this, "AppCompatActivityWithMyPrimaryMenu: Logout", Toast.LENGTH_SHORT).show();
        try {
           /* //Cancella le notifiche fatte
            MyServizioDiNotifiche.chiusuraServizioDiNotificheENotifiche(this);
            //Vai alla pagina di login*/
            GestioneSessione.cancellaSessioneAperte(this);//CANCELLA SESSIONE
            MyIntent i = new MyIntent(this, LoginActivity.class);
            startActivity(i);
            //Toast.makeText(this, "this.getClass() = "+ this.getClass()+" ProfiloUtenteActivity.class = " , Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
/*================================================================================================*/
/*################################################################################################*/


}
