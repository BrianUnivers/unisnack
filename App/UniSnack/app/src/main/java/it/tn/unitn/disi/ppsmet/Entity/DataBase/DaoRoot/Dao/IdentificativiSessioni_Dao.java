package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DatiPerSessione.IdentificativiSessioni;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;

@Dao
public interface IdentificativiSessioni_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(IdentificativiSessioni identificativiSessioni);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Query("DELETE FROM tableIdentificativiSessioni")
    public void deleteAll();
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = IdentificativiSessioni.class,
            onConflict = OnConflictStrategy.ABORT)
    public void update(IdentificativiSessioni identificativiSessioni);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID SESSION                                               */
/*================================================================================================*/
    @Query("SELECT * FROM tableIdentificativiSessioni WHERE idSessione == :idSessione")
    public IdentificativiSessioni getByIdSessione(String idSessione);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableIdentificativiSessioni WHERE idSessione == :idSessione")
    public LiveData<IdentificativiSessioni> getByIdSessione_Synchronized(String idSessione);
/*================================================================================================*/
/*################################################################################################*/
}
