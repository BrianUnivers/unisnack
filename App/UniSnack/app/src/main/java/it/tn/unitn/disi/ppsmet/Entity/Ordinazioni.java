package it.tn.unitn.disi.ppsmet.Entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Date;
import java.util.Objects;

@Entity(tableName = "tableOrdinazioni",
        foreignKeys = {
            @ForeignKey(entity = Utenti.class,
                parentColumns = "id",       //Colanna della classe a qui si rifierisce
                childColumns  = "id_utente",//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE),
            @ForeignKey(entity = TipoPasto.class,
                    parentColumns = "id",          //Colanna della classe a qui si rifierisce
                    childColumns  = "id_tipoPasto",//ForeignKey
                    onUpdate = ForeignKey.CASCADE,
                    onDelete = ForeignKey.CASCADE)},
        indices = {
                @Index(value = {"id_utente"}),
                @Index(value = {"id_tipoPasto"})})
public class Ordinazioni {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @PrimaryKey(autoGenerate = true)//Questo perchè è solo in locale
    private Long id = null;
    @NonNull
    @ColumnInfo(name = "giorno")
    private Date giorno;
    @NonNull
    @ColumnInfo(name = "id_utente")
    private Long id_utente;
    @NonNull
    @ColumnInfo(name = "id_tipoPasto")
    private Long id_tipoPasto;
    @Ignore
    private TipoPasto tipoPasto = null;


    @ColumnInfo(name = "dataValutazione")
    private Date  dataValutazione = null;
    @ColumnInfo(name = "votoCottura")
    private Short votoCottura     = null;//tra 0 e 10 compresi
    @ColumnInfo(name = "votoTemperatura")
    private Short votoTemperatura = null;//tra 0 e 10 compresi
    @ColumnInfo(name = "votoSapore")
    private Short votoSapore      = null;//tra 0 e 10 compresi
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    @Ignore
    public Ordinazioni() { }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Ordinazioni(Long id,
                       @NonNull Date giorno,
                       @NonNull Long id_utente,
                       @NonNull Long id_tipoPasto) {
        this.id = id;
        this.giorno = giorno;
        this.id_utente = id_utente;
        this.id_tipoPasto = id_tipoPasto;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Ordinazioni(Long id,
                       Date giorno,
                       Long id_utente,
                       TipoPasto tipoPasto) {
        this.id = id;
        this.giorno = giorno;
        this.id_utente = id_utente;
        setTipoPasto(tipoPasto);
    }
//--------------------------------------------------------------------------------------------------
    public Ordinazioni(Long id,
                       @NonNull Date giorno,
                       @NonNull Long id_utente,
                       @NonNull Long id_tipoPasto,
                       Date dataValutazione,
                       Short votoCottura,
                       Short votoTemperatura,
                       Short votoSapore) {
        this.id = id;
        this.giorno = giorno;
        this.id_utente = id_utente;
        this.id_tipoPasto = id_tipoPasto;
        this.dataValutazione = dataValutazione;
        this.votoCottura = votoCottura;
        this.votoTemperatura = votoTemperatura;
        this.votoSapore = votoSapore;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Ordinazioni(Long id,
                       Date giorno,
                       Long id_utente,
                       TipoPasto tipoPasto,
                       Date dataValutazione,
                       Short votoCottura,
                       Short votoTemperatura,
                       Short votoSapore) {
        this.id = id;
        this.giorno = giorno;
        this.id_utente = id_utente;
        setTipoPasto(tipoPasto);
        this.dataValutazione = dataValutazione;
        this.votoCottura = votoCottura;
        this.votoTemperatura = votoTemperatura;
        this.votoSapore = votoSapore;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    public Long getId() {
        return id;
    }
//--------------------------------------
    public void setId(Long id) {
        this.id = id;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Date getGiorno() {
        return giorno;
    }
//--------------------------------------
    public void setGiorno(@NonNull Date giorno) {
        this.giorno = giorno;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_utente() {
        return id_utente;
    }
//--------------------------------------
    public void setId_utente(@NonNull Long id_utente) {
        this.id_utente = id_utente;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_tipoPasto() {
        return id_tipoPasto;
    }
//--------------------------------------
    public void setId_tipoPasto(@NonNull Long id_tipoPasto) {
        this.id_tipoPasto = id_tipoPasto;
        if((tipoPasto!=null) && (id_tipoPasto!=null)  && (id_tipoPasto!=tipoPasto.getId())){
            tipoPasto=null;
        }else if(id_tipoPasto==null){
            tipoPasto=null;//TODO: Oppure mantenrere lo stesso id  di tipo
        }
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public TipoPasto getTipoPasto() {
        return tipoPasto;
    }
//--------------------------------------
    @Ignore
    public boolean setTipoPasto(TipoPasto tipoPasto) {
        boolean ris = true;
        if((id_tipoPasto!=null) && (id_tipoPasto==tipoPasto.getId()) ){
            this.tipoPasto = tipoPasto;
        }else if((id_tipoPasto==null) && (tipoPasto!=null)){
            this.id_tipoPasto = tipoPasto.getId();
            this.tipoPasto = tipoPasto;
        }else{
            ris = false;
        }
        return ris;
    }
//--------------------------------------------------------------------------------------------------
    public Date getDataValutazione() {
        return dataValutazione;
    }
//--------------------------------------
    public void setDataValutazione(Date dataValutazione) {
        this.dataValutazione = dataValutazione;
    }
//--------------------------------------------------------------------------------------------------
    public Short getVotoCottura() {
        return votoCottura;
    }
//--------------------------------------
    public void setVotoCottura(Short votoCottura) {
        this.votoCottura = votoCottura;
    }
//--------------------------------------------------------------------------------------------------
    public Short getVotoTemperatura() {
        return votoTemperatura;
    }
//--------------------------------------
    public void setVotoTemperatura(Short votoTemperatura) {
        this.votoTemperatura = votoTemperatura;
    }
//--------------------------------------------------------------------------------------------------
    public Short getVotoSapore() {
        return votoSapore;
    }
//--------------------------------------
    public void setVotoSapore(Short votoSapore) {
        this.votoSapore = votoSapore;
    }
//==================================================================================================
//==================================================================================================
//                                      EQUALS E HASH_CODE
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ordinazioni)) return false;
        Ordinazioni that = (Ordinazioni) o;
        return Objects.equals(getId(), that.getId()) &&
                getGiorno().equals(that.getGiorno()) &&
                getId_utente().equals(that.getId_utente()) &&
                getId_tipoPasto().equals(that.getId_tipoPasto()) &&
                Objects.equals(getTipoPasto(), that.getTipoPasto()) &&
                Objects.equals(getDataValutazione(), that.getDataValutazione()) &&
                Objects.equals(getVotoCottura(), that.getVotoCottura()) &&
                Objects.equals(getVotoTemperatura(), that.getVotoTemperatura()) &&
                Objects.equals(getVotoSapore(), that.getVotoSapore());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getGiorno(), getId_utente(), getId_tipoPasto(), getTipoPasto(), getDataValutazione(), getVotoCottura(), getVotoTemperatura(), getVotoSapore());
    }
//==================================================================================================
//##################################################################################################
}
