package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.Date;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.IscrizioniUtente;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

@Dao
public abstract class IscrizioniUtente_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
                    "U.matricola " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND U.id == :idUtenti " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
                    "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
                    "null AS C_id, null AS C_nome, null AS C_sigla, " +
                    "null " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND U.id_utente == :idUtenti " )
    public abstract List<IscrizioniUtente> getByIdOfUtente(Long idUtenti);
/*------------------------------------------------------------------------*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
            "U.matricola " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND U.id == :idUtenti " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "null AS C_id, null AS C_nome, null AS C_sigla, " +
            "null " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND U.id_utente == :idUtenti " )
    public abstract LiveData<List<IscrizioniUtente>> getByIdOfUtente_Synchronized(Long idUtenti);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE  INTO SPECIFIC DAY                             */
/*================================================================================================*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
            "U.matricola " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND" +
                  " U.id == :idUtenti  AND :date BETWEEN AA.dataInizio AND AA.dataFine " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "null AS C_id, null AS C_nome, null AS C_sigla, " +
            "null " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND "+
                  " U.id_utente == :idUtenti  AND :date BETWEEN AA.dataInizio AND AA.dataFine " )
    public abstract List<IscrizioniUtente> getByIdOfUtente(Long idUtenti, Date date);
/*------------------------------------------------------------------------*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
            "U.matricola " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND" +
            " U.id == :idUtenti  AND :date BETWEEN AA.dataInizio AND AA.dataFine " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "null AS C_id, null AS C_nome, null AS C_sigla, " +
            "null " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND "+
            " U.id_utente == :idUtenti  AND :date BETWEEN AA.dataInizio AND AA.dataFine " )
    public abstract LiveData<List<IscrizioniUtente>> getByIdOfUtente_Synchronized(Long idUtenti, Date date);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE TODAY                                          */
/*================================================================================================*/
    @Transaction
    public List<IscrizioniUtente> getByIdOfUtenteAtToday(Long idUtenti){
        return this.getByIdOfUtente(idUtenti,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<IscrizioniUtente>> getByIdOfUtenteAtToday_Synchronized(Long idUtenti){
        return this.getByIdOfUtente_Synchronized(idUtenti,new Date());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UNIVERSITA'                                           */
/*================================================================================================*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
            "U.matricola " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND U.id_universita == :idUniversita " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "null AS C_id, null AS C_nome, null AS C_sigla, " +
            "null " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND U.id_universita == :idUniversita " )
    public abstract List<IscrizioniUtente> getByIdOfUniversita(Long idUniversita);
/*------------------------------------------------------------------------*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
            "U.matricola " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND U.id_universita == :idUniversita " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "null AS C_id, null AS C_nome, null AS C_sigla, " +
            "null " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND U.id_universita == :idUniversita " )
    public abstract LiveData<List<IscrizioniUtente>> getByIdOfUniversita_Synchronized(Long idUniversita);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UNIVERSITA' INTO SPECIFIC DAY                         */
/*================================================================================================*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
            "U.matricola " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND" +
            " U.id_universita == :idUniversita  AND :date BETWEEN AA.dataInizio AND AA.dataFine " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "null AS C_id, null AS C_nome, null AS C_sigla, " +
            "null " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND "+
            " U.id_universita == :idUniversita  AND :date BETWEEN AA.dataInizio AND AA.dataFine " )
    public abstract List<IscrizioniUtente> getByIdOfUniversita(Long idUniversita, Date date);
/*------------------------------------------------------------------------*/
    @Query( "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "C.id AS C_id, C.nome AS C_nome, C.sigla AS C_sigla, " +
            "U.matricola " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableCorsi AS C, tableUtenti AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_corso == C.id AND U.id_universita == Uni.id AND" +
            " U.id_universita == :idUniversita  AND :date BETWEEN AA.dataInizio AND AA.dataFine " +
            "UNION " +
            "SELECT Uni.id AS Uni_id, Uni.nome AS Uni_nome, Uni.sigla AS Uni_sigla, " +
            "AA.id AS AA_id, AA.dataInizio AS AA_dataInizio, AA.dataFine AS AA_dataFine, "+
            "null AS C_id, null AS C_nome, null AS C_sigla, " +
            "null " +
            "FROM tableUniversita AS Uni, tableAnnoAccademico AS AA, tableU_partecipa_Uni AS U " +
            "WHERE  U.id_AA == AA.id AND U.id_universita == Uni.id AND "+
            " U.id_universita == :idUniversita  AND :date BETWEEN AA.dataInizio AND AA.dataFine " )
    public abstract LiveData<List<IscrizioniUtente>> getByIdOfUniversita_Synchronized(Long idUniversita, Date date);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UNIVERSITA' TODAY                                     */
/*================================================================================================*/
    @Transaction
    public List<IscrizioniUtente> getByIdOfUniversitaAtToday(Long idUniversita){
        return this.getByIdOfUniversita(idUniversita,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<IscrizioniUtente>> getByIdOfUniversitaAtToday_Synchronized(Long idUniversita){
        return this.getByIdOfUniversita_Synchronized(idUniversita,new Date());
    }
/*================================================================================================*/
/*################################################################################################*/
}
