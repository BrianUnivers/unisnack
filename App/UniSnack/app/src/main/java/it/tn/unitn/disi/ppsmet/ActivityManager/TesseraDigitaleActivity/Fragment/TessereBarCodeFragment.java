package it.tn.unitn.disi.ppsmet.ActivityManager.TesseraDigitaleActivity.Fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import it.tn.unitn.disi.ppsmet.ActivityManager.TesseraDigitaleActivity.TesseraDigitaleViewModel;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.GestioneAssetPerFoto.GestioneFotoDaAsset;
import it.tn.unitn.disi.ppsmet.R;

public class TessereBarCodeFragment extends Fragment {
/*################################################################################################*/
/*                                  INIZIALIZZAZIONE VALORI                                       */
/*################################################################################################*/
    private TesseraDigitaleViewModel  viewModel           = null;
    private LiveData<Tessere>         tesseraSelezionata = null;
    private GestioneFotoDaAsset       ACCEDI_FOTO        = null;
/*================================================================================================*/
/*                                  COSTRUTTORE                                                   */
/*================================================================================================*/
    /**
     * Questo è il metodo costruttore del fragment.
     */
    public TessereBarCodeFragment() {
        super();
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private ViewGroup rootView;
    private ImageView imgTessara;
/*================================================================================================*/
/*                                  CREAZIONE DELL FRAGMENT                                       */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'inizializazione del layout del fragment ed inoltre contiene le loro
     * possibili interazioni con l'utente.
     * In aggiunta viene richesto un modelView che è quello usato dall'activity in modo tale da accedere
     * alla sua stessa istanza, in modo tale da utilizarlo come metodo per passare parameti da una parte
     * all'altra.
     *
     * In questo caso viene usato per ricevere come informazione della tessera selezionata.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup)  inflater.inflate(R.layout.fragment_tessere_immagine, container, false);
/*------------------------------------------------------------------------*/
        ACCEDI_FOTO = new GestioneFotoDaAsset(getActivity());
/*------------------------------------------------------------------------*/
        try {
            viewModel = new ViewModelProvider(getActivity()).get(TesseraDigitaleViewModel.class);
        }catch (Exception e){
            viewModel=null;
        }
/*------------------------------------------------------------------------*/
        imgTessara = rootView.findViewById(R.id.img_Tessera);
/*------------------------------------------------------------------------*/
        if(viewModel!=null){
            richiediDatiEInserisci();
        }
/*------------------------------------------------------------------------*/
        return rootView;
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  ADATTA AL UTENTE                                              */
/*################################################################################################*/
/*================================================================================================*/
/*                                  RICHIEDI UTENTE E ADATTA I DATO                               */
/*================================================================================================*/
    /**
     * Questo metodo richiede al ViewModel la tessera selezionata e aggiunge un observer così da avere
     * sempre aggiornato la tessera selezionata.
     */
    private void richiediDatiEInserisci(){
        tesseraSelezionata = viewModel.getTesseraSelezzionata();
        tesseraSelezionata.observe(getViewLifecycleOwner(), new Observer<Tessere>() {
            @Override
            public void onChanged(Tessere tessere) {
                if(tessere!=null){
                    impostaDatiUtenti(tessere);
                }
            }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA I DATI TESSERE NEL LAYOUT                             */
/*================================================================================================*/
    /**
     * Questo è il metodo che dato una tessera inserisce la sua rappresentazione digitale sottoforma
     * di codice a barra.
     * @param tessere
     */
    private void impostaDatiUtenti(Tessere tessere){
        if(tessere.getUrlQRcode()!=null){
            Bitmap image = ACCEDI_FOTO.getFoto(tessere.getUrlCodiceABarre());
            if(image!=null){
                imgTessara.setImageBitmap(image);
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}