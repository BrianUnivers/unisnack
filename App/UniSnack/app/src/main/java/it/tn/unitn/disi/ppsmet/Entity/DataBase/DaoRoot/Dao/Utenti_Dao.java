package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;
import java.util.Objects;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.Authentication.HashPasswordWithSalt;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

@Dao
public abstract class Utenti_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract long insert(Utenti utenti);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Query("DELETE FROM tableUtenti")
    public abstract void deleteAll();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE By ID                                                    */
/*================================================================================================*/
    @Query("DELETE FROM tableUtenti WHERE id == :id")
    public abstract void delete(Long id);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = Utenti.class,
            onConflict = OnConflictStrategy.ABORT)
    public abstract void update(Utenti utenti);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    @Query("SELECT id FROM tableUtenti")
    public abstract List<Long> getAllPrimaryKey();
/*------------------------------------------------------------------------*/
    @Query("SELECT id FROM tableUtenti")
    public abstract LiveData<List<Long>> getAllPrimaryKey_Synchronized();
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    @Query("SELECT * FROM tableUtenti")
    public abstract List<Utenti> getAll();
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableUtenti")
    public abstract LiveData<List<Utenti>> getAll_Synchronized();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    @Query("SELECT * FROM tableUtenti WHERE id == :id")
    public abstract Utenti getByPrimaryKey(Long id);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableUtenti WHERE id == :id")
    public abstract LiveData<Utenti> getByPrimaryKey_Synchronized(Long id);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID SESSIONE                                              */
/*================================================================================================*/
    @Query( "SELECT U.* " +
            "FROM tableUtenti AS U, tableIdentificativiSessioni AS I  " +
            "WHERE U.id == I.idUtente AND I.idSessione == :idSessione")
    public abstract Utenti getByIdSessione(String idSessione);
/*------------------------------------------------------------------------*/
    @Query( "SELECT U.* " +
            "FROM tableUtenti AS U, tableIdentificativiSessioni AS I  " +
            "WHERE U.id == I.idUtente AND I.idSessione == :idSessione")
    public abstract LiveData<Utenti> getByIdSessione_Synchronized(String idSessione);
    /*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by AUTENTICATION                                            */
/*================================================================================================*/
    @Transaction
    public Utenti getByAuthentication(String email,String password){
        String passwordHashed = this.generaPassword(email,password);
        return this.getByAuthenticator(email,passwordHashed);
    }
/*------------------------------------------------------------------------*/
    //@Transaction
    public LiveData<Utenti> getByAuthentication_Synchronized(String email,String password){
        String passwordHashed = this.generaPassword(email,password);
        return this.getByAuthenticator_Synchronized(email,passwordHashed);
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo richede al database il salt e calcola la password a qui viene aggiunto il salt
     * per poi applicare la funzione di hash.
     * @param email
     * @param password
     * @return Il risultato è la hashed password oppure null.
     */
    private String generaPassword(String email,String password){
        String salt = this.getSaltByEmail(email);
        return HashPasswordWithSalt.genraPasswordHashWithSalt(password,salt);
    }
/*------------------------------------------------------------------------*/
    @Query( "SELECT * " +
            "FROM tableUtenti " +
            "WHERE email  == :email AND password == :passwordHashed")
    public abstract Utenti getByAuthenticator(String email,String passwordHashed);
/*------------------------------------------------------------------------*/
    @Query( "SELECT * " +
            "FROM tableUtenti " +
            "WHERE email  == :email AND password == :passwordHashed")
    public abstract LiveData<Utenti> getByAuthenticator_Synchronized(String email,String passwordHashed);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET SALT by EMAIL                                               */
/*================================================================================================*/
    @Query("SELECT salt FROM tableUtenti WHERE email  == :email")
    protected abstract String getSaltByEmail(String email);
/*================================================================================================*/
/*################################################################################################*/
}