package it.tn.unitn.disi.ppsmet.IncludeView_Setup;

import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import it.tn.unitn.disi.ppsmet.IncludeView_Setup.SelettoreABarra.SelettoreABarra;
import it.tn.unitn.disi.ppsmet.R;

public class GiornoSelezionatoRowView {
/*################################################################################################*/
    private ViewGroup contenitore;
    private TextView  numeroGiorno;
    private TextView  abbreviazioneMese;
    private TextView  giornoDellaSettimana;
    private ImageView frecciaLaterale;
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    public GiornoSelezionatoRowView(ViewGroup contenitore,
                                       TextView  numeroGiorno,
                                       TextView  abbreviazioneMese,
                                       TextView  giornoDellaSettimana,
                                       ImageView frecciaLaterale,
                                       Date date) {
        this.contenitore          = contenitore;
        this.numeroGiorno         = numeroGiorno;
        this.abbreviazioneMese    = abbreviazioneMese;
        this.giornoDellaSettimana = giornoDellaSettimana;
        this.frecciaLaterale      = frecciaLaterale;
        setData(date);
    }
/*------------------------------------------------------------------------------------------------*/
    public static GiornoSelezionatoRowView newInstance(ViewGroup rootLayout, int idLayoutIncluded, Date date) {
        GiornoSelezionatoRowView ris = null;
        if((rootLayout!=null) && (date!=null)){
            ViewGroup barraContenitore = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(barraContenitore!=null){
                TextView  numeroGiorno         = barraContenitore.findViewById(R.id.txt_NumeroGiorno);
                TextView  abbreviazioneMese    = barraContenitore.findViewById(R.id.txt_AbbreviazioneMese);
                TextView  giornoDellaSettimana = barraContenitore.findViewById(R.id.txt_GiornoDellaSettimana);
                ImageView frecciaLaterale      = barraContenitore.findViewById(R.id.img_Freccia);

                if((numeroGiorno!=null) && (abbreviazioneMese!=null) &&
                        (giornoDellaSettimana!=null) && (frecciaLaterale!=null)){
                    ris = new GiornoSelezionatoRowView( barraContenitore,
                                                        numeroGiorno,
                                                        abbreviazioneMese,
                                                        giornoDellaSettimana,
                                                        frecciaLaterale,date);
                }
            }
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  IMPOSTAZIONI DI LAYOU                                         */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA E CAMBIA DATA                                         */
/*================================================================================================*/
    public void setData(Date data){
        String format2 = "dd-MMM-yy";
        SimpleDateFormat sdf = new SimpleDateFormat(format2, Locale.ITALIAN);
        String dataStinga = sdf.format(data);
        String[] infoDati = dataStinga.split("-");
        if((infoDati!=null) && (infoDati.length==3)){
            this.numeroGiorno.setText(infoDati[0]);
            this.abbreviazioneMese.setText(infoDati[1]);
            this.giornoDellaSettimana.setText(new SimpleDateFormat("EEEE", Locale.ITALY).format(data.getTime()));
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA TIPO GIORNO ATTIVO O DISATTIVO                        */
/*================================================================================================*/
    public void setStatoAttivo(boolean b){
        if(!b){
            //Disattivo
            this.numeroGiorno.setTextColor(contenitore.getResources().getColor(R.color.colorNormalTextLite,null));
            this.abbreviazioneMese.setTextColor(contenitore.getResources().getColor(R.color.colorNormalTextLite,null));
            this.giornoDellaSettimana.setTextColor(contenitore.getResources().getColor(R.color.colorNormalTextLite,null));
            this.numeroGiorno.setTypeface(null, Typeface.NORMAL);
            this.abbreviazioneMese.setTypeface(null, Typeface.NORMAL);
            this.giornoDellaSettimana.setTypeface(null, Typeface.NORMAL);
            this.contenitore.setBackgroundResource(R.color.colorSecondaryLight);
            this.frecciaLaterale.setVisibility(View.GONE);
        }else{
            //Attivo
            this.numeroGiorno.setTextColor(contenitore.getResources().getColor(R.color.colorPrimary,null));
            this.abbreviazioneMese.setTextColor(contenitore.getResources().getColor(R.color.colorPrimary,null));
            this.giornoDellaSettimana.setTextColor(contenitore.getResources().getColor(R.color.colorPrimary,null));
            this.numeroGiorno.setTypeface(null, Typeface.BOLD);
            this.abbreviazioneMese.setTypeface(null, Typeface.BOLD);
            this.giornoDellaSettimana.setTypeface(null, Typeface.BOLD);
            this.contenitore.setBackgroundResource(R.color.colorBackgraundBlock);
            this.frecciaLaterale.setVisibility(View.VISIBLE);
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA FRECCIA                                               */
/*================================================================================================*/
    public void setFrecciaVersoDestra(){
        this.frecciaLaterale.setImageResource(R.drawable.ic_baseline_chevron_right_24);
    }
/*------------------------------------------------------------------------------------------------*/
    public void setFrecciaVersoSinistra(){
        this.frecciaLaterale.setImageResource(R.drawable.ic_baseline_chevron_left_24);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  IMPOSTAZIONI LISTENER                                         */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA ON CLICK LISTENER                                     */
/*================================================================================================*/
    public void setOnClickListener(View.OnClickListener listener){
        this.contenitore.setOnClickListener(listener);
    }
/*================================================================================================*/
/*################################################################################################*/
}
