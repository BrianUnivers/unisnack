package it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import it.tn.unitn.disi.ppsmet.ActivityManager.LoginActivity.LoginViewModel;
import it.tn.unitn.disi.ppsmet.ActivityManager.ProfiloUtenteActivity.ProfiloUtenteViewModel;
import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.InterfaceViewModel.MenuProfiloUtenteViewModel;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;
import it.tn.unitn.disi.ppsmet.GestioneAssetPerFoto.GestioneFotoDaAsset;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ShapedImageView;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.TitoloSopraContenutoView;
import it.tn.unitn.disi.ppsmet.R;


public class MenuProfiloUtenteFragment extends Fragment {
/*################################################################################################*/
/*                                  INIZIALIZZAZIONE VALORI                                       */
/*################################################################################################*/
    private MenuProfiloUtenteViewModel viewModel    = null;
    private LiveData<Utenti>           utenteAttivo = null;
    private GestioneFotoDaAsset        ACCEDI_FOTO = null;
/*================================================================================================*/
/*                                  COSTRUTTORE                                                   */
/*================================================================================================*/
    /**
     * Questo è il metodo costruttore del fragment.
     */
    public MenuProfiloUtenteFragment() {
        super();
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private ViewGroup                rootView;
    private TitoloSopraContenutoView titolo;
    private ShapedImageView          immagineUtente;
    private TextView                 nomeConomeUtente;
/*================================================================================================*/
/*                                  CREAZIONE DELL FRAGMENT                                       */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'inizializazione del layout del fragment ed inoltre contiene le loro
     * possibili interazioni con l'utente.
     * In aggiunta viene richesto un modelView che è quello usato dall'activity in modo tale da accedere
     * alla sua stessa istanza, in modo tale da utilizarlo come metodo per passare parameti da una parte
     * all'altra.
     *
     * In questo caso viene usato per ricevere come informazioni del utente attivo.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_menu_profilo_utente,
                                        container, false);
/*------------------------------------------------------------------------*/
        ACCEDI_FOTO = new GestioneFotoDaAsset(getActivity());
/*------------------------------------------------------------------------*/
        try {
            viewModel = new ViewModelProvider(getActivity()).get(ProfiloUtenteViewModel.class);
        }catch (Exception e){
            viewModel=null;
        }
/*------------------------------------------------------------------------*/
        nomeConomeUtente = rootView.findViewById(R.id.txt_NomeCognome);
        immagineUtente   = rootView.findViewById(R.id.img_Utente);
        immagineUtente.setCircularCenter();
        titolo = TitoloSopraContenutoView.newInstance(  rootView,
                                                        R.id.include_titolo_sopra_contenuto,
                                                        R.string.ttl_ProfiloUtente);
/*------------------------------------------------------------------------*/
        if(viewModel!=null){
            richiediDatiEInserisci();
        }
/*------------------------------------------------------------------------*/
        return rootView;
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  ADATTA AL UTENTE                                              */
/*################################################################################################*/
/*================================================================================================*/
/*                                  RICHIEDI UTENTE E ADATTA I DATO                               */
/*================================================================================================*/
    /**
     * Questo metodo permette di richedere un oggetto livedata contenete l'utente attivo così che ogni
     * cambiamento venga notificato e appotato al layout.
     */
    private void richiediDatiEInserisci(){
        utenteAttivo = viewModel.getUtenteAttivo();
        utenteAttivo.observe(getViewLifecycleOwner(), new Observer<Utenti>() {
            @Override
            public void onChanged(Utenti utenti) {
                if(utenti!=null){
                    impostaDatiUtenti(utenti);
                }
            }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA I DATI UTENTI NEL LAYOUT                              */
/*================================================================================================*/
    /**
     * Questo metodo permette di inserire i dati utenete nel layout ed inserisce se esiste la foto
     * dell'utente.
     * @param utenti
     */
    private void impostaDatiUtenti(Utenti utenti){
        nomeConomeUtente.setText(utenti.getNome()+"\n"+utenti.getCognome());
        if(utenti.getUrlImmagine()!=null){
            if(immagineUtente.setImageBitmapIfNotNUll(ACCEDI_FOTO.getFoto(utenti.getUrlImmagine(),R.drawable.img_userdefault))){
                immagineUtente.setCircularCenter();
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}