package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Ingredienti_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Pietanze_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Ingredienti_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.M_Fornisce_P_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Mense_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.P_Contiene_A_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Pietanze_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.TipoMenu_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.TipoMenu_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.M_fornisce_P;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.P_contiene_A;
import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;

public class PietanzeForniteMensa_ThreadAggiornamento extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA ANNO ACCADEMICO LOCALE           */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "PietanzeForniteMensa" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_PietanzeForniteMensa";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista dei Pietanze del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private List<M_fornisce_P>    listaMFornisceP;
        private Long              idMense;
        private Long              idTipoMenu;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * P_contiene_A.
         * @param listaMFornisceP Questo parametro contiene la lista di tutti gli elementi
         *                        tra qui quelli nuovi e quelli già presenti ma da modificare.
         */
        public ThreadAggiornamento(List<M_fornisce_P> listaMFornisceP,Long idMense,Long idTipoMenu) {
            super();
            this.listaMFornisceP = listaMFornisceP;
            this.idMense         = idMense;
            this.idTipoMenu      = idTipoMenu;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                                  */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella Pietanze.");
            aggionaTabellaPietanze(listaMFornisceP,idMense,idTipoMenu);
            Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella Pietanze.");
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param listaMFornisceP
         */
        private synchronized void aggionaTabellaPietanze(List<M_fornisce_P> listaMFornisceP,Long idMense,Long idTipoMenu){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella Pietanze.");
            //Dati presenti nel database locale.
            List<M_fornisce_P> listaMForniscePLocale = mForniscePDao.getMForniscePByIdOfMensa_TipoMenu(idMense,idTipoMenu);
            if((listaMForniscePLocale==null)||(listaMForniscePLocale.isEmpty())){
                inserisciListaElementi(listaMFornisceP);
            }else if((listaMFornisceP==null)||(listaMFornisceP.isEmpty())) {
                eliminaListaElementi(listaMForniscePLocale);
            }else{
                HashMap<String,M_fornisce_P> hashMapMForniscePLocali = convertiListaInHashMap(listaMForniscePLocale);

                List<M_fornisce_P> listaMForniscePNuove = new LinkedList<>();
                List<M_fornisce_P> listaMForniscePAggiornate = new LinkedList<>();
                //Serve solo per poter conoscere quali elementi caccelare tra quelli già presenti localmente.
                List<M_fornisce_P> listaVechieMForniscePDaMantenere = new LinkedList<>();

                Iterator<M_fornisce_P> iterator = listaMFornisceP.iterator();
                while (iterator.hasNext()){
                    M_fornisce_P MFornisceP =iterator.next();
                    if(MFornisceP!=null){
                        M_fornisce_P MForniscePVecchia = hashMapMForniscePLocali.get(MFornisceP.getId());
                        if(MForniscePVecchia==null){
                            listaMForniscePNuove.add(MFornisceP);
                        }else if(!MForniscePVecchia.equals(MFornisceP)){
                            listaMForniscePAggiornate.add(MFornisceP);
                            listaVechieMForniscePDaMantenere.add(MForniscePVecchia);
                        }else{
                            listaVechieMForniscePDaMantenere.add(MForniscePVecchia);
                        }
                    }
                }
                //MANTENGO SOLO GLI ELEMNETI CHE DEVO CANCELLARE
                listaMForniscePLocale.removeAll(listaVechieMForniscePDaMantenere);
                inserisciListaElementi(listaMForniscePNuove);
                aggiornaListaElementi(listaMForniscePAggiornate);
                eliminaListaElementi(listaMForniscePLocale);
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         GENERAZIONE HASH MAP FROM LIST                                         */
/*================================================================================================*/
        private synchronized HashMap<String,M_fornisce_P> convertiListaInHashMap(List<M_fornisce_P> lista){
            HashMap<String,M_fornisce_P> ris=new HashMap<>();
            if(lista!=null){
                Iterator<M_fornisce_P> iterator = lista.iterator();
                while (iterator.hasNext()){
                    M_fornisce_P elemento = iterator.next();
                    if((elemento!=null) && (elemento.getId()!=null)){
                        ris.put(elemento.getId(),elemento);
                    }
                }
            }
            return ris;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO INSERIMENTO NUOVI ELEMENTI                                      */
/*================================================================================================*/
        private synchronized void inserisciListaElementi(List<M_fornisce_P> lista){
            if(lista!=null){
                Iterator<M_fornisce_P> iterator = lista.iterator();
                while (iterator.hasNext()){
                    M_fornisce_P elemento=iterator.next();
                    if(elemento!=null){
                        mForniscePDao.insert(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO AGGIORNAMENTO ELEMENTI PRESENTI                                 */
/*================================================================================================*/
        private synchronized void aggiornaListaElementi(List<M_fornisce_P> lista){
            if(lista!=null){
                Iterator<M_fornisce_P> iterator = lista.iterator();
                while (iterator.hasNext()){
                    M_fornisce_P elemento=iterator.next();
                    if(elemento!=null){
                        mForniscePDao.update(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaListaElementi(List<M_fornisce_P> lista){
            if(lista!=null){
                Iterator<M_fornisce_P> iterator = lista.iterator();
                while (iterator.hasNext()){
                    M_fornisce_P elemento=iterator.next();
                    if(elemento!=null){
                        mForniscePDao.delete(elemento.getAnno(),elemento.getMese(),elemento.getGiornoSettimana(),
                                elemento.getId_mensa(),elemento.getId_pietanza(),elemento.getId_tipoMenu());
                    }
                }
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_Pietanze_Dao fbPietanzeDao = new FB_Pietanze_Dao();
    //QUERY FIREBASE
    private static LiveData<List<TipoMenu>> AllTipoMenu= null;
    //ROOM DATABASE
    protected static M_Fornisce_P_Dao mForniscePDao    = null;
    public  static Pietanze_Dao        pietanzeDao     = null;
    private static Mense_Dao           mensaDao        = null;
    private static TipoMenu_Dao        tipoMenuDao     = null;
/*------------------------------------------------------------------------------------------------*/
    //DIPENDENZE
    protected static Pietanze_ThreadAggiornamento dipendezaPietanze  = null;
    protected static TipoMenu_ThreadAggiornamento dipendezaTipoMenu  = null;
    protected static Mense_ThreadAggiornamento    dipendezaMense     = null;
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaPietanze(){
        if(mForniscePDao==null){
            mForniscePDao = db.MForniscePDao();
        }
        if(pietanzeDao==null){
            pietanzeDao = db.PietanzeDao();
        }
        if(mensaDao==null){
            mensaDao = db.MenseDao();
        }
        if(tipoMenuDao==null){
            tipoMenuDao = db.TipoMenuDao();
        }
        if(AllTipoMenu==null){
            AllTipoMenu= tipoMenuDao.getAll_Synchronized();
        }
        if(!AllTipoMenu.hasObservers()){//SE NON HA già osservatori per rilevare aggiornamenti
            AllTipoMenu.observeForever(new Observer<List<TipoMenu>>() {
                private  List<TipoMenu>  lastListTipoMenu = new LinkedList<>();
                private  List<TipoMenu>  temp = new LinkedList<>();
                @Override
                public void onChanged(List<TipoMenu> listaTipoMenu) {
                    if((listaTipoMenu!=null)){
                        temp.clear();
                        temp.addAll(listaTipoMenu);
                        if(!lastListTipoMenu.isEmpty()){
                            listaTipoMenu.removeAll(lastListTipoMenu);
                        }
                        //Solo quelli nuovi
                        Iterator<TipoMenu> iterator = listaTipoMenu.iterator();
                        while (iterator.hasNext()){
                            TipoMenu tipoMenu =iterator.next();
                            if((tipoMenu!=null) && (tipoMenu.getId()!=null)){
                                //Aggiungi un altro ossservatore
//--------------------------------------------------------------------------------------------------
                                mensaDao.getAll_Synchronized().observeForever(new MyMenseObserver(tipoMenu.getId()));
//--------------------------------------------------------------------------------------------------
                            }
                        }
                        lastListTipoMenu.clear();
                        lastListTipoMenu.addAll(temp);
                    }
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
private static class MyMenseObserver implements Observer<List<Mense>>{
    private Long idTipoMenu;
    private  List<Mense>  lastListMense = new LinkedList<>();
    private  List<Mense>  temp = new LinkedList<>();
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public MyMenseObserver(Long idTipoMenu) {
        this.idTipoMenu =idTipoMenu;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO CHIAMATO NEL MOMENTO DEL CAMBIAMNETO                            */
/*================================================================================================*/
    @Override
    public void onChanged(List<Mense> listaMense) {
        if((listaMense!=null)) {
            temp.clear();
            temp.addAll(listaMense);
            if (!lastListMense.isEmpty()) {
                listaMense.removeAll(lastListMense);
            }
            //Solo quelli nuovi
            Iterator<Mense> iterator = listaMense.iterator();
            while (iterator.hasNext()) {
                Mense mense = iterator.next();
                if ((mense != null) && (mense.getId() != null)) {
                    fbPietanzeDao.getMForniscePByIdOfMensa_TipoMenu_Synchronized(mense.getId(),this.idTipoMenu).observeForever(
                                                    new MyPietanzeForniteMenseObserver(mense.getId(),this.idTipoMenu));
                }
            }
            lastListMense.clear();
            lastListMense.addAll(temp);
        }
    }
/*================================================================================================*/
}
/*################################################################################################*/
private static class MyPietanzeForniteMenseObserver implements Observer<List<M_fornisce_P>>{
    private Long idMensa;
    private Long idTipoMenu;
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public MyPietanzeForniteMenseObserver(Long idMensa,Long idTipoMenu) {
        this.idTipoMenu = idTipoMenu;
        this.idMensa    = idMensa;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO CHIAMATO NEL MOMENTO DEL CAMBIAMNETO                            */
/*================================================================================================*/
    @Override
    public void onChanged(List<M_fornisce_P> lista) {
        if((lista!=null)){
            executor.execute(new ThreadAggiornamento(lista,idMensa,idTipoMenu));
        }
    }
/*================================================================================================*/
}
/*################################################################################################*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public PietanzeForniteMensa_ThreadAggiornamento(Application application) {
        super(application);

        if(dipendezaTipoMenu==null){
            dipendezaTipoMenu = new TipoMenu_ThreadAggiornamento(application);
        }
        if(dipendezaMense==null){
            dipendezaMense = new Mense_ThreadAggiornamento(application);
        }
        if(dipendezaPietanze==null){
            dipendezaPietanze = new Pietanze_ThreadAggiornamento(application);
        }
        gestioneAggiornametoTabellaPietanze();
    }
/*================================================================================================*/
/*################################################################################################*/
}
