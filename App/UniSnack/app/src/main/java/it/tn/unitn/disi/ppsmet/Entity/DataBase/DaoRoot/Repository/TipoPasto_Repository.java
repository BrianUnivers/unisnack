package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.TipoPasto_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.TipoMenu_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.TipoPasto_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;

public class TipoPasto_Repository extends TipoPasto_ThreadAggiornamento {
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA TIPO PASTO(e le sue derivazioni seguendo*/
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
    private LiveData<List<TipoPasto>> mAllTipoPasto;
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public TipoPasto_Repository(Application application) {
        super(application);
        //AllTipoPasto get any time the same result
        mAllTipoPasto = tipoPastoDao.getAll_Synchronized();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERT                                                                 */
/*================================================================================================*/
    /*public Future<Long> insert(TipoPasto tipoPasto){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                    @Override
                    public Long call() throws Exception {
                        return tipoPastoDao.insert(tipoPasto);
                    }
                });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    /*public void deleteAll(){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            tipoPastoDao.deleteAll();
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    /*public void update(TipoPasto tipoPasto){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            tipoPastoDao.update(tipoPasto);
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<TipoPasto>> getAll(){
        return mAllTipoPasto;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    /*public LiveData<TipoPasto> getByPrimaryKey(Long id){
        return tipoPastoDao.getByPrimaryKey_Synchronized(id);
    }
/*================================================================================================*/
/*################################################################################################*/
}
