package it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface;

import java.util.Date;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;

public interface OnFragmentModificaMenuDelSingoloGiornoTipoPastoEventListener {
/*================================================================================================*/
/*                                  SALVA TEPORANEAMENTE LE MODIFICHE DEL SINGOLO ORDONAZIONI     */
/*================================================================================================*/
    /**
     * Questo metodo permette di salvare le modifiche applicate al menu del singolo giorno che si è appena
     * personalizzato.
     */
    public void salvaModificheMenuSettimanaleDelGiorno(Long idUtente,Date data,TipoPasto tipoPasto,List<PietanzeDellaMense> listaPietanzeDellaMense);
/*================================================================================================*/
/*================================================================================================*/
/*                                   ANNULLA MODIFICHE DEL SINGOLO ORDONAZIONI                    */
/*================================================================================================*/
    /**
     * Questo metodo permette di annulare le modifice appotate al menu settimanale che si è appanna
     * modificato.
     */
    public void annullaModificheMenuSettimanale(Date data,TipoPasto tipoPasto);
/*================================================================================================*/
}
