package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.google.firebase.database.DatabaseReference;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class FB_AnnoAccademico_Dao extends ConnesioneAFirebase<AnnoAccademico> {
/*################################################################################################*/
    private static String     DATA_FORMAT  = "dd/MM/yyyy";
    private static SimpleDateFormat CONVERT_DATA = new SimpleDateFormat(DATA_FORMAT);
/*================================================================================================*/
/*                         INSERISCI DATI UTENTI                                                  */
/*================================================================================================*/
    public static HashMap<String,Object> inserisciDatiAnnoAccademico(HashMap<String,Object> istruzioni,String root,AnnoAccademico aa){
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_KEY).toString(),
                aa.getId());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_DATA_INIZIO).toString(),
                CONVERT_DATA.format(aa.getDataInizio()));
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_DATA_FINE).toString(),
                CONVERT_DATA.format(aa.getDataFine()));
        return istruzioni;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<AnnoAccademico>> getAll_OneTime(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_ANNO_ACCADEMICO);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_AnnoAccademico.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<AnnoAccademico>> getAll(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_ANNO_ACCADEMICO);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_AnnoAccademico.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<AnnoAccademico>> getAll_Synchronized(){
        DatabaseReference ref = super.getReferenceFireBase().getReference().child(L_ANNO_ACCADEMICO);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_AnnoAccademico.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    public LiveData<AnnoAccademico> getByPrimaryKey_OneTime(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_ANNO_ACCADEMICO)
                .child(id.toString());
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_AnnoAccademico.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<AnnoAccademico> getByPrimaryKey(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_ANNO_ACCADEMICO)
                .child(id.toString());
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_AnnoAccademico.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<AnnoAccademico> getByPrimaryKey_Synchronized(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_ANNO_ACCADEMICO)
                .child(id.toString());
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_AnnoAccademico.Deserializer_Object());
    }
/*================================================================================================*/
/*################################################################################################*/
}
