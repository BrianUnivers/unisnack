package it.tn.unitn.disi.ppsmet.Entity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.Uni_ha_C;

@Entity(tableName = "tableUtenti",
        foreignKeys = {
            @ForeignKey(entity = Uni_ha_C.class,
                parentColumns = {"id_universita",
                                 "id_corso"},     //Colanna della classe a qui si rifierisce
                childColumns  = {"id_universita",
                                 "id_corso"},     //ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE),
            @ForeignKey(entity = AnnoAccademico.class,
                    parentColumns = "id",     //Colanna della classe a qui si rifierisce
                    childColumns  = "id_AA",  //ForeignKey
                    onUpdate = ForeignKey.CASCADE,
                    onDelete = ForeignKey.CASCADE)},
        indices = {
                @Index(value = {"email"}, unique = true),
                @Index(value = {"salt"},  unique = true),
                @Index(value = {"id_universita","id_corso"}),
                @Index(value = {"id_AA"})})
public class Utenti {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @PrimaryKey
    private Long   id;
    @NonNull
    @ColumnInfo(name = "nome")
    private String nome;
    @NonNull
    @ColumnInfo(name = "cognome")
    private String cognome;
    @NonNull
    @ColumnInfo(name = "nato")
    private Date   nato;
    @NonNull
    @ColumnInfo(name = "email")
    private String email;
    @NonNull
    @ColumnInfo(name = "password")
    private String password;
    @NonNull
    @ColumnInfo(name = "salt")
    private String salt;
    @ColumnInfo(name = "immagine")
    private byte[] immagine = null;
    @ColumnInfo(name = "urlImmagine")
    private String urlImmagine = null;
    @NonNull
    @ColumnInfo(name = "id_universita")
    private Long id_Universita;
    @NonNull
    @ColumnInfo(name = "id_corso")
    private Long id_Corso;
    @NonNull
    @ColumnInfo(name = "matricola")
    private Integer matricola;
    @NonNull
    @ColumnInfo(name = "id_AA")
    private Long id_AA;
    @Ignore
    private AnnoAccademico annoAccademico = null;
    @NonNull
    @ColumnInfo(name = "statoTessera")
    private Boolean statoTessera;
    @ColumnInfo(name = "qrCode")
    private byte[] qrCode = null;
    @ColumnInfo(name = "urlQRCode")
    private String urlQRCode =null;
    @ColumnInfo(name = "codiceABarre")
    private byte[] codiceABarre =null;
    @ColumnInfo(name = "urlCodiceABarre")
    private String urlCodiceABarre =null;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    @Ignore
    public Utenti() { }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Utenti(Long id,
                  @NonNull String nome,
                  @NonNull String cognome,
                  @NonNull Date nato,
                  @NonNull String email,
                  @NonNull String password,
                  @NonNull String salt,
                  @NonNull Long id_Universita,
                  @NonNull Long id_Corso,
                  @NonNull Integer matricola,
                  @NonNull Long id_AA,
                  @NonNull Boolean statoTessera) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.nato = nato;
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.id_Universita = id_Universita;
        this.id_Corso = id_Corso;
        this.matricola = matricola;
        this.id_AA = id_AA;
        this.statoTessera = statoTessera;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Utenti(Long id,
                  String nome,
                  String cognome,
                  Date nato,
                  String email,
                  String password,
                  String salt,
                  Long id_Universita,
                  Long id_Corso,
                  Integer matricola,
                  AnnoAccademico annoAccademico,
                  Boolean statoTessera) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.nato = nato;
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.id_Universita = id_Universita;
        this.id_Corso = id_Corso;
        this.matricola = matricola;
        this.setAnnoAccademico(annoAccademico);
        this.statoTessera = statoTessera;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Utenti(Long id,
                  @NonNull String nome,
                  @NonNull String cognome,
                  @NonNull Date nato,
                  @NonNull String email,
                  @NonNull String password,
                  @NonNull  String salt,
                  byte[] immagine,
                  String urlImmagine,
                  @NonNull Long id_Universita,
                  @NonNull Long id_Corso,
                  @NonNull Integer matricola,
                  @NonNull Long id_AA,
                  @NonNull Boolean statoTessera) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.nato = nato;
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.immagine = immagine;
        this.urlImmagine = urlImmagine;
        this.id_Universita = id_Universita;
        this.id_Corso = id_Corso;
        this.matricola = matricola;
        this.id_AA = id_AA;
        this.statoTessera = statoTessera;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Utenti(Long id,
                  String nome,
                  String cognome,
                  Date nato,
                  String email,
                  String password,
                  String salt,
                  byte[] immagine,
                  String urlImmagine,
                  Long id_Universita,
                  Long id_Corso,
                  Integer matricola,
                  AnnoAccademico annoAccademico,
                  Boolean statoTessera) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.nato = nato;
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.immagine = immagine;
        this.urlImmagine =urlImmagine;
        this.id_Universita = id_Universita;
        this.id_Corso = id_Corso;
        this.matricola = matricola;
        this.setAnnoAccademico(annoAccademico);
        this.statoTessera = statoTessera;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Utenti(Long id,
                  @NonNull String nome,
                  @NonNull String cognome,
                  @NonNull Date nato,
                  @NonNull String email,
                  @NonNull String password,
                  @NonNull String salt,
                  @NonNull Long id_Universita,
                  @NonNull Long id_Corso,
                  @NonNull Integer matricola,
                  @NonNull Long id_AA,
                  @NonNull Boolean statoTessera,
                  byte[] qrCode,
                  String urlQRCode,
                  byte[] codiceABarre,
                  String urlCodiceABarre) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.nato = nato;
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.id_Universita = id_Universita;
        this.id_Corso = id_Corso;
        this.matricola = matricola;
        this.id_AA = id_AA;
        this.statoTessera = statoTessera;
        this.qrCode = qrCode;
        this.urlQRCode = urlQRCode;
        this.codiceABarre = codiceABarre;
        this.urlCodiceABarre = urlCodiceABarre;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Utenti(Long id,
                  String nome,
                  String cognome,
                  Date nato,
                  String email,
                  String password,
                  String salt,
                  Long id_Universita,
                  Long id_Corso,
                  Integer matricola,
                  AnnoAccademico annoAccademico,
                  Boolean statoTessera,
                  byte[] qrCode,
                  String urlQRCode,
                  byte[] codiceABarre,
                  String urlCodiceABarre) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.nato = nato;
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.id_Universita = id_Universita;
        this.id_Corso = id_Corso;
        this.matricola = matricola;
        this.setAnnoAccademico(annoAccademico);
        this.statoTessera = statoTessera;
        this.qrCode = qrCode;
        this.urlQRCode = urlQRCode;
        this.codiceABarre = codiceABarre;
        this.urlCodiceABarre = urlCodiceABarre;
    }
//--------------------------------------------------------------------------------------------------
    public Utenti(Long id,
                  @NonNull String nome,
                  @NonNull String cognome,
                  @NonNull Date nato,
                  @NonNull String email,
                  @NonNull String password,
                  @NonNull String salt,
                  byte[] immagine,
                  String urlImmagine,
                  @NonNull Long id_Universita,
                  @NonNull Long id_Corso,
                  @NonNull Integer matricola,
                  @NonNull Long id_AA,
                  @NonNull Boolean statoTessera,
                  byte[] qrCode,
                  String urlQRCode,
                  byte[] codiceABarre,
                  String urlCodiceABarre) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.nato = nato;
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.immagine = immagine;
        this.urlImmagine = urlImmagine;
        this.id_Universita = id_Universita;
        this.id_Corso = id_Corso;
        this.matricola = matricola;
        this.id_AA = id_AA;
        this.statoTessera = statoTessera;
        this.qrCode = qrCode;
        this.urlQRCode = urlQRCode;
        this.codiceABarre = codiceABarre;
        this.urlCodiceABarre = urlCodiceABarre;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Utenti(Long id,
                  String nome,
                  String cognome,
                  Date nato,
                  String email,
                  String password,
                  String salt,
                  byte[] immagine,
                  String urlImmagine,
                  Long id_Universita,
                  Long id_Corso,
                  Integer matricola,
                  AnnoAccademico annoAccademico,
                  Boolean statoTessera,
                  byte[] qrCode,
                  String urlQRCode,
                  byte[] codiceABarre,
                  String urlCodiceABarre) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.nato = nato;
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.immagine = immagine;
        this.urlImmagine = urlImmagine;
        this.id_Universita = id_Universita;
        this.id_Corso = id_Corso;
        this.matricola = matricola;
        this.setAnnoAccademico(annoAccademico);
        this.statoTessera = statoTessera;
        this.qrCode = qrCode;
        this.urlQRCode = urlQRCode;
        this.codiceABarre = codiceABarre;
        this.urlCodiceABarre = urlCodiceABarre;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    public Long getId() {
        return id;
    }
//--------------------------------------
    public void setId(Long id) {
        this.id = id;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getNome() {
        return nome;
    }
//--------------------------------------
    public void setNome(@NonNull String nome) {
        this.nome = nome;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getCognome() {
        return cognome;
    }
//--------------------------------------
    public void setCognome(@NonNull String cognome) {
        this.cognome = cognome;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Date getNato() {
        return nato;
    }
//--------------------------------------
    public void setNato(@NonNull Date nato) {
        this.nato = nato;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getEmail() {
        return email;
    }
//--------------------------------------
    public void setEmail(@NonNull String email) {
        this.email = email;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getPassword() {
        return password;
    }
//--------------------------------------
    public void setPassword(@NonNull String password) {
        this.password = password;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getSalt() {
        return salt;
    }
//--------------------------------------
    public void setSalt(@NonNull String salt) {
        this.salt = salt;
    }
//--------------------------------------------------------------------------------------------------
    public byte[] getImmagine() {
        return immagine;
    }
//--------------------------------------
    public Bitmap getBitmapImmagine() {
        //Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
        //Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);
        return BitmapFactory.decodeByteArray(immagine, 0, immagine.length);
    }
//--------------------------------------
    public void setImmagine(byte[] immagine) {
        this.immagine = immagine;
    }
//--------------------------------------------------------------------------------------------------
    public String getUrlImmagine() {
        return urlImmagine;
    }
//--------------------------------------
    public void setUrlImmagine(String urlImmagine) {
        this.urlImmagine = urlImmagine;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_Universita() {
        return id_Universita;
    }
//--------------------------------------
    public void setId_Universita(@NonNull Long id_Universita) {
        this.id_Universita = id_Universita;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_Corso() {
        return id_Corso;
    }
//--------------------------------------
    public void setId_Corso(@NonNull Long id_Corso) {
        this.id_Corso = id_Corso;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_AA() {
        return id_AA;
    }
//--------------------------------------
    public void setId_AA(@NonNull Long id_AA) {
        this.id_AA = id_AA;
        if((annoAccademico!=null) && (id_AA!=null)  && (id_AA!=annoAccademico.getId())){
            annoAccademico=null;
        }else if(id_AA==null){
            annoAccademico=null;//TODO: Oppure mantenrere lo stesso id  di tipo
        }
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public AnnoAccademico getAnnoAccademico() {
        return annoAccademico;
    }
//--------------------------------------
    @Ignore
    public boolean setAnnoAccademico(AnnoAccademico annoAccademico) {
        boolean ris = true;
        if((id_AA!=null) && (id_AA==annoAccademico.getId()) ){
            this.annoAccademico = annoAccademico;
        }else if((id_AA==null) && (annoAccademico!=null)){
            this.id_AA = annoAccademico.getId();
            this.annoAccademico = annoAccademico;
        }else{
            ris = false;
        }
        return ris;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Boolean getStatoTessera() {
        return statoTessera;
    }
//--------------------------------------
    public void setStatoTessera(@NonNull Boolean statoTessera) {
        this.statoTessera = statoTessera;
    }
//--------------------------------------------------------------------------------------------------
    public byte[] getQrCode() {
        return qrCode;
    }
//--------------------------------------
    public Bitmap getBitmapQrCode() {
        //Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
        //Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);
        return BitmapFactory.decodeByteArray(qrCode, 0, qrCode.length);
    }
//--------------------------------------
    public void setQrCode(byte[] qrCode) {
        this.qrCode = qrCode;
    }
//--------------------------------------------------------------------------------------------------
    public String getUrlQRCode() {
        return urlQRCode;
    }
//--------------------------------------
    public void setUrlQRCode(String urlQRCode) {
        this.urlQRCode = urlQRCode;
    }

//--------------------------------------------------------------------------------------------------
    public byte[] getCodiceABarre() {
        return codiceABarre;
    }
//--------------------------------------
    public Bitmap getBitmapCodiceABarre() {
        //Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
        //Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);
        return BitmapFactory.decodeByteArray(codiceABarre, 0, codiceABarre.length);
    }
//--------------------------------------
    public void setCodiceABarre(byte[] codiceABarre) {
        this.codiceABarre = codiceABarre;
    }
//--------------------------------------------------------------------------------------------------
    public String getUrlCodiceABarre() {
        return urlCodiceABarre;
    }
//--------------------------------------
    public void setUrlCodiceABarre(String urlCodiceABarre) {
        this.urlCodiceABarre = urlCodiceABarre;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Integer getMatricola() {
        return matricola;
    }
//--------------------------------------
    public void setMatricola(@NonNull Integer matricola) {
        this.matricola = matricola;
    }
//==================================================================================================
//==================================================================================================
//                                      EQUALS E HASH_CODE
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utenti)) return false;
        Utenti utenti = (Utenti) o;
        return Objects.equals(getId(), utenti.getId()) &&
                getNome().equals(utenti.getNome()) &&
                getCognome().equals(utenti.getCognome()) &&
                getNato().equals(utenti.getNato()) &&
                getEmail().equals(utenti.getEmail()) &&
                getPassword().equals(utenti.getPassword()) &&
                getSalt().equals(utenti.getSalt()) &&
                Objects.equals(getUrlImmagine(), utenti.getUrlImmagine()) &&
                getId_Universita().equals(utenti.getId_Universita()) &&
                getId_Corso().equals(utenti.getId_Corso()) &&
                getMatricola().equals(utenti.getMatricola()) &&
                getId_AA().equals(utenti.getId_AA()) &&
                getStatoTessera().equals(utenti.getStatoTessera()) &&
                Objects.equals(getUrlQRCode(), utenti.getUrlQRCode()) &&
                Objects.equals(getUrlCodiceABarre(), utenti.getUrlCodiceABarre());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNome(), getCognome(), getNato(), getEmail(), getPassword(),
                getSalt(), getUrlImmagine(), getId_Universita(), getId_Corso(), getMatricola(),
                getId_AA(), getStatoTessera(), getUrlQRCode(), getUrlCodiceABarre());
    }
//==================================================================================================
//##################################################################################################
}
