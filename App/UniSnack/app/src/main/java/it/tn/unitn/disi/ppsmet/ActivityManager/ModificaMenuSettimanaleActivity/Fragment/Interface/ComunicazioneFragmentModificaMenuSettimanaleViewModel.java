package it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface.Parameters.SelezionaGiornoTipoPasto;
import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.InterfaceViewModel.MenuProfiloUtenteViewModel;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;

public class ComunicazioneFragmentModificaMenuSettimanaleViewModel extends MenuProfiloUtenteViewModel {
/*################################################################################################*/
/*================================================================================================*/
/*                                  COSTRUTTORE VIEW MODEL                                        */
/*================================================================================================*/
    /**
     * Questo metodo costruttore del viewmode inizializza le repository che si interfacciano con entrambi
     * i database sia interno che estreno a seconda delle operazioni che vengono richeste. Che verranno
     * utilizate dall'activity e i suoi fragment.
     * @param application
     */
    public ComunicazioneFragmentModificaMenuSettimanaleViewModel(@NonNull Application application) {
        super(application);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GENERA LISTA DATE DELLA SETTIMANA                             */
/*################################################################################################*/
    private static final Integer LUNGHEZZA_SETTIMANA_LAVORATIVA = 5;
/*================================================================================================*/
/*                                  SELEZIONE PRIMO GIORNO DELLA SETTIMANA                        */
/*================================================================================================*/
    /**
     * Questo metodo deve infividuare il primo giorno della settimana corrente inteso come il primo
     * lunedì che ha almeno un giorno lavorativo che sia nel futuro, cioè che deve ancora finire
     * in modo tale da permette di avere almeno un giorno che sia possibile modificare il menu personalizato.
     * @return
     */
    private Date selezionePrimoGiornoDellaSettimana(){
        Date oggi = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(oggi);
        int offset = 0;
        switch(cal.get(Calendar.DAY_OF_WEEK)){
            case Calendar.MONDAY:
                offset = 0;
                break;
            case Calendar.TUESDAY:
                offset = -1;
                break;
            case Calendar.WEDNESDAY:
                offset = -2;
                break;
            case Calendar.THURSDAY:
                offset = -3;
                break;
            case Calendar.FRIDAY:
                offset = -4;
                break;
            case Calendar.SATURDAY:
                offset =  2;
                break;
            case Calendar.SUNDAY:
                offset =  1;
                break;
        }
        cal.add(Calendar.DATE, offset);
        return cal.getTime();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                 GENERA LISTA GIORNI DELLA SETTIMANA NON ANCORA CONCLUSA        */
/*================================================================================================*/
    /**
     * Questo metodo fornisce al richiedente la lista dei gioni della settimana LAVORATIVA che non
     * si è ancora conclusa. Cioè la settimana che ha,compreso quello attuale un giorno che deve ancora
     * essere trascorso.
     * @return
     */
    public List<Date> generaListaGiorniSettimanaNonAncoraConclusa(){
        List<Date> listDate = new LinkedList<>();
        Date dataInizio = selezionePrimoGiornoDellaSettimana();
        Calendar cal = Calendar.getInstance();
            cal.setTime(dataInizio);
            for(int i=0;i<LUNGHEZZA_SETTIMANA_LAVORATIVA; i++){
            listDate.add(cal.getTime());
            cal.add(Calendar.DATE, 1);
        }
        return listDate;
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  LISTA ORDINAZIONI PROPOSTE                                    */
/*################################################################################################*/
    /**
     * Questo parametro contiene la lista delle Ordinazioni che potranno essere rese definitive.
     */
    protected HashMap<String,Ordinazioni> listaProposteOrdinazione = new HashMap<>();
    /**
     * Questo variabile contiene la lista delle Ordinazioni che potranno essere rese definitive che
     * permettono di avaere dei observer per mantenere sempre aggiornato i richiedenti di questo parametro.
     */
    protected MediatorLiveData<List<Ordinazioni>> ldListaProposteOrdinazione = new MediatorLiveData<>();
/*================================================================================================*/
/*                                  IMPOSTAZIONE INIZIALE DEI EVENTUALI VECCHI ORDINI             */
/*================================================================================================*/
    /**
     * Questo metodo permette di aggiungere gli evenuali ordi fatti in precedenza nella lista
     * dei ordini con dati impostati.
     *
     * Questo metodo aggiona anche la lista LiveData così da aggiornare tutti gli ascoltatori.
     * @param listOrdini
     */
    public void nuovaImpostazioneOrdiniGiaPresenti(List<Ordinazioni> listOrdini){
        if(listOrdini!=null){

            Log.i("CIAO","2     INSERISCO TUTTO NUOVO "+ listOrdini.size());
        }else{
            Log.i("CIAO"," 2     INSERISCO TUTTO NUOVO null");
        }
        listaProposteOrdinazione = new HashMap<>();
        if(listOrdini!=null){
            Iterator<Ordinazioni> iterator = listOrdini.iterator();
            Log.i("CIAO"," 2     INSERISCO dd ");
            while (iterator.hasNext()){
                Log.i("CIAO"," 2     INSERISCO  ");
                Ordinazioni ordinazioni = iterator.next();
                if(ordinazioni!=null){
                    String key = generaChieaveDaOrdinazioni(ordinazioni);
                    Log.i("CIAO"," 2     Key  "+ key);
                    listaProposteOrdinazione.put(key,ordinazioni);
                }
            }
        }
        List<Ordinazioni> list = new LinkedList<>();
        list.addAll(listaProposteOrdinazione.values());
        Log.i("CIAO"," 2     Fine  "+ list.size());
        ldListaProposteOrdinazione.setValue(list);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  GENERA CHIAVE PER HASHMAP DEI ORDINI PROPOSTI                 */
/*================================================================================================*/
    /**
     * Questo metodo permette di generare una chiave univoca per il giorno e il tipo di pasto di
     * un ordinazione.
     * @param ordinazioni
     * @return
     */
    protected String generaChieaveDaOrdinazioni(Ordinazioni ordinazioni){
        String ris = null;
        if( (ordinazioni!=null) && (ordinazioni.getGiorno()!=null) && (ordinazioni.getId_tipoPasto()!=null)){
            ris =generaChieaveDaOrdinazioni(ordinazioni.getGiorno(),ordinazioni.getId_tipoPasto());
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo permette di generare una chiave univoca per il giorno e il tipo di pasto senza
     * conoscere l'ordinazione associata.
     * @param data
     * @param idTipoPasto
     * @return
     */
    private String generaChieaveDaOrdinazioni(Date data, Long  idTipoPasto){
        String ris = null;
        if( (data!=null) && (idTipoPasto!=null) ){
            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
            ris = new StringBuffer()
                    .append(fmt.format(data))
                    .append("-").append(idTipoPasto).toString();
        }
        return ris;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  AGGIUNGI E RIMUOVI SINGOLA ORDINAZIONE DA QUELLE PROPOSTE     */
/*================================================================================================*/
    /**
     * Questo metdo permette di inserire e sovrascrivere un ordinazione con un altra, a quelle che
     * sono proposte non ancora rese definitive.
     *
     * Questo metodo aggiona anche la lista LiveData così da aggiornare tutti gli ascoltatori.
     */
    public boolean aggiungiNuovaOrdinazione(Ordinazioni ordinazioni){
        boolean ris = false;
        String key = generaChieaveDaOrdinazioni(ordinazioni);
        if(key!=null){
            ris = true;
            listaProposteOrdinazione.put(key,ordinazioni);
        }
        List<Ordinazioni> list = new LinkedList<>();
        list.addAll(listaProposteOrdinazione.values());
        ldListaProposteOrdinazione.setValue(list);
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metdo permette di cancellare  un ordinazione da quelle che sono proposte non ancora
     * rese definitive.
     *
     * Questo metodo aggiona anche la lista LiveData così da aggiornare tutti gli ascoltatori.
     */
    public boolean rimuoviOrdinazione(Date data, Long  idTipoPasto){
        boolean ris = false;
        String key = generaChieaveDaOrdinazioni(data,idTipoPasto);
        if(key!=null){
            ris = true;
            listaProposteOrdinazione.remove(key);
        }
        List<Ordinazioni> list = new LinkedList<>();
        list.addAll(listaProposteOrdinazione.values());
        ldListaProposteOrdinazione.setValue(list);
        return ris;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  RICHIEDI ORDINAZIONI PROPOSTE                                 */
/*================================================================================================*/
    /**
     * Questo metodo permette di ricevere un oggetto LiveData che conterrà l'ultima stato delle ordinazioni
     * proposte che non sono ancora definitive in modo tale da permettendo tramite un suo observer
     * di rilevare dei cambiamei nei dati. Così facendo si da la possibilità al richiedente di questo
     * oggetto di rimanere aggiornato sul valore contenuto.
     * @return
     */
    public LiveData<List<Ordinazioni>> getListaProposteOrdinazione(){
        return ldListaProposteOrdinazione;
}
/*================================================================================================*/
/*================================================================================================*/
/*                                  ESISTE  ORDINAZIONI PROPOSTE                                  */
/*================================================================================================*/
    /**
     * Questo metodo permette sapere se esiste un ordinazione per quel giorno e tipo pasto specifico.
     * Attenzione! Questo non dice se l'ordinazione è un tra quelle che sono proposte o è una tra
     * quelle già presenti nel database.
     * @param data
     * @param idTipoPasto
     * @return
     */
    public boolean esisteOrdinazioneProposta(Date data, Long  idTipoPasto){
        boolean ris = false;
        if((data!=null) && (idTipoPasto!=null) ){
            String key = generaChieaveDaOrdinazioni(data,idTipoPasto);
            if(this.listaProposteOrdinazione.get(key)!=null){
                ris=true;
            }
        }
        return ris;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  RICHIEDI ORDINAZIONI PROPOSTE                                 */
/*================================================================================================*/
    /**
     * Questo metodo permette di ricevere la lista delle ordinazioni presenti in questo momento.
     * @return
     */
    public List<Ordinazioni> getListaProposteOrdinazioneInQuestoMomento(){
        List<Ordinazioni> listaOrdinazioni = new LinkedList();
        listaOrdinazioni.addAll(listaProposteOrdinazione.values());
        return listaOrdinazioni;
    }
/*================================================================================================*/
/*################################################################################################*/


/*################################################################################################*/
/*                                  LISTA ORDINAZIONE PIETANZA                                    */
/*################################################################################################*/
    /**
     * Questo parametro contiene la lista delle pietanze con i dati della mensa e al tipo menu ed
     * associto alla chiave generata dalla data e il tipo pasto della ordinazione a cui fanno riferimento
     * che potranno essere rese definitive.
     */
    protected HashMap<String, List<OrdinazionePietanzeDellaMenseConVoto>> listaPietanzeOrdinate = new HashMap<>();
/*================================================================================================*/
/*                                  AGGIUNGI E RICHIEDI LISTA PIETANZE DA QUELLE PROPOSTE          */
/*================================================================================================*/
    /**
     * Questo metodo permette di inserire la lista di pietanze ordinate in una mensa specifica, nel
     * giorno e tipo pasto specificato.
     * @param data
     * @param idTipoPasto
     * @param listaPietanzeForniteDaMesa
     * @return
     */
    public boolean aggiungiNuovaListaPietanze(Date data, Long  idTipoPasto,List<PietanzeDellaMense> listaPietanzeForniteDaMesa){
        boolean ris = false;
        if((data!=null) && (idTipoPasto!=null)){
            String key = generaChieaveDaOrdinazioni(data,idTipoPasto);
            if(key!=null){
                ris = true;
                List<OrdinazionePietanzeDellaMenseConVoto> lista = new LinkedList<>();
                Iterator<PietanzeDellaMense> iterator = listaPietanzeForniteDaMesa.iterator();
                while (iterator.hasNext()){
                    PietanzeDellaMense pietanzeDellaMense=iterator.next();
                    if(pietanzeDellaMense!=null){
                        OrdinazionePietanzeDellaMenseConVoto ordinazionePietanza = new OrdinazionePietanzeDellaMenseConVoto(pietanzeDellaMense);
                        lista.add(ordinazionePietanza);
                    }
                }
                listaPietanzeOrdinate.put(key,lista);
            }
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo permette di estrarre una lista di pietanze di una certa mensa e per tun certo
     * tipo di menu associati ad una data e un tipo di pasto.
     * Questo metodo ritorna null se i dati passati non sono entrambi diversi da null o se non è presente
     * una lista delle pietanze.
     * @param data
     * @param idTipoPasto
     * @return
     */
    public List<OrdinazionePietanzeDellaMenseConVoto> getListaPietanze(Date data, Long  idTipoPasto){
        List<OrdinazionePietanzeDellaMenseConVoto> ris = null;
        if((data!=null) && (idTipoPasto!=null)){
            String key = generaChieaveDaOrdinazioni(data,idTipoPasto);
            if(key!=null){
                ris = listaPietanzeOrdinate.get(key);
            }
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/

/*################################################################################################*/
/*                                  AZZIONI PER PASSARE LA GIORNO E TIPO PASTO SELEZIONATO        */
/*################################################################################################*/
    /**
     * Questo variabile contiene il valore di un oggetto LiveData in cui si potrà inserire un giorno
     * associato a un Tipo Pietanza e la fornirà ai richiedenti tramite un observer su questo oggetto.
     * In conclusione questo oggetto una valta chiamato il metodo "setGiornoTipoPastoSelezzionata()"
     * conterrà la mensa seleziona.
     */
    protected MediatorLiveData<SelezionaGiornoTipoPasto> giornoTipoPastoSelezionata = new MediatorLiveData<>();
/*================================================================================================*/
/*                                  IMPOSTA GIORNO E TIPO PASTO SELEZIONATA                       */
/*================================================================================================*/
    /**
     * Questo metodo permette d'impostare e sovrascrivere/cambiare la coppia selezionata di una Data
     * e di un oggetto TipoPasto se e soltanto se entrambi gli oggetti passati come paramento sono
     * entrambi diversi da null.
     * @param date
     * @param tipoPasto
     */
    public void setGiornoTipoPastoSelezzionato(Date date, TipoPasto tipoPasto){
        if( (date!=null) && (tipoPasto!=null) ){
            giornoTipoPastoSelezionata.setValue(new SelezionaGiornoTipoPasto(date,tipoPasto));
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  RICHIEDI GIORNO E TIPO PASTO SELEZIONATA                      */
/*================================================================================================*/
    /**
     * Questo metodo permette di ricevere un oggetto LiveData che conterrà l'ultima associazione tra
     * selezionato
     * permettendo tramite un suo observer di rilevare dei cambiamei nei dati. Così facendo si da la
     * possibilità al richiedente di questo oggetto di rimanere aggiornato sul valore contenuto.
     * @return
     */
    public LiveData<SelezionaGiornoTipoPasto> getGiornoTipoSelezzionato(){
        return giornoTipoPastoSelezionata;
    }
/*================================================================================================*/
/*################################################################################################*/
}
