package it.tn.unitn.disi.ppsmet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.MenuDelleMenseActivity.MenuDelleMenseViewModel;
import it.tn.unitn.disi.ppsmet.ActivityManager.ProfiloUtenteActivity.ProfiloUtenteViewModel;
import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Pietanze_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;
import it.tn.unitn.disi.ppsmet.GestioneAssetPerFoto.GestioneFotoDaAsset;
import it.tn.unitn.disi.ppsmet.GestioneRicordaSessione.GestioneSessione;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ShapedImageView;

public class MainActivity extends AppCompatActivity {
    private TextView textView;
    private Button button,btnSessione;
    private AppCompatActivity my = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ShapedImageView imageView = findViewById(R.id.shapedImageView);
        imageView.setImageOnShapeMask(R.drawable.img_menu_personalizza_menu_settimanale);


        Utenti utenti = new Utenti();
        utenti.setId(1L);
        utenti.setId_Corso(2L);
        utenti.setId_Universita(3L);

        btnSessione = findViewById(R.id.btnSessione);
        btnSessione.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GestioneSessione.cancellaSessioneAperte(getApplicationContext());
            }
        });


       /* FirebaseDatabase databaseReference = ConnesioneAFirebase.getReferenceFireBase();
        HashMap<String,Object> inserisci = new HashMap<>();
        inserisci.put("Ciao","prova");

        inserisci.put("//Ciao_all/1",77);
        inserisci.put("/Ciao_all/2",5);
        databaseReference.getReference().child("s").push().updateChildren(inserisci);

*/


        button = findViewById(R.id.btn1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                }catch (Exception e){
                    Log.d("MyTag-Errore",e.toString());
                    System.out.println(e);
                }

            }
        });

        textView = (TextView) findViewById(R.id.id_1);

        //MenuDelleMenseViewModel viewModel = new ViewModelProvider(this).get(MenuDelleMenseViewModel.class);;





        Button buttonddd= findViewById(R.id.map);
        buttonddd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri gmmIntentUri = Uri.parse("geo:37.7749,-122.4194");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW );
                mapIntent.setData(gmmIntentUri);
                startActivity(mapIntent);
            }
        });




        Date now = new Date();
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE"); // the day of the week spelled out completely
        button.setText(simpleDateformat.format(now));


        /*------------------------------------------------------------------------*/
        //ee viewModel =  new ViewModelProvider(this).get(ee.class);
        /*------------------------------------------------------------------------*/
       // viewModel.getAll();



/*
        LiveData<List<Pietanze>> data = new FB_Pietanze_Dao().getByIdOfTipoPietanza_Synchronized(2L);
        data.observe(this, new Observer<List<Pietanze>>() {
            @Override
            public void onChanged(List<Pietanze> listaPietanze) {
                textView.setText("");
                Iterator<Pietanze> iterator = listaPietanze.iterator();
                while (iterator.hasNext()){
                    Pietanze element = iterator.next();
                    if(element!=null) {//Importante
                        textView.setText(textView.getText() +
                                element.getNome()+"\n\t"+
                                element.getTipoPietanza().getId()+"\t"+
                                element.getTipoPietanza().getNome()+ "\n\n"
                                );
                    }
                }
            }
        });*/







///OLD TEST
/*
        LiveData<List<Tessere>> data = new FB_Tessere_Dao().getByIdOfUtente_Synchronized(utenti);
        data.observe(this, new Observer<List<Tessere>>() {
            @Override
            public void onChanged(List<Tessere> listaTessere) {
                textView.setText("");
                Iterator<Tessere> iterator = listaTessere.iterator();
                while (iterator.hasNext()){
                    Tessere element = iterator.next();
                    if(element!=null) {//Importante
                        if ((element.corsi != null) && (element.matricola != null)) {
                            textView.setText(textView.getText() +
                                    "Matricola: " + element.matricola +
                                    "\n\t Univerasita: " + element.universita.getNome() +
                                    "\n\t Corso: " + element.corsi.getNome() +
                                    "\n\t AA: " + element.annoAccademico.getDataFine().toString() +
                                    "\n\t\t ID_AA: " + element.annoAccademico.getId().toString() +
                                    "\n\t Stato tessera: " + element.getStatoTessera().toString() +
                                    "\n\n");
                        } else {
                            textView.setText(textView.getText() +
                                    "Matricola: " + "null" +
                                    "\n\t Univerasita: " + element.universita.getNome() +
                                    "\n\t Corso: " + "null" +
                                    "\n\t AA: " + element.annoAccademico.getDataFine().toString() +
                                    "\n\t\t ID_AA: " + element.annoAccademico.getId().toString() +
                                    "\n\t Stato tessera: " + element.getStatoTessera().toString() +
                                    "\n\n");
                        }
                    }

                }
            }
        });






*/











         ///OLD TEST
        /*
        LiveData<Long> data = ConnesioneAFirebase.getIdPietanze();
       /* data.observe(this, new Observer() {
            @Override
            public void onChanged(Object id) {
                if ((id instanceof Long) ) {
                    // update the UI here with values in the snapshot
                    textView.setText(id.toString());

                }
            }
        });* /




        LiveData<List<Allergeni>> data2 = new FB_Allergeni_Dao().getByIdOfUtente_Synchronized(utenti);

       data2.observe(this, new Observer() {
            @Override
            public void onChanged(Object ris) {
                if ((ris instanceof  List) ) {
                    Iterator iterator = ((List) ris).iterator();
                    textView.setText("");
                    Allergeni allergeni;
                    while (iterator.hasNext()){
                         allergeni = (Allergeni) iterator.next();
                        textView.setText(textView.getText()+allergeni.getId().toString()+" = \t"+allergeni.getNome()+" "+allergeni.getDescrizione()+"\n");
                    }

                }
            }
       });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Allergeni> list =new LinkedList<>();
                for(int i=0; i<10; i++){
                    Allergeni allergeni = new Allergeni(200L+i,"nome 20"+i,"descrizione 20"+i);
                    list.add(allergeni);
                }
                LiveData<List<Allergeni>> data22 = new FB_Allergeni_Dao().filterOnlyExists(list);
                data22.observe(my, new Observer<List<Allergeni>>() {
                    @Override
                    public void onChanged(List<Allergeni> allergenis) {
                        new FB_Allergeni_Dao().insertAllergia(utenti,allergenis);
                    }
                });
            }
        });



       LiveData<Allergeni> data3 = new FB_Allergeni_Dao().getByPrimaryKey(1L);
        /*
        data3.observe(this, new Observer() {
            @Override
            public void onChanged(Object ris) {

                if ((ris instanceof  Allergeni) ) {
                    textView.setText(((Allergeni)ris).getId().toString()+" = \t"+((Allergeni)ris).getNome()+" "+((Allergeni)ris).getDescrizione()+"\n");

                }
            }
        });*/



    }
}