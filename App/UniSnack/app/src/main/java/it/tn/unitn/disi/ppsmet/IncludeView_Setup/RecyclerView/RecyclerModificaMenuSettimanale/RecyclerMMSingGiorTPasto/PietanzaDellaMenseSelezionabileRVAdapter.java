package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerModificaMenuSettimanale.RecyclerMMSingGiorTPasto;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.GestioneAssetPerFoto.GestioneFotoDaAsset;
import it.tn.unitn.disi.ppsmet.R;

public class PietanzaDellaMenseSelezionabileRVAdapter extends RecyclerView.Adapter<PietanzaDellaMenseSelezionabileRVAdapter.MyViewHoder> {
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE INTERNA PADRE                                              */
/*################################################################################################*/
    /**
     * Questa classe è quella quella che viene usata per permettere alla RecyclingView Adapter di definire
     * il layout singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public ViewGroup v_mainLayout;
        public ImageView v_ImmaginePietanza;
        public TextView  v_NomePietanza;
        public CardView  v_CardPietanza;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout       = itemView.findViewById(R.id.cly_RowElemento);
            v_NomePietanza     = itemView.findViewById(R.id.txt_NomePietanza);
            v_ImmaginePietanza = itemView.findViewById(R.id.img_Piatto);
            v_CardPietanza     = itemView.findViewById(R.id.crd_ElementoPietanza);
        }
/*================================================================================================*/
    }
/*################################################################################################*/

/*################################################################################################*/
/*                              CLASSE PRINCIPALE ModificaMenuSettimanaleTipoPastoRVAdapter       */
/*################################################################################################*/
    private HashMap<Long,PietanzeDellaMense> elementiSelezionati = new HashMap<>();
/*------------------------------------------------------------------------------------------------*/
    private GestioneFotoDaAsset ACCEDI_FOTO = null;
/*------------------------------------------------------------------------------------------------*/
    private Context comtext=null;
    private List<PietanzeDellaMense> listaPietanze = null;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public PietanzaDellaMenseSelezionabileRVAdapter(Context comtext,
                                                    List<PietanzeDellaMense> listaPietanze) {
        this.comtext = comtext;
        this.listaPietanze = listaPietanze;
        this.ACCEDI_FOTO = new GestioneFotoDaAsset(comtext);
        this.elementiSelezionati = new HashMap<>();
    }
/*================================================================================================*/


/*================================================================================================*/
/*                              CERAZIONE DEL SINGOLO ELEMETOTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(comtext);
        View bloccoElemeto = inflater.inflate(R.layout.row_pietanza_con_immagine, parent,false);
        MyViewHoder myViewHoder = new MyViewHoder(bloccoElemeto);
        return myViewHoder;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              INSERIRE L'ELEMETO NEL LAYOUT                                     */
/*================================================================================================*/
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore ModificaMenuSettimanaleTipoPastoRVAdapter.
     */
    @Override
    public void onBindViewHolder(@NonNull MyViewHoder holder, int position) {
        final PietanzeDellaMense elemento  = listaPietanze.get(position);
        inserisciPietanzeLayout(holder, elemento);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  IMPOSTA COME BOTTONE APERTURA DEL LINK IN UN BROWER
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        holder.v_CardPietanza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Long key = elemento.getPietanze().getId();
                if(key!=null){
                    if(elementiSelezionati.get(key)!=null){
                        //Cancella la selezione
                        elementiSelezionati.remove(key);
                        indicaElementoComeNonSelezionato(holder);
                    }else{
                        //Indica come selezionata
                        elementiSelezionati.put(key,elemento);
                        indicaElementoComeSelezionato(holder);
                    }
                }
            }
        });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
/*================================================================================================*/
/*                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE                  */
/*================================================================================================*/
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaPietanze.size();
    }
/*================================================================================================*/


/*================================================================================================*/
/*                              INSERISCI I DATI NEL LAYOUT                                       */
/*================================================================================================*/
    /**
     * Questo metodo si occupa di inserire i dati della pietanza che è quello attuale  nel layout del
     * singolo elemeto.
     */
    private void inserisciPietanzeLayout(@NonNull MyViewHoder holder, PietanzeDellaMense pietanzeDellaMense){
        Pietanze pietanza = pietanzeDellaMense.getPietanze();
        String nomePietanza =pietanza.getNome();
        nomePietanza =  nomePietanza.substring(0, 1).toUpperCase() + nomePietanza.substring(1);
        holder.v_NomePietanza.setText(nomePietanza);
        Bitmap immaginePietanza = ACCEDI_FOTO.getFoto(pietanza.getUrlImmagine(),R.drawable.img_nessun_piatto);
        if(immaginePietanza!=null){
            holder.v_ImmaginePietanza.setImageBitmap(immaginePietanza);
        }else{
            holder.v_ImmaginePietanza.setImageResource(R.drawable.img_nessun_piatto);
        }
        Long key = pietanza.getId();
        if( (key!=null) && (elementiSelezionati.get(key)!=null)){
            //Elemento selezionato
            indicaElementoComeSelezionato(holder);
        }else{
            //Elemento non selezionato
            indicaElementoComeNonSelezionato(holder);
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                              LAYOUT SELEZIONATO E DESELEZIONATO                                */
/*================================================================================================*/
    /**
     * Questo metodo permette di indicare un elemento come selezionato.
     * @param holder
     */
    private void indicaElementoComeSelezionato(@NonNull MyViewHoder holder){
        int color = comtext.getResources().getColor(R.color.colorPrimaryLight,null);
        holder.v_CardPietanza.setCardBackgroundColor(color);
        holder.v_NomePietanza.setTypeface(null, Typeface.BOLD);
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo permette di indicare un elemento come non selezionato.
     * @param holder
     */
    private void indicaElementoComeNonSelezionato(@NonNull MyViewHoder holder){
        int color = comtext.getResources().getColor(R.color.colorSecondary,null);
        holder.v_CardPietanza.setCardBackgroundColor(color);
        holder.v_NomePietanza.setTypeface(null, Typeface.NORMAL);
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                              GET PIETANZE SELEZIONATE                                          */
/*================================================================================================*/
    /**
     * Questo metodo permette di restituire la lista di pietanze selezionate in questo momento.
     * @return
     */
    public List<PietanzeDellaMense> getListaPietanzeSelezionate(){
        List<PietanzeDellaMense> ris = new LinkedList<>();
        ris.addAll(elementiSelezionati.values());
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
}