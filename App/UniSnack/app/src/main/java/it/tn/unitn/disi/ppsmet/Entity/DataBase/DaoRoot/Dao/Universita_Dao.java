package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.Date;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

@Dao
public abstract class Universita_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(Universita universita);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Query("DELETE FROM tableUniversita")
    public abstract void deleteAll();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE By ID                                                    */
/*================================================================================================*/
    @Query("DELETE FROM tableUniversita WHERE id == :id")
    public abstract void delete(Long id);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = Universita.class,
            onConflict = OnConflictStrategy.ABORT)
    public abstract void update(Universita universita);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    @Query("SELECT * FROM tableUniversita")
    public abstract List<Universita> getAll();
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableUniversita")
    public abstract LiveData<List<Universita>> getAll_Synchronized();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    @Query("SELECT * FROM tableUniversita WHERE id == :id")
    public abstract Universita getByPrimaryKey(Long id);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableUniversita WHERE id == :id")
    public abstract LiveData<Universita> getByPrimaryKey_Synchronized(Long id);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
    @Query( "SELECT Uni.* " +
            "FROM tableUniversita AS Uni, tableUtenti AS U " +
            "WHERE U.id == :idUtenti AND Uni.id == U.id_universita " +
          "UNION " +
            "SELECT Uni.* " +
            "FROM tableUniversita AS Uni, tableU_partecipa_Uni AS P " +
            "WHERE P.id_utente == :idUtenti AND Uni.id == P.id_universita")
    public abstract List<Universita> getByIdOfUtente(Long idUtenti);
/*------------------------------------------------------------------------*/
    @Query( "SELECT Uni.* " +
            "FROM tableUniversita AS Uni, tableUtenti AS U " +
            "WHERE U.id == :idUtenti AND Uni.id == U.id_universita " +
            "UNION " +
            "SELECT Uni.* " +
            "FROM tableUniversita AS Uni, tableU_partecipa_Uni AS P " +
            "WHERE P.id_utente == :idUtenti AND Uni.id == P.id_universita")
    public abstract LiveData<List<Universita>> getByIdOfUtente_Synchronized(Long idUtenti);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE INTO SPECIFIC DAY                              */
/*================================================================================================*/
    @Query( "SELECT Uni.* " +
            "FROM tableUniversita AS Uni, tableUtenti AS U , tableAnnoAccademico AS AA " +
            "WHERE U.id == :idUtenti AND Uni.id == U.id_universita AND U.id_AA == AA.id AND " +
                " :date BETWEEN AA.dataInizio AND AA.dataFine " +
            "UNION " +
            "SELECT Uni.* " +
            "FROM tableUniversita AS Uni, tableU_partecipa_Uni AS P , tableAnnoAccademico AS AA " +
            "WHERE P.id_utente == :idUtenti AND Uni.id == P.id_universita AND P.id_AA == AA.id AND " +
                " :date BETWEEN AA.dataInizio AND AA.dataFine ")
    public abstract List<Universita> getByIdOfUtente(Long idUtenti,Date date);
/*------------------------------------------------------------------------*/
    @Query( "SELECT Uni.* " +
            "FROM tableUniversita AS Uni, tableUtenti AS U , tableAnnoAccademico AS AA " +
            "WHERE U.id == :idUtenti AND Uni.id == U.id_universita AND U.id_AA == AA.id AND " +
            " :date BETWEEN AA.dataInizio AND AA.dataFine " +
            "UNION " +
            "SELECT Uni.* " +
            "FROM tableUniversita AS Uni, tableU_partecipa_Uni AS P , tableAnnoAccademico AS AA " +
            "WHERE P.id_utente == :idUtenti AND Uni.id == P.id_universita AND P.id_AA == AA.id AND " +
            " :date BETWEEN AA.dataInizio AND AA.dataFine ")
    public abstract LiveData<List<Universita>> getByIdOfUtente_Synchronized(Long idUtenti,Date date);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE TODAY                                          */
/*================================================================================================*/
    @Transaction
    public List<Universita> getByIdOfUtenteAtToday(Long idUtenti){
        return this.getByIdOfUtente(idUtenti,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Universita>> getByIdOfUtenteAtToday_Synchronized(Long idUtenti){
        return this.getByIdOfUtente_Synchronized(idUtenti,new Date());
    }
/*================================================================================================*/
/*################################################################################################*/
}
