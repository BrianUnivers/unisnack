package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List_In_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.M_fornisce_P;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;

public class Deserializer_MFornisceP implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto MFornisceP.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
/**
 * Qusrto metodo restituisce un singolo ogetto "AnnoAccademico" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "AnnoAccademico".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public static List<M_fornisce_P> estraiDaDataSnapshot(DataSnapshot dataSnapshot,Long idMensa,Long idTipoMenu){
        List<M_fornisce_P> listaRisultati = new LinkedList<>();
        if((dataSnapshot.exists()) && (idMensa!=null)){
            //Campi obbligatori
            if(idTipoMenu!=null){
                //Menu di un anno mese e giorno settimanale
                Integer anno            = dataSnapshot.child(P_ANNO).getValue(Integer.class);
                Integer mese            = dataSnapshot.child(P_MESE).getValue(Integer.class);
                Integer giornoSettimana = dataSnapshot.child(P_GIORNO_SETTIMANA).getValue(Integer.class);
                //Campi obbligatori
                if((anno!=null) && (mese!=null) && (giornoSettimana!=null) &&
                        (dataSnapshot.child(L_PIETANZE).exists())){

                    Iterator<DataSnapshot> iteratorPietanze= dataSnapshot.child(L_PIETANZE).getChildren().iterator();
                    while (iteratorPietanze.hasNext()){
                        DataSnapshot dataSnapshotPietanza = iteratorPietanze.next();

                        Long idPietanza = dataSnapshotPietanza.child(P_KEY).getValue(Long.class);
                        //Campi obbligatori
                        if(idPietanza!=null){
                            listaRisultati.add(new M_fornisce_P(idMensa,idPietanza,mese,anno,giornoSettimana,idTipoMenu) );
                        }
                    }
                }
            }
        }
        return listaRisultati;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<AnnoAccademico>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
public static class Deserializer_List extends GenericDeserializer_List_In_List<M_fornisce_P> {
    private Long idMense;
    private Long idTipoMenu;
/*================================================================================================*/
/*                         COSTRUTTORE                                                            */
/*================================================================================================*/
    public Deserializer_List( Long idMense,Long idTipoMenu) {
        super();
        this.idMense= idMense;
        this.idTipoMenu = idTipoMenu;
    }
/*================================================================================================*/
        @Override
        public List<M_fornisce_P> estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_MFornisceP.estraiDaDataSnapshot(dataSnapshot,this.idMense,this.idTipoMenu);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
