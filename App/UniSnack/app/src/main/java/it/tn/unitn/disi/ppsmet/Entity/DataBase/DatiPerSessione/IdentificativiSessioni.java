package it.tn.unitn.disi.ppsmet.Entity.DataBase.DatiPerSessione;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Objects;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.Uni_ha_C;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

@Entity(tableName = "tableIdentificativiSessioni",
       foreignKeys = {
                @ForeignKey(entity = Utenti.class,
                        parentColumns = "id",       //Colanna della classe a qui si rifierisce
                        childColumns  = "idUtente", //ForeignKey
                        onUpdate = ForeignKey.CASCADE,
                        onDelete = ForeignKey.CASCADE)},
        indices = { @Index(value = "idSessione", unique = true)})
public class IdentificativiSessioni {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @PrimaryKey
    private Long idUtente;
    @NonNull
    @ColumnInfo(name = "idSessione")
    private String  idSessione;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
//--------------------------------------------------------------------------------------------------
    public IdentificativiSessioni(@NonNull Long idUtente,
                                  @NonNull String idSessione) {
        this.idUtente = idUtente;
        this.idSessione = idSessione;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    public Long getIdUtente() {
        return idUtente;
    }
//--------------------------------------
    public void setIdUtente(Long idUtente) {
        this.idUtente = idUtente;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getIdSessione() {
        return idSessione;
    }
//--------------------------------------
    public void setIdSessione(@NonNull String idSessione) {
        this.idSessione = idSessione;
    }
//==================================================================================================
//##################################################################################################
}
