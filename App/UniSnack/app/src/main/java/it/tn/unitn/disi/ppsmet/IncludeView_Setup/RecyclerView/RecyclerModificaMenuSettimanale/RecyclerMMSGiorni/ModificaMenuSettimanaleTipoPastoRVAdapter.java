package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerModificaMenuSettimanale.RecyclerMMSGiorni;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface.OnFragmentSelezioneGiornoETipoPastoEventListener;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.R;

public class ModificaMenuSettimanaleTipoPastoRVAdapter extends RecyclerView.Adapter<ModificaMenuSettimanaleTipoPastoRVAdapter.MyViewHoder> {
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE INTERNA PADRE                                              */
/*################################################################################################*/
    /**
     * Questa classe è quella quella che viene usata per permettere alla RecyclingView Adapter di definire
     * il layout singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public ViewGroup v_mainLayout;
        public Button   v_buttonTipoPasto;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout      = itemView.findViewById(R.id.cly_RowElemento);
            v_buttonTipoPasto = itemView.findViewById(R.id.btn_TipoPietanza);
        }
/*================================================================================================*/
    }
/*################################################################################################*/

/*################################################################################################*/
/*                              CLASSE PRINCIPALE ModificaMenuSettimanaleTipoPastoRVAdapter       */
/*################################################################################################*/
    private Context comtext=null;
    private List<TipoPasto> listaTipoPasto = null;
    private Date giornoDelOrdinazione = null;
    private Date giornoLimeteModifiche = null;
    /**
     * Questo parametro contiene le ordinazioni di un singolo giorno che però sono associate alla
     * chieave uguale al id del TipoPasto di cui fanno parte.
     * Così si identifica rapidamente la presenaz o l'assenza di una ordinazione pregressa.
     */
    private HashMap<Long, Ordinazioni> ordinazioniNelGiorno;
/*------------------------------------------------------------------------------------------------*/
    private OnFragmentSelezioneGiornoETipoPastoEventListener listener =null;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public ModificaMenuSettimanaleTipoPastoRVAdapter(Context comtext,
                                                     List<TipoPasto> listaTipoPasto,
                                                     Date giornoDelOrdinazione,
                                                     HashMap<Long, Ordinazioni> ordinazioniNelGiorno,
                                                     OnFragmentSelezioneGiornoETipoPastoEventListener listener) {
        this.comtext = comtext;
        this.listaTipoPasto = listaTipoPasto;
        this.giornoDelOrdinazione = giornoDelOrdinazione;
        this.ordinazioniNelGiorno = ordinazioniNelGiorno;
        this.listener = listener;
        if(this.listaTipoPasto==null){
            this.listaTipoPasto = new LinkedList<>();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH,-1);
        calendar.set(Calendar.MILLISECOND,999);
        calendar.set(Calendar.SECOND,59);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.HOUR_OF_DAY,23);
        this.giornoLimeteModifiche= calendar.getTime();
    }
/*================================================================================================*/


/*================================================================================================*/
/*                              CERAZIONE DEL SINGOLO ELEMETOTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(comtext);
        View bloccoElemeto = inflater.inflate(R.layout.row_modifica_menu_settimanale_tipo_pasto, parent,false);
        MyViewHoder myViewHoder = new MyViewHoder(bloccoElemeto);
        return myViewHoder;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              INSERIRE L'ELEMETO NEL LAYOUT                                     */
/*================================================================================================*/
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore ModificaMenuSettimanaleTipoPastoRVAdapter.
     */
    @Override
    public void onBindViewHolder(@NonNull MyViewHoder holder, int position) {
        final  TipoPasto elemento  = listaTipoPasto.get(position);
        inserisciTipoPastoLayout(holder, elemento);
        if((giornoDelOrdinazione!=null) && (giornoLimeteModifiche.before(giornoDelOrdinazione))){
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//                                  IMPOSTA COME BOTTONE APERTURA DEL LINK IN UN BROWER
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            holder.v_buttonTipoPasto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.apriModificaManuDelGiornoTipoPasto(giornoDelOrdinazione,elemento);
                }
            });
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }
    }
/*================================================================================================*/
/*                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE                  */
/*================================================================================================*/
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaTipoPasto.size();
    }
/*================================================================================================*/


/*================================================================================================*/
/*                              INSERISCI I DATI NEL LAYOUT                                       */
/*================================================================================================*/
    /**
     * Questo metodo si occupa di inserire i dati della pietanza che è quello attuale  nel layout del
     * singolo elemeto.
     */
    private void inserisciTipoPastoLayout(@NonNull MyViewHoder holder, TipoPasto tipoPasto){
        holder.v_buttonTipoPasto.setText(tipoPasto.getNome());
        Drawable icone = null;
        if( (ordinazioniNelGiorno!=null) && (ordinazioniNelGiorno.get(tipoPasto.getId())!=null)){
            //C'è un ordinazione pregressa
            holder.v_buttonTipoPasto.setBackgroundTintList(comtext.getResources().getColorStateList(R.color.colorPrimaryLight,null));
            icone = comtext.getResources().getDrawable(R.drawable.ic_menu_laterale_baseline_restaurant_menu_24, null);
        }else{
            //Non c'è un ordinazione pregressa
            holder.v_buttonTipoPasto.setBackgroundTintList(comtext.getResources().getColorStateList(R.color.colorSecondaryLight,null));
            icone = comtext.getResources().getDrawable(R.drawable.ic_baseline_add_24, null);

        }
        if( (giornoDelOrdinazione!=null) && (giornoLimeteModifiche.before(giornoDelOrdinazione)) ){
            holder.v_buttonTipoPasto.setCompoundDrawablesWithIntrinsicBounds(icone,null,null,null);
        }else{
            holder.v_buttonTipoPasto.setCompoundDrawablesWithIntrinsicBounds(null,null,null,null);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}