package it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface.OnFragmentSelezioneGiornoETipoPastoEventListener;
import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.ModificaMenuSettimanaleViewModel;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.BarraConDuePulsanti;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerModificaMenuSettimanale.RecyclerMMSGiorni.ModificaMenuSettimanaleGiorniRVAdapter;
import it.tn.unitn.disi.ppsmet.R;


public class SelezioneGiornoETipoPastoFragment extends Fragment {
    /**
     * Questo parametro coterrà se sarà possibile l'accesso a determinati metodi dell'acticity contenete
     * il seguete fragment.
     */
    private OnFragmentSelezioneGiornoETipoPastoEventListener listener = null;
/*################################################################################################*/
/*                                  INIZIALIZZAZIONE VALORI                                       */
/*################################################################################################*/
/*================================================================================================*/
/*                                  COSTRUTTORE                                                   */
/*================================================================================================*/
    /**
     * Questo è il metodo costruttore del fragment.
     */
    public SelezioneGiornoETipoPastoFragment() {
        super();
    }
/*================================================================================================*/

/*################################################################################################*/
/*                                  COMUNICAZIONE CON L'ACTIVITY                                  */
/*################################################################################################*/
/*================================================================================================*/
/*                                  DEFINIZIONE LINK DI COMUNICAZIONE CON L'ACTIVITY              */
/*================================================================================================*/
    /**
     * Questo metodo permete di estrarre un eventuale implementeazione da parte dell'activity(contesto
     * padre) così da poter accedere ad dei metodi esterni al fragmen.
     * @param context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (OnFragmentSelezioneGiornoETipoPastoEventListener) context;
        } catch (ClassCastException castException) {
            listener = null;
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private Fragment fragment = this;
/*------------------------------------------------------------------------*/
    private ModificaMenuSettimanaleViewModel viewModel    = null;
/*------------------------------------------------------------------------*/
    private RecyclerView recyclerView ;
    private BarraConDuePulsanti barraConDuePulsanti;
/*------------------------------------------------------------------------*/
    private ViewGroup rootView;
/*------------------------------------------------------------------------*/
    private LiveData<List<Ordinazioni>> ldListaProposteOrdinazione;
    private LiveData<List<TipoPasto>>   ldListaTipoPasto;
/*------------------------------------------------------------------------*/
    private List<Ordinazioni> listaProposteOrdinazione = null;
    private List<TipoPasto>   listaTipoPasto = null;
    private List<Date> listDate = new LinkedList<>();
/*================================================================================================*/
/*                                  CREAZIONE DELL'FRAGMENT                                       */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'inizializazione del layout del fragment ed inoltre contiene le loro
     * possibili interazioni con l'utente.
     * In aggiunta viene richesto un modelView che è quello usato dall'activity in modo tale da accedere
     * alla sua stessa istanza, in modo tale da utilizarlo come metodo per passare parameti da una parte
     * all'altra.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */

    @SuppressLint("FragmentLiveDataObserve")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = (ViewGroup)  inflater.inflate(R.layout.fragment_selezione_giorno_e_tipo_pasto, container, false);
/*------------------------------------------------------------------------*/
        recyclerView = rootView.findViewById(R.id.rcl_ListaGiorniETipiPasti);
        recyclerView.setLayoutManager(new LinearLayoutManager(fragment.getContext()));
        impostaLayoutStatico();
        impostaAzzioniPulsanti();
/*------------------------------------------------------------------------*/
        try {
            viewModel = new ViewModelProvider(getActivity()).get(ModificaMenuSettimanaleViewModel.class);
        }catch (Exception e){
            viewModel=null;
        }
/*------------------------------------------------------------------------*/
/*                      POSSO ACCEDERE AI DATI DEL VIEW MODEL             */
/*------------------------------------------------------------------------*/
        if(viewModel!=null){
            barraConDuePulsanti.setOnPrimaryButtonEnabled(true);
/*------------------------------------------------------------------------*/
            listDate = viewModel.generaListaGiorniSettimanaNonAncoraConclusa();
/*------------------------------------------------------------------------*/
            ldListaProposteOrdinazione = viewModel.getListaProposteOrdinazione();
            ldListaTipoPasto           = viewModel.getAllTipoPasto();
/*------------------------------------------------------------------------*/
            ldListaProposteOrdinazione.observe(fragment,
                            new Observer<List<Ordinazioni>>() {
                            @Override
                            public void onChanged(List<Ordinazioni> nuovaListaOrdinazioni) {
                                if(nuovaListaOrdinazioni!=null){
                                    impostaPaginaConNuoviDati(listaTipoPasto,nuovaListaOrdinazioni);
                                }
                            }
                        });
            ldListaTipoPasto.observe(fragment,
                            new Observer<List<TipoPasto>>() {
                            @Override
                            public void onChanged(List<TipoPasto> nuovaListaTipoPasto) {
                                if(nuovaListaTipoPasto!=null){
                                    impostaPaginaConNuoviDati(nuovaListaTipoPasto,listaProposteOrdinazione);
                                }
                            }
                        });
/*------------------------------------------------------------------------*/
        }
        return rootView;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA LAYOUT STATICO                                        */
/*================================================================================================*/
/**
     * Questo metodo pertette di impostrate le varie parti statiche di layout.
     */
    private void impostaLayoutStatico(){
        barraConDuePulsanti = new BarraConDuePulsanti(  rootView,
                                                        R.id.include_BottoniSalvaAnnullaModifiche,
                                                        R.string.lbl_ButtonSalva,
                                                        false,
                                                        R.string.lbl_ButtonAnnula);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  IMPOSTA AZZIONE PULSANTI                                      */
/*################################################################################################*/
/*================================================================================================*/
/*                                  INSERIMENTO E AGGIORNAMENTO RECYCLERVIEW                      */
/*================================================================================================*/
    /**
     * Questo metodo verifica che ci siano impostati i due valori che possono variare in ogni momento
     * e con quelli ricarca l'adapter in modo di tenerlo sempre aggiornato.
     * @param listaTipoPasto
     * @param listaOrdinazioni
     */
    private synchronized void impostaPaginaConNuoviDati(List<TipoPasto> listaTipoPasto,List<Ordinazioni> listaOrdinazioni){
        this.listaTipoPasto           = listaTipoPasto;
        this.listaProposteOrdinazione = listaOrdinazioni;
        if((listaTipoPasto!=null) && (listaOrdinazioni!=null)){
            recyclerView.setAdapter(new ModificaMenuSettimanaleGiorniRVAdapter( fragment.getContext(),
                                                                                listDate,
                                                                                this.listaTipoPasto,
                                                                                this.listaProposteOrdinazione,
                                                                                listener));
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  IMPOSTA AZZIONE PULSANTI                                      */
/*################################################################################################*/
/*================================================================================================*/
/*                                  AGGIUNGI LISSENER PULSANTI                                    */
/*================================================================================================*/
/**
     * Questo medodo definise i due lisserne dei due pulsanti infondo alla pagina, sia quello per
     * salvare le eventuali modifiche appotate, che quello per annulare l'operazione di modifica.
     */
    private void impostaAzzioniPulsanti(){
        barraConDuePulsanti.setOnPrimaryButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.salvaModificheMenuSettimanale();
            }
        });
/*------------------------------------------------------------------------*/
        barraConDuePulsanti.setOnSecondaryButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.annullaModificheMenuSettimanale();
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
}