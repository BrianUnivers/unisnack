package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Universita_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Universita_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Universita_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class Universita_Repository extends Universita_ThreadAggiornamento {
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
    private LiveData<List<Universita>> mAllUniversita;
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Universita_Repository(Application application) {
        super(application);
        //AllUniversita get any time the same result
        mAllUniversita = universitaDao.getAll_Synchronized();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERT                                                                 */
/*================================================================================================*/
    /*public Future<Long> insert(Universita universita){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
            @Override
            public Long call() throws Exception {
                return universitaDao.insert(universita);
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    /*public void deleteAll(){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            universitaDao.deleteAll();
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    /*public void update(Universita universita){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            universitaDao.update(universita);
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<Universita>> getAll(){
        return mAllUniversita;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    /*public LiveData<Universita> getByPrimaryKey(Long id){
        return universitaDao.getByPrimaryKey_Synchronized(id);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
    /*public LiveData<List<Universita>> getByIdOfUtente(Utenti utenti){
        return universitaDao.getByIdOfUtente_Synchronized(utenti.getId());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE INTO SPECIFIC DAY                              */
/*================================================================================================*/
    /*public LiveData<List<Universita>> getByIdOfUtente(Utenti utenti, Date date){
        return universitaDao.getByIdOfUtente_Synchronized(utenti.getId(),date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE TODAY                                          */
/*================================================================================================*/
    /*public LiveData<List<Universita>> getByIdOfUtenteAtToday(Utenti utenti){
        return universitaDao.getByIdOfUtenteAtToday_Synchronized(utenti.getId());
    }
/*================================================================================================*/
/*################################################################################################*/
}
