package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;

@Dao
public interface Ingredienti_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long insert(Ingredienti ingredienti);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Query("DELETE FROM tableIngredienti")
    public void deleteAll();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE By ID                                                    */
/*================================================================================================*/
    @Query("DELETE FROM tableIngredienti WHERE id == :id")
    public abstract void delete(Long id);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = Ingredienti.class,
            onConflict = OnConflictStrategy.ABORT)
    public void update(Ingredienti ingredienti);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    @Query("SELECT * FROM tableIngredienti")
    public List<Ingredienti> getAll();
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableIngredienti")
    public LiveData<List<Ingredienti>> getAll_Synchronized();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    @Query("SELECT * FROM tableIngredienti WHERE id == :id")
    public Ingredienti getByPrimaryKey(Long id);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableIngredienti WHERE id == :id")
    public LiveData<Ingredienti> getByPrimaryKey_Synchronized(Long id);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID PIETANZA                                              */
/*================================================================================================*/
    @Query( "SELECT I.* " +
            "FROM tableIngredienti AS I, tableP_contiene_A AS P " +
            "WHERE I.id == P.id_ingredienti AND P.id_pietanza == :idPietanza" )
    public List<Ingredienti> getByIdOfPietanza(Long idPietanza);
/*------------------------------------------------------------------------*/
    @Query( "SELECT I.* " +
            "FROM tableIngredienti AS I, tableP_contiene_A AS P " +
            "WHERE I.id == P.id_ingredienti AND P.id_pietanza == :idPietanza" )
    public LiveData<List<Ingredienti>> getByIdOfPietanza_Synchronized(Long idPietanza);
    /*================================================================================================*/
/*################################################################################################*/
}
