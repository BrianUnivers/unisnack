package it.tn.unitn.disi.ppsmet.IncludeView_Setup.AlertDialogApp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import it.tn.unitn.disi.ppsmet.R;

public class MessageAlertDialog extends AlertDialog {
/*################################################################################################*/
    protected Button btnViewOK=null;// OK
    protected View view=null;//Layout di tutto il AlertDialog
    protected Context context;
/*================================================================================================*/
/*                                  CREAZIONE DEL ALERTDIALOG                                     */
/*================================================================================================*/
    /**
     * Metodo che crea un alertdialog con la struttura di base e le funzionalità di base per inciare
     * un messaggio
     */
    public MessageAlertDialog(Context context, int idAlertDialog,  int idTitle, int idMessaggio) {
        super(context);
        this.context = context;
        //Imposta il layout
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(idAlertDialog,null);
/*------------------------------------------------------------------------------------------------*/
//                                  IMPOSTAZIONE STRINGE E ICONE
/*------------------------------------------------------------------------------------------------*/
        //Imposta  titolo
        TextView titolo =(TextView) view.findViewById(R.id.txt_Titolo);
        titolo.setText(idTitle);
        //Imposta  messaggio
        TextView messaggio =(TextView) view.findViewById(R.id.txt_Message);
        messaggio.setText(idMessaggio);

        //Imposta i bottone
        btnViewOK= (Button) view.findViewById(R.id.btn_OK);
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
//                                  LISTENER DEL BOTTONE
/*------------------------------------------------------------------------------------------------*/
        // Listener del pulsante
        if(btnViewOK!=null){
            btnViewOK.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    cancel();
                }
            });
        }
/*------------------------------------------------------------------------------------------------*/
        //Non chiudere si non con il tasto
        this.setCanceledOnTouchOutside(false);
        this.setView(view);
    }
/*================================================================================================*/
/*################################################################################################*/
}
