package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.U_intollerante_A;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

@Dao
public abstract class Allergeni_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)//TODO: CHECKED IF INSERT AN INGREDIENTE
    public abstract long insert(Allergeni allergeni);
/*------------------------------------------------------------------------*/
    @Transaction
    public long insertAllergia(Utenti utenti,Allergeni allergeni ){
        List<Allergeni> listaAllergeni = new LinkedList<>();
        listaAllergeni.add(allergeni);
        return this.insertAllergia(utenti,listaAllergeni);
    }
/*------------------------------------------------------------------------*/
    @Transaction
    public long insertAllergia(Utenti utenti,List<Allergeni> listaAllergeni){
        Iterator<Allergeni> iterator= listaAllergeni.iterator();
        long inseriti=0;
        while (iterator.hasNext()){
            Allergeni elemet = iterator.next();
            this.insert(new U_intollerante_A(utenti.getId(),elemet.getId()));
            inseriti++;
        }
        return inseriti;
    }
/*------------------------------------------------------------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    protected abstract long insert(U_intollerante_A intollerante);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Transaction
    public void deleteAll(){
        this.deleteAllIntollerante();
        this.deleteAllAllergeni();
    }
/*------------------------------------------------------------------------------------------------*/
    @Query("DELETE FROM tableU_intollerante_A")
    protected abstract void deleteAllIntollerante();
/*------------------------------------------------------------------------*/
    @Query("DELETE FROM tableAllergeni")
    public abstract void deleteAllAllergeni();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE by ID UTENTE AND ALLERGENI                               */
/*================================================================================================*/
    @Query( "DELETE FROM tableU_intollerante_A  " +
            "WHERE id_utente == :idUtente ")
    public abstract void deleteAllAllergeni(Long idUtente);
/*------------------------------------------------------------------------*/
    @Query( "DELETE FROM tableU_intollerante_A  " +
            "WHERE id_utente == :idUtente AND id_allergeni == :idAllergeni")
    public abstract void deleteSpecificAllergeniByIdOfUtente(Long idUtente,Long idAllergeni);
/*------------------------------------------------------------------------*/
    @Query( "DELETE FROM tableU_intollerante_A  " +
            "WHERE id_utente == :idUtente AND id_allergeni IN (:idAllergeni)")
    public abstract void deleteListAllergeniByIdOfUtente(Long idUtente,List<Long> idAllergeni);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = Allergeni.class,
            onConflict = OnConflictStrategy.ABORT)
    public abstract void update(Allergeni allergeni);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    @Query("SELECT * FROM tableAllergeni")
    public abstract List<Allergeni> getAll();
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableAllergeni")
    public abstract LiveData<List<Allergeni>> getAll_Synchronized();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    @Query("SELECT * FROM tableAllergeni WHERE id == :id")
    public abstract Allergeni getByPrimaryKey(Long id);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tableAllergeni WHERE id == :id")
    public abstract LiveData<Allergeni> getByPrimaryKey_Synchronized(Long id);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by by ID UTENTE                                             */
/*================================================================================================*/
    @Query( "SELECT A.id, A.nome, A.descrizione " +
            "FROM tableAllergeni AS A, tableU_intollerante_A AS I " +
            "WHERE A.id == I.id_allergeni AND I.id_utente == :idUtenti ")
    public abstract List<Allergeni> getByIdOfUtente(Long idUtenti);
/*------------------------------------------------------------------------*/
    @Query( "SELECT A.id, A.nome, A.descrizione " +
            "FROM tableAllergeni AS A, tableU_intollerante_A AS I " +
            "WHERE A.id == I.id_allergeni AND I.id_utente == :idUtenti ")
    public abstract LiveData<List<Allergeni>> getByIdOfUtente_Synchronized(Long idUtenti);
/*================================================================================================*/
/*################################################################################################*/
}
