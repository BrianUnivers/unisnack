package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.google.firebase.database.annotations.NotNull;

import java.util.Date;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.PietanzeForniteMensa_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;

public class PietanzeForniteMensa_Repository extends PietanzeForniteMensa_ThreadAggiornamento {
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA MENSE (e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
public PietanzeForniteMensa_Repository(Application application) {
    super(application);
}
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU'                                 */
/*                          SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR                         */
/*================================================================================================*/
    public LiveData<List<Pietanze>> getByIdOfMensa_TipoMenu_SpecificDay(@NonNull Long idMense,
                                                                           @NonNull Long idTipoMenu,
                                                                           @NotNull Date date){
        return pietanzeDao.getByIdOfMensa_TipoMenu_SpecificDay_Synchronized(idMense,idTipoMenu,date);
    }
/*------------------------------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificDay(@NonNull Long idMense,
                                                                                                  @NonNull Long idTipoMenu,
                                                                                                  @NotNull Date date){
        return pietanzeDao.getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificDay_Synchronized(idMense,idTipoMenu,date);
    }
/*================================================================================================*/
/*################################################################################################*/
}
