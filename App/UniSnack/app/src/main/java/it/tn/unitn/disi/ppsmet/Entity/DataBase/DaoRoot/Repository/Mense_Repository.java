package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Mense_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Mense_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class Mense_Repository extends Mense_ThreadAggiornamento{
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA MENSE (e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
    private LiveData<List<Mense>> mAllAMense;
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Mense_Repository(Application application) {
        super(application);
        //AllAMense get any time the same result
        mAllAMense = menseDao.getAll_Synchronized();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERT                                                                 */
/*================================================================================================*/
    /*public Future<Long> insert(Mense mense){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                    @Override
                    public Long call() throws Exception {
                        return menseDao.insert(mense);
                    }
                });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    /*public void deleteAll(){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            menseDao.deleteAll();
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    /*public void update(Mense mense){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            menseDao.update(mense);
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<Mense>> getAll(){
        return mAllAMense;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    /*public LiveData<Mense> getByPrimaryKey(Long id){
        return menseDao.getByPrimaryKey_Synchronized(id);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID OF UNIVERSITA'                                        */
/*================================================================================================*/
    /*public LiveData<List<Mense>> getByIdOfUniverita(Universita universita){
        return menseDao.getByIdOfUniverita_Synchronized(universita.getId());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID OF UENTE                                              */
/*================================================================================================*/
    public LiveData<List<Mense>> getAccessibleByIdOfUtenteAtToday(Utenti utenti){
        return menseDao.getAccessibleByIdOfUtenteAtSpecificDate_Synchronized(utenti.getId(),new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Mense>> getAccessibleByIdOfUtenteAtToday(Long idUtente){
        return menseDao.getAccessibleByIdOfUtenteAtSpecificDate_Synchronized(idUtente,new Date());
    }
/*================================================================================================*/
/*################################################################################################*/
}
