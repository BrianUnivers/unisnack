package it.tn.unitn.disi.ppsmet.IncludeView_Setup.ListDataWithLabel;

import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import it.tn.unitn.disi.ppsmet.IncludeView_Setup.DataWithLabelView;
import it.tn.unitn.disi.ppsmet.R;

public class ListDataWithLabelView{
/*################################################################################################*/
    private TextView     txtLabel  = null;
    private RecyclerView listValue = null;
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    /**
     * Questo metodo permette di costruire e definire il layout impostato
     */
    private ListDataWithLabelView(TextView txtLabel,RecyclerView txtValue) {
        this.txtLabel  = txtLabel;
        this.listValue = txtValue;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(listValue.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listValue.setLayoutManager(linearLayoutManager);
    }
/*------------------------------------------------------------------------*/
    /**
     * Questo è il costuttore di quando l'oggetto viene impostato in una pagina o in una sua parte.
     * @param rootLayout
     * @param idLayoutIncluded
     * @return
     */
    public static ListDataWithLabelView newInstance(ViewGroup rootLayout, int idLayoutIncluded) {
        ListDataWithLabelView ris = null;
        if(rootLayout!=null){
            ViewGroup contenitore = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(contenitore!=null){
                TextView     txtLabel  = (TextView)     contenitore.findViewById(R.id.txt_Label);
                RecyclerView listValue = (RecyclerView) contenitore.findViewById(R.id.rcl_ListaValore);
                if((txtLabel!=null) && (listValue!=null) ){
                    ris = new ListDataWithLabelView(txtLabel,listValue);
                }
            }
        }
        return ris;
    }
/*------------------------------------------------------------------------*/
    /**
     * Questo è il costuttore di quando l'oggetto viene impostato in una pagina o in una sua parte.
     * @param rootLayout
     * @param idLayoutIncluded
     * @return
     */
    public static ListDataWithLabelView newInstance(ViewGroup rootLayout, int idLayoutIncluded, int idTextLable) {
        ListDataWithLabelView ris = newInstance(rootLayout,idLayoutIncluded);
        if(ris!=null){
            ris.setLabel(idTextLable);
        }
        return ris;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    /**
     * Questo metodo permette di impostare e cambiare la lable del gurppo di layout, partendo da
     * identificatere della risosssa della stringa.
     * @param idTextTitolo
     */
    public void setLabel(int idTextTitolo){
        txtLabel.setText(idTextTitolo);
    }
/*--------------------------------------------*/
    /**
     * Questo metodo permette di impostare e cambiare la lable del gurppo di layout, partendo dalla
     * stringa.
     * @param textTitolo
     */
    public void setLabel(String textTitolo){
        txtLabel.setText(textTitolo);
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo permette di impostare un adapter che conterrà la lista di parametri che dovranno
     * essre rappresentati sotto la label.
     * @param adapter
     */
    public void setValue(Adapter adapter){
        listValue.setAdapter(adapter);
    }
/*================================================================================================*/
/*################################################################################################*/
}
