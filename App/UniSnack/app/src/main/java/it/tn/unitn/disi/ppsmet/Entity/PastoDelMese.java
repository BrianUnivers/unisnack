package it.tn.unitn.disi.ppsmet.Entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;

import java.util.Date;
import java.util.Objects;

@Entity(tableName = "tablePastoDelMese" ,
        primaryKeys = {"id_utente", "mese", "anno"},
        foreignKeys = {@ForeignKey(entity = Utenti.class,
                parentColumns = "id",       //Colanna della classe a qui si rifierisce
                childColumns  = "id_utente",//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE)})
public class PastoDelMese {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @ColumnInfo(name = "id_utente")
    private Long  id_utente;
    @NonNull
    @ColumnInfo(name = "mese")
    private Short mese;
    @NonNull
    @ColumnInfo(name = "anno")
    private Short anno;
    @NonNull
    @ColumnInfo(name = "dataProposta")
    private Date  dataProposta;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    @Ignore
    public PastoDelMese() { }
//--------------------------------------------------------------------------------------------------
    public PastoDelMese(@NonNull Long id_utente,
                        @NonNull Short mese,
                        @NonNull Short anno,
                        @NonNull Date dataProposta) {
        this.id_utente = id_utente;
        setMese(mese);
        this.anno = anno;
        this.dataProposta = dataProposta;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    public Long getId_utente() {
        return id_utente;
    }
//--------------------------------------
    public void setId_utente(Long id_utente) {
        this.id_utente = id_utente;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Short getMese() {
        return mese;
    }
//--------------------------------------
    public boolean setMese(@NonNull Short mese) {
        boolean ris=false;
        if((mese>=12) && (mese<=1)){
            this.mese = mese;
            ris=true;
        }
        return ris;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Short getAnno() {
        return anno;
    }
//--------------------------------------
    public void setAnno(@NonNull Short anno) {
        this.anno = anno;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Date getDataProposta() {
        return dataProposta;
    }
//--------------------------------------
    public void setDataProposta(@NonNull Date dataProposta) {
        this.dataProposta = dataProposta;
    }
//==================================================================================================
//==================================================================================================
//                                      EQUALS E HASH_CODE
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PastoDelMese)) return false;
        PastoDelMese that = (PastoDelMese) o;
        return getId_utente().equals(that.getId_utente()) &&
                getMese().equals(that.getMese()) &&
                getAnno().equals(that.getAnno()) &&
                getDataProposta().equals(that.getDataProposta());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(getId_utente(), getMese(), getAnno(), getDataProposta());
    }
//==================================================================================================
//##################################################################################################
}
