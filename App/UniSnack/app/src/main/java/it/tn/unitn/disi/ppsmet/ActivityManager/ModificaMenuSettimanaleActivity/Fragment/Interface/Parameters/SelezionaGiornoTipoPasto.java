package it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface.Parameters;

import com.google.firebase.database.annotations.NotNull;

import java.util.Date;

import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;

public class SelezionaGiornoTipoPasto {
    private Date      data;
    private TipoPasto tipoPasto;
/*################################################################################################*/
/*================================================================================================*/
/*                                  COSTRUTTORE                                                   */
/*================================================================================================*/
    public SelezionaGiornoTipoPasto(@NotNull Date data,@NotNull TipoPasto tipoPasto) {
        this.data = data;
        this.tipoPasto = tipoPasto;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  GET E SET                                                     */
/*================================================================================================*/
    public Date getData() {
        return data;
    }
/*-------------------------------------------------*/
    public void setData(Date data) {
        this.data = data;
    }
/*------------------------------------------------------------------------------------------------*/
    public TipoPasto getTipoPasto() {
        return tipoPasto;
    }
/*-------------------------------------------------*/
    public void setTipoPasto(TipoPasto tipoPasto) {
        this.tipoPasto = tipoPasto;
    }
    /*================================================================================================*/
/*################################################################################################*/
}
