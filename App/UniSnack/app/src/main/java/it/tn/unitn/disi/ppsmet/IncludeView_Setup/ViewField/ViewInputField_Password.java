package it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewField;

import android.renderscript.ScriptGroup;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.tn.unitn.disi.ppsmet.R;

public class ViewInputField_Password extends ViewInputField_Text {
/*################################################################################################*/
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP VIEW FIELD                                */
/*================================================================================================*/
    public ViewInputField_Password(ViewGroup rootLayout, int idLayoutIncluded, int idLable, int idTestValue) {
        super(rootLayout,idLayoutIncluded,idLable,idTestValue,false);
        impostaLayoutComune();
    }
/*------------------------------------------------------------------------*/
    public ViewInputField_Password(ViewGroup rootLayout, int idLayoutIncluded, int idLable, int idTestValue,boolean obbligatorio) {
        super(rootLayout,idLayoutIncluded,idLable,idTestValue,obbligatorio);
        impostaLayoutComune();
    }
/*------------------------------------------------------------------------------------------------*/
    private void impostaLayoutComune() {
        if(input!=null){
            input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  AZZIONI DURANTE L'INSERIMETO DEI DATI                         */
/*================================================================================================*/
    public String getPassword(){
        String ris = null;
        if(input!=null){
            ris =input.getText().toString();
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
}
