package it.tn.unitn.disi.ppsmet.GestioneAssetPerFoto;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Pair;


import com.google.firebase.database.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

public class GestioneFotoDaAsset {
//##################################################################################################
//                                  COSTRUTTORI
//##################################################################################################
    private Context context = null;
//==================================================================================================
//                                  COSTRUTTORI
//==================================================================================================
    /**
     * Questo è il metodo costruttore che riceve il contesto in modo tale da usate in seguito per
     * accedere ai assert.
     * @param context
     */
    public GestioneFotoDaAsset(Context context) {
        this.context = context;
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  GET IMMAGINE
//##################################################################################################
//==================================================================================================
//                                  GET BITMAP FOTO
//==================================================================================================
    /**
     * Questo metodo restituisce la foto in formato <<Bitmap>> se persente altrimeti null;
     */
    public Bitmap getFoto(String pathFile){
        Bitmap bitmap = null;
        if((pathFile!=null) && (!pathFile.equals(""))){
            try {
                if(accept(pathFile)){
                    AssetFileDescriptor fileDescriptor = this.context.getAssets().openFd(pathFile);
                    bitmap = BitmapFactory.decodeStream(fileDescriptor.createInputStream());
                }
            } catch (IOException e) {
                bitmap = null;
            }catch (Exception e){
                bitmap = null;
                Log.d("ERROR_IMG",e.getMessage(),e);
            }
        }
        return bitmap;
    }
//==================================================================================================
//==================================================================================================
//                                  GET BITMAP FOTO
//==================================================================================================
    /**
     * Questo metodo restituisce la foto in formato <<Bitmap>> se persente altrimeti il Bitmap passato
     * come valore di defoult.
     */
    public Bitmap getFoto(String pathFile,Bitmap defoult){
        Bitmap ris= getFoto(pathFile);
        if(ris==null){
            ris = defoult;
        }
        return ris;
    }
//==================================================================================================
//==================================================================================================
//                                  GET BITMAP FOTO
//==================================================================================================
    /**
     * Questo metodo restituisce la foto in formato <<Bitmap>> se persente altrimeti qulla legata al
     * id del riferimento passato. Se non è convertibile in bitmap ritornera null.
     */
    public Bitmap getFoto(String pathFile,int IdImageDefoult){
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), IdImageDefoult);
        return  getFoto(pathFile,bitmap);
    }
//==================================================================================================
//##################################################################################################

//##################################################################################################
//                                  CREAZIONE DELLE CARTRELLE DELLA GALLERA
//##################################################################################################
//==================================================================================================
//==================================================================================================
//                                  VERIFICA SE FILE E' UN IMMAGINE
//==================================================================================================
    /**
     * Verifica il filePath e il file in modo tale che sia rappresentabile come una foto.
     */
    private boolean accept(String fileName) {
        List okFileExtensions = new ArrayList();
        okFileExtensions.add(".jpg");
        okFileExtensions.add(".png");
        okFileExtensions.add(".jpeg");
        boolean estensioneTrovata = false;
        boolean fileAccetato = false;
        Iterator<String> iterator = okFileExtensions.iterator();
        while ( (iterator.hasNext()) && (!estensioneTrovata) ){
            if(fileName.endsWith(iterator.next())){
                estensioneTrovata = true;
                fileAccetato = true;
            }
        }
        return fileAccetato;
    }
//==================================================================================================
//##################################################################################################
}