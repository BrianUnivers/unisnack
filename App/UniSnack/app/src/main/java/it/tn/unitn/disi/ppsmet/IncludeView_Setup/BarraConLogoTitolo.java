package it.tn.unitn.disi.ppsmet.IncludeView_Setup;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import it.tn.unitn.disi.ppsmet.R;

public class BarraConLogoTitolo {
/*################################################################################################*/
    private TextView txtTitolo = null;
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    public BarraConLogoTitolo(ViewGroup rootLayout, int idLayoutIncluded,
                              int idTextTitolo) {
        if(rootLayout!=null){
            ViewGroup barraContenitore = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(barraContenitore!=null){
                txtTitolo = (TextView) barraContenitore.findViewById(R.id.txt_Titolo);
                txtTitolo.setText(idTextTitolo);
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
