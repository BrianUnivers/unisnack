package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.CorsiDelUniversita_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

public class Corsi_Repository extends CorsiDelUniversita_ThreadAggiornamento  {
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA CORSO e UNI_HA_C (e le sue derivazioni  */
/*                         seguendo un ordine topologico per garantire gli aggiornamenti)         */
/*                         ATTRAVERO IL DATABASE LOCALE (room)                                    */
/*################################################################################################*/
    private LiveData<List<Corsi>> mAllCorsi;
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Integer myI;
    public Corsi_Repository(Application application) {
        super(application);
        mAllCorsi=corsiDao.getAll_Synchronized();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERT                                                                 */
/*================================================================================================* /
    public Future<Long> insert(Corsi corsi, Universita universita){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
            @Override
            public Long call() throws Exception {
                return corsiDao.insert(corsi,universita);
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================* /
    public void deleteAll(){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            corsiDao.deleteAll();
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================* /
    public void update(Corsi corsi){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            corsiDao.update(corsi);
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    /*public LiveData<List<Corsi>> getAll(){
        return mAllCorsi;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================* /
    public LiveData<Corsi> getByPrimaryKey(Long id){
        return corsiDao.getByPrimaryKey_Synchronized(id);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UNIVERSITA'                                           */
/*================================================================================================*/
    public LiveData<List<Corsi>> getByIdOfUniversita(Universita universita){
        return corsiDao.getByIdOfUniverita_Synchronized(universita.getId());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================* /
    public LiveData<List<Corsi>> getByIdOfUtente(Utenti utenti){
        return corsiDao.getByIdOfUtente_Synchronized(utenti.getId());
    }
/*================================================================================================*/
/*################################################################################################*/
}
