package it.tn.unitn.disi.ppsmet;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.PersonalizzaMenuSettimanaleActivity.PersonalizzaMenuSettimanaleViewModel;
import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.AppCompatActivityWithMyPrimaryMenu;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;
import it.tn.unitn.disi.ppsmet.GestioneMyIntent.MyIntent;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListOrdinPiet.PersonMenuSetOrdinRVAdapter;

public class PersonalizzaMenuSettimanaleActivity extends AppCompatActivityWithMyPrimaryMenu {
/*################################################################################################*/
/*                                  IMPOSTA PARAMETRI DEI INTET PER MENU'LATERALE                 */
/*################################################################################################*/
    /**
     * Questo parametro viene chiamato nel momento in cui si preme un pulsante tra quelli del
     * menù laterale principale così da poter inserire i vari valori da passare tramite intent,
     * all'activity che segue. Conclusa questa procedura verrà attivato, l'intente per effetuare
     * il cabiamento d'activity, se chiamato dal menu laterare principale.
     * @param intent Questo parameto contiene l'intent pre impostata con l'activity d'arrivo.
     */
    @Override
    protected void setParamiter(MyIntent intent) {
        intent.putExtraIdUtente(MyIntent.I_UTENTE_ATTIVO,viewModel.getIdUtenteAttivo());
    }
/*################################################################################################*/
/*################################################################################################*/
/*                                  CAMBIA ACTIVITY                                               */
/*################################################################################################*/
/*================================================================================================*/
/*                                  APRI ACTYVITY MODIFICA MANU' DELLA SETTIMANA                  */
/*================================================================================================*/
    /**
     * Questo metodo permmette di cambiare activity aprendo quella per modificare il menu personaliato
     * della settimana.
     */
    private void apriActivityModificaMenuDellaSettimana(){
        MyIntent i = new MyIntent(PersonalizzaMenuSettimanaleActivity.this, ModificaMenuSettimanaleActivity.class);
        setParamiter(i);//Imposta i dati da passare
        startActivity(i);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private PersonalizzaMenuSettimanaleActivity activity = this;
/*------------------------------------------------------------------------*/
    private PersonalizzaMenuSettimanaleViewModel viewModel;
/*------------------------------------------------------------------------*/
    private RecyclerView listaDatiOrdinazioniView;
/*------------------------------------------------------------------------*/
    private LiveData<List<Ordinazioni>>                          ldListaOrdinazioni;
    private LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> ldListaPietanzeOrdinazione;
    private LiveData<List<TipoPietanza>>                         ldListaTipoPietanza;
    private LiveData<List<TipoPasto>>                            ldListaTipoPasto;
/*------------------------------------------------------------------------*/
    private List<Ordinazioni>                          listaOrdinazioni         = new LinkedList<>();
    private List<OrdinazionePietanzeDellaMenseConVoto> listaPietanzeOrdinazione = new LinkedList<>();
    private List<TipoPietanza>                         listaTipoPietanza        = new LinkedList<>();
    private List<TipoPasto>                            listaTipoPasto           = new LinkedList<>();
/*------------------------------------------------------------------------*/
    private Long idUtenteAttivo;
/*================================================================================================*/
/*                                  CREAZIONE DELL'ACTIVITY                                       */
/*================================================================================================*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personalizza_menu_settimanale);
/*------------------------------------------------------------------------*/
        //IMPOSTA MENU LETERALE
        super.initToolBar(R.menu.bar_menu_personalizza_menu_settimanale);
        super.impostaPaginaComeHome();
/*------------------------------------------------------------------------*/
        listaDatiOrdinazioniView = findViewById(R.id.rcl_ListaOrdinazioni);
        listaDatiOrdinazioniView.setLayoutManager(new LinearLayoutManager(activity));
/*------------------------------------------------------------------------*/
        //GESTIONE VIEWMODEL E UTENTE ATTIVO
        viewModel =  new ViewModelProvider(this).get(PersonalizzaMenuSettimanaleViewModel.class);
        viewModel.setParameterFromIntet(getIntent());
/*------------------------------------------------------------------------*/
        idUtenteAttivo = viewModel.getIdUtenteAttivo();
        if(idUtenteAttivo==null){
            super.doLogout();
        }else {
/*------------------------------------------------------------------------*/
            ldListaOrdinazioni         = viewModel.getAllOrdinazioniByIdUtente_InTheWeek(idUtenteAttivo);
            ldListaPietanzeOrdinazione = viewModel.getAlOrdinazionePietanzeDellaMenseConVotoByIdUtente_InTheWeek(idUtenteAttivo);
            ldListaTipoPietanza        = viewModel.getAllTipoPietanza();
            ldListaTipoPasto           = viewModel.getAllTipoPasto();
/*------------------------------------------------------------------------*/
            gestioneObserverMantenimetoAggiornatoRecyclerView();
/*------------------------------------------------------------------------*/
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE OBSERVER                                             */
/*################################################################################################*/
/*================================================================================================*/
/*                                  GESTIONE OBSERVER DEI DATI RICHIESTI ALL DATABASE             */
/*================================================================================================*/
    /**
     * Questo metodo permette di mantenere aggiornato tutti i dati che vongono utilizzati nella recyclerview
     * in modo tale che mostri sempre i dati aggiornati.
     */
    private void gestioneObserverMantenimetoAggiornatoRecyclerView(){
/*------------------------------------------------------------------------*/
        ldListaOrdinazioni.observe(activity, new Observer<List<Ordinazioni>>() {
            @Override
            public void onChanged(List<Ordinazioni> nuovaListaOrdinazioni) {
                if(nuovaListaOrdinazioni!=null){
                    listaOrdinazioni.clear();
                    listaOrdinazioni.addAll(nuovaListaOrdinazioni);
                    impostaRecylerViewPerProssimiOrdini(listaOrdinazioni,listaPietanzeOrdinazione,listaTipoPietanza,listaTipoPasto);
                }
            }
        });
/*------------------------------------------------------------------------*/
        ldListaPietanzeOrdinazione.observe(activity, new Observer<List<OrdinazionePietanzeDellaMenseConVoto>>() {
            @Override
            public void onChanged(List<OrdinazionePietanzeDellaMenseConVoto> nuovaListaPietanze) {
                if(nuovaListaPietanze!=null){
                    listaPietanzeOrdinazione.clear();
                    listaPietanzeOrdinazione.addAll(nuovaListaPietanze);
                    impostaRecylerViewPerProssimiOrdini(listaOrdinazioni,listaPietanzeOrdinazione,listaTipoPietanza,listaTipoPasto);
                }
            }
        });
/*------------------------------------------------------------------------*/
        ldListaTipoPietanza.observe(activity, new Observer<List<TipoPietanza>>() {
            @Override
            public void onChanged(List<TipoPietanza> nuovaListaTipoPietanza) {
                if(nuovaListaTipoPietanza!=null){
                    listaTipoPietanza.clear();
                    listaTipoPietanza.addAll(nuovaListaTipoPietanza);
                    impostaRecylerViewPerProssimiOrdini(listaOrdinazioni,listaPietanzeOrdinazione,listaTipoPietanza,listaTipoPasto);
                }
            }
        });
/*------------------------------------------------------------------------*/
        ldListaTipoPasto.observe(activity, new Observer<List<TipoPasto>>() {
            @Override
            public void onChanged(List<TipoPasto> nuovaListaTipoPasto) {
                if(nuovaListaTipoPasto!=null){
                    listaTipoPasto.clear();
                    listaTipoPasto.addAll(nuovaListaTipoPasto);
                    impostaRecylerViewPerProssimiOrdini(listaOrdinazioni,listaPietanzeOrdinazione,listaTipoPietanza,listaTipoPasto);
                }
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  RECYCLERVIEW                                                  */
/*################################################################################################*/
/*================================================================================================*/
/*                                  INSERIMENTO E AGGIORNAMENTO RECYCLERVIEW                      */
/*================================================================================================*/
    private synchronized void impostaRecylerViewPerProssimiOrdini(
                                                         List<Ordinazioni> listaOrdinazioni,
                                                         List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze,
                                                         List<TipoPietanza> listaTipoPietanza,
                                                         List<TipoPasto> listaTipoPasto){
        if((listaOrdinazioni!=null) && (listaPietanze!=null) &&
                (listaTipoPietanza!=null) && (listaTipoPasto!=null) &&
                (!listaOrdinazioni.isEmpty()) ){
            listaDatiOrdinazioniView.setVisibility(View.VISIBLE);
            PersonMenuSetOrdinRVAdapter adapter =
                                        new PersonMenuSetOrdinRVAdapter(activity,
                                                                                    listaOrdinazioni,
                                                                                    listaPietanze,
                                                                                    listaTipoPietanza,
                                                                                    listaTipoPasto);
            listaDatiOrdinazioniView.setAdapter(adapter);
        }else{
            listaDatiOrdinazioniView.setVisibility(View.GONE);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE DEL MENU' SULLA TOOLBAR                              */
/*################################################################################################*/
/*================================================================================================*/
/*                                  GESTIONE DEI EVENTI DI CLICK SULLE VOCI DEL MENU'             */
/*================================================================================================*/
    /**
     * Metodo per distingere i tasti della toolbar e sapere cosa si deve fare.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.btn_bar_Modifica:
                apriActivityModificaMenuDellaSettimana();
        }
        return super.onOptionsItemSelected(item);
    }
/*================================================================================================*/
/*################################################################################################*/
}