package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.IdentificativiSessioni_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.TipoPasto_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Utenti_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DatiPerSessione.IdentificativiSessioni;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class IdentificativiSessioni_Repository {
/*================================================================================================*/
/*                         DAO                                                                    */
/*================================================================================================*/
    private IdentificativiSessioni_Dao IdentificativiSessioniDao;
    private MyRoomDatabase db;
    private Utenti_Repository rUtenti;
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public IdentificativiSessioni_Repository(Application application) {
        db = MyRoomDatabase.getDatabase(application);
        IdentificativiSessioniDao = db.IdentificativiSessioni_Dao();
        rUtenti = new Utenti_Repository(application);
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERT                                                                 */
/*================================================================================================*/
    public void insert(IdentificativiSessioni identificativiSessioni){
        MyRoomDatabase.databaseWriteExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    IdentificativiSessioniDao.insert(identificativiSessioni);
                }
            });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    /*public void deleteAll(){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            IdentificativiSessioniDao.deleteAll();
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    /*public void update(IdentificativiSessioni identificativiSessioni){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            IdentificativiSessioniDao.update(identificativiSessioni);
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
   /* public IdentificativiSessioni getByIdSessione(String id){
        return IdentificativiSessioniDao.getByIdSessione(id);
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         AGGIUNGI SESSIONE                                                      */
/*================================================================================================*/
    public void insertSessioneByUtenteAndIdSessione(Utenti utenti, String idSessione){
        insert(new IdentificativiSessioni(utenti.getId(),idSessione));
    }
/*------------------------------------------------------------------------------------------------*/
    public void insertSessioneByUtenteAndIdSessione(Long idUtente, String idSessione){
        insert(new IdentificativiSessioni(idUtente,idSessione));
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         AGGIUNGI SESSIONE                                                      */
/*================================================================================================*/
    public Future<Utenti> getUtenteByIdSessione(String idSessione){
        return rUtenti.getByIdSessione(idSessione);
    }
/*================================================================================================*/
/*################################################################################################*/
}
