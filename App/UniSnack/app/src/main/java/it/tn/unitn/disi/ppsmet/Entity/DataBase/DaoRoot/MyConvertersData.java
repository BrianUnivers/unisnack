package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot;

import androidx.room.TypeConverter;

import java.util.Calendar;
import java.util.Date;

public class MyConvertersData {
/*================================================================================================*/
/*                                  CREAZIONE DELL'ACTIVITY                                       */
/*================================================================================================*/
    /**
     * Questo metodo permette di estrarre dal room database un oggetto di tipo Data che nel
     * database viene rappresentato come Long
     * @param value
     * @return
     */
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }
/*------------------------------------------------------------------------*/
    /**
     * Questo metodo permette d'inserire il tipo Date nel databesa e nelle sue query come dato normale
     * poichè in ogni occorenza di Date nel database non occore sapere l'ora, minuti secondi e millisecondi
     * vogliamo salvare la data senza tale informazioni così che si possa eseguire l'ugulaianza tra
     * due date.
     * @param date
     * @return
     */
    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        if(date == null){
            return null;
        }else{
            Calendar calendarData = Calendar.getInstance();
            calendarData.setTime(date);
            calendarData.set(Calendar.MILLISECOND,0);
            calendarData.set(Calendar.SECOND,0);
            calendarData.set(Calendar.MINUTE,0);
            calendarData.set(Calendar.HOUR_OF_DAY,0);
            return calendarData.getTime().getTime();
        }
    }
/*================================================================================================*/
}
