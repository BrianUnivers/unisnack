package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.old;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Allergeni_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.PastoDelMese_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.U_vota_PdM;
import it.tn.unitn.disi.ppsmet.Entity.PastoDelMese;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class PastoDelMese_Repository {
/*================================================================================================*/
/*                         DAO                                                                    */
/*================================================================================================*/
    private PastoDelMese_Dao pastoDelMeseDao;
    private MyRoomDatabase db;
/*================================================================================================*/
/*================================================================================================*/
/*                         RISULTATI SEMPRE AGGIORNATI                                            */
/*================================================================================================*/
    private LiveData<List<PastoDelMese>> mAllPastoDelMese;
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public PastoDelMese_Repository(Application application) {
        db = MyRoomDatabase.getDatabase(application);
        pastoDelMeseDao = db.PastoDelMeseDao();
        //AllPastoDelMese get any time the same result
        mAllPastoDelMese = pastoDelMeseDao.getAll_Synchronized();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERT                                                                 */
/*================================================================================================*/
    public Future<Long> insert(PastoDelMese pastoDelMese, List<Pietanze> listaPietanza){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                    @Override
                    public Long call() throws Exception {
                        return pastoDelMeseDao.insert(pastoDelMese,listaPietanza);
                    }
                });
    }
/*------------------------------------------------------------------------*/
    public Future<Long> insertVoto(PastoDelMese pastoDelMese, Long idUtenti ){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                    @Override
                    public Long call() throws Exception {
                        return pastoDelMeseDao.insertVoto(pastoDelMese,idUtenti);
                    }
                });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    public void delete(PastoDelMese pastoDelMese){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            pastoDelMeseDao.delete(pastoDelMese);
        });
    }
/*------------------------------------------------------------------------*/
    public void delete(U_vota_PdM vota){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            pastoDelMeseDao.delete(vota);
        });
    }
/*------------------------------------------------------------------------*/
    public void deleteAll(){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            pastoDelMeseDao.deleteAll();
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    public void update(PastoDelMese pastoDelMese){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            pastoDelMeseDao.update(pastoDelMese);
        });
    }
/*------------------------------------------------------------------------*/
    public void updateListaPietanze(PastoDelMese pastoDelMese, List<Pietanze> listaPietanza){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            pastoDelMeseDao.updateListaPietanze(pastoDelMese,listaPietanza);
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<PastoDelMese>> getAll(){
        return mAllPastoDelMese;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    public LiveData<PastoDelMese> getByPrimaryKey(Utenti utenti, Short mese, Short anno){
        return pastoDelMeseDao.getByPrimaryKey_Synchronized(utenti.getId(),mese,anno);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTI                                                */
/*================================================================================================*/
    public LiveData<List<PastoDelMese>> getByIdUtente(Utenti utenti){
        return pastoDelMeseDao.getByIdUtente_Synchronized(utenti.getId());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ANNO AND MESE                                            */
/*================================================================================================*/
    public LiveData<List<PastoDelMese>> getByAnno_Mese(Short mese,Short anno){
        return pastoDelMeseDao.getByAnno_Mese_Synchronized(mese,anno);
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET VOTI of PASTO DEL MESE                                      */
/*================================================================================================*/
    public LiveData<Long> getVotiSingoloPastoDelMese(PastoDelMese pastoDelMese){
        return pastoDelMeseDao.getVotiSingoloPastoDelMese_Synchronized(pastoDelMese);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET VOTI of PASTO DEL MESE RISPETTO AL TOTALE                   */
/*================================================================================================*/
    public LiveData<Double> getVotiSingoloPastoDelMeseRispettoAlTotale(PastoDelMese pastoDelMese){
        return pastoDelMeseDao.getVotiSingoloPastoDelMeseRispettoAlTotale_Synchronized(pastoDelMese);
    }
/*================================================================================================*/
/*################################################################################################*/
}
