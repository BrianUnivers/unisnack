package it.tn.unitn.disi.ppsmet.GestioneMyIntent;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;


import java.sql.Date;

import it.tn.unitn.disi.ppsmet.Entity.Utenti;
import it.tn.unitn.disi.ppsmet.GestioneMyIntent.Interface.CostantiInserireEscrarreDatiDaiInternt;

public class MyIntent extends Intent implements CostantiInserireEscrarreDatiDaiInternt {
/*################################################################################################*/
/*================================================================================================*/
/*                                  COSTRUTTORI                                                   */
/*================================================================================================*/
    public MyIntent() {
    }
//--------------------------------------------------------------------------------------------------
    public MyIntent(Intent o) {
        super(o);
    }
//--------------------------------------------------------------------------------------------------
    public MyIntent(String action) {
        super(action);
    }
//--------------------------------------------------------------------------------------------------
    public MyIntent(String action, Uri uri) {
        super(action, uri);
    }
//--------------------------------------------------------------------------------------------------
    public MyIntent(Context packageContext, Class<?> cls) {
        super(packageContext, cls);
    }
//--------------------------------------------------------------------------------------------------
    public MyIntent(String action, Uri uri, Context packageContext, Class<?> cls) {
        super(action, uri, packageContext, cls);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  SET E SET DEI PARAMTRI IN UN INTENT                           */
/*################################################################################################*/
/*================================================================================================*/
/*                                  SET UTENTE                                                    */
/*================================================================================================*/
    /**
     * Questo metodo permette di inserire id del utente nel intent da un oggetto utente.
     */
    public MyIntent putExtraIdUtente(String nome, Utenti utente){
        if((utente!=null) && (utente.getId()!=null)){
            this.putExtra(I_UTENTE_ID+"-"+nome,  utente.getId());
        }
        return this;
    }
//--------------------------------------------------------------------------------------------------
    /**
     * Questo metodo permette di inserire id del utente nel intent.
     */
    public MyIntent putExtraIdUtente(String nome, Long idUtente){
        if(idUtente!=null){
            this.putExtra(I_UTENTE_ID+"-"+nome,  idUtente);
        }
        return this;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  GET ID UTENTE                                                 */
/*================================================================================================*/
    /**
     * Questo metodo permette di estrare se esite l'id di un utente da un Intent, se non esiste il
     * valore di ritorno ritorna null.
     */
    public static Long getIdUtenteExtra(Intent intent,String nome){
        Long idUtente =null;
        long idTemp = intent.getLongExtra(I_UTENTE_ID+"-"+nome,-1 );
        if(idTemp!=-1) {
            idUtente = idTemp;
        }
        return idUtente;
    }
/*================================================================================================*/
/*################################################################################################*/
}
