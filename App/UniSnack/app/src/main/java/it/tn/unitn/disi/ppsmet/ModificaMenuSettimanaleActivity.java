package it.tn.unitn.disi.ppsmet;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface.OnFragmentModificaMenuDelSingoloGiornoTipoPastoEventListener;
import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.Interface.OnFragmentSelezioneGiornoETipoPastoEventListener;
import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.ModificaMenuDelSingoloGiornoTipoPastoFragment;
import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.Fragment.SelezioneGiornoETipoPastoFragment;
import it.tn.unitn.disi.ppsmet.ActivityManager.ModificaMenuSettimanaleActivity.ModificaMenuSettimanaleViewModel;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.GestioneMyIntent.MyIntent;

public class ModificaMenuSettimanaleActivity extends AppCompatActivity
                                             implements OnFragmentSelezioneGiornoETipoPastoEventListener,
                                                        OnFragmentModificaMenuDelSingoloGiornoTipoPastoEventListener {
/*################################################################################################*/
/*                                  IMPOSTA PARAMETRI DEI INTET PER MENU'LATERALE                 */
/*################################################################################################*/
    /**
     * Questo parametro viene chiamato nel momento in cui si preme un pulsante tra quelli del
     * menù laterale principale così da poter inserire i vari valori da passare tramite intent,
     * all'activity che segue. Conclusa questa procedura verrà attivato, l'intente per effetuare
     * il cabiamento d'activity, se chiamato dal menu laterare principale.
     * @param intent Questo parameto contiene l'intent pre impostata con l'activity d'arrivo.
     */
    protected void setParamiter(MyIntent intent) {
        intent.putExtraIdUtente(MyIntent.I_UTENTE_ATTIVO,viewModel.getIdUtenteAttivo());
    }
/*################################################################################################*/
/*################################################################################################*/
/*                                  CAMBIA ACTIVITY                                               */
/*################################################################################################*/
/*================================================================================================*/
/*                                  APRI ACTYVITY PESONALIZZA MANU' DELLA SETTIMANA               */
/*================================================================================================*/
    /**
     * Questo metodo permmette di cambiare activity aprendo quella che mostra il menu personaliato
     * della settimana.
     */
    private void apriActivityPersonalizaMenuSettimana(){
        MyIntent i = new MyIntent(ModificaMenuSettimanaleActivity.this, PersonalizzaMenuSettimanaleActivity.class);
        setParamiter(i);//Imposta i dati da passare
        startActivity(i);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private ModificaMenuSettimanaleActivity activity = this;
    private Fragment selezioneGiornoTipoPastoFragment;
/*------------------------------------------------------------------------*/
    private ViewGroup pagina;
    /*------------------------------------------------------------------------*/
    private Toolbar toolbar=null;
/*------------------------------------------------------------------------*/
    private ModificaMenuSettimanaleViewModel viewModel;
/*------------------------------------------------------------------------*/
    private LiveData<List<Ordinazioni>> ldListaOrdinazioniPassate;
/*------------------------------------------------------------------------*/
    private List<Ordinazioni> listaOrdinazioniPassateVecchie = new LinkedList<>();
/*------------------------------------------------------------------------*/
    private List<Date> listaDate;
    private Long idUtenteAttivo;
/*================================================================================================*/
/*                                  CREAZIONE DELL'ACTIVITY                                       */
/*================================================================================================*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifica_menu_settimanale);
/*------------------------------------------------------------------------*/
        pagina       = findViewById(R.id.cly_PageModificaMenuSettimanale);
/*------------------------------------------------------------------------*/
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
/*------------------------------------------------------------------------*/
        //GESTIONE VIEWMODEL E UTENTE ATTIVO
        viewModel =  new ViewModelProvider(this).get(ModificaMenuSettimanaleViewModel.class);
        viewModel.setParameterFromIntet(getIntent());
/*------------------------------------------------------------------------*/
        idUtenteAttivo = viewModel.getIdUtenteAttivo();
        if(idUtenteAttivo==null){
            //Uscita dalla pagina se non c'è un utente.
            annullaModificheMenuSettimanale();
        }else{
/*------------------------------------------------------------------------*/
/*                      C'E' UN UTENTE ATTIVO                             */
/*------------------------------------------------------------------------*/
            listaDate =viewModel.generaListaGiorniSettimanaNonAncoraConclusa();
/*------------------------------------------------------------------------*/
            /**
             * Questa parte del aggiornamento viene fatta una singola volta.
             */
            ldListaOrdinazioniPassate = viewModel.getAllOrdinazioniByIdUtente_InListDate(idUtenteAttivo,listaDate);
            ldListaOrdinazioniPassate.observe(activity, new Observer<List<Ordinazioni>>() {
                    @Override
                    public void onChanged(List<Ordinazioni> list) {
                        viewModel.nuovaImpostazioneOrdiniGiaPresenti(list);
                        ldListaOrdinazioniPassate.removeObserver(this);
                        if((list!=null) && (!list.isEmpty())){
                            listaOrdinazioniPassateVecchie.clear();
                            listaOrdinazioniPassateVecchie.addAll(list);
                        }
                    }
                });
/*------------------------------------------------------------------------*/
            selezioneGiornoTipoPastoFragment = new SelezioneGiornoETipoPastoFragment();
            cambiaFragment(selezioneGiornoTipoPastoFragment,true);
/*------------------------------------------------------------------------*/
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE SELEZIONE FRAGMENT                                   */
/*################################################################################################*/
/*================================================================================================*/
/*                                  CAMBIA FRAGMENT                                               */
/*================================================================================================*/
    /**
     * Questo metodo permette di selezionare un frammeto tra i due e sostituirlo
     * @param fragment
     */
    private void cambiaFragment(Fragment fragment, boolean primoInserimento){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if(!primoInserimento){
            if(fragment instanceof SelezioneGiornoETipoPastoFragment){
                ft.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_right);
            }else if(fragment instanceof ModificaMenuDelSingoloGiornoTipoPastoFragment){
                ft.setCustomAnimations(R.anim.slide_in_right,R.anim.slide_out_left);
            }
        }
        ft.replace(R.id.fly_PaginaDiModifica, fragment);
        ft.commit();
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE SELEZIONE FRAGMENT                                   */
/*                                  OnFragmentSelezioneGiornoETipoPastoEventListener              */
/*################################################################################################*/
/*================================================================================================*/
/*                                  APERTURA DEL FRAMMETO MODIFICATORE DEL GIORNO SELEZIONETO     */
/*================================================================================================*/
    /**
     * Questo metodo permete al fragmet contenete la lista di giorni e dei tipi di pasto di aprire
     * il frammeto contenete i dati da modificare richiesti.
     * Per fare questo, il seguente medodo
     * @param data
     * @param tipoPasto
     */
    @Override
    public void apriModificaManuDelGiornoTipoPasto(Date data, TipoPasto tipoPasto) {
        if( (data!=null) && (tipoPasto!=null) && (tipoPasto.getId()!=null) ){
            //Selezione elementi
            viewModel.setGiornoTipoPastoSelezzionato(data,tipoPasto);
            //Cambiamento fragment
            Fragment fragment = ModificaMenuDelSingoloGiornoTipoPastoFragment.newInstance(idUtenteAttivo);
            cambiaFragment(fragment,false);
            //Imposta tasto indietro per passare all'altro frame
            this.getOnBackPressedDispatcher().addCallback(this,
                    new OnBackPressedCallback(true) {
                        @Override
                        public void handleOnBackPressed() {
                            annullaModificheMenuSettimanale(data, tipoPasto);
                        }
                    });
        }else{
            Toast.makeText(activity, R.string.msg_ImpossibileModificareManu, Toast.LENGTH_LONG).show();
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  SALVA LE MODIFICHE DELLE ORDONAZIONI                          */
/*================================================================================================*/
    /**
     * Questo metodo permette di salvare le modifiche applicate al menu settimanale che si è appena
     * personalizzato.
     */
    @Override
    public void salvaModificheMenuSettimanale() {
        List<Ordinazioni> listaOrdinazioni = viewModel.getListaProposteOrdinazioneInQuestoMomento();
        viewModel.rendiDefinitiveLeModifiche(listaOrdinazioni,listaOrdinazioniPassateVecchie);
        apriActivityPersonalizaMenuSettimana();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  ANNULLA MODIFICHE                                             */
/*================================================================================================*/
    /**
     * Questo metodo permette di annulare le modifice appotate al menu settimanale che si è appanna
     * modificato.
     */
    @Override
    public void annullaModificheMenuSettimanale() {
        apriActivityPersonalizaMenuSettimana();
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE SELEZIONE FRAGMENT                                   */
/*                                  OnFragmentModificaMenuDelSingoloGiornoTipoPastoEventListener  */
/*################################################################################################*/
/*================================================================================================*/
/*                                  SALVA TEPORANEAMENTE LE MODIFICHE DEL SINGOLO ORDONAZIONI     */
/*================================================================================================*/
    /**
     * Questo metodo permette di salvare le modifiche applicate al menu del singolo giorno che si è appena
     * personalizzato.
     */
    public void salvaModificheMenuSettimanaleDelGiorno(Long idUtente,Date data,TipoPasto tipoPasto,List<PietanzeDellaMense> listaPietanzeDellaMense){
        if(((listaPietanzeDellaMense==null)||(listaPietanzeDellaMense.isEmpty())) && (tipoPasto!=null) && (tipoPasto.getId()!=null) ){
            //Cancella questa ordinazione
            viewModel.rimuoviOrdinazione(data,tipoPasto.getId());
            cambiaFragment(selezioneGiornoTipoPastoFragment,false);
        }else if((idUtente!=null) && (data!=null) && (tipoPasto!=null) && (tipoPasto.getId()!=null)){
            Ordinazioni ordinazioni=new Ordinazioni();
            ordinazioni.setId_utente(idUtente);
            ordinazioni.setTipoPasto(tipoPasto);
            ordinazioni.setGiorno(data);
            //Salva orginazione
            viewModel.aggiungiNuovaOrdinazione(ordinazioni);
            //Salva lista Pientanze
            viewModel.aggiungiNuovaListaPietanze(data,tipoPasto.getId(),listaPietanzeDellaMense);
            cambiaFragment(selezioneGiornoTipoPastoFragment,false);
        }else{
            Toast.makeText(activity, R.string.msg_ErroreGenerico, Toast.LENGTH_LONG).show();
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                   ANNULLA MODIFICHE DEL SINGOLO ORDONAZIONI                    */
/*================================================================================================*/
    /**
     * Questo metodo permette di annulare le modifice appotate al menu settimanale che si è appanna
     * modificato.
     */
    public void annullaModificheMenuSettimanale(Date data,TipoPasto tipoPasto){
        //Cambiamento fragment
        cambiaFragment(selezioneGiornoTipoPastoFragment,false);
    }
/*================================================================================================*/
/*################################################################################################*/
}