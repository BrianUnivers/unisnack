package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;

public class Deserializer_Utenti implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto Utenti.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String DATA_FORMAT = "dd/MM/yyyy";
    private static String LOG_TAG = "Utenti_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "Utenti" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "Utenti".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public static Utenti estraiDaDataSnapshot(DataSnapshot dataSnapshot){
        Utenti utenti = null;
        if(dataSnapshot.exists()){
            try{
                //Campi not null
                Long id = null ;
                Long idUniversita= null;
                Long idCorso =null ;
                AnnoAccademico aa = null;

                String   idUniversita_idCorso_id = dataSnapshot.getKey();
                String[] tuttiID = idUniversita_idCorso_id.split(S_SEPARARORE_PER_CHIAVI);

                if(tuttiID.length==3){
                    idUniversita = Long.parseLong(tuttiID[0]);
                    idCorso      = Long.parseLong(tuttiID[1]);
                    id           = Long.parseLong(tuttiID[2]);
                }

                aa = Deserializer_AnnoAccademico.getAttributeOf(dataSnapshot,L_ANNO_ACCADEMICO);
                //Verifica campi not null
                if( (id!=null) &&
                    (idUniversita!=null) &&
                    (idCorso!=null) &&
                    (aa!=null) ){
                    //Campi not null
                    String nome     = dataSnapshot.child(P_NOME).getValue(String.class);
                    String cognome  =  dataSnapshot.child(P_COGNOME).getValue(String.class);
                    Date   nato     = new SimpleDateFormat(DATA_FORMAT)
                                        .parse(  dataSnapshot.child(P_NATO).getValue(String.class));
                    String email    = dataSnapshot.child(P_EMAIL).getValue(String.class);
                    String password = dataSnapshot.child(P_HASHED_PSW).getValue(String.class);
                    String salt     = dataSnapshot.child(P_SALT).getValue(String.class);
                    //-----------
                    Boolean statoTessera  = dataSnapshot.child(P_STATO_TESSERA).getValue(Boolean.class);
                    Integer matricola     = Integer.parseInt(dataSnapshot.child(P_MATRICOLA).getValue(String.class));
                    //Verifica campi not null
                    if((nome!=null) && (cognome!=null) && (nato!=null) && (email!=null) && (password!=null) &&
                        (salt!=null) && (statoTessera!=null) && (matricola!=null)){
                        utenti = new Utenti(id,nome,cognome,nato,email,password,salt,idUniversita,idCorso,matricola,aa,statoTessera);
                        utenti.setUrlImmagine(       dataSnapshot.child(P_URL_IMMAGINE).getValue(String.class));
                        utenti.setUrlQRCode(         dataSnapshot.child(P_URL_QRCODE).getValue(String.class) );
                        utenti.setUrlCodiceABarre(   dataSnapshot.child(P_URL_CODICE_A_BARRE).getValue(String.class));
                    }
                }
            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
                utenti = null;
            }
        }
        return utenti;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         GET SALT                                                               */
/*================================================================================================*/
    public static String getSalt(DataSnapshot dataSnapshot){
        String salt = null;
        if(dataSnapshot.exists()){
            try{
                if(dataSnapshot.child(P_SALT).exists()){
                    salt = dataSnapshot.child(P_SALT).getValue(String.class);
                }
            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
                salt = null;
            }
        }
        return salt;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, Utenti>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Object extends GenericDeserializer_Object<Utenti> {
        @Override
        public Utenti estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Utenti.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT FILTER                                             */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, Utenti>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_ObjectFilter extends GenericDeserializer_Object<Utenti> {
        @Override
        public Utenti estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            Utenti utenti = null;
            Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
            while((iterator.hasNext()) && (utenti==null)){
                utenti = Deserializer_Utenti.estraiDaDataSnapshot(iterator.next());
            }
            return utenti;
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<Utenti>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List<Utenti> {
        @Override
        public Utenti estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Utenti.estraiDaDataSnapshot(dataSnapshot);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER SALT                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, String>" così da poter
 * deserializzare un il salt da un utente dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Salt extends  GenericDeserializer_Object<String>{
    @Override
    public String estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
        String salt = null;
        Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
        while(iterator.hasNext()){
            salt = getSalt(iterator.next());
        }
        return salt;
    }
}
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER BOOLEAN                                                   */
/*================================================================================================*/
    /**
     * Questa classe contiene un implemetazione del "Function<DataSnapshot, Boolean>" così da poter
     * verificare l'esitenza di un utente con lo stesso valore indicati dalla query(tra quelli unici
     * come email e matricolo per una universita)
     */
    public static class Deserializer_Boolean extends  GenericDeserializer_Object<Boolean>{
        @Override
        public Boolean estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            Boolean esite = false;
            Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
            while(iterator.hasNext()){
                if(Deserializer_Utenti.estraiDaDataSnapshot(iterator.next())!=null){
                    esite =true;
                }
            }
            return esite;
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
