package it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewValutazione.OnChangeValueListener;

import android.view.View;

public interface OnChangeValueListener{
/*################################################################################################*/
/*                                  CREAZIONE DEL LISTENER                                        */
/*################################################################################################*/
    public void onChangeValueListener(View view, short value);
/*################################################################################################*/
}
