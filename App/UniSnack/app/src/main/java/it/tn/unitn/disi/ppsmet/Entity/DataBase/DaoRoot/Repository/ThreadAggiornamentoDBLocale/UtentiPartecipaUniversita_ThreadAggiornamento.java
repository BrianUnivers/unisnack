package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Corsi_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Corsi_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Tessere_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.IscrizioniUtente_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Tessere_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Universita_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Utenti_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.U_partecipa_Uni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.Uni_ha_C;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.IscrizioniUtente;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class UtentiPartecipaUniversita_ThreadAggiornamento extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA ANNO ACCADEMICO LOCALE           */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "UtentiPartecipaUniversita" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_UtentiPartecipaUniversita";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista dei UtentiPartecipaUniversita del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private List<Tessere> listaTessere;
        private Utenti        utenti;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * Corsi.
         * @param listaTessere Questo parametro contiene la lista di tutti gli elementi
         *                        tra qui quelli nuovi e quelli già presenti ma da modificare.
         */
        public ThreadAggiornamento(List<Tessere> listaTessere,Utenti utenti) {
            super();
            this.listaTessere =listaTessere;
            this.utenti=utenti;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                                  */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella Corsi.");
            if(utenti!=null){
                aggionaTabellaU_Partecipa_Uni(listaTessere,utenti);
                Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella Corsi.");
            }else{
                Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella Corsi. ERRORE UNIVERSITA");
            }
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param listaTessere
         * @param utenti
         */
        private synchronized void aggionaTabellaU_Partecipa_Uni(List<Tessere> listaTessere,Utenti utenti){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella Corsi.");
            //Controllo se posso già inserire i dati
            if(utenti!=null){
                //Dati presenti nel database locale.
                List<Tessere> listaTessereLocale = tessereDao.getSecondaryByIdOfUtente(utenti.getId());
                if((listaTessereLocale==null)||(listaTessereLocale.isEmpty())){
                    inserisciListaElementi(listaTessere,utenti);
                }else if((listaTessere==null)||(listaTessere.isEmpty())) {
                    eliminaListaElementi(listaTessereLocale,utenti);
                }else{
                    HashMap<String,Tessere> hashMapAALocali = convertiListaInHashMap(listaTessereLocale,utenti);

                    List<Tessere> listaTessereNuove = new LinkedList<>();
                    List<Tessere> listaTessereAggiornate = new LinkedList<>();
                    //Serve solo per poter conoscere quali elementi caccelare tra quelli già presenti localmente.
                    List<Tessere> listaVechieTessereDaMantenere = new LinkedList<>();

                    Iterator<Tessere> iterator = listaTessere.iterator();
                    while (iterator.hasNext()){
                        Tessere tessere =iterator.next();
                        if(tessere!=null){
                            Tessere tessereVecchia = hashMapAALocali.get(tessere.getId(utenti));
                            if(tessereVecchia==null){
                                listaTessereNuove.add(tessere);
                            }else if(!tessereVecchia.equals(tessere)){

                                listaTessereAggiornate.add(tessere);
                                listaVechieTessereDaMantenere.add(tessereVecchia);
                            }else{
                                listaVechieTessereDaMantenere.add(tessereVecchia);
                            }
                        }
                    }
                    //MANTENGO SOLO GLI ELEMNETI CHE DEVO CANCELLARE
                    listaTessereLocale.removeAll(listaVechieTessereDaMantenere);
                    inserisciListaElementi(listaTessereNuove,utenti);
                    aggiornaListaElementi(listaTessereAggiornate,utenti);
                    eliminaListaElementi(listaTessereLocale,utenti);
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         GENERAZIONE HASH MAP FROM LIST                                         */
/*================================================================================================*/
        private synchronized HashMap<String,Tessere> convertiListaInHashMap(List<Tessere> lista,Utenti utenti){
            HashMap<String,Tessere> ris=new HashMap<>();
            if(lista!=null){
                Iterator<Tessere> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Tessere elemento = iterator.next();
                    if((elemento!=null) && (elemento.getId(utenti)!=null)){
                        ris.put(elemento.getId(utenti),elemento);
                    }
                }
            }
            return ris;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO AGGIORNAMENTO ELEMENTI PRESENTI                                 */
/*================================================================================================*/
    private synchronized void aggiornaListaElementi(List<Tessere> lista,Utenti utenti){
        if(lista!=null){
            Iterator<Tessere> iterator = lista.iterator();
            while (iterator.hasNext()){
                Tessere elemento=iterator.next();
                if(elemento!=null){
                    tessereDao.update(new U_partecipa_Uni(utenti.getId(),
                            elemento.universita.getId(),
                            elemento.annoAccademico.getId(),
                            elemento.getStatoTessera(),
                            elemento.getQRcode(),
                            elemento.getUrlQRcode(),
                            elemento.getCodiceABarre(),
                            elemento.getUrlCodiceABarre()));
                }
            }
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO INSERIMENTO NUOVI ELEMENTI                                      */
/*================================================================================================*/
        private synchronized void inserisciListaElementi(List<Tessere> lista,Utenti utenti){
            if(lista!=null){
                Iterator<Tessere> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Tessere elemento=iterator.next();
                    if(elemento!=null){
                        tessereDao.insert(new U_partecipa_Uni(  utenti.getId(),
                                                                elemento.universita.getId(),
                                                                elemento.annoAccademico.getId(),
                                                                elemento.getStatoTessera(),
                                                                elemento.getQRcode(),
                                                                elemento.getUrlQRcode(),
                                                                elemento.getCodiceABarre(),
                                                                elemento.getUrlCodiceABarre()));
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaListaElementi(List<Tessere> lista,Utenti utenti){
            if(lista!=null){
                Iterator<Tessere> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Tessere elemento=iterator.next();
                    if(elemento!=null){
                        tessereDao.delete(utenti.getId(),elemento.annoAccademico.getId(),elemento.universita.getId());
                    }
                }
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_Tessere_Dao fbTessereDao = new FB_Tessere_Dao();
    //ROOM DATABASE
    public static Utenti_Dao utentiDao = null;
/*------------------------------------------------------------------------------------------------*/
    //DIPENDENZE
    protected static Universita_ThreadAggiornamento     dipendezaUniversita = null;
    protected static AnnoAccademico_ThreadAggiornamento dipendezaAA         = null;
    protected static Utenti_ThreadAggiornamento         dipendezaUtenti     = null;
    //ROOM DATABASE
    public static Tessere_Dao tessereDao = null;
    public static IscrizioniUtente_Dao iscrizioniUtenteDao = null;
    //QUERY ROOM DATABASE
    protected static  LiveData<List<Utenti>> allUtenti = new MediatorLiveData<>();
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaU_Partecipa_Uni(){
        if(utentiDao==null){
            utentiDao = db.UtentiDao();
            allUtenti = utentiDao.getAll_Synchronized();
        }
        if(tessereDao==null){
            tessereDao = db.TessereDao();
        }
        if(iscrizioniUtenteDao==null){//Solo per le sotto calssi
            iscrizioniUtenteDao = db.IscrizioniUtenteDao();
        }
        if((!allUtenti.hasObservers())){//SE NON HA già osservatori per rilevare aggiornamenti
            allUtenti.observeForever(new Observer<List<Utenti>>() {
                private  List<Utenti>  lastListUtenti = new LinkedList<>();
                private  List<Utenti>  temp = new LinkedList<>();
                @Override
                public void onChanged(List<Utenti> listaUtenti) {
                    temp.clear();
                    temp.addAll(listaUtenti);
                    if(!lastListUtenti.isEmpty()){
                        listaUtenti.removeAll(lastListUtenti);
                    }
                    //Solo quelli nuovi
                    Iterator<Utenti> iterator = listaUtenti.iterator();
                    while (iterator.hasNext()){
                        Utenti utenti =iterator.next();
                        if((utenti!=null) && (utenti.getId()!=null)){
                            //Aggiungi un altro ossservatore
                            fbTessereDao.getSecondaryByIdOfUtente_Synchronized(utenti).observeForever(new MyTessereObserver(utenti));
                        }
                    }
                    lastListUtenti.clear();
                    lastListUtenti.addAll(temp);
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
    private static class MyTessereObserver implements Observer<List<Tessere>>{
        private Utenti utenti;
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        public MyTessereObserver(Utenti utenti) {
            this.utenti =utenti;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO CHIAMATO NEL MOMENTO DEL CAMBIAMNETO                            */
/*================================================================================================*/
        @Override
        public void onChanged(List<Tessere> lista) {
            if((lista!=null)){
                executor.execute(new ThreadAggiornamento(lista,this.utenti));
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public UtentiPartecipaUniversita_ThreadAggiornamento(Application application) {
        super(application);
        if(dipendezaUtenti==null){
            dipendezaUtenti = new Utenti_ThreadAggiornamento(application);
        }
        gestioneAggiornametoTabellaU_Partecipa_Uni();
    }
/*================================================================================================*/
/*################################################################################################*/
}
