package it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Ignore;

import com.google.firebase.database.annotations.NotNull;

import java.util.Arrays;
import java.util.Objects;

import it.tn.unitn.disi.ppsmet.Entity.AnnoAccademico;
import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class Tessere extends IscrizioniUtente {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @ColumnInfo(name = "statoTessera")
    private Boolean statoTessera;
    @ColumnInfo(name = "QRcode")
    private byte[]  QRcode = null;
    @ColumnInfo(name = "urlQRCode")
    private String  urlQRcode = null;
    @ColumnInfo(name = "codiceABarre")
    private byte[]  codiceABarre = null;
    @ColumnInfo(name = "urlCodiceABarre")
    private String  urlCodiceABarre = null;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    public Tessere(@NonNull Universita universita,
                   @NonNull AnnoAccademico annoAccademico,
                   Corsi corsi,
                   Integer matricola,
                   @NonNull Boolean statoTessera,
                   byte[] QRcode,
                   String urlQRcode,
                   byte[] codiceABarre,
                   String urlCodiceABarre) {
        super(universita, annoAccademico, corsi, matricola);
        this.statoTessera = statoTessera;
        this.QRcode = QRcode;
        this.urlQRcode = urlQRcode;
        this.codiceABarre = codiceABarre;
        this.urlCodiceABarre = urlCodiceABarre;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Tessere(@NonNull Universita universita,
                   @NonNull AnnoAccademico annoAccademico,
                   Corsi corsi,
                   Integer matricola,
                   @NonNull Boolean statoTessera,
                   String urlQRcode,
                   String urlCodiceABarre) {
        super(universita, annoAccademico, corsi, matricola);
        this.statoTessera = statoTessera;
        this.QRcode = QRcode;
        this.urlQRcode = urlQRcode;
        this.codiceABarre = codiceABarre;
        this.urlCodiceABarre = urlCodiceABarre;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Tessere(@NonNull Universita universita,
                   @NonNull AnnoAccademico annoAccademico,
                   @NonNull Boolean statoTessera,
                   String urlQRcode,
                   String urlCodiceABarre) {
        super(universita, annoAccademico);
        this.statoTessera = statoTessera;
        this.QRcode = QRcode;
        this.urlQRcode = urlQRcode;
        this.codiceABarre = codiceABarre;
        this.urlCodiceABarre = urlCodiceABarre;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Tessere(@NonNull Universita universita,
                   @NonNull AnnoAccademico annoAccademico,
                   @NonNull Boolean statoTessera) {
        super(universita, annoAccademico);
        this.statoTessera = statoTessera;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    @NonNull
    public String getId(@NotNull Utenti utenti) {
        return new StringBuffer().append(annoAccademico.getId())
                        .append("-").append(universita.getId())
                        .append("-").append(utenti.getId()).toString();
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Boolean getStatoTessera() {
        return statoTessera;
    }
//--------------------------------------
    public void setStatoTessera(@NonNull Boolean statoTessera) {
        this.statoTessera = statoTessera;
    }
//--------------------------------------------------------------------------------------------------
    public byte[] getQRcode() {
        return QRcode;
    }
//--------------------------------------
    public void setQRcode(byte[] QRcode) {
        this.QRcode = QRcode;
    }
//--------------------------------------------------------------------------------------------------
    public String getUrlQRcode() {
        return urlQRcode;
    }
//--------------------------------------
    public void setUrlQRcode(String urlQRcode) {
        this.urlQRcode = urlQRcode;
    }
//--------------------------------------------------------------------------------------------------
    public byte[] getCodiceABarre() {
        return codiceABarre;
    }
//--------------------------------------
    public void setCodiceABarre(byte[] codiceABarre) {
        this.codiceABarre = codiceABarre;
    }
//--------------------------------------------------------------------------------------------------
    public String getUrlCodiceABarre() {
        return urlCodiceABarre;
    }
//--------------------------------------
    public void setUrlCodiceABarre(String urlCodiceABarre) {
        this.urlCodiceABarre = urlCodiceABarre;
    }
//==================================================================================================
//##################################################################################################
//==================================================================================================
//                                      EQUALS AND HASHCODE
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tessere)) return false;
        if (!super.equals(o)) return false;
        Tessere tessere = (Tessere) o;
        return getStatoTessera().equals(tessere.getStatoTessera()) &&
                Arrays.equals(getQRcode(), tessere.getQRcode()) &&
                Objects.equals(getUrlQRcode(), tessere.getUrlQRcode()) &&
                Arrays.equals(getCodiceABarre(), tessere.getCodiceABarre()) &&
                Objects.equals(getUrlCodiceABarre(), tessere.getUrlCodiceABarre());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        int result = Objects.hash(super.hashCode(), getStatoTessera(), getUrlQRcode(), getUrlCodiceABarre());
        result = 31 * result + Arrays.hashCode(getQRcode());
        result = 31 * result + Arrays.hashCode(getCodiceABarre());
        return result;
    }
//==================================================================================================
//##################################################################################################
}
