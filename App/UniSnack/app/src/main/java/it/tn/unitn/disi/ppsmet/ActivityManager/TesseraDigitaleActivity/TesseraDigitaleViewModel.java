package it.tn.unitn.disi.ppsmet.ActivityManager.TesseraDigitaleActivity;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import java.util.List;

import it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.InterfaceViewModel.MenuProfiloUtenteViewModel;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Tessere_Repository;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class TesseraDigitaleViewModel extends MenuProfiloUtenteViewModel {
/*################################################################################################*/
    private Tessere_Repository rTessere;
/*================================================================================================*/
/*                                  COSTRUTTORE VIEW MODEL                                        */
/*================================================================================================*/
    /**
     * Questo metodo costruttore del viewmode inizializza le repository che si interfacciano con entrambi
     * i database sia interno che estreno a seconda delle operazioni che vengono richeste. Che verranno
     * utilizate dall'activity e i suoi fragment.
     * @param application
     */
    public TesseraDigitaleViewModel(@NonNull Application application) {
        super(application);
        rTessere   = new Tessere_Repository(application);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  AZZIONI LEGATE ALLA GESITIONE DEI DATABASE                    */
/*################################################################################################*/
    /**
     * Questo variabile contiene il valore di un oggetto LiveData in cui si potrà inserire una tessera
     * e la fornirà ai richiedenti tramite un observer su questo oggetto.
     * In conclusione questo oggetto una valta chiamato il metodo "setTesseraSelezzionata()" conterrà la
     * tessera seleziona.
     */
    protected MediatorLiveData<Tessere> tessereSelezionata = new MediatorLiveData<>();
/*================================================================================================*/
/*                                  TESSERE ATTIVE NEL ANNO ACCADEMICO ATTUALE                    */
/*================================================================================================*/
    /**
     *
     * Questo metodo permette di richedere tutte le tessere attive dell'utente che è identificato tramite
     * l'id pasato come parametro. Le tessere passate comprendono sia quelli delle universitò a cui
     * l'utente partecipa ma non è iscritto si di quelle a cui si è iscritto.
     * @param idUtente
     * @return
     */
    public LiveData<List<Tessere>> getActiveByIdOfUtenteAtToday(Long idUtente){
        return rTessere.getActiveByIdOfUtenteAtToday(idUtente);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA TESSERA SELEZIONATA                                   */
/*================================================================================================*/
    /**
     * Questo metodo permette d'impostare e sovrascrivere/cambiare la tessera selezionata
     * se e soltanto se l'oggetto passato come paramento è diverso da null.
     * @param tessera
     */
    public void setTesseraSelezzionata(Tessere tessera){
        if(tessera!=null){
            tessereSelezionata.setValue(tessera);
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  RICHIEDI TESSERA SELEZIONATA                                  */
/*================================================================================================*/
    /**
     * Questo metodo permette di ricevere un oggetto LiveData che conterrà l'ultima tessera
     * selezionata permettendo tramite un suo observer di rilevare dei cambiamei nei dati.
     * Così facendo si da la possibilità al richiedente di questo oggetto di rimanere aggiornato sul
     * valore contenuto.
     * @return
     */
    public LiveData<Tessere> getTesseraSelezzionata(){
        return tessereSelezionata;
    }
/*================================================================================================*/
/*################################################################################################*/
}
