package it.tn.unitn.disi.ppsmet.IncludeView_Setup.SelettoreABarra;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import it.tn.unitn.disi.ppsmet.IncludeView_Setup.SelettoreABarra.OnChangeListener.OnChangeListener;
import it.tn.unitn.disi.ppsmet.R;

public class SelettoreABarra<T> {
/*################################################################################################*/
    private ViewGroup barraContenitore         = null;
    private ImageButton btnIncremento          = null;
    private ImageButton btnDecremento          = null;
    private Spinner     spnElementoSelezionato = null;
    private ArrayAdapter<T> adapter  = null;
    //Lissenere
    private OnChangeListener listenerOnChangeListener = null;
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP                                           */
/*================================================================================================*/
    /**
     * Questo è il metodo costruttore che riceve solo dal metodo statico gli elementi che compone
     * l'oggetto così che solo se è stato possiblie crealo verrà ritornato il seguente oggetto.
     * Garantendo così che si possa dare per scontato la presenza dei elementi di layout che lo
     * compongono.
     * @param barraContenitore
     * @param btnIncremento
     * @param btnDecremento
     * @param spnElementoSelezionato
     */
    protected SelettoreABarra(ViewGroup barraContenitore,
                            ImageButton btnIncremento,
                            ImageButton btnDecremento,
                            Spinner     spnElementoSelezionato) {
        this.barraContenitore = barraContenitore;
        this.btnIncremento = btnIncremento;
        this.btnDecremento = btnDecremento;
        this.spnElementoSelezionato = spnElementoSelezionato;
        //Impostazione lissaener per il camiamento dell'elemento selezionato
        this.btnIncremento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNextElement();
            }
        });
        this.btnDecremento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPreviousElement();
            }
        });
        this.spnElementoSelezionato.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                goOnChangeListener();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Qusto metodo è utilizzato come metodo costruttore pubblic che costruisce un oggetto di tipo
     * "SelettoreABarra" solo è sono presenti gli elementi che lo compongono.
     * Se non dovesse essere possibile il risultato di tale metodo è null.
     * @param rootLayout
     * @param idLayoutIncluded
     * @return
     */
    public static SelettoreABarra newInstance(ViewGroup rootLayout, int idLayoutIncluded) {
        SelettoreABarra ris = null;
        if((rootLayout!=null)){
            ViewGroup barraContenitore = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(barraContenitore!=null){
                ImageButton btnIncremento      =  barraContenitore.findViewById(R.id.btn_Incremento);
                ImageButton btnDecremento      =  barraContenitore.findViewById(R.id.btn_Decremento);
                Spinner     spnSpinnerElemento =  barraContenitore.findViewById(R.id.spn_ElementoSelezionato);
                if((btnIncremento!=null) && (btnDecremento!=null) && (spnSpinnerElemento!=null)){
                    ris = new SelettoreABarra(barraContenitore,btnIncremento,btnDecremento,spnSpinnerElemento);
                }
            }
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  SESTIONE ELEMENTI INTERNO                                     */
/*################################################################################################*/
/*================================================================================================*/
/*                                  SELEZIONA PROSSIMO ELEMENTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo permette, anche dal esterno, di cambiare l'elemento selezionato con il suo succesivo
     * CIRCOLARE, e rendere visibile tale elemento. In oltre se c'è un lissener on change lo attiva.
     */
    public void setNextElement(){
        if(adapter!=null){
            int posizioneItmeSelezionato=spnElementoSelezionato.getSelectedItemPosition();
            if(posizioneItmeSelezionato!=AdapterView.INVALID_POSITION){
                posizioneItmeSelezionato=(posizioneItmeSelezionato+1)%spnElementoSelezionato.getCount();
                spnElementoSelezionato.setSelection(posizioneItmeSelezionato);
            }
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  SELEZIONA PROSSIMO ELEMENTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo permette, anche dal esterno, di cambiare l'elemento selezionato con il suo precedente
     * CIRCOLARE, e rendere visibile tale elemento. In oltre se c'è un lissener on change lo attiva.
     */
    public void setPreviousElement(){
        if(adapter!=null){
            int posizioneItmeSelezionato=spnElementoSelezionato.getSelectedItemPosition();
            if(posizioneItmeSelezionato!=AdapterView.INVALID_POSITION){
                posizioneItmeSelezionato= (posizioneItmeSelezionato+spnElementoSelezionato.getCount()-1)%spnElementoSelezionato.getCount();
                spnElementoSelezionato.setSelection(posizioneItmeSelezionato);
            }
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  SET AND GET                                                   */
/*################################################################################################*/
/*================================================================================================*/
/*                                  SET ADAPTER                                                   */
/*================================================================================================*/
    /**
     * Questo parametro permette di cambiare adapter che si sta utilizzando nello spinner.
     * @param adapter
     */
    public void setAdapter(ArrayAdapter<T> adapter){
        this.adapter = adapter;
        spnElementoSelezionato.setAdapter(adapter);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  GET ELEMENTO SELEZIONATO                                      */
/*================================================================================================*/
    /**
     * Permette di estrarre l'elemento selezionato dello spinner in quel momento.
     * @return
     */
    public T getItemSelected(){
        T ris = null;
        if(adapter!=null){
            ris = (T) spnElementoSelezionato.getSelectedItem();
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*                                  ACTION LISSENER                                               */
/*################################################################################################*/
/*================================================================================================*/
/*                                  SET LISSENER ON CHANGE ITEM SELECTION                         */
/*================================================================================================*/
    /**
     * Permette di aggiungere un lissser e sostituirlo con il precedente, sul cambiameto del parametro
     * selezionato.
     * @param listener
     */
    public void setOnChangeListener(OnChangeListener<T> listener){
        this.listenerOnChangeListener= listener;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  AVVIO LISSENER ON CHANGE ITEM SELECTION                       */
/*================================================================================================*/
    /**
     * Metodo chiamanto quando viene modificato il parametro selezionato in modo tale
     * da inviare al eventuale lissener il camiameto del parametro.
     */
    private void goOnChangeListener(){
        if(listenerOnChangeListener!=null){
            listenerOnChangeListener.onChangeListener(barraContenitore,getItemSelected());
        }
    }
/*================================================================================================*/
/*################################################################################################*/

}
