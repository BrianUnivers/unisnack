package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_IscrizioniUtente;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.IscrizioniUtente;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;


public class FB_IscrizioniUtente_Dao extends ConnesioneAFirebase<IscrizioniUtente> {
    private FB_Universita_Dao FB_Universita_Dao = new FB_Universita_Dao();
    private FB_Corsi_Dao      FB_Corsi_Dao      = new FB_Corsi_Dao();
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
    public LiveData<List<IscrizioniUtente>> getByIdOfUtente_OneTime(@NonNull Utenti utenti){
        return this.getByIdOfUtente_OneTime(utenti,null);
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
    public LiveData<List<IscrizioniUtente>> getByIdOfUtente(@NonNull Utenti utenti){
        return this.getByIdOfUtente(utenti,null);
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
    public LiveData<List<IscrizioniUtente>> getByIdOfUtente_Synchronized(@NonNull Utenti utenti){
        return this.getByIdOfUtente_Synchronized(utenti,null);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE  INTO SPECIFIC DAY                             */
/*================================================================================================*/
    public  LiveData<List<IscrizioniUtente>> getByIdOfUtente_OneTime(@NonNull Utenti utenti,@NonNull Date date){
//..........................................
    //Patecipa ma non iscritto
    DatabaseReference refPrimaParte = super.getReferenceFireBase().getReference()
            .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
            .child(builderKeyFromUtenti(utenti))
            .child(L_PARTECIPA_UNIVERSITA);
    //_OneTime
    FirebaseQueryLiveData_OneTime pripmoParziale = new FirebaseQueryLiveData_OneTime(refPrimaParte);
    LiveData<List<IscrizioniUtente>> risPripmoParziale =
            Transformations.map(pripmoParziale, new Deserializer_IscrizioniUtente.Deserializer_List(date));
//..........................................
    //Partecipa PERCHE' E' iscritto
    //_OneTime
    LiveData<Universita> universita = FB_Universita_Dao.getByPrimaryKey_OneTime(utenti.getId_Universita());

    MediatorLiveData<List<IscrizioniUtente>> risSecondoParziale = new MediatorLiveData<>();
    risSecondoParziale.addSource(universita, new Observer<Universita>() {
        @Override
        public void onChanged(Universita universita) {
            //_OneTime
            LiveData<Corsi>  corsi  = FB_Corsi_Dao.getByIdOfUtente_OneTime(utenti);
            risSecondoParziale.addSource(corsi, new Observer<Corsi>() {
                @Override
                public void onChanged(Corsi corsi) {
                    //Query vera e propria
                    DatabaseReference refSecondaParte = getReferenceFireBase().getReference()
                            .child(L_UTENTI_DEL_CORSO_DEL_UNIVERSITA)
                            .child(utenti.getId_Universita().toString())
                            .child(L_CORSI)
                            .child(utenti.getId_Corso().toString())
                            .child(L_ISCRITTI)
                            .child(builderKeyFromUtenti(utenti));
                    //_OneTime
                    FirebaseQueryLiveData_OneTime secondaParziale = new FirebaseQueryLiveData_OneTime(refSecondaParte);
                    LiveData<IscrizioniUtente> risSecondaParziale =
                            Transformations.map(secondaParziale, new Deserializer_IscrizioniUtente.Deserializer_Object(universita,corsi,date));
                    risSecondoParziale.addSource(risSecondaParziale, new Observer<IscrizioniUtente>() {
                        @Override
                        public void onChanged(IscrizioniUtente iscrizioniUtente) {
                            List<IscrizioniUtente> list = new LinkedList<>();
                            list.add(iscrizioniUtente);
                            risSecondoParziale.setValue(list);
                        }
                    });
                }
            });
        }
    });
//..........................................
    return this.mergeLiveData(risPripmoParziale,risSecondoParziale);
}
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
    public LiveData<List<IscrizioniUtente>> getByIdOfUtente(@NonNull Utenti utenti,@NonNull Date date){
//..........................................
        //Patecipa ma non iscritto
        DatabaseReference refPrimaParte = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PARTECIPA_UNIVERSITA);
        //_
        FirebaseQueryLiveData pripmoParziale = new FirebaseQueryLiveData(refPrimaParte);
        LiveData<List<IscrizioniUtente>> risPripmoParziale =
                Transformations.map(pripmoParziale, new Deserializer_IscrizioniUtente.Deserializer_List(date));
//..........................................
        //Partecipa PERCHE' E' iscritto
        //_
        LiveData<Universita> universita = FB_Universita_Dao.getByPrimaryKey(utenti.getId_Universita());

        MediatorLiveData<List<IscrizioniUtente>> risSecondoParziale = new MediatorLiveData<>();
        risSecondoParziale.addSource(universita, new Observer<Universita>() {
            @Override
            public void onChanged(Universita universita) {
                //_
                LiveData<Corsi>  corsi  = FB_Corsi_Dao.getByIdOfUtente(utenti);
                risSecondoParziale.addSource(corsi, new Observer<Corsi>() {
                    @Override
                    public void onChanged(Corsi corsi) {
                        //Query vera e propria
                        DatabaseReference refSecondaParte = getReferenceFireBase().getReference()
                                .child(L_UTENTI_DEL_CORSO_DEL_UNIVERSITA)
                                .child(utenti.getId_Universita().toString())
                                .child(L_CORSI)
                                .child(utenti.getId_Corso().toString())
                                .child(L_ISCRITTI)
                                .child(builderKeyFromUtenti(utenti));
                        //_
                        FirebaseQueryLiveData secondaParziale = new FirebaseQueryLiveData(refSecondaParte);
                        LiveData<IscrizioniUtente> risSecondaParziale =
                                Transformations.map(secondaParziale, new Deserializer_IscrizioniUtente.Deserializer_Object(universita,corsi,date));
                        risSecondoParziale.addSource(risSecondaParziale, new Observer<IscrizioniUtente>() {
                            @Override
                            public void onChanged(IscrizioniUtente iscrizioniUtente) {
                                List<IscrizioniUtente> list = new LinkedList<>();
                                list.add(iscrizioniUtente);
                                risSecondoParziale.setValue(list);
                            }
                        });
                    }
                });
            }
        });
//..........................................
        return this.mergeLiveData(risPripmoParziale,risSecondoParziale);
    }
    /*------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    public LiveData<List<IscrizioniUtente>> getByIdOfUtente_Synchronized(@NonNull Utenti utenti,@NonNull Date date){
//..........................................
        //Patecipa ma non iscritto
        DatabaseReference refPrimaParte = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PARTECIPA_UNIVERSITA);
        //_Synchronized
        FirebaseQueryLiveData_Synchronized pripmoParziale = new FirebaseQueryLiveData_Synchronized(refPrimaParte);
        LiveData<List<IscrizioniUtente>> risPripmoParziale =
                Transformations.map(pripmoParziale, new Deserializer_IscrizioniUtente.Deserializer_List(date));
//..........................................
        //Partecipa PERCHE' E' iscritto
        //_Synchronized
        LiveData<Universita> universita = FB_Universita_Dao.getByPrimaryKey_Synchronized(utenti.getId_Universita());

        MediatorLiveData<List<IscrizioniUtente>> risSecondoParziale = new MediatorLiveData<>();
        risSecondoParziale.addSource(universita, new Observer<Universita>() {
            @Override
            public void onChanged(Universita universita) {
                //_Synchronized
                LiveData<Corsi>  corsi  = FB_Corsi_Dao.getByIdOfUtente_Synchronized(utenti);
                risSecondoParziale.addSource(corsi, new Observer<Corsi>() {
                    @Override
                    public void onChanged(Corsi corsi) {
                        //Query vera e propria
                        DatabaseReference refSecondaParte = getReferenceFireBase().getReference()
                                .child(L_UTENTI_DEL_CORSO_DEL_UNIVERSITA)
                                .child(utenti.getId_Universita().toString())
                                .child(L_CORSI)
                                .child(utenti.getId_Corso().toString())
                                .child(L_ISCRITTI)
                                .child(builderKeyFromUtenti(utenti));
                        //_Synchronized
                        FirebaseQueryLiveData_Synchronized secondaParziale = new FirebaseQueryLiveData_Synchronized(refSecondaParte);
                        LiveData<IscrizioniUtente> risSecondaParziale =
                                Transformations.map(secondaParziale, new Deserializer_IscrizioniUtente.Deserializer_Object(universita,corsi,date));
                        risSecondoParziale.addSource(risSecondaParziale, new Observer<IscrizioniUtente>() {
                            @Override
                            public void onChanged(IscrizioniUtente iscrizioniUtente) {
                                List<IscrizioniUtente> list = new LinkedList<>();
                                list.add(iscrizioniUtente);
                                risSecondoParziale.setValue(list);
                            }
                        });
                    }
                });
            }
        });
//..........................................
        return this.mergeLiveData(risPripmoParziale,risSecondoParziale);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE TODAY                                          */
/*================================================================================================*/
    public LiveData<List<IscrizioniUtente>> getByIdOfUtenteAtToday_OneTime(@NonNull Utenti utenti){
        return this.getByIdOfUtente_OneTime(utenti,new Date());
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
    public LiveData<List<IscrizioniUtente>> getByIdOfUtenteAtToday(@NonNull Utenti utenti){
        return this.getByIdOfUtente(utenti,new Date());
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
    public LiveData<List<IscrizioniUtente>> getByIdOfUtenteAtToday_Synchronized(@NonNull Utenti utenti){
        return this.getByIdOfUtente_Synchronized(utenti,new Date());
    }
/*================================================================================================*/
/*################################################################################################*/
}
