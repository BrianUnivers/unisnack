package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.PastoDelMese;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

/*################################################################################################*/
/*################################################################################################*/
public class FB_PietanzeDellaMense_Dao extends FB_PietanzeDellaMenseInsertExist_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID MENSA AND TIPO MENU'                                  */
/*================================================================================================*/
    public  LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByMensa_TipoMenu_OneTime(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu){
        DatabaseReference ref = this.getReferenceFireBase().getReference()
                .child(L_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(mense.getId().toString())
                .child(L_TIPO_MENU)
                .child(tipoMenu.getId().toString());
        //_OneTime
        FirebaseQueryLiveData_OneTime risParziale = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(risParziale, new Deserializer_PietanzeDellaMense.Deserializer_List(mense, tipoMenu));
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByMensa_TipoMenu(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu){
        DatabaseReference ref = this.getReferenceFireBase().getReference()
                .child(L_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(mense.getId().toString())
                .child(L_TIPO_MENU)
                .child(tipoMenu.getId().toString());
        //_
        FirebaseQueryLiveData risParziale = new FirebaseQueryLiveData(ref);
        return Transformations.map(risParziale, new Deserializer_PietanzeDellaMense.Deserializer_List(mense, tipoMenu));
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByMensa_TipoMenu_Synchronized(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu){
        DatabaseReference ref = this.getReferenceFireBase().getReference()
                .child(L_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(mense.getId().toString())
                .child(L_TIPO_MENU)
                .child(tipoMenu.getId().toString());
        //_Synchronized
        FirebaseQueryLiveData_Synchronized risParziale = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(risParziale, new Deserializer_PietanzeDellaMense.Deserializer_List(mense, tipoMenu));
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU'                                 */
/*                          SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR                         */
/*================================================================================================*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment_OneTime(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu,
            @NonNull Integer anno,
            @NonNull Integer mese,
            @NonNull Integer giornoSettimana){
        DatabaseReference ref = this.getReferenceFireBase().getReference()
                .child(L_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(mense.getId().toString())
                .child(L_TIPO_MENU)
                .child(tipoMenu.getId().toString())
                .child(builderKeyFromFornisce(mense,tipoMenu,anno,mese,giornoSettimana));
        //_OneTime
        FirebaseQueryLiveData_OneTime risParziale = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(risParziale, new Deserializer_PietanzeDellaMense.Deserializer_List_Menu(mense, tipoMenu));
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu,
            @NonNull Integer anno,
            @NonNull Integer mese,
            @NonNull Integer giornoSettimana){
        DatabaseReference ref = this.getReferenceFireBase().getReference()
                .child(L_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(mense.getId().toString())
                .child(L_TIPO_MENU)
                .child(tipoMenu.getId().toString())
                .child(builderKeyFromFornisce(mense,tipoMenu,anno,mese,giornoSettimana));
        //_
        FirebaseQueryLiveData risParziale = new FirebaseQueryLiveData(ref);
        return Transformations.map(risParziale, new Deserializer_PietanzeDellaMense.Deserializer_List_Menu(mense, tipoMenu));
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment_Synchronized(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu,
            @NonNull Integer anno,
            @NonNull Integer mese,
            @NonNull Integer giornoSettimana){
        DatabaseReference ref = this.getReferenceFireBase().getReference()
                .child(L_PIETANZE_DEL_PERIODO_PER_TIPO_MENU_DEL_MENSE)
                .child(mense.getId().toString())
                .child(L_TIPO_MENU)
                .child(tipoMenu.getId().toString())
                .child(builderKeyFromFornisce(mense,tipoMenu,anno,mese,giornoSettimana));
        //_Synchronized
        FirebaseQueryLiveData_Synchronized risParziale = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(risParziale, new Deserializer_PietanzeDellaMense.Deserializer_List_Menu(mense, tipoMenu));
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU' SPECIFIC DAY                    */
/*================================================================================================*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificDay_OneTime(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu,
            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment_OneTime(mense,tipoMenu,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificDay(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu,
            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment(mense,tipoMenu,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificDay_Synchronized(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu,
            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment_Synchronized(mense,tipoMenu,anno,mese,giornoSettimana);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU' AND TIPO PIETANZA               */
/*                          SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR                         */
/*================================================================================================*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_OneTime(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu,
            @NonNull Long idTipoPietanza,
            @NonNull Integer anno,
            @NonNull Integer mese,
            @NonNull Integer giornoSettimana){

        MediatorLiveData<List<PietanzeDellaMense>> ris = new MediatorLiveData<>();
        ris.addSource(
                FB_Pietanze_Dao.getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_OneTime(mense.getId(), tipoMenu.getId(), idTipoPietanza,
                        anno, mese, giornoSettimana),
                new Observer<List<Pietanze>>() {
                    @Override
                    public void onChanged(List<Pietanze> pietanzes) {
                        List<PietanzeDellaMense> listaRis=new LinkedList<>();
                        Iterator<Pietanze> iterator = pietanzes.iterator();
                        while (iterator.hasNext()){
                            Pietanze pietanze=iterator.next();
                            if(pietanze!=null){
                                listaRis.add(new PietanzeDellaMense(mese,anno,giornoSettimana,tipoMenu,mense,pietanze));
                            }
                        }
                        ris.setValue(listaRis);
                    }
                });
        return ris;
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu,
            @NonNull Long idTipoPietanza,
            @NonNull Integer anno,
            @NonNull Integer mese,
            @NonNull Integer giornoSettimana){

        MediatorLiveData<List<PietanzeDellaMense>> ris = new MediatorLiveData<>();
        ris.addSource(
                FB_Pietanze_Dao.getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment(mense.getId(), tipoMenu.getId(), idTipoPietanza,
                        anno, mese, giornoSettimana),
                new Observer<List<Pietanze>>() {
                    @Override
                    public void onChanged(List<Pietanze> pietanzes) {
                        List<PietanzeDellaMense> listaRis=new LinkedList<>();
                        Iterator<Pietanze> iterator = pietanzes.iterator();
                        while (iterator.hasNext()){
                            Pietanze pietanze=iterator.next();
                            if(pietanze!=null){
                                listaRis.add(new PietanzeDellaMense(mese,anno,giornoSettimana,tipoMenu,mense,pietanze));
                            }
                        }
                        ris.setValue(listaRis);
                    }
                });
        return ris;
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_Synchronized(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu,
            @NonNull Long idTipoPietanza,
            @NonNull Integer anno,
            @NonNull Integer mese,
            @NonNull Integer giornoSettimana){

        MediatorLiveData<List<PietanzeDellaMense>> ris = new MediatorLiveData<>();
        ris.addSource(
                FB_Pietanze_Dao.getByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_Synchronized(mense.getId(), tipoMenu.getId(), idTipoPietanza,
                        anno, mese, giornoSettimana),
                new Observer<List<Pietanze>>() {
                    @Override
                    public void onChanged(List<Pietanze> pietanzes) {
                        List<PietanzeDellaMense> listaRis=new LinkedList<>();
                        Iterator<Pietanze> iterator = pietanzes.iterator();
                        while (iterator.hasNext()){
                            Pietanze pietanze=iterator.next();
                            if(pietanze!=null){
                                listaRis.add(new PietanzeDellaMense(mese,anno,giornoSettimana,tipoMenu,mense,pietanze));
                            }
                        }
                        ris.setValue(listaRis);
                    }
                });
        return ris;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA AND TIPO MENU' AND TIPO PIETANZA SPECIFIC DAY  */
/*================================================================================================*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay_OneTime(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu,
            @NonNull Long idTipoPietanza,
            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_OneTime( mense,tipoMenu,idTipoPietanza,
                anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu,
            @NonNull Long idTipoPietanza,
            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment( mense,tipoMenu,idTipoPietanza,
                anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificDay_Synchronized(
            @NonNull Mense mense,
            @NonNull TipoMenu tipoMenu,
            @NonNull Long idTipoPietanza,
            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_TipoMenu_TipoPietanza_SpecificMoment_Synchronized( mense,tipoMenu,idTipoPietanza,
                anno,mese,giornoSettimana);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by ID MENSA SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR  */
/*================================================================================================*/
    public  LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_SpecificMoment_OneTime(
            @NonNull Mense mense,
            @NonNull Integer anno,
            @NonNull Integer mese,
            @NonNull Integer giornoSettimana){
        MediatorLiveData<List<PietanzeDellaMense>> ris = new MediatorLiveData<>();
        //_OneTime
        ris.addSource(FB_TipoMenu_Dao.getAll_OneTime(),
                new Observer<List<TipoMenu>>() {
                    @Override
                    public void onChanged(List<TipoMenu> tipoMenus) {
                        List<LiveData<List<PietanzeDellaMense>>> listaRisultati =new LinkedList<>();
                        Iterator<TipoMenu> iterator=tipoMenus.iterator();
                        while (iterator.hasNext()){
                            TipoMenu tipoMenu = iterator.next();
                            if(tipoMenu!=null){
                                //_OneTime
                                listaRisultati.add(getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment_OneTime(
                                        mense,tipoMenu,anno,mese,giornoSettimana));
                            }
                        }
                        ris.addSource(mergeListOfLiveData(listaRisultati),
                                new Observer<List<PietanzeDellaMense>>() {
                                    @Override
                                    public void onChanged(List<PietanzeDellaMense> pietanzeDellaMenses) {
                                        ris.setValue(pietanzeDellaMenses);
                                    }
                                });
                    }
                });
        return ris;
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_SpecificMoment(
            @NonNull Mense mense,
            @NonNull Integer anno,
            @NonNull Integer mese,
            @NonNull Integer giornoSettimana){
        MediatorLiveData<List<PietanzeDellaMense>> ris = new MediatorLiveData<>();
        //_
        ris.addSource(FB_TipoMenu_Dao.getAll(),
                new Observer<List<TipoMenu>>() {
                    @Override
                    public void onChanged(List<TipoMenu> tipoMenus) {
                        List<LiveData<List<PietanzeDellaMense>>> listaRisultati =new LinkedList<>();
                        Iterator<TipoMenu> iterator=tipoMenus.iterator();
                        while (iterator.hasNext()){
                            TipoMenu tipoMenu = iterator.next();
                            if(tipoMenu!=null){
                                //_
                                listaRisultati.add(getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment(
                                        mense,tipoMenu,anno,mese,giornoSettimana));
                            }
                        }
                        ris.addSource(mergeListOfLiveData(listaRisultati),
                                new Observer<List<PietanzeDellaMense>>() {
                                    @Override
                                    public void onChanged(List<PietanzeDellaMense> pietanzeDellaMenses) {
                                        ris.setValue(pietanzeDellaMenses);
                                    }
                                });
                    }
                });
        return ris;
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_SpecificMoment_Synchronized(
            @NonNull Mense mense,
            @NonNull Integer anno,
            @NonNull Integer mese,
            @NonNull Integer giornoSettimana){
        MediatorLiveData<List<PietanzeDellaMense>> ris = new MediatorLiveData<>();
        //_Synchronized
        ris.addSource(FB_TipoMenu_Dao.getAll_Synchronized(),
                new Observer<List<TipoMenu>>() {
                    @Override
                    public void onChanged(List<TipoMenu> tipoMenus) {
                        List<LiveData<List<PietanzeDellaMense>>> listaRisultati =new LinkedList<>();
                        Iterator<TipoMenu> iterator=tipoMenus.iterator();
                        while (iterator.hasNext()){
                            TipoMenu tipoMenu = iterator.next();
                            if(tipoMenu!=null){
                                //_Synchronized
                                listaRisultati.add(getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment_Synchronized(
                                        mense,tipoMenu,anno,mese,giornoSettimana));
                            }
                        }
                        ris.addSource(mergeListOfLiveData(listaRisultati),
                                new Observer<List<PietanzeDellaMense>>() {
                                    @Override
                                    public void onChanged(List<PietanzeDellaMense> pietanzeDellaMenses) {
                                        ris.setValue(pietanzeDellaMenses);
                                    }
                                });
                    }
                });
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
/*                          METODO GET by ID MENSA SPECIFIC DAY                                   */
/*------------------------------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_SpecificDay_OneTime(
            @NonNull Mense mense,
            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_SpecificMoment_OneTime(mense,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_SpecificDay(
            @NonNull Mense mense,
            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_SpecificMoment(mense,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfMensa_SpecificDay_Synchronized(
            @NonNull Mense mense,
            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfMensa_SpecificMoment_Synchronized(mense,anno,mese,giornoSettimana);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                          METODO GET by TIPO MENU' SPECIFIC DAY OF WEEK OF THE MONTH OF THE YEAR*/
/*================================================================================================*/
    public  LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfTipoMenu_SpecificMoment_OneTime(
            @NonNull TipoMenu tipoMenu,
            @NonNull Integer anno,
            @NonNull Integer mese,
            @NonNull Integer giornoSettimana){
        MediatorLiveData<List<PietanzeDellaMense>> ris = new MediatorLiveData<>();
        //_OneTime
        ris.addSource(FB_Mense_Dao.getAll_OneTime(),
                new Observer<List<Mense>>() {
                    @Override
                    public void onChanged(List<Mense> menses) {
                        List<LiveData<List<PietanzeDellaMense>>> listaRisultati =new LinkedList<>();
                        Iterator<Mense> iterator=menses.iterator();
                        while (iterator.hasNext()){
                            Mense mense = iterator.next();
                            if(mense!=null){
                                //_OneTime
                                listaRisultati.add(getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment_OneTime(
                                        mense,tipoMenu,anno,mese,giornoSettimana));
                            }
                        }
                        ris.addSource(mergeListOfLiveData(listaRisultati),
                                new Observer<List<PietanzeDellaMense>>() {
                                    @Override
                                    public void onChanged(List<PietanzeDellaMense> pietanzeDellaMenses) {
                                        ris.setValue(pietanzeDellaMenses);
                                    }
                                });
                    }
                });
        return ris;
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfTipoMenu_SpecificMoment(
            @NonNull TipoMenu tipoMenu,
            @NonNull Integer anno,
            @NonNull Integer mese,
            @NonNull Integer giornoSettimana){
        MediatorLiveData<List<PietanzeDellaMense>> ris = new MediatorLiveData<>();
        //_
        ris.addSource(FB_Mense_Dao.getAll(),
                new Observer<List<Mense>>() {
                    @Override
                    public void onChanged(List<Mense> menses) {
                        List<LiveData<List<PietanzeDellaMense>>> listaRisultati =new LinkedList<>();
                        Iterator<Mense> iterator=menses.iterator();
                        while (iterator.hasNext()){
                            Mense mense = iterator.next();
                            if(mense!=null){
                                //_
                                listaRisultati.add(getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment(
                                        mense,tipoMenu,anno,mese,giornoSettimana));
                            }
                        }
                        ris.addSource(mergeListOfLiveData(listaRisultati),
                                new Observer<List<PietanzeDellaMense>>() {
                                    @Override
                                    public void onChanged(List<PietanzeDellaMense> pietanzeDellaMenses) {
                                        ris.setValue(pietanzeDellaMenses);
                                    }
                                });
                    }
                });
        return ris;
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfTipoMenu_SpecificMoment_Synchronized(
            @NonNull TipoMenu tipoMenu,
            @NonNull Integer anno,
            @NonNull Integer mese,
            @NonNull Integer giornoSettimana){
        MediatorLiveData<List<PietanzeDellaMense>> ris = new MediatorLiveData<>();
        //_Synchronized
        ris.addSource(FB_Mense_Dao.getAll_Synchronized(),
                new Observer<List<Mense>>() {
                    @Override
                    public void onChanged(List<Mense> menses) {
                        List<LiveData<List<PietanzeDellaMense>>> listaRisultati =new LinkedList<>();
                        Iterator<Mense> iterator=menses.iterator();
                        while (iterator.hasNext()){
                            Mense mense = iterator.next();
                            if(mense!=null){
                                //_Synchronized
                                listaRisultati.add(getPietanzeOfMenseByIdOfMensa_TipoMenu_SpecificMoment_Synchronized(
                                        mense,tipoMenu,anno,mese,giornoSettimana));
                            }
                        }
                        ris.addSource(mergeListOfLiveData(listaRisultati),
                                new Observer<List<PietanzeDellaMense>>() {
                                    @Override
                                    public void onChanged(List<PietanzeDellaMense> pietanzeDellaMenses) {
                                        ris.setValue(pietanzeDellaMenses);
                                    }
                                });
                    }
                });
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
/*                          METODO GET by TIPO MENU' SPECIFIC DAY                                 */
/*------------------------------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfTipoMenu_SpecificDay_OneTime(
            @NonNull TipoMenu tipoMenu,
            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfTipoMenu_SpecificMoment_OneTime(tipoMenu,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfTipoMenu_SpecificDay(
            @NonNull TipoMenu tipoMenu,
            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfTipoMenu_SpecificMoment(tipoMenu,anno,mese,giornoSettimana);
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<PietanzeDellaMense>> getPietanzeOfMenseByIdOfTipoMenu_SpecificDay_Synchronized(
            @NonNull TipoMenu tipoMenu,
            @NonNull Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer anno =localDate.getYear();
        Integer mese =localDate.getMonthValue();
        Integer giornoSettimana =localDate.getDayOfWeek().getValue();
        return this.getPietanzeOfMenseByIdOfTipoMenu_SpecificMoment_Synchronized(tipoMenu,anno,mese,giornoSettimana);
    }
/*================================================================================================*/
/*################################################################################################*/
}

/*################################################################################################*/
/*################################################################################################*/
/*################################################################################################*/
 class FB_PietanzeDellaMenseInsertExist_Dao  extends ConnesioneAFirebase<PietanzeDellaMense> {
     protected FB_Pietanze_Dao FB_Pietanze_Dao = new FB_Pietanze_Dao();
     protected FB_TipoMenu_Dao FB_TipoMenu_Dao = new FB_TipoMenu_Dao();
     protected FB_Mense_Dao    FB_Mense_Dao    = new FB_Mense_Dao();
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
 /*TODO    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(Pietanze pietanze);
/*------------------------------------------------------------------------*/
/*TODO     @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    public long insertPietazaAlMenuDellaMensa(Pietanze pietanze,
                                              Mense mense,
                                              TipoMenu tipoMenu,
                                              Integer anno,
                                              Integer mese) {
        if (mese <= 0 || mese > 12) {
            throw new SQLiteConstraintException("Mese inserito errato deve essere compreso tra 1 e 12 mentre e' " + mese + ".");
        }
        Long id = exist(pietanze);
        if (id == null) {
            id = this.insert(pietanze);
        }
        M_fornisce_P relazione = new M_fornisce_P(mense.getId(), id, mese, anno, tipoMenu.getId());
        this.insert(relazione);
        return id;
    }
/*------------------------------------------------------------------------*/
/*TODO     @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    public Long insertAssociazioneIngrediente(Pietanze pietanze, Ingredienti ingredienti) {
        List<Ingredienti> listaIngredienti = new LinkedList<Ingredienti>();
        listaIngredienti.add(ingredienti);
        List<Long> ris = this.insertAssociazioneIngrediente(pietanze, listaIngredienti);
        if (ris.isEmpty()) {
            throw new SQLiteConstraintException("Non è stato possible creare l'associazione.");
        }
        return ris.get(0);
    }
/*------------------------------------------------------------------------*/
/*TODO   @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    public List<Long> insertAssociazioneIngrediente(Pietanze pietanze, List<Ingredienti> ingredienti) {
        Iterator<Ingredienti> iterator = ingredienti.iterator();
        List<Long> ris = new ArrayList<>();
        while (iterator.hasNext()) {
            P_contiene_A contiene = new P_contiene_A(pietanze.getId(), iterator.next().getId());
            Long id = this.inset(contiene);
            ris.add(id);
        }
        return ris;
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
 /*TODO    @Insert(onConflict = OnConflictStrategy.ABORT)
    protected abstract long insert(M_fornisce_P m_fornisce_p);

/*------------------------------------------------------------------------*/
 /*TODO    @Insert(onConflict = OnConflictStrategy.IGNORE)
    protected abstract long inset(P_contiene_A contiene);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         EXIST                                                                  */
/*================================================================================================*/
 /*TODO    @Transaction
    public long exist(Pietanze pietanze) {
        return this.exist(pietanze.getNome(),
                pietanze.getCalorie(),
                pietanze.getCarboidrati(),
                pietanze.getGrassi(),
                pietanze.getProteine(),
                pietanze.getImmagine(),
                pietanze.getId_tipoPietanza());
    }

/*------------------------------------------------------------------------------------------------*/
 /*TODO    @Query("SELECT P.id " +
            "FROM tablePietanze AS P " +
            "WHERE   P.calorie == :calorie AND " +
            "P.carboidrati == :carboidrati AND " +
            "P.grassi == :grassi AND " +
            "P.id_tipoPietanza == :id_tipoPietanza AND " +
            "P.immagine == :immagine AND " +
            "P.nome == :nome AND " +
            "P.proteine == :proteine")
    protected abstract long exist(String nome,
                                  Double calorie,
                                  Double carboidrati,
                                  Double grassi,
                                  Double proteine,
                                  byte[] immagine,
                                  Long id_tipoPietanza);
/*================================================================================================*/
/*################################################################################################*/
}