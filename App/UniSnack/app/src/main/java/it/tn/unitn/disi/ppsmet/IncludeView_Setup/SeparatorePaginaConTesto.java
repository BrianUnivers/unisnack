package it.tn.unitn.disi.ppsmet.IncludeView_Setup;

import android.view.ViewGroup;
import android.widget.TextView;

import it.tn.unitn.disi.ppsmet.R;

public class SeparatorePaginaConTesto {
/*================================================================================================*/
/*                                  IMPOSTAZIONI DELLE VARIANTI DI BLOCHI INCLUSI                 */
/*================================================================================================*/
/**
 * Questi metodi permettono di impostatre i separatori delle pagine con un  testo
 */
    static public void setSeparatorePaginaConTestoDeiInclude(ViewGroup rootLayout, int idLayoutIncluded, int idText) {
        if(rootLayout!=null){
            ViewGroup barraPreferito = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(barraPreferito!=null){
                TextView label = (TextView) barraPreferito.findViewById(R.id.txt_LabelDivisore);
                label.setText(idText);
            }
        }
    }
/*================================================================================================*/
}
