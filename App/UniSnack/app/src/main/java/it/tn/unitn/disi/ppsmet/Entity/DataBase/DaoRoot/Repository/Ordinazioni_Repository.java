package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.room.OnConflictStrategy;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import it.tn.unitn.disi.ppsmet.Entity.Allergeni;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Allergeni_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Ordinazioni_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Ordinazioni_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.O_relativa_F;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.PietanzeDellaMense;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class Ordinazioni_Repository extends Ordinazioni_ThreadAggiornamento {
/*================================================================================================*/
/*                         RISULTATI SEMPRE AGGIORNATI                                            */
/*================================================================================================*/
    private LiveData<List<Ordinazioni>> mAllOrdinazioni;
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Ordinazioni_Repository(Application application) {
        super(application);
        //AllOrdinazioni get any time the same result
        mAllOrdinazioni = ordinazioniDao.getAll_Synchronized();
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERT                                                                 */
/*================================================================================================*/
  /*  public Future<Long> insert(Ordinazioni ordinazioni, List<PietanzeDellaMense> listaPietanzeDellaMense ){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                    @Override
                    public Long call() throws Exception {
                        return ordinazioniDao.insert(ordinazioni,listaPietanzeDellaMense);
                    }
                });
    }
/*------------------------------------------------------------------------*/
     public Future<Long> insertConVoto(Ordinazioni ordinazioni, List<OrdinazionePietanzeDellaMenseConVoto> listaPietanzeDellaMenseConVoto ){
        return  MyRoomDatabase.databaseWriteExecutor.submit(new Callable<Long>(){
                    @Override
                    public Long call() throws Exception {
                    try{
                        return ordinazioniDao.insertConVoto(ordinazioni,listaPietanzeDellaMenseConVoto);
                    }catch (Exception e){
                        Log.i("ERROR",e.getMessage());
                    }
                    return null;
                }
        });
     }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
 /*   public void deleteAll(){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            ordinazioniDao.deleteAll();
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE by ID                                                    */
/*================================================================================================*/
    public void delete(Ordinazioni ordinazioni){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            try {
                ordinazioniDao.delete(ordinazioni.getId());
            }catch (Exception e){
                Log.i("ERROR",e.getMessage());
            }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO DELETE by ID UTENTE ID TIPO PASTO AT SPECIFIC DAY               */
/*================================================================================================*/
    public void deleteByIdUtente_IdTipoPasto_AtSpecificDay(Ordinazioni ordinazioni){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            try {
                ordinazioniDao.deleteByIdUtente_IdTipoPasto_AtSpecificDay(ordinazioni.getId_utente(),
                        ordinazioni.getId_tipoPasto(),
                        ordinazioni.getGiorno());
            }catch (Exception e){
                Log.i("ERROR",e.getMessage());
            }
            });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
  /*  public void update(Ordinazioni ordinazioni){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
            ordinazioniDao.update(ordinazioni);
        });
    }
/*------------------------------------------------------------------------*/
    public void update(Ordinazioni ordinazioni, List<OrdinazionePietanzeDellaMenseConVoto> listaPietanzeDellaMenseConVoto ){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
        try {
            ordinazioniDao.update(ordinazioni,listaPietanzeDellaMenseConVoto);
        }catch (Exception e){
            Log.i("ERROR",e.getMessage());
        }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO OVERWRITE ALL Ordinazione                                       */
/*                                          AND OrdinazionePietanzeDellaMenseConVoto              */
/*================================================================================================*/
    public void overwriteAll(Ordinazioni ordinazioni, List<OrdinazionePietanzeDellaMenseConVoto> listaPietanzeDellaMenseConVoto ){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
        try {
                ordinazioniDao.overwriteAll(ordinazioni, listaPietanzeDellaMenseConVoto);
        }catch (Exception e){
            Log.i("ERROR",e.getMessage());
        }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO OVERWRITE OrdinazionePietanzeDellaMenseConVoto                  */
/*================================================================================================*/
    public void overwriteListaOrdinazionePietanzeDellaMenseConVoto(Ordinazioni ordinazioni, List<OrdinazionePietanzeDellaMenseConVoto> listaPietanzeDellaMenseConVoto ){
        MyRoomDatabase.databaseWriteExecutor.execute(()->{
        try {
            ordinazioniDao.overwriteListaOrdinazionePietanzeDellaMenseConVoto(ordinazioni.getId(),listaPietanzeDellaMenseConVoto);
        }catch (Exception e){
            Log.i("ERROR",e.getMessage());
        }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    /*public LiveData<List<Ordinazioni>> getAll(){
        return ordinazioniDao.getAll_Synchronized();
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
 /*   public LiveData<Ordinazioni> getByPrimaryKey(Long id){
        return ordinazioniDao.getByPrimaryKey_Synchronized(id);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID ORDINAZIONE                                           */
/*================================================================================================*/
    public LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> getPietanzeOrdinateByIdOfOrdinazioni(Long idOrdinazione){
        return ordinazioniDao.getPietanzeOrdinateByIdOfOrdinazioni_Synchronized(idOrdinazione);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
  /*  public LiveData<List<Ordinazioni>> getByIdOfUtenti(Utenti utenti){
        return ordinazioniDao.getByIdOfUtenti_Synchronized(utenti.getId());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE IN THE FUTURE                                  */
/*================================================================================================*/
    public LiveData<List<Ordinazioni>> getByIdOfUtenti_InTheFutureOfSpecificDate(Long idUtenti,Date date){
        return ordinazioniDao.getByIdOfUtenti_InTheFutureOfSpecificDate_Synchronized(idUtenti,date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET ORDINAZIONE PIETANZE DELLA MENSE CON VOTO                   */
/*                         by ID UTENTE IN THE FUTURE                                             */
/*================================================================================================*/
    public LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> getAllOrdinazionePietanzeDellaMenseConVotoByIdUtente_InTheFuture(Long idUtenti,Date date){
        return ordinazioniDao.getAllOrdinazionePietanzeDellaMenseConVotoByIdUtente_InTheFuture_Synchronized(idUtenti,date);
    }
/*================================================================================================*/

/*================================================================================================*/
/*                         METODO GET by ID UTENTE LAST EAT IN THIS DATA                          */
/*================================================================================================*/
  /*  public LiveData<List<Ordinazioni>> getLastEatBeforeDataByIdOfUtenti(Utenti utenti, Date date){
        return ordinazioniDao.getLastEatBeforeDataByIdOfUtenti_Synchronized(utenti.getId(),date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE LAST EAT IN THIS DATA                          */
/*================================================================================================*/
 /*   public LiveData<List<Ordinazioni>> getLastEatBeforeTodayByIdOfUtenti(Utenti utenti){
        return ordinazioniDao.getLastEatBeforeTodayByIdOfUtenti_Synchronized(utenti.getId());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND TIPO PASTO                                 */
/*================================================================================================*/
  /* public LiveData<List<Ordinazioni>> getByIdOfUtenti_TipoPasto(Utenti utenti, TipoPasto tipoPasto){
        return ordinazioniDao.getByIdOfUtenti_TipoPasto_Synchronized(utenti.getId(),tipoPasto.getId());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND TIPO PASTO  LAST EAT IN THIS DATA          */
/*================================================================================================*/
    public LiveData<Ordinazioni> getLastEatBeforeTodayByIdOfUtenti_IdTipoPasto(Utenti utenti, Long idTipoPasto){
        return ordinazioniDao.getLastEatBeforeTodayByIdOfUtenti_IdTipoPasto_Synchronized(utenti.getId(),idTipoPasto);
    }
/*------------------------------------------------------------------------------------------------*/
    public LiveData<Ordinazioni> getLastEatBeforeTodayByIdOfUtenti_IdTipoPasto(Long idUtenti, Long idTipoPasto){
        return ordinazioniDao.getLastEatBeforeTodayByIdOfUtenti_IdTipoPasto_Synchronized(idUtenti,idTipoPasto);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND TIPO PASTO  LAST EAT IN THIS DATA          */
/*================================================================================================*/
    public LiveData<Ordinazioni> getLastEatBeforeTodayByIdOfUtenti_ListIdTipoPasto(Utenti utenti, List<Long> listaIdTipoPasto){
        return ordinazioniDao.getLastEatBeforeTodayByIdOfUtenti_ListIdTipoPasto_Synchronized(utenti.getId(),listaIdTipoPasto);
    }
/*------------------------------------------------------------------------------------------------*/
    public LiveData<Ordinazioni> getLastEatBeforeTodayByIdOfUtenti_ListIdTipoPasto(Long idUtenti, List<Long> listaIdTipoPasto){
        return ordinazioniDao.getLastEatBeforeTodayByIdOfUtenti_ListIdTipoPasto_Synchronized(idUtenti,listaIdTipoPasto);
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND TIPO PASTO  LAST EAT IN THIS DATA          */
/*================================================================================================*/
   /*public  LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> getPietanzeOrdinateByIdOfOrdinazioni(Ordinazioni ordinazioni){
        return ordinazioniDao.getPietanzeOrdinateByIdOfOrdinazioni_Synchronized(ordinazioni.getId());
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE AND WITH DATE IN THIS LIST                     */
/*================================================================================================*/
    public LiveData<List<Ordinazioni>> getAllOrdinazioniByIdUtente_InListDate(Long idUtente, List<Date> listaData){
        return ordinazioniDao.getAllOrdinazioniByIdUtente_InListDate_Synchronized(idUtente,listaData);
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET PIETANZE ORDINATE by ID UTENTE AND TIPO PASTO               */
/*                         AT SPECIFIC DATA                                                       */
/*================================================================================================*/
    public LiveData<List<OrdinazionePietanzeDellaMenseConVoto>> getPietanzeOrdinateByIdUtente_IdTipoPasto_AtSpecificDate(Long idUntente, Long idTipoPasto, Date date){
        return ordinazioniDao.getPietanzeOrdinateByIdUtente_IdTipoPasto_AtSpecificDate_Synchronized(idUntente,idTipoPasto,date);
    }
/*================================================================================================*/
/*################################################################################################*/
}
