package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListOrdinPiet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.OrdinazionePietanzeDellaMenseConVoto;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;
import it.tn.unitn.disi.ppsmet.Entity.TipoMenu;
import it.tn.unitn.disi.ppsmet.Entity.TipoPasto;
import it.tn.unitn.disi.ppsmet.Entity.TipoPietanza;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.CheckableSubtitleView;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.DataWithLabelView;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.SeparatorePaginaConTesto;
import it.tn.unitn.disi.ppsmet.R;

public class PersonMenuSetOrdinRVAdapter extends RecyclerView.Adapter<PersonMenuSetOrdinRVAdapter.MyViewHoder> {
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE INTERNA                                                    */
/*################################################################################################*/
    /**
     * Questa classe è quella quella che viene usata per permettere alla RecyclingView Adapter di definire
     * il layout singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public ViewGroup             v_mainLayout;
        //Contenitorei
        public TableRow              v_ContenitoreMensaETipoPasto;
        public TableRow              v_ContenitoreDivisore;
        //Elemsnrti
        public CheckableSubtitleView    v_SottoticoloOrdine;
        public DataWithLabelView        v_Mensa;
        public DataWithLabelView        v_TipoMenu;
        public RecyclerView             v_ListaPietanzeETipoPietanza;
        //Elemsnrti Tabella
        public TextView v_Carboidrati;
        public TextView v_Grassi;
        public TextView v_Proteine;
        public TextView v_CalorieTotali;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout        = itemView.findViewById(R.id.cly_RowElemento);
            //Contenitorei
            v_ContenitoreMensaETipoPasto = itemView.findViewById(R.id.trw_ContenitoreMensaTipoPasto);
            v_ContenitoreDivisore        = itemView.findViewById(R.id.trw_Divisore);
            //Elemsnrti
            SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude((ViewGroup)v_ContenitoreDivisore,
                                                                            R.id.include_DivisoreMicronutrienti,
                                                                            R.string.lbl_dvd_Micronutrienti);
            v_SottoticoloOrdine = CheckableSubtitleView.newInstance(v_mainLayout,R.id.include_Titolo);
            v_SottoticoloOrdine.setCheckable(false);
            v_Mensa             = DataWithLabelView.newInstance(v_ContenitoreMensaETipoPasto,R.id.include_Mensa,R.string.lbl_Mensa);
            v_TipoMenu          = DataWithLabelView.newInstance(v_ContenitoreMensaETipoPasto,R.id.include_TipoMenu,R.string.lbl_TipoMenu);
            v_ListaPietanzeETipoPietanza = itemView.findViewById(R.id.rcl_ListaTipiPitanzeEPietanze);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(comtext);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            v_ListaPietanzeETipoPietanza.setLayoutManager(linearLayoutManager);
            //Elemsnrti Tabella
            v_Carboidrati   = itemView.findViewById(R.id.txt_ValueCarboidrati);
            v_Grassi        = itemView.findViewById(R.id.txt_ValueGrassi);
            v_Proteine      = itemView.findViewById(R.id.txt_ValueProteine);
            v_CalorieTotali = itemView.findViewById(R.id.txt_ValueCalorieTotali);
        }
/*================================================================================================*/
    }
/*################################################################################################*/


/*################################################################################################*/
/*                              CLASSE PRINCIPALE ModificaMenuSettimanaleGiorniRVAdapter          */
/*################################################################################################*/
    private Date IERI = null;

    private Context comtext = null;
    private List<Ordinazioni>  listaOrdinazioni = null;
    /**
     * Questo attributo contiene le liste delle pietanze associate all'id dell'ordine di cui fanno
     * parte.
     */
    private HashMap<Long,List<OrdinazionePietanzeDellaMenseConVoto>> hmListaPietanze  = null;
    private HashMap<Long,TipoPietanza> hmTipoPietanza  = null;
    private HashMap<Long,TipoPasto> hmTipoPasto  = null;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public PersonMenuSetOrdinRVAdapter(Context comtext,
                                       List<Ordinazioni> listaOrdinazioni,
                                       List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze,
                                       List<TipoPietanza> listaTipoPietanza,
                                       List<TipoPasto> listaTipoPasto) {
        //Calcola giorno ieri
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE,-1);
        this.IERI = calendar.getTime();
        //Prepara
        this.comtext = comtext;
        this.listaOrdinazioni  = impostaOrdineDelleOrdinazioni(listaOrdinazioni);
        this.hmListaPietanze   = generaStrutturaPietanzeOrdinazione(listaPietanze);
        this.hmTipoPietanza    = generaStrutturaTipoPietanza(listaTipoPietanza);
        this.hmTipoPasto       = generaStrutturaTipoPasto(listaTipoPasto);
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              CERAZIONE DEL SINGOLO ELEMETOTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(comtext);
        View bloccoElemeto = inflater.inflate(R.layout.row_ordinazioni, parent,false);
        MyViewHoder myViewHoder = new MyViewHoder(bloccoElemeto);
        return myViewHoder;
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              INSERIRE L'ELEMETO NEL LAYOUT                                     */
/*================================================================================================*/
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore ModificaMenuSettimanaleGiorniRVAdapter.
     */
    @Override
    public void onBindViewHolder(@NonNull MyViewHoder holder, int position) {
        final Ordinazioni elemento = listaOrdinazioni.get(position);
        final TipoPasto tipoPasto  = hmTipoPasto.get(elemento.getId_tipoPasto()) ;
        holder.v_SottoticoloOrdine.setData(elemento.getGiorno());
        holder.v_SottoticoloOrdine.setTipoPasto(tipoPasto);

        //Giorni futuri
        if(IERI.before(elemento.getGiorno())){
            holder.v_SottoticoloOrdine.setStatoAttivo(true);
        }else{
            holder.v_SottoticoloOrdine.setStatoAttivo(false);
        }
        //Pietanze
        List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze = hmListaPietanze.get(elemento.getId());
        //Seleziona il tipo di layout
        inserisciPietanzeNelLayout(holder,listaPietanze);
    }
/*================================================================================================*/
/*                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE                  */
/*================================================================================================*/
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaOrdinazioni.size();
    }
/*================================================================================================*/


/*================================================================================================*/
/*                              INSERISCI I DATI NEL LAYOUT                                       */
/*================================================================================================*/
    /**
     * Questo metodo si occupa di inserire i dati della pietanza che è quello attuale  nel layout del
     * singolo elemeto.
     */
    private void inserisciPietanzeNelLayout(@NonNull MyViewHoder holder, List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze){
        TipoMenu tipoMenu = null;
        Mense    mense    = null;
        if((listaPietanze!=null) && (!listaPietanze.isEmpty())){
           tipoMenu = listaPietanze.get(0).getTipoMenu();
           mense    = listaPietanze.get(0).getMense();
        }
        if((tipoMenu!=null) && (tipoMenu.getNome()!=null)){
            holder.v_TipoMenu.setValue(tipoMenu.getNome());
        }
        if((mense!=null) && (mense.getNome()!=null)){
            holder.v_Mensa.setValue(mense.getNome());
        }

        PersonMenuSetPietETPietRVAdapter adapter = new PersonMenuSetPietETPietRVAdapter(comtext,listaPietanze,hmTipoPietanza);
        holder.v_ListaPietanzeETipoPietanza.setAdapter(adapter);

        inserisciValoriMicronutrienti(holder, listaPietanze);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                              INSERISCI I DATI DEI MICONUTRIENTI NEL LAYOUT                     */
/*================================================================================================*/
    /**
     * Questo metodo permette di calcolare in base alle pietanze scelte i valori dei micronutrienti totali.
     * @param holder
     * @param listaPietanze
     */
    private void  inserisciValoriMicronutrienti(@NonNull MyViewHoder holder, List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze){
        Double carboidrati = .0;
        Double grassi = .0;
        Double proteine = .0;
        Double calorieTotali = .0;
        if(listaPietanze!=null){
            Iterator<OrdinazionePietanzeDellaMenseConVoto> iterator = listaPietanze.iterator();
            while (iterator.hasNext()){
                OrdinazionePietanzeDellaMenseConVoto pietanzaDellaMense =iterator.next();
                if((pietanzaDellaMense!=null) && (pietanzaDellaMense.getPietanze()!=null)){
                    Pietanze pietanze = pietanzaDellaMense.getPietanze();
                    if(pietanze.getCarboidrati()!=null){
                        carboidrati+=pietanze.getCarboidrati();
                    }
                    if(pietanze.getGrassi()!=null){
                        grassi+=pietanze.getGrassi();
                    }
                    if(pietanze.getProteine()!=null){
                        proteine+=pietanze.getProteine();
                    }
                    if(pietanze.getCalorie()!=null){
                        calorieTotali+=pietanze.getCalorie();
                    }
                }
            }
        }
        holder.v_Carboidrati.setText(  carboidrati.toString() +" g ");
        holder.v_Grassi.setText(       grassi.toString()+" g ");
        holder.v_Proteine.setText(     proteine.toString()+" g ");
        holder.v_CalorieTotali.setText(calorieTotali.toString()+" cal");
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                              METODI D'APPOGGIO AL ADAPTER                                      */
/*################################################################################################*/
/*================================================================================================*/
/*                              CEAZIONE DEL HASMAP DEI ORDINANZIONI PIETANZE                     */
/*================================================================================================*/
    /**
     * Questo metodo permette di separare le varie pietanze per le singole ordinazioni, associando
     * la lista delle pietanze all'id del ordinazione a cui fanno riferimento.
     * @param listaPietanze
     * @return
     */
    private HashMap<Long,List<OrdinazionePietanzeDellaMenseConVoto>> generaStrutturaPietanzeOrdinazione(
                                                                    List<OrdinazionePietanzeDellaMenseConVoto> listaPietanze){
        HashMap<Long,List<OrdinazionePietanzeDellaMenseConVoto>> hmListaPietanzePerOrdinazione = new HashMap<>();
        if(listaPietanze!=null){
            Iterator<OrdinazionePietanzeDellaMenseConVoto> iterator = listaPietanze.iterator();
            while (iterator.hasNext()){
                OrdinazionePietanzeDellaMenseConVoto pietanzePerOrdinazione = iterator.next();
                if((pietanzePerOrdinazione!=null) && (pietanzePerOrdinazione.getIdOrdinazione()!=null)){
                    Long key = pietanzePerOrdinazione.getIdOrdinazione();
                    List<OrdinazionePietanzeDellaMenseConVoto> listaTemp = new LinkedList<>();
                    if(hmListaPietanzePerOrdinazione.get(key)!=null){
                        listaTemp.addAll(hmListaPietanzePerOrdinazione.get(key));
                    }
                    listaTemp.add(pietanzePerOrdinazione);
                    hmListaPietanzePerOrdinazione.put(key,listaTemp);
                }
            }
        }
        return hmListaPietanzePerOrdinazione;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                              CEAZIONE DEL HASMAP DEI TIPI DI PIETANZE                          */
/*================================================================================================*/
    /**
     * Questo metodo permette di defineire un metodo di ricecrca rapido per i vari tipi di pietanze,
     * cioè si associa ad ogni tipo di pietanza il suo id come chiave di ricerca.
     * @param listaTipoPietanza
     * @return
     */
    private HashMap<Long,TipoPietanza> generaStrutturaTipoPietanza(List<TipoPietanza> listaTipoPietanza){
        HashMap<Long,TipoPietanza> ris = new HashMap<>();
        if(listaTipoPietanza!=null){
            Iterator<TipoPietanza> iterator = listaTipoPietanza.iterator();
            while (iterator.hasNext()){
                TipoPietanza tipoPietanza = iterator.next();
               if((tipoPietanza!=null) && (tipoPietanza.getId()!=null)){
                   ris.put(tipoPietanza.getId(),tipoPietanza);
               }
            }
        }
        return ris;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                              CEAZIONE DEL HASMAP DEI TIPI PASTO                                */
/*================================================================================================*/

    /**
     * Questo metodo permette di defineire un metodo di ricecrca rapido per i vari tipi di pasto,
     * cioè si associa ad ogni tipo di pasto il suo id come chiave di ricerca.
     * @param listaTipoPasto
     * @return
     */
    private HashMap<Long,TipoPasto> generaStrutturaTipoPasto(List<TipoPasto> listaTipoPasto){
        HashMap<Long,TipoPasto> ris = new HashMap<>();
        if(listaTipoPasto!=null){
            Iterator<TipoPasto> iterator = listaTipoPasto.iterator();
            while (iterator.hasNext()){
                TipoPasto tipoPasto = iterator.next();
                if((tipoPasto!=null) && (tipoPasto.getId()!=null)){
                    ris.put(tipoPasto.getId(),tipoPasto);
                }
            }
        }
        return ris;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                              IMPOSTA L'ORDINE DI VISUALIZZAZIONE DEI ORDINI                    */
/*================================================================================================*/
    /**
     * Questo metodo permette di ottenere una lista con le ordinazioni dei giorni futuri a partire da
     * oggi come primi elementi,e poi seguiti dai ordini più vecchi fino ad raggiungere quelli di ieri.
     * @param listaOrdinazioni
     * @return
     */
    private List<Ordinazioni> impostaOrdineDelleOrdinazioni(List<Ordinazioni> listaOrdinazioni){
        List<Ordinazioni> ris = new LinkedList<>();
        List<Ordinazioni> ordiniPassati = new LinkedList<>();
        List<Ordinazioni> ordiniFuturiEOggi = new LinkedList<>();

        if(listaOrdinazioni!=null){
            Iterator<Ordinazioni> iterator = listaOrdinazioni.iterator();
            while (iterator.hasNext()){
                Ordinazioni ordinazioni = iterator.next();
                if((ordinazioni!=null)&& (ordinazioni.getGiorno()!=null)){
                    //SELEZIONA E INSERISCI GLI ORNDINI IN BASE AL GIORNO
                    if(IERI.before(ordinazioni.getGiorno())){
                        ordiniFuturiEOggi.add(ordinazioni);
                    }else{
                        ordiniPassati.add(ordinazioni);
                    }
                }
            }
        }
        //Ordinare
        ordiniFuturiEOggi.sort( new OrderOrdinazioni());
        ordiniPassati.sort( new OrderOrdinazioni());
        //Insrrimento ordinato
        ris.addAll(ordiniFuturiEOggi);
        ris.addAll(ordiniPassati);
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                              CLASSE D'APPOGGIO PER RIORDINARE GLI ORDINI                       */
/*################################################################################################*/
    /**
     * Questa classe d'appoggio ci permette di ordinare gli ordini in modo di data crescente e se
     * le date sono uguali allora per tipo pasto crescente.
     */
    private class OrderOrdinazioni implements Comparator<Ordinazioni>{
        public int compare(Ordinazioni a, Ordinazioni b){
            int ris;
            //Ordine A
            Calendar calendarA = Calendar.getInstance();
            calendarA.setTime(a.getGiorno());
            calendarA.set(Calendar.MILLISECOND,0);
            calendarA.set(Calendar.SECOND,0);
            calendarA.set(Calendar.MINUTE,0);
            calendarA.set(Calendar.HOUR_OF_DAY,0);
            Date dateA = calendarA.getTime();

            //Ordine B
            Calendar calendarB = Calendar.getInstance();
            calendarB.setTime(b.getGiorno());
            calendarB.set(Calendar.MILLISECOND,0);
            calendarB.set(Calendar.SECOND,0);
            calendarB.set(Calendar.MINUTE,0);
            calendarB.set(Calendar.HOUR_OF_DAY,0);
            Date dateB = calendarB.getTime();


            if(dateA.equals(dateB)){
                if(a.getId_tipoPasto()<=b.getId_tipoPasto()){
                    ris = -1;
                }else {
                    ris = 1;
                }
            }else if(dateA.before(dateB)){
                ris = -1;
            }else{
                ris = 1;
            }
            return ris;
        }
    }
/*################################################################################################*/
}