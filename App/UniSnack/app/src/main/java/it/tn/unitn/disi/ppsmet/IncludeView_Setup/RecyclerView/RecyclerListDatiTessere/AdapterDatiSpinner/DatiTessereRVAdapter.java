package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListDatiTessere.AdapterDatiSpinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.Tessere;
import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListOrdinPiet.PersonMenuSetOrdinRVAdapter;
import it.tn.unitn.disi.ppsmet.R;

public class DatiTessereRVAdapter extends RecyclerView.Adapter<DatiTessereRVAdapter.MyViewHoder> {
/*################################################################################################*/
/*                              CLASSE INTERNA                                                    */
/*################################################################################################*/
    /**
     * Questa classe è quella che permette alla RecyclingView Adapter di definire un layout custom
     * del singolo elemeto.
     */
    protected class MyViewHoder extends RecyclerView.ViewHolder  {
        public View      v_mainLayout;
        //Informazioni generali
        public TextView  v_universitaView;
        //Righe
        public TableRow  v_rigaMatricola;
        public TableRow  v_rigaCorsoDiLaurea;
        //Valori
        public TextView  v_matricolaView;
        public TextView  v_corsoDiLaureaView;
        public TextView  v_annoAccademicoView;
        public TextView  v_statoTesseraView;
        public ImageView v_imgStatoTesseraView;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
        /**
         * Nel costruttore si estrae i vari elemeti del layout che dovranno contenere le informazioni
         * del singolo elemto così che quando passo un oggeto di tipo MyViewHoder posso accedere
         * direttamete a quelle view del layout.
         */
        public MyViewHoder(@NonNull View itemView) {
            super(itemView);
            v_mainLayout         = itemView.findViewById(R.id.cly_Tessera);
            v_universitaView     = itemView.findViewById(R.id.txt_ValoreUniversita);
            //Righe non obbligatorie.
            v_rigaMatricola      = itemView.findViewById(R.id.trw_RigaMatricola);
            v_rigaCorsoDiLaurea  = itemView.findViewById(R.id.trw_RigaCorsoDiLaurea);
            //Valori
            v_matricolaView      = itemView.findViewById(R.id.txt_ValueMatricola);
            v_corsoDiLaureaView  = itemView.findViewById(R.id.txt_ValueCorsoDiLaurea);
            v_statoTesseraView   = itemView.findViewById(R.id.txt_ValueSatatoTessera);
            v_imgStatoTesseraView= itemView.findViewById(R.id.img_StatoTessera);
            v_annoAccademicoView = itemView.findViewById(R.id.txt_ValueAnnoAccademico);
        }
/*================================================================================================*/
    }
/*################################################################################################*/



/*################################################################################################*/
/*                              CLASSE PRINCIPALE CampionatoRVAdapter                             */
/*################################################################################################*/
    private Context comtext=null;
    private List<Tessere> listaTessere=null;
/*================================================================================================*/
/*                              COSTRUTTORI                                                       */
/*================================================================================================*/
    /**
     * Nel costruttore si prende l'informazioni riguardante il contesto in cui ci si trova e i dati
     * sottoforma di lista che si vuole vedere nella RecyclingView.
     */
    public DatiTessereRVAdapter(Context comtext, List<Tessere> listaTessere) {
        this.comtext = comtext;
        this.listaTessere = listaTessere;
        Collections.sort(this.listaTessere,new OrderTessere());
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              CERAZIONE DEL SINGOLO ELEMETOTO                                   */
/*================================================================================================*/
    /**
     * Questo metodo specifica il layout da usare per rappresentare il singolo elemento però non definisce
     * ne come inserire i dati ne quali.
     */
    @NonNull
    @Override
    public MyViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(comtext);
        View bloccoElemeto = inflater.inflate(R.layout.row_tessere, parent,false);
        return new MyViewHoder(bloccoElemeto);
    }
/*================================================================================================*/

/*================================================================================================*/
/*                              INSERIRE L'ELEMETO NEL LAYOUT                                     */
/*================================================================================================*/
    /**
     * In questo metodo si riceve un oggeto MyViewHoder che contiene i vari elemeti in cui inserire
     * le informazioni del elemeto in postizione <<position>> dopo averlo estratto dalla lista ricevuta
     * dal costruttore CampionatoRVAdapter.
     * Inoltre aggiunge la possibilità di premere sul singolo oggeto così da aprire un altra activity.
     * <<SingoloCampionatoActivity>>
     */
    @Override
    public void onBindViewHolder(@NonNull MyViewHoder holder, int position) {
        final Tessere tessere =listaTessere.get(position);
        //Inserisci i dati nel layout
        inserisciTessereNelLayout(holder, tessere);
    }
/*================================================================================================*/
/*                              NUMERO DI ELEMETI PRESENTI CHE POSSO VISUALIZARE                  */
/*================================================================================================*/
    /**
     * Questa funzione deve ritornare il numero di elemeti che possono essere visualizati cioè il numero
     * di elemeti presenti nella lista passata al costruttore.
     */
    @Override
    public int getItemCount() {
        return listaTessere.size();
    }
/*================================================================================================*/


/*================================================================================================*/
/*                              INSERISCI I DATI NEL LAYOUT                                       */
/*================================================================================================*/
    /**
     * Questo metodo si occupa di inserire i dati del campinato nel layout del singolo elemeto.
     */
    private void inserisciTessereNelLayout(@NonNull MyViewHoder holder, Tessere tessere){
        //Imposta Titolo
        holder.v_universitaView.setText(tessere.getUniversita().getNome());
        //Impota righe non obbligatorie
        if(tessere.getMatricola()!=null){
            holder.v_rigaMatricola.setVisibility(View.VISIBLE);
            holder.v_matricolaView.setText(tessere.getMatricola().toString());
        }else{
            holder.v_rigaMatricola.setVisibility(View.GONE);
            holder.v_matricolaView.setText("");
        }
        if(tessere.getCorsi()!=null){
            holder.v_rigaCorsoDiLaurea.setVisibility(View.VISIBLE);
            holder.v_corsoDiLaureaView.setText(tessere.getCorsi().getSigla());
        }else{
            holder.v_rigaCorsoDiLaurea.setVisibility(View.GONE);
            holder.v_corsoDiLaureaView.setText("");
        }
        //Impota righe obbligatorie
        holder.v_annoAccademicoView.setText(tessere.getAnnoAccademico().getStringPeriodo());

        //Imposta Layout e icona
        if(tessere.getStatoTessera()){
            //Attiva
            holder.v_statoTesseraView.setText(R.string.txt_Attiva);
            holder.v_imgStatoTesseraView.setImageResource(R.drawable.ic_baseline_check_circle_24_primary);
            holder.v_statoTesseraView.setTextColor(comtext.getResources().getColor(R.color.colorPrimary,null));
        }else{
            holder.v_statoTesseraView.setText(R.string.txt_Disattiva);
            holder.v_imgStatoTesseraView.setImageResource(R.drawable.ic_baseline_error_24_error);
            holder.v_statoTesseraView.setTextColor(comtext.getResources().getColor(R.color.colorError,null));
        }
    }
/*================================================================================================*/
/*################################################################################################*/

/*================================================================================================*/
/*                              SORT NOTIFY DATA CHANGE                                           */
/*================================================================================================*/
    /**
     * Questo metodo permette, oltre a notificare il cambiamento dei dati utilizati dal adapter come
     * indicato dal metodo "notifyDataSetChanged" li ordina prima, così da visualizzare sempre come
     * prima tessera quella di cui l'utente è iscritto all universita (anche quella con il lay)
     */
    public void notifySortDataSetChanged(){
        Collections.sort(this.listaTessere,new OrderTessere());
        this.notifyDataSetChanged();
    }
/*================================================================================================*/
/*################################################################################################*/
/*                              CLASSE D'APPOGGIO PER RIORDINARE LE TESSERE                       */
/*################################################################################################*/
    /**
     * Questa classe d'appoggio ci permette di ordinare le tessere in modo tale che la tessera che
     * dell'universita a cui lo studente si è iscritto sia la prima e che sia seguita da tutte quelle
     * attive e poi quelle disattive.
     */
    private class OrderTessere implements Comparator<Tessere> {
        public int compare(Tessere a, Tessere b){
            int ris;

            if((a.getMatricola()!=null) && (b.getMatricola()!=null)){
                if((a.getStatoTessera() == b.getStatoTessera())){
                    if(a.getMatricola()<=b.getMatricola()){
                        ris = -1;
                    }else{
                        ris = 1;
                    }
                }else if(a.getStatoTessera()==true){
                    ris = -1;
                }else if(b.getStatoTessera()==true){
                    ris = 1;
                }else{
                    ris = 0;//Non dovrebbe accadere
                }
            }else if(a.getMatricola()!=null){
                ris = -1;
            }else if(b.getMatricola()!=null){
                ris = 1;
            }else{
                if((a.getStatoTessera() == b.getStatoTessera())){
                    ris = 0;
                }else if(a.getStatoTessera()==true){
                    ris = -1;
                }else if(b.getStatoTessera()==true){
                    ris = 1;
                }else{
                    ris = 0;//Non dovrebbe accadere
                }
            }
            return ris;
        }
    }
/*################################################################################################*/
}
