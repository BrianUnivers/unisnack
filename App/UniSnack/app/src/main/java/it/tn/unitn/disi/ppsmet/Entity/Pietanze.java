package it.tn.unitn.disi.ppsmet.Entity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Arrays;
import java.util.Objects;

@Entity(tableName = "tablePietanze",
        foreignKeys = {@ForeignKey(entity = TipoPietanza.class,
                parentColumns = "id",             //Colanna della classe a qui si rifierisce
                childColumns  = "id_tipoPietanza",//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE)},
        indices = @Index("id_tipoPietanza"))
public class Pietanze {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @PrimaryKey
    private Long    id;
    @NonNull
    @ColumnInfo(name = "nome")
    private String  nome;
    @ColumnInfo(name = "calorie")
    private Double  calorie     = null;
    @ColumnInfo(name = "carboidrati")
    private Double  carboidrati = null;
    @ColumnInfo(name = "grassi")
    private Double  grassi      = null;
    @ColumnInfo(name = "proteine")
    private Double  proteine   = null;
    @ColumnInfo(name = "immagine")
    private byte[]  immagine    = null;
    @ColumnInfo(name = "urlImmagine")
    private String  urlImmagine    = null;
    @NonNull
    @ColumnInfo(name = "id_tipoPietanza")
    private Long id_tipoPietanza;
    @Ignore
    private TipoPietanza tipoPietanza = null;//Oggetto non obbligatorio ma fortemete legato.
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    @Ignore
    public Pietanze() {}
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Pietanze(Long id,
                    @NonNull String nome,
                    @NonNull Long id_tipoPietanza) {
        this.id = id;
        this.nome = nome;
        this.id_tipoPietanza = id_tipoPietanza;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Pietanze(Long id,
                    String nome,
                    TipoPietanza tipoPietanza) {
        this.id = id;
        this.nome = nome;
        this.id_tipoPietanza = null;
        setTipoPietanza(tipoPietanza);
    }
//--------------------------------------------------------------------------------------------------
    public Pietanze(Long id,
                    @NonNull String nome,
                    Double calorie,
                    Double carboidrati,
                    Double grassi,
                    Double proteine,
                    byte[] immagine,
                    String urlImmagine,
                    @NonNull Long id_tipoPietanza) {
        this.id = id;
        this.nome = nome;
        this.calorie = calorie;
        this.carboidrati = carboidrati;
        this.grassi = grassi;
        this.proteine = proteine;
        this.immagine = immagine;
        this.urlImmagine = urlImmagine;
        this.id_tipoPietanza = id_tipoPietanza;
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public Pietanze(Long id,
                    String nome,
                    Double calorie,
                    Double carboidrati,
                    Double grassi,
                    Double proteine,
                    byte[] immagine,
                    String urlImmagine,
                    TipoPietanza tipoPietanza) {
        this.id = id;
        this.nome = nome;
        this.calorie = calorie;
        this.carboidrati = carboidrati;
        this.grassi = grassi;
        this.proteine = proteine;
        this.immagine = immagine;
        this.urlImmagine = urlImmagine;
        this.id_tipoPietanza = null;
        setTipoPietanza(tipoPietanza);
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    public Long getId() {
        return id;
    }
//--------------------------------------
    public void setId(Long id) {
        this.id = id;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public String getNome() {
        return nome;
    }
//--------------------------------------
    public void setNome(@NonNull String nome) {
        this.nome = nome;
    }
//--------------------------------------------------------------------------------------------------
    public Double getCalorie() {
        return calorie;
    }
//--------------------------------------
    public void setCalorie(Double calorie) {
        this.calorie = calorie;
    }
//--------------------------------------------------------------------------------------------------
    public Double getCarboidrati() {
        return carboidrati;
    }
//--------------------------------------
    public void setCarboidrati(Double carboidrati) {
        this.carboidrati = carboidrati;
    }
//--------------------------------------------------------------------------------------------------
    public Double getGrassi() {
        return grassi;
    }
//--------------------------------------
    public void setGrassi(Double grassi) {
        this.grassi = grassi;
    }
//--------------------------------------------------------------------------------------------------
    public Double getProteine() {
        return proteine;
    }
//--------------------------------------
    public void setProteine(Double proteine) {
        this.proteine = proteine;
    }
//--------------------------------------------------------------------------------------------------
    public byte[] getImmagine() {
        return immagine;
    }
    public Bitmap getBitmapImmagine() {
        //Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
        //Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);
        return BitmapFactory.decodeByteArray(immagine, 0, immagine.length);
    }
//--------------------------------------
    public void setImmagine(byte[] immagine) {
        this.immagine = immagine;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_tipoPietanza() {
        return id_tipoPietanza;
    }
//--------------------------------------
    public void setId_tipoPietanza(@NonNull Long id_tipoPietanza) {
        this.id_tipoPietanza = id_tipoPietanza;
        if((tipoPietanza!=null) && (id_tipoPietanza!=null)  && (id_tipoPietanza!=tipoPietanza.getId())){
            tipoPietanza=null;
        }else if(id_tipoPietanza==null){
            tipoPietanza=null;//TODO: Oppure mantenrere lo stesso id  di tipo
        }
    }
//--------------------------------------------------------------------------------------------------
    @Ignore
    public TipoPietanza getTipoPietanza() {
        return tipoPietanza;
    }
//------------------------------------
    @Ignore
    public boolean setTipoPietanza(TipoPietanza tipoPietanza){
        boolean ris = true;
        if((id_tipoPietanza!=null) && (id_tipoPietanza==tipoPietanza.getId()) ){
            this.tipoPietanza = tipoPietanza;
        }else if((id_tipoPietanza==null) && (tipoPietanza!=null)){
            this.id_tipoPietanza = tipoPietanza.getId();
            this.tipoPietanza = tipoPietanza;
        }else{
            ris = false;
        }
        return ris;
    }
//--------------------------------------------------------------------------------------------------
    public String getUrlImmagine() {
        return urlImmagine;
    }
//------------------------------------
    public void setUrlImmagine(String urlImmagine) {
        this.urlImmagine = urlImmagine;
    }
//==================================================================================================
//==================================================================================================
//                                      EQUALS E HASH_CODE
//==================================================================================================
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pietanze)) return false;
        Pietanze pietanze = (Pietanze) o;
        return Objects.equals(getId(), pietanze.getId()) &&
                getNome().equals(pietanze.getNome()) &&
                Objects.equals(getCalorie(), pietanze.getCalorie()) &&
                Objects.equals(getCarboidrati(), pietanze.getCarboidrati()) &&
                Objects.equals(getGrassi(), pietanze.getGrassi()) &&
                Objects.equals(getProteine(), pietanze.getProteine()) &&
                Objects.equals(getUrlImmagine(), pietanze.getUrlImmagine()) &&
                getId_tipoPietanza().equals(pietanze.getId_tipoPietanza());
    }
//--------------------------------------------------------------------------------------------------
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNome(), getCalorie(), getCarboidrati(), getGrassi(), getProteine(), getUrlImmagine(), getId_tipoPietanza());
    }
//==================================================================================================
//##################################################################################################
}
