package it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;


import it.tn.unitn.disi.ppsmet.Entity.Ordinazioni;

@Entity(tableName = "tableO_relativa_F",
        primaryKeys = {"id_ordinazione",
                       "id_f_mensa", "id_f_pietanza", "id_f_mese", "id_f_anno","id_f_giornoSettimana", "id_f_tipoMenu"},
        foreignKeys = {
            @ForeignKey(entity = Ordinazioni.class,
                parentColumns = "id",             //Colanna della classe a qui si rifierisce
                childColumns  = "id_ordinazione",//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE),
            @ForeignKey(entity = M_fornisce_P.class,
                parentColumns = {"id_mensa",
                                "id_pietanza",
                                "mese",
                                "anno",
                                "giornoSettimana",
                                "id_tipoMenu"},//Colanna della classe a qui si rifierisce
                childColumns  = {"id_f_mensa",
                                 "id_f_pietanza",
                                 "id_f_mese",
                                 "id_f_anno",
                                 "id_f_giornoSettimana",
                                 "id_f_tipoMenu"},//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE)},
        indices = @Index(value = {"id_f_mensa","id_f_pietanza","id_f_mese","id_f_anno","id_f_giornoSettimana","id_f_tipoMenu"}))
public class O_relativa_F {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @ColumnInfo(name = "id_ordinazione")
    private Long    id_ordinazione;
    @NonNull
    @ColumnInfo(name = "id_f_mensa")
    private Long    id_f_mensa;
    @NonNull
    @ColumnInfo(name = "id_f_pietanza")
    private Long    id_f_pietanza;
    @NonNull
    @ColumnInfo(name = "id_f_mese")
    private Integer id_f_mese;//tra 1 e 12 compresi
    @NonNull
    @ColumnInfo(name = "id_f_anno")
    private Integer id_f_anno;
    @NonNull
    @ColumnInfo(name = "id_f_giornoSettimana")
    private Integer id_f_giornoSettimana;//tra 1 e 7 compresi
    @NonNull
    @ColumnInfo(name = "id_f_tipoMenu")
    private Long    id_f_tipoMenu;
    @ColumnInfo(name = "votoPietanza")
    private Short   votoPietanza = null;//tra 0 e 10 compresi
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    @Ignore
    public O_relativa_F(@NonNull Long id_ordinazione,
                        @NonNull Long id_f_mensa,
                        @NonNull Long id_f_pietanza,
                        @NonNull Integer id_f_mese,
                        @NonNull Integer id_f_anno,
                        @NonNull Integer id_f_giornoSettimana,
                        @NonNull Long id_f_tipoMenu) {
        this.id_ordinazione = id_ordinazione;
        this.id_f_mensa = id_f_mensa;
        this.id_f_pietanza = id_f_pietanza;
        this.id_f_mese = id_f_mese;
        this.id_f_anno = id_f_anno;
        this.id_f_giornoSettimana = id_f_giornoSettimana;
        this.id_f_tipoMenu = id_f_tipoMenu;
    }
//--------------------------------------------------------------------------------------------------
    public O_relativa_F(@NonNull Long id_ordinazione,
                        @NonNull Long id_f_mensa,
                        @NonNull Long id_f_pietanza,
                        @NonNull Integer id_f_mese,
                        @NonNull Integer id_f_anno,
                        @NonNull Integer id_f_giornoSettimana,
                        @NonNull Long id_f_tipoMenu,
                        Short votoPietanza) {
        this.id_ordinazione = id_ordinazione;
        this.id_f_mensa     = id_f_mensa;
        this.id_f_pietanza  = id_f_pietanza;
        this.id_f_mese      = id_f_mese;
        this.id_f_anno      = id_f_anno;
        this.id_f_giornoSettimana = id_f_giornoSettimana;
        this.id_f_tipoMenu  = id_f_tipoMenu;
        this.votoPietanza   = votoPietanza;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    @NonNull
    public Long getId_ordinazione() {
        return id_ordinazione;
    }
//--------------------------------------
    public void setId_ordinazione(@NonNull Long id_ordinazione) {
        this.id_ordinazione = id_ordinazione;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_f_mensa() {
        return id_f_mensa;
    }
//--------------------------------------
    public void setId_f_mensa(@NonNull Long id_f_mensa) {
        this.id_f_mensa = id_f_mensa;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_f_pietanza() {
        return id_f_pietanza;
    }
//--------------------------------------
    public void setId_f_pietanza(@NonNull Long id_f_pietanza) {
        this.id_f_pietanza = id_f_pietanza;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Integer getId_f_mese() {
        return id_f_mese;
    }
//--------------------------------------
    public void setId_f_mese(@NonNull Integer id_f_mese) {
        this.id_f_mese = id_f_mese;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Integer getId_f_anno() {
        return id_f_anno;
    }
//--------------------------------------
    public void setId_f_anno(@NonNull Integer id_f_anno) {
        this.id_f_anno = id_f_anno;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Integer getId_f_giornoSettimana() {
        return id_f_giornoSettimana;
    }
//--------------------------------------
    public void setId_f_giornoSettimana(@NonNull Integer id_f_giornoSettimana) {
        this.id_f_giornoSettimana = id_f_giornoSettimana;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_f_tipoMenu() {
        return id_f_tipoMenu;
    }
//--------------------------------------
    public void setId_f_tipoMenu(@NonNull Long id_f_tipoMenu) {
        this.id_f_tipoMenu = id_f_tipoMenu;
    }
//--------------------------------------------------------------------------------------------------
    public Short getVotoPietanza() {
        return votoPietanza;
    }
//--------------------------------------
    public void setVotoPietanza(Short votoPietanza) {
        this.votoPietanza = votoPietanza;
    }
//==================================================================================================
//##################################################################################################
}
