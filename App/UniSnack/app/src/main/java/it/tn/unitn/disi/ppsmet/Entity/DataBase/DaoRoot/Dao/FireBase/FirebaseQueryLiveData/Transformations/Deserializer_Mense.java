package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;

import java.util.Date;
import java.util.Iterator;

import it.tn.unitn.disi.ppsmet.Entity.Ingredienti;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_List;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Interface.GenericDeserializer_Object;

public class Deserializer_Mense implements CostantiPerJSON {
/*################################################################################################*/
/**
 * Questa classe astratta contiene il metodo per escrarre da un oggetto "DataSnapshot" i dati che
 * forma l'oggetto Mense.
 */
/*================================================================================================*/
/*                         GET OBJECT                                                             */
/*================================================================================================*/
    private static String LOG_TAG = "Mense_FromDS";
/*------------------------------------------------------------------------------------------------*/
/*                         GET OBJECT FROM DataSnapshot                                           */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce un singolo ogetto "Mense" dai dati passati da "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "Mense".
 * @return  Il riutato è l'oggetto creato e se non è stato possibile crearlo sara null.
 */
    public static Mense estraiDaDataSnapshot(DataSnapshot dataSnapshot,Long idUniversita){
        Mense mense = null;
        if(dataSnapshot.exists()){
            try{
                //Campi not null
                Long id      = Long.parseLong( dataSnapshot.getKey());
                String nome  = dataSnapshot.child(P_NOME).getValue(String.class);
                String sigla = dataSnapshot.child(P_SIGLA).getValue(String.class);
                //Verifica campi not null
                if((id!=null) && (nome!=null) && (sigla!=null)){
                    mense = new Mense(id,nome,sigla,idUniversita);
                    mense.setLatitudine(  dataSnapshot.child(P_LATITUDINE).getValue(Double.class));
                    mense.setLongitudine( dataSnapshot.child(P_LONGITUDINE).getValue(Double.class));
                }
            }catch (Exception e){
                Log.d(LOG_TAG, "Immposibile costruire un oggetto da un possibile risultato.",e);
                mense = null;
            }
        }
        return mense;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         GET AN ACTRIBUTE OBJECT FOR AN OTHER                                   */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET AN ACTRIBUTE                                                       */
/*------------------------------------------------------------------------------------------------*/
/**
 * Qusrto metodo restituisce, se esiste, un singolo ogetto "Mense" da un attributo di un altro
 * oggetto attraverso una sottoparte dell'oggetto "DataSnapshot".
 * @param dataSnapshot Questo parametro contiene il risultato o parte di esso da qui estrarre le
 *                     informazioni utili per creare un oggetto di tipo "Mense".
 * @param nomeAttributo Questo parametro contine il nome del attributo in cui cercare un siglo oggetto.
 * @return  Il riutato è il primo oggetto, valido se ce ne fossero più d'uno ritorna il primo trovato
 *          mentre se non ce ne sono ritorna null.
 */
    public static Mense getAttributeOf(DataSnapshot dataSnapshot, String nomeAttributo,@NonNull Long idUniversita){
        Mense mense = null;
        DataSnapshot menseDataSnapshot = dataSnapshot.child(nomeAttributo);
        if(menseDataSnapshot.exists()){
            Iterator<DataSnapshot> iterator =menseDataSnapshot.getChildren().iterator();
            while ( (iterator.hasNext()) && (mense==null) ){//Restituisce il primo oggetto valido
                mense = estraiDaDataSnapshot(iterator.next(),idUniversita);
            }
        }
        return mense;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER OBJECT                                                    */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, Mense>" così da poter
 * deserializzare un singolo oggetto dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_Object extends GenericDeserializer_Object<Mense> {
    private Long idUniversita =-1L;
/*================================================================================================*/
/*                         COSTRUTTORE                                                            */
/*================================================================================================*/
    public Deserializer_Object() {
        super();
    }
/*------------------------------------------------------------------------------------------------*/
    public Deserializer_Object(@NonNull Long idUniversita) {
        super();
        this.idUniversita= idUniversita;
    }
/*================================================================================================*/
        @Override
        public Mense estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Mense.estraiDaDataSnapshot(dataSnapshot,idUniversita);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         DESERIALIZER LIST                                                      */
/*================================================================================================*/
/**
 * Questa classe contiene un implemetazione del "Function<DataSnapshot, List<Mense>>" così da poter
 * deserializzare una lista di oggette dal risultato di una query partendo da un oggetto "DataSnapshot".
 */
    public static class Deserializer_List extends GenericDeserializer_List<Mense> {
    private Long idUniversita=-1L;
/*================================================================================================*/
/*                         COSTRUTTORE                                                            */
/*================================================================================================*/
    public Deserializer_List() {
        super();
    }
/*------------------------------------------------------------------------------------------------*/
    public Deserializer_List(@NonNull Long idUniversita) {
        super();
        this.idUniversita= idUniversita;
    }
/*================================================================================================*/
        @Override
        public Mense estraiDaDataSnapshot(DataSnapshot dataSnapshot) {
            return Deserializer_Mense.estraiDaDataSnapshot(dataSnapshot,this.idUniversita);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
