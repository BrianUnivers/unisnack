package it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

import it.tn.unitn.disi.ppsmet.Entity.Corsi;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

@Entity(tableName = "tableUni_ha_C",
        primaryKeys = {"id_universita",
                       "id_corso"},
        foreignKeys = {
            @ForeignKey(entity = Universita.class,
                parentColumns = "id",            //Colanna della classe a qui si rifierisce
                childColumns  = "id_universita", //ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE),
            @ForeignKey(entity = Corsi.class,
                    parentColumns = "id",       //Colanna della classe a qui si rifierisce
                    childColumns  = "id_corso", //ForeignKey
                    onUpdate = ForeignKey.CASCADE,
                    onDelete = ForeignKey.CASCADE)},
        indices = @Index("id_corso"))
public class Uni_ha_C {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @ColumnInfo(name = "id_universita")
    private Long id_universita;
    @NonNull
    @ColumnInfo(name = "id_corso")
    private Long id_corso;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    public Uni_ha_C(@NonNull Long id_universita,
                    @NonNull Long id_corso) {
        this.id_universita = id_universita;
        this.id_corso = id_corso;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    @NonNull
    public Long getId_universita() {
        return id_universita;
    }
//--------------------------------------
    public void setId_universita(@NonNull Long id_universita) {
        this.id_universita = id_universita;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_corso() {
        return id_corso;
    }
//--------------------------------------
    public void setId_corso(@NonNull Long id_corso) {
        this.id_corso = id_corso;
    }
//==================================================================================================
//##################################################################################################
}
