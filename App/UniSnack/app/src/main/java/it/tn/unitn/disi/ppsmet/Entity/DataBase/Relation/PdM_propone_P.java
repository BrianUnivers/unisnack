package it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

import it.tn.unitn.disi.ppsmet.Entity.PastoDelMese;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;


@Entity(tableName = "tablePdM_propone_P",
        primaryKeys = {"id_proponente", "id_mese", "id_anno",
                       "id_pietanza"},
        foreignKeys = {
            @ForeignKey(entity = PastoDelMese.class,
                parentColumns = {"id_utente",
                                 "mese",
                                 "anno"},   //Colanna della classe a qui si rifierisce
                childColumns  = {"id_proponente",
                                 "id_mese",
                                 "id_anno"},//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE),
            @ForeignKey(entity = Pietanze.class,
                parentColumns = "id",         //Colanna della classe a qui si rifierisce
                childColumns  = "id_pietanza",//ForeignKey
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE)},
        indices = {@Index(value = "id_pietanza")})
public class PdM_propone_P {
//##################################################################################################
//                                      DATI PRINCIPALI
//##################################################################################################
    @NonNull
    @ColumnInfo(name = "id_proponente")
    private Long    id_proponente;
    @NonNull
    @ColumnInfo(name = "id_mese")
    private Short id_mese;
    @NonNull
    @ColumnInfo(name = "id_anno")
    private Short id_anno;
    @NonNull
    @ColumnInfo(name = "id_pietanza")
    private Long    id_pietanza;
//##################################################################################################

//##################################################################################################
//                                      METODI
//##################################################################################################
//==================================================================================================
//                                      COSTRUTTORI
//==================================================================================================
    public PdM_propone_P(@NonNull Long id_proponente,
                         @NonNull Short id_mese,
                         @NonNull Short id_anno,
                         @NonNull Long id_pietanza) {
        this.id_proponente = id_proponente;
        this.id_mese = id_mese;
        this.id_anno = id_anno;
        this.id_pietanza = id_pietanza;
    }
//==================================================================================================
//==================================================================================================
//                                      GETTER E SETTER
//==================================================================================================
    @NonNull
    public Long getId_proponente() {
        return id_proponente;
    }
//--------------------------------------
    public void setId_proponente(@NonNull Long id_proponente) {
        this.id_proponente = id_proponente;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Short getId_mese() {
        return id_mese;
    }
//--------------------------------------
    public void setId_mese(@NonNull Short id_mese) {
        this.id_mese = id_mese;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Short getId_anno() {
        return id_anno;
    }
//--------------------------------------
    public void setId_anno(@NonNull Short id_anno) {
        this.id_anno = id_anno;
    }
//--------------------------------------------------------------------------------------------------
    @NonNull
    public Long getId_pietanza() {
        return id_pietanza;
    }
//--------------------------------------
    public void setId_pietanza(@NonNull Long id_pietanza) {
        this.id_pietanza = id_pietanza;
    }
//==================================================================================================
//##################################################################################################
}
