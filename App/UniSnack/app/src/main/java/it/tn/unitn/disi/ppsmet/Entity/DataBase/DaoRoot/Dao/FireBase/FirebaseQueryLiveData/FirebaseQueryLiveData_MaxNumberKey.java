package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData;

import android.util.Log;

import androidx.lifecycle.LiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Iterator;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.CostantiPerJSON;

public class FirebaseQueryLiveData_MaxNumberKey extends LiveData<Long> implements CostantiPerJSON {
/**
 * Questa classe ci permette di gestire e mantenere sempre aggiornati i risultati di una query al
 * firebase runtime database ed disativare tali aggirnamenti nel momento stesso che tali dati non sono
 * utilizzaati all'interno della attivtà.
 */
/*################################################################################################*/
    private static final String LOG_TAG = "FirebaseQueryLiveData_MaxNumberKey";

    private final Query query;
    private final MyValueEventListener listener = new MyValueEventListener();
/*================================================================================================*/
/*                         CONSTRUCTOR OF QUERY AT FIREBASE RUNTIME DATABASE                      */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET LAST ID OF ORDINAZIONI                                             */
/*------------------------------------------------------------------------------------------------*/
    public FirebaseQueryLiveData_MaxNumberKey(Query query) {
        this.query = query;
        query.addListenerForSingleValueEvent(listener);
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         GET LAST ID OF ORDINAZIONI                                             */
/*------------------------------------------------------------------------------------------------*/
    public FirebaseQueryLiveData_MaxNumberKey(DatabaseReference ref) {
        this.query = ref;
        query.addListenerForSingleValueEvent(listener);
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*================================================================================================*/
/*                         ACTIVATION AND INATIVATION UPDATE OF RESULTS OF QUERY                  */
/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/*                         ACTIVATION UPDATE OF RESULTS OF QUERY (Activity is show on screen)     */
/*                         It is call when activity is onSTARTED or onRESUMED                     */
/*------------------------------------------------------------------------------------------------*/
    @Override
    public void onActive() {
        Log.d(LOG_TAG, "onActive");
        //query.addValueEventListener(listener);
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                         INATIVATION UPDATE OF RESULTS OF QUERY (Activity is inactive or destroied)*/
/*                         It is call when activity is onDESTROY or onPAUSE                       */
/*------------------------------------------------------------------------------------------------*/
    @Override
    protected void onInactive() {
        Log.d(LOG_TAG, "onInactive");
        query.removeEventListener(listener);
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
/*################################################################################################*/
    private class MyValueEventListener implements ValueEventListener {
/*================================================================================================*/
/*                         ACTION FOR UPDATE THE RESULTS OF QUERY                                 */
/*================================================================================================*/
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            setValue(getMaxNumId(dataSnapshot.getChildren()));//Insermento nella lista LiveData dei risultati
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         ERROR IT CAN'T CONNECTION TO FIREBASE RUNTIME DATABASE                 */
/*================================================================================================*/
        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e(LOG_TAG, "Can't listen to query " + query, databaseError.toException());
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         MAX VALUE OF ONLY NUMBER                                               */
/*================================================================================================*/
    private static Long  getMaxNumId(Iterable<DataSnapshot> iterable){
        Long ris=0L;
        Iterator<DataSnapshot> iterator = iterable.iterator();
        while (iterator.hasNext()){
            DataSnapshot element= iterator.next();
            Long key =null;
            try {
                if(element.child(P_KEY).exists()){
                    key = element.child(P_KEY).getValue(Long.class);
                }else{
                    key =Long.parseLong(element.getKey());
                }

                if( (key!=null) && (ris < key)){
                    ris = key;
                }
            }catch (NumberFormatException error){
                Log.d("FireBase","La chiave letta non conteneva un numero di tipo Long.",error);
            }
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
}