package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.Date;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Allergeni_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Tessere_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.IscrizioniUtente_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.MyRoomDatabase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.UtentiPartecipaUniversita_ThreadAggiornamento;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.IscrizioniUtente;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class IscrizioniUtente_Repository  extends UtentiPartecipaUniversita_ThreadAggiornamento {
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA U_PARTECIPA_UNI (e le sue derivazioni   */
/*                         seguendo un ordine topologico per garantire gli aggiornamenti)         */
/*                         ATTRAVERO IL DATABASE LOCALE (room)                                    */
/*################################################################################################*/
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public IscrizioniUtente_Repository(Application application) {
        super(application);
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
   /* public LiveData<List<IscrizioniUtente>> getByIdOfUtente(Utenti utenti){
        return iscrizioniUtenteDao.getByIdOfUtente_Synchronized(utenti.getId());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE  INTO SPECIFIC DAY                             */
/*================================================================================================*/
   /* public LiveData<List<IscrizioniUtente>> getByIdOfUtente(Utenti utenti, Date date){
        return iscrizioniUtenteDao.getByIdOfUtente_Synchronized(utenti.getId(),date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE TODAY                                          */
/*================================================================================================*/
  /*  public LiveData<List<IscrizioniUtente>> getByIdOfUtenteAtToday(Utenti utenti){
        return iscrizioniUtenteDao.getByIdOfUtenteAtToday_Synchronized(utenti.getId());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UNIVERSITA'                                           */
/*================================================================================================*/
   /* public LiveData<List<IscrizioniUtente>> getByIdOfUniversita(Universita universita){
        return iscrizioniUtenteDao.getByIdOfUniversita_Synchronized(universita.getId());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UNIVERSITA' INTO SPECIFIC DAY                         */
/*================================================================================================*/
   /* public LiveData<List<IscrizioniUtente>> getByIdOfUniversita(Universita universita, Date date){
        return iscrizioniUtenteDao.getByIdOfUniversita_Synchronized(universita.getId(),date);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UNIVERSITA' TODAY                                     */
/*================================================================================================*/
   /* public LiveData<List<IscrizioniUtente>> getByIdOfUniversitaAtToday(Universita universita){
        return iscrizioniUtenteDao.getByIdOfUniversitaAtToday_Synchronized(universita.getId());
    }
/*================================================================================================*/
/*################################################################################################*/
}
