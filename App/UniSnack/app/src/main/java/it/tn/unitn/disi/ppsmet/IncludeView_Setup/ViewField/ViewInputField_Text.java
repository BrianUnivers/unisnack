package it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewField;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import it.tn.unitn.disi.ppsmet.R;

public class ViewInputField_Text {
/*################################################################################################*/
    private   TextView label = null;
    protected EditText input = null;
/*------------------------------------------------------------------------*/
    protected boolean  campoObbligatorio = false;
/*================================================================================================*/
/*                                  COSTRUTTORE E SETUP VIEW FIELD                                */
/*================================================================================================*/
    public ViewInputField_Text(ViewGroup rootLayout, int idLayoutIncluded, int idLable, int idTestValue) {
        this.campoObbligatorio =false;
        impostaLayoutComune(rootLayout,idLayoutIncluded,idLable,idTestValue);
    }
/*------------------------------------------------------------------------*/
    public ViewInputField_Text(ViewGroup rootLayout, int idLayoutIncluded, int idLable, int idTestValue,boolean obbligatorio) {
        this.campoObbligatorio =obbligatorio;
        impostaLayoutComune(rootLayout,idLayoutIncluded,idLable,idTestValue);
    }
/*------------------------------------------------------------------------------------------------*/
    private void impostaLayoutComune(ViewGroup rootLayout, int idLayoutIncluded, int idLable, int idTestValue) {
        if(rootLayout!=null){
            ViewGroup gruppoField = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(gruppoField!=null){
                //LABLE
                label = (TextView) gruppoField.findViewById(R.id.txt_LabelField);
                label.setText(idLable);
                if(campoObbligatorio){
                    label.setText(label.getText()+" *");
                }
                //INPUT
                input = (EditText) gruppoField.findViewById(R.id.txt_Input_Field);
                input.setHint(idTestValue);
                input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if(campoObbligatorio && ( (input!=null) && (input.getText().toString().trim().length() == 0)) ){
                            impostaErroreCampoObbligatorio();
                        }
                        azzioneDuranteInserimetoDati();
                    }
                });
                input.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                    @Override
                    public void afterTextChanged(Editable editable) {
                        annullaErrori();
                    }
                });
            }
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  MOSTRA ERRORE                                                 */
/*================================================================================================*/
    public void impostaErrore(String testo){
        input.setTextColor(input.getResources().getColor(R.color.colorError,null));
        input.setBackgroundTintList(ColorStateList.valueOf(input.getResources().getColor(R.color.colorError,null)));
        input.setError(testo);
    }
/*------------------------------------------------------------------------*/
    public void impostaErrore(int idTesto){
        String   testo = input.getResources().getString(idTesto);
        impostaErrore(testo);
    }
/*------------------------------------------------------------------------*/
    public void impostaErroreCampoObbligatorio(){
        impostaErrore(R.string.err_CampoObbligatorio);
    }
/*------------------------------------------------------------------------------------------------*/
    public void annullaErrori(){
        input.setTextColor(input.getResources().getColor(R.color.colorNormalText,null));
        input.setBackgroundTintList(ColorStateList.valueOf(input.getResources().getColor(R.color.colorPrimary,null)));
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  AZZIONI DURANTE L'INSERIMETO DEI DATI                         */
/*================================================================================================*/
    public String getTesto(){
        String ris = null;
        if(input!=null){
            ris =input.getText().toString();
        }
        return ris;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  AZZIONI DURANTE L'INSERIMETO DEI DATI                         */
/*================================================================================================*/
    protected void azzioneDuranteInserimetoDati(){
        //Per le classi succesive.
    }
/*================================================================================================*/
/*################################################################################################*/
}
