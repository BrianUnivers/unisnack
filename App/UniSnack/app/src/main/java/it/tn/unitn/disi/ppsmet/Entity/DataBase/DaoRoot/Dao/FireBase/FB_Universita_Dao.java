package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Corsi;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_IscrizioniUtente;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Universita;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.ResultCustom.IscrizioniUtente;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;


public class FB_Universita_Dao extends ConnesioneAFirebase<Universita> {
/*################################################################################################*/
/*================================================================================================*/
/*                         INSERISCI DATI UTENTI                                                  */
/*================================================================================================*/
    public static HashMap<String,Object> inserisciDatiUniversita(HashMap<String,Object> istruzioni, String root, Universita universita){
        //Singoli Campi
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_KEY).toString(),
                universita.getId());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_NOME).toString(),
                universita.getNome());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_SIGLA).toString(),
                universita.getSigla());
        return istruzioni;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<Universita>> getAll_OneTime(){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Universita.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<Universita>> getAll(){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Universita.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<Universita>> getAll_Synchronized(){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Universita.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    public LiveData<Universita> getByPrimaryKey_OneTime(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA)
                .child(id.toString());
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Universita.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Universita> getByPrimaryKey(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA)
                .child(id.toString());
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Universita.Deserializer_Object());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Universita> getByPrimaryKey_Synchronized(@NonNull Long id){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA)
                .child(id.toString());
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Universita.Deserializer_Object());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE                                                */
/*================================================================================================*/
   public  LiveData<List<Universita>> getByIdOfUtente_OneTime(@NonNull Utenti utenti){
        //Universita a qui l'utente è iscritto
        MediatorLiveData<List<Universita>> risPirmaParziale = new MediatorLiveData<>();
        risPirmaParziale.addSource( getByPrimaryKey_OneTime(utenti.getId_Universita()),
            new Observer<Universita>() {
            @Override
            public void onChanged(Universita universita) {
                List<Universita> list = new LinkedList<>();
                list.add(universita);
                risPirmaParziale.setValue(list);
            }
        });
        //Universita a qui l'utente partecipa
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PARTECIPA_UNIVERSITA);
        FirebaseQueryLiveData_OneTime secondoParziale = new FirebaseQueryLiveData_OneTime(ref);
        LiveData<List<Universita>> risSecondoParziale =
                Transformations.map(secondoParziale, new Deserializer_Universita.Deserializer_List());

        return this.mergeLiveData(risPirmaParziale,risSecondoParziale);
   }
/*------------------------------------------------------------------------*/
    public  LiveData<List<Universita>> getByIdOfUtente(@NonNull Utenti utenti){
        //Universita a qui l'utente è iscritto
        MediatorLiveData<List<Universita>> risPirmaParziale = new MediatorLiveData<>();
        risPirmaParziale.addSource( getByPrimaryKey(utenti.getId_Universita()),
                new Observer<Universita>() {
                    @Override
                    public void onChanged(Universita universita) {
                        List<Universita> list = new LinkedList<>();
                        list.add(universita);
                        risPirmaParziale.setValue(list);
                    }
                });
        //Universita a qui l'utente partecipa
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PARTECIPA_UNIVERSITA);
        FirebaseQueryLiveData secondoParziale = new FirebaseQueryLiveData(ref);
        LiveData<List<Universita>> risSecondoParziale =
                Transformations.map(secondoParziale, new Deserializer_Universita.Deserializer_List());

        return this.mergeLiveData(risPirmaParziale,risSecondoParziale);
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<Universita>> getByIdOfUtente_Synchronized(@NonNull Utenti utenti){
        //Universita a qui l'utente è iscritto
        MediatorLiveData<List<Universita>> risPirmaParziale = new MediatorLiveData<>();
        risPirmaParziale.addSource( getByPrimaryKey_Synchronized(utenti.getId_Universita()),
                new Observer<Universita>() {
                    @Override
                    public void onChanged(Universita universita) {
                        List<Universita> list = new LinkedList<>();
                        list.add(universita);
                        risPirmaParziale.setValue(list);
                    }
                });
        //Universita a qui l'utente partecipa
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PARTECIPA_UNIVERSITA);
        FirebaseQueryLiveData_Synchronized secondoParziale = new FirebaseQueryLiveData_Synchronized(ref);
        LiveData<List<Universita>> risSecondoParziale =
                Transformations.map(secondoParziale, new Deserializer_Universita.Deserializer_List());

        return this.mergeLiveData(risPirmaParziale,risSecondoParziale);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE INTO SPECIFIC DAY                              */
/*================================================================================================*/
    public  LiveData<List<Universita>> getByIdOfUtente_OneTime(@NonNull Utenti utenti,Date date){
        //Universita a qui l'utente è iscritto
        MediatorLiveData<List<Universita>> risPirmaParziale = new MediatorLiveData<>();
        //_OneTime
        risPirmaParziale.addSource(getByPrimaryKey_OneTime(utenti.getId_Universita()),
                new Observer<Universita>() {
                    @Override
                    public void onChanged(Universita universita) {
                        DatabaseReference refIscrizione = getReferenceFireBase().getReference()
                                .child(L_UTENTI_DEL_CORSO_DEL_UNIVERSITA)
                                .child(utenti.getId_Universita().toString())
                                .child(L_CORSI)
                                .child(utenti.getId_Corso().toString())
                                .child(L_ISCRITTI)
                                .child(builderKeyFromUtenti(utenti));
                        //_OneTime
                        FirebaseQueryLiveData_OneTime iscrizione  = new FirebaseQueryLiveData_OneTime(refIscrizione);
                        LiveData<IscrizioniUtente> risIscrizione =
                                Transformations.map(iscrizione, new Deserializer_IscrizioniUtente.Deserializer_Object(universita,null,date));

                        risPirmaParziale.addSource(risIscrizione,
                            new Observer<IscrizioniUtente>() {
                            @Override
                            public void onChanged(IscrizioniUtente iscrizioniUtente) {
                                //Solo l'iscrizione (NON LA PARTECIPAZIONE)
                                if(iscrizioniUtente.annoAccademico.dataNelAnnoAccademico(date)){
                                    List<Universita> list = new LinkedList<>();
                                    list.add(universita);
                                    risPirmaParziale.setValue(list);
                                }
                            }
                        });
                    }
                });
        //Universita a qui l'utente partecipa
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PARTECIPA_UNIVERSITA);
        //_OneTime
        FirebaseQueryLiveData_OneTime secondoParziale = new FirebaseQueryLiveData_OneTime(ref);
        LiveData<List<Universita>> risSecondoParziale =
                Transformations.map(secondoParziale, new Deserializer_Universita.Deserializer_List(date));

        return this.mergeLiveData(risPirmaParziale,risSecondoParziale);
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<Universita>> getByIdOfUtente(@NonNull Utenti utenti,Date date){
        //Universita a qui l'utente è iscritto
        MediatorLiveData<List<Universita>> risPirmaParziale = new MediatorLiveData<>();
        //_
        risPirmaParziale.addSource(getByPrimaryKey(utenti.getId_Universita()),
                new Observer<Universita>() {
                    @Override
                    public void onChanged(Universita universita) {
                        DatabaseReference refIscrizione = getReferenceFireBase().getReference()
                                .child(L_UTENTI_DEL_CORSO_DEL_UNIVERSITA)
                                .child(utenti.getId_Universita().toString())
                                .child(L_CORSI)
                                .child(utenti.getId_Corso().toString())
                                .child(L_ISCRITTI)
                                .child(builderKeyFromUtenti(utenti));
                        //_
                        FirebaseQueryLiveData iscrizione  = new FirebaseQueryLiveData(refIscrizione);
                        LiveData<IscrizioniUtente> risIscrizione =
                                Transformations.map(iscrizione, new Deserializer_IscrizioniUtente.Deserializer_Object(universita,null,date));

                        risPirmaParziale.addSource(risIscrizione,
                                new Observer<IscrizioniUtente>() {
                                    @Override
                                    public void onChanged(IscrizioniUtente iscrizioniUtente) {
                                        //Solo l'iscrizione (NON LA PARTECIPAZIONE)
                                        if(iscrizioniUtente.annoAccademico.dataNelAnnoAccademico(date)){
                                            List<Universita> list = new LinkedList<>();
                                            list.add(universita);
                                            risPirmaParziale.setValue(list);
                                        }
                                    }
                                });
                    }
                });
        //Universita a qui l'utente partecipa
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PARTECIPA_UNIVERSITA);
        //_
        FirebaseQueryLiveData secondoParziale = new FirebaseQueryLiveData(ref);
        LiveData<List<Universita>> risSecondoParziale =
                Transformations.map(secondoParziale, new Deserializer_Universita.Deserializer_List(date));

        return this.mergeLiveData(risPirmaParziale,risSecondoParziale);
    }
/*------------------------------------------------------------------------*/
    public  LiveData<List<Universita>> getByIdOfUtente_Synchronized(@NonNull Utenti utenti,Date date){
        //Universita a qui l'utente è iscritto
        MediatorLiveData<List<Universita>> risPirmaParziale = new MediatorLiveData<>();
        //_Synchronized
        risPirmaParziale.addSource(getByPrimaryKey_Synchronized(utenti.getId_Universita()),
                new Observer<Universita>() {
                    @Override
                    public void onChanged(Universita universita) {
                        DatabaseReference refIscrizione = getReferenceFireBase().getReference()
                                .child(L_UTENTI_DEL_CORSO_DEL_UNIVERSITA)
                                .child(utenti.getId_Universita().toString())
                                .child(L_CORSI)
                                .child(utenti.getId_Corso().toString())
                                .child(L_ISCRITTI)
                                .child(builderKeyFromUtenti(utenti));
                        //_Synchronized
                        FirebaseQueryLiveData_Synchronized iscrizione  = new FirebaseQueryLiveData_Synchronized(refIscrizione);
                        LiveData<IscrizioniUtente> risIscrizione =
                                Transformations.map(iscrizione, new Deserializer_IscrizioniUtente.Deserializer_Object(universita,null,date));

                        risPirmaParziale.addSource(risIscrizione,
                            new Observer<IscrizioniUtente>() {
                                @Override
                                public void onChanged(IscrizioniUtente iscrizioniUtente) {
                                    //Solo l'iscrizione (NON LA PARTECIPAZIONE)
                                    if(iscrizioniUtente.annoAccademico.dataNelAnnoAccademico(date)){
                                        List<Universita> list = new LinkedList<>();
                                        list.add(universita);
                                        risPirmaParziale.setValue(list);
                                    }
                                }
                            });
                    }
                });
        //Universita a qui l'utente partecipa
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UNIVERSITA_E_TESSERE_DEL_UTENTE)
                .child(builderKeyFromUtenti(utenti))
                .child(L_PARTECIPA_UNIVERSITA);
        //_Synchronized
        FirebaseQueryLiveData_Synchronized secondoParziale = new FirebaseQueryLiveData_Synchronized(ref);
        LiveData<List<Universita>> risSecondoParziale =
                Transformations.map(secondoParziale, new Deserializer_Universita.Deserializer_List(date));

        return this.mergeLiveData(risPirmaParziale,risSecondoParziale);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTE TODAY                                          */
/*================================================================================================*/
    public LiveData<List<Universita>> getByIdOfUtenteAtToday_OneTime(@NonNull Utenti utenti){
        return this.getByIdOfUtente_OneTime(utenti,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Universita>> getByIdOfUtenteAtToday(@NonNull Utenti utenti){
        return this.getByIdOfUtente(utenti,new Date());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Universita>> getByIdOfUtenteAtToday_Synchronized(@NonNull Utenti utenti){
        return this.getByIdOfUtente_Synchronized(utenti,new Date());
    }
/*================================================================================================*/
/*################################################################################################*/
}
