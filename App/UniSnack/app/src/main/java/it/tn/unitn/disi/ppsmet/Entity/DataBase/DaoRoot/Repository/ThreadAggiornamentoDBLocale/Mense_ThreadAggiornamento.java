package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Mense_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Mense_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Universita_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.Mense;
import it.tn.unitn.disi.ppsmet.Entity.Universita;

public class Mense_ThreadAggiornamento extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA ANNO ACCADEMICO LOCALE           */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "Mense" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_Mense";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista dei Mense del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private List<Mense> listaMense;
        private Long        idUniversita;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * Mense.
         * @param listaMense Questo parametro contiene la lista di tutti gli elementi
         *                        tra qui quelli nuovi e quelli già presenti ma da modificare.
         */
        public ThreadAggiornamento(List<Mense> listaMense,Long idUniversita) {
            super();
            this.listaMense =listaMense;
            this.idUniversita=idUniversita;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                                  */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella Mense.");
            if(idUniversita!=null){
                aggionaTabellaMense(listaMense,idUniversita);
                Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella Mense.");
            }else{
                Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella Mense. ERRORE UNIVERSITA");
            }
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param listaMense
         * @param idUniversita
         */
        private synchronized void aggionaTabellaMense(List<Mense> listaMense,Long idUniversita){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella Mense.");
            //Controllo se posso già inserire i dati
            Universita universita = universitaDao.getByPrimaryKey(idUniversita);
            if(universita!=null){
                //Dati presenti nel database locale.
                List<Mense> listaMenseLocale = menseDao.getByIdOfUniverita(idUniversita);
                if((listaMenseLocale==null)||(listaMenseLocale.isEmpty())){
                    inserisciListaElementi(listaMense);
                }else if((listaMense==null)||(listaMense.isEmpty())) {
                    eliminaListaElementi(listaMenseLocale);
                }else{
                    HashMap<Long,Mense> hashMapMenseLocali = convertiListaInHashMap(listaMenseLocale);

                    List<Mense> listaMenseNuove = new LinkedList<>();
                    List<Mense> listaMenseAggiornate = new LinkedList<>();
                    //Serve solo per poter conoscere quali elementi caccelare tra quelli già presenti localmente.
                    List<Mense> listaVechieMenseDaMantenere = new LinkedList<>();

                    Iterator<Mense> iterator = listaMense.iterator();
                    while (iterator.hasNext()){
                        Mense mense =iterator.next();
                        if(mense!=null){
                            Mense menseVecchia = hashMapMenseLocali.get(mense.getId());
                            if(menseVecchia==null){
                                listaMenseNuove.add(mense);
                            }else if(!menseVecchia.equals(mense)){
                                listaMenseAggiornate.add(mense);
                                listaVechieMenseDaMantenere.add(menseVecchia);
                            }else{
                                listaVechieMenseDaMantenere.add(menseVecchia);
                            }
                        }
                    }
                    //MANTENGO SOLO GLI ELEMNETI CHE DEVO CANCELLARE
                    listaMenseLocale.removeAll(listaVechieMenseDaMantenere);
                    inserisciListaElementi(listaMenseNuove);
                    aggiornaListaElementi(listaMenseAggiornate);
                    eliminaListaElementi(listaMenseLocale);
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         GENERAZIONE HASH MAP FROM LIST                                         */
/*================================================================================================*/
        private synchronized HashMap<Long,Mense> convertiListaInHashMap(List<Mense> lista){
            HashMap<Long,Mense> ris=new HashMap<>();
            if(lista!=null){
                Iterator<Mense> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Mense elemento = iterator.next();
                    if((elemento!=null) && (elemento.getId()!=null)){
                        ris.put(elemento.getId(),elemento);
                    }
                }
            }
            return ris;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO INSERIMENTO NUOVI ELEMENTI                                      */
/*================================================================================================*/
        private synchronized void inserisciListaElementi(List<Mense> lista){
            if(lista!=null){
                Iterator<Mense> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Mense elemento=iterator.next();
                    if(elemento!=null){
                        menseDao.insert(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO AGGIORNAMENTO ELEMENTI PRESENTI                                 */
/*================================================================================================*/
        private synchronized void aggiornaListaElementi(List<Mense> lista){
            if(lista!=null){
                Iterator<Mense> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Mense elemento=iterator.next();
                    if(elemento!=null){
                        menseDao.update(elemento);
                    }
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaListaElementi(List<Mense> lista){
            if(lista!=null){
                Iterator<Mense> iterator = lista.iterator();
                while (iterator.hasNext()){
                    Mense elemento=iterator.next();
                    if(elemento!=null){
                        menseDao.delete(elemento.getId());
                    }
                }
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_Mense_Dao fbMenseDao = new FB_Mense_Dao();
    //ROOM DATABASE
    public static Mense_Dao menseDao = null;
/*------------------------------------------------------------------------------------------------*/
    //DIPENDENZE
    protected static Universita_ThreadAggiornamento dipendezaUniversita = null;
    //ROOM DATABASE
    protected static Universita_Dao universitaDao = null;
    //QUERY ROOM DATABASE
    protected static  LiveData<List<Universita>> allUniversita = new MediatorLiveData<>();
    protected static  LiveData<List<Mense>>      allMense      = new MediatorLiveData<>();
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaMense(){
        if(universitaDao==null){
            //universitaDao è un attributo statico anche se è presente solo se è stoto creato un oggetto
            universitaDao= dipendezaUniversita.universitaDao;
            allUniversita = universitaDao.getAll_Synchronized();
        }
        if(menseDao==null){
            menseDao = db.MenseDao();
        }
        if((!allUniversita.hasObservers())){//SE NON HA già osservatori per rilevare aggiornamenti
            allUniversita.observeForever(new Observer<List<Universita>>() {
                private  List<Universita>  lastListUniversita = new LinkedList<>();
                private  List<Universita>  temp = new LinkedList<>();
                @Override
                public void onChanged(List<Universita> listaUniversita) {
                    temp.clear();
                    temp.addAll(listaUniversita);
                    if(!lastListUniversita.isEmpty()){
                        listaUniversita.removeAll(lastListUniversita);
                    }
                    //Solo quelli nuovi
                    Iterator<Universita> iterator = listaUniversita.iterator();
                    while (iterator.hasNext()){
                        Universita universita =iterator.next();
                        if((universita!=null) && (universita.getId()!=null)){
                            //Aggiungi un altro ossservatore
                            fbMenseDao.getByIdOfUniverita_Synchronized(universita.getId()).observeForever(new MyMensaObserver(universita.getId()));
                        }
                    }
                    lastListUniversita.clear();
                    lastListUniversita.addAll(temp);
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
    private static class MyMensaObserver implements Observer<List<Mense>>{
        private Long idUniversita;
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        public MyMensaObserver(Long idUniversita) {
            this.idUniversita =idUniversita;
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO CHIAMATO NEL MOMENTO DEL CAMBIAMNETO                            */
/*================================================================================================*/
        @Override
        public void onChanged(List<Mense> lista) {
            if((lista!=null)){
                executor.execute(new ThreadAggiornamento(lista,this.idUniversita));
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Mense_ThreadAggiornamento(Application application) {
        super(application);
        if(dipendezaUniversita==null){
            dipendezaUniversita = new Universita_ThreadAggiornamento(application);
        }
        gestioneAggiornametoTabellaMense();
    }
/*================================================================================================*/
/*################################################################################################*/
}
