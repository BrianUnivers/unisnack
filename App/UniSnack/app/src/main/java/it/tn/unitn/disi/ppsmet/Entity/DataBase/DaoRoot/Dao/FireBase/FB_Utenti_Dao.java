package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.Authentication.HashPasswordWithSalt;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.Common.ConnesioneAFirebase;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_OneTime;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.FirebaseQueryLiveData_Synchronized;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Universita;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FirebaseQueryLiveData.Transformations.Deserializer_Utenti;
import it.tn.unitn.disi.ppsmet.Entity.Universita;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class FB_Utenti_Dao  extends ConnesioneAFirebase<Utenti> {
/*################################################################################################*/
    private static String     DATA_FORMAT  = "dd/MM/yyyy";
    private static SimpleDateFormat CONVERT_DATA = new SimpleDateFormat(DATA_FORMAT);
/*================================================================================================*/
/*                         INSERISCI DATI UTENTI                                                  */
/*================================================================================================*/
    public static HashMap<String,Object> inserisciDatiUtenti(HashMap<String,Object> istruzioni, String root,Utenti utenti){
        //Singoli Campi
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_KEY).toString(),
                utenti.getId());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_NOME).toString(),
                utenti.getNome());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_COGNOME).toString(),
                utenti.getCognome());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_NATO).toString(),
                CONVERT_DATA.format(utenti.getNato()) );
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_EMAIL).toString(),
                utenti.getEmail());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_HASHED_PSW).toString(),
                utenti.getPassword());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_SALT).toString(),
                utenti.getSalt());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_EMAIL__HASHED_PSW).toString(),
                builderAuthenticatorKey(utenti.getEmail(),utenti.getPassword()));
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_URL_IMMAGINE).toString(),
                utenti.getUrlImmagine());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_MATRICOLA).toString(),
                utenti.getMatricola().toString());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_STATO_TESSERA).toString(),
                utenti.getStatoTessera());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_URL_QRCODE).toString(),
                utenti.getUrlQRCode());
        istruzioni.put(new StringBuffer(root).append(S_SEPARARORE).append(P_URL_CODICE_A_BARRE).toString(),
                utenti.getUrlCodiceABarre());


        FB_AnnoAccademico_Dao.inserisciDatiAnnoAccademico(istruzioni,
                                    new StringBuffer(root).append(S_SEPARARORE).append(L_ANNO_ACCADEMICO).toString(),
                                    utenti.getAnnoAccademico());
        return istruzioni;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         CREAZIONE HASHMAP PER L'INSERMIENTO                                    */
/*================================================================================================*/
    private static HashMap<String,Object> creazioniIstruzioniPerInserimento(Utenti utenti){
        DateFormat CONVERT_DATA = new SimpleDateFormat(DATA_FORMAT);
        HashMap<String,Object> istruzioni = new HashMap<>();
        List<String> listRoot = new ArrayList<>();
        //Primo posto
        StringBuffer rootSpecifico = new StringBuffer();
        rootSpecifico.append(L_UTENTI_DEL_CORSO_DEL_UNIVERSITA)
                .append(S_SEPARARORE).append(utenti.getId_Universita())
                .append(S_SEPARARORE).append(L_CORSI)
                .append(S_SEPARARORE).append(utenti.getId_Corso())
                .append(S_SEPARARORE).append(L_ISCRITTI)
                .append(S_SEPARARORE).append(builderKeyFromUtenti(utenti))
                .append(S_SEPARARORE);
        listRoot.add(rootSpecifico.toString());
        //Secondo posto
        StringBuffer rootAll = new StringBuffer();
        rootAll.append(L_UTENTI)
                .append(S_SEPARARORE).append(builderKeyFromUtenti(utenti));
        listRoot.add(rootAll.toString());
        //ParteComune
        Iterator<String> iterator = listRoot.iterator();
        while (iterator.hasNext()){
            String root = iterator.next();
            inserisciDatiUtenti(istruzioni,root,utenti);
        }
        return istruzioni;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    public LiveData<Long> insert(@NotNull Utenti utenti){
        MediatorLiveData<Long> idRisultato =new MediatorLiveData<>();
        idRisultato.addSource(ConnesioneAFirebase.getMaxIdUtenti(), new Observer<Long>() {
            @Override
            public void onChanged(Long id) {
                if(id!=null){
                //Inserimento
                    utenti.setId(id+1);
                    /*-------------------------------*/
                    /*        SOLO PER SIMILAZIONE   */
                    /*-------------------------------*/
                    utenti.setUrlImmagine(    "Foto/Utenti/"+utenti.getId()+".jpg");
                    /*-------------------------------*/
                    /*-------------------------------*/
                    getReferenceFireBase()
                            .getReference()
                            .updateChildren(creazioniIstruzioniPerInserimento(utenti));
                }
                idRisultato.postValue(utenti.getId());
            }
        });
        return idRisultato;
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    public LiveData<List<Utenti>> getAll_OneTime(){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UTENTI);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Utenti.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Utenti>> getAll(){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UTENTI);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Utenti.Deserializer_List());
    }
/*------------------------------------------------------------------------*/
    public LiveData<List<Utenti>> getAll_Synchronized(){
        DatabaseReference ref = super.getReferenceFireBase().getReference()
                .child(L_UTENTI);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Utenti.Deserializer_List());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    public LiveData<Utenti> getByPrimaryKey_OneTime(@NonNull Long id){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_UTENTI)
                .orderByChild(P_KEY)
                .equalTo(id);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Utenti.Deserializer_ObjectFilter());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Utenti> getByPrimaryKey(@NonNull Long id){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_UTENTI)
                .orderByChild(P_KEY)
                .equalTo(id);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Utenti.Deserializer_ObjectFilter());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Utenti> getByPrimaryKey_Synchronized(@NonNull Long id){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_UTENTI)
                .orderByChild(P_KEY)
                .equalTo(id);
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Utenti.Deserializer_ObjectFilter());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by AUTENTICATION                                            */
/*================================================================================================*/
    public LiveData<Utenti> getByAuthentication_OneTime(@NonNull String email,@NonNull String password){
        MediatorLiveData<Utenti> utente= new MediatorLiveData<>();
        utente.addSource(this.generaPassword_OneTime(email, password), new Observer<String>() {
            @Override
            public void onChanged(String passwordHashed) {
                if(passwordHashed!=null){
                    //_OneTime
                    utente.addSource(getByAuthenticator_OneTime(email, passwordHashed), new Observer<Utenti>() {
                        @Override
                        public void onChanged(Utenti utenti) {
                            utente.setValue(utenti);
                        }
                    });
                }else{
                    utente.setValue(null);
                }
            }
        });
        return utente;
    }
/*------------------------------------------------------------------------*/
    public LiveData<Utenti> getByAuthentication(@NonNull String email,@NonNull String password){
        MediatorLiveData<Utenti> utente= new MediatorLiveData<>();
        utente.addSource(this.generaPassword_OneTime(email, password), new Observer<String>() {
            @Override
            public void onChanged(String passwordHashed) {
                if(passwordHashed!=null){
                    //_
                    utente.addSource(getByAuthenticator(email, passwordHashed), new Observer<Utenti>() {
                        @Override
                        public void onChanged(Utenti utenti) {
                            utente.setValue(utenti);
                        }
                    });
                }else{
                    utente.setValue(null);
                }
            }
        });
        return utente;
    }
/*------------------------------------------------------------------------*/
    public LiveData<Utenti> getByAuthentication_Synchronized(@NonNull String email,@NonNull String password){
        MediatorLiveData<Utenti> utente= new MediatorLiveData<>();
        utente.addSource(this.generaPassword_OneTime(email, password), new Observer<String>() {
            @Override
            public void onChanged(String passwordHashed) {
                if(passwordHashed!=null){
                    //_Synchronized
                    utente.addSource(getByAuthenticator_Synchronized(email, passwordHashed), new Observer<Utenti>() {
                        @Override
                        public void onChanged(Utenti utenti) {
                            utente.setValue(utenti);
                        }
                    });
                }else{
                    utente.setValue(null);
                }
            }
        });
        return utente;
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo richede al database il salt e calcola la password a qui viene aggiunto il salt
     * per poi applicare la funzione di hash.
     * @param email
     * @param password
     * @return Il risultato è la hashed password oppure null.
     */
    private LiveData<String> generaPassword_OneTime(@NonNull String email,@NonNull String password){
        MediatorLiveData<String> passwordHashed= new MediatorLiveData<>();
        passwordHashed.addSource(this.getSaltByEmail_OneTime(email), new Observer<String>() {
            @Override
            public void onChanged(String salt) {
                if(salt!=null){
                    passwordHashed.postValue(HashPasswordWithSalt.genraPasswordHashWithSalt(password,salt));
                }else{
                    passwordHashed.postValue("");
                }
            }
        });
        return passwordHashed;
    }
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
    public LiveData<Utenti> getByAuthenticator_OneTime(@NonNull String email,@NonNull String passwordHashed){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_UTENTI)
                .orderByChild(P_EMAIL__HASHED_PSW)
                .equalTo(builderAuthenticatorKey(email,passwordHashed))
                .limitToFirst(1);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Utenti.Deserializer_ObjectFilter());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Utenti> getByAuthenticator(@NonNull String email,@NonNull String passwordHashed){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_UTENTI)
                .orderByChild(P_EMAIL__HASHED_PSW)
                .equalTo(builderAuthenticatorKey(email,passwordHashed))
                .limitToFirst(1);
        FirebaseQueryLiveData ris = new FirebaseQueryLiveData(ref);
        return Transformations.map(ris, new Deserializer_Utenti.Deserializer_ObjectFilter());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Utenti> getByAuthenticator_Synchronized(@NonNull String email,@NonNull String passwordHashed){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_UTENTI)
                .orderByChild(P_EMAIL__HASHED_PSW)
                .equalTo(builderAuthenticatorKey(email,passwordHashed))
                .limitToFirst(1);;
        FirebaseQueryLiveData_Synchronized ris = new FirebaseQueryLiveData_Synchronized(ref);
        return Transformations.map(ris, new Deserializer_Utenti.Deserializer_ObjectFilter());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET SALT by EMAIL                                               */
/*================================================================================================*/
    protected LiveData<String> getSaltByEmail_OneTime(@NonNull String email){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_UTENTI)
                .orderByChild(P_EMAIL)
                .equalTo(email)
                .limitToFirst(1);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Utenti.Deserializer_Salt());
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  ESISTE UTENTE CON EMAIL                                       */
/*================================================================================================*/
    public LiveData<Boolean> esiteEmail_OneTime (@NotNull String email){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_UTENTI)
                .orderByChild(P_EMAIL)
                .equalTo(email)
                .limitToFirst(1);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Utenti.Deserializer_Boolean());
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  ESISTE UTENTE CON EMAIL                                       */
/*================================================================================================*/
    public LiveData<Boolean> esiteMatricola_OneTime (@NotNull Long idUniversita,@NotNull Long idCrso  ,@NotNull Integer matricola){
        Query ref = super.getReferenceFireBase().getReference()
                .child(L_UTENTI_DEL_CORSO_DEL_UNIVERSITA)
                .child(idUniversita.toString())
                .child(L_CORSI)
                .child(idCrso.toString())
                .child(L_ISCRITTI)
                .orderByChild(P_MATRICOLA)
                .equalTo(matricola.toString())
                .limitToFirst(1);
        FirebaseQueryLiveData_OneTime ris = new FirebaseQueryLiveData_OneTime(ref);
        return Transformations.map(ris, new Deserializer_Utenti.Deserializer_Boolean());
    }
/*================================================================================================*/
/*################################################################################################*/
}