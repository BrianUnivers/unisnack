package it.tn.unitn.disi.ppsmet;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import it.tn.unitn.disi.ppsmet.ActivityManager.LoginActivity.LoginViewModel;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;
import it.tn.unitn.disi.ppsmet.GestioneMyIntent.MyIntent;
import it.tn.unitn.disi.ppsmet.GestioneRicordaSessione.GestioneSessione;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.SeparatorePaginaConTesto;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewField.ViewInputField_Email;
import it.tn.unitn.disi.ppsmet.IncludeView_Setup.ViewField.ViewInputField_Password;
import it.tn.unitn.disi.ppsmet.MyAnimation.MyAnimationView;

public class LoginActivity extends AppCompatActivity {
/*################################################################################################*/
/*                                  RIDUCE APPLICAZIONE                                           */
/*################################################################################################*/
/*================================================================================================*/
/*                                  METODO PER IMPEDIRE DI RAGGIUNGERE ACTIVITY PRECEDENTI        */
/*================================================================================================*/
    /**
     * Questo metodo impedisce di ritornare tramite il tasto "Fisico" indietro dello smartphone alle
     * activity precedenti così da rendere la activity attuale quella di root per la navigazione.
     */
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
/*================================================================================================*/
/*################################################################################################*/
    private Long  DELAY = 200L;
    private Timer TIMER = new Timer();
/*################################################################################################*/
/*                                  CREAZIONE DEL LAYOUT                                          */
/*################################################################################################*/
    private LoginActivity  activity=this;
    private LoginViewModel viewModel;
/*------------------------------------------------------------------------*/
    private MyAnimationView logoAnimato;
    private TextView        titolo;
    private ViewGroup       loginPage;
    private View            divisore;
/*------------------------------------------------------------------------*/
    private ViewGroup      formView;
    private TableRow       divisoreView;
    private CheckBox                ricordamiView;
    private ViewInputField_Email    campoEmail;
    private ViewInputField_Password campoPassword;
/*------------------------------------------------------------------------*/
    private Button  btnAccedi;
    private Button  btnRegistrati;
    private Button  btnAccediConGoogle;
    private Button  btnAccediConUniversita;
/*------------------------------------------------------------------------*/
/*================================================================================================*/
/*                                  CREAZIONE DELL'ACTITVITY                                      */
/*================================================================================================*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
/*------------------------------------------------------------------------*/
        logoAnimato  = findViewById(R.id.img_anim_LogoApp);
        titolo       = findViewById(R.id.txt_NomeApp);
        loginPage    = findViewById(R.id.scl_Login);
        divisore     = findViewById(R.id.dvd_DivisoreLogo);
        divisoreView = findViewById(R.id.trw_Divisore);
        formView     = findViewById(R.id.lly_LoginConCredenzali);
        ricordamiView= findViewById(R.id.ckb_Input_Ricordami);
        impostataLayoutStatico();
/*------------------------------------------------------------------------*/
        btnAccedi     = findViewById(R.id.btn_Accedi);
        btnRegistrati = findViewById(R.id.btn_Registrati);
        btnAccediConGoogle     = findViewById(R.id.btn_AccediConGoogle);
        btnAccediConUniversita = findViewById(R.id.btn_AccediEmailUniversita);
        impostataBottoneAccedi();
        impostataBottoneRegistrati();
        impostataBottoneAccediConGoogle();
        impostataBottoneAccediConUniversita();
/*------------------------------------------------------------------------*/
        viewModel =  new ViewModelProvider(this).get(LoginViewModel.class);
/*------------------------------------------------------------------------*/
       //SESSIONE
        gestioneSessioneUtenteAttiva();
/*------------------------------------------------------------------------*/
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA LAYOUT STATICO                                        */
/*================================================================================================*/
    /**
     * Questo metodo pertette di impostrate le varie parti statiche di layout.
     */
    private void impostataLayoutStatico(){
        campoEmail    = new ViewInputField_Email(formView,
                                                R.id.include_Input_Email,
                                                R.string.lbl_Email,
                                                R.string.try_Email);
        campoPassword = new ViewInputField_Password(formView,
                                                    R.id.include_Input_Password,
                                                    R.string.lbl_Password,
                                                    R.string.try_Password);
/*------------------------------------------------------------------------*/
        SeparatorePaginaConTesto.setSeparatorePaginaConTestoDeiInclude( divisoreView,
                                                                        R.id.include_AccediRecistratiCon,
                                                                        R.string.lbl_dvd_AccediORegistratiCon);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA VISIONE METODO DI ACCESSO E REGISTRAZIONE             */
/*================================================================================================*/
    /**
     * Questo metodo rende visibile la parte della pagina contenete i pulsanti per la registrazione
     * e la form per l'insermento dei dati per l'autenticazione.
     */
    private void rendiVisibileFormAccesso(){
        divisore.setVisibility(View.VISIBLE);
        loginPage.setVisibility(View.VISIBLE);
    }
/*------------------------------------------------------------------------*/
    /**
     * Questo metodo toglie dalla parte della pagina contenete i pulsanti per la registrazione
     * e la form per l'insermento dei dati per l'autenticazione. In questo modo il logo ed il
     * titolo verranno centrati nella pagina.
     */
    private void rendiInvisibileFormAccesso(){
        divisore.setVisibility(View.GONE);
        //loginPage.setVisibility(View.GONE);
        loginPage.animate()
                .scaleY(0)
                .alpha(0.0f)
                .setDuration(DELAY)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        loginPage.setVisibility(View.GONE);
                    }
                });
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  CAMBIA ACTIVITY                                               */
/*################################################################################################*/
/*================================================================================================*/
/*                                  APRI ACTYVITY PROFILO UTENTE                                  */
/*================================================================================================*/
    /**
     * Questo metodo, dato l'utente che è stato autenticato in precedenza, fa partire una task teporizata
     * dopo un certo periodo di tempo, che effetuerà il cambiamento della activity, apredo quella
     * del profilo utente.
     * @param utenti Questo parametro contiene l'utente autenticato che dovrà apparire nella pagina
     *               del profilo utente.
     */
    private void apriActivityProfiloUtente(Utenti utenti){
        //Attiva animazione infinita
        TIMER.schedule(new TimerTask() {
            @Override
            public void run() {
                MyIntent i = new MyIntent(LoginActivity.this, ProfiloUtenteActivity.class);
                i.putExtraIdUtente(MyIntent.I_UTENTE_ATTIVO,utenti);
                startActivity(i);
                }
            },5*DELAY);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  APRI ACTYVITY REGISTAZIONE                                    */
/*================================================================================================*/
    /**
     * Questo metodo permmette di cambiare activity aprendo quella per effetuare la registazione.
     */
    private void apriActivityRegistrazione(){
        Intent i = new Intent(LoginActivity.this, RegistrazioneActivity.class);
        startActivity(i);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONI SESSIONE ATTIVE                                      */
/*################################################################################################*/
/*================================================================================================*/
/*                                  ATTIVA SE PRESENTE L'UTENTE DELLA SESSIONE ATTIVA E ACCEDI    */
/*================================================================================================*/
    /**
     * Questo metodo permette di verificare se è presente una sessione attiva valida ed eventualmente
     * richiedere i dati dell'utente per poi accedere all'app.
     * Se non c'è nessuna sessione ativa fa riapparire il form con tutti i metodi d'accesso.
     * Se l'autenticazione richiede troppo tempo, ad esempio del DEBUG, c'è un limite di tempo che dopo
     * il quale fa riapplarire il form con tutti i metodi d'accesso.
     */
    private void gestioneSessioneUtenteAttiva(){
        if(GestioneSessione.esisteSessioneApera(getApplicationContext())){
            Future<Utenti> utenteAttivoFuturo =
                    viewModel.restituisceSePossibileUtenteConSessioneAttva(
                            GestioneSessione.getIdSessione(getApplicationContext()));
            try {
                Utenti utenteAttivo = utenteAttivoFuturo.get(DELAY,TimeUnit.MILLISECONDS);
                if (utenteAttivo != null) {
                    azzioneEsitoAutenticazione(utenteAttivo, true);
                } else {
                    rendiVisibileFormAccesso();
                }
            }catch (Exception e){
                Log.d("ERROR_SESSIONE",e.getMessage(),e);
            }
        }else{
            rendiVisibileFormAccesso();
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  IMPOSTA GESTIONI BOTTONI                                      */
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA BOTTONE ACCEDI                                        */
/*================================================================================================*/
    /**
     * Questo moetodo contiene l'imostazione del Listener del pulsante Accedi con tutte le azzioni
     * per il recupero di tutte le informazioni del form d'autenticazione. Ed se tutto correto porta
     * ad effetuare l'atenticazione e di conseguenza l'accesso dell'utente.
     */
    private void impostataBottoneAccedi(){
        btnAccedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email      = campoEmail.getEmail();
                String password   = campoPassword.getPassword();
                Boolean ricordami = ricordamiView.isChecked();

                boolean datiValidi = true;
                if(!viewModel.verificaValiditaEmail(email)){
                    campoEmail.impostaErrore(R.string.err_CampoObbligatorio);
                    datiValidi =false;
                }
                if(!viewModel.verificaValiditaPassword(password)){
                    campoPassword.impostaErrore(R.string.err_CampoObbligatorio);
                    datiValidi =false;
                }
                if(datiValidi){
                    richiediAutenticazione(email,password,ricordami);
                }
            }
        });
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo riceve i valori già controllati ed effetua l'autentificazione del utentente tramite
     * il viewModel. Non appena c'è il risultato (Vedi il metodo "autenticazioneUtentete" della
     * "LoginViewModel") fa partire un metodo per analizzare l'esito dell'autenticazione.
     * @param email     Parametro che contiene l'email del account che si vule autenticare.
     * @param password  Parametro che contiene la password del account che si vule autenticare.
     * @param ricordami Parametro che contiene  l'informazione per sapere se devo rendere persitenete
     *                  l'autenticazione.
     */
    private void richiediAutenticazione(String email,String password,boolean ricordami){
        LiveData<Utenti> utentiLiveData = viewModel.autenticazioneUtentete(email,password);
        utentiLiveData.observe(activity, new Observer<Utenti>() {
                private boolean primoTentativo=true;
                @Override
                public void onChanged(Utenti utenti) {
                    if((utenti!=null)){
                        azzioneEsitoAutenticazione(utenti,ricordami);
                        utentiLiveData.removeObserver(this);
                    }else if(primoTentativo){
                        primoTentativo =false;
                    }else{
                        azzioneEsitoAutenticazione(utenti,ricordami);
                        utentiLiveData.removeObserver(this);
                    }
                }
            });
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo metodo riceve l'esito del'autenticazione e se l'utente è null segala un possibile errore
     * nella password o un errore nella connesione poichè ritorna null anche superata una certa soglia
     * d'attesa. Se invece esite un utente allora da il bentornato all'utente e avvia la richesta di
     * cambiare l'activity con quuella del profilo utente.
     * Inoltre in base al parametro "ricordami" attiva, se true, o non attiva,se false, una sessione per
     * antenere l'accesso in seguito.
     * @param utenti
     * @param ricordami
     */
    private void azzioneEsitoAutenticazione(Utenti utenti,boolean ricordami){
        if(utenti==null){
            campoPassword.impostaErrore(R.string.err_ImpossibileAccedere);
        }else{
            String messaggio = "Bentornato \n"+utenti.getCognome()+" "+utenti.getNome();
            titolo.setText(messaggio);
            rendiInvisibileFormAccesso();
            String idSessione = GestioneSessione.creaSessione(getApplicationContext(),ricordami);
            if(ricordami){
                viewModel.aggiungiNuovaSessioneAttiva(utenti,idSessione);
            }
            apriActivityProfiloUtente(utenti);
        }
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA BOTTONE REGISTRATI                                    */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'imostanzione del listener del pulsante registrati che farà aprire la pagina
     * di registrazione.
     */
    private void impostataBottoneRegistrati(){
        btnRegistrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                apriActivityRegistrazione();
            }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA BOTTONE ACCEDI CON GOOOGLE                            */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'imostanzione del listener del pulsante accedi con google che
     * dovrebbe autenticare un utente con l'accout google.
     */
    private void impostataBottoneAccediConGoogle(){
        btnAccediConGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplication(),"Premuto Accedi Con Google",Toast.LENGTH_SHORT).show();
            }
        });
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  IMPOSTA BOTTONE ACCEDI CON UNIVERSITA'                        */
/*================================================================================================*/
    /**
     * Questo metodo contiene l'imostanzione del listener del pulsante accedi con Universita che
     * dovrebbe autenticare un utente con l'accout Universita.
     */
    private void impostataBottoneAccediConUniversita(){
        btnAccediConUniversita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplication(),"Premuto Accedi Con Universita",Toast.LENGTH_SHORT).show();
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  GESTIONE ANIMAZIONE                                           */
/*################################################################################################*/
/*================================================================================================*/
/*                                  ON RESUME                                                     */
/*================================================================================================*/
    /**
     * In questo override del metodo "onResume" si trova la logica legata alla attivazione delle
     * animazioni infinite presenti nella pagina. Questo poichè saranno immediatamente fermate appena
     * verrà chiamato il metodo "onPause".
     */
    @Override
    protected void onResume() {
        super.onResume();
        logoAnimato.setNewAnimazioneRipetuta(   R.drawable.avd_anim_logo,
                                                MyAnimationView.RIPETIZIONE_INFINITA);
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  ON PAUSE                                                      */
/*================================================================================================*/
    /**
     * In questo override del metodo "onPause" si trova la logica legata alla attivazione delle
     * animazioni presenti nella pagina così da blocare qualunque animazione.
     */
    @Override
    protected void onPause() {
        super.onPause();
        //Attiva animazione infinita
        logoAnimato.setNewAnimazioneRipetuta(   R.drawable.avd_anim_logo,
                                                MyAnimationView.RIPETIZIONE_DI_DEFAUULT);
    }
/*================================================================================================*/
/*################################################################################################*/
}