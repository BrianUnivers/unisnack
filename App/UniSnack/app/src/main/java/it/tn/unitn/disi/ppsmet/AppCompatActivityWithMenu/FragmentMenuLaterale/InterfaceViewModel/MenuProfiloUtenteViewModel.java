package it.tn.unitn.disi.ppsmet.AppCompatActivityWithMenu.FragmentMenuLaterale.InterfaceViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import it.tn.unitn.disi.ppsmet.ActivityManager.CommonViewModelUserLogged;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.Utenti_Repository;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

/**
 * Questa classe contiene dei metodi utilizati dalla fragment che si trova nel menu latrare cosi che
 * possa accedere ai dati del utente per inserili.
 */
public class MenuProfiloUtenteViewModel extends CommonViewModelUserLogged {
/*################################################################################################*/
    private Utenti_Repository rUtenti;
/*================================================================================================*/
/*                                  COSTRUTTORE VIEW MODEL                                        */
/*================================================================================================*/
    /**
     * Questo metodo costruttore del viewmode inizializza le repository che si interfacciano con entrambi
     * i database sia interno che estreno a seconda delle operazioni che vengono richeste. Che verranno
     * utilizate dall'activity e i suoi fragment.
     * @param application
     */
    public MenuProfiloUtenteViewModel(@NonNull Application application) {
        super(application);
        rUtenti = new Utenti_Repository(application);
    }
/*================================================================================================*/
/*################################################################################################*/
/*################################################################################################*/
/*                                  AZZIONI LEGATE ALLA GESITIONE DEI DATABASE                    */
/*################################################################################################*/
    private   Long  idUtenteAttivo = null;
    protected MediatorLiveData<Utenti> utenteAttivo = new MediatorLiveData<>();
/*================================================================================================*/
/*                                  CAMBIA UTENTE ATTIVO DA ID                                    */
/*================================================================================================*/
    /**
     * Questo metodo viene utilizato da chi conosce l'id (Activity) così facendeo viene salvato il
     * risultato nel ViewModel che lo rende disponibile ai richiedenti tramite il metodo
     * "getUtenteAttivo".
     * @param id Questo parametro contiene id dell'utente che si vuole rendere attivo.
     * @return Il risultato è un oggetto di tipo LiveData<Utenti> che non appena effetuerà l'acceso
     *         ai dati conterrà l'utente richieso.
     */
    public  LiveData<Utenti> cambiaUtenteAttivo(Long id){
        idUtenteAttivo=id;
        utenteAttivo.addSource(rUtenti.getByPrimaryKey(id), new Observer<Utenti>() {
            @Override
            public void onChanged(@Nullable Utenti utenti) {
                utenteAttivo.postValue(utenti);
            }
        });
        return utenteAttivo;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  GET UTENTE ATTIVO                                             */
/*================================================================================================*/
    /**
     * Questo metodo viene utilizato da delle componeti di una activity come i fragment, che potrebbero
     * non conosere l'id dell'utente attivo ma necessitano i dei suoi dati. Così facendo qualcunaltro
     * invierà la richesta dell'utente tramite l'id e tutti coloro che hanno richesto l'id riceveranno
     * l'utente.
     * @return Il risultato è un oggetto di tipo LiveData<Utenti> che non appena qualcuno effetua
     *          la richiesta "cambiaUtenteAttivo" e viene effetuerà l'acceso ai dati, conterrà
     *          le informazioni dell'utente.
     */
    public LiveData<Utenti> getUtenteAttivo(){
        return utenteAttivo;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                                  ESISTE UN UTENTE ATTIVO                                       */
/*================================================================================================*/
    /**
     * Questo metodo verifica se c'è stata già eseguita almeno una volta, la richiesta dei dati di un
     * utente.
     * @return Il risultato è vero se qualcuno ha chamato prima di quata chamanta ha eseguito il metodo
     *          "cambiaUtenteAttivo" con un paramento valido.
     */
    public boolean isSetUtenteAttivo(){
        return ((idUtenteAttivo!=null) && (idUtenteAttivo>0));
    }
/*================================================================================================*/
/*################################################################################################*/
}
