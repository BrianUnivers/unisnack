package it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListSpinnerWithAdd;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import it.tn.unitn.disi.ppsmet.IncludeView_Setup.RecyclerView.RecyclerListSpinnerWithAdd.AdapterWithSpinner.Interface.ListaSpinnerRVAdapter;
import it.tn.unitn.disi.ppsmet.R;

public class ViewRecyclerListSpinnerWithAdd {
/*################################################################################################*/
    private Button       btnAggiungiElemento     = null;
    private RecyclerView rclListaSeletoriSpinner = null;
    public ListaSpinnerRVAdapter adapter = null;
    private Context context;
    private ViewGroup contenitore = null;
    private View.OnClickListener listener = null;
/*================================================================================================*/
/*                                  COSTRUTTRI                                                    */
/*================================================================================================*/
    /**
     * Questo è il metodo costrittore che imposta un blocco di layout importato in modo tale da contenere
     * un recyclerView con un tasto aggiungi imbase al numero di dati che contiene uno dei suoi
     * spinner.
     * Qui viene passato direttamente l'adaprer di uno spinner, con i dati già associati.
     * Mentre l'adapter che gestisce il recyclerView è gestito internamente.
     * @param context
     * @param rootLayout
     * @param idLayoutIncluded
     * @param idTextButtonAggiungi
     * @param adapter
     */
    public ViewRecyclerListSpinnerWithAdd(  Context context,
                                            ViewGroup rootLayout,
                                            int idLayoutIncluded,
                                            int idTextButtonAggiungi,
                                            ListaSpinnerRVAdapter adapter) {
        if(rootLayout!=null){
            contenitore = (ViewGroup) rootLayout.findViewById(idLayoutIncluded);
            if(contenitore!=null){
                try {
                    btnAggiungiElemento = (Button) contenitore.findViewById(R.id.btn_AggiungiNouvoElemento);
                    rclListaSeletoriSpinner = (RecyclerView) contenitore.findViewById(R.id.rcl_ListaSpinner);
                }catch (Exception e){
                    btnAggiungiElemento    = null;
                    rclListaSeletoriSpinner= null;
                }
                if( (btnAggiungiElemento!=null) &&
                    (rclListaSeletoriSpinner!=null)){

                    btnAggiungiElemento.setText(idTextButtonAggiungi);
                    if((adapter!=null) && (context!=null)){
                        impostaAdapterEPulsante(context, adapter);
                    }else{
                        btnAggiungiElemento.setVisibility(View.GONE);
                    }
                }
            }
        }
    }
/*------------------------------------------------------------------------------------------------*/
    /**
     * Questo medodo si occupa di impostare o reimpostare l'adapter degli pinner da utilizzare.
     * @param context
     * @param adapter
     */
    private void impostaAdapterEPulsante(Context context,ListaSpinnerRVAdapter adapter){
        btnAggiungiElemento.setVisibility(View.VISIBLE);
        this.adapter=adapter;
        this.context=context;
        rclListaSeletoriSpinner.setLayoutManager(new LinearLayoutManager(context));
        adapter.aggiungiBottoneDaAttivareOsiattivare(btnAggiungiElemento);
        rclListaSeletoriSpinner.setAdapter(adapter);
        adapter.addChangeListener(listener,contenitore);
        btnAggiungiElemento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.inserisciNuovoElemento();
            }
        });
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA IL SEGALE D'AVVISO DEGLI SPINNER                      */
/*================================================================================================*/
    /**
     * Questo metodo fa evidenziare tutti gli spinner di un altro colore( usato per indicere che c'è
     * stato un aggiornamento dei dati, tra cui quello selezionato e quindi è meglio stare attenti che
     * sia tutto selezionato corretamente).
     */
    public void impostaAvviso(){
        if(adapter!=null){
            adapter.impostaAvviso();
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  IMPOSTA IL SEGALE D'AVVISO DEGLI SPINNER                      */
/*================================================================================================*/
    /**
     * Questo metodo permette di reimpostare gli adapter degli spinner in seguito se non viene fatto
     * direttamente nel costruttore o se li si vuole cambiare.
     * @param context
     * @param adapter
     */
    public void setNewAdapter(Context context,ListaSpinnerRVAdapter adapter){
        if( (btnAggiungiElemento!=null) && (adapter!=null) && (context!=null) &&
                (rclListaSeletoriSpinner!=null)){
            impostaAdapterEPulsante(context,adapter);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                                  ADD LISSERENR ON CHANGE                                       */
/*================================================================================================*/
    /**
     * Questo metodo permette di aggiungere un Listener che si attiva nel momento in cui si aggiunge
     * o rimuovere uno spinnere dalla lista delgli spinner, e se implementato nell'adapter degli
     * spinnere ListaSpinnerRVAdapter(Come ad esempio nella classe "ListaSpinnerAllergeniRVAdapter")
     * può attivare il lisserne anche quando c'è una selezione di un elemento.
     * @param listener
     */
    public void addChangeListener(View.OnClickListener listener){
        this.listener=listener;
        if(adapter!=null && contenitore!=null){
            adapter.addChangeListener(listener,contenitore);
        }
    }
/*================================================================================================*/
/*################################################################################################*/
}
