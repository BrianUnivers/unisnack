package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Ignore;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.PdM_propone_P;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.U_intollerante_A;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.Relation.U_vota_PdM;
import it.tn.unitn.disi.ppsmet.Entity.PastoDelMese;
import it.tn.unitn.disi.ppsmet.Entity.Pietanze;

@Dao
public abstract class PastoDelMese_Dao extends PietanzeInsertExist_Dao {
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO INSERT                                                          */
/*================================================================================================*/
    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long insert(PastoDelMese pastoDelMese, List<Pietanze> listaPietanza){
        Long ris = this.insert(pastoDelMese);
        Iterator<Pietanze> iterator=listaPietanza.iterator();
        while(iterator.hasNext()){
            Pietanze pietanze=iterator.next();
            Long idPietanze =pietanze.getId();
            if(idPietanze==null){
                idPietanze =super.exist(pietanze);
                if(idPietanze==null){
                    idPietanze =this.insert(pietanze);
                }
            }
            PdM_propone_P propone =new PdM_propone_P(pastoDelMese.getId_utente(),pastoDelMese.getMese(),pastoDelMese.getAnno(),idPietanze);
            ris++;
        }
        this.insertVoto(pastoDelMese,pastoDelMese.getId_utente());
        ris++;
        return ris;
    }
/*------------------------------------------------------------------------*/
    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long insertVoto(PastoDelMese pastoDelMese, Long idUtenti ){
        U_vota_PdM voto =new U_vota_PdM(pastoDelMese.getId_utente(),pastoDelMese.getMese(),pastoDelMese.getAnno(),idUtenti);
        if(this.exist(voto)!=0){
            this.delete(voto);
        }
        return this.insert(voto);
    }
/*------------------------------------------------------------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    protected abstract long insert(PdM_propone_P propone);
/*------------------------------------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    protected abstract long insert(PastoDelMese pastoDelMese);
/*------------------------------------------------------------------------*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    protected abstract long insert(U_vota_PdM vota);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO DELETE                                                          */
/*================================================================================================*/
    @Transaction
    public void delete(PastoDelMese pastoDelMese){
        this.deleteVoto(pastoDelMese.getId_utente(),pastoDelMese.getMese(),pastoDelMese.getAnno());
        this.deletePropone(pastoDelMese.getId_utente(),pastoDelMese.getMese(),pastoDelMese.getAnno());
        this.deletePastoDelMeseSingolo(pastoDelMese.getId_utente(),pastoDelMese.getMese(),pastoDelMese.getAnno(),pastoDelMese.getDataProposta());
    }
/*------------------------------------------------------------------------*/
    @Transaction
    public void delete(U_vota_PdM vota){
        this.deleteVotoSingolo(vota.getId_proponete(),
                    vota.getId_mese(),
                    vota.getId_anno(),
                    vota.getId_utente());
    }
/*------------------------------------------------------------------------*/
    @Transaction
    public void deleteAll(){
        this.deleteAllVoto();
        this.deleteAllPropone();
        this.deleteAllPastoDelMese();
    }
/*------------------------------------------------------------------------------------------------*/
    @Query( "DELETE FROM tableu_vota_pdm " +
            "WHERE   id_proponete == :idProponente AND " +
                    "id_mese == :mese AND " +
                    "id_anno == :anno AND " +
                    "id_utente == :idUtente ")
    protected abstract void deleteVotoSingolo(Long idProponente,
                                   Short mese,
                                   Short anno,
                                   Long idUtente);
/*------------------------------------------------------------------------*/
    @Query( "DELETE FROM tableu_vota_pdm " +
            "WHERE   id_proponete == :idProponente AND " +
                    "id_mese == :mese AND " +
                    "id_anno == :anno ")
    protected abstract void deleteVoto(Long idProponente,
                                       Short mese,
                                       Short anno);
/*------------------------------------------------------------------------*/
    @Query( "DELETE FROM tablePdM_propone_P " +
            "WHERE   id_proponente == :idProponente AND " +
                    "id_mese == :mese AND " +
                    "id_anno == :anno ")
    protected abstract void deletePropone( Long idProponente,
                                           Short mese,
                                           Short anno);
/*------------------------------------------------------------------------*/
    @Query( "DELETE FROM tableu_vota_pdm ")
    protected abstract void deleteAllVoto();
/*------------------------------------------------------------------------*/
    @Query( "DELETE FROM tablePdM_propone_P ")
    protected abstract void deleteAllPropone();
/*------------------------------------------------------------------------*/
    @Query( "DELETE FROM tablePastoDelMese ")
    protected abstract void deleteAllPastoDelMese();
/*------------------------------------------------------------------------*/
    @Query( "DELETE FROM tablePastoDelMese " +
            "WHERE id_utente   == :idProponente AND " +
            "mese == :mese AND " +
            "anno == :anno AND " +
            "dataProposta == :dataProposta ")
    protected abstract void deletePastoDelMeseSingolo(Long idProponente,
                                        Short mese,
                                        Short anno,
                                        Date dataProposta);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO UPDATE                                                          */
/*================================================================================================*/
    @Update(entity = PastoDelMese.class,
            onConflict = OnConflictStrategy.ABORT)
    public abstract void update(PastoDelMese pastoDelMese);
/*------------------------------------------------------------------------*/
    @Transaction
    @Update(onConflict = OnConflictStrategy.ABORT)
    public void updateListaPietanze(PastoDelMese pastoDelMese, List<Pietanze> listaPietanza){
        this.deleteVoto(pastoDelMese.getId_utente(),pastoDelMese.getMese(),pastoDelMese.getAnno());
        this.deletePropone(pastoDelMese.getId_utente(),pastoDelMese.getMese(),pastoDelMese.getAnno());
        Iterator<Pietanze> iterator=listaPietanza.iterator();
        while(iterator.hasNext()){
            Pietanze pietanze=iterator.next();
            Long idPietanze =pietanze.getId();
            if(idPietanze==null){
                idPietanze =super.exist(pietanze);
                if(idPietanze==null){
                    idPietanze =this.insert(pietanze);
                }
            }
            PdM_propone_P propone =new PdM_propone_P(pastoDelMese.getId_utente(),pastoDelMese.getMese(),pastoDelMese.getAnno(),idPietanze);
        }
        this.insertVoto(pastoDelMese,pastoDelMese.getId_utente());
    }
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET ALL                                                         */
/*================================================================================================*/
    @Query("SELECT * FROM tablePastoDelMese")
    public abstract List<PastoDelMese> getAll();
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tablePastoDelMese")
    public abstract LiveData<List<PastoDelMese>> getAll_Synchronized();
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID                                                       */
/*================================================================================================*/
    @Query("SELECT * FROM tablePastoDelMese WHERE id_utente == :idUtente AND mese == :mese AND anno == :anno")
    public abstract PastoDelMese getByPrimaryKey(Long idUtente, Short mese,Short anno);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tablePastoDelMese WHERE id_utente == :idUtente AND mese == :mese AND anno == :anno")
    public abstract LiveData<PastoDelMese> getByPrimaryKey_Synchronized(Long idUtente, Short mese,Short anno);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ID UTENTI                                                */
/*================================================================================================*/
    @Query("SELECT * FROM tablePastoDelMese WHERE id_utente == :idUtente")
    public abstract List<PastoDelMese> getByIdUtente(Long idUtente);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tablePastoDelMese WHERE id_utente == :idUtente")
    public abstract LiveData<List<PastoDelMese>> getByIdUtente_Synchronized(Long idUtente);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET by ANNO AND MESE                                            */
/*================================================================================================*/
    @Query("SELECT * FROM tablePastoDelMese WHERE mese == :mese AND anno == :anno")
    public abstract List<PastoDelMese> getByAnno_Mese(Short mese,Short anno);
/*------------------------------------------------------------------------*/
    @Query("SELECT * FROM tablePastoDelMese WHERE mese == :mese AND anno == :anno")
    public abstract LiveData<List<PastoDelMese>> getByAnno_Mese_Synchronized(Short mese,Short anno);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         METODO GET VOTI of PASTO DEL MESE                                      */
/*================================================================================================*/
    @Transaction
    public Long getVotiSingoloPastoDelMese(PastoDelMese pastoDelMese){
        return this.getVotiSingoloPastoDelMese(  pastoDelMese.getId_utente(),
                                                pastoDelMese.getMese(),
                                                pastoDelMese.getAnno());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Long> getVotiSingoloPastoDelMese_Synchronized(PastoDelMese pastoDelMese){
        return this.getVotiSingoloPastoDelMese_Synchronized(pastoDelMese.getId_utente(),
                                                            pastoDelMese.getMese(),
                                                            pastoDelMese.getAnno());
    }
/*------------------------------------------------------------------------------------------------*/
    @Query( "SELECT COUNT(DISTINCT V.id_utente) " +
            "FROM tablePastoDelMese AS PdM, tableU_vota_PdM AS V " +
            "WHERE   V.id_proponete == PdM.id_utente AND V.id_mese == PdM.mese AND V.id_anno == PdM.anno AND " +
                    "PdM.id_utente == :idUtente AND " +
                    "PdM.mese == :mese AND " +
                    "PdM.anno == :anno " )
    protected abstract Long getVotiSingoloPastoDelMese(Long idUtente,
                                                      Short mese,
                                                      Short anno);
/*------------------------------------------------------------------------*/
    @Query( "SELECT COUNT(DISTINCT V.id_utente) " +
            "FROM tablePastoDelMese AS PdM, tableU_vota_PdM AS V " +
            "WHERE   V.id_proponete == PdM.id_utente AND V.id_mese == PdM.mese AND V.id_anno == PdM.anno AND " +
                    "PdM.id_utente == :idUtente AND " +
                    "PdM.mese == :mese AND " +
                    "PdM.anno == :anno " )
    protected abstract LiveData<Long>  getVotiSingoloPastoDelMese_Synchronized(Long idUtente,
                                                                               Short mese,
                                                                               Short anno);
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO GET VOTI of PASTO DEL MESE RISPETTO AL TOTALE                   */
/*================================================================================================*/
    @Transaction
    public Double getVotiSingoloPastoDelMeseRispettoAlTotale(PastoDelMese pastoDelMese){
        return this.getVotiSingoloPastoDelMeseRispettoAlTotale(  pastoDelMese.getId_utente(),
                pastoDelMese.getMese(),
                pastoDelMese.getAnno());
    }
/*------------------------------------------------------------------------*/
    public LiveData<Double> getVotiSingoloPastoDelMeseRispettoAlTotale_Synchronized(PastoDelMese pastoDelMese){
        return this.getVotiSingoloPastoDelMeseRispettoAlTotale_Synchronized(pastoDelMese.getId_utente(),
                pastoDelMese.getMese(),
                pastoDelMese.getAnno());
    }
/*------------------------------------------------------------------------------------------------*/
    @Query( "Select ParzV.voti/TotV.voti " +
            "From ( SELECT COUNT(DISTINCT V.id_utente) AS voti " +
                    "FROM tablePastoDelMese AS PdM, tableU_vota_PdM AS V " +
                    "WHERE   V.id_proponete == PdM.id_utente AND V.id_mese == PdM.mese AND V.id_anno == PdM.anno AND " +
                    "PdM.id_utente == :idUtente AND " +
                    "PdM.mese == :mese AND " +
                    "PdM.anno == :anno " +
                ") AS ParzV ," +
                "(  SELECT COUNT(DISTINCT V.id_utente) AS voti " +
                    "FROM tablePastoDelMese AS PdM, tableU_vota_PdM AS V " +
                    "WHERE   V.id_proponete == PdM.id_utente AND V.id_mese == PdM.mese AND V.id_anno == PdM.anno AND " +
                        "PdM.mese == :mese AND " +
                        "PdM.anno == :anno ) AS TotV ")
    protected abstract Double getVotiSingoloPastoDelMeseRispettoAlTotale(Long idUtente,
                                                       Short mese,
                                                       Short anno);
/*------------------------------------------------------------------------*/
    @Query( "Select ParzV.voti/TotV.voti " +
            "From ( SELECT COUNT(DISTINCT V.id_utente) AS voti " +
                    "FROM tablePastoDelMese AS PdM, tableU_vota_PdM AS V " +
                    "WHERE   V.id_proponete == PdM.id_utente AND V.id_mese == PdM.mese AND V.id_anno == PdM.anno AND " +
                            "PdM.id_utente == :idUtente AND " +
                            "PdM.mese == :mese AND " +
                            "PdM.anno == :anno " +
                    ") AS ParzV ," +
                    "(  SELECT COUNT(DISTINCT V.id_utente) AS voti " +
                        "FROM tablePastoDelMese AS PdM, tableU_vota_PdM AS V " +
                        "WHERE   V.id_proponete == PdM.id_utente AND V.id_mese == PdM.mese AND V.id_anno == PdM.anno AND " +
                                "PdM.mese == :mese AND " +
                                "PdM.anno == :anno ) AS TotV ")
    protected abstract LiveData<Double>  getVotiSingoloPastoDelMeseRispettoAlTotale_Synchronized(Long idUtente,
                                                                                   Short mese,
                                                                                   Short anno);
/*================================================================================================*/
/*################################################################################################*/
/*================================================================================================*/
/*                         EXIST                                                                  */
/*================================================================================================*/
    @Transaction
    public long exist(PastoDelMese pastoDelMese){
        return this.exist(  pastoDelMese.getId_utente(),
                            pastoDelMese.getMese(),
                            pastoDelMese.getAnno(),
                            pastoDelMese.getDataProposta());
    }
/*------------------------------------------------------------------------*/
    @Transaction
    public long exist(U_vota_PdM vota){
        return this.exist(  vota.getId_proponete(),
                            vota.getId_mese(),
                            vota.getId_anno(),
                            vota.getId_utente());
    }
/*------------------------------------------------------------------------------------------------*/
   @Query( "SELECT COUNT(  *) AS tot  " +
           "FROM (  SELECT DISTINCT PdM.id_utente AS col1 , PdM.mese  AS col2, PdM.anno AS col3 " +
                    "FROM tablePastoDelMese AS PdM " +
                    "WHERE   PdM.id_utente == :idUtente AND " +
                            "PdM.mese == :mese AND " +
                            "PdM.anno == :anno AND " +
                            "PdM.dataProposta == :dataProposta)" )
    protected abstract long exist(Long idUtente,
                                  Short mese,
                                  Short anno,
                                  Date dataProposta);
/*------------------------------------------------------------------------*/
    @Query( "SELECT COUNT(*) AS tot  " +
            "FROM (  SELECT DISTINCT V.id_utente AS col1 , V.id_mese AS col2 , V.id_anno AS col3 "+
                    "FROM tableU_vota_PdM AS V " +
                    "WHERE   V.id_proponete == :idProponete AND " +
                            "V.id_mese == :idMese AND " +
                            "V.id_anno == :idAnno AND " +
                            "V.id_utente == :idUtente ) " )
    protected abstract long exist(Long idProponete,
                                  Short idMese,
                                  Short idAnno,
                                  Long idUtente);
/*================================================================================================*/
/*################################################################################################*/
}