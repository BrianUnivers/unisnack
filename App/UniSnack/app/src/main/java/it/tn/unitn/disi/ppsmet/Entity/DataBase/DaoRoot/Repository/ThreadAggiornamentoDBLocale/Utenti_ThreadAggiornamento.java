package it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.FireBase.FB_Utenti_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Dao.Utenti_Dao;
import it.tn.unitn.disi.ppsmet.Entity.DataBase.DaoRoot.Repository.ThreadAggiornamentoDBLocale.Generico.Generico_Repository;
import it.tn.unitn.disi.ppsmet.Entity.Utenti;

public class Utenti_ThreadAggiornamento extends Generico_Repository {
/*################################################################################################*/
/*                         METODO DI AGGIORNAMENTO DELLA TABELLA ANNO ACCADEMICO LOCALE           */
/*################################################################################################*/
    /**
     * Questa classe contiene i metodi che vengono eseguiti in un thread estreno appena viene notificato
     * un aggionameto delle tabella "Utenti" nel database esterno.
     */
    private static class ThreadAggiornamento implements Runnable {
        protected static final String LOG_TAG = "THREAD_AGG_Utenti";
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
        /**
         * Questo parametro contiene la lista dei AnnoAccademici del database esterno che devono essere
         * presenti anche nel database interno.
         */
        private Utenti utentiFirebase;
        private Long idUtentiLocale;
/*------------------------------------------------------------------------------------------------*/
        /**
         * Questo metodo è il costruttore del thread che si occupa del aggionameto della tabella
         * Utenti.
         * @param utentiFirebase  Questo parametro contiene un elemente tra quelli presenti sul database
         *                        esterno corrispondente al elemento locale.
         * @param idUtentiLocale    Questo parametro contiene l'id dell'elemente tra quelli presenti sul database
         *                         interno con i quale si è richiesto il corrispettivo esterno.
         */
        public ThreadAggiornamento(Utenti utentiFirebase, Long idUtentiLocale) {
            super();
            this.utentiFirebase = utentiFirebase;
            this.idUtentiLocale = idUtentiLocale;
        }
/*================================================================================================*/
/*                         MEDODO RUN DEL THREAD                                                  */
/*================================================================================================*/
        /**
         * Questo è il metodo che viene chiamato che chontiene il main del nuovo thread.
         */
        @Override
        public void run() {
            Log.d(LOG_TAG,"Avvio di un tread per l'aggionamento della tabella Utenti.");
            aggionaTabellaUtenti(utentiFirebase, idUtentiLocale);
            Log.d(LOG_TAG,"Conclusione di un tread per l'aggionamento della tabella Utenti.");
        }
/*================================================================================================*/
/*                         MEDODO CHE CERIFICA E CALCOLA IL NUMERO MININIMO DI OPERAZIONI DA FARE */
/*================================================================================================*/
        /**
         * Questo metodo permette di calcolare ed identificare il numero minimo di inserimenti,
         * aggiornamenti e cancellazioni a partire dallo stato attuale del database locale.
         * Questo partendo dai dati presenti nel natabase esteno.
         * @param utentiFirebase
         * @param idUtentiLocale
         */
        private synchronized void aggionaTabellaUtenti(Utenti utentiFirebase, Long idUtentiLocale){
            Log.d(LOG_TAG,"/n Avvio procedura d'aggionamento della tabella Utenti.");
            //Dati presenti nel database locale.
            Utenti utentiLocale = utentiDao.getByPrimaryKey(idUtentiLocale);
            if(utentiLocale!=null){
                if(utentiFirebase==null){
                    eliminaElementi(utentiLocale);
                }else if(!utentiFirebase.equals(utentiLocale)){
                    aggiornaElementi(utentiFirebase);
                }
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO AGGIORNAMENTO ELEMENTI PRESENTI                                 */
/*================================================================================================*/
        private synchronized void aggiornaElementi(Utenti elemento){
            if(elemento!=null){
                utentiDao.update(elemento);
            }
        }
/*================================================================================================*/
/*================================================================================================*/
/*                         MEDODO PER LA CANCELLAZIONE DEI ELEMENTI                               */
/*================================================================================================*/
        private synchronized void eliminaElementi(Utenti elemento){
            if(elemento!=null){
                utentiDao.delete(elemento.getId());
            }
        }
/*================================================================================================*/
    }
/*################################################################################################*/
    //FIREBASE RUNTIME DATABASE
    private static FB_Utenti_Dao fbUtentiDao = new FB_Utenti_Dao();
/*------------------------------------------------------------------------------------------------*/
    //DIPENDENZE
    protected static AnnoAccademico_ThreadAggiornamento     dipendezaAnnoAccademico     = null;
    protected static CorsiDelUniversita_ThreadAggiornamento dipendezaCorsiDelUniversita = null;
/*------------------------------------------------------------------------------------------------*/
    //ROOM DATABASE
    protected static Utenti_Dao utentiDao=null;
    //QUERY ROOM DATABASE
    private static LiveData<List<Long>> allPrimaryKeyUtenti=null;
/*################################################################################################*/
/*================================================================================================*/
/*                         ATTIVAZIONE DI UN CONTROLLORE CHE SARA ATTIVO FINO ALLA FINE           */
/*                         ESECUZIONE DELL'APP                                                    */
/*================================================================================================*/
    private static void gestioneAggiornametoTabellaUtenti(){
        if(utentiDao==null){
            utentiDao = db.UtentiDao();
        }
        if(allPrimaryKeyUtenti==null){
            allPrimaryKeyUtenti= utentiDao.getAllPrimaryKey_Synchronized();
        }
        if(!allPrimaryKeyUtenti.hasObservers()){//SE NON HA già osservatori per rilevare aggiornamenti
            allPrimaryKeyUtenti.observeForever(new Observer<List<Long>>() {
                private  List<Long>  lastListUtenti = new LinkedList<>();
                private  List<Long>  temp = new LinkedList<>();
                //Serve solo per aggiornare i dati già presensti localmente
                @Override
                public void onChanged(List<Long> listaPrimaryKeyLocale) {
                    temp.clear();
                    temp.addAll(listaPrimaryKeyLocale);
                    if(!lastListUtenti.isEmpty()){
                        listaPrimaryKeyLocale.removeAll(lastListUtenti);
                    }
                    //Solo quelli nuovi
                    Iterator<Long> iterator = listaPrimaryKeyLocale.iterator();
                    while (iterator.hasNext()){
                        Long id =iterator.next();
                        if((id!=null)){
                            //Aggiungi un altro ossservatore
                            fbUtentiDao.getByPrimaryKey_Synchronized(id).observeForever(
                                    new MyUtentiObserver(id));
                        }
                    }
                    lastListUtenti.clear();
                    lastListUtenti.addAll(temp);
                }
            });
        }
    }
/*================================================================================================*/
/*################################################################################################*/
    private static class MyUtentiObserver implements Observer<Utenti>{
    private Long idUtentiLocale;
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public MyUtentiObserver(Long idUtentiLocale) {
        this.idUtentiLocale =idUtentiLocale;
    }
/*================================================================================================*/
/*================================================================================================*/
/*                         METODO CHIAMATO NEL MOMENTO DEL CAMBIAMNETO                            */
/*================================================================================================*/
    @Override
    public void onChanged(Utenti utentiFirebase) {
        executor.execute(new ThreadAggiornamento(utentiFirebase,this.idUtentiLocale));
    }
/*================================================================================================*/
}
/*################################################################################################*/
/*################################################################################################*/
/*                         OPERAZIONI FATTE SULLA TABELLA UTENTE(e le sue derivazioni seguendo    */
/*                         un ordine topologico per garantire gli aggiornamenti) ATTRAVERO IL     */
/*                         DATABASE LOCALE (room)                                                 */
/*################################################################################################*/
/*================================================================================================*/
/*                         COSTRUTORE                                                             */
/*================================================================================================*/
    public Utenti_ThreadAggiornamento(Application application) {
        super(application);
        if(dipendezaAnnoAccademico==null){
            dipendezaAnnoAccademico = new AnnoAccademico_ThreadAggiornamento(application);
        }
        if(dipendezaCorsiDelUniversita==null){
            dipendezaCorsiDelUniversita = new CorsiDelUniversita_ThreadAggiornamento(application);
        }
        gestioneAggiornametoTabellaUtenti();
    }
/*================================================================================================*/
/*################################################################################################*/
}
